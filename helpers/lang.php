<?php

function lang($string)
{
	return App__Ed__Model__Language__Model::get_translated_value($string);
}

function get_lang_list()
{
    $language_list = array();
    $id_service = (int)Lib__Session::get('id_service');
    $items = Language__App__Ed__Model__Service::find(
        Language__App__Ed__Model__Service::where("id_service=$id_service")
    );
    if(!empty($items))
    {
        $ids = get_lang_ids($items);
        $items_db = App__Ed__Model__Language::find(
            App__Ed__Model__Language::where("id IN (".implode(',',$ids).")"),
            App__Ed__Model__Language::order("name ASC")
        );
        
        //domyślny język zawsze pierwszy!!!
        foreach($items_db as $i_db)
        {
            if($i_db->id != Lib__Session::get('default_lang'))
            {
                $items_sorted[$i_db->id] = $i_db;
            }
            else 
            {
                $default_lang[$i_db->id] = $i_db;
            }
        }
        
        $items = array_merge($default_lang,$items_sorted);
    }
    return $items;
}

function get_lang_ids($lang_list)
{
    $ids = array();
    foreach($lang_list as $k=>$v)
    {
        if(isset($v->id_lang) && !empty($v->id_lang))
        {
            $ids[] = $v->id_lang;
        }
        else
        {
            $ids[] = $v->id;
        }
    }
    return $ids;
}