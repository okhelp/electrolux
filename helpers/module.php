<?php

function is_module_enabled($code_name)
{
	App__Ed__Model__Modules__Model::is_enabled($code_name);
}

function is_module_installed($code_name)
{
	App__Ed__Model__Modules__Model::is_installed($code_name);
}