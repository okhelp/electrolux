<?php

function is_ed()
{
    if(uri_segment(1) == 'ed')
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
    
}

function is_request()
{
    return !empty($_POST) || !empty($_FILES);
}

function is_ajax()
{
	return (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && 
			strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') 
				? TRUE : FALSE;
}

function is_admin()
{
	if(is_ed() && App__Ed__Model__Users::is_logged() && App__Ed__Model__Acl::has_group('admin', 0))
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

function verify_address($correct_url)
{
	$url = $_SERVER['SCRIPT_URI'];
	
	if($correct_url !== $url)
	{
		redirect($correct_url);
	}
}

function is_module_page()
{
    $uri_segment_number = is_ed() ? 2 : 1;
    return is_dir(BASE_PATH . 'app/modules/' . uri_segment($uri_segment_number));
}

function is_ie()
{
    return isset($_SERVER['HTTP_USER_AGENT']) && (strpos($_SERVER['HTTP_USER_AGENT'], 'Trident') !== false);
}