<?php

function unserialize_data($string)
{
    $string = preg_replace_callback ( '!s:(\d+):"(.*?)";!',
            function($match) 
            {
                return ($match[1] == strlen($match[2])) ? $match[0] : 's:' . strlen($match[2]) . ':"' . $match[2] . '";';
            },
            $string );
    return unserialize($string);
}

function is_email($string)
{
	return (filter_var($string, FILTER_VALIDATE_EMAIL)) ? TRUE : FALSE; 
}

function friendly_date($timestamp)
{
	/* langs past */
	$lang['Minute_Ago'] = 'Minutę temu';
	$lang['Minutes_Ago_2_4'] = '%s minuty temu';
	$lang['Minutes_Ago_5_59'] = '%s minut temu';
	$lang['Today'] = 'Dzisiaj, %s';
	$lang['Yesterday'] = 'Wczoraj, %s';
	$lang['Mon'] = 'Poniedziałek, %s';
	$lang['Tue'] = 'Wtorek, %s';
	$lang['Wed'] = 'Środa, %s';
	$lang['Thu'] = 'Czwartek, %s';
	$lang['Fri'] = 'Piątek, %s';
	$lang['Sat'] = 'Sobota, %s';
	$lang['Sun'] = 'Niedziela, %s';
	
	/* langs future */
	$lang['For_Minute'] = 'Za minutę';
	$lang['For_Minutes_2_4'] = 'Za %s minuty';
	$lang['For_Minutes_5_59'] = 'Za %s minut';
	$lang['Tomorow'] = 'Jutro, %s';
		$lang['Now'] = 'W tym momencie'; // można zmienić np. na 'Teraz'


	$timestamp = intval($timestamp);
	$now = time();
	$day_time = floor($timestamp/86400);
	$timedate = date("H:i", $timestamp); 
	$day_now = floor($now/86400);
	$same_day = ($day_time == $day_now) ? TRUE : FALSE;

	if ( $timestamp < 1 )
	{
		return FALSE;
	}
	$past_or_future = ($now > $timestamp) ? TRUE : FALSE;
	if ( $now == $timestamp )
	{
		return $lang['Now'];
	}
	else if  ( $past_or_future )
	{
		$maths = $now - $timestamp;
		$day_before = $day_now-$day_time;

		if ( $same_day && $maths <= 60 )
		{
			return $lang['Minute_Ago'];
		}
		else if ( $same_day && $maths > 60 && $maths <= 240 )
		{
			$ret = ceil($maths/60);
			return sprintf($lang['Minutes_Ago_2_4'], $ret);
		}
		else if ( $same_day && $maths > 240 && $maths <= 3540 )
		{
			$ret = ceil($maths/60);
			if ( substr($ret, 0, 1) > 1 && substr($ret, 1, 1) > 1 && substr($ret, 1, 1) < 5 )
			{
				return sprintf($lang['Minutes_Ago_2_4'], $ret);
			}
			else
			{
				return sprintf($lang['Minutes_Ago_5_59'], $ret);
			}
		}
		else if ( $same_day && $maths > 3540 )
		{
			return sprintf($lang['Today'], $timedate);
		}
		else if ( !$same_day && $day_before == 1 )
		{
			return sprintf($lang['Yesterday'], $timedate);
		}
			else if ( !$same_day && $day_before > 1 && $day_before < 7 )
		{
			if ( $day_before == 2 )
			{
			return sprintf($lang[date("D", ($now-172800))], $timedate);
			}
			else if ( $day_before == 3 )
			{
				return sprintf($lang[date("D", ($now-259200))], $timedate);
			}
			else if ( $day_before == 4 )
			{
				return sprintf($lang[date("D", ($now-345600))], $timedate);
			}
			else if ( $day_before == 5 )
			{
				return sprintf($lang[date("D", ($now-432000))], $timedate);
			}
			else if ( $day_before == 6 )
			{
				return sprintf($lang[date("D", ($now-518400))], $timedate);
			}
		}
		else
		{
			return date("Y-m-d, H:i", $timestamp);
		}
	}
	else
	{
		$maths = $timestamp - $now;
		$day_after = $day_time-$day_now;

		if ( $same_day && $maths <= 60 )
		{
			return $lang['For_Minute'];
		}
		else if ( $same_day && $maths > 60 && $maths <= 240 )
		{
			$ret = ceil($maths/60);
			return sprintf($lang['For_Minutes_2_4'], $ret);
		}
		else if ( $same_day && $maths > 240 && $maths <= 3540 )
		{
			$ret = ceil($maths/60);
			if ( substr($ret, 0, 1) > 1 && substr($ret, 1, 1) > 1 && substr($ret, 1, 1) < 5 )
			{
				return sprintf($lang['For_Minutes_2_4'], $ret);
			}
			else
			{
				return sprintf($lang['For_Minutes_5_59'], $ret);
			}
		}
		else if ( $same_day && $maths > 3540 )
		{
			return sprintf($lang['Today'], $timedate);
		}
		else if ( !$same_day && $day_after == 1 )
		{
			return sprintf($lang['Tomorow'], $timedate);
		}
			else if ( !$same_day && $day_after > 1 && $day_after < 7 )
		{
			if ( $day_after == 2 )
			{
			return sprintf($lang[date("D", ($now+172800))], $timedate);
			}
			else if ( $day_after == 3 )
			{
				return sprintf($lang[date("D", ($now+259200))], $timedate);
			}
			else if ( $day_after == 4 )
			{
				return sprintf($lang[date("D", ($now+345600))], $timedate);
			}
			else if ( $day_after == 5 )
			{
				return sprintf($lang[date("D", ($now+432000))], $timedate);
			}
			else if ( $day_after == 6 )
			{
				return sprintf($lang[date("D", ($now+518400))], $timedate);
			}
		}
		else
		{
			return date("Y-m-d, H:i", $timestamp);
		}
	}
}