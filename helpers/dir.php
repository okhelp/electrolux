<?php

/* tworzę drzewo katalogów */

function create_dirs($path)
{
	$path = str_replace(array('./', BASE_URL, BASE_PATH), '', $path);
	$path_arr = explode('/', $path);
	$path_arr = array_filter($path_arr);
	$path_add = BASE_PATH;

	foreach ($path_arr as $dir)
	{
		$path_add .= $dir . '/';
		if (!is_dir($path_add))
		{
			mkdir($path_add);
		}
	}
}

function copy_files_from_dir($source, $dest)
{
	if (is_dir($source))
	{
		$dir_handle = opendir($source);
		$sourcefolder = basename($source);
		mkdir($dest . "/" . $sourcefolder);
		while ($file = readdir($dir_handle))
		{
			if ($file != "." && $file != "..")
			{
				if (is_dir($source . "/" . $file))
				{
					copy_files_from_dir($source . "/" . $file, $dest . "/" . $sourcefolder);
				}
				else
				{
					copy($source . "/" . $file, $dest . "/" . $file);
				}
			}
		}
		closedir($dir_handle);
	}
	else
	{
		copy($source, $dest);
	}
}

function get_last_modification_dir($path)
{
	$dir = new SplFileInfo($path);
	return $dir->getMTime();
}

function remove_dir($path)
{
	$path = substr($path,-1) == '/' ? substr($path,0,-1) : $path;
	
	$files = glob($path . '/*');

	foreach ($files as $file)
	{
		if(is_dir($file))
		{
			remove_dir($file);
		}
		else
		{
			unlink($file);
		}
	}
	
	rmdir($path);
	return;
}

function get_module_path($module_name = NULL)
{
    return BASE_PATH . 'app/modules/' . (!empty($module_name) ? "$module_name/" : '');
}

function get_module_template_path($module_name)
{
    return get_module_path($module_name) . (is_ed() ? 'ed/' : 'front/') . 'templates/';
}

function get_app_template_path()
{
    return BASE_PATH . 'app/' . (is_ed() ? 'ed/' : 'front/') . 'templates/';
}

function force_download($filepath)
{
    // Process download
    if(file_exists($filepath))
    {
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename="'.basename($filepath).'"');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($filepath));
        flush(); // Flush system output buffer
        readfile($filepath);
        exit;
    }
}