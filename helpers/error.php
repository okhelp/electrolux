<?php

function show_404()
{
    header("HTTP/1.0 404 Not Found");
	
	$template_file_path = BASE_PATH . 'app/ed/templates/_partials/errors/404.tpl';
	
	$smarty = new Smarty;
	echo $smarty->fetch($template_file_path);
}

function show_403()
{
	header('HTTP/1.0 403 Forbidden');
	die('brak dostępu');
}
