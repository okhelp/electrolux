<?php

function get_customer_data($id_customer)
{
    return App__Ed__Model__Customers::find($id_customer);
}

function get_customer_name($id_customer='')
{
    $id_customer = (int)$id_customer;
    
    if(!$id_customer) 
        return '';
    
    $customer = get_customer_data($id_customer);
    return $customer->firstname.' '.$customer->surname;
}