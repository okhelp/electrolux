<?php

function generate_tree(array $elements, $start_id_parent=0, $end_id_parent = -1)
{
	$result = [];
	
	//jeżeli startuję od 0, a mam kilka głównych trzonów to muszę wybrać odpowiedni
	if(count($elements) > 1 && $start_id_parent == 0 && $end_id_parent != -1)
	{
		
		$elements = App__Ed__Model__Tree::find_main_key($elements,$end_id_parent);
		
	}
	
	foreach ($elements as $element)
	{
		if ($start_id_parent == $element['id_parent'])
		{
			$element['children'] = generate_tree($elements, $element['id'], $end_id_parent);

			if ($end_id_parent == -1 || $end_id_parent >= $element['id'])
			{
				$result[] = $element;
			}
		}
	}
	return $result;
}

function get_tree_array($tree)
{
	$return_list = array();

	$list_item = reset($tree);

	$return_list[] = $list_item;

	if (!empty($list_item['children']))
	{
	    $tree_array = get_tree_array($list_item['children']);
		$return_list[] = reset($tree_array);
	}

	return $return_list;
}
