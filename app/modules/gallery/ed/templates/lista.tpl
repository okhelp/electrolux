{load_plugin name="datatable"}

<a href="ed.html" class="btn btn-info">Dodaj</a>

<table class='datatable'>
	<thead>
		<tr>
			<th>#id</th>
			<th>nazwa</th>
			<th>kategoria</th>
			<th>status</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		{foreach from=$list item=l}
			<tr>
				<td>{$l->id}</td>
				<td>{$l->name}</td>
				<td>{if !empty($l->get_category())}{$l->get_category()->name}{/if}</td>
				<td>
					{if $l->status == 1}
						<span class="label label-success">aktywna</span>
					{else}
						<span class="label label-danger">nieaktywna</span>
					{/if}
				</td>
				<td class="text-right">
					<a href="{BASE_ED_URL}gallery/ed.html?id={$l->id}" class='btn btn-default'>edytuj</a>
					<a href="{BASE_ED_URL}gallery/usun.html?id={$l->id}" class='btn btn-danger confirm' data-confirm-text="Czy na pewno chcesz usunąć tą galerię?">usuń</a>
				</td>
			</tr>
		{/foreach}
	</tbody>
</table>
