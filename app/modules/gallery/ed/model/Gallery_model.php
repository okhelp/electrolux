<?php

    namespace App\modules\gallery\ed\model;

	use App\modules\gallery\ed\model\gallery\Gallery_categories_model;
    use App\modules\gallery\ed\model\gallery\Gallery_photos;
    use Lib__Base__Model;

    class Gallery_model extends Lib__Base__Model
	{
		static $table_name = "gallery";
		static $primary_key = "id";

		public function get_photos($on_page=0, $current_page = 0)
		{
			//określam limit
			if(!empty($current_page) && $on_page)
			{
				$offset = $current_page*$on_page;
			}
			else
			{
				$offset = $current_page;
			}

			if(!empty($on_page))
			{
				$limit = "LIMIT $offset,$on_page";
			}
			else
			{
				$limit = NULL;
			}
			
			//buduję zapytanie
			$sql = "SELECT SQL_CALC_FOUND_ROWS * "
					. "FROM gallery_photos "
					. "WHERE id_gallery = " . $this->id . " "
					. "ORDER by `sort` ASC $limit";
			
			$list = Gallery_photos::find_by_sql($sql);
			$total_rows = Gallery_photos::find_by_sql("SELECT FOUND_ROWS() as total");
			$total_rows = reset($total_rows)->total;
		
			return array(
				'list' => $list,
				'total' => $total_rows
			);
		}

		public function get_category()
		{
			$category = '';
			if($this->id_category)
			{
				$category = Gallery_categories_model::find($this->id_category);
			}

			return $category;
		}

		public function get_url()
		{
			$category = $this->get_category();
			return BASE_URL . "gallery/" . url_slug($category->name) . ',' . url_slug($this->name) 
					. ',g' . $this->id . '.html';
		}

		public function get_type()
		{
			if($this->id_type == 1)
			{
				return lang('Zdjęcia');
			}
			else
			{
				return lang('Wideo');
			}
		}

		public function get_main_photo()
		{
			$photo = NULL;

			//zdjęcia
			if($this->id_type == 1)
			{
				$photos = $this->get_photos();
				if(!empty($photos) && !empty($photos['list']))
				{
					$photo = $photos['list'];
					$photo = reset($photo);
					$photo = $photo->id_cimg;
				}
			}
			elseif($this->id_type == 2 && !empty($this->youtube))
			{

				//jeżeli mamy już cimg miniatury, zwracam go
				if($this->youtube_cimg)
				{
					$photo = $this->youtube_cimg;
				}
				else //nie ma, tworzę go
				{
					//pobieram adres url youtube
					$youtube_url = $this->youtube;

					//pobieram id filmu
					$youtube_url_arr = explode('/',$youtube_url);
					$youtube_id_movie = end($youtube_url_arr);
					$youtube_thumb = "https://img.youtube.com/vi/$youtube_id_movie/maxresdefault.jpg";
					$photo = (int)App__Ed__Model__Img::create_id_cimg($youtube_thumb);

					//zapisuję wartość do bazy
					$gallery = App__Ed__Model__Files::find($this->id);
					$gallery->youtube_cimg = $photo;
					$gallery->save();
				}

			}

			return $photo;
		}
	}
