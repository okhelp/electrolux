<?php

namespace App\modules\gallery\ed\gallery\model;

use App\modules\gallery\ed\model\gallery\Gallery_categories_model;

class Gallery
{
	public static function get_list($params = array(), $with_total_rows = 0)
	{
		//ustawiam limit
		$limit = NULL;
		
		//przygotowuję parametry
		$where = array();
		if(!empty($params))
		{
			if(isset($params['page']) && !empty($params['page']) 
					&& isset($params['on_page']) && !empty($params['on_page']))
			{
				$page = (int)$params['page'] - 1;
				$on_page = (int)$params['on_page'];
				unset($params['page'], $params['on_page']);
				
				$offset = $page*$on_page;
				$limit = "LIMIT $offset,$on_page";
			}
			elseif(!empty($params['on_page']))
			{
				$limit = "LIMIT 0," . (int)$params['on_page'];
				unset($params['on_page']);
			}
			
			foreach($params as $param_name => $param_value)
			{
				if(!empty($param_value))
				{
					if(strpos($param_value,'not_') !== FALSE)
					{
						$param_value = str_replace('not_','',$param_value);
						$where[] = $param_name . " NOT IN (" . "'$param_value')";
					}
					else
					{
						$where[] = $param_name . " = " . "'$param_value'";
					}
				}
			}
		}
		
		//buduję zaputanie
		$sql = "SELECT SQL_CALC_FOUND_ROWS g.*, gl.name, gl.description FROM gallery g ";
		//dołączam odpowiednią wersję językową
		$id_service = (int)Lib__Session::get('id_service');
		$id_lang = (int)Lib__Session::get('id_lang');
		$sql .= "INNER JOIN gallery_lang gl ON gallery_id = g.id AND "
				. "gl.id_service = $id_service AND gl.id_lang = $id_lang ";
		$sql .= !empty($where) ? " WHERE " . implode(" AND ",$where) : NULL;
		$sql .= "ORDER by g.ts DESC";
		$sql .= " $limit ";
		$list = App__Ed__Model__Files::find_by_sql($sql);
		
		//flaga, czy wyświetlamy ilość znalezionych rekordów.
		if($with_total_rows)
		{
			$total_rows = reset(App__Ed__Model__Files::find_by_sql("SELECT FOUND_ROWS() as total"))->total;
			
			return array(
				'list' => $list,
				'total_rows' => $total_rows
			);
		}
		else
		{
			return $list;
		}
		
	}
	
	public static function get_categories()
	{
		return Gallery_categories_model::find(Gallery_categories_model::order("name ASC"));
	}
}