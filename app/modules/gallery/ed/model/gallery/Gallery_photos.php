<?php

namespace App\modules\gallery\ed\model\gallery;

use App__Ed__Model__Img;
use Lib__Base__Model;

class Gallery_photos extends Lib__Base__Model
{
    static $table_name = "gallery_photos";
    static $primary_key = "id";
	
	public function get_photo_data()
	{
		return App__Ed__Model__Img::find($this->id_cimg);
	}
}