<?php

/**
 * Moduł Galerii zdjęć
 */

class App__Modules__Gallery__Index
{
    const AUTHOR = "Paweł Otłowski, imset.it";
    const VERSION = "1.0";
    const CODE_NAME = "gallery";
    const NAME = "Galeria zdjęć";
    const DESCRIPTION = "Moduł galerii zdjęć z podziałem na kategorie.";

    public static function install()
    {
        //wgrywam plik sql z bazą danych
        $sql_file_path = get_module_path(static::CODE_NAME) . '_install/install.sql';

        if(file_exists($sql_file_path))
        {
            $pdo = new Lib__PDO;
            $pdo->query(file_get_contents($sql_file_path));

            //pobieram id modułu
            $id_module = App__Ed__Model__Modules__Model::get_id_module(self::CODE_NAME);

            //dodaję główne menu
            $module_ed_menu = new App__Ed__Model__Modules__Ed_menu;
            $module_ed_menu->id_module = $id_module;
            $module_ed_menu->id_parent = 0;
            $module_ed_menu->name = "Galeria zdjęć";
            $module_ed_menu->url = "#";
            $module_ed_menu->save();

            //ustalam id menu głównego
            $id_main_module_menu = $module_ed_menu->id;

            //dodaję podmenu

            $module_ed_menu = new App__Ed__Model__Modules__Ed_menu;
            $module_ed_menu->id_module = $id_module;
            $module_ed_menu->id_parent = $id_main_module_menu;
            $module_ed_menu->name = "Lista";
            $module_ed_menu->url = "gallery/lista.html";
            $module_ed_menu->save();

            $module_ed_menu = new App__Ed__Model__Modules__Ed_menu;
            $module_ed_menu->id_module = $id_module;
            $module_ed_menu->id_parent = $id_main_module_menu;
            $module_ed_menu->name = "Kategorie";
            $module_ed_menu->url = "gallery/categories/lista.html";
            $module_ed_menu->save();        

        }
        else
        {
            throw new Exception('Brak pliku ze zrzutem bazy danych: ' . $sql_file_path);
        }
    }

    public static function uninstall()
    {
        $sql[] = "DROP TABLE gallery;";
        $sql[] = "DROP TABLE gallery_lang;";
        $sql[] = "DROP TABLE gallery_categories;";
        $sql[] = "DROP TABLE gallery_categories_lang;";
        $sql[] = "DROP TABLE gallery_photos ;";

        $pdo = new Lib__PDO;
        $pdo->query(implode(' ', $sql));
    }
}

