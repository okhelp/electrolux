<?php

use App\modules\prizes\ed\models\Prize_category_model;
use App\modules\prizes\ed\objects\Add_category;
use App\modules\prizes\ed\objects\Prizes_categories_tree;
use App\modules\prizes\ed\objects\Save_category_data;
use App\modules\prizes\ed\objects\Remove_product_category;

class App__Modules__Prizes__Ed__Controller__Categories extends Lib__Base__Ed_Controller
{
    public function action_lista()
    {
        $tree = Prizes_categories_tree::get_categories();

        $this->view->assign('tree', json_encode($tree, JSON_UNESCAPED_UNICODE));
        $this->template = get_module_template_path('prizes') . 'categories_list.tpl';
        $this->page_title = "Lista kategorii";
        $this->breadcrumb = ['' => 'Lista kategorii'];
    }

    public function action_ed()
    {
        if (!empty($_POST) && !empty($_GET['id']))
        {
            $category_data = new Save_category_data($_POST, (string)$_GET['id']);
            go_back($category_data->save() ? 'g|Dane zostały zapisane' : 'd|Dane nie zostały zapisane', BASE_ED_URL . 'prizes/categories/lista.html');
        }
        else
        {
            if (!empty($_GET['id']))
            {
                $category = Prize_category_model::find($_GET['id']);

                if (!empty($category))
                {
                    $this->view->assign('category', $category);
                    $this->view->assign(
                        'category_tree',
                        Prizes_categories_tree::get_ed_tree(
                            0,
                            -1,
                            'id_parent',
                            !empty($category->{Prize_category_model::COLUMN_PARENT_ID}) ? $category->{Prize_category_model::COLUMN_PARENT_ID} : '0'
                        )
                    );
                    $this->template = get_module_template_path('prizes') . 'category_edit.tpl';
                    $this->page_title = "Edycja kategorii";
                }
            }
            else
            {
                go_back('d|Brak dostępu');
            }
        }
    }

    public function action_remove()
    {
        $message = 'd|Brak dostępu';

        if (!empty($_GET['id']))
        {
            try
            {
                $remove_product_category = new Remove_product_category($_GET['id']);
                $remove_product_category->remove();

                $message = "g|Kategoria została usunięta.";
            }
            catch (Exception $exception)
            {
                $message = "d|" . $exception->getMessage();
            }

        }

        go_back($message);
    }

    public function action_create()
    {
        if (!empty($_POST['save']))
        {
            $category_add = new Add_category($_POST);

            go_back($category_add->add() ? "g|Kategoria została dodana." : "d|Kategoria nie została dodana.", BASE_ED_URL . 'prizes/categories/lista.html');
        }

        $this->view->assign(
            'category_tree',
            Prizes_categories_tree::get_ed_tree(0, -1, 'id_parent', null)
        );

        $this->template = get_module_template_path('prizes') . 'category_add.tpl';
        $this->page_title = "Dodanie kategorii";
    }
}