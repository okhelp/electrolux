<?php

use App\modules\prizes\ed\models\Prize_model;
use App\modules\prizes\ed\models\Prize_photos_model;
use App\modules\prizes\ed\objects\Prize_add;
use App\modules\prizes\ed\objects\Prizes_categories_tree;
use App\modules\prizes\ed\objects\Remove_prize;
use App\modules\prizes\ed\objects\Save_prize_data;
use App\modules\prizes\front\collections\Get_prize_variants;

class App__Modules__Prizes__Ed__Controller__Index extends Lib__Base__Ed_Controller
{
    public function __construct()
    {
        parent::__construct();

        if(!App__Ed__Model__Acl::has_access('prizes', 0))
        {
            go_back('w|Brak dostępu.'); die();
        }
    }

    public function action_lista()
    {
        $this->view->assign('prizes', Prize_model::find_by_sql("
            SELECT prize.*, prize_lang.*, prize_variant.points
            FROM prize
            JOIN prize_lang ON prize.id=prize_lang.prize_id
            JOIN prize_variant ON id_prize=prize.id
            WHERE prize_lang.id_lang={$_SESSION['id_lang']} AND prize_lang.id_service={$_SESSION['id_service']}
                AND prize.deleted_at IS NULL
        "));
        $this->template = get_module_template_path('prizes') . 'prizes_list.tpl';
        $this->page_title = "Lista nagród";
        $this->breadcrumb = ['' => 'Lista nagród'];
    }

    public function action_ed()
    {
        if (!empty($_POST['save']) && !empty($_GET['id']))
        {
            try
            {
                $save_product_data = new Save_prize_data($_POST, $_GET['id']);
                $save_product_data->save();

                $message = "g|Zmiany zostały zapisane";
            }
            catch (Exception $exception)
            {
                $message = "d|Zmiany nie zostały zapisane";
            }

            go_back($message, BASE_ED_URL . 'prizes/lista.html');
        }

        if (!empty($_GET['id']))
        {
            $prize = Prize_model::find($_GET['id']);

            $this->view->assign(
                'category_tree',
                Prizes_categories_tree::get_ed_tree(
                0, -1, 'id_parent',!empty($prize->{Prize_model::COLUMN_CATEGORY_ID}) ? $prize->{Prize_model::COLUMN_CATEGORY_ID} : null
                )
            );

            $this->view->assign('prize_variants', Get_prize_variants::get_by_id($_GET['id']));
            $this->view->assign('photos_list', $this->get_photos((int)$_GET['id']));

            $this->view->assign('prize', $prize);
            $this->template = get_module_template_path('prizes') . 'prize_edit.tpl';
            $this->page_title = "Edycja nagrody";
            $this->breadcrumb = [
                'prizes/lista.html'                => 'Lista nagród',
                'prizes/ed.html?id=' . $_GET['id'] => 'Edycja nagrody',
            ];
        }
    }

    private function get_photos(int $id_prize): array
    {
        $return = [];
        $photos_list = Prize_photos_model::find(
            Prize_photos_model::where("id_prize = $id_prize"),
            Prize_photos_model::order("sort ASC")
        );

        if (!empty($photos_list))
        {
            $ids = [];
            foreach ($photos_list as $photo)
            {
                $ids[] = $photo->id_cimg;
            }

            $ids = implode(',', $ids);

            $return = App__Ed__Model__Img::find(
                App__Ed__Model__Img::where("id IN ($ids)"),
                App__Ed__Model__Img::order("FIELD(id, $ids)")
            );
        }

        return $return;
    }

    public function action_add()
    {
        if(!empty($_POST))
        {
            $post_data = $_POST;
            $picture_data = empty($_FILES['image']) ? [] : $_FILES['image'];

            $prize_add = new Prize_add($post_data, $picture_data);

            go_back($prize_add->add() ? 'g|Nagroda została dodana' : 'd|Nagroda nie została dodana', BASE_ED_URL . 'prizes/lista.html');
        }

        $this->view->assign(
            'category_tree',
            Prizes_categories_tree::get_ed_tree(0, -1, 'id_parent', 0)
        );

        $this->view->assign('photos_list', $this->get_photos(0));

        $this->template = get_module_template_path('prizes') . 'prize_add.tpl';
        $this->page_title = "Dodawanie nagrody";
        $this->breadcrumb = ['' => 'Dodawanie nagrody'];
    }

    public function action_remove()
    {
        $message = 'd|Brak dostępu';

        if(!empty($_GET['id']))
        {
            $remove_product = new Remove_prize((string)$_GET['id']);
            $remove_product->remove();

            $message = 'g|Produkt został usunięty';
        }

        go_back($message);
    }
}