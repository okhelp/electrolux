{if !empty($category)}
    <form method="POST">
        <div class="form-group">
            <label>Nazwa:</label>
            <input type="text" class="form-control" name="name" required="required"
                   value="{if !empty($category->name)}{$category->name}{/if}"/>
        </div>
        <input type="hidden" name="category_lang_id" value="{$category->id}">
        <div class="form-group">
            <div id="accordion">
                <div class="card">
                    <div class="card-header" id="headingOne">
                        <h5 class="mb-0">
                            <a class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                Wybór kategorii nadrzędnej
                            </a>
                        </h5>
                    </div>

                    <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                        <div class="card-body">
                            {$category_tree}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <button type="submit" name="save" value="1" class="btn btn-dafault">Zapisz</button>
    </form>
{else}
    <div class="alert alert-info">
        Nie odnaleziono kategorii
    </div>
{/if}