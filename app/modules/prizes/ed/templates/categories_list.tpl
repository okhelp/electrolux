{load_plugin name="jstree"}
{load_plugin name="bootbox"}

<div class="row">
    <div class="col-md-4 col-sm-8 col-xs-8">
        <button type="button" class="btn btn-success btn-sm" onclick="create();">Dodaj</button>
        <button type="button" class="btn btn-warning btn-sm" onclick="rename();">Edytuj</button>
        <button type="button" class="btn btn-danger btn-sm" onclick="delete_category();">Usuń</button>
        <input type="text" value="" id="demo_q" placeholder="Wyszukaj">
    </div>
</div>
<div class="row" style="margin-top: 15px; margin-left: 5px;">
    <div id="jst"></div>
</div>

<script>
    {literal}
    $(function() {
        $('#jst').jstree({
            'core' : {
                'data' : {/literal}{$tree}{literal},
                check_callback : true,
            },
            "types" : {
                "1" : { "icon": "glyphicon glyphicon-tags", "valid_children" : [] },
                "0" : { "icon": "glyphicon glyphicon-ban-circle", "valid_children" : [] },
            },
            "plugins" : ["search", "state", "types", "wholerow" ]
        });
    });


    function create() {
        window.location.href=BASE_ED_URL + "prizes/categories/create.html";
    };
    function rename() {
        var ref = $('#jst').jstree(true),
            sel = ref.get_selected();
        if(!sel.length) { return false; }
        sel = sel[0];

        window.location.href = BASE_ED_URL + "prizes/categories/ed.html?id=" + sel;
    };
    function delete_category() {
        var ref = $('#jst').jstree(true),
            sel = ref.get_selected();
        if(!sel.length) { return false; }

        sel = sel[0];
        var link = "remove.html?id=" + sel;
        var text = "Czy na pewno chcesz usunąć tą kategorię?";

        bootbox.confirm({
            title: '',
            message: text,
            buttons: {
                confirm: {
                    label: 'Tak',
                    className: 'btn-info'
                },
                cancel: {
                    label: 'Nie',
                    className: 'btn-danger'
                }
            },
            callback: function(result) {
                if (result) {
                    document.location.href = link;
                }
            }
        });
    };

    $(function () {
        var to = false;
        $('#demo_q').keyup(function () {
            if(to) { clearTimeout(to); }
            to = setTimeout(function () {
                var v = $('#demo_q').val();
                $('#jst').jstree(true).search(v);
            }, 250);
        });

    });
    {/literal}
</script>

<style>
    #jst .glyphicon-ban-circle {
        color: red !important;
    }
    #jst .glyphicon-tags {
        color: green !important;
    }
</style>
