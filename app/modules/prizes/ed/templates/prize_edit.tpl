{load_plugin name="ckeditor"}
{if !empty($prize)}
    <form method="POST" enctype="multipart/form-data">
        <div class="form-group">
            <label>Tytuł:</label>
            <input type="text" class="form-control" name="name" required="required"
                   value="{$prize->name}"/>
        </div>
        <div class="form-group">
            <label>Podtytuł:</label>
            <input type="text" class="form-control" name="sub_name"
                   value="{$prize->sub_name}"/>
        </div>
        <div class="form-group">
            <label>Krótki opis:</label>
            <textarea class="form-control" name="short_description" required="required">{$prize->short_description}</textarea>
        </div>
        <div class="form-group">
            <label>Opis:</label>
            <textarea class="form-control ckeditor" name="description" required="required">{$prize->description}</textarea>
        </div>
        <div class="form-group">
            <label>Lista zdjęć:</label>
            <div class="row">
                <div class="col-md-12">
                    {include file="_partials/upload/images_form.tpl" name="site_social_logo" uploaded_data=$photos_list instance_number=1 file_limit=100 show_thumb=1 multiple=1}
                </div>
            </div>
        </div>
        <div class="form-group">
            <label>Status:</label>
            <select name="status" class="form-control">
                <option value="1" {if $prize->status == 1}selected="selected"{/if}>aktywny</option>
                <option value="0" {if $prize->status == 0}selected="selected"{/if}>nieaktywny</option>
            </select>
        </div>
        <div class="form-check">
            <input type="checkbox" class="form-check-input" name="year_prize" value="1" {if !empty($prize->year_prize)}checked{/if}>
            <label class="form-check-label">Nagroda roczna</label>
        </div>
        <div class="form-group mt-3">
            <label>Szczegóły</label>
            <textarea name="details" class="ckeditor">{$prize->details}</textarea>
        </div>
        <div class="form-group">
            <div id="accordion">
                <div class="card">
                    <div class="card-header" id="headingOne">
                        <h5 class="mb-0">
                            <a class="btn btn-link" data-toggle="collapse" data-target="#collapseOne"
                               aria-expanded="true"
                               aria-controls="collapseOne">
                                Wybór kategorii nagrody
                            </a>
                        </h5>
                    </div>

                    <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                        <div class="card-body">
                            {$category_tree}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="variants-container">
            {if !empty($prize_variants)}
                {foreach $prize_variants as $prize_variant}
                    <div class="variant">
                        <div class="form-group">
                            <label>Tytuł wariantu</label>
                            <input type="text" class="form-control" name="existing_variant_title[{$prize_variant->id}]"
                                   value="{$prize_variant->title}"/>
                        </div>
                        <div class="form-group">
                            <label>Opis wariantu</label>
                            <textarea type="text" class="form-control"
                                      name="existing_variant_description[{$prize_variant->id}]">{$prize_variant->description}</textarea>
                        </div>
                        <div class="form-group">
                            <label>Cena w E-LUX</label>
                            <input type="text" class="form-control" name="existing_variant_price[{$prize_variant->id}]"
                                   value="{$prize_variant->points}"/>
                        </div>
                        <a class="btn btn-danger remove-variant">Usuń wariant</a>
                        <hr>
                    </div>
                {/foreach}
            {/if}
        </div>
        <a id="add-option" class="btn btn-success">Dodaj wariant nagrody</a>
        <button type="submit" name="save" value="1" class="btn btn-dafault">Zapisz</button>
    </form>
    <script>
        function add_event_to_remove_variant_button()
        {
            $('.remove-variant').click(function(){
                $(this).parent().remove();
            });
        }

        $(document).ready(function () {
            $('#add-option').click(function () {
                $('#variants-container').append('<div class="variant"><div class="form-group">\n' +
                    '            <label>Tytuł wariantu</label>\n' +
                    '            <input type="text" class="form-control" name="variant_title[]"/>\n' +
                    '        </div>' +
                    '<div class="form-group">\n' +
                    '            <label>Opis wariantu</label>\n' +
                    '            <textarea type="text" class="form-control" name="variant_description[]"></textarea>\n' +
                    '        </div>' +
                    '<div class="form-group">\n' +
                    '            <label>Cena w E-LUX</label>\n' +
                    '            <input type="text" class="form-control" name="variant_price[]"/>\n' +
                    '        </div><a class="btn btn-danger remove-variant">Usuń wariant</a>' +
                    '<hr></div>');

                add_event_to_remove_variant_button();
            });

            add_event_to_remove_variant_button();
        });
    </script>
{else}
    <div class="alert alert-info">
        Nie odnaleziono produktu
    </div>
{/if}