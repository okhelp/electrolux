{load_plugin name="datatable"}
{load_plugin name="bootbox"}

<a class="btn btn-success" href="{BASE_ED_URL}prizes/add.html">Dodaj nagrodę</a>

<table class='datatable'>
	<thead>
	<tr>
		<th>Nazwa</th>
		<th>Nagroda roczna</th>
		<th>Data utworzenia</th>
		<th>Data modyfikacji</th>
		<th>Status</th>
		<th>{if $smarty.session.id == 1364 || $smarty.session.id == 1480}E-LUX{/if}</th>
	</tr>
	</thead>
	<tbody>
	{foreach $prizes as $prize}
		<tr>
			<td>{$prize->name}</td>
			<td>
				{if $prize->year_prize == 1}
					<span class="label label-success">Tak</span>
				{else}
					<span class="label label-info">Nie</span>
				{/if}
			</td>
			<td>{$prize->created_at->format('Y-m-d H:i:s')}</td>
			<td>{if $prize->updated_at}{$prize->updated_at->format('Y-m-d H:i:s')}{else}-{/if}</td>
			<td>
				{if $prize->status == 1}
					<span class="label label-success">aktywny</span>
				{else}
					<span class="label label-danger">nieaktywny</span>
				{/if}
			</td>
			{if $smarty.session.id == 1364 || $smarty.session.id == 1480}
				<td>
					{$prize->points}
				</td>
			{else}
				<td class="text-right">
					<a href="{BASE_ED_URL}prizes/ed.html?id={$prize->id}" class='btn btn-default'>edytuj</a>
					<a href="{BASE_ED_URL}prizes/remove.html?id={$prize->id}" class='btn btn-danger confirm' data-confirm-text="Czy na pewno chcesz usunąć tą pozycję?">usuń</a>
				</td>
			{/if}
		</tr>
	{/foreach}
	</tbody>
</table>

