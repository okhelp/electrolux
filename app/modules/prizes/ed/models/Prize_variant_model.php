<?php
declare(strict_types=1);

namespace App\modules\prizes\ed\models;

use Lib__Base__Model;

class Prize_variant_model extends Lib__Base__Model
{
    const COLUMN_ID          = 'id';
    const COLUMN_POINTS      = 'points';
    const COLUMN_ID_PRIZE    = 'id_prize';
    const COLUMN_CREATED_AT  = 'created_at';
    const COLUMN_DELETED_AT  = 'deleted_at';
    const COLUMN_DESCRIPTION = 'description';
    const COLUMN_TITLE       = 'title';

    const TABLE_NAME = 'prize_variant';

    static $table_name = self::TABLE_NAME;
    static $primary_key = self::COLUMN_ID;
}