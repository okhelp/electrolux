<?php

namespace App\modules\prizes\ed\models;

use Lib__Base__Model;

class Prize_category_model extends Lib__Base__Model
{
    const TABLE_NAME = 'prize_category';

    const COLUMN_ID         = 'id';
    const COLUMN_PARENT_ID  = 'parent_id';
    const COLUMN_CREATED_AT = 'created_at';
    const COLUMN_UPDATED_AT = 'updated_at';
    const COLUMN_NAME       = 'name';
    const COLUMN_SEO_NAME   = 'seo_name';

    static $table_name = self::TABLE_NAME;
    static $primary_key = self::COLUMN_ID;
}