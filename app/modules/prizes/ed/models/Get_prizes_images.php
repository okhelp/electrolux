<?php
declare(strict_types=1);

namespace App\modules\prizes\ed\models;

class Get_prizes_images
{
    public static function get(array $prizes_ids)
    {
        $prizes_images_to_return = [];

        if(!empty($prizes_ids))
        {
            $prizes_photos = Prize_photos_model::find_by_sql("
                SELECT *
                FROM prize_photos
                WHERE id_prize IN ('" . implode("','", $prizes_ids) . "')
            ");

            if(!empty($prizes_photos))
            {
                foreach ($prizes_photos as $prize_photo)
                {
                    $prizes_images_to_return[$prize_photo->{Prize_photos_model::COLUMN_ID_PRIZE}][] = $prize_photo;
                }
            }
        }

        return $prizes_images_to_return;
    }
}