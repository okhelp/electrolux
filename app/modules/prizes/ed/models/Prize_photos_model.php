<?php

namespace App\modules\prizes\ed\models;

use Lib__Base__Model;

class Prize_photos_model extends Lib__Base__Model
{
    const TABLE_NAME = 'prize_photos';

    const COLUMN_ID       = 'id';
    const COLUMN_ID_PRIZE = 'id_prize';
    const COLUMN_ID_CIMG  = 'id_cimg';
    const COLUMN_SORT   = 'sort';

    static $table_name = self::TABLE_NAME;
    static $primary_key = self::COLUMN_ID;
}