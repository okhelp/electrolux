<?php
declare(strict_types=1);

namespace App\modules\prizes\ed\models;

use App\ed\model\log\Log;
use App\modules\prizes\ed\objects\Prizes_import;

class Parse_prizes_from_actualization_file
{
    /** @var \PHPExcel */
    private $data;

    /** @var Log */
    private $log;

    /** @var array */
    private $columns;

    /**
     * Parse_prizes_from_actualization_file constructor.
     * @param \PHPExcel $data
     * @param Log $log
     */
    public function __construct(\PHPExcel $data, Log $log)
    {
        $this->data = $data;
        $this->log = $log;
    }

    public function parse(): array
    {
        $this->log->add('Rozpoczynam pobieranie nagród');

        $data_to_return = [];

        $sheet = $this->data->getSheet(0);
        $highest_row = $sheet->getHighestRow();
        $highest_column = $sheet->getHighestColumn();

        //iteracja po wierszach pliku
        for ($row = 1; $row <= $highest_row; $row++)
        {
            $row_data = $sheet->rangeToArray(
                'A' . $row . ':' . $highest_column . $row,
                null,
                true,
                false
            );

            if (empty($row_data[0]))
            {
                continue;
            }
            else
            {
                $row_data = $row_data[0];
            }

            if ($row == 1 && !empty($row_data))
            {
                $this->columns = $row_data;
                $this->log->add('Pobrałem kolumny');
            }
            else
            {
                $prize_data = [
                    Prizes_import::COLUMN_NAME              => $row_data[array_search(Prizes_import::COLUMN_NAME, $this->columns)],
                    Prizes_import::COLUMN_SUB_TITLE         => $row_data[array_search(Prizes_import::COLUMN_SUB_TITLE, $this->columns)],
                    Prizes_import::COLUMN_SHORT_DESCRIPTION => $row_data[array_search(Prizes_import::COLUMN_SHORT_DESCRIPTION, $this->columns)],
                    Prizes_import::COLUMN_DESCRIPTION       => $row_data[array_search(Prizes_import::COLUMN_DESCRIPTION, $this->columns)],
                    Prizes_import::COLUMN_DETAILS           => $row_data[array_search(Prizes_import::COLUMN_DETAILS, $this->columns)],
                    Prizes_import::COLUMN_POINTS            => intval($row_data[array_search(Prizes_import::COLUMN_POINTS, $this->columns)]),
                    Prizes_import::COLUMN_ANNUAL            => intval($row_data[array_search(Prizes_import::COLUMN_ANNUAL, $this->columns)]),
                ];

                //rozpoczęcie pobierania variantów nagród
                $variant_iteration = 0;
                for ($i = 8; $i <= (count($this->columns) - 1); $i++)
                {
                    $variant_iteration++;

                    if($variant_iteration == 1 && !empty($row_data[$i]) && !empty($row_data[$i + 1]) && !empty($row_data[$i + 2]))
                    {
                        $variant = [
                            Prizes_import::COLUMN_VARIANT_NAME => $row_data[$i],
                            Prizes_import::COLUMN_VARIANT_DESCRIPTION => $row_data[$i + 1],
                            Prizes_import::COLUMN_VARIANT_POINTS => intval($row_data[$i + 2]),
                        ];

                        $prize_data['variants'][] = $variant;
                    }

                    if($variant_iteration == 3)
                    {
                        $variant_iteration = 0;
                    }
                }
                $this->log->add('Pobrałem dane dla wiersza ' . $row);
                $data_to_return[] = $prize_data;
            }
        }

        $this->log->add('Zakończono parsowanie danych, ilość znalezionych wierszy to ' . count($data_to_return));
        return $data_to_return;
    }

}