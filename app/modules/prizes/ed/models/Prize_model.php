<?php

namespace App\modules\prizes\ed\models;

use Lib__Base__Model;

class Prize_model extends Lib__Base__Model
{
    const TABLE_NAME = 'prize';

    const COLUMN_ID                = 'id';
    const COLUMN_CATEGORY_ID       = 'category_id';
    const COLUMN_STATUS            = 'status';
    const COLUMN_CREATED_AT        = 'created_at';
    const COLUMN_UPDATED_AT        = 'updated_at';
    const COLUMN_DELETED_AT        = 'deleted_at';
    const COLUMN_NAME              = 'name';
    const COLUMN_SUB_NAME          = 'sub_name';
    const COLUMN_SHORT_DESCRIPTION = 'short_description';
    const COLUMN_DETAILS           = 'details';
    const COLUMN_DESCRIPTION       = 'description';
    const COLUMN_YEAR_PRIZE        = 'year_prize';

    static $table_name = self::TABLE_NAME;
    static $primary_key = self::COLUMN_ID;
}