<?php
declare(strict_types=1);

namespace App\modules\prizes\ed\objects;

use App\modules\prizes\ed\models\Prize_model;
use App\modules\prizes\ed\models\Prize_photos_model;
use App\modules\prizes\ed\models\Prize_variant_model;

class Prize_add
{
    /** @var int|null */
    private $category_id;

    /** @var int|null */
    private $sub_name;

    /** @var int */
    private $price;

    /** @var int */
    private $status;

    /** @var string */
    private $name;

    /** @var string|null */
    private $description;

    /** @var array */
    private $pictures;

    /** @var int */
    private $year_prize;

    /** @var string */
    private $short_description;

    /** @var string */
    private $details;

    /** @var array */
    private $prize_variants_names;

    /** @var array */
    private $prize_variants_descriptions;

    /** @var array */
    private $prize_variants_prices;

    /**
     * Prize_add constructor.
     * @param array $post_data
     * @param array $picture_data
     */
    public function __construct(array $post_data, array $picture_data)
    {
        $this->status = empty($post_data['status']) ? 0 : $post_data['status'];
        $this->name = empty($post_data['name']) ? '' : $post_data['name'];
        $this->category_id = empty($post_data['id_parent']) ? null : $post_data['id_parent'];
        $this->description = empty($post_data['description']) ? '' : $post_data['description'];
        $this->pictures = empty($post_data['upload_1']) ? [] : $post_data['upload_1'];
        $this->year_prize = (int)!empty($post_data['year_prize']);
        $this->short_description = empty($post_data['short_description']) ? '' : $post_data['short_description'];
        $this->details = empty($post_data['details']) ? '' : $post_data['details'];
        $this->sub_name = empty($post_data['sub_name']) ? '' : $post_data['sub_name'];

        $this->prize_variants_names = empty($post_data['variant_title']) ? [] : $post_data['variant_title'];
        $this->prize_variants_descriptions = empty($post_data['variant_description']) ? [] : $post_data['variant_description'];
        $this->prize_variants_prices = empty($post_data['variant_price']) ? [] : $post_data['variant_price'];
    }

    /**
     * @return bool
     */
    public function add(): bool
    {
        $prize = new Prize_model();
        $prize->{Prize_model::COLUMN_CATEGORY_ID} = $this->category_id;
        $prize->{Prize_model::COLUMN_STATUS} = $this->status;
        $prize->{Prize_model::COLUMN_NAME} = $this->name;
        $prize->{Prize_model::COLUMN_DESCRIPTION} = $this->description;
        $prize->{Prize_model::COLUMN_DETAILS} = $this->details;
        $prize->{Prize_model::COLUMN_YEAR_PRIZE} = $this->year_prize;
        $prize->{Prize_model::COLUMN_SHORT_DESCRIPTION} = $this->short_description;
        $prize->{Prize_model::COLUMN_SUB_NAME} = $this->sub_name;

        $prize_saved = $prize->save();

        $this->add_new_variants($prize);

        $this->upload_pictures($prize);

        return $prize_saved;
    }

    private function add_new_variants(Prize_model $prize)
    {
        if (!empty($this->prize_variants_names) && !empty($this->prize_variants_descriptions))
        {
            for ($i = 0; $i < count($this->prize_variants_names); $i++)
            {
                $variant = new Prize_variant_model();
                $variant->{Prize_variant_model::COLUMN_ID_PRIZE} = $prize->{Prize_model::COLUMN_ID};
                $variant->{Prize_variant_model::COLUMN_DESCRIPTION} = empty($this->prize_variants_descriptions[$i]) ? '' : $this->prize_variants_descriptions[$i];
                $variant->{Prize_variant_model::COLUMN_TITLE} = empty($this->prize_variants_names[$i]) ? '' : $this->prize_variants_names[$i];
                $variant->{Prize_variant_model::COLUMN_POINTS} = empty($this->prize_variants_prices[$i]) ? 0 : $this->prize_variants_prices[$i];
                $variant_saved = $variant->save();
            }
        }
    }

    private function upload_pictures(Prize_model $prize)
    {
        if (!empty($prize->id))
        {
            $photos = Prize_photos_model::find(Prize_photos_model::where("id_prize = $prize->id"));
            if (!empty($photos))
            {
                foreach ($photos as $photo)
                {
                    $photo->delete();
                }
            }
        }

        if (!empty($this->pictures))
        {
            foreach ($this->pictures as $id_cimg)
            {
                $gallery_photo = new Prize_photos_model();
                $gallery_photo->{Prize_photos_model::COLUMN_ID_PRIZE} = $prize->id;
                $gallery_photo->{Prize_photos_model::COLUMN_ID_CIMG} = $id_cimg;
                $gallery_photo->save();
            }
        }
    }

}