<?php
declare(strict_types=1);

namespace App\modules\prizes\ed\objects;

use App\modules\prizes\ed\models\Prize_category_model;

class Add_category
{
    /** @var array */
    private $category_data;

    /**
     * @param array $new_data
     */
    public function __construct(array $new_data)
    {
        $this->category_data = $new_data;
    }

    /**
     * @return bool
     */
    public function add(): bool
    {
        $category = new Prize_category_model();
        $category->{Prize_category_model::COLUMN_NAME} = $this->category_data['name'];
        $category->{Prize_category_model::COLUMN_SEO_NAME} = url_slug($this->category_data['name']);
        $category->{Prize_category_model::COLUMN_PARENT_ID} = empty($this->category_data['id_parent']) ? null : $this->category_data['id_parent'];

        return $category->save();
    }
}