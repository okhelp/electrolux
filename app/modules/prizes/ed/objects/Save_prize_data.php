<?php
declare(strict_types=1);

namespace App\modules\prizes\ed\objects;

use ActiveRecord\DateTime;
use App\modules\prizes\ed\models\Prize_model;
use App\modules\prizes\ed\models\Prize_photos_model;
use App\modules\prizes\ed\models\Prize_variant_model;
use App\modules\prizes\front\collections\Get_prize_variants;

class Save_prize_data
{
    /** @var array */
    private $new_data;

    /** @var int */
    private $prize_id;

    /**
     * @param array $new_data
     * @param string $prize_id
     */
    public function __construct(array $new_data, string $prize_id)
    {
        $this->new_data = $new_data;
        $this->prize_id = $prize_id;
    }

    /**
     * @throws \Exception
     */
    public function save()
    {
        $prize = Prize_model::find($this->prize_id);

        $prize_to_update = Prize_model::find($prize->{Prize_model::COLUMN_ID});
        if (!empty($prize_to_update))
        {
            $prize_to_update->{Prize_model::COLUMN_STATUS} = $this->new_data['status'];
            $prize_to_update->{Prize_model::COLUMN_CATEGORY_ID} = $this->new_data['id_parent'];
            $prize_to_update->{Prize_model::COLUMN_NAME} = $this->new_data['name'];
            $prize_to_update->{Prize_model::COLUMN_SUB_NAME} = $this->new_data['sub_name'];
            $prize_to_update->{Prize_model::COLUMN_DETAILS} = $this->new_data['details'];
            $prize_to_update->{Prize_model::COLUMN_DESCRIPTION} = $this->new_data['description'];
            $prize_to_update->{Prize_model::COLUMN_YEAR_PRIZE} = $this->new_data['year_prize'];
            $prize_to_update->{Prize_model::COLUMN_SHORT_DESCRIPTION} = $this->new_data['short_description'];

            $prize_to_update->save();

            $this->remove_variants();

            $this->add_new_variants($prize_to_update);

            $this->update_existing_variants($prize_to_update);

            $this->upload_pictures();
        }
    }

    private function add_new_variants(Prize_model $prize)
    {
        if (!empty($this->new_data['variant_title']) && !empty($this->new_data['variant_description']))
        {
            for ($i = 0; $i < count($this->new_data['variant_title']); $i++)
            {
                $variant = new Prize_variant_model();
                $variant->{Prize_variant_model::COLUMN_ID_PRIZE} = $prize->{Prize_model::COLUMN_ID};
                $variant->{Prize_variant_model::COLUMN_DESCRIPTION} = empty($this->new_data['variant_description'][$i]) ? '' : $this->new_data['variant_description'][$i];
                $variant->{Prize_variant_model::COLUMN_TITLE} = empty($this->new_data['variant_title'][$i]) ? '' : $this->new_data['variant_title'][$i];
                $variant->{Prize_variant_model::COLUMN_POINTS} = empty($this->new_data['variant_price'][$i]) ? 0 : $this->new_data['variant_price'][$i];
                $variant->save();
            }
        }
    }

    private function update_existing_variants(Prize_model $prize)
    {
        if (!empty($this->new_data['existing_variant_title']) && !empty($this->new_data['existing_variant_description']))
        {
            foreach ($this->new_data['existing_variant_title'] as $index => $title)
            {
                $variant = Prize_variant_model::find($index);

                if(!empty($variant))
                {
                    $variant->{Prize_variant_model::COLUMN_ID_PRIZE} = $prize->{Prize_model::COLUMN_ID};
                    $variant->{Prize_variant_model::COLUMN_DESCRIPTION} = empty($this->new_data['existing_variant_description'][$index]) ? '' :
                        $this->new_data['existing_variant_description'][$index];
                    $variant->{Prize_variant_model::COLUMN_TITLE} = empty($this->new_data['existing_variant_title'][$index]) ? '' :
                        $this->new_data['existing_variant_title'][$index];
                    $variant->{Prize_variant_model::COLUMN_POINTS} = empty($this->new_data['existing_variant_price'][$index]) ? 0 :
                        $this->new_data['existing_variant_price'][$index];
                    $variant->save();
                }
            }
        }
    }

    private function remove_variants()
    {
        $variants = Get_prize_variants::get_by_id((int)$this->prize_id);

        if (!empty($variants))
        {
            $variants_ids = array_map(
                function($variant) {
                    return $variant->id;
                }, $variants);

            $variants_ids_from_form = array_keys($this->new_data['existing_variant_title']);

            if (!empty($variants_ids))
            {
                foreach ($variants_ids as $variant_id)
                {
                    if (!in_array($variant_id, $variants_ids_from_form))
                    {
                        $variant = Prize_variant_model::find($variant_id);
                        $variant->{Prize_variant_model::COLUMN_DELETED_AT} = (new DateTime())->format('Y:m:d H:i:s');
                        $variant->save();
                    }
                }
            }
        }
    }

    private function upload_pictures()
    {
        if (!empty($this->prize_id))
        {
            $photos = Prize_photos_model::find(Prize_photos_model::where("id_prize = $this->prize_id"));
            if (!empty($photos))
            {
                foreach ($photos as $photo)
                {
                    $photo->delete();
                }
            }
        }

        if (!empty($_POST['upload_1']))
        {
            foreach ($_POST['upload_1'] as $id_cimg)
            {
                $gallery_photo = new Prize_photos_model();
                $gallery_photo->{Prize_photos_model::COLUMN_ID_PRIZE} = $this->prize_id;
                $gallery_photo->{Prize_photos_model::COLUMN_ID_CIMG} = $id_cimg;
                $gallery_photo->save();
            }
        }
    }
}