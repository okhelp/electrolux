<?php
declare(strict_types=1);

namespace App\modules\prizes\ed\objects;

use ActiveRecord\DateTime;
use App\modules\prizes\ed\models\Prize_model;
use App__Ed__Model__Img;

class Remove_prize
{
    /** @var int */
    private $prize_id;

    /**
     * @param string $prize_id
     */
    public function __construct(string $prize_id)
    {
        $this->prize_id = $prize_id;
    }

    public function remove()
    {
        $prize_to_remove = Prize_model::find($this->prize_id);
        $prize_to_remove->{Prize_model::COLUMN_DELETED_AT} = (new DateTime('now'))->format('Y-m-d h:i:s');
        $prize_to_remove->save();
    }
}