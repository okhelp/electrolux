<?php

namespace App\modules\prizes\ed\objects;

use App\modules\prizes\ed\models\Prize_category_model;
use App\modules\products\front\collections\Get_all_products_categories_collection;
use App\modules\products\front\collections\Get_product_category_by_electrolux_id;
use App\modules\products\front\models\Product_category_model;
use RecursiveArrayIterator;
use RecursiveIteratorIterator;
use Smarty;

class Prizes_categories_tree
{
    public static function get_ed_tree($start_id_parent = 0, $status, $input_name, $checked_value = null, $multi_value = 0)
    {
        $smarty = new Smarty;
        $smarty->assign('list', self::get_list($start_id_parent, $status));
        $smarty->assign('input_name', $input_name);
        $smarty->assign('checked_value', self::prepare_checked_value($checked_value));
        $smarty->assign('multi_value', $multi_value);

        return $smarty->fetch(get_module_template_path('prizes') . "_partials/tree.tpl");
    }

    private static function prepare_checked_value($checked_value)
    {
        return $checked_value;
    }

    public static function get_list($start_id_parent = 0, $status = 1, $end_id_parent = -1, $mode = 'without_pages', $tree = 1)
    {
        $arr = [];

        $list = Prize_category_model::all();

        foreach ($list as $category)
        {
            $arr[] = [
                'id'        => $category->{Prize_category_model::COLUMN_ID},
                'id_parent' => $category->{Prize_category_model::COLUMN_PARENT_ID},
                'name'      => $category->{Prize_category_model::COLUMN_NAME},
                'data'      => $category,
            ];
        }

        return $tree ? generate_tree($arr, $start_id_parent, $end_id_parent) : $arr;
    }

    public static function get($start_id_parent = 0, $status = 1, $end_id_parent = -1, $mode = 'without_pages', $tree = 1)
    {
        $arr = [];

        $list = Get_all_products_categories_collection::get($_SESSION['id_lang']);

        foreach ($list as $category)
        {
            $category_path = self::build_path($category->electrolux_id, $list);

            if(in_array($start_id_parent, $category_path))
            {
                $arr[] = $category;
            }
        }

        return $arr;
    }

    /**
     * @param $category_id
     * @param $categories
     * @return array
     */
    public static function build_path($category_id, $categories)
    {
        $path = [$category_id];

        if (!empty($categories[$category_id]->parent_id))
        {
            return array_merge($path, self::build_path($categories[$category_id]->parent_id, $categories));
        }

        return $path;
    }

    /**
     * @param array $elements
     * @param int $parentId
     * @return array
     */
    private static function categories_tree(array $elements, $parentId = 0)
    {
        $branch = [];

        foreach ($elements as $element)
        {
            if ($element['parent_id'] == $parentId)
            {
                $children = self::categories_tree($elements, $element['id']);
                if ($children)
                {
                    $element['children'] = $children;
                }
                $branch[] = $element;
            }
        }

        return $branch;
    }

    /**
     * @param string $category_id
     * @param int $lang_id
     * @return array
     */
    public static function get_categories(string $category_id = '0', $lang_id = 1): array
    {
        $categories_array = [];

        $categories_to_return = [];

        $categories = Product_category_model::find_by_sql(
            "
            SELECT prize_category.id, prize_category.parent_id, prize_category_lang.name AS text, 1 AS type
            FROM prize_category
            JOIN prize_category_lang ON prize_category_lang.prize_category_id=prize_category.id
            WHERE prize_category_lang.id_lang=$lang_id AND prize_category_lang.id_service={$_SESSION['id_service']}
            ORDER BY prize_category.parent_id DESC;
        ");

        foreach ($categories as $category)
        {
            $categories_array[] = $category->to_array();
        }

        $categories_tree = self::categories_tree($categories_array);

        $arrIt = new RecursiveIteratorIterator(new RecursiveArrayIterator($categories_tree));

        foreach ($arrIt as $sub)
        {
            $subArray = $arrIt->getSubIterator();
            if (empty($category_id))
            {
                $categories_to_return[] = iterator_to_array($subArray);
            }
            else
            {
                if ($subArray['id'] === $category_id)
                {
                    $categories_to_return[] = iterator_to_array($subArray);
                }
            }
        }

        return $categories_to_return;
    }

    public static function get_categories_list(array $categories, string $category_id, $lang_id = 1): array
    {
        $categories_list = [];

        $categories_array = [];

        if (!empty($categories))
        {
            foreach ($categories as $category)
            {
                $categories_array[] = $category->to_array();
            }

            $categories_tree = self::categories_tree($categories_array, Get_product_category_by_electrolux_id::get($category_id, $lang_id)->parent_id);

            $categories_iterator = new RecursiveIteratorIterator(new RecursiveArrayIterator($categories_tree));

            foreach ($categories_iterator as $index => $value)
            {
                if ($index == 'electrolux_id' && $value == $category_id)
                {
                    $searched_category_data = $categories_iterator->getSubIterator();
                    $categories_list[] = iterator_to_array($searched_category_data);
                    $parent_id = $searched_category_data['parent_id'];
                    for ($count = $categories_iterator->getDepth(); $count && $count--;)
                    {
                        $parent_data = $categories_iterator->getSubIterator($count);
                        foreach ($parent_data as $parent_key => $parent)
                        {
                            if (is_array($parent))
                            {
                                if (!isset($parent['electrolux_id']))
                                {
                                    continue;
                                }

                                if ($parent['electrolux_id'] == $parent_id)
                                {
                                    $parent_id = $parent['parent_id'];
                                    $categories_list[] = $parent;
                                }
                            }
                            else
                            {
                                if ($parent_key == 'electrolux_id' && $parent == $parent_id)
                                {
                                    $categories_list[] = iterator_to_array($parent_data);
                                    $parent_id = $parent_data['parent_id'];
                                }
                            }
                        }
                    }
                    break;
                }
            }
        }

        return $categories_list;
    }

    public static function get_categories_with_pages(int $category_id = 0, $lang_id = 1)
    {
        $categories = self::get_categories($category_id, $lang_id);

        $categories_ids = [];

        $categories_iterator = new RecursiveIteratorIterator(new RecursiveArrayIterator($categories));

        foreach ($categories_iterator as $index => $value)
        {
            if ($index == 'id' && is_numeric($value))
            {
                $categories_ids[] = (int)$value;
            }
        }

        $pages_by_category_id = Pages::get_pages_by_categories_ids($categories_ids);

        return ['categories' => $categories, 'pages' => $pages_by_category_id];
    }
}