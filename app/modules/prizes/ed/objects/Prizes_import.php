<?php
declare(strict_types=1);

namespace App\modules\prizes\ed\objects;

use App\ed\model\log\Log;
use App\modules\prizes\ed\models\Parse_prizes_from_actualization_file;
use App\modules\prizes\ed\models\Prize_model;
use App\modules\products\front\interfaces\Runnable_interface;
use DateTime;
use Exception;

class Prizes_import implements Runnable_interface
{
    const ACTUALIZATION_FILE_PATH = BASE_PATH . 'data/uploads/actualization/';

    const LOG_FILE_PATH = BASE_PATH . 'data/log/';

    const COLUMN_NAME                = 'name';
    const COLUMN_SUB_TITLE           = 'sub_title';
    const COLUMN_SHORT_DESCRIPTION   = 'short_description';
    const COLUMN_DESCRIPTION         = 'description';
    const COLUMN_DETAILS             = 'details';
    const COLUMN_POINTS              = 'points';
    const COLUMN_ANNUAL              = 'annual';
    const COLUMN_VARIANT_NAME        = 'variant_name';
    const COLUMN_VARIANT_DESCRIPTION = 'variant_description';
    const COLUMN_VARIANT_POINTS      = 'variant_points';

    const IMPORT_CATEGORY_ID = 23;

    /** @var Log */
    private $log;

    /** @var array */
    private $data;

    private $actualization_file;

    public function __construct()
    {
        $this->log = new Log(self::LOG_FILE_PATH . 'log_prizes_import_' . (new DateTime('now'))->format('Y-m-d') . '.txt');
    }

    public function run()
    {
        $this->get_file_data();

        $this->add_prizes();
    }

    /**
     * @throws \PHPExcel_Exception
     * @throws \PHPExcel_Reader_Exception
     */
    private function get_file_data()
    {
        require_once BASE_PATH . 'lib/phpexcel.class.php';

        $this->add_log('Wczytywanie pliku aktualizacji');
        $this->actualization_file = \PHPExcel_IOFactory::load(self::ACTUALIZATION_FILE_PATH . "prizes_" . (new DateTime('now'))->format('Ymd') . '.xlsx');

        $this->add_log('Parsowanie danych z pliku');
        $data_parser = new Parse_prizes_from_actualization_file($this->actualization_file, $this->log);
        $this->data = $data_parser->parse();
    }

    private function add_prizes()
    {
        if (!empty($this->data))
        {
            $this->add_log('Rozpoczęcie dodawania nagród do bazy');

            foreach ($this->data as $prize)
            {
                $post_data = [
                    'status'            => 0,
                    'name'              => $prize[Prizes_import::COLUMN_NAME],
                    'id_parent'         => Prizes_import::IMPORT_CATEGORY_ID,
                    'description'       => $prize[Prizes_import::COLUMN_DESCRIPTION],
                    'upload_1'          => [],
                    'year_prize'        => $prize[Prizes_import::COLUMN_ANNUAL],
                    'short_description' => $prize[Prizes_import::COLUMN_SHORT_DESCRIPTION],
                    'details'           => $prize[Prizes_import::COLUMN_DETAILS],
                    'sub_name'          => $prize[Prizes_import::COLUMN_SUB_TITLE],
                ];

                if (!empty($prize['variants']))
                {
                    foreach ($prize['variants'] as $variant)
                    {
                        $post_data['variant_title'][] = $variant['variant_name'];
                        $post_data['variant_description'][] = $variant['variant_description'];
                        $post_data['variant_price'][] = $variant['variant_points'];
                    }
                }

                $prize_add = new Prize_add($post_data, []);

                if ($prize_add->add())
                {
                    $this->add_log('Nagroda została dodana - ' . $prize[Prizes_import::COLUMN_NAME]);
                }
                else
                {
                    $this->add_log('Nagroda nie została dodana - ' . $prize[Prizes_import::COLUMN_NAME]);
                }
            }
        }
        else {
            $this->add_log('Brak nagród do dodania');
        }
    }

    /**
     * @param string $message
     * @throws Exception
     */
    private function add_log(string $message)
    {
        $this->log->add($message);
    }
}