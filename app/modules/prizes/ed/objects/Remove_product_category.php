<?php
declare(strict_types=1);

namespace App\modules\prizes\ed\objects;

use App\modules\prizes\ed\models\Prize_category_model;

class Remove_product_category
{
    /** @var string */
    private $category_id;

    /**
     * @param string $category_id
     */
    public function __construct(string $category_id)
    {
        $this->category_id = $category_id;
    }

    /**
     * @throws \ActiveRecord\RecordNotFound|\Exception
     */
    public function remove()
    {
        if ($this->has_childs())
        {
            throw new \Exception('Kategoria posiada kategorie podrzędne, nie można jej usunąć.');
        }
        else
        {
            $category_to_remove = Prize_category_model::find($this->category_id);
            if (!empty($category_to_remove))
            {
                $category_to_remove->delete();
            }
        }

    }

    /**
     * @return bool
     */
    private function has_childs()
    {
        $childs = Prize_category_model::find_by_sql(
            "
            SELECT " . Prize_category_model::COLUMN_ID . "
            FROM " . Prize_category_model::TABLE_NAME . "
            WHERE " . Prize_category_model::COLUMN_PARENT_ID . " = '" . $this->category_id . "'
        ");

        return (bool)!empty($childs);
    }
}