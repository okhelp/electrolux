<div class="container-fluid awards_main_list page_content equalHeightContainer">
    {if !empty($categories)}
        {assign var=points value={$smarty.session.user_points}}
        {foreach $categories as $category}
            <div class="row awards_main_list__row">
                <div class="col-12 d-flex align-items-center">
                    <a href="{BASE_URL}prizes/category.html?id={$category->id}"
                       class="awards_main_title">{$category->name}</a>
                    <a href="{BASE_URL}prizes/category.html?id={$category->id}{if isset($smarty.get.annual)}&annual{/if}"
                       class="btn btn-outline-primary btn-pills ml-3 ml-sm-5 awards_main_title_button">zobacz więcej
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-12 d-flex flex-wrap awards_main_list__row_prices">
                    {foreach $category->prizes as $prize}
                        {if $prize->year_prize == 1}
                            {assign var=userPoints value={$annual}}
                        {else}
                            {assign var=userPoints value={$points}}
                        {/if}
                        {if {$prize->price} > 0}
                            {math assign=progress equation="x / y * 100" x={$userPoints} y={$prize->price} format="%.2f"}
                            {math assign=progress_missing equation="y - x" x={$userPoints} y={$prize->price}}
                        {elseif empty($prize->price)}
                            {assign var=progress value = false}
                            {assign var=progress_missing value = false}
                        {/if}
                        <div class="product_listing__single_product {if !empty($progress) && $progress < 100}no_points{/if}">
                            <div class="row single_product__img_row">
                                <div class="col-12 single_product_info--wrapper">
                                    <a href="{BASE_URL}prizes/prize.html?id={$prize->id}">
                                        {if !empty($prize->photos[0]->id_cimg)}
                                            <img class="single_product__img img-fluid extra_info lazy"
                                                 data-src="{img id_cimg={$prize->photos[0]->id_cimg} width=300 height=300}">
                                        {else}
                                            <img class="single_product__img img-fluid extra_info lazy"
                                                 data-src="{img id_cimg=0}">
                                        {/if}
                                    </a>
                                </div>
                            </div>
                            {if !empty($progress)}
                            <div class="row progress_wrapper">
                                <div class="col-12">
                                    <div class="progress_label">
                                        <span>0</span><span>1/2</span><span>p</span>
                                    </div>
                                    <div class="progress">
                                        <div class="progress-bar" role="progressbar" style="width: {$progress}%" aria-valuenow="{$progress}" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                    <div class="text-right progress_missing">
                                        <span>{if ($progress_missing) < 0}+{math equation="x * y" x=-1 y=$progress_missing}{else}{$progress_missing}{/if}</span>
                                    </div>
                                </div>
                            </div>
                            {/if}
                            <div class="row single_product__price_row">
                                <div class="col-12 d-flex flex-column justify-content-between">
                                    <p class="single_product__name"><a href="{BASE_URL}prizes/prize.html?id={$prize->id}">{$prize->name}</a></p>
                                    <p class="single_product__points">{if !empty($prize->price)}{$prize->price} E-LUX{/if}</p>
                                </div>
                            </div>
                            <div class="row single_product__buttons">
                                <div class="col-6">
                                    <a href="{BASE_URL}prizes/prize.html?id={$prize->id}"
                                       class="btn btn-outline-primary btn-pills single_product__more">więcej
                                    </a>
                                </div>
                                {if !empty($progress) && $progress < 100}
                                <div class="col-6">
                                    <a href="#" class="btn btn-primary btn-pills single_product__order">
                                        dodaj
                                    </a>
                                </div>
                                {else}
                                    <div class="col-6">
                                        <a href="{BASE_URL}prizes/prize.html?id={$prize->id}#tab-1" class="btn btn-primary btn-pills single_product__order">
                                            dodaj
                                        </a>
                                    </div>
                                {/if}
                            </div>
                        </div>
                    {/foreach}
                </div>
            </div>
        {/foreach}
    {/if}
</div>
