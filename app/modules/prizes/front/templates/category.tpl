<form id="prize_filters">
    <div class="container-fluid product_list page_content">
        <div class="row mb-4">
            <div class="col-lg-3 listing__main_col_small d-flex align-items-center">
                <div class="row">
                    <div class="col-12">
                        <p id="filter_show" class="product_list_filter__title">Filtruj Produkty<span
                                    class="product_list_filter__icon_show"><i class="icon-arrow"></i><i
                                        class="icon-filter_cross"></i></span></p>
                    </div>
                </div>
            </div>
            <div class="col-lg-9 listing__main_col_big">
                <div class="row product_list_sort__row">
                    <div class="col-auto d-none d-md-flex align-items-center">
                        <a id="grid" class="product_list_sort__link" href="#"><i
                                    class="icon-filter_grid filter_grid_icon"></i></a>
                        <a id="list" class="product_list_sort__link" href="#"><i
                                    class="icon-filter_list1 filter_grid_icon"></i></a>
                        <a id="list_no_img" class="product_list_sort__link" href="#"><i
                                    class="icon-filter_list2 filter_grid_icon"></i></a>
                    </div>
                    <div class="col d-flex justify-content-sm-end product_list_sort__options--wrapper">
                        <div class="row w-auto-sm justify-content-sm-end">
                            <div class="col-12 col-sm-auto">
                                <div class="form-group row mb-0">
                                    <label for="products_count_select"
                                           class="col-auto col-form-label product_list_sort__label">Wyświetl:</label>
                                    <div class="col col-sm-auto sort_select_wrapper">
                                        <select class="form-control" id="products_count_select" name="products_count_select">
                                            <option selected value="15">15</option>
                                            <option value="30">30</option>
                                            <option value="all">Wszystkie</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-auto">
                                <div class="form-group row mb-0">
                                    <label for="products_sort_select"
                                           class="col-auto col-form-label no-wrap product_list_sort__label">Sortuj według:</label>
                                    <div class="col col-sm-auto sort_select_wrapper">
                                        <select class="form-control" id="products_sort_select" name="products_sort_select">
                                            <option value="popularity">Popularność</option>
                                            <option value="elux_asc">E-LUX rosnąco</option>
                                            <option selected value="elux_desc">E-LUX malejąco</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row position-relative">
            <div class="col-lg-3 listing__main_col_small product_list_filter">
                <div class="row product_list_filter__row">
                    <div class="col-12">
                        <a class="product_list_filter__link_collapse" data-toggle="collapse" href="#filter_card1"
                           role="button" aria-expanded="false" aria-controls="filter_card1">Rodzaj produktu <i
                                    class="icon-filter_arrow product_list_filter__icon"></i></a>
                        <div class="row">
                            <div class="col">
                                {if !empty($subcategories)}
                                    <div class="collapse show multi-collapse" id="filter_card1">
                                        <ul id="product_category" class="product_list_filter__options">
                                            {foreach $subcategories as $subcategory}
                                                <li class="filter_options__wrapper">
                                                    <label class="filter_options__label">
                                                        <input type="checkbox" class="filter_options__input"
                                                               name="subcategory_id[]"
                                                               value="{$subcategory->id}">{$subcategory->name}
                                                        <span class="filter_options__helper"></span>
                                                    </label>
                                                </li>
                                            {/foreach}
                                        </ul>
                                    </div>
                                {/if}
                            </div>
                        </div>
                    </div>
                </div>
                {*<div class="row product_list_filter__row">*}
                {*<div class="col-12">*}
                {*<a class="product_list_filter__link_collapse" data-toggle="collapse" href="#filter_card2"*}
                {*role="button" aria-expanded="false" aria-controls="filter_card2">Sterowanie <i*}
                {*class="icon-filter_arrow product_list_filter__icon"></i></a>*}
                {*<div class="row">*}
                {*<div class="col">*}
                {*<div class="collapse show multi-collapse" id="filter_card2">*}
                {*<ul class="product_list_filter__options">*}
                {*<li class="filter_options__wrapper">*}
                {*<label class="filter_options__label">*}
                {*<input type="checkbox" class="filter_options__input" name="" value="">manualnie<span*}
                {*class="filter_options__helper"></span>*}
                {*</label>*}
                {*</li>*}
                {*<li class="filter_options__wrapper">*}
                {*<label class="filter_options__label">*}
                {*<input type="checkbox" class="filter_options__input" name="" value="">panel*}
                {*cyfrowy<span class="filter_options__helper"></span>*}
                {*</label>*}
                {*</li>*}
                {*</ul>*}
                {*</div>*}
                {*</div>*}
                {*</div>*}
                {*</div>*}
                {*</div>*}
                {*<div class="row product_list_filter__row">*}
                {*<div class="col-12">*}
                {*<a class="product_list_filter__link_collapse" data-toggle="collapse" href="#filter_card3"*}
                {*role="button" aria-expanded="false" aria-controls="filter_card3">Rodzaj produktu <i*}
                {*class="icon-filter_arrow product_list_filter__icon"></i></a>*}
                {*<div class="row">*}
                {*<div class="col">*}
                {*<div class="collapse show multi-collapse" id="filter_card3">*}
                {*<ul class="product_list_filter__options">*}
                {*<li class="filter_options__wrapper">*}
                {*<label class="filter_options__label">*}
                {*<input type="checkbox" class="filter_options__input" name="" value="">CombiSteam*}
                {*Deluxe<span class="filter_options__helper"></span>*}
                {*</label>*}
                {*</li>*}
                {*<li class="filter_options__wrapper">*}
                {*<label class="filter_options__label">*}
                {*<input type="checkbox" class="filter_options__input" name="" value="">CombiSteam<span*}
                {*class="filter_options__helper"></span>*}
                {*</label>*}
                {*</li>*}
                {*<li class="filter_options__wrapper">*}
                {*<label class="filter_options__label">*}
                {*<input type="checkbox" class="filter_options__input" name="" value="">SousVide<span*}
                {*class="filter_options__helper"></span>*}
                {*</label>*}
                {*</li>*}
                {*<li class="filter_options__wrapper">*}
                {*<label class="filter_options__label">*}
                {*<input type="checkbox" class="filter_options__input" name="" value="">PlusSteam<span*}
                {*class="filter_options__helper"></span>*}
                {*</label>*}
                {*</li>*}
                {*</ul>*}
                {*</div>*}
                {*</div>*}
                {*</div>*}
                {*</div>*}
                {*</div>*}
            </div>
            <div class="col-lg-9 listing__main_col_big product_listing">
                <div class="row">
                    <div class="col-12">
                        <div id="prizes_container" class="row product_listing__row product_listing--grid equalHeightContainer">
                            {include file={get_module_template_path('prizes')|cat:"_partials/prizes_listing.tpl"}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" value="{$category_id}" name="category_id">
</form>
<script>
    $(document).ready(function () {


        function paginationButtons() {
            $('.pagination a').on('click', function (e) {
                e.preventDefault();
                var page_number = $(this).data('page');
                sendForm2(page_number);
            });
        }
        paginationButtons();
        $('#product_category, #products_count_select, #products_sort_select').change(function () {
            sendForm();
        });
        function sendForm() {
            loaderInit();
            $.post(BASE_URL + 'prizes/change_filter.html{if isset($smarty.get.annual)}?annual{/if}', $('#prize_filters').serialize(), function (response) {
                $('#prizes_container').html(response);
            }).done(function () {
                initProductListing();
                paginationButtons();
                loaderHide();
            });
        }
        function sendForm2(page_number) {
            loaderInit();
            $.ajax({
                url: BASE_URL + 'prizes/change_filter.html{if isset($smarty.get.annual)}?annual{/if}',
                type: 'POST',
                data: $('#prize_filters').serialize() + "&page=" + page_number,
                success: function (response) {
                    $('#prizes_container').html(response);
                }
            }).done(function () {
                initProductListing();
                paginationButtons();
                loaderHide();
            });
        }
        $('#product_category_select input[name="subcategories_ids[]"]').change(function () {
            var valueCheck = $(this).val();
            console.log(valueCheck);
            if (valueCheck === 'all') {
                $('.product_listing__single_product').fadeIn();
            } else {
                $('.product_listing__single_product').hide();
                $('.product_listing__single_product[data-item="'+ valueCheck +'"]').fadeIn();
            }
        });
        if ($(window).width() >= 991) {
            lastView();
        }
    });
</script>