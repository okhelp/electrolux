{if !empty($prizes)}
    {assign var=points value={$smarty.session.user_points}} {*PRZYKŁADOWA ILOŚC PUNKTÓW*}
    {foreach $prizes as $prize}
        {if !empty($prize->price) && {$prize->price} > 0}
        {math assign=progress equation="x / y * 100" x={$points} y={$prize->price} format="%.2f"}
        {math assign=progress_missing equation="y - x" x={$points} y={$prize->price}}
            {elseif empty($prize->price)}
            {assign var=progress value = false}
            {assign var=progress_missing value = false}
        {/if}
        <div class="product_listing__single_product {if !empty($progress) && $progress < 100}no_points{/if}">
            <div class="row single_product__img_row">
                <div class="col-12 single_product_info--wrapper">
                    <a href="{BASE_URL}prizes/prize.html?id={$prize->id}">
                        {if !empty($prize->photos[0]->id_cimg)}
                            <img class="single_product__img img-fluid extra_info lazy"
                                 data-tooltip-content="#tooltip_content{$prize->id}"
                                 data-src="{img id_cimg={$prize->photos[0]->id_cimg} width=300 height=300}">
                        {else}
                            <img class="single_product__img img-fluid extra_info"
                                 data-tooltip-content="#tooltip_content{$prize->id}"
                                 src="{img id_cimg=0 width=300 height=300}">
                        {/if}
                    </a>
                    <div class="tooltip_templates">
                        <div class="extra_info_tooltip" id="tooltip_content{$prize->id}">
                            <div class="container">
                                <div class="row mb-3">
                                    <div class="col-6">
                                        <p class="single_product__name single_product__name--bold">
                                            {$prize->name}
                                        </p>
                                    </div>
                                    <div class="col-6 text-right">
                                        {if !empty($prize->price)}
                                            <p class="single_product__points">
                                                {$prize->price}
                                                E-LUX</p>
                                        {/if}
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <div class="col-12">
                                        <p class="tooltip_product__subtitle">
                                            {$prize->sub_name}
                                        </p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <p class="tooltip_product__description">
                                            {$prize->short_description}
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {if !empty($progress)}
            <div class="row progress_wrapper">
                <div class="col-12">
                    <div class="progress_label">
                        <span>0</span><span>1/2</span><span>p</span>
                    </div>
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" style="width: {$progress}%" aria-valuenow="{$progress}" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                    <div class="text-right progress_missing">
                        <span>{if ($progress_missing) < 0}+{math equation="x * y" x=-1 y=$progress_missing}{else}{$progress_missing}{/if}</span>
                    </div>
                </div>
            </div>
            {/if}
            <div class="row single_product__price_row">
                <div class="col-6 single_product__name--container">
                    <p class="single_product__name"><a href="{BASE_URL}prizes/prize.html?id={$prize->id}">{$prize->name}</a></p>
                </div>
                <div class="col-6 single_product__price--container">
                    <p class="single_product__points">{if !empty($prize->price)}{$prize->price} <span class="text-nowrap">E-LUX</span>{/if}</p>

                </div>
            </div>
            <div class="row single_product__buttons">
                <div class="col-6 justify-content-center d-flex single_product__buttons--two">
                    <a href="{BASE_URL}prizes/prize.html?id={$prize->id}"
                       class="btn btn-outline-primary btn-pills single_product__more">
                        więcej
                    </a>
                </div>
                {if !empty($progress) && $progress < 100}
                <div class="col-6 justify-content-center d-flex single_product__buttons--two">
                    <a href="#"
                            class="btn btn-primary btn-pills single_product__order">
                        dodaj
                    </a>
                </div>
                    {else}
                    <div class="col-6 justify-content-center d-flex single_product__buttons--two">
                        <a href="{BASE_URL}prizes/prize.html?id={$prize->id}#tab-1"
                           class="btn btn-primary btn-pills single_product__order">
                            dodaj
                        </a>
                    </div>
                {/if}
            </div>
        </div>
    {/foreach}
    <div class="col-12">
        {pagination all_items=$pages_count per_page=$items_count current_page=$page}
    </div>
{/if}
