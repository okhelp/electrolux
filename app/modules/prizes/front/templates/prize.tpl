<div class="container-fluid product_more award_more">
    <div class="row">
        <div class="col-md-6 d-flex align-items-center product_more__text">
            <div class="row">
                <div class="col-12">
                    <div class="row">
                        <div class="col-12">
                            <p class="product_more__title">{$prize->name}</p>
                            <p class="product_more__code">{$prize->sub_name}</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <p class="product_more__description">
                                {$prize->description}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 align-items-center justify-content-center product_more__photo">
            <div class="row w-100">
                <div class="col">
                    <div class="product_more_slick__main" style="width: 100%">
                        {if !empty($prize_photos)}
                            {foreach $prize_photos as $prize_photo}
                                <div class="product_more_slick__main_single" style="min-height: 100%"
                                     data-src="{img id_cimg=$prize_photo->id_cimg width=800 height=600}" data-exthumbimage="{img id_cimg=$prize_photo->id_cimg width=96 height=76}">
                                    <img class="img-fluid"
                                         {if $prize_photos|@count == 1}src{else}data-lazy{/if}="{img id_cimg=$prize_photo->id_cimg width=500 height=600}">
                                </div>
                                {*<a class="product_more_slick__main_single" href="{img id_cimg=$prize_photo->id_cimg width=500 height=600}"><img class="img-fluid" style="max-width: 100%;"*}
                                                                                                                {*data-lazy="{img id_cimg=$prize_photo->id_cimg width=500 height=600}"></a>*}
                            {/foreach}
                        {/if}
                        <div class="slider-buttons">
                            <button class="prev-slide-product-more custom__arrow btn">
                                <i class="icon-prev"></i>
                            </button>
                            <button class="next-slide-product-more custom__arrow btn">
                                <i class="icon-next"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-4 w-100">
                <div class="col">
                    <div class="product_more_slick__thumbs">
                        {if !empty($prize_photos)}
                            {foreach $prize_photos as $prize_photo}
                                <div class="product_more_slick__thumbs_single"
                                     data-src="{img id_cimg=$prize_photo->id_cimg width=250 height=150}">
                                    <img class="img-fluid" style="width: auto; max-height: 125px;"
                                         data-lazy="{img id_cimg=$prize_photo->id_cimg width=250 height=150}">
                                </div>
                            {/foreach}
                        {/if}
                    </div>
                </div>
            </div>
        </div>
        {*{if $prize->cimg}*}
        {*<div class="col-md-6 align-items-center justify-content-center product_more__photo">*}
        {*<img class="img-fluid" src="{img id_cimg={$prize->cimg}}">*}
        {*</div>*}
        {*{else}*}
        {*<div class="col-md-6 align-items-center justify-content-center product_more__photo">*}
        {*<img class="img-fluid" src="{BASE_URL}_images/award_photo.jpg">*}
        {*</div>*}
        {*{/if}*}
        <div id="tab-1"
             class="col-md-6 product_more__specification product_more__specification--lightBlue award_more__order">
            <span data-target="tab-1" class="product_more__close"><i class="icon-filter_cross"></i></span>
            <ul class="award_more__order_list">
                {if !empty($prize_variants)}
                    {foreach $prize_variants as $prize_variant}
                        <li class="award_more__order_point mb-4">
                            <div class="row mb-3">
                                <div class="col-sm-8">
                                    <p class="award_more__order_point_title">{$prize_variant->title}</p>
                                </div>
                                <div class="col-sm-4 text-right">
                                    <p class="award_more__order_point_points">{$prize_variant->points} E-LUX</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-8 mb-3 mb-sm-0">
                                    <p class="award_more__order_point_details">
                                        {$prize_variant->description}
                                    </p>
                                </div>
                                <div class="col-sm-4 award_more__order_buttons d-flex flex-wrap align-content-center justify-content-sm-end justify-content-around">
                                    {if $can_order}
                                        <button data-prize="{$prize_variant->id_prize}"
                                                data-variant="{$prize_variant->id}"
                                                type="button"
                                                class="btn btn-primary btn-pills award_more__order_orderLink">
                                            dodaj
                                        </button>
                                    {/if}
                                </div>
                            </div>
                        </li>
                    {/foreach}
                {/if}
            </ul>
        </div>
        <div id="tab-2"
             class="col-md-6 product_more__specification award_more__details product_more__specification--border">
            <span data-target="tab-2" class="product_more__close"><i class="icon-filter_cross"></i></span>
            <div id="product_more__specification_list2" class="product_more__specification_list">
            {$prize->details}
            </div>
            <span class="linkMore" data-target="product_more__specification_list2">Pokaż więcej</span>
        </div>
    </div>
    <div class="row d-none d-md-flex">
        <div class="col-6 product_more__extra_links product_more__extra_links--lightBlue">
            <span data-tab="tab-1" class="product_more__tab_link"><i
                        class="icon-plus product_more__extra_links--icon product_more__extra_links--iconChange"></i>Zamów</span>
        </div>
        <div class="col-6 product_more__extra_links product_more__extra_links--blue">
            <span data-tab="tab-2" class="product_more__tab_link"><i
                        class="icon-plus product_more__extra_links--icon product_more__extra_links--iconChange"></i>Szczegóły</span>
        </div>
    </div>
</div>
<script>
    {literal}
    _paq.push(['trackEvent', 'Nagroda', 'ID Nagrody', {/literal}{$prize->id}{literal}]);
    {/literal}
</script>


