<?php
declare(strict_types=1);

namespace App\modules\prizes\front\collections;

use App\modules\prizes\ed\models\Prize_category_model;

class Get_root_categories
{
    public static function get(): array
    {
        $prize_categories = [];

        $categories = Prize_category_model::find_by_sql(<<<SQL
SELECT *
FROM prize_category pc
JOIN prize_category_lang pcl ON pcl.prize_category_id=pc.id AND pcl.id_lang={$_SESSION['id_lang']} AND pcl.id_service={$_SESSION['id_service']}
WHERE pc.parent_id IS NULL
ORDER BY pc.position ASC
SQL
        );

        if(!empty($categories))
        {
            foreach($categories as $category)
            {
                $prize_categories[$category->{Prize_category_model::COLUMN_ID}] = $category;
            }
        }

        return $prize_categories;
    }
}
