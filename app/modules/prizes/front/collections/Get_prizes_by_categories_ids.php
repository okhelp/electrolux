<?php
declare(strict_types=1);

namespace App\modules\prizes\front\collections;

use App\modules\prizes\ed\models\Get_prizes_images;
use App\modules\prizes\ed\models\Prize_model;

class Get_prizes_by_categories_ids
{
    public static function get(array $categories_ids, string $limit = '', string $order_by = 'elux_desc', bool $annual = false, ?int $page): array
    {
        $limit_condition = empty($limit) ? '' : self::parse_limit($limit, $page);

        $order_by_contidion = empty($order_by) ? '' : self::parse_order_by($order_by);

        $prizes_to_return = [];

        $annual_condition = ' AND prize.year_prize=' . (int)$annual;

        if (!empty($categories_ids))
        {
            $prizes = Prize_model::find_by_sql(
                "
                SELECT *, (SELECT MIN(prize_variant.points) from prize_variant WHERE id_prize=prize.id AND deleted_at IS NULL) AS min_price
                FROM prize
                JOIN prize_lang ON prize_lang.prize_id=prize.id
                WHERE prize.category_id IN (" . implode(',', $categories_ids) . ")
                AND prize.status=1 AND prize_lang.id_lang={$_SESSION['id_lang']} AND prize_lang.id_service={$_SESSION['id_service']}
                AND prize.deleted_at IS NULL
                $annual_condition
                $order_by_contidion
                $limit_condition
            ");

            $prizes_ids = [];
            if (!empty($prizes))
            {
                foreach ($prizes as $prize)
                {
                    $prizes_ids[] = $prize->id;
                    $prizes_to_return[$prize->{Prize_model::COLUMN_ID}] = $prize;
                }
            }

            $prizes_variants = Get_prize_variants::get_by_ids($prizes_ids);

            $prizes_photos = Get_prizes_images::get($prizes_ids);

            if (!empty($prizes_photos))
            {
                foreach ($prizes_to_return as $prize)
                {
                    $prize->assign_attribute('photos', empty($prizes_photos[$prize->id]) ? [] : $prizes_photos[$prize->id]);
                }
            }

            $min_prizes_prices = [];
            if (!empty($prizes_variants))
            {
                foreach ($prizes_variants as $id_prize => $prize_variants)
                {
                    $min_prize = 0;
                    foreach ($prize_variants as $prize_variant)
                    {
                        if (empty($min_prize))
                        {
                            $min_prize = $prize_variant->points;
                        }
                        else
                        {
                            if ($min_prize > $prize_variant->points)
                            {
                                $min_prize = $prize_variant->points;
                            }
                        }
                    }

                    $min_prizes_prices[$id_prize] = $min_prize;
                }
            }

            foreach ($prizes_to_return as $prize)
            {
                $prize->assign_attribute('price', empty($min_prizes_prices[$prize->id]) ? 0 : $min_prizes_prices[$prize->id]);
            }
        }

        return $prizes_to_return;
    }

    private static function parse_limit(string $limit, ?int $page): string
    {
        $limit_to_return = '';

        if (is_numeric($limit))
        {
            $limit_to_return = " LIMIT " . (int)$limit . " ";
            $limit_to_return .= empty($page) ? '' : (' OFFSET ' . ($limit * ($page - 1)));
        }

        return $limit_to_return;
    }

    private static function parse_order_by(string $order): string
    {
        switch ($order)
        {
            case 'popularity':
                return ' ORDER BY id ';
            case 'elux_asc':
                return ' ORDER BY min_price ASC ';
            case 'elux_desc':
                return ' ORDER BY min_price DESC ';
            default:
                return '';
        }
    }
}