<?php
declare(strict_types=1);

namespace App\modules\prizes\front\collections;

use App\modules\prizes\ed\models\Prize_category_model;

class Get_all_categories
{
    public static function get(): array
    {
        $categories_to_return = [];

        $categories = Prize_category_model::all();

        if (!empty($categories))
        {
            foreach ($categories as $category)
            {
                $categories_to_return[$category->{Prize_category_model::COLUMN_ID}] = $category;
            }
        }

        return $categories_to_return;
    }
}