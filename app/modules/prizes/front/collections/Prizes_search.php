<?php
declare(strict_types=1);

namespace App\modules\prizes\front\collections;

use App\modules\prizes\ed\models\Prize_model;

class Prizes_search
{
    public static function search(string $search_phrase, int $count = 20, string $order = 'e_lux_desc', int $page = 1): array
    {
        $prizes_to_return = [];

        $where = '';
        $order_condition = '';
        $offset = $count * ($page - 1);

        if(!empty($search_phrase))
        {
            $where = "
                AND (MATCH(prize_lang.name) AGAINST ('{$search_phrase}*' IN BOOLEAN MODE)
                  OR MATCH(sub_name) AGAINST ('{$search_phrase}*' IN BOOLEAN MODE)
                  OR MATCH(details) AGAINST ('{$search_phrase}*' IN BOOLEAN MODE)
                  OR MATCH(description) AGAINST ('{$search_phrase}*' IN BOOLEAN MODE))
            ";
        }

        $order_condition = $order == 'e_lux_desc' ? ' price DESC ' : ' price ASC ';

        $prizes = Prize_model::find_by_sql("
            SELECT *,
            (SELECT id_cimg FROM prize_photos WHERE prize_photos.id_prize=prize.id LIMIT 1) AS cimg,
            CONCAT('" . (BASE_URL . 'prizes/prize.html?id=') . "', prize.id) AS url,
            (SELECT MIN(prize_variant.points) from prize_variant WHERE id_prize=prize.id AND deleted_at IS NULL) AS price
            FROM prize
            JOIN prize_lang ON prize_lang.prize_id=prize.id
            WHERE prize_lang.id_service={$_SESSION['id_service']} AND prize_lang.id_lang={$_SESSION['id_lang']}
            $where
            ORDER BY $order_condition
            LIMIT $count OFFSET $offset
        ");

        if(!empty($prizes))
        {
            foreach ($prizes as $prize)
            {
                $prizes_to_return[$prize->{Prize_model::COLUMN_ID}] = $prize;
            }
        }

        return $prizes_to_return;
    }
}