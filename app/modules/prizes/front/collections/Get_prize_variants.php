<?php
declare(strict_types=1);

namespace App\modules\prizes\front\collections;

use App\modules\prizes\ed\models\Prize_variant_model;

class Get_prize_variants
{
    public static function get_by_ids(array $prizes_ids): array
    {
        $variants_by_prize_id = [];

        if (!empty($prizes_ids))
        {
            $variants = Prize_variant_model::find_by_sql(
                "
                SELECT * 
                FROM prize_variant
                JOIN prize_variant_lang ON prize_variant.id=prize_variant_lang.prize_variant_id
                WHERE prize_variant.id_prize IN (" . implode(',', $prizes_ids) . ")
                    AND prize_variant.deleted_at IS NULL
            ");

            foreach ($variants as $variant)
            {
                $variants_by_prize_id[$variant->{Prize_variant_model::COLUMN_ID_PRIZE}][$variant->id] = $variant;
            }
        }

        return $variants_by_prize_id;
    }

    public static function get_by_id(int $id_prize): array
    {
        $variants_to_return = [];

        $variants = self::get_by_ids([$id_prize]);

        if (!empty($variants[$id_prize]))
        {
            $variants_to_return = $variants[$id_prize];
        }

        return $variants_to_return;
    }
}