<?php
declare(strict_types=1);

namespace App\modules\prizes\front\collections;

use App\modules\prizes\ed\models\Prize_category_model;

class Get_direct_subcategories
{
    public static function get(array $categories_ids): array
    {
        $subcategories_to_return = [];

        if (!empty($categories_ids))
        {
            $subcategories = Prize_category_model::find_by_sql(
                "
                SELECT *
                FROM prize_category
                JOIN prize_category_lang ON prize_category_lang.prize_category_id=prize_category.id
                WHERE prize_category_lang.id_lang = " . $_SESSION['id_lang'] . "
                    AND prize_category.parent_id IN (" . implode(',', $categories_ids) . ")
                    AND prize_category_lang.id_service = " . $_SESSION['id_service'] . "
            ");

            if (!empty($subcategories))
            {
                foreach ($subcategories as $subcategory)
                {
                    $subcategories_to_return[$subcategory->{Prize_category_model::COLUMN_ID}] = $subcategory;
                }
            }
        }

        return $subcategories_to_return;
    }
}