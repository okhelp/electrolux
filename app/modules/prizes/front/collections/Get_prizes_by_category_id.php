<?php
declare(strict_types=1);

namespace App\modules\prizes\front\collections;

use App\modules\prizes\ed\models\Get_prizes_images;
use App\modules\prizes\ed\models\Prize_category_model;
use App\modules\prizes\ed\models\Prize_model;
use App\modules\prizes\ed\objects\Prizes_categories_tree;

class Get_prizes_by_category_id
{
    public static function get(array $categories_ids, bool $annual, ?int $prizes_limit = null): array
    {
        $prizes_by_category_id = [];

        $all_categories_ids = [];

        $subcategories = Prizes_categories_tree::get_list(0, 1, -1, 'without_pages', 0);

        foreach ($categories_ids as $category_id)
        {
            if (!empty($subcategories))
            {
                $subcategories_list = generate_tree($subcategories, $category_id);

                $subcategories_ids = array_map(
                    function($subcategory) {
                        return $subcategory[Prize_category_model::COLUMN_ID];
                    }, $subcategories_list);

                if (empty($subcategories_ids))
                {
                    $all_categories_ids = array_merge($all_categories_ids, [$category_id]);
                }
                else
                {
                    $all_categories_ids = array_merge($all_categories_ids, $subcategories_ids);
                    $all_categories_ids = array_merge($all_categories_ids, [$category_id]);
                }
            }
            else
            {
                $all_categories_ids = array_merge($all_categories_ids, [$category_id]);
            }
        }

        $annual_condition = ' AND year_prize=' . (int)$annual;

        $prizes = Prize_model::find_by_sql(
            "
                SELECT *
                FROM prize
                JOIN prize_lang ON prize.id=prize_lang.prize_id
                WHERE prize.status=1 AND prize_lang.id_lang={$_SESSION['id_lang']} AND prize_lang.id_service={$_SESSION['id_service']}
                    AND prize.category_id IN (" . implode(',', $all_categories_ids) . ")
                    AND prize.deleted_at IS NULL
                    $annual_condition
            ");

        if (!empty($prizes))
        {
            $prizes_ids = [];
            foreach ($prizes as $prize)
            {
                $prize_root_category = self::get_price_root_category($prize);

                if (!empty($prizes_limit) && !empty($prizes_by_category_id[$prize_root_category]))
                {
                    $prizes_count = count($prizes_by_category_id[$prize_root_category]);

                    if($prizes_count >= $prizes_limit)
                    {
                        continue;
                    }
                }

                $prizes_ids[] = $prize->id;
                $prizes_by_category_id[$prize_root_category][] = $prize;
            }

            $prizes_variants = Get_prize_variants::get_by_ids($prizes_ids);

            $min_prizes_prices = [];
            if (!empty($prizes_variants))
            {
                foreach ($prizes_variants as $id_prize => $prize_variants)
                {
                    $min_prize = 0;
                    foreach ($prize_variants as $prize_variant)
                    {
                        if (empty($min_prize))
                        {
                            $min_prize = $prize_variant->points;
                        }
                        else if ($min_prize > $prize_variant->points)
                        {
                            $min_prize = $prize_variant->points;
                        }
                    }

                    $min_prizes_prices[$id_prize] = $min_prize;
                }
            }

            $prizes_photos = Get_prizes_images::get($prizes_ids);

            if(!empty($prizes_photos))
            {
                foreach ($prizes_by_category_id as $prizes)
                {
                    foreach ($prizes as $prize)
                    {
                        $prize->assign_attribute('photos', empty($prizes_photos[$prize->id]) ? [] : $prizes_photos[$prize->id]);
                    }
                }
            }


                foreach ($prizes_by_category_id as $prizes)
                {
                    foreach ($prizes as $prize)
                    {
                        $prize->assign_attribute('price', empty($min_prizes_prices[$prize->id]) ? 0 : $min_prizes_prices[$prize->id]);
                    }
                }
        }

        return $prizes_by_category_id;
    }

    private static function get_price_root_category(Prize_model $prize)
    {
        $all_categories = Get_all_categories::get();

        if (empty($all_categories[$prize->{Prize_model::COLUMN_CATEGORY_ID}]->{Prize_category_model::COLUMN_PARENT_ID}))
        {
            return $all_categories[$prize->{Prize_model::COLUMN_CATEGORY_ID}]->{Prize_category_model::COLUMN_ID};
        }
        else
        {
            return self::search_for_root_category($all_categories, (int)$all_categories[$prize->{Prize_model::COLUMN_CATEGORY_ID}]->{Prize_category_model::COLUMN_ID});
        }
    }

    private static function search_for_root_category(array $categories, int $category_id)
    {
        if (empty($categories[$category_id]->{Prize_category_model::COLUMN_PARENT_ID}))
        {
            return $categories[$category_id]->{Prize_category_model::COLUMN_ID};
        }
        else
        {
            return self::search_for_root_category($categories, (int)$categories[$category_id]->{Prize_category_model::COLUMN_PARENT_ID});
        }
    }
}
