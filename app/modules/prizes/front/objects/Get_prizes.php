<?php
declare(strict_types=1);

namespace App\modules\prizes\front\objects;

use App\modules\prizes\ed\models\Get_prizes_images;
use App\modules\prizes\ed\models\Prize_category_model;
use App\modules\prizes\ed\models\Prize_model;
use App\modules\prizes\front\collections\Get_prizes_by_category_id;
use App\modules\prizes\front\collections\Get_root_categories;

class Get_prizes
{
    public static function by_categories(bool $annual, ?int $limit = null): array
    {
        $categories_with_prizes = [];

        $prizes_categories = Get_root_categories::get();

        if (!empty($prizes_categories))
        {
            $categories_ids = array_keys($prizes_categories);

            $prizes = Get_prizes_by_category_id::get($categories_ids, $annual, $limit);

            if (!empty($prizes))
            {
                $prizes_by_category = [];

                foreach ($prizes as $category_id => $prize)
                {
                    $prizes_by_category[$category_id] = $prize;
                }

                foreach ($prizes_categories as $prizes_category)
                {
                    if (!empty($prizes_by_category[$prizes_category->{Prize_category_model::COLUMN_ID}]))
                    {
                        $categories_with_prizes[$prizes_category->{Prize_category_model::COLUMN_ID}] = $prizes_category;
                        $categories_with_prizes[$prizes_category->{Prize_category_model::COLUMN_ID}]->assign_attribute(
                            'prizes',
                            $prizes_by_category[$prizes_category->{Prize_category_model::COLUMN_ID}]
                        );
                    }
                }
            }
        }

        return $categories_with_prizes;
    }

    public static function by_ids(array $prizes_ids): array
    {
        $prizes_to_return = [];

        if(!empty($prizes_ids))
        {
            $prizes = Prize_model::find_by_sql("
                SELECT *
                FROM prize
                JOIN prize_lang ON prize_lang.prize_id=prize.id
                WHERE prize.id IN (" . implode(',', $prizes_ids) . ")
            ");

            if(!empty($prizes))
            {
                $prizes_ids = array_map(function($prize){ return $prize->id; }, $prizes);

                $prizes_photos = Get_prizes_images::get($prizes_ids);

                foreach ($prizes as $prize)
                {
                    if(!empty($prizes_photos[$prize->id]))
                    {
                        $prize->assign_attribute('photos', $prizes_photos[$prize->id]);
                    }

                    $prizes_to_return[$prize->id] = $prize;
                }
            }
        }

        return $prizes_to_return;
    }
}
