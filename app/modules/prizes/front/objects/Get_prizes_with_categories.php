<?php
declare(strict_types=1);

namespace App\modules\prizes\front\objects;

use App\modules\prizes\front\collections\Get_direct_subcategories;
use App\modules\prizes\front\collections\Get_prizes_by_categories_ids;

class Get_prizes_with_categories
{
    public static function get(array $categories_ids, string $limit = '', string $order_by = 'elux_desc', bool $annual = false, ?int $page = null)
    {
        $prizes = Get_prizes_by_categories_ids::get($categories_ids, $limit, $order_by, $annual, $page);

        $direct_subcategories = Get_direct_subcategories::get($categories_ids);

        return [
            'prizes'        => $prizes,
            'subcategories' => $direct_subcategories,
        ];
    }
}