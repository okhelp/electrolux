<?php

use App\ed\model\users\Verify_user_confirmation;
use App\modules\prizes\ed\models\Get_prizes_images;
use App\modules\prizes\ed\models\Prize_category_model;
use App\modules\prizes\ed\models\Prize_model;
use App\modules\prizes\ed\objects\Prizes_categories_tree;
use App\modules\prizes\front\collections\Get_prize_variants;
use App\modules\prizes\front\objects\Get_prizes;
use App\modules\prizes\front\objects\Get_prizes_with_categories;

class App__Modules__Prizes__Front__Controller__Index extends Lib__Base__Ed_Controller
{
    public function action_index()
    {
        $annual = isset($_GET['annual']) && App__Ed__Model__Acl::has_group('premium-user', 0);

        $prizes_by_categories = Get_prizes::by_categories($annual, 6);

        $annualPoints = \App\ed\model\points\Get_users_points::get_annual([Lib__Session::get('id')]);
        $annualPoints = reset($annualPoints);
        $this->view->assign('categories', $prizes_by_categories);
        $this->view->assign('annual', $annualPoints);
        $this->template = get_module_template_path('prizes') . 'prizes.tpl';
        $this->page_title = 'Nagrody';
        $this->breadcrumb = [
            BASE_URL . 'prizes' . ($annual ? '?annual' : '') => 'Nagrody',
        ];
    }

    public function action_prize()
    {
        if (!empty($_GET['id']))
        {
            $prize = Prize_model::find($_GET['id']);

            $this->view->assign('prize', $prize);

            $this->view->assign('prize_variants', Get_prize_variants::get_by_id((int)$_GET['id']));

            $prize_photos = Get_prizes_images::get([(int)$_GET['id']]);

            $this->view->assign('prize_photos', empty($prize_photos[$_GET['id']]) ? [] : $prize_photos[$_GET['id']]);

            $is_user_confirmed = empty($this->session['is_company_admin']) ? Verify_user_confirmation::verify((int)$this->session['id']) : true;

            $this->view->assign('can_order', App__Ed__Model__Acl::has_access('cart', 0) && $is_user_confirmed);

            $category_data = Prize_category_model::find($prize->category_id);

            $this->template = get_module_template_path('prizes') . 'prize.tpl';
            $this->page_title = "Nagroda";
            $this->breadcrumb = [
                BASE_URL . 'prizes' . (!empty($prize->year_prize) ? '?annual' : '')                                        => 'Nagrody',
                BASE_URL . 'prizes/category.html?id=' . $category_data->id . (!empty($prize->year_prize) ? '&annual' : '') => $category_data->name,
                BASE_URL . 'prizes/prize.html?id=' . $_GET['id']                                                           => $prize->name,
            ];
        }
        else
        {
            go_back("d|Nie odnaleziono produktu");
        }
    }

    public function action_category()
    {
        if(!empty($_GET['id']))
        {
            $annual = isset($_GET['annual']) && App__Ed__Model__Acl::has_group('premium-user', 0);

            $categories_ids = [$_GET['id']];

            $subcategories = Prizes_categories_tree::get_list($_GET['id'], 1, -1, 'without_pages', 0);

            if (!empty($subcategories))
            {
                $subcategories = generate_tree($subcategories, $_GET['id']);

                $subcategories_ids = array_map(
                    function($subcategory) {
                        return $subcategory[Prize_category_model::COLUMN_ID];
                    }, $subcategories);

                $categories_ids = array_merge($categories_ids, $subcategories_ids);
            }

            $data = Get_prizes_with_categories::get($categories_ids, 15, 'elux_desc', (bool)isset($_GET['annual']));

            $category_data = Prize_category_model::find($_GET['id']);

            if (empty($category_data))
            {
                go_back('d|Nie odnaleziono kategorii', BASE_URL);
            }
            else
            {
                $this->view->assign('prizes', $data['prizes']);
                $this->view->assign('category_id', $_GET['id']);
                $this->view->assign('subcategories', $data['subcategories']);
                $this->view->assign('page', 1);
                $all_category_details = Get_prizes_with_categories::get($categories_ids, 'all', 'elux_desc', (bool)isset($_GET['annual']));
                $this->view->assign('pages_count', count($all_category_details['prizes']));
                $this->view->assign('items_count', 15);
                $this->template = get_module_template_path('prizes') . 'category.tpl';
                $this->page_title = "Nagroda";
                $this->breadcrumb = [
                    BASE_URL . 'prizes' . ($annual ? '?annual' : '')                                 => 'Nagrody',
                    BASE_URL . 'prizes/category.html?id=' . $_GET['id'] . ($annual ? '&annual' : '') => empty($category_data->name) ? '' : $category_data->name,
                ];
            }
        }
        else
        {
            go_back('d|Nie odnaleziono kategorii', BASE_URL);
        }
    }

    public function action_change_filter()
    {
        if(!empty($_POST))
        {
            $categories_ids = empty($_POST['subcategory_id']) ? [$_POST['category_id']] : $_POST['subcategory_id'];
            $limit = !empty($_POST['products_count_select']) && is_numeric($_POST['products_count_select']) ? (int)$_POST['products_count_select'] : 'all';
            $order = $_POST['products_sort_select'];
            $page = empty($_POST['page']) ? 1 : (int)$_POST['page'];

            $all_categories_ids = [];

            $subcategories = Prizes_categories_tree::get_list(0, 1, -1, 'without_pages', 0);

            foreach($categories_ids as $category_id)
            {

                if(!empty($subcategories))
                {
                    $subcategories_list = generate_tree($subcategories, $category_id);

                    $subcategories_ids = array_map(function($subcategory){ return $subcategory[Prize_category_model::COLUMN_ID]; }, $subcategories_list);

                    if(empty($subcategories_ids))
                    {
                        $all_categories_ids = array_merge($all_categories_ids, [$category_id]);
                    }
                    else
                    {
                        $all_categories_ids = array_merge($all_categories_ids, $subcategories_ids);
                    }
                }
                else
                {
                    $all_categories_ids = array_merge($all_categories_ids, [$category_id]);
                }
            }


            $data = Get_prizes_with_categories::get($all_categories_ids, $limit, $order, (bool)isset($_GET['annual']), $page);

            $smarty = new Smarty();
            $smarty->assign('page', $page);
            $all_category_details = Get_prizes_with_categories::get($all_categories_ids, 'all', $order, (bool)isset($_GET['annual']), $page);
            $smarty->assign('pages_count', count($all_category_details['prizes']));
            $smarty->assign('items_count', is_numeric($limit) ? $limit : 999999999);
            $smarty->assign('prizes', $data['prizes']);

            echo $smarty->fetch(get_module_template_path('prizes') . '_partials/prizes_listing.tpl');
        }
    }
}