<?php

class App__Modules__Prizes__Front__Controller__Router
{
	public static function set()
	{
		$router = [];

		$router[] = array(
			'uri' => str_replace(BASE_DIR,'',$_SERVER['REQUEST_URI']),
			'controller' => 'App__Modules__Prizes__Front__Controller__Index',
			'method' => 'action_index'
		);
		
		return $router;
	}
}