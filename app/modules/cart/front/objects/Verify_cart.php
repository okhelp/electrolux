<?php
declare(strict_types=1);

namespace App\modules\cart\front\objects;

use App\front\models\user\User_points;
use App\modules\cart\front\collections\Get_cart;
use Exception;

class Verify_cart
{
    /** @var int */
    private $user_id;

    /** @var array */
    private $cart;

    /**
     * Verify_cart constructor.
     * @param int $user_id
     */
    public function __construct(int $user_id)
    {
        $this->user_id = $user_id;
    }

    /**
     * @throws Exception
     */
    public function verify()
    {
        $this->get_user_cart();

        $this->is_cart_not_empty();

        $this->has_enough_money();
    }

    private function get_user_cart()
    {
        $this->cart = Get_cart::get($this->user_id);
    }

    private function is_cart_not_empty()
    {
        if (empty($this->cart))
        {
            throw new Exception('Twój koszyk jest pusty.');
        }
    }

    private function has_enough_money()
    {
        if (!empty($this->cart))
        {
            $cart_sum = Cart::sum_cart_products_standard($this->cart);
            $cart_sum_annual = Cart::sum_cart_products_annual($this->cart);

            $user_points = \App__Ed__Model__Points_log::getPoints($this->user_id);
            $user_points_annual = \App\ed\model\points\Get_users_points::get_annual([$this->user_id]);
            $user_points_annual = reset($user_points_annual);

            if ((empty($user_points) && empty($user_points_annual)) || (!empty($user_points) && $user_points < $cart_sum) || (!empty($user_points_annual) && $user_points_annual < $cart_sum_annual))
            {
                throw new Exception('Nie masz wystarczającej ilości punktów.');
            }
        }
    }
}