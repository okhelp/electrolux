<?php
declare(strict_types=1);

namespace App\modules\cart\front\objects;

use App\modules\cart\front\models\Cart_model;
use App\modules\prizes\ed\models\Prize_variant_model;

class Add_product_to_cart
{
    /** @var int */
    private $id_prize;

    /** @var int */
    private $id_variant;

    /** @var int */
    private $id_user;

    /** @var Prize_variant_model */
    private $prize_variant;

    /**
     * @param int $id_prize
     * @param int $id_variant
     * @param int $id_user
     * @throws \ActiveRecord\RecordNotFound
     */
    public function __construct(int $id_prize, int $id_variant, int $id_user)
    {
        $this->id_prize = $id_prize;
        $this->id_variant = $id_variant;
        $this->id_user = $id_user;
        $this->prize_variant = Prize_variant_model::find($id_variant);
    }


    public function add(): bool
    {
        $cart_log = new Cart_log($this->id_user);

        if (!empty($this->prize_variant) && $this->prize_variant->{Prize_variant_model::COLUMN_ID_PRIZE} == $this->id_prize)
        {
            $product_exists_in_cart = self::product_exist_in_cart($cart_log->cart_before);

            if (!empty($product_exists_in_cart))
            {
                $added = self::update_product_count_in_cart($product_exists_in_cart);
            }
            else
            {
                $added = self::add_new_product_to_cart();
            }

            $cart_log->add(Cart::ACTION_ADD);

            return $added;
        }

        return false;
    }

    private function product_exist_in_cart(array $cart): array
    {
        $cart_element_to_return = [];

        if (!empty($cart))
        {
            foreach ($cart as $cart_element)
            {
                if ($cart_element['id_prize'] == $this->id_prize && $cart_element['id_prize_option'] == $this->id_variant)
                {
                    $cart_element_to_return = $cart_element;
                }
            }
        }

        return $cart_element_to_return;
    }

    private function add_new_product_to_cart(): bool
    {
        $cart_model = new Cart_model();
        $cart_model->{Cart_model::COLUMN_ID_PRIZE} = $this->id_prize;
        $cart_model->{Cart_model::COLUMN_ID_PRIZE_OPTION} = $this->id_variant;
        $cart_model->{Cart_model::COLUMN_ID_USER} = $this->id_user;
        $cart_model->{Cart_model::COLUMN_PRICE} = $this->prize_variant->{Prize_variant_model::COLUMN_POINTS};
        $cart_model->{Cart_model::COLUMN_COUNT} = 1;

        return $cart_model->save();
    }

    private function update_product_count_in_cart(array $cart_element): bool
    {
        $cart_model = Cart_model::find($cart_element['id']);
        $cart_model->{Cart_model::COLUMN_COUNT} = (int)$cart_model->{Cart_model::COLUMN_COUNT} + 1;

        return $cart_model->save();
    }
}