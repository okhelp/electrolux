<?php
declare(strict_types=1);

namespace App\modules\cart\front\objects;

use App\modules\order\ed\models\Order_address_model;
use App\modules\order\ed\models\Order_model;
use App\modules\order\front\collections\Get_not_submitted_order;
use App__Ed__Model__Company;

class Get_order_addresses
{
    public static function get(int $id_user, array $company_addresses, App__Ed__Model__Company $company)
    {
        $addresses_to_return = [];

        $order = Get_not_submitted_order::get($id_user);

        if(!empty($order))
        {
            $order_address = Order_address_model::find($order->{Order_model::COLUMN_ID_ADDRESS});
        }

        if (!empty($company_addresses))
        {
            if (!empty($order_address))
            {
                foreach ($company_addresses as $company_address)
                {
                    $company_address['is_main'] = 0;
                    $addresses_to_return[$company_address['id']] = $company_address;
                }
            }
            else
            {
                $addresses_to_return = $company_addresses;
            }
        }

        if(!empty($order_address))
        {
            $addresses_to_return = self::assign_order_address($order_address, $addresses_to_return, $company);
        }

        return $addresses_to_return;
    }

    private static function assign_order_address(Order_address_model $order_address, array $addresses, App__Ed__Model__Company $company)
    {
        $addresses['c' . $order_address->id] = [
                'id' => $order_address->id,
                'id_company' => $company->id,
                'is_main' => 1,
                'name' => 'Adres zamówienia',
                'street' => $order_address->street,
                'street_number' => $order_address->street_number,
                'apartment_number' => $order_address->apartment_number,
                'post_code' => $order_address->post_code,
                'city' => $order_address->city,
                'recipent_name' => $order_address->recipent_name,
                'recipent_surname' => $order_address->recipent_surname,
                'recipent_company' => $order_address->recipent_company,
                'telephone' => $order_address->telephone,
                'email' => $order_address->email,
        ];

        return $addresses;
    }
}