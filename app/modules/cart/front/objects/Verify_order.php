<?php
declare(strict_types=1);

namespace App\modules\cart\front\objects;

class Verify_order
{
    private $order_data;

    private $id_user;

    /**
     * @param array $order_data
     * @param int $id_user
     */
    public function __construct(array $order_data, int $id_user)
    {
        $this->order_data = $order_data;
        $this->id_user = $id_user;
    }

    public function verify()
    {
        $this->check_points_amount();
    }

    private function check_points_amount()
    {
        //TODO jeżeli nie wystarczy punktów throw exception
    }


}