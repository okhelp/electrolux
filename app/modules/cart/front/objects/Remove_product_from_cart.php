<?php
declare(strict_types=1);

namespace App\modules\cart\front\objects;

use App\modules\cart\front\models\Cart_model;

class Remove_product_from_cart
{
    /** @var int */
    private $id_cart_element;

    /** @var int */
    private $id_user;

    /**
     * Remove_product_from_cart constructor.
     * @param int $id_user
     * @param int $id_cart_element
     */
    public function __construct(int $id_user, int $id_cart_element)
    {
        $this->id_cart_element = $id_cart_element;
        $this->id_user = $id_user;
    }


    public function remove(): bool
    {
        $cart_log = new Cart_log($this->id_user);

        $cart_element = Cart_model::find($this->id_cart_element);

        if (!empty($cart_element) && $cart_element->{Cart_model::COLUMN_ID_USER} == $this->id_user)
        {
            $product_exists_in_cart = self::product_exist_in_cart($cart_log->cart_before, $cart_element);

            $removed = false;
            if (!empty($product_exists_in_cart))
            {
                $removed = $cart_element->delete();
            }

            $cart_log->add(Cart::ACTION_REMOVE);

            return $removed;
        }

        return false;
    }


    private function product_exist_in_cart(array $cart, $selected_cart_element): array
    {
        $cart_element_to_return = [];

        if (!empty($cart))
        {
            foreach ($cart as $cart_element)
            {
                if ($cart_element['id_prize'] == $selected_cart_element->id_prize)
                {
                    $cart_element_to_return = $cart_element;
                }
            }
        }

        return $cart_element_to_return;
    }
}