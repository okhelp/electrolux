<?php
declare(strict_types=1);

namespace App\modules\cart\front\objects;

use App\modules\cart\front\models\Cart_model;
use App\modules\prizes\ed\models\Prize_model;

class Cart
{
    const ACTION_ADD          = 'add';
    const ACTION_CHANGE_COUNT = 'count_change';
    const ACTION_REMOVE       = 'remove';
    const ACTION_SUB          = 'sub';
    const ACTION_CART_ORDERED = 'ordered';

    public static function add_to_cart(int $id_prize, int $id_variant, int $id_user): bool
    {
        $add_to_cart = new Add_product_to_cart($id_prize, $id_variant, $id_user);

        return $add_to_cart->add();
    }

    public static function remove_from_cart(int $id_user, int $id_cart_element): bool
    {
        $remove_from_cart = new Remove_product_from_cart($id_user, $id_cart_element);

        return $remove_from_cart->remove();
    }

    public static function change_product_count(int $id_user, int $id_cart_element, int $count): bool
    {
        $change_product_count_in_cart = new Change_product_count_in_cart($id_user, $id_cart_element, $count);

        return $change_product_count_in_cart->change();
    }

    public static function sum_cart_products_standard(array $cart): int
    {
        return self::sum_cart_products($cart, 0);
    }

    public static function sum_cart_products_annual(array $cart): int
    {
        return self::sum_cart_products($cart, 1);
    }

    public static function sum_cart_products(array $cart, int $annual): int
    {
        $cart_sum = 0;

        if (!empty($cart))
        {
            foreach ($cart as $cart_item)
            {
                if (Prize_model::find($cart_item->id_prize)->year_prize == $annual) {
                    $cart_sum += $cart_item->{Cart_model::COLUMN_PRICE} * $cart_item->{Cart_model::COLUMN_COUNT};
                }
            }
        }

        return $cart_sum;
    }

    public static function remove_products_form_cart_after_order(int $id_user)
    {
        $cart_log = new Cart_log($id_user);

        Cart_model::delete_all(['conditions' => ['id_user = ?', $id_user]]);

        $cart_log->add(Cart::ACTION_CART_ORDERED);
    }
}