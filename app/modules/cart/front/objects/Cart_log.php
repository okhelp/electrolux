<?php
declare(strict_types=1);

namespace App\modules\cart\front\objects;

use App\modules\cart\front\collections\Get_cart;
use App\modules\cart\front\models\Cart_log_model;

class Cart_log
{
    /** @var array */
    public $cart_before;

    /** @var array */
    private $cart_after;

    /** @var int */
    private $id_user;

    /**
     * @param int $id_user
     */
    public function __construct(int $id_user)
    {
        $this->id_user = $id_user;

        $this->cart_before = $this->get_cart();
    }

    public function add(string $action): bool
    {
        $this->cart_after = $this->get_cart();

        $cart_log_model = new Cart_log_model();
        $cart_log_model->{Cart_log_model::COLUMN_ID_USER} = $this->id_user;
        $cart_log_model->{Cart_log_model::COLUMN_ACTION} = $action;
        $cart_log_model->{Cart_log_model::COLUMN_CART_BEFORE} = json_encode($this->cart_before);
        $cart_log_model->{Cart_log_model::COLUMN_CART_AFTER} = json_encode($this->cart_after);

        return $cart_log_model->save();
    }

    private function get_cart()
    {
        $cart_as_array = [];

        $cart = Get_cart::get($this->id_user);

        if(!empty($cart))
        {
            foreach($cart as $cart_element)
            {
                $cart_as_array[] = $cart_element->to_array();
            }
        }

        return $cart_as_array;
    }
}