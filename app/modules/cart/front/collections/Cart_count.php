<?php
declare(strict_types=1);

namespace App\modules\cart\front\collections;

use App\modules\cart\front\models\Cart_model;

class Cart_count
{
    public static function get(int $id_user): int
    {
        $count = 0;

        $cart = Cart_model::find_by_sql(
            "
            SELECT *
            FROM cart
            WHERE id_user=$id_user
        ");

        if (!empty($cart))
        {
            foreach ($cart as $cart_item)
            {
                $count += (int)$cart_item->{Cart_model::COLUMN_COUNT};
            }
        }

        return $count;
    }
}