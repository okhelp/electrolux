<?php
declare(strict_types=1);

namespace App\modules\cart\front\collections;

use App\modules\cart\front\models\Cart_model;

class Get_cart
{
    public static function get(int $id_user): array
    {
        $cart_elements = [];

        $cart = Cart_model::find_by_sql(
            "
            SELECT *
            FROM cart
            WHERE id_user = $id_user
        ");

        if (!empty($cart))
        {
            $cart_elements = $cart;
        }

        return $cart_elements;

    }
}