<div class="container-fluid page_content cart mb-5">
    <div class="row">
        <div class="col-auto">
            <p class="cart_title">Twój Koszyk</p>
        </div>
        <div class="col-12 col-lg mt-md-0 mt-3 d-flex flex-wrap justify-content-md-end align-items-center">
            <div class="cart_steps">
                <a href="{BASE_URL}cart" class="cart_step cart_steps--orange">
                    zawartość koszyka
                </a>
            </div>
            <div class="cart_steps">
                <a href="{BASE_URL}cart/delivery.html" class="cart_step  cart_steps--backgroundMainBlue">
                    dostawa
                </a>
            </div>
            <div class="cart_steps">
                <a href="{BASE_URL}cart/summary.html" class="cart_step cart_steps--backgroundBlue">
                    podsumowanie
                </a>
            </div>
            <div class="cart_steps">
                <a href="{BASE_URL}cart/end.html" class="cart_step cart_steps--backgroundLightBlue">
                    koniec
                </a>
            </div>
        </div>
    </div>
    <div class="row mt-4">
        <div class="cart-products-listing" style="width: 100%;">
            {if !empty($cart)}
                {include file={get_module_template_path('cart')|cat:"cart_products_listing.tpl"}}
            {else}
                <div class="alert alert-info">
                    Brak produktów w koszyku.
                </div>
            {/if}
        </div>
    </div>
</div>
