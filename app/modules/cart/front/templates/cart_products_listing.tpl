{if !empty($cart)}
    <div class="col">
    <table class="table table-sm cart_table">
        <thead>
        <tr>
            <th scope="col">Nagroda</th>
            <th scope="col">Cena</th>
            <th scope="col">Ilość</th>
            <th scope="col">Razem</th>
            <th scope="col">Usuń</th>
        </tr>
        </thead>
        <tbody>
        {foreach $cart as $cart_item}
            {if !empty($prizes[$cart_item->id_prize])}
                <tr class="cart_table_row">
                    <td class="cart_product_prize_info" scope="row">
                        <div>
                            {if !empty($prizes[$cart_item->id_prize]->photos[0]->id_cimg)}
                                <img class="img-fluid"
                                     src="{img id_cimg={$prizes[$cart_item->id_prize]->photos[0]->id_cimg} height=250 width=250}">
                                {else}
                                <img class="img-fluid"
                                     src="{img id_cimg=0 height=250 width=250}">
                            {/if}
                        </div>
                        <div>
                            <p class="cart_product_title">{$prizes[$cart_item->id_prize]->name}</p>
                            <p class="cart_product_description">
                                {$prizes_variants[$cart_item->id_prize][$cart_item->id_prize_option]->description}
                            </p>
                        </div>
                    </td>
                    <td class="cart_product_price">
                        <span class="cart_table_label">Cena:</span>
                        <p class="cart_product_info_text">{$cart_item->price} <span class="text-nowrap">
                                {if $prizes[$cart_item->id_prize]->year_prize == 1}Roczne E-LUXY{else}E-LUX{/if}
                            </span>
                        </p>
                    </td>
                    <td class="cart_product_quantity">
                        <span class="cart_table_label">Ilość:</span>
                        <div class="input-group">
                                        <span class="input-group-btn">
                                            <button type="button" class="btn btn-default btn-number btn_change_quantity"
                                                    data-type="minus"
                                                    data-field="quant[{$cart_item->id}]">
                                                <i class="icon-minus"></i>
                                            </button>
                                        </span>
                            <input type="number" name="quant[{$cart_item->id}]"
                                   class="form-control input-number input_product_quantity"
                                   value="{$cart_item->count}"
                                   min="1"
                                   data-cart-element="{$cart_item->id}">
                            <span class="input-group-btn">
                                            <button type="button" class="btn btn-default btn-number btn_change_quantity"
                                                    data-type="plus"
                                                    data-field="quant[{$cart_item->id}]">
                                                <i class="icon-plus"></i>
                                            </button>
                                        </span>
                        </div>
                    </td>
                    <td class="cart_product_price_total">
                        <span class="cart_table_label">Razem:</span>
                        <p class="cart_product_info_text">{math equation="x * y" x=$cart_item->price y=$cart_item->count}
                            <span class="text-nowrap">{if $prizes[$cart_item->id_prize]->year_prize == 1}Roczne E-LUXY{else}E-LUX{/if}</span>
                        </p>
                    </td>
                    <td class="cart_product_delete">
                        <span class="cart_table_label">Usuń:</span>
                        <span class="delete_link" data-id-cart-element="{$cart_item->id}">
                                        <i class="icon-filter_cross"></i>
                                    </span>
                    </td>
                </tr>
            {/if}
        {/foreach}
        <tr class="next_step_tr">
            <td class="d-none d-lg-table-cell" colspan="3"></td>
            <td>
                {if $cart_sum_annual == 0 || $cart_sum > 0}
                    <p class="cart_product_sum_points"><span class="cart_table_label">Razem: </span>{$cart_sum} <span class="text-nowrap">E-LUX</span></p>
                {/if}
                {if $cart_sum_annual > 0}
                    <p class="cart_product_sum_points"><span class="cart_table_label">Razem: </span>{$cart_sum_annual} <span class="text-nowrap">Roczne E-LUXY</span></p>
                {/if}
            </td>
            <td>
                <a href="{BASE_URL}cart/delivery.html" class="btn btn-outline-primary btn-pills cart_product_next_step">
                    dalej
                </a>
            </td>
        </tr>
        </tbody>
    </table>
    </div>
{/if}