<div class="container-fluid page_content cart mb-5">
    <div class="row">
        <div class="col-auto">
            <p class="cart_title">Twój Koszyk</p>
        </div>
        <div class="col-12 col-lg mt-md-0 mt-3 d-flex flex-wrap justify-content-md-end align-items-center">
            <div class="cart_steps">
                <a href="{BASE_URL}cart" class="cart_step cart_steps--orange">
                    zawartość koszyka
                </a>
            </div>
            <div class="cart_steps">
                <a href="{BASE_URL}cart/delivery.html" class="cart_step  cart_steps--orange">
                    dostawa
                </a>
            </div>
            <div class="cart_steps">
                <a href="{BASE_URL}cart/summary.html" class="cart_step cart_steps--orange">
                    podsumowanie
                </a>
            </div>
            <div class="cart_steps">
                <a href="{BASE_URL}cart/end.html" class="cart_step cart_steps--backgroundLightBlue">
                    koniec
                </a>
            </div>
        </div>
    </div>
    <div class="mt-4 cart_summary">
        <div class="row">
            <div class="col-12">
                <p class="cart_delivery__title mb-3">Podsumowanie zamówienia</p>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-lg-6">
                <div class="row">
                    <div class="col">
                        {foreach $cart as $cart_item}
                            {if !empty($prizes[$cart_item->id_prize])}
                                <div class="cart_summary__single_product">
                                    <div class="cart_summary__single_product_img">
                                        {if !empty($prizes[$cart_item->id_prize]->photos[0]->id_cimg)}
                                            <img class="img-fluid"
                                                 src="{img id_cimg={$prizes[$cart_item->id_prize]->photos[0]->id_cimg} height=250 width=250}"/>
                                        {else}
                                            <img class="img-fluid"
                                                 src="{img id_cimg=0 height=250 width=250}"/>
                                        {/if}
                                    </div>
                                    <div class="cart_summary__single_product_description">
                                        <span class="cart_summary__single_product_description--wrapper">
                                            <p class="cart_summary__text--small">
                                                {$prizes[$cart_item->id_prize]->name}
                                            </p>
                                            <p class="cart_summary__single_product_description--big">
                                                {$prizes_variants[$cart_item->id_prize][$cart_item->id_prize_option]->description}
                                            </p>
                                        </span>
                                        <span class="cart_summary__single_product_description--wrapper">
                                            <p class="cart_summary__text--small">Ilość</p>
                                            <p class="cart_summary__single_product_description--big">{$cart_item->count}</p>
                                        </span>
                                        <span class="cart_summary__single_product_description--wrapper">
                                            <p class="cart_summary__text--small">Wartość</p>
                                            <p class="cart_summary__single_product_description--big">{math equation="x * y" x=$cart_item->price y=$cart_item->count} E-LUX</p>
                                        </span>
                                    </div>
                                </div>
                            {/if}
                        {/foreach}
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-6">
                <div class="cart_summary__user_info">
                    <p class="cart_summary__text--small">Wysyłka</p>
                    <p class="cart_summary__user_info--big">{$order->address->recipent_company}</p>
                    <p class="cart_summary__user_info--medium">
                        ul. {$order->address->street} {$order->address->street_number}{if !empty($order->address->apartment_number)}/{$order->address->apartment_number}{/if}</p>
                    <p class="cart_summary__user_info--medium">{$order->address->post_code} {$order->address->city}</p>
                </div>
                <div class="cart_summary__user_info">
                    <p class="cart_summary__text--small">Dane kontaktowe</p>
                    <p class="cart_summary__user_info--big">{$order->address->recipent_name} {$order->address->recipent_surname}</p>
                    <p class="cart_summary__user_info--medium">{$order->address->telephone}</p>
                    <p class="cart_summary__user_info--medium">{$order->address->email}</p>
                </div>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-12 col-lg-6">
                <div class="cart_summary__sum">
                    <p class="cart_summary__text--small">Razem</p>
                    {if $cart_sum_annual == 0 || $cart_sum > 0}
                        <p class="cart_summary__sum--big">{$cart_sum} <span class="text-nowrap">E-LUX</span></p>
                    {/if}
                    {if $cart_sum_annual > 0}
                        <p class="cart_summary__sum--big">{$cart_sum_annual} <span
                                    class="text-nowrap">Roczne E-LUXY</span></p>
                    {/if}
                </div>
            </div>
            <div class="col-12 col-lg-6">
                <form action="{BASE_URL}cart/submit_order.html" method="post"
                      class="flex-wrap flex-sm-nowrap d-flex justify-content-between align-items-center">
                    <li class="w-100 filter_options__wrapper text-nowrap align-self-end">
                        <label class="filter_options__label">
                            <input type="checkbox" class="filter_options__input" name="cart_summary_regulations"
                                   value="1">
                            Zgadzam się z
                            <span class="filter_options__helper"></span>
                        </label>
                        <input type="hidden" name="id_order" value="{$order->id}">
                        <span class="cart_summary_regulations">Regulaminem portalu.</span>
                    </li>
                    <div class="mt-3 mt-sm-0 d-flex justify-content-end w-100">
                        <button type="submit"
                                class="btn btn-outline-primary btn-pills cart_product_next_step">
                            zamów
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
	$(document).ready(function () {
		$('.cart_product_next_step').on('click', function (e) {
			if ($('input[name="cart_summary_regulations"]').is(':checked')) {
          {foreach $cart as $cart_item}
          {if !empty($prizes[$cart_item->id_prize])}
          {literal}
				_mtm.push({
					'orderTotal': {/literal}{math equation="x * y" x=$cart_item->price y=$cart_item->count}{literal},
					'orderCurrency': 'E-LUX'
				});
				_paq.push(['trackEvent', 'Zamówienie', 'ID zamawianego produktu', {/literal}{$cart_item->id_prize}{literal}]);
          {/literal}
          {/if}
          {/foreach}
          {literal}
				$(this).closest('form').submit();
				e.preventDefault();
			}
		})
	});
  {/literal}
</script>