<div class="container-fluid page_content cart mb-5" xmlns="http://www.w3.org/1999/html">
    <div class="row">
        <div class="col-auto">
            <p class="cart_title">Twój Koszyk</p>
        </div>
        <div class="col-12 col-lg mt-md-0 mt-3 d-flex flex-wrap justify-content-md-end align-items-center">
            <div class="cart_steps">
                <a href="{BASE_URL}cart" class="cart_step cart_steps--orange">
                    zawartość koszyka
                </a>
            </div>
            <div class="cart_steps">
                <a href="{BASE_URL}cart/delivery.html" class="cart_step  cart_steps--orange">
                    dostawa
                </a>
            </div>
            <div class="cart_steps">
                <a href="{BASE_URL}cart/summary.html" class="cart_step cart_steps--orange">
                    podsumowanie
                </a>
            </div>
            <div class="cart_steps">
                <a href="{BASE_URL}cart/end.html" class="cart_step cart_steps--orange">
                    koniec
                </a>
            </div>
        </div>
    </div>
    <div class="mt-4 cart_summary cart_end">
        <div class="row">
            <div class="col-12">
                <p class="cart_end__title">Twoje zamówienie zostało złożone!</p>
                <p class="cart_end__standard">
                    Dziękujemy za złożenie zamówienia, które otrzymało nr <span class="cart_end__standard--lightBlue">{if !empty($smarty.get.id_order)}{$smarty.get.id_order}{/if}</span>
                </p>
                <p class="cart_end__standard">Niebawem otrzymasz maila potwierdzającego zamówienie.</p>
                <p class="cart_end__standard mb-4">W każdej chwili jesteś w stanie sprawdzić stan swojego zamówienia w panelu użytkownika w zakładce ‘Nagrody’.</p>
                <p class="cart_end__standard">W razie jakichkolwiek wątpliwości zapraszamy do kontaktu z swoim handlowcem.</p>
            </div>
            <div class="col-12 mt-4 mt-sm-5 d-flex flex-column flex-sm-row w-100">
                <div class="d-flex justify-content-end">
                <a href="{BASE_URL}prizes" class="btn btn-outline-primary btn-pills cart_product_next_step mb-3 mb-sm-0 mr-0 mr-sm-3">
                    wróć do zakupów
                </a>
                </div>
                <div class="d-flex justify-content-end">
                <a href="{BASE_URL}panel/nagrody.html" class="btn btn-primary btn-pills cart_product_next_step">
                    panel użytkownika
                </a>
                </div>
            </div>
        </div>
    </div>
</div>