<div class="container-fluid page_content cart mb-5">
    <div class="row">
        <div class="col-auto">
            <p class="cart_title">Twój Koszyk</p>
        </div>
        <div class="col-12 col-lg mt-md-0 mt-3 d-flex flex-wrap justify-content-md-end align-items-center">
            <div class="cart_steps">
                <a href="{BASE_URL}cart" class="cart_step cart_steps--orange">
                    zawartość koszyka
                </a>
            </div>
            <div class="cart_steps">
                <a href="{BASE_URL}cart/delivery.html" class="cart_step  cart_steps--orange">
                    dostawa
                </a>
            </div>
            <div class="cart_steps">
                <a href="{BASE_URL}cart/summary.html" class="cart_step cart_steps--backgroundBlue">
                    podsumowanie
                </a>
            </div>
            <div class="cart_steps">
                <a href="{BASE_URL}cart/end.html" class="cart_step cart_steps--backgroundLightBlue">
                    koniec
                </a>
            </div>
        </div>
    </div>
    <div class="row mt-4 cart_delivery">
        <form action="{BASE_URL}cart/save_order_address.html" style="width: 100%; display: inherit;">
            <div class="col-12 col-md-6">
                <p class="cart_delivery__title mb-3">Dane zamawiającego</p>
                <div class="form-group cart_delivery__input">
                    <label class="cart_delivery__input_label" for="cart_delivery_name">Imię</label>
                    <input type="text" class="form-control" id="cart_delivery_name" name="cart_delivery_name"
                           placeholder="" value="">
                </div>
                <div class="form-group cart_delivery__input">
                    <label class="cart_delivery__input_label" for="cart_delivery_lname">Nazwisko</label>
                    <input type="text" class="form-control" id="cart_delivery_lname" name="cart_delivery_lname"
                           placeholder="" value="">
                </div>
                <div class="form-group cart_delivery__input">
                    <label class="cart_delivery__input_label" for="cart_delivery_firm">Firma</label>
                    <input type="text" class="form-control" id="cart_delivery_firm" name="cart_delivery_firm"
                           placeholder="" value="">
                </div>
            </div>
            <div class="col-12 col-md-6">
                <p class="cart_delivery__title mb-3">Adres dostawy</p>
                <div class="form-group cart_delivery__input">
                    <label class="d-none d-md-block cart_delivery__input_label">&nbsp;</label>
                    <select class="form-control form-control-select" id="address-select">
                        <option value="">Wybierz adres</option>
                        {if !empty($addresses)}
                            {foreach $addresses as $address_id => $address}
                                <option value="{$address_id}"
                                        {if !empty($address['is_main'])}selected{/if}>{$address['name']}</option>
                            {/foreach}
                        {/if}
                    </select>
                </div>
                <div class="form-row">
                    <div class="col-lg-6 form-group cart_delivery__input">
                        <label class="cart_delivery__input_label" for="cart_delivery_street">Ulica</label>
                        <input type="text" class="form-control" id="cart_delivery_street" name="cart_delivery_street"
                               placeholder="">
                    </div>
                    <div class="col-6 col-lg form-group cart_delivery__input">
                        <label class="cart_delivery__input_label text-nowrap" for="cart_delivery_street_number">Nr
                            domu</label>
                        <input type="text" class="form-control" id="cart_delivery_street_number"
                               name="cart_delivery_street_number" placeholder="">
                    </div>
                    <div class="col-6 col-lg form-group cart_delivery__input">
                        <label class="cart_delivery__input_label text-nowrap" for="cart_delivery_street_local">Nr
                            lokalu</label>
                        <input type="text" class="form-control" id="cart_delivery_street_local"
                               name="cart_delivery_street_local" placeholder="">
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-12 col-md-6 form-group cart_delivery__input">
                        <label class="cart_delivery__input_label" for="cart_delivery_city">Miejscowość</label>
                        <input type="text" class="form-control" id="cart_delivery_city" name="cart_delivery_city"
                               placeholder="">
                    </div>
                    <div class="col-12 col-md-6 form-group cart_delivery__input">
                        <label class="cart_delivery__input_label" for="cart_delivery_street_zipCode">Kod
                            pocztowy</label>
                        <input type="text" class="form-control" id="cart_delivery_street_zipCode"
                               name="cart_delivery_street_zipCode" placeholder="">
                    </div>
                </div>
                <div class="form-row">
                    <div class="col col-lg-6 form-group cart_delivery__input">
                        <label class="cart_delivery__input_label" for="cart_delivery_telephone">Numer kontaktowy</label>
                        <input type="text" class="form-control" id="cart_delivery_telephone"
                               name="cart_delivery_telephone"
                               placeholder="" value="">
                    </div>
                </div>
                <div class="form-row">
                    <div class="col col-lg-6 form-group cart_delivery__input">
                        <label class="cart_delivery__input_label" for="cart_delivery_email">Adres e-mail</label>
                        <input type="email" class="form-control" id="cart_delivery_email" name="cart_delivery_email"
                               placeholder="" value="">
                    </div>
                </div>
                <div class="d-flex justify-content-end mt-5">
                    <button type="submit"
                            class="btn btn-outline-primary btn-pills cart_product_next_step"
                            formmethod="post">
                        dalej
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>

<script>
    {literal}
    $(document).ready(function () {
        function autocompleteAddressFields() {
            var name = $('#cart_delivery_name');
            var surname = $('#cart_delivery_lname');
            var company = $('#cart_delivery_firm');
            var street = $('#cart_delivery_street');
            var streetNumber = $('#cart_delivery_street_number');
            var apartmentNumber = $('#cart_delivery_street_local');
            var city = $('#cart_delivery_city');
            var zipCode = $('#cart_delivery_street_zipCode');
            var telephone = $('#cart_delivery_telephone');
            var email = $('#cart_delivery_email');

            var addressId = $('#address-select').val();

            if (addressId && addresses[addressId]) {
                address = addresses[addressId];

                street.val(address['street']);
                streetNumber.val(address['street_number']);
                apartmentNumber.val(address['apartment_number']);
                zipCode.val(address['post_code']);
                city.val(address['city']);
                telephone.val(address['telephone']);
                email.val(address['email']);
                name.val(address['recipent_name']);
                surname.val(address['recipent_surname']);
                company.val(address['recipent_company']);
            }
            else
            {
                street.val('');
                streetNumber.val('');
                apartmentNumber.val('');
                zipCode.val('');
                city.val('');
                telephone.val('');
                email.val('');
                name.val('');
                surname.val('');
                company.val('');
            }
        }

        function insertOptionsToSelect() {
            $.each(addresses, function(index, addressElement){
                $('#address-select').append(
                    '<option value="' + index + '" '+ (addressElement.is_main ? 'selected' : '') +'>' + addressElement.name + '</option>');
            });
        }

        $('#address-select').change(function () {
            autocompleteAddressFields();
        });

        loaderInit();
        $.get(BASE_URL + 'cart/get_company_addresses.html', function (response) {
            if(response){
                addresses = JSON.parse(response);

                insertOptionsToSelect();

                autocompleteAddressFields();
            }
        })
            .always(function(){
                loaderHide();
            });
    });
    {/literal}
</script>