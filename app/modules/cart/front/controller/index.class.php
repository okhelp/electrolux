<?php

use App\ed\model\company\Get_by_user_id;
use App\ed\model\company\Get_company_adresses;
use App\modules\cart\front\collections\Cart_count;
use App\modules\cart\front\collections\Get_cart;
use App\modules\cart\front\models\Cart_model;
use App\modules\cart\front\objects\Cart;
use App\modules\cart\front\objects\Get_order_addresses;
use App\modules\cart\front\objects\Verify_cart;
use App\modules\order\front\collections\Get_not_submitted_order;
use App\modules\order\front\collections\Get_not_submitted_order_with_address;
use App\modules\order\front\objects\Save_order_address;
use App\modules\order\front\objects\Submit_order;
use App\modules\prizes\ed\models\Prize_model;
use App\modules\prizes\front\collections\Get_prize_variants;
use App\modules\prizes\front\objects\Get_prizes;

class App__Modules__Cart__Front__Controller__Index extends Lib__Base__Ed_Controller
{
    public function __construct()
    {
        parent::__construct();

        if (!empty($this->session['id']) && !App__Ed__Model__Acl::has_access('cart', 0))
        {
            if (is_ajax())
            {
                http_response_code(400);
            }
            else
            {
                go_back('w|Brak dostępu.');
            }

            die();
        }
    }

    public function action_index()
    {
        $cart = !empty($this->_user->id) ? Get_cart::get($this->_user->id) : [];

        if (!empty($cart))
        {
            $prizes_ids = array_map(
                function($cart_item) {
                    return $cart_item->id_prize;
                }, $cart);

            $prizes = Get_prizes::by_ids($prizes_ids);

            $this->view->assign('prizes', $prizes);

            $prizes_variants = Get_prize_variants::get_by_ids($prizes_ids);

            $this->view->assign('prizes_variants', $prizes_variants);

            $this->view->assign('cart_sum', Cart::sum_cart_products_standard($cart));
            $this->view->assign('cart_sum_annual', Cart::sum_cart_products_annual($cart));
        }

        $this->view->assign('cart', $cart);

        $this->template = get_module_template_path('cart') . 'cart.tpl';

        $this->breadcrumb[BASE_URL . 'cart'] = 'Twój koszyk';
    }

    public function action_add_product()
    {
        $added_to_cart = false;

        if (!empty($_POST['id_prize']) && !empty($_POST['id_variant']) && !empty($this->_user->id))
        {
            $added_to_cart = Cart::add_to_cart((int)$_POST['id_prize'], (int)$_POST['id_variant'], $this->_user->id);
        }

        http_response_code($added_to_cart ? 200 : 400);
    }

    public function action_remove_product()
    {
        $deleted = false;

        if (!empty($_POST['id_cart_element']))
        {
            $deleted = Cart::remove_from_cart($this->_user->id, (int)$_POST['id_cart_element']);
        }

        echo $this->get_cart_products_listing();

        http_response_code($deleted ? 200 : 400);
    }

    public function action_update_product_count()
    {
        $count_changed = false;

        if (!empty($_POST['id_cart_element']) && !empty($_POST['count']) && !empty($this->_user->id))
        {
            $count_changed = Cart::change_product_count($this->_user->id, (int)$_POST['id_cart_element'], (int)$_POST['count']);
        }

        echo $this->get_cart_products_listing();

        http_response_code($count_changed ? 200 : 400);
    }

    public function action_get_cart_count()
    {
        $count = 0;

        if (!empty($this->_user->id))
        {
            $count = Cart_count::get($this->_user->id);
        }

        if (!empty($count))
        {
            echo $count;
        }
    }

    private function get_cart_products_listing()
    {
        $cart = Get_cart::get($this->_user->id);

        $smarty = new Smarty();
        if (!empty($cart))
        {
            $prizes_ids = array_map(
                function($cart_item) {
                    return $cart_item->id_prize;
                }, $cart);

            $prizes = Get_prizes::by_ids($prizes_ids);

            $smarty->assign('prizes', $prizes);

            $prizes_variants = Get_prize_variants::get_by_ids($prizes_ids);

            $smarty->assign('prizes_variants', $prizes_variants);

            $cart_sum = $this->sum_cart_products($cart, 0);
            $cart_sum_annual = $this->sum_cart_products($cart, 1);

            $smarty->assign('cart_sum', $cart_sum);
            $smarty->assign('cart_sum_annual', $cart_sum_annual);
        }

        $smarty->assign('cart', $cart);

        return $smarty->fetch(get_module_template_path('cart') . 'cart_products_listing.tpl');
    }

    public function action_delivery()
    {
        try
        {
            $verify_cart = new Verify_cart($this->_user->id);
            $verify_cart->verify();

            $this->template = get_module_template_path('cart') . 'delivery.tpl';

            $this->breadcrumb = [
                BASE_URL . 'cart'               => 'Twój koszyk',
                BASE_URL . 'cart/delivery.html' => 'Dostawa',
            ];
        }
        catch (Exception $exception)
        {
            go_back('d|' . $exception->getMessage(), BASE_URL . 'cart');
        }
    }

    public function action_summary()
    {
        try
        {
            $verify_cart = new Verify_cart($this->_user->id);
            $verify_cart->verify();

            $order = Get_not_submitted_order_with_address::get($this->_user->id);

            if (empty($order->address))
            {
                go_back('d|Nie wybrano adresu dostawy.', BASE_URL . 'cart/delivery.html');
            }
            else
            {
                $cart = Get_cart::get($this->_user->id);

                if (!empty($cart))
                {
                    $prizes_ids = array_map(
                        function($cart_item) {
                            return $cart_item->id_prize;
                        }, $cart);

                    $prizes = Get_prizes::by_ids($prizes_ids);

                    $this->view->assign('prizes', $prizes);

                    $prizes_variants = Get_prize_variants::get_by_ids($prizes_ids);

                    $this->view->assign('prizes_variants', $prizes_variants);

                    $this->view->assign('cart_sum', Cart::sum_cart_products_standard($cart));
                    $this->view->assign('cart_sum_annual', Cart::sum_cart_products_annual($cart));

                    $this->view->assign('cart', $cart);

                    $this->view->assign('order', $order);

                    $this->breadcrumb = [
                        BASE_URL . 'cart'               => 'Twój koszyk',
                        BASE_URL . 'cart/delivery.html' => 'Dostawa',
                        BASE_URL . 'cart/summary.html'  => 'Podsumowanie',
                    ];
                } else {
                    go_back('d|Napotkaliśmy błąd.', BASE_URL . 'cart/');
                }
            }
        }
        catch (Exception $exception)
        {
            go_back('d|' . $exception->getMessage(), BASE_URL . 'cart/delivery.html');
        }

        $this->template = get_module_template_path('cart') . 'summary.tpl';
    }

    public function action_save_order_address()
    {
        if (!empty($_POST))
        {
            $save_order_address = new Save_order_address($_POST, $this->_user->id);

            $address_was_saved = $save_order_address->save();

            go_back(
                $address_was_saved ? 'g|Adres został zapisany.' : 'd|Adres nie został zapisany.',
                $address_was_saved ? BASE_URL . 'cart/summary.html' : null);
        }
        else
        {
            go_back('d|Brak dostępu.');
        }
    }

    public function action_end()
    {
        if (!empty($_GET['id_order']))
        {
            $this->template = get_module_template_path('cart') . 'end.tpl';

            $this->breadcrumb = [
                BASE_URL . 'cart'                                        => 'Twój koszyk',
                BASE_URL . 'cart/end.html?id_order=' . $_GET['id_order'] => 'Koniec',
            ];
        }
        else
        {
            go_back('d|Brak dostępu.');
        }
    }

    public function action_get_company_addresses()
    {
        $company = Get_by_user_id::get($this->_user->id);

        $company_addresses = Get_company_adresses::get_as_array($company->id);

        foreach ($company_addresses as $index => $company_address)
        {
            if(empty($company_address['public']) && $company_address['id_user'] != $_SESSION['id'])
            {
                unset($company_addresses[$index]);
            }
        }

        $adresses = Get_order_addresses::get($this->_user->id, $company_addresses, $company);

        $user_data = App__Ed__Model__Users::find($this->_user->id);

        foreach ($adresses as &$address)
        {
            $address['recipent_name'] = !isset($address['recipent_name']) ? $user_data->name : $address['recipent_name'];
            $address['recipent_surname'] = !isset($address['recipent_surname']) ? $user_data->surname : $address['recipent_surname'];
            $address['recipent_company'] = !isset($address['recipent_company']) ? $company->name : $address['recipent_company'];
            $address['telephone'] = !isset($address['telephone']) ? $user_data->telephone : $address['telephone'];
            $address['email'] = !isset($address['email']) ? $user_data->email : $address['email'];
        }

        echo json_encode($adresses);
    }

    public function action_submit_order()
    {
        if (!empty($_POST['cart_summary_regulations']) && !empty($_POST['id_order']))
        {
            $connection = \ActiveRecord\ConnectionManager::get_connection();
            $connection->transaction();

            //zamówienie możemy złożyć jak osiągniemy próg 500 e-luxów
            $all_points_from_sale = \App\ed\model\points\Get_users_points::get_all([(int)Lib__Session::get('id')]);
            $all_points_from_sale = reset($all_points_from_sale);

            if($all_points_from_sale < Submit_order::MIN_POINTS)
            {
                go_back('d|Nie możesz złożyć zamówienia. <br>Nie osiągnąłeś progu ' . Submit_order::MIN_POINTS . ' E-luxów umożliwiającego składania zamówień na nagrody.<br>Sprawdz §4 pkt. 17 regulaminu.');
            }

            try
            {
                $submit_object = new Submit_order((int)$_POST['id_order'], $this->_user->id);
                $submit_object->submit();

                $connection->commit();

                go_back('g|Twoje zamówienie zostało złożone.', BASE_URL . 'cart/end.html?id_order=' . $_POST['id_order']);
            }
            catch (Exception $exception)
            {
                $connection->rollback();

                go_back('d|Twoje zamówienie nie zostało złożone.', BASE_URL . 'cart/summary.html');
            }
        }
        else
        {
            go_back('d|Musisz wyrazić zgodę z regulaminem portalu.');
        }
    }

    private function sum_cart_products($cart, int $annual): int
    {
        $cart_sum = 0;
        foreach ($cart as $cart_item)
        {
            if (Prize_model::find($cart_item->id_prize)->year_prize == $annual) {
                $cart_sum += $cart_item->{Cart_model::COLUMN_PRICE} * $cart_item->{Cart_model::COLUMN_COUNT};
            }
        }
        return $cart_sum;
    }
}