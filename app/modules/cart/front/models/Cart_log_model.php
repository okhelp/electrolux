<?php
declare(strict_types=1);

namespace App\modules\cart\front\models;

use Lib__Base__Model;

class Cart_log_model extends Lib__Base__Model
{
    const COLUMN_ID          = 'id';
    const COLUMN_CART_BEFORE = 'cart_before';
    const COLUMN_CART_AFTER  = 'cart_after';
    const COLUMN_ID_USER     = 'id_user';
    const COLUMN_ACTION      = 'action';
    const COLUMN_CREATED_AT  = 'created_at';

    const TABLE_NAME = 'cart_log';

    static $table_name = self::TABLE_NAME;
    static $primary_key = self::COLUMN_ID;
}