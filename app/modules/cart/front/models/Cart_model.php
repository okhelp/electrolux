<?php
declare(strict_types=1);

namespace App\modules\cart\front\models;

use Lib__Base__Model;

class Cart_model extends Lib__Base__Model
{
    const COLUMN_ID              = 'id';
    const COLUMN_ID_USER         = 'id_user';
    const COLUMN_ID_PRIZE        = 'id_prize';
    const COLUMN_ID_PRIZE_OPTION = 'id_prize_option';
    const COLUMN_COUNT           = 'count';
    const COLUMN_PRICE           = 'price';
    const COLUMN_CREATED_AT      = 'created_at';

    const TABLE_NAME = 'cart';

    static $table_name = self::TABLE_NAME;
    static $primary_key = self::COLUMN_ID;
}