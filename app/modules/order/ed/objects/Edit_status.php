<?php
declare(strict_types=1);

namespace App\modules\order\ed\objects;

use App\modules\order\ed\collections\Get_order_statuses;
use App\modules\order\ed\models\Order_status;

class Edit_status
{
    /** @var int */
    private $id_status;

    /** @var array */
    private $post_data;

    /**
     * Edit_status constructor.
     * @param int $id_status
     * @param array $post_data
     */
    public function __construct(int $id_status, array $post_data)
    {
        $this->id_status = $id_status;
        $this->post_data = $post_data;
    }

    public function edit(): bool
    {
        $edited = false;

        $status = Order_status::find($this->id_status);

        if (!empty($status))
        {
            $status->{Order_status::COLUMN_NAME} = empty($this->post_data['name']) ? 'Nowy status' : $this->post_data['name'];
            $status->{Order_status::COLUMN_SEND_NOTIFICATION} = empty($this->post_data['send_notification']) ? 0 : 1;
            $status->{Order_status::COLUMN_NOTIFICATION_TEXT} = empty($this->post_data['notification_text']) ? '' : $this->post_data['notification_text'];

            $edited = $status->save();

            if ($edited)
            {
                Get_order_statuses::get_all((int)$status->{Order_status::COLUMN_ID_SERVICE}, (int)$status->{Order_status::COLUMN_ID_LANG}, true);
            }
        }

        return $edited;
    }
}