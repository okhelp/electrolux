<?php
declare(strict_types=1);

namespace App\modules\order\ed\objects;

use App\front\models\user\User_points;
use App\modules\cart\front\objects\Cart;
use App\modules\order\ed\models\Order_model;
use App\modules\order\ed\models\Order_status_log;

class Change_order_status
{
    /** @var int */
    private $id_order;

    /** @var int */
    private $status;

    /**
     * Change_order_status constructor.
     * @param int $id_order
     * @param int $status
     */
    public function __construct(int $id_order, ?int $status)
    {
        $this->id_order = $id_order;
        $this->status = $status;
    }

    /**
     * @param array $order_statuses
     * @return bool
     * @throws \ActiveRecord\RecordNotFound
     */
    public function change_status(array $order_statuses): bool
    {
        $status_changed = false;

        $order = Order_model::find($this->id_order);

        if (!empty($order) && $order->status != 17) {
            $order_status_log = new Order_status_log(clone $order, $order_statuses);
            if ($this->status) {
                $order->{Order_model::COLUMN_STATUS} = $this->status;
            }
            $order->{Order_model::COLUMN_DATE_REALIZATION} = $_POST['date_realization'] ?? null;
            $status_changed = $order->save();

            if($status_changed)
            {
                if ($this->status == 17) {
                    $cart = json_decode($order->{Order_model::COLUMN_PRODUCTS});
                    User_points::sub($order->{Order_model::COLUMN_ID_USER}, -Cart::sum_cart_products_standard($cart));
                    User_points::sub_annual($order->{Order_model::COLUMN_ID_USER}, -Cart::sum_cart_products_annual($cart));
                }
                $notify = new Email_order_status_changed($order);
                $notify->send();

                $order_status_log->add($order);
            }
        }

        return $status_changed;
    }
}