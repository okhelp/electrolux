<?php
declare(strict_types=1);

namespace App\modules\order\ed\objects;

use App\ed\model\mail\Send_email_by_queue;
use App\ed\model\users\Get_by_ids;
use App\modules\order\ed\collections\Get_order_statuses;
use App\modules\order\ed\models\Order_model;
use App\modules\order\ed\models\Order_status;
use App\modules\order\front\objects\Shortcodes;

class Email_order_status_changed
{
    /** @var int */
    private $id_status;

    /** @var int */
    private $id_user;

    /** @var \App__Ed__Model__Users */
    private $user;

    /** @var ?Order_status */
    private $status;

    /** @var Order_model */
    private $order;

    /**
     * @param int $id_status
     * @param int $id_user
     */
    public function __construct(Order_model $order)
    {
        $this->id_status = $order->status;
        $this->id_user = $order->id_user;
        $this->order = $order;
        $this->status = Get_order_statuses::get_by_status_id($this->id_status);
        $this->user = Get_by_ids::get([$this->id_user])[$this->id_user];
    }

    public function send()
    {
        if (!empty($this->status->{Order_status::COLUMN_SEND_NOTIFICATION}))
        {
            Send_email_by_queue::send($this->user->email, 'Zmiana statusu zamówienia', 'Twoje zamówienie zmieniło status!', $this->prepare_content());
        }
    }

    private function prepare_content(): string
    {
        return Shortcodes::parse($this->status->{Order_status::COLUMN_NOTIFICATION_TEXT}, $this->user, $this->status, $this->order);
    }
}