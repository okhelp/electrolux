<?php
declare(strict_types=1);

namespace App\modules\order\ed\objects;

use ActiveRecord\DateTime;
use App\modules\order\ed\collections\Get_order_statuses;
use App\modules\order\ed\models\Order_status;
use Exception;

class Remove_status
{
    /** @var int */
    private $id_status;

    /**
     * Remove_status constructor.
     * @param int $id_status
     */
    public function __construct(int $id_status)
    {
        $this->id_status = $id_status;
    }

    /**
     * @return bool
     * @throws \ActiveRecord\RecordNotFound
     */
    public function remove(): bool
    {
        $removed = false;

        $status = Order_status::find($this->id_status);

        if (!empty($status))
        {
            $id_service = (int)$status->{Order_status::COLUMN_ID_SERVICE};
            $id_lang = (int)$status->{Order_status::COLUMN_ID_LANG};

            try
            {
                $status->{Order_status::COLUMN_DELETED_AT} = (new DateTime('now'))->format('Y:m:d H:i:s');

                $removed = $status->save();

                if ($removed)
                {
                    Get_order_statuses::get_all($id_service, $id_lang, true);
                }
            }
            catch(Exception $exception)
            {
                $removed = false;
            }
        }

        return $removed;
    }
}