<?php
declare(strict_types=1);

namespace App\modules\order\ed\objects;

use App\modules\order\ed\collections\Get_order_statuses;
use App\modules\order\ed\models\Order_status;

class Add_status
{
    /** @var int */
    private $id_lang;

    /** @var int */
    private $id_service;

    /** @var array */
    private $post_data;

    /**
     * Add_status constructor.
     * @param int $id_lang
     * @param int $id_service
     * @param array $post_data
     */
    public function __construct(int $id_lang, int $id_service, array $post_data)
    {
        $this->id_lang = $id_lang;
        $this->id_service = $id_service;
        $this->post_data = $post_data;
    }

    /**
     * @return bool
     */
    public function add(): bool
    {
        $status = new Order_status();
        $status->{Order_status::COLUMN_ID_LANG} = $this->id_lang;
        $status->{Order_status::COLUMN_ID_SERVICE} = $this->id_service;
        $status->{Order_status::COLUMN_NAME} = empty($this->post_data['name']) ? 'Nowy status' : $this->post_data['name'];
        $status->{Order_status::COLUMN_SEND_NOTIFICATION} = empty($this->post_data['send_notification']) ? 0 : 1;
        $status->{Order_status::COLUMN_NOTIFICATION_TEXT} = empty($this->post_data['notification_text']) ? '' : $this->post_data['notification_text'];

        $saved = $status->save();

        if ($saved)
        {
            Get_order_statuses::get_all($this->id_service, $this->id_lang, true);
        }

        return $saved;
    }
}