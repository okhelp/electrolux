{load_plugin name="datatable"}
{load_plugin name="datepicker"}

<div class="card">
    <div class="card-header">
        Produkty
    </div>
    <div class="card-body">
        {if !empty($order->products_array)}
            <table class='datatable'>
                <thead>
                <tr>
                    <th>Zdjęcie</th>
                    <th>Szczegóły</th>
                    <th>Cena</th>
                    <th>Ilość</th>
                    <th>Razem</th>
                </tr>
                </thead>
                <tbody>
                {foreach $order->products_array as $product}
                    <tr>
                        <td>
                            <div>
                                {if !empty($product->prize->photos[0]->id_cimg)}
                                <img class="img-fluid"
                                     src="{img id_cimg={$product->prize->photos[0]->id_cimg} height=150 width=150}">
                                {else}
                                    <img class="img-fluid"
                                         src="{img id_cimg=0 height=150 width=150}" style="width: 150px; height: 150px;">
                                {/if}
                            </div>
                        </td>
                        <td>
                            <div>
                                <p class="cart_product_title">{$product->prize->name}</p>
                                <p class="cart_product_description">
                                    {$product->prize_variant->title}
                                </p>
                                <p class="cart_product_description">
                                    {$product->prize_variant->description}
                                </p>
                            </div>
                        </td>
                        <td>
                            <p class="cart_product_info_text">
                                {$product->price} <span class="text-nowrap">{if $product->prize->year_prize == 1}Roczne E-LUXY{else}E-LUX{/if}</span>
                            </p>
                        </td>
                        <td>
                            <p class="cart_product_info_text">{$product->count}</p>
                        </td>
                        <td>
                            <p>{math equation="x * y" x={$product->price} y={$product->count}} <span
                                        class="text-nowrap">{if $product->prize->year_prize == 1}Roczne E-LUXY{else}E-LUX{/if}</span></p>
                        </td>
                    </tr>
                {/foreach}
                </tbody>
            </table>
        {/if}
    </div>
</div>

<div class="card">
    <div class="card-header">
        Dane adresowe zamówienia
    </div>
    <div class="card-body">
        <div class="row justify-content-start">
            <div class="col">
                <div>{$order->address->recipent_company}</div>
                <div>{$order->address->recipent_name} {$order->address->recipent_surname}</div>
                <div>{$order->address->telephone}</div>
                <div>{$order->address->email}</div>
            </div>
            <div class="col">
                <div>{$order->address->street} {$order->address->street_number}{if !empty($order->address->apartment_number)}/{$order->address->apartment_number}{/if}</div>
                <div>{$order->address->post_code} {$order->address->city}</div>
            </div>
        </div>
    </div>
</div>

<div class="card">
    <div class="card-header">
        Status
    </div>
    <div class="card-body">
        <p>Aktualny status: {$order_statuses[$order->status]->name}</p>
        <form action="{BASE_ED_URL}order/change_order_status.html">
            <div class="form-group">
                <label>Nowy status zamówienia</label>
                <select name="order_new_status"{if $order->status == 17} disabled{/if}>
                    {foreach $order_statuses as $order_status}
                        {if empty($order_status->deleted_at)}
                            <option value="{$order_status->id}" {if $order_status->id == $order->status}selected disabled{/if}>{$order_status->name}</option>
                        {/if}
                    {/foreach}
                </select>
                <input type="hidden" name="id_order" value="{$order->id}">
            </div>
            <div class="form-group"{if $order->status != 19} style="display: none"{/if}>
                <label>Data realizacji zamówienia</label>
                <input type="text" name="date_realization" class="datepicker" value="{if !empty($order->date_realization)}{$order->date_realization->format('Y-m-d')}{/if}">
            </div>
            <div class="form-group">
                <button class="btn btn-primary" type="submit" formmethod="post"{if $order->status == 17} disabled{/if}>Zmień status</button>
            </div>
        </form>
        {if !empty($order_status_log)}
            <table class='datatable'>
                <thead>
                <tr>
                    <th>Zmiana statusu</th>
                    <th>Data</th>
                </tr>
                </thead>
                <tbody>
                {foreach $order_status_log as $status_log}
                    <tr>
                        <td>{$status_log->status_before_text}</td>
                        <td>{$status_log->status_after_text}</td>
                    </tr>
                {/foreach}
                </tbody>
            </table>
        {/if}
    </div>
</div>

<div class="card">
    <div class="card-header">
        Wysłane powiadomienia
    </div>
    <div class="card-body">
        {*TODO zebranie powiadomień wysłanych w związku ze zmianami statusu zamówienia*}
    </div>
</div>

<script>
    $('select[name="order_new_status"]').on('change', function () {
    	var $formGroup = $('input[name="date_realization"]').closest('.form-group');
        if (parseInt($(this).val()) === 19) {
          $formGroup.show();
        } else {
        	$formGroup.hide();
        }
    });
</script>