{load_plugin name="datatable"}
{load_plugin name="datepicker"}
<form type="post" action="">
    <div class="row">
        <div class="col-6 col-xl">
            <div class="form-group">
                <label>Numer zamówienia</label>
                <input type="text" name="id" class="form-control form-control-sm" placeholder="">
            </div>
        </div>
        <div class="col-6 col-xl">
            <div class="form-group">
                <label>Firma</label>
                <input type="text" name="company" class="form-control form-control-sm" placeholder="">
            </div>
        </div>
        <div class="col-6 col-xl">
            <div class="form-group">
                <label>Data od</label>
                <input type="text" name="date_from" class="form-control form-control-sm datepicker" autocomplete="off" placeholder="">
            </div>
        </div>
        <div class="col-6 col-xl">
            <div class="form-group">
                <label>Data do</label>
                <input type="text" name="date_to" class="form-control form-control-sm datepicker" autocomplete="off" placeholder="">
            </div>
        </div>
        <div class="col-6 col-xl">
            <label>Status</label>
            <select name="status" class="form-control form-control-sm">
                <option></option>
                {foreach $statuses as $status}
                    <option value="{$status->id}">{$status->name}</option>
                {/foreach}
            </select>
        </div>
        {if $is_admin}
            <div class="col-6 col-xl">
                <div class="form-group">
                    <label>Opiekun</label>
                    <input type="text" name="superior" class="form-control form-control-sm" placeholder="">
                </div>
            </div>
        {/if}
    </div>
    <div class="row">
        <div class="col-12">
            <button type="submit" formmethod="post" class="btn btn-primary btn-block ml-auto" style="width: 100px">Filtruj</button>
        </div>
    </div>
</form>

<table class='datatable'>
    <thead>
    <tr>
        <th>Numer zamówienia</th>
        <th>Zamawiający</th>
        <th>Adres dostawy</th>
        <th>Status</th>
        <th>Produkty</th>
        <th>data dodania</th>
        <th>data modyfikacji</th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    {foreach $subordinates_orders as $order}
        <tr>
            <td>{$order->id}</td>
            <td>{$order->user->name} {$order->user->surname}</td>
            <td>
                <ul style="list-style: none;">
                    <li>{$order->address->recipent_company}</li>
                    <li>{$order->address->recipent_name} {$order->address->recipent_surname}</li>
                    <li>{$order->address->street} {$order->address->street_number}{if !empty($order->address->apartment_number)}/{$order->address->apartment_number}{/if}</li>
                    <li>{$order->address->post_code} {$order->address->city}</li>
                    <li>{$order->address->telephone}</li>
                    <li>{$order->address->email}</li>
                </ul>
            </td>
            <td>{$order->status_text}</td>
            <td>
                {if !empty($order->products_array)}
                    <ul style="list-style: none;">
                        {foreach $order->products_array as $product}
                            <li>{$product->prize->name}, {$product->prize_variant->title}</li>
                        {/foreach}
                    </ul>
                {/if}
            </td>
            <td>{if $order->created_at}{$order->created_at->format('Y-m-d H:i:s')}{else}-{/if}</td>
            <td>{if $order->updated_at}{$order->updated_at->format('Y-m-d H:i:s')}{else}-{/if}</td>
            <td class="text-right">
                <a href="{BASE_ED_URL}order/show.html?id={$order->id}" class='btn btn-primary text-white'>podgląd</a>
            </td>
        </tr>
    {/foreach}
    </tbody>
</table>
	
