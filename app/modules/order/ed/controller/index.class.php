<?php

use App\modules\order\ed\collections\Get_order_statuses;
use App\modules\order\ed\collections\Get_order_statuses_log;
use App\modules\order\ed\collections\Get_subordinates_orders;
use App\modules\order\ed\models\Order_model;
use App\modules\order\ed\objects\Change_order_status;

class App__Modules__Order__Ed__Controller__Index extends Lib__Base__Ed_Controller
{
    public function action_index()
    {
        $subordinates_orders = Get_subordinates_orders::get_with_filters($this->_user->id, empty($_POST) ? [] : $_POST);

        $statuses = Get_order_statuses::get_all((int)$this->session['id_service'], (int)$this->session['id_lang']);

        $this->view->assign('subordinates_orders', $subordinates_orders);

        $this->view->assign('statuses', $statuses);

        $this->view->assign('is_admin', App__Ed__Model__Acl::has_group('admin-electrolux', 0));

        $this->template = get_module_template_path('order') . 'orders.tpl';

        $this->breadcrumb = [
            'order' => 'Zamówienia',
        ];
    }

    public function action_show()
    {
        if (!empty($_GET['id']))
        {
            $order_data = Get_subordinates_orders::get($this->_user->id, $_GET['id']);

            $order_statuses = Get_order_statuses::get_all($this->session['id_service'], $this->session['id_lang']);

            if (!empty($order_data))
            {
                $order = $order_data[0];

                $order_status_log = Get_order_statuses_log::get($order->{Order_model::COLUMN_ID});

                $this->view->assign('order_status_log', $order_status_log);

                $this->view->assign('order_statuses', $order_statuses);

                $this->view->assign('order', $order);

                $this->template = get_module_template_path('order') . 'order.tpl';

                $this->breadcrumb = [
                    'order'                             => 'Zamówienia',
                    'order/show.html?id=' . $_GET['id'] => 'Zamówienie nr ' . $_GET['id'],
                ];
            }
            else
            {
                go_back('w|Zamówienie nie istnieje.');
            }
        }
        else
        {
            go_back('w|Brak dostępu.');
        }
    }

    public function action_change_order_status()
    {
        if (!empty($_POST))
        {
            $change_status = new Change_order_status((int)$_POST['id_order'], $_POST['order_new_status'] ?? null);

            go_back(
                $change_status->change_status(
                    Get_order_statuses::get_all((int)$this->session['id_service'], (int)$this->session['id_lang'])) ?
                        'g|Status został zapisany.' :
                        'w|Status nie został zapisany.'
            );
        }
        else
        {
            go_back('w|Brak dostępu.');
        }
    }
}
