<?php
declare(strict_types=1);

namespace App\modules\order\ed\models;

use Lib__Base__Model;

class Order_model extends Lib__Base__Model
{
    const COLUMN_ID = 'id';
    const COLUMN_ID_USER = 'id_user';
    const COLUMN_ID_ADDRESS = 'id_address';
    const COLUMN_STATUS = 'status';
    const COLUMN_PRODUCTS = 'products';
    const COLUMN_DATE_REALIZATION = 'date_realization';
    const COLUMN_CREATED_AT = 'created_at';
    const COLUMN_UPDATED_AT = 'updated_at';

    const TABLE_NAME = 'order';

    static $table_name = self::TABLE_NAME;
    static $primary_key = self::COLUMN_ID;

    const STATUS_NOT_SUBMITTED = 0;
    const STATUS_NEW = 1;
    const STATUS_IN_PROGRESS = 2;
    const STATUS_COMPLETED = 3;
    const STATUS_CANCELLED = 4;
}