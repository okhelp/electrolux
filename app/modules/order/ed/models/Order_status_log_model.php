<?php
declare(strict_types=1);

namespace App\modules\order\ed\models;

use Lib__Base__Model;

class Order_status_log_model extends Lib__Base__Model
{
    const COLUMN_ID            = 'id';
    const COLUMN_ID_ORDER      = 'id_order';
    const COLUMN_STATUS_BEFORE = 'status_before';
    const COLUMN_STATUS_AFTER  = 'status_after';
    const COLUMN_CREATED_AT    = 'created_at';

    const TABLE_NAME = 'order_status_log';

    static $table_name = self::TABLE_NAME;
    static $primary_key = self::COLUMN_ID;
}