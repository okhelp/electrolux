<?php
declare(strict_types=1);

namespace App\modules\order\ed\models;

use Lib__Base__Model;

class Order_status extends Lib__Base__Model
{
    const COLUMN_ID                = 'id';
    const COLUMN_IS_DEFAULT        = 'is_default';
    const COLUMN_SEND_NOTIFICATION = 'send_notification';
    const COLUMN_CREATED_AT        = 'created_at';
    const COLUMN_UPDATED_AT        = 'updated_at';
    const COLUMN_DELETED_AT        = 'deleted_at';
    const COLUMN_ORDER_STATUS_ID   = 'order_status_id';
    const COLUMN_NAME              = 'name';
    const COLUMN_NOTIFICATION_TEXT = 'notification_text';
    const COLUMN_ID_LANG           = 'id_lang';
    const COLUMN_ID_SERVICE        = 'id_service';

    const TABLE_NAME = 'order_status';

    static $table_name = self::TABLE_NAME;
    static $primary_key = self::COLUMN_ID;
}