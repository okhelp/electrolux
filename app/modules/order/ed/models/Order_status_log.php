<?php
declare(strict_types=1);

namespace App\modules\order\ed\models;

class Order_status_log
{
    /** @var ?Order_model */
    private $order;

    /** @var array */
    public $order_statuses;

    /**
     * Order_status_log constructor.
     * @param Order_model|null $order
     * @param array $order_statuses
     */
    public function __construct(?Order_model $order, array $order_statuses)
    {
        $this->order_statuses = $order_statuses;

        $this->order = $order;
    }

    public function add(Order_model $order): bool
    {
        $status_before = empty($this->order) ? 0 : $this->order->{Order_model::COLUMN_STATUS};

        $status_after = $order->{Order_model::COLUMN_STATUS};

        $order_status_log = new Order_status_log_model();
        $order_status_log->{Order_status_log_model::COLUMN_ID_ORDER} = $order->{Order_model::COLUMN_ID};
        $order_status_log->{Order_status_log_model::COLUMN_STATUS_BEFORE} = $status_before;
        $order_status_log->{Order_status_log_model::COLUMN_STATUS_AFTER} = $status_after;

        return $order_status_log->save();
    }
}