<?php
declare(strict_types=1);

namespace App\modules\order\ed\models;

use Lib__Base__Model;

class Order_address_model extends Lib__Base__Model
{
    const COLUMN_ID               = 'id';
    const COLUMN_RECIPENT_NAME    = 'recipent_name';
    const COLUMN_RECIPENT_SURNAME = 'recipent_surname';
    const COLUMN_RECIPENT_COMPANY = 'recipent_company';
    const COLUMN_STREET           = 'street';
    const COLUMN_STREET_NUMBER    = 'street_number';
    const COLUMN_APARTMENT_NUMBER = 'apartment_number';
    const COLUMN_CITY             = 'city';
    const COLUMN_POST_CODE        = 'post_code';
    const COLUMN_TELEPHONE        = 'telephone';
    const COLUMN_EMAIL            = 'email';
    const COLUMN_CREATED_AT       = 'created_at';
    const COLUMN_UPDATED_AT       = 'updated_at';

    const TABLE_NAME = 'order_address';

    static $table_name = self::TABLE_NAME;
    static $primary_key = self::COLUMN_ID;
}