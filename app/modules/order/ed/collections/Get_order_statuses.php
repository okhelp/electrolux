<?php
declare(strict_types=1);

namespace App\modules\order\ed\collections;

use App\modules\order\ed\models\Order_status;
use Lib__Memcache;

class Get_order_statuses
{
    public static function get_all(int $id_service, int $id_lang, bool $update_cache = false): array
    {
        $cache_key = "order_statuses-$id_service-$id_lang";

        if(!$update_cache)
        {

            $cache_data = Lib__Memcache::get($cache_key);

            if ($cache_data)
            {
                return unserialize($cache_data);
            }
        }

        $statuses_to_return = [];

        $statuses = Order_status::find_by_sql(
            "
            SELECT *
            FROM order_status
            JOIN order_status_lang ON order_status.id=order_status_lang.order_status_id
            WHERE order_status_lang.id_service = $id_service AND order_status_lang.id_lang = $id_lang AND order_status.deleted_at IS NULL
        ");

        if (!empty($statuses))
        {
            foreach ($statuses as $status)
            {
                $statuses_to_return[$status->{Order_status::COLUMN_ID}] = $status;
            }
        }

        Lib__Memcache::set(
            $cache_key, serialize($statuses_to_return), [
            'class_name' => get_class(),
            'key_name'   => $cache_key,
        ]);

        return $statuses_to_return;
    }

    public static function get_by_status_id(int $id_status): ?Order_status
    {
        $status_to_return = null;

        $status = Order_status::find($id_status);

        if(!empty($status))
        {
            $status_to_return = $status;
        }

        return $status_to_return;
    }
}