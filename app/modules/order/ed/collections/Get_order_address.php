<?php
declare(strict_types=1);

namespace App\modules\order\ed\collections;

use App\modules\order\ed\models\Order_address_model;

class Get_order_address
{
    public static function get(array $addresses_ids): array
    {
        $addresses_to_return = [];

        if (!empty($addresses_ids))
        {
            $addresses = Order_address_model::find_by_sql(
                "
                SELECT *
                FROM order_address
                WHERE id IN (" . implode(',', $addresses_ids) . ")
            ");

            if (!empty($addresses))
            {
                foreach ($addresses as $address)
                {
                    $addresses_to_return[$address->id] = $address;
                }
            }
        }

        return $addresses_to_return;
    }
}