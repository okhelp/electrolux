<?php
declare(strict_types=1);

namespace App\modules\order\ed\collections;

use App\ed\model\company\Get_company_users;
use App\ed\model\users\Get_by_ids;
use App\ed\model\users\Get_subordinate_companies;
use App\modules\order\ed\models\Order_model;
use App\modules\order\ed\models\Order_status;
use App\modules\prizes\ed\models\Get_prizes_images;
use App\modules\prizes\front\collections\Get_prize_variants;
use App\modules\prizes\front\objects\Get_prizes;
use App__Ed__Model__Acl;

class Get_subordinates_orders
{
    public static function get(int $id_supervisor, ?int $id_order = null): array
    {
        $orders_to_return = [];

        $subordinates_companies_ids = Get_subordinate_companies::get([$id_supervisor]);

        $subordinates_users = Get_company_users::get_by_ids($subordinates_companies_ids);

        $subordinates_companies_users_ids = [];
        if (!empty($subordinates_users))
        {
            foreach ($subordinates_users as $companies)
            {
                foreach ($companies as $company_user)
                {
                    $subordinates_companies_users_ids[] = $company_user->id;
                }
            }
        }

        if (!empty($subordinates_companies_users_ids))
        {
            $id_order_condition = empty($id_order) ? '' : ' AND id=' . $id_order . ' ';
            $orders = Order_model::find_by_sql(
                "
                    SELECT *
                    FROM " . DB_NAME . ".order
                    WHERE 
                        /* id_user IN (" . implode(',', $subordinates_companies_users_ids) . ") AND */
                         status != " . Order_model::STATUS_NOT_SUBMITTED . "
                        $id_order_condition
                ");

            if (!empty($orders))
            {
                foreach ($orders as $order)
                {
                    $order->assign_attribute('products_array', json_decode($order->{Order_model::COLUMN_PRODUCTS}));
                    $orders_to_return[] = $order;
                }

                $prizes_ids = [];
                $prizes_variants_ids = [];
                $orders_addresses_ids = [];
                $users_ids = [];




                foreach ($orders as $order)
                {
                    $orders_addresses_ids[] = $order->id_address;
                    $users_ids[] = $order->id_user;

                    foreach ($order->products_array as $product)
                    {
                        $prizes_ids[] = $product->id_prize;
                        $prizes_variants_ids[] = $product->id_prize_option;
                    }
                }

                $prizes_photos = Get_prizes_images::get(array_unique($prizes_ids));
		
                $prizes = Get_prizes::by_ids(array_unique($prizes_ids));
                $prizes_variants = Get_prize_variants::get_by_ids(array_unique($prizes_ids));
                $orders_addresses = Get_order_address::get(array_unique($orders_addresses_ids));
                $users = Get_by_ids::get(array_unique($users_ids));
                $order_statuses = Get_order_statuses::get_all((int)$_SESSION['id_service'], (int)$_SESSION['id_lang']);

                foreach ($orders as $order)
                {
                    $order->assign_attribute('address', $orders_addresses[$order->id_address]);
                    $order->assign_attribute('user', $users[$order->id_user]);

                    $order = self::assign_status($order, $order_statuses);

                    foreach ($order->products_array as $product)
                    {
                        $product->prize = $prizes[$product->id_prize];
                        $product->prize->assign_attribute('photos', empty($prizes_photos[$product->id_prize]) ? [] : $prizes_photos[$product->id_prize]);
                        $product->prize_variant = $prizes_variants[$product->id_prize][$product->id_prize_option];
                    }
                }

                $orders_to_return = $orders;
            }
        }

        return $orders_to_return;
    }

    private static function assign_status(Order_model $order, array $order_statuses): Order_model
    {
        $order->assign_attribute(
            'status_text',
            empty($order_statuses[$order->{Order_model::COLUMN_STATUS}]) ? 'Brak statusu' : $order_statuses[$order->{Order_model::COLUMN_STATUS}]->{Order_status::COLUMN_NAME});

        return $order;
    }

    public static function get_with_filters(int $id_supervisor, array $filters = []): array
    {
        $orders_to_return = [];

        $subordinates_companies_ids = Get_subordinate_companies::get([$id_supervisor]);

        $subordinates_users = Get_company_users::get_by_ids($subordinates_companies_ids);

        $subordinates_companies_users_ids = [];
        if (!empty($subordinates_users))
        {
            foreach ($subordinates_users as $companies)
            {
                foreach ($companies as $company_user)
                {
                    $subordinates_companies_users_ids[] = $company_user->id_user;
                }
            }
        }

        $subordinates_condition = (App__Ed__Model__Acl::has_group('admin-electrolux', 0) || App__Ed__Model__Acl::has_group('premium-user', 0)) ?
            '' : ("AND " . DB_NAME . ".order.id_user IN (" . implode(',', $subordinates_companies_users_ids) . ")");

        if (!empty($subordinates_companies_users_ids))
        {
            $order_id_condition = empty($filters['id']) ? '' : ' AND ' . DB_NAME . '.order.id LIKE "%' . $filters['id'] . '%" ';

            $company_condition = empty($filters['company']) ? '' : ' AND company.name LIKE "%' . $filters['company'] . '%" ';

            $date_from_condition = empty($filters['date_from']) ? '' : ' AND ' . DB_NAME . '.order.created_at > "' . $filters['date_from'] . '"';

            $date_to_condition = empty($filters['date_to']) ? '' : ' AND ' . DB_NAME . '.order.created_at < "' . $filters['date_to'] . '"';

            $administrator_condition = empty($filters['superior']) ? '' : ' AND (SELECT CONCAT(supervisor.`name`, "", supervisor.`surname`) FROM `users` supervisor WHERE `company_supervisor`.`id_supervisor` = `users`.`id` LIMIT 1) LIKE "%' . $filters['superior'] . '%" ';

            $status_condition = empty($filters['status']) ?
                (DB_NAME . ".order.status != " . Order_model::STATUS_NOT_SUBMITTED) : (DB_NAME . ".order.status={$filters['status']} ");

            $orders = Order_model::find_by_sql(
                "
                    SELECT 
                        " . DB_NAME . ".order.*
                    FROM " . DB_NAME . ".order
                        JOIN `users` ON `users`.`id` = " . DB_NAME . ".order.id_user
                        JOIN `company_users` ON `users`.`id` = `company_users`.`id_user`
                        JOIN `company` ON `company_users`.`id_company`=`company`.`id`
                        JOIN `company_supervisor` ON `company_supervisor`.`id_company` = `company_users`.`id_company`
                    WHERE $status_condition
                        $subordinates_condition
                        $order_id_condition
                        $company_condition
                        $date_from_condition
                        $date_to_condition
                        $administrator_condition
                    GROUP by " . DB_NAME ." .order.id
                ");

            if (!empty($orders))
            {
                foreach ($orders as $order)
                {
                    $order->assign_attribute('products_array', json_decode($order->{Order_model::COLUMN_PRODUCTS}));
                    $orders_to_return[] = $order;
                }

                $prizes_ids = [];
                $prizes_variants_ids = [];
                $orders_addresses_ids = [];
                $users_ids = [];

                $prizes_photos = Get_prizes_images::get($prizes_ids);

                foreach ($orders as $order)
                {
                    $orders_addresses_ids[] = $order->id_address;
                    $users_ids[] = $order->id_user;

                    foreach ($order->products_array as $product)
                    {
                        $prizes_ids[] = $product->id_prize;
                        $prizes_variants_ids[] = $product->id_prize_option;
                    }
                }

                $prizes = Get_prizes::by_ids(array_unique($prizes_ids));
                $prizes_variants = Get_prize_variants::get_by_ids(array_unique($prizes_ids));
                $orders_addresses = Get_order_address::get(array_unique($orders_addresses_ids));
                $users = Get_by_ids::get(array_unique($users_ids));
                $order_statuses = Get_order_statuses::get_all((int)$_SESSION['id_service'], (int)$_SESSION['id_lang']);

                foreach ($orders as $index => $order)
                {
                    $order->assign_attribute('address', $orders_addresses[$order->id_address]);
                    $order->assign_attribute('user', $users[$order->id_user]);

                    $order = self::assign_status($order, $order_statuses);

                    foreach ($order->products_array as $product)
                    {
                        if(!empty($prizes[$product->id_prize]))
                        {
                            $product->prize = $prizes[$product->id_prize];
                            $product->prize->assign_attribute('photos', empty($prizes_photos[$product->id_prize]) ? [] : $prizes_photos[$product->id_prize]);
                            $product->prize_variant = $prizes_variants[$product->id_prize][$product->id_prize_option];
                        }
                        else
                        {
                            unset($orders[$index]);
                        }
                    }
                }

                $orders_to_return = $orders;
            }
        }

        return $orders_to_return;
    }
}
