<?php
declare(strict_types=1);

namespace App\modules\order\ed\collections;

use App\modules\order\ed\models\Order_status;
use App\modules\order\ed\models\Order_status_log_model;

class Get_order_statuses_log
{
    public static function get(int $id_order): array
    {
        $order_statuses_log_to_return = [];

        $order_statuses_log = Order_status_log_model::find_by_sql(
            "
            SELECT *
            FROM order_status_log
            WHERE id_order=$id_order
            ORDER BY created_at
        ");

        if (!empty($order_statuses_log))
        {
            $order_statuses = Get_order_statuses::get_all((int)$_SESSION['id_service'], (int)$_SESSION['id_lang']);

            foreach ($order_statuses_log as $order_status_log)
            {
                $order_status_log->assign_attribute(
                    'status_before_text', empty($order_status_log->{Order_status_log_model::COLUMN_STATUS_BEFORE}) ? '' :
                    $order_statuses[$order_status_log->{Order_status_log_model::COLUMN_STATUS_BEFORE}]->{Order_status::COLUMN_NAME});

                $order_status_log->assign_attribute(
                    'status_after_text', $order_statuses[$order_status_log->{Order_status_log_model::COLUMN_STATUS_AFTER}]->{Order_status::COLUMN_NAME});

                $order_statuses_log_to_return[] = $order_status_log;
            }
        }

        return $order_statuses_log_to_return;
    }
}