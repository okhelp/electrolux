<?php
declare(strict_types=1);

namespace App\modules\order\front\objects;

use ActiveRecord\ConnectionManager;
use App\modules\order\ed\models\Order_address_model;
use App\modules\order\ed\models\Order_model;
use App\modules\order\front\collections\Get_not_submitted_order;
use Exception;

class Save_order_address
{
    /** @var array */
    private $address_data;

    /** @var int */
    private $id_user;

    /** @var Order_model */
    private $order;

    /** @var Order_address_model */
    private $address;

    /**
     * Save_order_address constructor.
     * @param array $address_data
     * @param int $id_user
     */
    public function __construct(array $address_data, int $id_user)
    {
        $this->address_data = $address_data;
        $this->id_user = $id_user;
    }

    public function save(): bool
    {
        $connection = ConnectionManager::get_connection();
        $connection->transaction();

        try
        {
            $this->get_order();
            $this->updated_order_address_data();
            $this->create_order_if_not_exists();

            $connection->commit();

            return true;
        }
        catch(Exception $exception)
        {
            $connection->rollback();

            return false;
        }
    }

    private function get_order()
    {
        $order = Get_not_submitted_order::get($this->id_user);

        $this->order = empty($order) ? [] : $order;
    }

    private function create_order_if_not_exists()
    {
        $order = empty($this->order) ? new Order_model() : Order_model::find($this->order->{Order_model::COLUMN_ID});
        $order->{Order_model::COLUMN_ID_USER} = $this->id_user;
        $order->{Order_model::COLUMN_ID_ADDRESS} = $this->address->{Order_address_model::COLUMN_ID};
        $order->{Order_model::COLUMN_STATUS} = Order_model::STATUS_NOT_SUBMITTED;

        if(!$order->save())
        {
            throw new Exception('Nie udało się zapisać danych zamówienia.');
        }

        $this->order = $order;
    }

    private function updated_order_address_data()
    {
        $address = empty($this->order) ? new Order_address_model() : Order_address_model::find($this->order->{Order_model::COLUMN_ID_ADDRESS});
        $address->{Order_address_model::COLUMN_RECIPENT_NAME} = $this->address_data['cart_delivery_name'];
        $address->{Order_address_model::COLUMN_RECIPENT_SURNAME} = $this->address_data['cart_delivery_lname'];
        $address->{Order_address_model::COLUMN_RECIPENT_COMPANY} = $this->address_data['cart_delivery_firm'];
        $address->{Order_address_model::COLUMN_STREET} = $this->address_data['cart_delivery_street'];
        $address->{Order_address_model::COLUMN_STREET_NUMBER} = $this->address_data['cart_delivery_street_number'];
        $address->{Order_address_model::COLUMN_APARTMENT_NUMBER} = $this->address_data['cart_delivery_street_local'];
        $address->{Order_address_model::COLUMN_CITY} = $this->address_data['cart_delivery_city'];
        $address->{Order_address_model::COLUMN_POST_CODE} = $this->address_data['cart_delivery_street_zipCode'];
        $address->{Order_address_model::COLUMN_TELEPHONE} = $this->address_data['cart_delivery_telephone'];
        $address->{Order_address_model::COLUMN_EMAIL} = $this->address_data['cart_delivery_email'];

        if(!$address->save())
        {
            throw new Exception('Nie udało się zapisać danych adresowych zamówienia.');
        }

        $this->address = $address;
    }
}