<?php
declare(strict_types=1);

namespace App\modules\order\front\objects;

use App\ed\model\mail\Add_to_queue;
use App\ed\model\points\Get_users_points;
use App\front\models\user\User_points;
use App\modules\cart\front\collections\Get_cart;
use App\modules\cart\front\objects\Cart;
use App\modules\cart\front\objects\Verify_cart;
use App\modules\order\ed\collections\Get_order_statuses;
use App\modules\order\ed\models\Order_model;
use App\modules\order\ed\models\Order_status_log;
use App\modules\order\ed\objects\Email_order_status_changed;
use App\modules\order\front\collections\Get_not_submitted_order_with_address;
use App\modules\prizes\ed\models\Prize_model;
use App__Ed__Model__Users;
use Exception;
use Lib__Session;
use Smarty;

class Submit_order
{
    /** @var int */
    private $id_order;

    /** @var int */
    private $id_user;

    /** @var ?Order_model */
    private $order;

    /** @var array */
    private $cart;

    /** @var string */
    const MIN_POINTS = 500;

    /**
     * Submit_order constructor.
     * @param int $id_order
     * @param int $id_user
     * @throws \ActiveRecord\RecordNotFound
     */
    public function __construct(int $id_order, int $id_user)
    {
        $this->id_order = $id_order;
        $this->id_user = $id_user;

        $this->order = Order_model::find($id_order);
    }

    /**
     * @throws Exception
     */
    public function submit()
    {
        $this->order_exists();
        $this->order_belongs_to_user();
        $this->validate_cart();
        $this->validate_delivery();
        $this->save_order_data();
        $this->sub_user_points();
        $this->remove_products_form_cart();
    }

    /**
     * @throws Exception
     */
    private function order_exists()
    {
        if (empty($this->order))
        {
            throw new Exception('Nie odnaleziono zamówienia.');
        }
    }

    /**
     * @throws Exception
     */
    private function order_belongs_to_user()
    {
        if ($this->id_user != $this->order->id_user)
        {
            throw new Exception('Zamówienie nie należy do użytkownika.');
        }
    }

    /**
     * @throws Exception
     */
    private function validate_cart()
    {
        $verify_cart = new Verify_cart($this->id_user);
        $verify_cart->verify();

        $this->cart = Get_cart::get($this->id_user);
    }

    /**
     * @throws Exception
     */
    private function validate_delivery()
    {
        $not_submitted_order = Get_not_submitted_order_with_address::get($this->id_user);

        if (empty($not_submitted_order->address))
        {
            throw new Exception('Nieprawidłowe dane dostawy zamówienia.');
        }
        else
        {
            $this->order = $not_submitted_order;
        }
    }

    /**
     * @throws \ActiveRecord\RecordNotFound
     * @throws Exception
     */
    private function save_order_data()
    {
        $order = Order_model::find($this->id_order);

        $order_status_log = new Order_status_log(null, Get_order_statuses::get_all((int)$_SESSION['id_service'], (int)$_SESSION['id_lang']));

        $cart_prizes = [];
        foreach ($this->cart as $cart_item)
        {
            $cart_prizes[] = [
                'id_prize'        => $cart_item->id_prize,
                'id_prize_option' => $cart_item->id_prize_option,
                'count'           => $cart_item->count,
                'price'           => $cart_item->price,
            ];
        }

        $order->{Order_model::COLUMN_STATUS} = Order_model::STATUS_NEW;
        $order->{Order_model::COLUMN_PRODUCTS} = json_encode($cart_prizes);

        if (!$order->save())
        {
            throw new Exception('Nie zapisano danych zamówienia.');
        }

        $points = \App__Ed__Model__Points_log::getPoints($this->id_user);

        Lib__Session::set(['user_points' => $points]);

        $smarty = new Smarty();
        $smarty->assign('id_order', $this->id_order);

        $user = App__Ed__Model__Users::find($order->{Order_model::COLUMN_ID_USER});

        if (!empty($user->email))
        {
            $add_to_queue = new Add_to_queue(
                $user->email,
                'Zamówienie złożone',
                $smarty->fetch(get_app_template_path() . '_emails/zamowienie_zlozone.tpl')
            );

            $add_to_queue->add();

            $notify = new Email_order_status_changed($order);
            $notify->send();
            $order_status_log->add($order);
        }
    }

    /**
     * @throws Exception
     */
    private function sub_user_points()
    {
        User_points::sub($this->id_user, Cart::sum_cart_products_standard($this->cart));
        User_points::sub_annual($this->id_user, Cart::sum_cart_products_annual($this->cart));
    }

    private function remove_products_form_cart()
    {
        Cart::remove_products_form_cart_after_order($this->id_user);
    }
}