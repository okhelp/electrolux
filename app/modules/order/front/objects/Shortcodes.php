<?php
declare(strict_types=1);

namespace App\modules\order\front\objects;

use ActiveRecord\DateTime;
use App\modules\order\ed\models\Order_model;
use App\modules\order\ed\models\Order_status;
use App__Ed__Model__Users;

class Shortcodes
{
    public static function shortcode_list()
    {
        return [
            '%%data%%',
            '%%data_realizacji%%',
            '%%imie%%',
            '%%nazwisko%%',
            '%%email%%',
            '%%nazwa_uzytkownika%%',
            '%%telefon%%',
            '%%numer_zamowienia%%',
            '%%nazwa_statusu%%',
        ];
    }

    public static function parse($string, App__Ed__Model__Users $user, Order_status $status, Order_model $order): string
    {
        $shortcodes = self::shortcode_list();

        foreach ($shortcodes as $shortcode)
        {
            if (strpos($string, $shortcode) !== false)
            {
                $parse_method_name = 'parse_' . str_replace('%', '', $shortcode);

                $string = str_replace($shortcode, self::{$parse_method_name}($user, $status, $order) ?? '', $string);
            }
        }

        return $string;
    }

    private static function parse_data(App__Ed__Model__Users $user, Order_status $status, Order_model $order): string
    {
        return (new DateTime('now'))->format('d-m-Y H:i:s');
    }

    private static function parse_nazwa_uzytkownika(App__Ed__Model__Users $user, Order_status $status, Order_model $order): ?string
    {
        return "$user->name $user->surname";
    }

    private static function parse_imie(App__Ed__Model__Users $user, Order_status $status, Order_model $order): ?string
    {
        return $user->name;
    }

    private static function parse_nazwisko(App__Ed__Model__Users $user, Order_status $status, Order_model $order): ?string
    {
        return $user->surname;
    }

    private static function parse_email(App__Ed__Model__Users $user, Order_status $status, Order_model $order): ?string
    {
        return $user->email;
    }

    private static function parse_telefon(App__Ed__Model__Users $user, Order_status $status, Order_model $order): ?string
    {
        return $user->telephone;
    }

    private static function parse_nazwa_statusu(App__Ed__Model__Users $user, Order_status $status, Order_model $order): ?string
    {
        return $status->name;
    }

    private static function parse_numer_zamowienia(App__Ed__Model__Users $user, Order_status $status, Order_model $order): ?int
    {
        return $order->id;
    }

    private static function parse_data_realizacji(App__Ed__Model__Users $user, Order_status $status, Order_model $order): ?string
    {
        return !empty($order->date_realization) ? $order->date_realization->format('d-m-Y') : '';
    }
}