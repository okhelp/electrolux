<?php
declare(strict_types=1);

namespace App\modules\order\front\collections;

use App\modules\order\ed\models\Order_address_model;
use App\modules\order\ed\models\Order_model;

class Get_not_submitted_order_with_address
{
    public static function get(int $id_user): ?Order_model
    {
        $order = Get_not_submitted_order::get($id_user);

        if (!empty($order))
        {
            $order->assign_attribute('address', Order_address_model::find($order->{Order_model::COLUMN_ID_ADDRESS}));

            return $order;
        }

        return null;
    }

}