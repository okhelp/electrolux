<?php
declare(strict_types=1);

namespace App\modules\order\front\collections;

use App\modules\order\ed\models\Order_model;
use App\modules\prizes\front\collections\Get_prize_variants;
use App\modules\prizes\front\objects\Get_prizes;

class Get_user_prizes_earned
{
    public static function get(int $id_user, int $start = 0, int $limit = 9999, string $search_value = '', string $order_column = '', string $order_type = 'asc'): array
    {
        $prizes_to_return = [];

        $order_condition = '';
        if(!empty($order_column))
        {
            $order_condition = " ORDER BY $order_column $order_type ";
        }

        $search_condition = '';
        if(!empty($search_value))
        {
            $search_condition = " AND (id LIKE '%$search_value%' OR created_at LIKE '%$search_value%')";
        }

        $orders = Order_model::find_by_sql("
            SELECT *
            FROM " . DB_NAME . ".order
            WHERE id_user=$id_user AND status!=" . Order_model::STATUS_NOT_SUBMITTED . "
            $search_condition
            $order_condition
            LIMIT $start, $limit
        ");

        if(!empty($orders))
        {
            $prizes_ids = [];
            $prizes_variants_ids = [];

            foreach ($orders as &$order)
            {
                $order->assign_attribute('products_decoded', json_decode($order->products));

                if(!empty($order->products_decoded))
                {
                    foreach ($order->products_decoded as $prize)
                    {
                        $prizes_ids[] = $prize->id_prize;
                        $prizes_variants_ids[] = $prize->id_prize_option;
                    }
                }
            }

            $prizes = Get_prizes::by_ids(array_unique($prizes_ids));

            $prizes_variants = Get_prize_variants::get_by_ids(array_unique($prizes_ids));

            foreach ($orders as &$order)
            {
                if(!empty($order->products_decoded))
                {
                    foreach ($order->products_decoded as $prize)
                    {
                        $prize->prize = empty($prizes[$prize->id_prize]) ? [] : $prizes[$prize->id_prize];
                        $prize->prize_option = empty($prizes_variants[$prize->id_prize][$prize->id_prize_option]) ? [] : $prizes_variants[$prize->id_prize][$prize->id_prize_option];
                    }
                }
            }

            $prizes_to_return = $orders;
        }

        return $prizes_to_return;
    }
}