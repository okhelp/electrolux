<?php

namespace App\modules\order\front\collections;

use App\modules\order\ed\models\Order_model;

class Get_not_submitted_order
{
    public static function get(int $id_user): ?Order_model
    {
        $order_to_return = null;

        if (!empty($id_user))
        {
            $order = Order_model::find_by_sql(
                "
                SELECT *
                FROM " . DB_NAME . ".order
                WHERE " . DB_NAME . ".order.id_user=$id_user
                AND " . DB_NAME . ".order.status=" . Order_model::STATUS_NOT_SUBMITTED . "
            ");

            if (!empty($order))
            {
                $order_to_return = $order[0];
            }
        }

        return $order_to_return;
    }
}