<?php

require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class App__Modules__Statistic__Ed__Objects__Generate_Excel
{
    /** @var array  */
    protected $_params = [];

    /** @var Spreadsheet  */
    protected $_spreadsheet;

    /** @var array  */
    const TYPES_LIST = [
        'registered_users'          => 'Aktywni użytkownicy',
        'invited_users'             => 'Zaproszeni użytkownicy',
        'not_registered_users'      => 'Nieaktywni użytkownicy',
        'sale'                      => 'Zarejestrowana sprzedaż',
        'accepted_sale'             => 'Potwierdzona sprzedaż',
        'not_accepted_sale'         => 'Odrzucona sprzedaż',
        'users_history_logged'      => 'Historia logowań użytkowników',
        'number_of_login'           => 'Ilość logowań',
        'e_luxy'                    => 'E-LUXY',
    ];

    /** @var array  */
    const TEST_COMPANIES = [1,956,957,958,959,960,961,962,963,964,965];

    public function __construct(array $data)
    {
        $this->_params = $data;
        $this->_spreadsheet = new Spreadsheet();
    }

    /**
     * @param $sheet_name
     * @return null|string
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    private function add_sheet($sheet_name)
    {
        $sheet = new \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet(
            $this->_spreadsheet,
            self::TYPES_LIST[$sheet_name]
        );
        $this->_spreadsheet->addSheet($sheet);

        return $sheet->getCodeName();
    }

    /**
     * @param $field
     * @return null|string
     */
    private function set_date_where($field)
    {
        $where = [];

        if(!empty($this->_params['date_from']))
        {
            $where[] = "$field >= '" . $this->_params['date_from'] . " 00:00:00'";
        }

        if(!empty($this->_params['date_to']))
        {
            $where[] = "$field <= '" . $this->_params['date_to'] . " 23:59:59'";
        }

        return !empty($where) ? implode(" AND ", $where) : null;
    }

    private function set_sheet_row($sheet_code_name, $data)
    {
        $sheet = $this->_spreadsheet->getSheetByCodeName($sheet_code_name);

        $alphabet = range('A','Z');
        foreach($data as $index => $row_item)
        {
            foreach($row_item as $k => $v)
            {
                $word = $alphabet[$k];
                $column_name = $word . ($index+1);
                $sheet->setCellValue($column_name, $v);

                //ustawiam szerokość kolumn
                $this->_spreadsheet->getSheetByCodeName($sheet_code_name)->getColumnDimension($word)->setAutoSize(true);
            }
        }

        return $sheet;
    }

    /**
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    public function create_registered_users()
    {
        //tworzę nowy arkusz
        $sheet_name = str_replace('create_', '',
            $this->_where_date = __FUNCTION__);
        $sheet_code_name = $this->add_sheet($sheet_name);


        //pobieram warnek daty
        $where_date = $this->set_date_where("cu.updated_at");
        $where_date = !empty($where_date) ? " AND " .$where_date : '';

        //pobieram warunek dla firm
        $where_companies = " AND c.id NOT IN(" . implode(',', self::TEST_COMPANIES) . ")";

        //pobieram dane
        $sql = "
            SELECT u.id_electrolux, ag.name as rola, u.login, u.name, u.surname, c.name as company_name, c.nip,
              ca.post_code as firma_kod_pocztowy, ca.city as firma_miasto, ca.street as firma_ulica, u.telephone, u.email,
              u3.login as login_zapraszajacego, c2.name as firma_zapraszajacego, u3.name as imie_zapraszajacego, u3.surname as nazwisko_zapraszajacego,
              cup.points, cup.extra_points, 
              u2.name as handlowiec_imie, u2.surname as handlowiec_nazwisko, u2.email as handlowiec_email
            FROM users u 
            LEFT JOIN company_users_points cup ON cup.company_user_id = u.id
            INNER JOIN company_users cu ON cu.id_user = u.id
            LEFT JOIN company c ON c.id = cu.id_company
            LEFT JOIN company_supervisor cs ON cs.id_company = c.id
            LEFT JOIN users u2 ON u2.id = cs.id_supervisor
            LEFT JOIN company_address ca ON ca.id_company = c.id AND ca.is_main = 1
            JOIN acl_group_user agu ON agu.id_user=u.id
            JOIN acl_group ag ON ag.id=agu.id_group
            LEFT JOIN confirmed_co_workers ccw ON ccw.id_user = u.id 
            LEFT JOIN users u3 ON u3.id = ccw.confirmed_by
            LEFT JOIN company_users cu2 ON cu2.id_user = u3.id
            LEFT JOIN company c2 ON c2.id = cu2.id_company
            WHERE u.ts_change_password >= '2019-05-06' $where_date $where_companies
        ";
        $list = App__Ed__Model__Users::find_by_sql($sql);

        if(!empty($list))
        {
            $sheet_data = array_merge([0 => [
                'ID ELECTROLUX',
                'Rola',
                'Login',
                'Imię',
                'Nazwisko',
                'Firma',
                'NIP',
                'Kod pocztowy',
                'Miasto',
                'Ulica',
                'Telefon kontaktowy',
                'Email kontaktowy',
                'Login zapraszającego',
                'Firma zapraszającego',
                'Imię zapraszającego',
                'Nazwisko zapraszającego',
                'E-Lux',
                'E-Lux extra',
                'Handlowiec imię',
                'Handlowiec nazwisko',
                'Handlowiec email',
            ]], array_map(function($item){
                return array_values($item->to_array());
            },$list));

            $this->set_sheet_row($sheet_code_name, $sheet_data);
        }
    }

    /**
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    public function create_invited_users()
    {
        //tworzę nowy arkusz
        $sheet_name = str_replace('create_', '', $this->_where_date = __FUNCTION__);
        $sheet_code_name = $this->add_sheet($sheet_name);


        //pobieram warnek daty
        $where_date = $this->set_date_where("ccw.created_at");
        $where_date = !empty($where_date) ? " AND " .$where_date : '';

        //pobieram warunek dla firm
        $where_companies = " AND c.id NOT IN(" . implode(',', self::TEST_COMPANIES) . ")";

        //pobieram dane
        $sql = "
            SELECT u.login, u.name, u.surname, ag.name as rola, cup.points, cup.extra_points,
              c.name as company_name, c.nip, 
              ca.post_code as firma_kod_pocztowy, ca.city as firma_miasto, ca.street as firma_ulica, u.telephone, u.email,
              u3.login as login_zapraszającego, c2.name as firma_zapraszajacego, u3.name as admin_name, u3.surname as admin_surname,
              u2.name as handlowiec_imie, u2.surname as handlowiec_nazwisko, u2.email as confirmed_email
            FROM company_users cu
            INNER JOIN users u ON cu.id_user = u.id
            LEFT JOIN company_users_points cup ON cup.company_user_id = u.id
            LEFT JOIN company c ON c.id = cu.id_company
            INNER JOIN confirmed_co_workers ccw ON ccw.id_user = u.id 
            LEFT JOIN users u2 ON u2.id = ccw.confirmed_by
            INNER JOIN company_users cu2 ON cu2.role = 'admin' AND cu2.id_company = c.id
            INNER JOIN users u3 ON u3.id = cu2.id_user
            LEFT JOIN company_address ca ON ca.id_company = c.id AND ca.is_main = 1
            JOIN acl_group_user agu ON agu.id_user=u.id
            JOIN acl_group ag ON ag.id=agu.id_group
            LEFT JOIN company c2 ON c2.id = cu2.id_company
            WHERE cu.role = 'user' $where_date $where_companies
            GROUP BY u.id
        ";

        $list = App__Ed__Model__Users::find_by_sql($sql);

        if(!empty($list))
        {
            $sheet_data = array_merge([0 => [
                'Login',
                'Imię',
                'Nazwisko',
                'Rola',
                'E-lux',
                'E-lux extra',
                'Firma',
                'NIP',
                'Kod pocztowy',
                'Miasto',
                'Ulica',
                'Telefon kontaktowy',
                'Email kontaktowy',
                'Login zapraszającego',
                'Firma zapraszającego',
                'Imię zapraszającego',
                'Nazwisko zapraszającego',
                'Handlowiec imię',
                'Handlowiec nazwisko',
                'Handlowiec e-mail',
            ]], array_map(function($item){
                return array_values($item->to_array());
            },$list));

            $this->set_sheet_row($sheet_code_name, $sheet_data);
        }
    }


    /**
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    public function create_not_registered_users()
    {
        //tworzę nowy arkusz
        $sheet_name = str_replace('create_', '',
            $this->_where_date = __FUNCTION__);
        $sheet_code_name = $this->add_sheet($sheet_name);

        //pobieram warunek dla firm
        $where_companies = " AND c.id NOT IN(" . implode(',', self::TEST_COMPANIES) . ")";

        //pobieram dane
        $sql = "
            SELECT u.id_electrolux, ag.name as rola, u.login, u.name, u.surname, 
              c.name as company_name, c.nip,
              ca.post_code as firma_kod_pocztowy, ca.city as firma_miasto, ca.street as firma_ulica, u.telephone, u.email,
              u3.login as login_zapraszajacego, c2.name as firma_zapraszajacego, u3.name as imie_zapraszajacego, u3.surname as nazwisko_zapraszajacego,
              u2.name as handlowiec_imie, u2.surname as handlowiec_nazwisko, u2.email as handlowiec_email
            FROM users u 
            INNER JOIN company_users cu ON cu.id_user = u.id AND cu.role = 'admin'
            INNER JOIN company c ON c.id = cu.id_company
            INNER JOIN company_supervisor cs ON cs.id_company = c.id
            INNER JOIN users u2 ON u2.id = cs.id_supervisor
            LEFT JOIN company_address ca ON ca.id_company = c.id AND ca.is_main = 1
            JOIN acl_group_user agu ON agu.id_user=u.id
            JOIN acl_group ag ON ag.id=agu.id_group
            LEFT JOIN confirmed_co_workers ccw ON ccw.id_user = u.id 
            LEFT JOIN users u3 ON u3.id = ccw.confirmed_by
            LEFT JOIN company_users cu2 ON cu2.id_user = u3.id
            LEFT JOIN company c2 ON c2.id = cu2.id_company
            WHERE u.ts_change_password < '2019-05-06' $where_companies
        ";
        $list = App__Ed__Model__Users::find_by_sql($sql);

        if(!empty($list))
        {
            $sheet_data = array_merge([0 => [
                'ID ELECTROLUX',
                'Rola',
                'Login',
                'Imię',
                'Nazwisko',
                'Firma',
                'NIP',
                'Kod pocztowy',
                'Miasto',
                'Ulica',
                'Telefon kontaktowy',
                'Email kontaktowy',
                'Login zapraszającego',
                'Firma zapraszającego',
                'Imię zapraszającego',
                'Nazwisko zapraszającego',
                'Handlowiec imię',
                'Handlowiec nazwisko',
                'Handlowiec email',
            ]], array_map(function($item){
                return array_values($item->to_array());
            },$list));

            $this->set_sheet_row($sheet_code_name, $sheet_data);
        }
    }

    public function create_sale()
    {
        //tworzę nowy arkusz
        $sheet_name = str_replace('create_', '',
            $this->_where_date = __FUNCTION__);
        $sheet_code_name = $this->add_sheet($sheet_name);

        //pobieram warunek dla firm
        $where_companies = "c.id NOT IN(" . implode(',', self::TEST_COMPANIES) . ")";

        //pobieram warnek daty
        $where_date = $this->set_date_where("s.created_at");
        $where_date = !empty($where_date) ? " AND " .$where_date : '';

        //pobieram dane
        $sql = "
            SELECT s.invoice_number, p.id, pl.name, p.model, s.price, s.count, s.points, s.created_at,
              u.id_electrolux, ag.name as rola,
              c.name as company_name, c.nip, 
              ca.post_code as firma_kod_pocztowy, ca.city as firma_miasto, ca.street as firma_ulica, u.telephone, u.email,
              u2.name as handlowiec_imie, u2.surname as handlowiec_nazwisko, u2.email as handlowiec_email
            FROM sale s
            INNER JOIN product p ON p.id = s.id_product
            INNER JOIN product_lang pl ON pl.product_id = p.id
            INNER JOIN users u ON s.id_customer = u.id
            INNER JOIN company_users cu ON cu.id_user = u.id
            INNER JOIN company c ON c.id = cu.id_company
            INNER JOIN company_supervisor cs ON cs.id_company = c.id
            INNER JOIN users u2 ON u2.id = cs.id_supervisor
            LEFT JOIN company_address ca ON ca.id_company = c.id AND ca.is_main = 1
            JOIN acl_group_user agu ON agu.id_user=u.id
            JOIN acl_group ag ON ag.id=agu.id_group
            WHERE $where_companies $where_date
            GROUP by s.id 
        ";
        //echo '<pre>'; print_r($sql); die;
        $list = App__Ed__Model__Users::find_by_sql($sql);

        if(!empty($list))
        {
            $sheet_data = array_merge([0 => [
                'Nr faktury',
                'ID',
                'Produkt',
                'Model',
                'Cena',
                'Ilość',
                'Punkty',
                'Data rejestracji sprzedaży',
                'ID ELECTROLUX',
                'Rola',
                'Firma',
                'NIP',
                'Kod pocztowy',
                'Miasto',
                'Ulica',
                //'Imię',
                //'Nazwisko',
                'Telefon kontaktowy',
                'Email kontaktowy',
                'Handlowiec imię',
                'Handlowiec nazwisko',
                'Handlowiec email',
            ]], array_map(function($item){
                return array_values($item->to_array());
            },$list));

            $this->set_sheet_row($sheet_code_name, $sheet_data);
        }
    }

    public function create_accepted_sale()
    {
        //tworzę nowy arkusz
        $sheet_name = str_replace('create_', '',
            $this->_where_date = __FUNCTION__);
        $sheet_code_name = $this->add_sheet($sheet_name);

        //pobieram warunek dla firm
        $where_companies = "c.id NOT IN(" . implode(',', self::TEST_COMPANIES) . ")";

        //pobieram warnek daty
        $where_date = $this->set_date_where("s.created_at");
        $where_date = !empty($where_date) ? " AND " .$where_date : '';

        //pobieram dane
        $sql = "
            SELECT s.invoice_number, p.id, pl.name, p.model, s.price, s.count, s.points,
              s.created_at, s.updated_at, 
              u.id_electrolux, ag.name as rola,
              c.name as company_name, c.nip, 
              ca.post_code as firma_kod_pocztowy, ca.city as firma_miasto, ca.street as firma_ulica, u.telephone, u.email,
              u2.name as handlowiec_imie, u2.surname as handlowiec_nazwisko, u2.email as handlowiec_email
            FROM sale s
            INNER JOIN product p ON p.id = s.id_product
            INNER JOIN product_lang pl ON pl.product_id = p.id
            INNER JOIN users u ON s.id_customer = u.id
            INNER JOIN company_users cu ON cu.id_user = u.id
            INNER JOIN company c ON c.id = cu.id_company
            INNER JOIN company_supervisor cs ON cs.id_company = c.id
            INNER JOIN users u2 ON u2.id = cs.id_supervisor
            LEFT JOIN company_address ca ON ca.id_company = c.id AND ca.is_main = 1
            JOIN acl_group_user agu ON agu.id_user=u.id
            JOIN acl_group ag ON ag.id=agu.id_group
            WHERE s.status = 2 AND $where_companies $where_date
            GROUP by s.id 
        ";
        $list = App__Ed__Model__Users::find_by_sql($sql);

        if(!empty($list))
        {
            $sheet_data = array_merge([0 => [
                'Nr faktury',
                'ID',
                'Produkt',
                'Model',
                'Cena',
                'Ilość',
                'Punkty',
                'Data rejestracji sprzedaży',
                'Data potwierdzenia sprzedaży',
                'ID ELECTROLUX',
                'Rola',
                'Firma',
                'NIP',
                'Kod pocztowy',
                'Miasto',
                'Ulica',
                //'Imię',
                //'Nazwisko',
                'Telefon kontaktowy',
                'Email kontaktowy',
                'Handlowiec imię',
                'Handlowiec nazwisko',
                'Handlowiec email',
            ]], array_map(function($item){
                return array_values($item->to_array());
            },$list));

            $this->set_sheet_row($sheet_code_name, $sheet_data);
        }
    }


    public function create_not_accepted_sale()
    {
        //tworzę nowy arkusz
        $sheet_name = str_replace('create_', '',
            $this->_where_date = __FUNCTION__);
        $sheet_code_name = $this->add_sheet($sheet_name);

        //pobieram warunek dla firm
        $where_companies = "c.id NOT IN(" . implode(',', self::TEST_COMPANIES) . ")";

        //pobieram warnek daty
        $where_date = $this->set_date_where("s.created_at");
        $where_date = !empty($where_date) ? " AND " .$where_date : '';

        //pobieram dane
        $sql = "
            SELECT s.invoice_number, p.id, pl.name, p.model, s.price, s.count, s.points,
              s.created_at, s.updated_at, 
              u.id_electrolux, ag.name as rola,
              c.name as company_name, c.nip, 
              ca.post_code as firma_kod_pocztowy, ca.city as firma_miasto, ca.street as firma_ulica, u.telephone, u.email,
              u2.name as handlowiec_imie, u2.surname as handlowiec_nazwisko, u2.email as handlowiec_email
            FROM sale s
            INNER JOIN product p ON p.id = s.id_product
            INNER JOIN product_lang pl ON pl.product_id = p.id
            INNER JOIN users u ON s.id_customer = u.id
            INNER JOIN company_users cu ON cu.id_user = u.id
            INNER JOIN company c ON c.id = cu.id_company
            INNER JOIN company_supervisor cs ON cs.id_company = c.id
            INNER JOIN users u2 ON u2.id = cs.id_supervisor
            LEFT JOIN company_address ca ON ca.id_company = c.id AND ca.is_main = 1
            JOIN acl_group_user agu ON agu.id_user=u.id
            JOIN acl_group ag ON ag.id=agu.id_group
            WHERE s.status = 1 AND $where_companies $where_date
            GROUP by s.id 
        ";
        $list = App__Ed__Model__Users::find_by_sql($sql);

        if(!empty($list))
        {
            $sheet_data = array_merge([0 => [
                'Nr faktury',
                'ID',
                'Produkt',
                'Model',
                'Cena',
                'Ilość',
                'Punkty',
                'Data rejestracji sprzedaży',
                'Data odrzucenia',
                'ID ELECTROLUX',
                'Rola',
                'Firma',
                'NIP',
                'Kod pocztowy',
                'Miasto',
                'Ulica',
                //'Imię',
                //'Nazwisko',
                'Telefon kontaktowy',
                'Email kontaktowy',
                'Handlowiec imię',
                'Handlowiec nazwisko',
                'Handlowiec email',
            ]], array_map(function($item){
                return array_values($item->to_array());
            },$list));

            $this->set_sheet_row($sheet_code_name, $sheet_data);
        }
    }

    public function create_users_history_logged()
    {
        //tworzę nowy arkusz
        $sheet_name = str_replace('create_', '',
            $this->_where_date = __FUNCTION__);
        $sheet_code_name = $this->add_sheet($sheet_name);

        //pobieram warnek daty
        $where_date = $this->set_date_where("uhl.ts");
        $where_date = !empty($where_date) ? " AND " .$where_date : '';
        //pobieram dane
        $sql = "
            SELECT u.id_electrolux, ag.name as rola, u.login, u.name, u.surname,
              c.name as company_name, c.nip, ca.post_code as firma_kod_pocztowy, ca.city as firma_miasto, ca.street as firma_ulica, 
              u.telephone, u.email, u2.login as login_zapraszajacego, c2.name as firma_zapraszajacego, u2.name as imie_zapraszajacego, u2.surname as nazwisko_zapraszajacego,
              uhl.ts, uhl.ip, uhl.host, uhl.user_agent
            FROM users_history_logged uhl
            JOIN users u ON uhl.id_user = u.id
            JOIN company_users cu ON cu.id_user = u.id
            JOIN company c ON c.id = cu.id_company
            LEFT JOIN company_address ca ON ca.id_company = c.id AND ca.is_main = 1
            JOIN acl_group_user agu ON agu.id_user=u.id
            JOIN acl_group ag ON ag.id=agu.id_group
            LEFT JOIN users u2 ON u2.id=u.id_up
            LEFT JOIN company_users cu2 ON cu2.id_user = u2.id
            LEFT JOIN company c2 ON c2.id = cu2.id_company
            WHERE u.status = 1 $where_date
            GROUP by uhl.id 
        ";
        $list = App__Ed__Model__Users__History_Logged::find_by_sql($sql);
        if(!empty($list))
        {
            $sheet_data = array_merge([0 => [
                'ID electrolux',
                'Rola',
                'Login',
                'Imię',
                'Nazwisko',
                'Firma',
                'NIP',
                'Kod pocztowy',
                'Miasto',
                'Ulica',
                'Telefon kontaktowy',
                'Email kontaktowy',
                'Login zapraszającego',
                'Firma zapraszającego',
                'Imię zapraszającego',
                'Nazwisko zapraszającego',
                'Data',
                'IP',
                'Host',
                'User-Agent',
            ]], array_map(function($item){
                return array_values($item->to_array());
            },$list));

            $this->set_sheet_row($sheet_code_name, $sheet_data);
        }
    }

    public function create_number_of_login()
    {
        //tworzę nowy arkusz
        $sheet_name = str_replace('create_', '',
            $this->_where_date = __FUNCTION__);
        $sheet_code_name = $this->add_sheet($sheet_name);

        //pobieram warnek daty
        $where_date = $this->set_date_where("uhl.ts");
        $where_date = !empty($where_date) ? " AND " .$where_date : '';
        //pobieram dane
        $sql = "
            SELECT COUNT(uhl.id) as ilosc_logowan, u.id_electrolux, ag.name as rola, u.login, u.name, u.surname,
              c.name as company_name, c.nip, ca.post_code as firma_kod_pocztowy, ca.city as firma_miasto, ca.street as firma_ulica, 
              u.telephone, u.email, u2.login as login_zapraszajacego, c2.name as firma_zapraszajacego, u2.name as imie_zapraszajacego, u2.surname as nazwisko_zapraszajacego
            FROM users u
            LEFT JOIN users_history_logged uhl ON uhl.id_user = u.id
            JOIN company_users cu ON cu.id_user = u.id
            JOIN company c ON c.id = cu.id_company
            LEFT JOIN company_address ca ON ca.id_company = c.id AND ca.is_main = 1
            JOIN acl_group_user agu ON agu.id_user=u.id
            JOIN acl_group ag ON ag.id=agu.id_group
            LEFT JOIN users u2 ON u2.id=u.id_up
            LEFT JOIN company_users cu2 ON cu2.id_user = u2.id
            LEFT JOIN company c2 ON c2.id = cu2.id_company
            WHERE u.status = 1 $where_date
            GROUP by uhl.id 
        ";
        $list = App__Ed__Model__Users__History_Logged::find_by_sql($sql);
        if(!empty($list))
        {
            $sheet_data = array_merge([0 => [
                'Ilość logowań',
                'ID electrolux',
                'Rola',
                'Login',
                'Imię',
                'Nazwisko',
                'Firma',
                'NIP',
                'Kod pocztowy',
                'Miasto',
                'Ulica',
                'Telefon kontaktowy',
                'Email kontaktowy',
                'Login zapraszającego',
                'Firma zapraszającego',
                'Imię zapraszającego',
                'Nazwisko zapraszającego',
            ]], array_map(function($item){
                return array_values($item->to_array());
            },$list));

            $this->set_sheet_row($sheet_code_name, $sheet_data);
        }
    }

    public function create_e_luxy()
    {
        //tworzę nowy arkusz
        $sheet_name = str_replace('create_', '',
            $this->_where_date = __FUNCTION__);
        $sheet_code_name = $this->add_sheet($sheet_name);

        //pobieram warnek daty
        $where_date = $this->set_date_where("uptl.created_at");
        $where_date = !empty($where_date) ? " AND " .$where_date : '';
        //pobieram dane
        $sql = "
            SELECT (1.*uptl.`sender_points_before` - uptl.`sender_points_after`) as points, u1.id_electrolux, 
            c1.name as company_name1, c1.nip as company_nip1, cup1.points as points1, 
            u2.name as wspolpracownik_imie, u2.surname as wspolpracownik_nazwisko, u2.login as wspolpracownik_login, 
            cup2.points as points2, c2.name as company_name2, c2.nip as company_nip2, ca.post_code as firma_kod_pocztowy, ca.city as firma_miasto, ca.street as firma_ulica, u2.telephone, u2.email, 
            u3.name as zapraszajacy_login, u3.name as zapraszajacy_imie, u3.surname as zapraszajacy_nazwisko, u3.email as zapraszajacy_email,
            u4.name as handlowiec_imie, u4.surname as handlowiec_nazwisko, u4.email as handlowiec_email
            FROM `users_points_transfer_log` uptl
            JOIN users u1 ON u1.id = uptl.id_sender
            JOIN company_users cu1 ON cu1.id_user = u1.id
            JOIN company c1 ON c1.id = cu1.id_company
            JOIN company_users_points cup1 ON cup1.company_user_id=u1.id
            JOIN users u2 ON u2.id = uptl.id_receiver
            JOIN company_users cu2 ON cu2.id_user = u2.id
            JOIN company c2 ON c2.id = cu2.id_company
            LEFT JOIN users u3 ON u3.id = u1.id_up
            LEFT JOIN company_users cu3 ON cu3.id_user = u3.id
            LEFT JOIN company c3 ON c3.id = cu3.id_company
            LEFT JOIN company_users_points cup2 ON cup2.company_user_id=u2.id
            LEFT JOIN company_address ca ON ca.id_company = c2.id AND ca.is_main = 1
            LEFT JOIN company_supervisor cs ON cs.id_company = c1.id
            LEFT JOIN users u4 ON u4.id = cs.id_supervisor
            WHERE u1.status = 1 $where_date
            GROUP by uptl.id 
        ";
        $list = App__Ed__Model__Users__History_Logged::find_by_sql($sql);
        if(!empty($list))
        {
            $sheet_data = array_merge([0 => [
                'E-lux przekazane',
                'ID electrolux',
                'Firma',
                'NIP',
                'Saldo E-luxów',
                'Współpracownik imię',
                'Współpracownik nazwisko',
                'Login współpracownika',
                'Saldo E-luxów współpracownika',
                'Firma współpracownika',
                'NIP współpracownika',
                'Kod pocztowy współpracownika',
                'Miasto współpracownika',
                'Ulica współpracownika',
                'Telefon kontaktowy',
                'Email kontaktowy',
                'Login zapraszającego',
                'Firma zapraszającego',
                'Imię zapraszającego',
                'Nazwisko zapraszającego',
                'Handlowiec imię',
                'Handlowiec nazwisko',
                'Handlowiec email',
            ]], array_map(function($item){
                return array_values($item->to_array());
            },$list));

            $this->set_sheet_row($sheet_code_name, $sheet_data);
        }
    }

    public function generate()
    {
        //tworzę arkusze
        if(!empty($this->_params['type']))
        {
            //usuwam domyślny arkusz
            $sheetIndex = $this->_spreadsheet->getIndex(
                $this->_spreadsheet->getActiveSheet()
            );
            $this->_spreadsheet->removeSheetByIndex($sheetIndex);


            foreach($this->_params['type'] as $type => $on)
            {
                $method_name = "create_{$type}";
                if(method_exists($this, $method_name))
                {
                    $this->{$method_name}();
                }
            }

            //generuję plik excel
            $filename = BASE_PATH . "data/tmp/statistic_" . date('Ymd_His') . ".xlsx";
            $writer = new Xlsx($this->_spreadsheet);
            $writer->save($filename);

            //wymuszam pobieranie
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment; filename="'.pathinfo($filename)['basename'].'"');
            header('Content-Length: ' . filesize($filename));
            readfile($filename); // send file
            unlink($filename); // delete file
            exit;
        }

    }
}