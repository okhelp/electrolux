<?php

class App__Modules__Statistic__Ed__Controller__Index extends Lib__Base__Ed_Controller
{
    public function action_generuj()
    {

        if(!empty($_POST))
        {
            $statistic = new App__Modules__Statistic__Ed__Objects__Generate_Excel($_POST);
            $statistic->generate();
        }

        $this->template = get_module_template_path('statistic') . 'generuj.tpl';
        $this->page_title = "Zbiorcze statystyki";
        $this->breadcrumb = ['' => $this->page_title];
    }
}