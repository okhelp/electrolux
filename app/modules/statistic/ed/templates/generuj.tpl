{load_plugin name="datepicker"}

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h3 class="card-title">Wybierz zakres</h3>

                <form method="POST">

                    <div class="row">

                        <div class="col-md-3 form-group">
                            <label>Data od</label>
                            <input type="text" class="form-control datepicker" name="date_from" />
                        </div>
                        <div class="col-md-3 form-group">
                            <label>Data do</label>
                            <input type="text" class="form-control datepicker" name="date_to" value="{$smarty.now|date_format:"Y-m-d"}" />
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-md-12 form-group">

                            <div class="form-group">
                                <input id="t_registered_users" type="checkbox" name="type[registered_users]" />
                                <label for="t_registered_users">lista zarejestrowanych użytkowników</label>
                            </div>

                            <div class="form-group">
                                <input id="t_invited_users" type="checkbox" name="type[invited_users]" />
                                <label for="t_invited_users">lista zaproszonych użytkowników</label>
                            </div>

                            <div class="form-group">
                                <input id="t_not_registered_users" type="checkbox" name="type[not_registered_users]" />
                                <label for="t_not_registered_users">lista tych którzy się nie zarejestrowali (brak zakresu dat)</label>
                            </div>

                            <div class="form-group">
                                <input id="t_sale" type="checkbox" name="type[sale]" />
                                <label for="t_sale">lista zarejestrowanej sprzedaży</label>
                            </div>

                            <div class="form-group">
                                <input id="t_accepted_sale" type="checkbox" name="type[accepted_sale]" />
                                <label for="t_accepted_sale">lista potwierdzonych sprzedaży</label>
                            </div>

                            <div class="form-group">
                                <input id="t_not_accepted_sale" type="checkbox" name="type[not_accepted_sale]" />
                                <label for="t_not_accepted_sale">lista niepotwierdzonych sprzedaży</label>
                            </div>

                            <div class="form-group">
                                <input id="t_users_history_logged" type="checkbox" name="type[users_history_logged]" />
                                <label for="t_users_history_logged">lista logowań użytkowników</label>
                            </div>

                            <div class="form-group">
                                <input id="t_number_of_login" type="checkbox" name="type[number_of_login]" />
                                <label for="t_number_of_login">ilość logowań</label>
                            </div>

                            <div class="form-group">
                                <input id="t_e_luxy" type="checkbox" name="type[e_luxy]" />
                                <label for="t_e_luxy">e-luxy</label>
                            </div>

                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-success">Generuj plik EXCEL</button>
                        </div>
                    </div>

                </form>

            </div>
        </div>
    </div>
</div>