<?php
declare(strict_types=1);

namespace App\modules\products\ed\objects;

use App\modules\products\front\collections\Get_root_category_id;
use App\modules\products\front\models\Product_category_model;

class Add_category
{
    /** @var array */
    private $category_data;

    /**
     * @param array $new_data
     */
    public function __construct(array $new_data)
    {
        $this->category_data = $new_data;
    }

    /**
     * @return bool
     */
    public function add(): bool
    {
        $category = new Product_category_model();
        $category->{Product_category_model::COLUMN_ELECTROLUX_ID} = strtoupper($this->v4());
        $category->name = $this->category_data['name'];
        $category->{Product_category_model::COLUMN_PARENT_ID} = empty($this->category_data['id_parent']) ? Get_root_category_id::get() : $this->category_data['id_parent'];

        return $category->save();
    }

    private function v4()
    {
        return sprintf(
            '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            mt_rand(0, 0xffff), mt_rand(0, 0xffff),
            mt_rand(0, 0xffff),
            mt_rand(0, 0x0fff) | 0x4000,
            mt_rand(0, 0x3fff) | 0x8000,
            mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
        );
    }
}