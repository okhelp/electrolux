<?php
declare(strict_types=1);

namespace App\modules\products\ed\objects;

use App\modules\products\front\models\Product_model;
use App__Ed__Model__Img;

class Remove_product
{
    /** @var int */
    private $product_id;

    /**
     * @param string $product_id
     */
    public function __construct(string $product_id)
    {
        $this->product_id = $product_id;
    }

    public function remove()
    {
        $product_to_remove = Product_model::find($this->product_id);
        if (!empty($product_to_remove->{Product_model::COLUMN_CIMG}))
        {
            App__Ed__Model__Img::remove_cimg($product_to_remove->{Product_model::COLUMN_CIMG});
        }
        $product_to_remove->delete();
    }
}