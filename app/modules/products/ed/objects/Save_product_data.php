<?php
declare(strict_types=1);

namespace App\modules\products\ed\objects;

use App\modules\products\front\collections\Get_product_by_id_collection;
use App\modules\products\front\collections\Get_root_category_id;
use App\modules\products\front\models\Product_model;
use App__Ed__Model__Img;
use App__Ed__Model__Product_package_media;
use App__Ed__Model__Upload;

class Save_product_data
{
    /** @var array */
    private $new_data;

    /** @var int */
    private $product_id;

    /** @var int */
    private $lang_id;

    /**
     * @param array $new_data
     * @param string $product_id
     * @param int $lang_id
     */
    public function __construct(array $new_data, $product_id, int $lang_id)
    {
        $this->new_data = $new_data;
        $this->product_id = $product_id;
        $this->lang_id = $lang_id;
    }

    /**
     * @throws \Exception
     */
    public function save()
    {
        if ($this->product_id) {
            $product = Get_product_by_id_collection::get($this->product_id);
        } else {
            $product = new Product_model();
        }

        if (!empty($_FILES['image']))
        {
            $upload = new App__Ed__Model__Upload('images');
            $cimg = $upload->send_file($_FILES['image'], 1);
        }
        if (!empty($product))
        {
            if (!$this->product_id) {
                $product->{Product_model::COLUMN_ELECTROLUX_ID} = strtoupper('package-' . md5('p' . time() . rand(1000, 9999)));
            }
            $product->{Product_model::COLUMN_BRAND} = $this->new_data[Product_model::COLUMN_BRAND];
            $product->{Product_model::COLUMN_PRODUCT_CODE} = $this->new_data[Product_model::COLUMN_PRODUCT_CODE];
            $product->{Product_model::COLUMN_MODEL} = $this->new_data[Product_model::COLUMN_MODEL];
            $product->{Product_model::COLUMN_BADGES} = empty($this->new_data[Product_model::COLUMN_BADGES]) ? null : json_encode($this->new_data[Product_model::COLUMN_BADGES]);
            $product->{Product_model::COLUMN_CATEGORY} = $this->new_data[Product_model::COLUMN_CATEGORY];
            $product->{Product_model::COLUMN_BAR_CODE} = $this->new_data[Product_model::COLUMN_BAR_CODE];
            $product->{Product_model::COLUMN_STATUS} = $this->new_data[Product_model::COLUMN_STATUS];
            $product->{Product_model::COLUMN_PARENT_ID} = empty($this->new_data['id_parent']) ? Get_root_category_id::get() : $this->new_data['id_parent'];
            $product->{Product_model::COLUMN_POINTS} = $this->new_data[Product_model::COLUMN_POINTS];
            $product->{Product_model::COLUMN_EXTRA_POINTS} = $this->new_data[Product_model::COLUMN_EXTRA_POINTS];
            $product->{Product_model::COLUMN_PRICE} = $this->new_data[Product_model::COLUMN_PRICE];
            $product->name = $this->new_data['name'];
            $product->description = $this->new_data['description'];
            $product->marketing_content = $this->new_data['marketing_content'];
            $product->specification = $this->new_data['specification'];
            if (!empty($cimg) && is_numeric($cimg))
            {
                if (!empty($product->{Product_model::COLUMN_CIMG}))
                {
                    App__Ed__Model__Img::remove_cimg($product->{Product_model::COLUMN_CIMG});
                }

                $product->{Product_model::COLUMN_CIMG} = $cimg;
            }

            $product->save();

            foreach (explode(',', $this->new_data['gallery_delete']) as $idCimg) {
                if (App__Ed__Model__Product_package_media::delete_all(['conditions' => [
                    'id_product' => $product->{Product_model::COLUMN_ID},
                    'id_cimg' => $idCimg,
                ]])) {
                    App__Ed__Model__Img::remove_cimg($idCimg);
                }
            }
            if (!empty($_FILES['gallery']))
            {
                $gallery = [];
                foreach ($_FILES['gallery'] as $key => $values) {
                    foreach ($values as $k => $value) {
                        if (!isset($gallery[$k])) {
                            $gallery[$k] = [];
                        }
                        $gallery[$k][$key] = $value;
                    }
                }
                $upload = new App__Ed__Model__Upload('images');
                foreach ($gallery as $file) {
                    $cimg = $upload->send_file($file, 1);
                    $productPackageMedia = new \App__Ed__Model__Product_package_media();
                    $productPackageMedia->id_product = $product->{Product_model::COLUMN_ID};
                    $productPackageMedia->id_cimg = intval($cimg);
                    $productPackageMedia->save();
                }
            }
            return $product->{Product_model::COLUMN_ID};
        }
        return null;
    }
}