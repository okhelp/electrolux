<?php
declare(strict_types=1);

namespace App\modules\products\ed\objects;

use App\modules\products\front\models\Product_category_model;

class Save_category_data
{
    /** @var array */
    private $new_data;

    /** @var int */
    private $category_id;

    /**
     * @param array $new_data
     * @param string $category_id
     */
    public function __construct(array $new_data, string $category_id)
    {
        $this->new_data = $new_data;
        $this->category_id = $category_id;
    }

    /**
     * @return bool
     * @throws \ActiveRecord\RecordNotFound
     */
    public function save()
    {
        $category = Product_category_model::find($this->category_id);

        if (!empty($category))
        {
            if ($category->{Product_category_model::COLUMN_ID} == $this->category_id)
            {
                if (!empty($this->new_data['id_parent']))
                {
                    $category->{Product_category_model::COLUMN_PARENT_ID} = $this->new_data['id_parent'];
                }

                $category->name = $this->new_data['name'];

                return $category->save();
            }
        }

        return false;
    }
}