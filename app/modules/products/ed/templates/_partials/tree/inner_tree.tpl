<ul>
    {foreach from=$data item=d}
        <li>
            {if empty($smarty.get.id) || (!empty($smarty.get.id) && $smarty.get.id!=$d.int_id)}
                <input type="{$input_type}" value="{$d.id}" name="{$input_name}" id="{$input_name}-{$d.id}"{if $checked_value && $checked_value == $d.id} checked="checked"{/if} />
            {/if}
            <span>{$d.name}</span>
            {if !empty($d.children)}
                {display_tree checked_value=$checked_value data=$d.children input_name=$input_name input_type=$input_type template = {get_module_template_path('products')|cat:'_partials/tree/inner_tree.tpl'}}
            {/if}
        </li>
    {/foreach}
</ul>