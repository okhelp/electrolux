{load_plugin name="tree"}

{if $list}
    <div class="tree well">
        {if $multi_value == 1}
            {assign var="input_type" value="checkbox"}
        {else}
            {assign var="input_type" value="radio"}
        {/if}

        {display_tree checked_value=$checked_value input_name=$input_name input_type=$input_type data=$list template = {get_module_template_path('products')|cat:"_partials/tree/inner_tree.tpl"}}
    </div>
{else}
    <div class="alert alert-warning">
        Brak dostępnych kategorii.
    </div>
{/if}
