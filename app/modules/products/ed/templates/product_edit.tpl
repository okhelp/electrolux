{if !empty($product)}
    <form method="POST" enctype="multipart/form-data">
        <div class="form-group">
            <label>Nazwa:</label>
            <input type="text" class="form-control" name="name" required="required"
                   value="{$product->name}"/>
        </div>
        <div class="form-group">
            <label>Opis:</label>
            <textarea class="form-control" name="description" rows="5">{if !empty($product->description)}{$product->description}{/if}</textarea>
        </div>
        <div class="form-group">
            <label>Cechy produktu:</label>
            <textarea class="form-control" name="marketing_content" rows="5">{if !empty($product->marketing_content)}{$product->marketing_content}{/if}</textarea>
        </div>
        <div class="form-group">
            <label>Specyfikacja produktu (tylko zestawy):</label>
            <textarea class="form-control" name="specification" rows="5">{if !empty($product->specification)}{$product->specification}{/if}</textarea>
        </div>
        <div class="form-group">
            <label>Marka:</label>
            <input type="text" class="form-control" name="brand" required="required" value="{$product->brand}"/>
        </div>
        <div class="form-group">
            <label>Kod produktu:</label>
            <input type="text" class="form-control" name="product_code" required="required"
                   value="{$product->product_code}"/>
        </div>
        <div class="form-group">
            <label>Model:</label>
            <input type="text" class="form-control" name="model" required="required" value="{$product->model}"/>
        </div>
        <div class="form-group">
            <label>Kategoria:</label>
            <input type="text" class="form-control" name="category" required="required" value="{$product->category}"/>
        </div>
        <div class="form-group">
            <label>Kod kreskowy:</label>
            <input type="text" class="form-control" name="bar_code" required="required" value="{$product->bar_code}"/>
        </div>
        <div class="form-group">
            <label>Ilość punktów:</label>
            <input type="number" min="0" class="form-control" name="points" required="required"
                   value="{$product->points}"/>
        </div>
        <div class="form-group">
            <label>Promocyjna ilość punktów:</label>
            <input type="number" min="0" class="form-control" name="extra_points"
                   value="{$product->extra_points}"/>
        </div>
        <div class="form-group">
            <label>Cena:</label>
            <input type="number" min="0" class="form-control" name="price" step=0.01 value="{$product->price}"/>
        </div>
        <div class="form-group">
            <label>Etykieta</label>
            <div class="form-check">
                <input type="checkbox" class="form-check-input" name="badges[]" value="new"
                       {if !empty($product->badges) && in_array('new', json_decode($product->badges))}checked{/if}>
                <label class="form-check-label">nowość</label>
            </div>
            <div class="form-check">
                <input type="checkbox" class="form-check-input" name="badges[]" value="series_end"
                       {if !empty($product->badges) && in_array('series_end', json_decode($product->badges))}checked{/if}>
                <label class="form-check-label">koniec serii</label>
            </div>
            <div class="form-check">
                <input type="checkbox" class="form-check-input" name="badges[]" value="super_new"
                       {if !empty($product->badges) && in_array('super_new', json_decode($product->badges))}checked{/if}>
                <label class="form-check-label">super nowość</label>
            </div>
            <div class="form-check">
                <input type="checkbox" class="form-check-input" name="badges[]" value="eco"
                       {if !empty($product->badges) && in_array('eco', json_decode($product->badges))}checked{/if}>
                <label class="form-check-label">eco</label>
            </div>
        </div>
        <div class="form-group">
            <label>Zdjęcie:</label>
            <div class="row">
                <div class="col-lg-6">
                    <img class="img-thumbnail" src="{img id_cimg=$product->cimg}" style="height: 150px; width: auto;"/>
                </div>
                <div class="col-lg-6 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-header">
                            Dodaj plik nowego zdjęcia
                        </div>
                        <div class="card-body">
                            <input type="file" name="image"
                                   accept=".jpg,.png,.jpeg">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group">
            <label>Galeria (nie obejmuje zdjęć z importu):</label>
            <div class="col-lg-12">
                <div class="row card-header" style="margin-bottom: 15px">
                {foreach from=$gallery item=image}
                    <div class="col-lg-4 float-left gallery_img" data-id_cimg="{$image->id_cimg}" style="margin-bottom: 15px; margin-top: 15px">
                        <img class="img-fluid" src="{img id_cimg={$image->id_cimg} height=700 width=700}">
                        <div class="btn btn-danger icon-trash col-lg-12 gallery_img_delete"> usuń</div>
                    </div>
                {/foreach}
                </div>
            </div>
            <input type="file" name="gallery[]" accept=".jpg,.png,.jpeg" multiple />
            <input type="hidden" name="gallery_delete" />
        </div>

        <div class="form-group">
            <label>Status:</label>
            <select name="status" class="form-control">
                <option value="1"{if $product->status == 1} selected="selected"{/if}>aktywny</option>
                <option value="0"{if $product->status == 0} selected="selected"{/if}>nieaktywny</option>
            </select>
        </div>
        <div class="form-group">
            <div id="accordion">
                <div class="card">
                    <div class="card-header" id="headingOne">
                        <h5 class="mb-0">
                            <a class="btn btn-link" data-toggle="collapse" data-target="#collapseOne"
                               aria-expanded="true" aria-controls="collapseOne">
                                Wybór kategorii produktu
                            </a>
                        </h5>
                    </div>

                    <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                        <div class="card-body">
                            {$category_tree}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <button type="submit" name="save" value="1" class="btn btn-dafault">Zapisz</button>
    </form>
    <script src="{BASE_URL}data/ed/js/tinymce.min.js"></script>
    <script src="{BASE_URL}data/ed/js/theme.min.js"></script>
    <script>
        {literal}
				tinymce.init({
					selector: 'textarea',
					setup: function (editor) {
						editor.on('change', function () {
							editor.save();
						});
					}
				});
				$(document).ready(function () {
                    $('.gallery_img_delete').on('click', function () {
                        var $galleryImg = $(this).closest('.gallery_img');
                        var $inputGalleryDelete = $('input[name="gallery_delete"]');
                        $inputGalleryDelete.val($inputGalleryDelete.val() + ($inputGalleryDelete.val().length > 0 ? ',' : '') + $galleryImg.data('id_cimg'));
                        $galleryImg.remove();
                    });
				});
        {/literal}
    </script>
{else}
    <div class="alert alert-info">
        Nie odnaleziono produktu
    </div>
{/if}