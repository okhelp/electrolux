{load_plugin name="datatable"}
<div class="row">
    <div class="col-md-4 col-sm-8 col-xs-8">
        <a class="btn btn-success btn-sm" href="{BASE_ED_URL}products/ed.html">Dodaj</a>
    </div>
</div>
<table id="datatable">
    <thead>
    <tr>
        <th>Nazwa</th>
        <th>Model</th>
        <th>Data utworzenia</th>
        <th>Data modyfikacji</th>
        <th>Status</th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    </tbody>
</table>

<script>
    $(document).ready(function () {
        $('#datatable').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": "{BASE_ED_URL}products/get_products_list.html",
                "type": 'post',
                "data": function (d) {
                    d.records_total = "200";
                }
            },
            "columnDefs": [
                {
                    "orderable": false,
                    "targets": [4, 5],
                },
                {
                    "searchable": false,
                    "targets": [2, 3],
                }
            ],
            "language" : {
                "processing": "Przetwarzanie...",
                "search": "Szukaj:",
                "lengthMenu": "Pokaż _MENU_ pozycji",
                "info": "Pozycje od _START_ do _END_ z _TOTAL_ łącznie",
                "infoEmpty": "Pozycji 0 z 0 dostępnych",
                "infoFiltered": "(filtrowanie spośród _MAX_ dostępnych pozycji)",
                "infoPostFix": "",
                "loadingRecords": "Wczytywanie...",
                "zeroRecords": "Nie znaleziono pasujących pozycji",
                "emptyTable": "Brak danych",
                "paginate": {
                    "first": "Pierwsza",
                    "previous": "Poprzednia",
                    "next": "Następna",
                    "last": "Ostatnia"
                },
                "aria": {
                    "sortAscending": ": aktywuj, by posortować kolumnę rosnąco",
                    "sortDescending": ": aktywuj, by posortować kolumnę malejąco"
                }
            },
        });
    });
</script>

