<?php

use App\modules\products\ed\objects\Add_category;
use App\modules\products\ed\objects\Products_categories_tree;
use App\modules\products\ed\objects\Remove_product_category;
use App\modules\products\ed\objects\Save_category_data;
use App\modules\products\front\collections\Get_root_category_id;
use App\modules\products\front\models\Product_category_model;
use App\modules\products\front\models\Sample_model;

class App__Modules__Products__Ed__Controller__Categories extends Lib__Base__Ed_Controller
{
    public function action_lista()
    {
        $tree = Products_categories_tree::get_categories(Get_root_category_id::get(), 1, true);

        $this->view->assign('tree', json_encode($tree, JSON_UNESCAPED_UNICODE));
        $this->template = get_module_template_path('products') . 'categories_list.tpl';
        $this->page_title = "Lista kategorii";
        $this->breadcrumb = ['' => 'Lista kategorii'];
    }

    public function action_ed()
    {
        if (!empty($_POST) && !empty($_GET['id']))
        {
            $category_data = new Save_category_data($_POST, (string)$_GET['id']);
            go_back($category_data->save() ? 'g|Dane zostały zapisane' : 'd|Dane nie zostały zapisane');
        }
        else
        {
            if (!empty($_GET['id']))
            {
                $category = Product_category_model::find($_GET['id']);

                if (!empty($category))
                {
                    $this->view->assign('category', $category);
                    $this->view->assign(
                        'category_tree',
                        Products_categories_tree::get_ed_tree(
                            Get_root_category_id::get(),
                            -1,
                            'id_parent',
                            !empty($category->{Product_category_model::COLUMN_PARENT_ID}) ? $category->{Product_category_model::COLUMN_PARENT_ID} : ''
                        )
                    );
                    $this->template = get_module_template_path('products') . 'category_edit.tpl';
                    $this->page_title = "Edycja kategorii";
                }
            }
            else
            {
                go_back('d|Brak dostępu');
            }
        }
    }

    public function action_remove()
    {
        $message = 'd|Brak dostępu';

        if (!empty($_GET['id']))
        {
            try
            {
                $remove_product_category = new Remove_product_category($_GET['id']);
                $remove_product_category->remove();

                $message = "g|Kategoria została usunięta.";
            }
            catch (Exception $exception)
            {
                $message = "d|" . $exception->getMessage();
            }

        }

        go_back($message);
    }

    public function action_create()
    {
        if (!empty($_POST['save']))
        {
            $category_add = new Add_category($_POST);

            go_back($category_add->add() ? "g|Kategoria została dodana." : "d|Kategoria nie została dodana.", BASE_ED_URL . 'products/categories/lista.html');
        }

        $this->view->assign(
            'category_tree',
            Products_categories_tree::get_ed_tree(
                Get_root_category_id::get(),
                -1,
                'id_parent',
                Get_root_category_id::get()
            )
        );

        $this->template = get_module_template_path('products') . 'category_add.tpl';
        $this->page_title = "Dodanie kategorii";
    }
}