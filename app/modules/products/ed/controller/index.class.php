<?php

use App\modules\products\ed\objects\Products_categories_tree;
use App\modules\products\ed\objects\Remove_product;
use App\modules\products\ed\objects\Save_product_data;
use App\modules\products\front\collections\Get_all_products_products_collection;
use App\modules\products\front\collections\Get_product_by_id_collection;
use App\modules\products\front\collections\Get_product_media;
use App\modules\products\front\collections\Get_root_category_id;
use App\modules\products\front\models\Product_model;

class App__Modules__Products__ED__Controller__Index extends Lib__Base__Ed_Controller
{
    public function action_lista()
    {
        $this->view->assign('products', Get_all_products_products_collection::get($_SESSION['id_lang']));
        $this->template = get_module_template_path('products') . 'products_list.tpl';
        $this->page_title = "Lista produktów";
        $this->breadcrumb = ['' => 'Lista produktów'];
    }

    public function action_get_products_list()
    {
        $table = 'product';
        $primaryKey = 'id';
        $columns = [
            ['db' => 'name', 'dt' => 0],
            ['db' => 'model', 'dt' => 1],
            [
                'db'        => 'created_at',
                'dt'        => 2,
                'formatter' => function($d, $row) {
                    return date('Y-m-d H:i:s', strtotime($d));
                },
            ],
            [
                'db'        => 'updated_at',
                'dt'        => 3,
                'formatter' => function($d, $row) {
                    return date('Y-m-d H:i:s', strtotime($d));
                },
            ],
            [
                'db'        => 'status',
                'dt'        => 4,
                'formatter' => function($d, $row) {
                    $status_string = '';

                    if ($d == 1)
                    {
                        $status_string = '<span class="label label-success">aktywny</span>';
                    }
                    else
                    {
                        $status_string = '<span class="label label-danger">nieaktywny</span>';
                    }

                    return $status_string;
                },
            ],
            [
                'db'        => 'id',
                'dt'        => 5,
                'formatter' => function($d, $row) {
                    return '
                        <a href="' . BASE_ED_URL . 'products/ed.html?id= ' . $d . '" class="btn btn-default">edytuj</a>
				        <a href="' . BASE_ED_URL . 'products/remove.html?id= '  . $d . '" class="btn btn-danger confirm" data-confirm-text="Czy na pewno chcesz usunąć tą pozycję?">usuń</a>
                    ';
                },
            ],
        ];
        $join = " JOIN product_lang ON product_lang.product_id=product.id ";
        $where = " id_service={$this->session['id_service']} AND id_lang={$this->session['id_lang']}";

        echo json_encode(
            SSP::complex(
                $_POST,
                $table,
                $primaryKey,
                $columns,
                $join,
                null,
                $where)
        );
    }

    public function action_ed()
    {
        $productID = $_GET['id'] ?? null;
        if (!empty($_POST['save'])) {
            try {
                $save_product_data = new Save_product_data($_POST, $productID, (int)$_SESSION['id_lang']);
                $productID = $save_product_data->save();
                $message = "g|Zmiany zostały zapisane";
            } catch (Exception $exception) {
                $message = "d|Zmiany nie zostały zapisane";
            }
            if ($productID) {
                go_back($message, BASE_ED_URL . "products/ed.html?id=$productID");
            } else {
                go_back($message);
            }
        }

        $product = $productID ? Get_product_by_id_collection::get($productID) : new Product_model;

        $this->view->assign('category_tree', Products_categories_tree::get_ed_tree(Get_root_category_id::get(), -1, 'id_parent', !empty($product->{Product_model::COLUMN_PARENT_ID}) ? $product->{Product_model::COLUMN_PARENT_ID} : null));
        $this->view->assign('product', $product);
        $this->view->assign('gallery',  $productID ? App__Ed__Model__Product_package_media::get_images($productID) : []);
        $this->template = get_module_template_path('products') . 'product_edit.tpl';
        $this->page_title = $productID ? 'Edycja produktu' : 'Dodawanie produktu';
        $this->breadcrumb = ['' => $productID ? 'Edycja produktu' : 'Dodawanie produktu'];
    }

    public function action_remove()
    {
        $message = 'd|Brak dostępu';

        if(!empty($_GET['id']))
        {
            $remove_product = new Remove_product((string)$_GET['id']);
            $remove_product->remove();

            $message = 'g|Produkt został usunięty';
        }

        go_back($message);
    }
}