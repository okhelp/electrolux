<div class="product-details">
    <div class="container-fluid product_more">
        <div class="row">
            <div class="col-md-6 d-md-flex product_more__text">
                <div class="row">
                    <div class="col-12">
                        <div class="row">
                            <div class="col-12">
                                <p class="product_more__title">{$product->name}</p>
                                <p class="product_more__code">{$product->model}</p>
                            </div>
                        </div>
                        <div class="row product_more__row_border">
                            <div class="col-6">
                                <p class="product_more__points">{$product->points} <span
                                            class="text-nowrap">E-LUX</span></p>
                            </div>
                            <div class="col-6">
                                <p class="product_more__price">{$product->price} ZŁ</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <p class="product_more__description">
                                    {$product->description}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 align-items-center justify-content-center product_more__photo">
                <div class="row w-100">
                    <div class="col">
                        <div class="product_more_slick__main" style="width: 100%">
                            <div class="product_more_slick__main_single" style="min-height: 100%"
                                 data-src="{img id_cimg={$product->cimg} height=700 width=700}" data-exthumbimage="{img id_cimg={$product->cimg} width=96 height=76}">
                                <img class="img-fluid"
                                     src="{img id_cimg={$product->cimg} height=700 width=700}">
                            </div>
                            {if !empty($product_gallery)}
                                {foreach $product_gallery as $gallery_element}
                                    {if in_array($gallery_element->type, ['picture', 'video'])}
                                            {if $gallery_element->type == 'picture'}
                                                <div class="product_more_slick__main_single" data-exthumbimage="{$gallery_element->link}" data-src="{$gallery_element->link}"><img class="img-fluid" style="max-width: 100%;"
                                                                                        data-lazy="{$gallery_element->link}"></div>
                                            {/if}
                                            {*{if $gallery_element->type == 'video'}*}
                                                {*<img class="img-fluid"*}
                                                     {*src="{img id_cimg={$product->cimg} height=600 width=500}">*}
                                            {*{/if}*}
                                            {if $gallery_element->type == 'video'}
                                                <div style="display:none;" id="video{$gallery_element->id}">
                                                    <video class="lg-video-object lg-html5" controls preload="none">
                                                        <source src="{$gallery_element->link}" type="video/webm">
                                                        Your browser does not support HTML5 video.
                                                    </video>
                                                </div>
                                                <a class="product_more_slick__main_single product_video_big" data-exthumbimage="{img id_cimg={$product->cimg} width=96 height=76}" data-poster="{img id_cimg={$product->cimg} height=600 width=500}" data-html="#video{$gallery_element->id}"><img class="img-fluid" style="max-width: 100%;" src="{img id_cimg={$product->cimg} height=600 width=500}"></a>
                                            {/if}
                                    {/if}
                                {/foreach}
                            {/if}
                            <div class="slider-buttons">
                                <button class="prev-slide-product-more custom__arrow btn">
                                    <i class="icon-prev"></i>
                                </button>
                                <button class="next-slide-product-more custom__arrow btn">
                                    <i class="icon-next"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-4 w-100">
                    <div class="col">
                        <div class="product_more_slick__thumbs">
                            <div class="product_more_slick__thumbs_single"
                                 data-src="{img id_cimg={$product->cimg} height=50 width=75}">
                                <img class="img-fluid" style="max-width: 125px; max-height: 125px;"
                                     data-lazy="{img id_cimg={$product->cimg}}">
                            </div>
                            {if !empty($product_gallery)}
                                {foreach $product_gallery as $gallery_element}
                                    {if in_array($gallery_element->type, ['picture', 'video'])}
                                        <div class="product_more_slick__thumbs_single"
                                             data-src="{$gallery_element->link}">
                                            {if $gallery_element->type == 'picture'}
                                                <img class="img-fluid" style="width: auto; max-height: 125px;"
                                                     data-lazy="{$gallery_element->link}">
                                            {/if}
                                            {if $gallery_element->type == 'video'}
                                                <div class="product_video_big">
                                                    <img class="img-fluid" style="max-width: 125px; max-height: 125px;"
                                                         src="{img id_cimg={$product->cimg}}">
                                                    {*<img class="img-fluid" style="max-width: 125px; max-height: 125px;"*}
                                                         {*src="{img id_cimg={$product->cimg} height=125 width=95}">*}
                                                </div>
                                            {/if}
                                        </div>
                                    {/if}
                                {/foreach}
                            {/if}
                        </div>
                    </div>
                </div>
            </div>
            <div id="tab-1" class="col-md-6 product_more__specification product_more__specification--lightBlue">
                <span data-target="tab-1" class="product_more__close"><i class="icon-filter_cross"></i></span>
                <p class="product_more__specification_title">Cechy produktu</p>
                <ul id="product_more__specification_list1" class="product_more__specification_list">
                    {$product->marketing_content}
                </ul>
                <span class="linkMore" data-target="product_more__specification_list1">Pokaż więcej</span>
            </div>
            <div id="tab-2" class="col-md-6 product_more__specification product_more__specification--border">
                <span data-target="tab-2" class="product_more__close"><i class="icon-filter_cross"></i></span>
                <p class="product_more__specification_title">Podstawowe<br> dane techniczne</p>
                {if !empty($product->parent_id) && $product->parent_id=='F7D7BC70-3751-4A0A-8DFF-3BB52F2DE595'}
                    {if !empty($product->specification)}{$product->specification}{/if}
                {else}
                    <ul id="product_more__specification_list2" class="product_more__specification_list">
                        {foreach $product_attributes as $product_attribute}
                            {if !empty($product_attribute['value'])}
                                <li class="product_more__specification_point">
                                    <span class="product_more__specification_point--bold">{$product_attribute['name']}: </span>{$product_attribute['value']}
                                </li>
                            {/if}
                        {/foreach}
                    </ul>
                {/if}
                <span class="linkMore" data-target="product_more__specification_list2">Pokaż więcej</span>
                <ul class="product_more__specification_list_files">
                    {if !empty($product_catalogue_card)}
                        <li class="product_more__specification_list_files_point">
                            <a target="_blank" href="{$product_catalogue_card}">karta katologowa produktu</a>
                        </li>
                    {/if}
                    {foreach $product_gallery as $gallery_item}
                        {if $gallery_item->type == 'user_manual'}
                            <li class="product_more__specification_list_files_point">
                                <a target="_blank" href="{$gallery_item->link}">karta informacyjna EU</a>
                            </li>
                        {/if}
                        {if $gallery_item->type == 'installation_diagram'}
                            <li class="product_more__specification_list_files_point">
                                <a target="_blank" href="{$gallery_item->link}">rysunek techniczny</a>
                            </li>
                        {/if}
                    {/foreach}
                </ul>
            </div>
        </div>
        <div class="row d-none d-md-flex">
            <div class="col-6 product_more__extra_links product_more__extra_links--lightBlue">
            <span data-tab="tab-1" class="product_more__tab_link"><i
                        class="icon-plus product_more__extra_links--icon product_more__extra_links--iconChange"></i>
                Funkcje</span>
            </div>
            <div class="col-6 product_more__extra_links product_more__extra_links--blue">
            <span data-tab="tab-2" class="product_more__tab_link"><i
                        class="icon-plus product_more__extra_links--icon product_more__extra_links--iconChange"></i>Specyfikacja</span>
            </div>
        </div>
        {if !empty($related_products)}
            <div class="row product_list recommended_products equalHeightContainer">
                <div class="col-12 text-center">
                    <p class="recommended_products__title">Produkty polecane</p>
                </div>
                <div class="col-12 product_listing__row product_listing--grid recommended_products_slick">
                    {foreach $related_products as $related_product}
                        <div class="product_listing__single_product">
                            <div class="row single_product__img_row">
                                <div class="col-12 single_product_info--wrapper">
                                    {*<div class="single_product_info">*}
                                    {*<div class="single_product_info--container">*}
                                    {*<p class="single_product_info--label single_product_info--new">*}
                                    {*nowość</p>*}
                                    {*</div>*}
                                    {*<div class="single_product_info--container">*}
                                    {*<p class="single_product_info--label single_product_info--end">*}
                                    {*koniec*}
                                    {*serii</p>*}
                                    {*</div>*}
                                    {*<div class="single_product_info--container">*}
                                    {*<p class="single_product_info--label single_product_info--quality">*}
                                    {*super*}
                                    {*jakość</p>*}
                                    {*</div>*}
                                    {*</div>*}
                                    <img class="single_product__img img-fluid extra_info lazy"
                                         data-tooltip-content="#tooltip_content{$related_product->id}"
                                         data-src="{img id_cimg={$related_product->cimg} width=250 height=250}" src="">
                                    <div class="tooltip_templates">
                                        <div class="extra_info_tooltip" id="tooltip_content{$related_product->id}">
                                            <div class="container">
                                                <div class="row mb-3">
                                                    <div class="col-6">
                                                        <p class="single_product__name single_product__name--bold">
                                                            {$related_product->name}
                                                        </p>
                                                    </div>
                                                    <div class="col-6 text-right">
                                                        <p class="single_product__points">{$related_product->points}
                                                            E-LUX</p>
                                                    </div>
                                                </div>
                                                <div class="row mb-3">
                                                    <div class="col-12">
                                                        <p class="tooltip_product__subtitle">
                                                            {$related_product->name}
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12">
                                                        <p class="tooltip_product__description">
                                                            {$related_product->description}
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row single_product__price_row">
                                <div class="col-6 single_product__name--container">
                                    <p class="single_product__model"><b>{$related_product->model}</b></p>
                                    <p class="single_product__name">{$related_product->name}</p>
                                </div>
                                <div class="col-6 single_product__price--container">
                                    <p class="single_product__points">{$related_product->points} <span
                                                class="text-nowrap">E-LUX</span></p>
                                    {if !empty($related_product->extra_points)}
                                        <p class="single_product__extra_points">+ {$related_product->extra_points} <span class="text-nowrap">E-LUX</span></p>
                                    {/if}
                                    <p class="single_product__price">{$related_product->price} ZŁ</p>
                                </div>
                            </div>
                            <div class="row single_product__buttons">
                                <div class="col-12">
                                    <a href="{BASE_URL}products/product.html?id={$related_product->id}"
                                       class="btn btn-outline-primary btn-pills single_product__more">
                                        więcej
                                    </a>
                                </div>
                                {*<div class="col-6">*}
                                {*<button type="button"*}
                                {*class="btn btn-primary btn-pills single_product__order">*}
                                {*zamów*}
                                {*</button>*}
                                {*</div>*}
                            </div>
                        </div>
                    {/foreach}
                    <div class="slider-buttons">
                        <button class="prev-slide-recommended custom__arrow btn">
                            <i class="icon-prev"></i>
                        </button>
                        <button class="next-slide-recommended custom__arrow btn">
                            <i class="icon-next"></i>
                        </button>
                    </div>
                </div>
            </div>
        {/if}
    </div>
</div>
<script>
    {assign var=slick_items_main value=$product_gallery|@count}
    {literal}
    slideShowMain = {/literal}{$slick_items_main}{literal};
    {/literal}
    {assign var=slick_items_recommended value=$related_products|@count}
    {literal}
    slideShowRecommended = {/literal}{$slick_items_recommended}{literal};
    $(document).ready(function () {
        _paq.push(['trackEvent', 'Produkt', 'ID Produktu', {/literal}{$product->id}{literal}]);
        $('.product_more__extra_links--lightBlue').on('click', function () {
          _paq.push(['trackEvent', 'Przycisk funkcje', 'Kliknięty', true]);
        });
        $('.product_more__extra_links--blue').on('click', function () {
          _paq.push(['trackEvent', 'Przycisk specyfikacja', 'Kliknięty', true]);
        });
        $('.single_product__more').on('click', function () {
          _paq.push(['trackEvent', 'Polecane produkty', 'Kliknięty', $(this).attr('href')]);
        });
    });
    {/literal}
</script>

