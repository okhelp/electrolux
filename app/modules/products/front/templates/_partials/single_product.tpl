{if !empty($products)}
    {foreach $products as $product}
        <div class="product_listing__single_product">
            <div class="row single_product__img_row">
                <div class="col-12 single_product_info--wrapper">
                    <a href="{$product->url}">
                        <div class="single_product_info">
                            {if !empty($product->extra_points)}
                                <div class="single_product_info--container">
                                    <p class="single_product_info--label single_product_info--extraPoints">
                                        extra e-luxy
                                    </p>
                                </div>
                            {/if}
                            {if !empty($product->badges) && in_array('new', json_decode($product->badges))}
                                <div class="single_product_info--container">
                                    <p class="single_product_info--label single_product_info--new">
                                        nowość</p>
                                </div>
                            {/if}
                            {if !empty($product->badges) && in_array('series_end', json_decode($product->badges))}
                                <div class="single_product_info--container">
                                    <p class="single_product_info--label single_product_info--end">
                                        koniec
                                        serii</p>
                                </div>
                            {/if}
                            {if !empty($product->badges) && in_array('super_new', json_decode($product->badges))}
                                <div class="single_product_info--container">
                                    <p class="single_product_info--label single_product_info--quality">
                                        super
                                        jakość</p>
                                </div>
                            {/if}
                            {if !empty($product->badges) && in_array('eco', json_decode($product->badges))}
                                <div class="single_product_info--container">
                                    <p class="single_product_info--label single_product_info--quality">
                                        eco
                                    </p>
                                </div>
                            {/if}
                            `{if !empty($product->parent_id) && $product->parent_id=='F7D7BC70-3751-4A0A-8DFF-3BB52F2DE595'}`
                                <div class="single_product_info--container">
                                    <p class="single_product_info--label single_product_info--quality">
                                        Zestaw
                                    </p>
                                </div>
                            {/if}
                        </div>
                        <img class="single_product__img img-fluid extra_info lazy"
                             data-tooltip-content="#tooltip_content{$product->id}"
                             data-src="{img id_cimg={$product->cimg} width=300 height=300}" src="">
                    </a>
                    <div class="tooltip_templates">
                        <div class="extra_info_tooltip" id="tooltip_content{$product->id}">
                            <div class="container">
                                <div class="row mb-3">
                                    <div class="col-6">
                                        <p class="single_product__name single_product__name--bold">
                                            {$product->name}
                                        </p>
                                    </div>
                                    <div class="col-6 text-right">
                                        <p class="single_product__points">{$product->points}
                                            E-LUX</p>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <div class="col-12">
                                        <p class="tooltip_product__subtitle">
                                            {$product->name}
                                        </p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <p class="tooltip_product__description">
                                            {$product->description}
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row single_product__price_row">
                <div class="col-6 single_product__name--container">
                    <p class="single_product__model"><b><a href="{$product->url}">{$product->model}</a></b></p>
                    <p class="single_product__name"><a href="{$product->url}">{$product->name}</a></p>
                </div>
                <div class="col-6 single_product__price--container">
                    <p class="single_product__points">{$product->points} <span class="text-nowrap">E-LUX</span></p>
                    {if !empty($product->extra_points)}
                        <p class="single_product__extra_points">+ {$product->extra_points} <span class="text-nowrap">E-LUX</span></p>
                    {/if}
                    <p class="single_product__price">{$product->price} ZŁ</p>
                </div>
            </div>
            <div class="row single_product__buttons">
                <div class="col-12">
                    <a href="{$product->url}"
                       class="btn btn-outline-primary btn-pills single_product__more">
                        więcej
                    </a>
                </div>
            </div>
        </div>
    {/foreach}
{else}
    <div class="alert alert-info">
        Brak produktów do wyświetlenia.
    </div>
{/if}
<div class="col-12">
    {pagination all_items=$pages_count per_page=$items_count current_page=$page}
</div>