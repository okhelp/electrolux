<?php

namespace App\modules\products\front\objects;

use App\modules\products\front\objects\xml\Category;
use App\modules\products\front\objects\xml\Product;

class Xml_element_factory
{
    const CATEGORY_XML_ELEMENT = 'Navigation';
    const PRODUCT_XML_ELEMENT  = 'Product';

    public static function create(\SimpleXMLElement $element, $parent_element_id)
    {
        $element_name = $element->getName();

        switch ($element_name)
        {
            case self::CATEGORY_XML_ELEMENT:
                return new Category($element, $parent_element_id);
                break;
            case self::PRODUCT_XML_ELEMENT:
                return new Product($element, $parent_element_id);
                break;
            default:
                return null;
        }
    }
}