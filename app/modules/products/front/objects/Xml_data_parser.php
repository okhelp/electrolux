<?php

namespace App\modules\products\front\objects;

use App\front\models\company\Get_company_services;
use App\modules\products\front\collections\Get_all_products_attributes;
use App\modules\products\front\collections\Get_all_products_categories_collection;
use App\modules\products\front\collections\Get_all_products_products_collection;
use App\modules\products\front\interfaces\Xml_element;
use App__Ed__Model__Attribute;

class Xml_data_parser
{
    /** @var \SimpleXMLElement */
    private $xml;

    /** @var string */
    private $file_version;

    /** @var int */
    private $language_id;

    /** @var array */
    private $existing_products;

    /** @var array */
    private $existing_categories;

    /** @var array */
    private $existing_products_attributes;

    /**
     * Xml_data_parser constructor.
     * @param \SimpleXMLElement $xml
     * @throws \Exception
     */
    public function __construct(\SimpleXMLElement $xml)
    {
        $this->xml = $xml;

        $this->file_version = $this->xml['Version']->__toString();

        $this->language_id = $this->get_language_id($this->xml['LanguageIsoCode']->__toString());

        $this->existing_products = $this->get_existing_products();
        $this->existing_products = is_null($this->existing_products) ? [] : $this->existing_products;

        $this->existing_categories = $this->get_existing_categories();
        $this->existing_categories = is_null($this->existing_categories) ? [] : $this->existing_categories;

        $this->existing_products_attributes = $this->get_existing_attributes();
    }

    public function parse()
    {
        $main_category = $this->xml->Navigation->Navigation;

        $this->walk_through_xml('', $main_category);
    }

    /**
     * @param string $previous_category_id
     * @param \SimpleXMLElement $navigation
     * @throws \ActiveRecord\RecordNotFound
     */
    private function walk_through_xml(string $previous_category_id, \SimpleXMLElement $navigation)
    {
        //TODO coś bardziej elastycznego
        $xml_element = Xml_element_factory::create($navigation, $previous_category_id);

        if ($xml_element instanceof Xml_element)
        {
            $xml_element->perform_actions($this->existing_categories, $this->existing_products, $this->existing_products_attributes, $this->language_id);
        }

        if (!empty($navigation->Product))
        {
            if ($navigation->Product->count() > 1)
            {
                for ($i = 0; $i < $navigation->Product->count(); $i++)
                {
                    $product = Xml_element_factory::create($navigation->Product[$i], $navigation['NavigationID']->__toString());
                    $this->add_not_existing_attributes($product);
                    $product->perform_actions($this->existing_categories, $this->existing_products, $this->existing_products_attributes, $this->language_id);
                }
            }
            else
            {
                $product = Xml_element_factory::create($navigation->Product, $navigation['NavigationID']->__toString());
                $this->add_not_existing_attributes($product);
                $product->perform_actions($this->existing_categories, $this->existing_products, $this->existing_products_attributes, $this->language_id);
            }
        }

        if (count($navigation->Navigation))
        {
            foreach ($navigation->Navigation as $navigation_element)
            {
                if ($navigation_element->getName() == 'Navigation' && !empty($navigation_element['NavigationID']->__toString()))
                {
                    $this->walk_through_xml($navigation['NavigationID']->__toString(), $navigation_element);
                }
            }
        }
    }

    /**
     * @param string $language_iso_code
     * @return int
     * @throws \Exception
     */
    private function get_language_id(string $language_iso_code): int
    {
        $language_iso_code_exploded = explode('-', $language_iso_code);

        if (!empty($language_iso_code_exploded[0]))
        {
            $language_code = $language_iso_code_exploded[0];

            $language_model = \App__Ed__Model__Language::find_by_sql(
                "
                SELECT *
                FROM language
                WHERE code='$language_code'
                "
            );

            if (!empty($language_model))
            {
                return $language_model[0]->id;
            }
        }
        else
        {
            throw new \Exception('Nie odnaleziono kodu języka');
        }
    }

    private function get_existing_products()
    {
        return Get_all_products_products_collection::get($this->language_id);
    }

    private function get_existing_categories()
    {
        return Get_all_products_categories_collection::get($this->language_id, true);
    }

    private function get_existing_attributes(): array
    {
        return Get_all_products_attributes::get($this->language_id, true);
    }

    private function add_not_existing_attributes(Xml_element $xml_element)
    {
        if(!empty($xml_element->attributes))
        {
            $_SESSION['id_service'] = $xml_element->brand == 'AEG' ? Get_company_services::ID_SERVICE_AEG : Get_company_services::ID_SERVICE_ELECTROLUX;

            foreach($xml_element->attributes as $attribute)
            {
                if(empty($this->existing_products_attributes[$attribute['id']][$_SESSION['id_service']]))
                {
                    $new_attribute = new App__Ed__Model__Attribute();
                    $new_attribute->{App__Ed__Model__Attribute::COLUMN_ELECTROLUX_ID} = $attribute['id'];
                    $new_attribute->{App__Ed__Model__Attribute::COLUMN_NAME} = $attribute['name'];
                    if($new_attribute->save())
                    {
                        $this->existing_products_attributes[$attribute['id']][$_SESSION['id_service']] = $new_attribute;
                    }
                }
            }

            $_SESSION['id_service'] = Get_company_services::ID_SERVICE_ELECTROLUX;
        }
    }

}