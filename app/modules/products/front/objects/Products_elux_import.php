<?php
declare(strict_types=1);

namespace App\modules\products\front\objects;

use ActiveRecord\ConnectionManager;
use ActiveRecord\DateTime;
use App\modules\products\front\interfaces\Runnable_interface;
use App\modules\products\front\models\Product_model;
use PHPExcel;

class Products_elux_import implements Runnable_interface
{
    /**
     * @throws \ActiveRecord\DatabaseException
     * @throws \PHPExcel_Reader_Exception
     */
    public function run(): void
    {
        $this->log('Rozpoczęcie importu');

        $actualization_file = $this->get_actualization_file();

        $this->log('Poprawnie pobrano plik aktualizacyjny');

        $this->log('Rozpoczęcie aktualizacji produktów.');

        $this->actualize_products($actualization_file);

        $this->log('Zakończono aktualizację produktów.');
    }

    /**
     * @return PHPExcel
     * @throws \PHPExcel_Reader_Exception
     */
    private function get_actualization_file(): PHPExcel
    {
        require_once BASE_PATH . 'lib/phpexcel.class.php';

        $this->log('Pobranie pliku aktualizacji');

        return \PHPExcel_IOFactory::load(BASE_PATH . "data/uploads/actualization/productselux_" . (new DateTime('now'))->format('Ymd') . '.xlsx');
    }

    /**
     * @param PHPExcel $php_excel
     * @throws \ActiveRecord\DatabaseException
     */
    private function actualize_products(PHPExcel $php_excel): void
    {
        $connection = ConnectionManager::get_connection();

        $this->log('Rozpoczęcie iterowania po arkuszu');

        foreach ($php_excel->getWorksheetIterator() as $worksheet)
        {
            $highestRow = $worksheet->getHighestDataRow();
            $highestColumn = $worksheet->getHighestDataColumn();

            for ($row = 2; $row <= $highestRow; $row++)
            {
                $rowData = $worksheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, null, true, false);

                if (!empty($rowData[0][2]))
                {
                    $pnc = $rowData[0][2];
                    $price = number_format((float)$rowData[0][4], 2, '.', '');
                    $points = (int)round($rowData[0][5], 0, PHP_ROUND_HALF_UP);

                    $product = Product_model::find_by_sql(
                        "
                            SELECT *
                            FROM `product`
                            WHERE REPLACE(product_code, ' ', '') = '$pnc'
                        ");

                    if (!empty($product))
                    {
                        $this->log('Aktualizacja produktu ' . $product[0]->id);

                        $connection->query(
                            "
                                UPDATE `product` SET `points` = '$points', `price` = '$price' WHERE `product`.`id` = " . $product[0]->id . ";
                            ");
                    }
                    else
                    {
                        $this->log('Nie znaleziono produktu o PNC = ' . $pnc);
                    }
                }
            }
        }
    }

    /**
     * @param string $log_message
     * @throws \Exception
     */
    private function log(string $log_message): void
    {
        $fp = fopen(BASE_PATH . 'data/log/product_elux_import_' . (new DateTime('now'))->format('Ymd') . '.txt', 'a');
        fwrite($fp, (new DateTime('now'))->format('Y-m-d H:i:s') . " - $log_message \n");
        fclose($fp);
    }
}