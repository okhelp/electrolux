<?php
declare(strict_types=1);

namespace App\modules\products\front\objects\xml;

use ActiveRecord\ConnectionManager;
use App\ed\model\Product_media;
use App\front\models\company\Get_company_services;
use App\modules\products\front\collections\Get_products_attributes;
use App\modules\products\front\interfaces\Xml_element;
use App\modules\products\front\models\Product_model;
use App__Ed__Model__Attribute;
use App__Ed__Model__Img;
use App__Ed__Model__Product_attribute;
use App__Ed__Model__Upload;
use SimpleXMLElement;

class Product implements Xml_element
{
    /** @var string */
    private $id;

    /** @var string */
    private $name;

    /** @var string */
    private $description;

    /** @var string */
    private $parent_category_id;

    /** @var string */
    private $picture_url;

    /** @var string */
    public $brand;

    /** @var string */
    private $product_code;

    /** @var string */
    private $model;

    /** @var string */
    private $category;

    /** @var string */
    private $bar_code;

    /** @var array */
    private $related_product = [];

    /** @var string */
    private $marketing_content;

    /** @var array */
    public $attributes;

    /** @var array */
    private $medias;

    /** @var float */
    private $price;

    /**
     * @param SimpleXMLElement $element
     * @param string $parent_element_id
     */
    public function __construct(SimpleXMLElement $element, string $parent_element_id)
    {
        $this->id = $element['ProductID']->__toString();

        $this->name = empty($element->MasterData->ProdTitle->__toString()) ? $element->MasterData->ProdShortDesc->__toString() : $element->MasterData->ProdTitle->__toString();

        $this->description = empty($element->MasterData->ProdShortDesc->__toString()) ? '' : $element->MasterData->ProdShortDesc->__toString();

        $this->parent_category_id = $parent_element_id;

        $this->picture_url = count($element->Medias) ? $this->get_picture($element->Medias) : '';

        $this->brand = $element->MasterData->Brand->__toString();

        $this->product_code = $element->MasterData->ProductCode->__toString();

        $this->model = $element->MasterData->ModelD->__toString();

        $this->category = $element->MasterData->Category->__toString();

        $this->bar_code = $element->MasterData->BarCode->__toString();

        $this->marketing_content = $this->build_benefits($element);

        $this->related_product = $this->get_related_products($element);

        $this->attributes = $this->fetch_attributes($element);

        $this->price = $this->fetch_price($element);

        $this->medias = $this->get_medias($element);
    }

    /**
     * @param array $categories
     * @param array $products
     * @param array $products_attributes
     * @param int $language_id
     * @return mixed|void
     * @throws \ActiveRecord\RecordNotFound
     */
    public function perform_actions(array $categories, array $products, array $products_attributes, int $language_id)
    {
        $_SESSION['id_service'] = $this->brand == 'AEG' ? Get_company_services::ID_SERVICE_AEG : Get_company_services::ID_SERVICE_ELECTROLUX;

        if (empty($products[$this->id]))
        {
            $this->create_if_not_exists($language_id, $products_attributes);
        }
        else
        {
            $product = $products[$this->id];

            if (!empty($this->picture_url))
            {
                $upload_model = new App__Ed__Model__Upload('images');
                $image_id = $upload_model->copy_remote_file($this->picture_url);
            }

            $product_model = Product_model::find($product->{Product_model::COLUMN_ID});

            if(!empty($product_model->{Product_model::COLUMN_CIMG}) && !empty($image_id) && $product_model->{Product_model::COLUMN_CIMG} != $image_id)
            {
                App__Ed__Model__Img::remove_cimg($product_model->{Product_model::COLUMN_CIMG});
                $product_model->{Product_model::COLUMN_CIMG} = null;
            }

            $product_model->{Product_model::COLUMN_PRICE} = $this->price;
            $product_model->{Product_model::COLUMN_ELECTROLUX_ID} = $this->id;
            $product_model->{Product_model::COLUMN_PARENT_ID} = $this->parent_category_id;
            $product_model->{Product_model::COLUMN_STATUS} = Product_model::STATUS_ACTIVE;
            $product_model->{Product_model::COLUMN_CIMG} = empty($image_id) ? null : $image_id;
            $product_model->{Product_model::COLUMN_BRAND} = $this->brand;
            $product_model->{Product_model::COLUMN_PRODUCT_CODE} = $this->product_code;
            $product_model->{Product_model::COLUMN_MODEL} = $this->model;
            $product_model->{Product_model::COLUMN_CATEGORY} = $this->category;
            $product_model->{Product_model::COLUMN_BAR_CODE} = $this->bar_code;
            $product_model->{Product_model::COLUMN_RELATED_PRODUCTS} = !empty($this->related_product) ? json_encode($this->related_product) : null;
            $product_model->name = $this->name;
            $product_model->description = $this->description;
            $product_model->marketing_content = $this->marketing_content;
            $product_model->save();

            $this->add_products_attributes($products_attributes, $product);

            $this->assign_medias($product->{Product_model::COLUMN_ID});
        }

        $_SESSION['id_service'] = Get_company_services::ID_SERVICE_ELECTROLUX;
    }

    /**
     * @param int $language_id
     */
    private function create_if_not_exists(int $language_id, array $products_attributes)
    {
        try
        {
            if (!empty($this->picture_url))
            {
                $upload_model = new App__Ed__Model__Upload('images');
                $image_id = $upload_model->copy_remote_file($this->picture_url);
            }

            $product = new Product_model();

            $product->{Product_model::COLUMN_ELECTROLUX_ID} = $this->id;
            $product->{Product_model::COLUMN_PARENT_ID} = $this->parent_category_id;
            $product->{Product_model::COLUMN_STATUS} = Product_model::STATUS_ACTIVE;
            $product->{Product_model::COLUMN_CIMG} = empty($image_id) ? null : $image_id;
            $product->{Product_model::COLUMN_BRAND} = $this->brand;
            $product->{Product_model::COLUMN_PRODUCT_CODE} = $this->product_code;
            $product->{Product_model::COLUMN_MODEL} = $this->model;
            $product->{Product_model::COLUMN_CATEGORY} = $this->category;
            $product->{Product_model::COLUMN_BAR_CODE} = $this->bar_code;
            $product->{Product_model::COLUMN_SERVICE_ID} = $this->brand == 'AEG' ? 6 : 5;
            $product->{Product_model::COLUMN_RELATED_PRODUCTS} = !empty($this->related_product) ? json_encode($this->related_product) : null;
            $product->name = $this->name;
            $product->description = $this->description;
            $product->marketing_content = $this->marketing_content;
            $product->save();

            if(!empty($this->attributes))
            {
                foreach($this->attributes as $attribute)
                {
                    if (!empty($attribute['id']) && !empty($products_attributes[$attribute['id']]))
                    {
                        $new_attribute = new App__Ed__Model__Product_attribute();
                        $new_attribute->{App__Ed__Model__Product_attribute::COLUMN_ATTRIBUTE_ID} = $products_attributes[$attribute['id']]->{App__Ed__Model__Attribute::COLUMN_ID};
                        $new_attribute->{App__Ed__Model__Product_attribute::COLUMN_PRODUCT_ID} = $product->{Product_model::COLUMN_ID};
                        $new_attribute->{App__Ed__Model__Product_attribute::COLUMN_NAME} = $attribute['value'];
                        $new_attribute->save();
                    }
                }
            }

            if(!empty($this->medias))
            {
                $this->assign_medias((int)$product->{Product_model::COLUMN_ID});
            }

        }
        catch (\Exception $exception)
        {
            //TODO zapis w logu o nieudanej próbie zapisu
        }

    }

    /**
     * @param SimpleXMLElement $medias
     * @return string
     */
    private function get_picture(SimpleXMLElement $medias)
    {
        foreach ($medias->Media as $media)
        {
            if (($media['Type'] == '0P' && $media['Resolution'] == 'PN') || ($media['Type'] == 'Product Standard Image' && $media['Resolution'] == '9'))
            {
                $img_url = filter_var($media->__toString(), FILTER_VALIDATE_URL) ? $media->__toString() : '';

                //w szczególności obraz z drygiego warunku nie ma wprost rozszerzenia, dlatego też sztucznie tworzymy plik
                if(!empty($img_url) && strpos($img_url, '.image?defid=') !== FALSE)
                {
                    $img_url = self::get_photo_data($img_url);
                }

                return $img_url;
            }
        }

        return '';
    }

    private static function get_photo_data($url)
    {

        $return_file_path = '';

        //wyłączam link ssl z powodu błędu - OpenSSL SSL_connect: SSL_ERROR_SYSCALL in connection to www.sesame.electrolux.com:443
        $url = str_replace("https://", "http://", $url);

        //ustawiam podstawowe dane
        $options[CURLOPT_SSL_VERIFYHOST] = false;
        $options[CURLOPT_SSL_VERIFYPEER] = false;
        $options[CURLOPT_SSL_VERIFYSTATUS] = false;
        $options[CURLOPT_SSLVERSION] = 5;
        $options[CURLOPT_IPRESOLVE] = CURL_IPRESOLVE_V4;

        $options[CURLOPT_COOKIEJAR] = BASE_PATH . 'data/electrolux_cookie.txt';
        $options[CURLOPT_USERAGENT] = "Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 "
            . "(KHTML, like Gecko) Chrome/48.0.2564.116 Safari/537.36";
        $options[CURLOPT_RETURNTRANSFER] = true;
        $options[CURLOPT_FOLLOWLOCATION] = true;

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);

        //pobieram domyślne ustawienia
        curl_setopt_array($ch,$options);

        $curl_return = curl_exec($ch);

        curl_close($ch);

        //wydobywam rozszerzenie z urla
        $url_arr = explode('ext=.', $url);

        //pobieram rozszerzenie
        $file_extension = $url_arr[1];

        //tworzę nazwę nowego pliku
        $file_name = md5($url) . "." . $file_extension;

        //określam katalog docelowy
        $file_path = BASE_PATH . "data/tmp/import_photos/";
        if(!is_dir($file_path))
        {
            create_dirs($file_path);
        }

        //tworzę plik
        file_put_contents($file_path . $file_name,$curl_return);

        if(is_file($file_path . $file_name))
        {
            $return_file_path = $file_path . $file_name;
        }

        return $return_file_path;
    }

    private function build_benefits(SimpleXMLElement $element): string
    {
        $benefits_content = '';

        if (count($element->Benefits->MarketingContent))
        {
            $contents = [];

            for ($i = 0; $i < count($element->Benefits->MarketingContent); $i++)
            {
                $content = '';

                $content .= '<li class="product_more__specification_point"><p class="product_more__specification_point--bold">' . $element->Benefits->MarketingContent[$i]->Bullet->__toString() . '</p>';

                $content .= "\n<span>";

                $content .= !empty($element->Benefits->MarketingContent[$i]->LongDesc->__toString()) ? $element->Benefits->MarketingContent[$i]->LongDesc->__toString() :
                    $element->Benefits->MarketingContent[$i]->ShortDesc->__toString();

                $content .= "</span></li>";

                if (!empty($content))
                {
                    $contents[] = $content;
                }
            }

            $benefits_content = implode("\n", $contents);
        }

        return $benefits_content;
    }

    private function fetch_attributes(SimpleXMLElement $element): array
    {
        $attributes = [];

        if (!empty($element->Containers->Container) && count($element->Containers->Container))
        {
            for ($i = 0; $i < count($element->Containers->Container); $i++)
            {
                $container_name = $element->Containers->Container[$i]['ContainerName']->__toString();

                $container_name_exploded = explode('_', $container_name);

                if ($container_name_exploded[0] == 'STechSpec' || $container_name_exploded[0] == 'Nav')
                {
                    for ($j = 0; $j < count($element->Containers->Container[$i]->Attribute); $j++)
                    {
                        $attribute_data = $element->Containers->Container[$i]->Attribute[$j];

                        $attributes[$attribute_data['AttributeID']->__toString()] = [
                            'id'    => $attribute_data['AttributeID']->__toString(),
                            'name'  => $attribute_data['TranslatedName']->__toString(),
                            'value' => $attribute_data->__toString(),
                        ];
                    }
                }
            }
        }

        return $attributes;
    }

    private function fetch_price(SimpleXMLElement $element): float
    {
        if (!empty($element->Containers->Container) && count($element->Containers->Container))
        {
            for ($i = 0; $i < count($element->Containers->Container); $i++)
            {
                $container_name = $element->Containers->Container[$i]['ContainerName']->__toString();

                $container_name_exploded = explode('_', $container_name);

                if ($container_name_exploded[0] == 'STechSpec')
                {
                    for ($j = 0; $j < count($element->Containers->Container[$i]->Attribute); $j++)
                    {
                        $attribute_data = $element->Containers->Container[$i]->Attribute[$j];

                        if(!empty($attribute_data['Name']) && $attribute_data['Name']->__toString() == 'ListPrice')
                        {
                            return (float)$attribute_data;
                        }
                    }
                }
            }
        }

        return (float)0;
    }

    private function add_products_attributes(array $existing_products_attributes, Product_model $product)
    {
        if (!empty($this->attributes))
        {
            $connection = ConnectionManager::get_connection();
            $connection->query("DELETE FROM product_attribute WHERE product_id=" . $product->{Product_model::COLUMN_ID});

            foreach ($this->attributes as $attribute)
            {
                $new_attribute = new App__Ed__Model__Product_attribute();
                $new_attribute->{App__Ed__Model__Product_attribute::COLUMN_ATTRIBUTE_ID} = $existing_products_attributes[$attribute['id']][$_SESSION['id_service']]->{App__Ed__Model__Attribute::COLUMN_ID};
                $new_attribute->{App__Ed__Model__Product_attribute::COLUMN_PRODUCT_ID} = $product->{Product_model::COLUMN_ID};
                $new_attribute->{App__Ed__Model__Product_attribute::COLUMN_NAME} = $attribute['value'];
                $new_attribute->save();
            }
        }
    }

    private function get_related_products(SimpleXMLElement $element): array
    {
        $related_products = [];

        if (!empty($element->RelatedProducts->LinkProductID) && count($element->RelatedProducts->LinkProductID) > 1)
        {
            for ($i = 0; $i < count($element->RelatedProducts->LinkProductID); $i++)
            {
                $related_product = $element->RelatedProducts->LinkProductID[$i]->__toString();

                $related_products[] = $related_product;
            }
        }
        else if (!empty($element->RelatedProducts->LinkProductID))
        {
            $related_products[] = $element->RelatedProducts->LinkProductID->__toString();
        }

        return $related_products;
    }

    private function get_medias(SimpleXMLElement $element): array
    {
        $medias = [];

        if (count($element->Medias))
        {
            foreach ($element->Medias->Media as $media)
            {
                if ($media['Classification'] == 'Picture' && $media['Resolution'] == 'PN' && $media['Type'] != '0D' && $media['TypeName'] != 'Key Visual')
                {
                    if (filter_var($media->__toString(), FILTER_VALIDATE_URL))
                    {
                        $medias[] = ['type' => 'picture', 'url' => $media->__toString()];
                    }
                }
                if ($media['Classification'] == 'Picture' && $media['Resolution'] == '19' && $media['Type'] != 'Product Image' && $media['TypeName'] != 'Product Image')
                {
                    if (filter_var($media->__toString(), FILTER_VALIDATE_URL))
                    {
                        $medias[] = ['type' => 'picture', 'url' => $media->__toString()];
                    }
                }
                if ($media['Classification'] == 'Picture' && $media['Resolution'] == '19' && $media['Type'] != 'Product Image' && $media['TypeName'] != 'Product Image')
                {
                    if (filter_var($media->__toString(), FILTER_VALIDATE_URL))
                    {
                        $medias[] = ['type' => 'picture', 'url' => $media->__toString()];
                    }
                }
                else if ($media['Classification'] == 'Video' && $media['Resolution'] == 'Online_Desk_WEBM_HDReady')
                {
                    if (filter_var($media->__toString(), FILTER_VALIDATE_URL))
                    {
                        $medias[] = ['type' => 'video', 'url' => $media->__toString()];
                    }
                }
                else if ($media['Classification'] == 'Picture' && $media['Resolution'] == 'JPEG_LARGE' && $media['Type'] == '0D')
                {
                    if (filter_var($media->__toString(), FILTER_VALIDATE_URL))
                    {
                        $medias[] = ['type' => 'installation_diagram', 'url' => $media->__toString()];
                    }
                }
                else if ($media['Classification'] == 'Document' && $media['Resolution'] == 'PDF' && $media['LanguageCode'] == 'en')
                {
                    if (filter_var($media->__toString(), FILTER_VALIDATE_URL))
                    {
                        $medias[] = ['type' => 'user_manual', 'url' => $media->__toString()];
                    }
                }
            }
        }

        return $medias;
    }

    private function assign_medias(int $id_product)
    {
        $connection = ConnectionManager::get_connection();

        $connection->query("DELETE FROM product_media WHERE id_product=" . $id_product);

        foreach($this->medias as $media)
        {
            if($media['type'] == 'picture')
            {
                $media_model = new Product_media();
                $media_model->{Product_media::COLUMN_LINK} = $media['url'];
                $media_model->{Product_media::COLUMN_TYPE} = 'picture';
                $media_model->{Product_media::COLUMN_ID_PRODUCT} = $id_product;
                $media_model->save();
            }
            elseif($media['type'] == 'video')
            {
                $media_model = new Product_media();
                $media_model->{Product_media::COLUMN_LINK} = $media['url'];
                $media_model->{Product_media::COLUMN_TYPE} = 'video';
                $media_model->{Product_media::COLUMN_ID_PRODUCT} = $id_product;
                $media_model->save();
            }
            elseif($media['type'] == 'installation_diagram')
            {
                $media_model = new Product_media();
                $media_model->{Product_media::COLUMN_LINK} = $media['url'];
                $media_model->{Product_media::COLUMN_TYPE} = 'installation_diagram';
                $media_model->{Product_media::COLUMN_ID_PRODUCT} = $id_product;
                $media_model->save();
            }
            elseif($media['type'] == 'user_manual')
            {
                $media_model = new Product_media();
                $media_model->{Product_media::COLUMN_LINK} = $media['url'];
                $media_model->{Product_media::COLUMN_TYPE} = 'user_manual';
                $media_model->{Product_media::COLUMN_ID_PRODUCT} = $id_product;
                $media_model->save();
            }
        }
    }
}