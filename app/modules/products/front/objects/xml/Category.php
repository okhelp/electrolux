<?php
declare(strict_types=1);

namespace App\modules\products\front\objects\xml;

use App\front\models\company\Get_company_services;
use App\modules\products\front\interfaces\Xml_element;
use App\modules\products\front\models\Product_category_lang_model;
use App\modules\products\front\models\Product_category_model;

class Category implements Xml_element
{
    /** @var string */
    private $id;

    /** @var string */
    private $name;

    /** @var string|null */
    private $parent_id;

    /**
     * @param array $categories
     * @param array $products
     * @param array $products_attributes
     * @param int $language_id
     * @return mixed|void
     */
    public function perform_actions(array $categories, array $products, array $products_attributes, int $language_id)
    {
        foreach ([Get_company_services::ID_SERVICE_AEG, Get_company_services::ID_SERVICE_ELECTROLUX] as $id_service)
        {
            $_SESSION['id_service'] = $id_service;

            if (empty($categories[$this->id][$id_service]))
            {
                $this->create_if_not_exists();
            }
            else
            {
                $category = $categories[$this->id][$id_service];
                if ($category->name != $this->name)
                {
                    $this->update_if_category_name_changed($category);
                }

                if ($category->{Product_category_model::COLUMN_PARENT_ID} != $this->parent_id)
                {
                    $this->update_if_category_parent_changed($category);
                }
            }
        }

    }

    /**
     * @param \SimpleXMLElement $element
     * @param string $parent_id
     */
    public function __construct(\SimpleXMLElement $element, string $parent_id)
    {
        $this->name = empty($element['Name']->__toString()) ? '' : $element['Name']->__toString();

        $this->id = empty($element['NavigationID']->__toString()) ? '' : $element['NavigationID']->__toString();

        $this->parent_id = empty($parent_id) ? null : $parent_id;
    }

    private function create_if_not_exists()
    {
        try
        {
            $product_category_model = new Product_category_model();
            $product_category_model->{Product_category_model::COLUMN_PARENT_ID} = $this->parent_id;
            $product_category_model->{Product_category_model::COLUMN_ELECTROLUX_ID} = $this->id;
            $product_category_model->name = $this->name;
            $product_category_model->seo_name = url_slug($this->name);
            $product_category_model->id_lang = 1;
            $product_category_model->id_service = 5;
            $product_category_model->save();

            $product_category_model = new Product_category_model();
            $product_category_model->{Product_category_model::COLUMN_PARENT_ID} = $this->parent_id;
            $product_category_model->{Product_category_model::COLUMN_ELECTROLUX_ID} = $this->id;
            $product_category_model->name = $this->name;
            $product_category_model->seo_name = url_slug($this->name);
            $product_category_model->id_lang = 1;
            $product_category_model->id_service = 6;
            $product_category_model->save();

        } catch (\Exception $exception)
        {
            //TODO zapis w logu informacji o błędzie
        }
    }

    /**
     * @param Product_category_model $category
     */
    private function update_if_category_name_changed(Product_category_model $category)
    {
        try
        {
            $category_model = Product_category_model::find($category->{Product_category_model::COLUMN_ID});
            $category_model->name = $this->name;
            $category_model->save();
        } catch (\Exception $exception)
        {
            //TODO zapis w logu informacji o błędzie
        }
    }

    /**
     * @param Product_category_model $category
     */
    private function update_if_category_parent_changed(Product_category_model $category)
    {
        try
        {
            $category_model = Product_category_model::find($category->{Product_category_model::COLUMN_ID});
            $category_model->{Product_category_model::COLUMN_PARENT_ID} = $this->parent_id;
            $category_model->save();
        } catch (\Exception $exception)
        {
            //TODO zapis w logu informacji o błędzie
        }
    }
}