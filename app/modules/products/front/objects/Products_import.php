<?php
declare(strict_types=1);

namespace App\modules\products\front\objects;

use App\modules\products\front\interfaces\Runnable_interface;
use App\modules\products\front\objects\xml\Marketing_content;
use App\modules\products\front\objects\xml\Navigation;
use App\modules\products\front\objects\xml\Xml;
use App__Ed__Model__Img;
use Lib__PDO;

class Products_import implements Runnable_interface
{
    const ACTUALIZATION_FILE_PATH = BASE_PATH . 'data/uploads/actualization/';

    /** @var Xml_data_parser */
    private $xml_data_parser;

    /** @var \SimpleXMLElement */
    private $xml_file_data;

    public function run()
    {
        $this->get_actualization_file();

        $this->parse_xml_data();
    }

    private function get_actualization_file()
    {
        $actual_date = (new \DateTime())->format('Ymd');

        $file_name = 'products_' . $actual_date . '.xml';

        if(!file_exists(self::ACTUALIZATION_FILE_PATH . $file_name))
        {
            throw new \Exception('Plik aktualizacyjny nie został znaleziony');
        }

        $this->xml_file_data = simplexml_load_file(self::ACTUALIZATION_FILE_PATH . $file_name);
    }

    private function parse_xml_data()
    {
        $this->xml_data_parser = new Xml_data_parser($this->xml_file_data);

//        $this->set_inactive_all_products();
//        $this->remove_products_images();

        $this->xml_data_parser->parse();
    }

    private function set_inactive_all_products()
    {
        $pdo = new Lib__PDO();
        $pdo->query("
            UPDATE product
            SET product.status = 0
        ");
    }

    private function remove_products_images()
    {
        $images = App__Ed__Model__Img::find_by_sql("
            SELECT *
            FROM img
            WHERE id IN (SELECT cimg FROM product)
        ");

        if(!empty($images))
        {
            foreach ($images as $image)
            {
                App__Ed__Model__Img::remove_cimg($image->id);
            }
        }
    }
}