<?php
declare(strict_types=1);

namespace App\modules\products\front\objects;

class Filter_subcategories
{
    const SUBCATEGORIES_ARRAY = [
        //piekarniki
        773 => [
            '322ED085-790D-425B-ABFF-CB342DCCA4EB' => 'parowy',
            '14F888CF-B197-4D6E-A4B6-43A04421FBD3' => 'tradycyjny',
            '45206AFA-0D7C-46FD-9BE4-1E0FC52CB35D' => 'kompaktowy'
        ],
        //zmywarki
        801 => [],
        //płyty
        779 => [],
        //Okapy
        789 => [],
        //Mikrofale
        796 => [],
        //Chłodziarkozamrażarki
        760 => [
            '3C2FF9DF-185E-4694-841A-0CFAB6040665' => 'Do zabudowy',
            'E9620178-78F0-4C7C-8722-8834C3348BE0' => 'Wolnostojące',
        ],
        //szuflady
        778 => [],
        //pralki
        810 => [
            'F9BB2037-DA70-4D0D-BB35-B56C510F4037' => 'Pełnogabarytowe',
            '80933F1A-0052-422D-B2DE-7CCDDD88CD94' => 'Pralki SLIM',
            '893F320E-510C-4194-A6E3-FF07774178B4' => 'Pralki TOP',
        ],
        //akcesoria
        925 => [],
    ];

    public static function filter(int $id_category, array $subcategories): array
    {
        $subcategories_to_return = [];

        if(!empty(self::SUBCATEGORIES_ARRAY[$id_category]))
        {
            $valid_subcategories = self::SUBCATEGORIES_ARRAY[$id_category];

            $valid_subcategories_ids  = array_keys($valid_subcategories);

            foreach ($subcategories as $id => $subcategory)
            {
                if(in_array($id, $valid_subcategories_ids))
                {
                    $subcategories_to_return[$valid_subcategories[$id]] = $subcategory;
                }
            }
        }

        return $subcategories_to_return;
    }
}