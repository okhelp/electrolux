<?php

namespace App\modules\products\front\objects;

use App\modules\products\ed\objects\Products_categories_tree;
use App\modules\products\front\collections\Get_all_products_categories_collection;
use App\modules\products\front\collections\Get_product_category_by_electrolux_id;
use App\modules\products\front\collections\Get_root_category_id;
use Lib__Memcache;

class Products_main_menu
{
    //kuchnia
    const PIEKARNIKI         = '2A2A0502-F118-4CFC-9C00-BFE8E391C415';
    const PŁYTY              = 'DCF4CC98-95C6-4BBD-B02D-30C7B85398CD';
    const ZMYWARKI           = '99B96C56-897C-4E8A-9B9A-02A78BFDCE41';
    const OKAPY              = '5D678402-B0CA-4F0F-A91C-B636CE1F2B61';
    const MIKROFALE          = 'A9C6B111-BB22-46DF-905E-F9797A1558CA';
    const LODOWKI            = '7132D767-1138-46B9-BFA2-C3798FAB1C93';
   // const LODOWKI            = 'E9E1D7A9-7443-4E72-9F29-CE59B68EAA3A';

    //const KAWARKI            = '05851A5F-D951-428C-AE90-0148A6FDF181';
    
    const KAWARKI            = 'E2543EA0-736D-482D-80F2-4B35BEC95992';
    const WINIARKI           = 'C98E921F-12B5-451F-8413-342C2E1E889E';
    const SZUFLADY           = '38E49402-4213-4BBE-977A-FC83A0798E90';
    const MALE_AGD           = '18771F38-E001-421D-828C-ADCE4B5F33FE';
    const AKCESORIA_KUCHENNE = '';

    //łazienka
    const PRALKI          = 'F6A44687-C942-4F20-A025-B7183216EB5D';
    const SUSZARKI        = 'BE6A7346-724D-4EA2-9ADA-ABD9446D5443';
    const PRALKO_SUSZARKI = 'F3BA5461-B098-441D-9A2B-43E1E6A166E8';

    //akcesoria
    //const ODKURZACZE              = '7D7C1D2F-AFBE-4CDC-8F73-F7F6F4EB2CCC';
    const ODKURZACZE              = '73D499B7-1CC8-4E90-831C-B1A649DCB98C';
    //const ROBOTY_KUCHENNE         = '21F67EF1-24D9-422D-8BF5-163AB3365884';
    const ROBOTY_KUCHENNE         = 'C9894B03-31F3-4E6B-B5FC-94E93AAD6580';
    //const AKCESORIA               = 'B896FF59-84F4-46E2-B4AE-77A85C53818B';
    const AKCESORIA               = '2648D43E-76B7-4255-A21F-B0D94C713CE3';
    const AKCESORIA_DO_GOTOWANIA  = '';
    const AKCESORIA_DO_SPRZATANIA = '';
    const ZESTAWY = 'F7D7BC70-3751-4A0A-8DFF-3BB52F2DE595';

    //TODO część kategorii nie ma obecnie w xml

    public static function get(): array
    {
        $cache_key = __METHOD__;

        $cache_data = Lib__Memcache::get($cache_key);

        if ($cache_data)
        {
            return unserialize($cache_data);
        }

        $all_categories = Get_all_products_categories_collection::get($_SESSION['id_lang']);

        $product_menu = [];

        $product_menu['KUCHNIA'] = [
            ['name' => 'Piekarniki', 'url' => self::get_category_url(self::PIEKARNIKI, $all_categories)],
            ['name' => 'Płyty', 'url' => self::get_category_url(self::PŁYTY, $all_categories)],
            ['name' => 'Zmywarki', 'url' => self::get_category_url(self::ZMYWARKI, $all_categories)],
            ['name' => 'Okapy', 'url' => self::get_category_url(self::OKAPY, $all_categories)],
            ['name' => 'Mikrofale', 'url' => self::get_category_url(self::MIKROFALE, $all_categories)],
            ['name' => 'Chłodziarko-zamrażarki', 'url' => self::get_category_url(self::LODOWKI, $all_categories)],
            ['name' => 'Kawiarki', 'url' => self::get_category_url(self::KAWARKI, $all_categories)],
            ['name' => 'Winiarki', 'url' => self::get_category_url(self::WINIARKI, $all_categories)],
            ['name' => 'Szuflady', 'url' => self::get_category_url(self::SZUFLADY, $all_categories)],
//            ['name' => 'Małe AGD', 'url' => self::get_category_url(self::MALE_AGD, $all_categories)],
            //['name' => 'Akcesoria kuchenne', 'url' => '#'],
        ];

        $product_menu['ŁAZIENKA'] = [
            ['name' => 'Pralki', 'url' => self::get_category_url(self::PRALKI, $all_categories)],
            ['name' => 'Suszarki', 'url' => self::get_category_url(self::SUSZARKI, $all_categories)],
            ['name' => 'Pralko-suszarki', 'url' => self::get_category_url(self::PRALKO_SUSZARKI, $all_categories)],
        ];

        $product_menu['AKCESORIA'] = [
            ['name' => 'Odkurzacze', 'url' => self::get_category_url(self::ODKURZACZE, $all_categories)],
            ['name' => 'Roboty kuchenne', 'url' => self::get_category_url(self::ROBOTY_KUCHENNE, $all_categories)],
            ['name' => 'Chemia i akcesoria', 'url' => self::get_category_url(self::AKCESORIA, $all_categories)],
            //['name' => 'Akcesoria do gotowania', 'url' => '#'],
            //['name' => 'Akcesoria do sprzątania', 'url' => '#'],
        ];

        $product_menu['ZESTAWY'] = [
            ['name' => 'Zestawy promocyjne', 'url' => self::get_category_url(self::ZESTAWY, $all_categories)],
        ];
        Lib__Memcache::set(
            $cache_key, serialize($product_menu), [
            'class_name' => get_class(),
            'key_name'   => $cache_key,
        ]);

        return $product_menu;
    }

    /**
     * @param string $category_id
     * @param array $all_categories
     * @return string
     */
    private static function get_category_url(string $category_id, array $all_categories)
    {
        $cook_category_path = Products_categories_tree::build_path($category_id, $all_categories);

        $root_category = Get_root_category_id::get();

        $cook_category_path_elements = [];
        foreach ($cook_category_path as $path_element)
        {
            if($path_element != $root_category)
            {
                $cook_category_path_elements[$path_element] = Get_product_category_by_electrolux_id::get($path_element);
            }
        }

        $seo_names = array_map(
            function($path_element) {
                return !empty($path_element->seo_name) ? $path_element->seo_name : '';
            }, $cook_category_path_elements);

        $seo_names = array_reverse($seo_names);

        return BASE_URL . 'products/category/' . implode('/', $seo_names) . '/';
    }
}
