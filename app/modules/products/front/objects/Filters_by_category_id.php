<?php
declare(strict_types=1);

namespace App\modules\products\front\objects;

use App\front\models\company\Get_company_services;

class Filters_by_category_id
{
    const FILTERS_ARRAY = [
        //Piekarniki
        '2A2A0502-F118-4CFC-9C00-BFE8E391C415' => [
            //Rodzaje pary
            [
                'id'      => '21222A96-20A6-4C09-BAF5-D28DA7BD35CE',
                'name'    => 'Rodzaje pary',
                'options' => [
                    'SteamPro'   => 'SteamPro',
                    'SteamBoost' => 'SteamBoost',
                    'SteamCrisp' => 'SteamCrisp',
                    'SteamBake'  => 'SteamBake',
                ],
            ],
            //Rodzaje czyszczenia
            [
                'id'      => '818F2BA8-8765-4C0E-A9F4-018A8301D506',
                'name'    => 'Rodzaje czyszczenia',
                'options' => [
                    'pyroliza'                                => 'Czyszczenie pyrolityczne',
                    'powłoka katalityczna'                    => 'Czyszczenie katalityczne',
                    'para'                                    => 'Czyszczenie parowe',
                    'emalia samoczyszcząca/czyszczenie wodne' => 'Emaliowana powłoka',
                ],
            ],
            //Kolor
            [
                'id'      => '6B02D10A-DF22-4E35-93A4-E077C9DB447B',
                'name'    => 'Kolor',
                'options' => [
                    'czarny/stal szlachetna z powłoką Antifingerprint' => 'Czarno-stalowy',
                    'czarny'                                           => 'Czarny',
                    'biały'                                            => 'Biały',
                ],
            ],
            //Connectivity
            [
                'id'      => 'B9CEACCF-8A4F-4C08-92F5-528AF1E6E4F7',
                'name'    => 'Connectivity',
                'options' => [
                    'Wi-Fi' => 'Tak',
                    'nie'   => 'Nie',
                ],
            ],
        ],
        //Zmywarki
        '99B96C56-897C-4E8A-9B9A-02A78BFDCE41' => [
            //QuickSelect
            [
                'id'      => '1CD16B53-218B-4731-8041-4DC0CC5FA558',
                'name'    => 'QuickSelect',
                'options' => [
                    'poziom 4, wyświetlacz TFT' => 'Poziom 4',
                    'poziom 3'                  => 'Poziom 3',
                    'poziom 2'                  => 'Poziom 2',
                    'poziom1'                  => 'Poziom 1',
                ],
            ],
            //3 kosz
            [
                'id'      => 'CF04237B-096F-4A1E-BE7B-956996C7182D',
                'name'    => '3 kosz',
                'options' => [
                    'tak' => 'Tak',
                    'nie' => 'Nie',
                ],
            ],
            //ComfortLift
            [
                'id'      => 'E3CCAAC6-45DE-4B81-A78C-23A942848B71',
                'name'    => 'ComfortLift',
                'options' => [
                    'tak' => 'Tak',
                    'nie' => 'Nie',
                ],
            ],
            //Oświetlenie wnętrza
            [
                'id'      => '5BC989BD-B749-415A-947F-B3DED41064A1',
                'name'    => 'Oświetlenie wnętrza',
                'options' => [
                    'tak' => 'Tak',
                    'nie' => 'Nie',
                ],
            ],
            //Szuflada MaxiFlex
            [
                'id'      => 'A07D8EA0-148F-4349-B014-79764EEDF87F',
                'name'    => 'Szuflada MaxiFlex',
                'options' => [
                    'tak' => 'Tak',
                    'nie' => 'Nie',
                ],
            ],
            //TimeBeam
            [
                'id'      => '833E6E2C-F8A0-404E-9B0F-2668FD517DBD',
                'name'    => 'TimeBeam',
                'options' => [
                    'tak' => 'Tak',
                    'nie' => 'Nie',
                ],
            ],
            //Beam on floor
            [
                'id'      => '7AC3213B-46F1-4E31-B4B4-5BFC420F4EBA',
                'name'    => 'Beam on floor',
                'options' => [
                    'Time Beam' => 'Tak',
                    'brak' => 'Nie',
                ],
            ],
            //GlassCare
            [
                'id'      => 'F3879EC3-2C03-4EE1-AF6C-0FA67F04294D',
                'name'    => 'GlassCare',
                'options' => [
                    'tak' => 'Tak',
                    'nie' => 'Nie',
                ],
            ],
        ],
        //Płyty
        'DCF4CC98-95C6-4BBD-B02D-30C7B85398CD' => [
            //Szerokość
            [
                'id'      => '29D02690-B7DB-49C3-8DEB-CFDB79DB5CFA',
                'name'    => 'Szerokość',
                'options' => [
                    '40-59' => '60 cm',
                    '70-79' => '70 cm',
                    '80-89' => '80 cm',
                    '>80' => '90 cm',
                ],
            ],
            //Kolor
            [
                'id'      => '41433E9F-09C9-44EE-ADCC-B3A0B82BA92D',
                'name'    => 'Kolor',
                'options' => [
                    'czarny'    => 'Czarny',
                    'grafitowy' => 'Grafitowy',
                    'biel'      => 'Biały',
                ],
            ],
            //Seria - brak danych wartości
            [
                'id'      => '0704906E-FF72-4294-BAE9-F14EAF3A40C6',
                'name'    => 'Seria',
                'options' => [
                    'SensePro'  => 'SensePro',
                    'SenseFry'  => 'SenseFry',
                    'Pure'       => 'Pure',
                    'SenseBoil' => 'SenseBoil',
                ],
            ],
            //H2H
            [
                'id'      => 'DACC32EC-841B-4E51-AEFB-C59ED85C5456',
                'name'    => 'H2H',
                'options' => [
                    'tak' => 'Tak',
                    'nie' => 'Nie',
                ],
            ],
        ],
        //Okapy
        '5D678402-B0CA-4F0F-A91C-B636CE1F2B61' => [
            //Rodzaj
            [
                'id'      => 'DEC78DF5-D37C-4DA7-9692-0632CF18D2D1',
                'name'    => 'Rodzaj',
                'options' => [
                    'wyspowy'     => 'Wyspowy',
                    'kominowy'    => 'Kominowy przyścienny',
                    'kominowy'    => 'Kominowy ukośny',
                    'do zabudowy' => 'Podszafkowy',
                    'teleskopowy' => 'Teleskopowy',
                ],
            ],
            //Szerokość
            [
                'id'      => 'C60303B1-FDB0-423E-A81A-F8CC36690E56',
                'name'    => 'Szerokość',
                'options' => [
                    '90' => '90 cm',
                    '80' => '80 cm',
                    '60' => '60 cm',
                ],
            ],
        ],
        //Mikrofale
        'A9C6B111-BB22-46DF-905E-F9797A1558CA' => [
            //Kolor
            [
                'id'      => 'BBD81451-8596-44EC-B9D0-67C060D7BBA8',
                'name'    => 'Kolor',
                'options' => [
                    'czarny/stal szlachetna z powłoką Antifingerprint' => 'Czarno-stalowy',
                    'czarny'                                           => 'Czarny',
                    'biały'                                            => 'Biały',
                ],
            ],
        ],
        //Szuflady
        '38E49402-4213-4BBE-977A-FC83A0798E90' => [
            //Kolor
            [
                'id'      => '56137E7C-68DF-4075-96E2-5B2A51AB9CD0',
                'name'    => 'Przeznaczenie',
                'options' => [
                    'Szuflada do podgrzewania'          => 'Podgrzewanie',
                    'Szuflada do pakowania próżniowego' => 'Pakowanie próżniowe',
                ],
            ],
        ],
        //Pralki
        'F6A44687-C942-4F20-A025-B7183216EB5D' => [
            //Funkcje parowe
            [
                'id'      => '9FC818C2-713F-4334-836A-98D3C2A72580',
                'name'    => 'Funkcje parowe',
                'options' => [
                    'tak' => 'Tak',
                    'nie' => 'Nie',
                ],
            ],
            //Connectivity
            [
                'id'      => 'B9CEACCF-8A4F-4C08-92F5-528AF1E6E4F7',
                'name'    => 'Connectivity',
                'options' => [
                    'ready' => 'Tak',
                    'nie'   => 'Nie',
                ],
            ],
        ],
        //Akcesoria ??
    ];

    public static function get(string $category_id): array
    {
        $filters_to_return = [];

        if (!empty(self::FILTERS_ARRAY[$category_id]))
        {
            $filters_to_return = self::FILTERS_ARRAY[$category_id];
        }

        //usunięcie filtrów AEG - zmywarki
        if($category_id == '99B96C56-897C-4E8A-9B9A-02A78BFDCE41' && $_SESSION['id_service'] == Get_company_services::ID_SERVICE_AEG)
        {
            unset($filters_to_return[0]);
            unset($filters_to_return[4]);
        }

        //filtry AEG - piekarniki
        if($category_id == '2A2A0502-F118-4CFC-9C00-BFE8E391C415' && $_SESSION['id_service'] == Get_company_services::ID_SERVICE_AEG)
        {
            unset($filters_to_return[0]);

            array_unshift(
                $filters_to_return, [
                'id'      => 'CFBF24AB-1A19-42E0-8F3C-230C5B8E2022',
                'name'    => 'Rodzaje pary',
                'options' => [
                    'parowe gotowanie SousVide'                      => 'SousVide',
                    'gotowanie/pieczenie na parze CombiSteam Deluxe' => 'CombiSteam Deluxe',
                    'uderzenie pary PlusSteam'                       => 'PlusSteam',
                ],
            ]);
        }

        //filtry AEG - płyty
        if($category_id == 'DCF4CC98-95C6-4BBD-B02D-30C7B85398CD' && $_SESSION['id_service'] == Get_company_services::ID_SERVICE_AEG)
        {
            $filters_to_return[2] = [
                'id'      => '0704906E-FF72-4294-BAE9-F14EAF3A40C6',
                'name'    => 'Seria',
                'options' => [
                    'SenseFry' => 'SenseFry',
                    'Pure'     => 'Pure',
                ],
            ];

            $filters_to_return[4] = $filters_to_return[3];
            $filters_to_return[3] = $filters_to_return[2];

            $filters_to_return[2] = [
                'id'      => '90E1272F-15F0-4170-B0A7-9FCC69B65ACB',
                'name'    => 'Slim-Fit',
                'options' => [
                    'tak' => 'Tak',
                ],
            ];
        }

        return $filters_to_return;
    }
}