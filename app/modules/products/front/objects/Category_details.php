<?php
declare(strict_types=1);

namespace App\modules\products\front\objects;

use App\modules\products\front\collections\Get_products_assigned_to_category_collection;
use App\modules\products\front\collections\Get_products_categories_by_seo_names_collection;
use App\modules\products\front\collections\Get_products_subcategories_collection;
use App\modules\products\front\models\Product_category_model;

class Category_details
{
    public static function get_category_data_by_url(string $url)
    {
        $url = rtrim($url, '/');

        $categories_data = [
            'subcategories' => [],
            'products'      => [],
        ];

        $category_to_search = [];

        $categories_seo_names = explode('/', str_replace(BASE_DIR . 'products/category/', '', $url));
        $categories = Get_products_categories_by_seo_names_collection::get($categories_seo_names);

        foreach ($categories as $category)
        {
            if ($category->seo_name == end($categories_seo_names))
            {
                $penultimate_element_of_url = count($categories_seo_names) > 1 ? $categories_seo_names[count($categories_seo_names) - 2] :
                    $categories_seo_names[count($categories_seo_names) - 1];
                if (!empty($categories[$category->parent_id]) && $categories[$category->parent_id]->seo_name == $penultimate_element_of_url)
                {
                    $category_to_search = $category;
                }
                else
                {
                    if (count($categories_seo_names) == 1)
                    {
                        $category_to_search = $category;

                        break;
                    }
                }
            }
        }

        if (!empty($category_to_search))
        {
            $categories_data['subcategories'] = Get_products_subcategories_collection::get($category_to_search->electrolux_id, $_SESSION['id_lang'], false);

            $url = str_replace(BASE_DIR, '', $url);

            foreach ($categories_data['subcategories'] as $subcategory_data)
            {
                $subcategory_data->assign_attribute('url', BASE_URL . $url . '/' . $subcategory_data->seo_name);
            }
        }

        $categories_data['category_id'] = empty($category_to_search) ? 0 : $category_to_search->id;
        $categories_data['category'] = empty($category_to_search) ? null : $category_to_search;

        $subcategories_ids = [];
        if (!empty($categories_data['subcategories']))
        {
            $subcategories_ids = array_map(
                function($subcategory) {
                    return $subcategory->electrolux_id;
                }, Get_products_subcategories_collection::get($category_to_search->electrolux_id, $_SESSION['id_lang'], true));
        }

        $subcategories_ids[$category_to_search->electrolux_id] = $category_to_search->electrolux_id;

        $products_assigned_to_category = Get_products_assigned_to_category_collection::get($subcategories_ids, $_SESSION['id_lang'], '15', 'e_lux_desc', (int)$_SESSION['id_service']);

        if (!empty($products_assigned_to_category))
        {
            foreach ($products_assigned_to_category as $product_assigned_to_category)
            {
                $product_assigned_to_category->assign_attribute('url', BASE_URL . 'products/product.html?id=' . $product_assigned_to_category->id);
            }
        }

        $categories_data['products'] = $products_assigned_to_category;

        return $categories_data;
    }

    /**
     * @param int $category_id
     * @param string $limit
     * @param string $order
     * @param array $subcategories
     * @param array $filters
     * @param int $page
     * @return array
     * @throws \ActiveRecord\RecordNotFound
     */
    public static function get_category_products(int $category_id, string $limit, string $order, array $subcategories, array $filters, ?int $page = null) : array
    {
        $subcategories = empty($subcategories) ? [$category_id] : $subcategories;

        $categories = [];
        foreach($subcategories as $subcategory_id)
        {
            $subcategory_id = Product_category_model::find($subcategory_id);
            $categories = array_merge($categories, Get_products_subcategories_collection::get($subcategory_id->electrolux_id, $_SESSION['id_lang'], true));
        }

        $categories_ids = [];
        foreach($categories as $category)
        {
            $categories_ids[] = $category->electrolux_id;
        }

        $products_assigned_to_category = Get_products_assigned_to_category_collection::get($categories_ids, $_SESSION['id_lang'], $limit, $order, (int)$_SESSION['id_service'], $filters, $page);

        if (!empty($products_assigned_to_category))
        {
            foreach ($products_assigned_to_category as $product_assigned_to_category)
            {
                $product_assigned_to_category->assign_attribute('url', BASE_URL . 'products/product.html?id=' . $product_assigned_to_category->id);
            }
        }

        return $products_assigned_to_category;
    }
}