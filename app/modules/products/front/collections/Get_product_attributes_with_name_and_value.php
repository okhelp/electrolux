<?php
declare(strict_types=1);

namespace App\modules\products\front\collections;

class Get_product_attributes_with_name_and_value
{
    public static function get(array $products_ids): array
    {
        $product_attributes_to_return = [];

        $product_attributes = Get_products_attributes::get($products_ids);

        $all_products_attributes = Get_all_products_attributes::get_by_attribute_id($_SESSION['id_lang']);

        foreach ($product_attributes as $product_attribute)
        {
            $product_attributes_to_return[] = [
                'name' => $all_products_attributes[$product_attribute->attribute_id]->name,
                'value' => $product_attribute->name,
            ];
        }

        return $product_attributes_to_return;
    }
}