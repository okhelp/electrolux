<?php

declare(strict_types=1);

namespace App\modules\products\front\collections;

use App\modules\products\front\models\Product_category_model;

class Get_products_categories_by_seo_names_collection
{
    /**
     * @param array $seo_names
     * @return array
     */
    public static function get(array $seo_names): array
    {
        $categories_collection = [];

        $categories = Product_category_model::find_by_sql("
            SELECT *
            FROM product_category
            JOIN product_category_lang ON product_category_lang.product_category_id=product_category.id
            WHERE product_category_lang.seo_name IN ('" . implode("','", $seo_names) . "')
        ");

        if (!empty($categories))
        {
            foreach ($categories as $category)
            {
                $categories_collection[$category->electrolux_id] = $category;
            }
        }

        return $categories_collection;
    }
}