<?php

declare(strict_types=1);

namespace App\modules\products\front\collections;

use App\modules\products\front\models\Product_category_model;

class Get_product_category_by_electrolux_id
{
    /**
     * @param string $product_category_id
     * @return array
     */
    public static function get(string $product_category_id)
    {
        $product_category = Product_category_model::find_by_sql(
            "
          SELECT * 
          FROM product_category 
          JOIN product_category_lang ON product_category.id=product_category_lang.product_category_id
          WHERE product_category.electrolux_id='" . $product_category_id . "'
        ");

        if (!empty($product_category))
        {
            $product_category = $product_category[0];

            return $product_category;
        }
        else
        {
            return [];
        }
    }
}