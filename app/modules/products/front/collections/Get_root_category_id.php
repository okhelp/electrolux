<?php

declare(strict_types=1);

namespace App\modules\products\front\collections;

use App\modules\products\front\models\Product_category_model;

class Get_root_category_id
{

    public static function get()
    {
        $category_to_return = 0;

        $category = Product_category_model::find_by_sql("
            SELECT electrolux_id
            FROM product_category
            WHERE parent_id IS NULL
            LIMIT 1
        ");

        if(!empty($category))
        {
            $category_to_return = $category[0]->{Product_category_model::COLUMN_ELECTROLUX_ID};
        }

        return $category_to_return;
    }
}