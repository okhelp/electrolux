<?php

declare(strict_types=1);

namespace App\modules\products\front\collections;

use App\modules\products\front\interfaces\Products_collection_interface;
use App\modules\products\front\models\Product_category_model;

class Get_all_products_categories_collection implements Products_collection_interface
{
    /**
     * @param int $lang_id
     * @return array
     * @throws \Exception
     */
    public static function get(int $lang_id, bool $all_services = false): array
    {
        $categories_collection = [];

        $categories = Product_category_model::find_by_sql("
            SELECT *
            FROM product_category
            JOIN product_category_lang ON product_category.id=product_category_lang.product_category_id
        ");

        if (!empty($categories))
        {
            foreach ($categories as $category)
            {
                if($all_services)
                {
                    $categories_collection[$category->electrolux_id][$category->id_service] = $category;
                }
                else
                {
                    $categories_collection[$category->electrolux_id] = $category;
                }
            }
        }

        return $categories_collection;
    }
}