<?php
declare(strict_types=1);

namespace App\modules\products\front\collections;

use App__Ed__Model__Attribute;

class Get_all_products_attributes
{
    public static function get(int $language_id = 1, bool $group_by_service = false): array
    {
        $product_attributes_to_return = [];

        $products_attributes = App__Ed__Model__Attribute::find_by_sql(
            "
            SELECT *
            FROM attribute
            JOIN attribute_lang ON attribute.id=attribute_lang.attribute_id
            WHERE attribute_lang.id_lang = " . $language_id . "
        ");

        if (!empty($products_attributes))
        {
            foreach ($products_attributes as $product_attribute)
            {
                if($group_by_service)
                {
                    $product_attributes_to_return[$product_attribute->{App__Ed__Model__Attribute::COLUMN_ELECTROLUX_ID}][$product_attribute->id_service] = $product_attribute;
                }
                else
                {
                    $product_attributes_to_return[$product_attribute->{App__Ed__Model__Attribute::COLUMN_ELECTROLUX_ID}] = $product_attribute;
                }
            }
        }

        return $product_attributes_to_return;
    }
    public static function get_by_attribute_id(int $language_id = 1): array
    {
        $product_attributes_to_return = [];

        $products_attributes = App__Ed__Model__Attribute::find_by_sql(
            "
            SELECT *
            FROM attribute
            JOIN attribute_lang ON attribute.id=attribute_lang.attribute_id
            WHERE attribute_lang.id_lang = " . $language_id . "
        ");

        if (!empty($products_attributes))
        {
            foreach ($products_attributes as $product_attribute)
            {
                $product_attributes_to_return[$product_attribute->{App__Ed__Model__Attribute::COLUMN_ID}] = $product_attribute;
            }
        }

        return $product_attributes_to_return;
    }
}