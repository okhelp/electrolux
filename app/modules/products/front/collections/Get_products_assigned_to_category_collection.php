<?php
declare(strict_types=1);

namespace App\modules\products\front\collections;

use App\modules\products\front\models\Product_model;

class Get_products_assigned_to_category_collection
{
    /**
     * @param array $categories_ids
     * @param int $lang_id
     * @param string $limit
     * @param string $order
     * @param int $brand_id
     * @param array $filters
     * @param int $page
     * @return array
     */
    public static function get(array $categories_ids, int $lang_id, string $limit = '', string $order = '', int $brand_id = 0, array $filters = [], ?int $page = null): array
    {
        $products_collection = [];

        $limit = is_numeric($limit) ? $limit : '';

        $offset = empty($limit) ? '' : empty($page) ?  '' : ' OFFSET ' . ($limit * ($page - 1));

        $order_condition = '';
        if (!empty($order))
        {
            switch ($order)
            {
                case 'popularity':
                    $order_condition = ' ORDER BY popularity DESC ';
                    break;
                case 'price_asc':
                    $order_condition = ' ORDER BY price ASC ';
                    break;
                case 'price_desc':
                    $order_condition = ' ORDER BY price DESC ';
                    break;
                case 'e_lux_asc':
                    $order_condition = ' ORDER BY points ASC ';
                    break;
                case 'e_lux_desc':
                    $order_condition = ' ORDER BY points DESC ';
                    break;
            }
        }

        $filters_condition = self::get_filters_condition($filters);
        $filters_join_condition = '';
        if(!empty($filters_condition))
        {
            $filters_join_condition = "
            LEFT JOIN product_attribute ON product_attribute.product_id=product.id
            LEFT JOIN product_attribute_lang ON product_attribute_lang.product_attribute_id=product_attribute.id
			LEFT JOIN attribute ON product_attribute.attribute_id=attribute.id
            ";
        }

        $brand_condition = '';
        if(!empty($brand_id))
        {
            $brand_condition = " AND product.brand='" . ($brand_id == 6 ? 'AEG' : 'Electrolux') . "'";
        }
        if (in_array('F7D7BC70-3751-4A0A-8DFF-3BB52F2DE595', $categories_ids)) {
            $packs = [];
        } else {
            $packs = Product_model::find_by_sql("
                SELECT product.*, product_lang.*, (SELECT COUNT(s.id) FROM sale s WHERE s.id_product=product.id) AS popularity
                FROM product
                JOIN product_lang ON product_lang.product_id=product.id
                $filters_join_condition
                WHERE 
                  product.parent_id='F7D7BC70-3751-4A0A-8DFF-3BB52F2DE595'
                  AND product_lang.id_lang=$lang_id
                  AND product.status = 1
                  AND product.points<>0
                  $brand_condition
            ");
        }
        $products = Product_model::find_by_sql("
            SELECT product.*, product_lang.*, (SELECT COUNT(s.id) FROM sale s WHERE s.id_product=product.id) AS popularity
            FROM product
            JOIN product_lang ON product_lang.product_id=product.id
            $filters_join_condition
            WHERE product_lang.id_lang=$lang_id
              AND product.parent_id IN ('" . implode("','", $categories_ids) . "')
              AND product.status = 1
              AND product.points<>0
              $brand_condition
              $filters_condition
              $order_condition
              LIMIT $limit $offset
        ");

        if (!empty($products))
        {
            foreach ($packs as $product)
            {
                $products_collection[$product->electrolux_id] = $product;
            }
            foreach ($products as $product)
            {
                $products_collection[$product->electrolux_id] = $product;
            }
        }

        return $products_collection;
    }

    private static function get_filters_condition(array $filters): string
    {
        $condition = '';

        if(!empty($filters))
        {
            foreach ($filters as $filter_id => $filter_value)
            {
                $condition .= " AND attribute.electrolux_id='$filter_id' AND product_attribute_lang.name IN ('" . (implode("','", $filter_value)) . "') ";
            }
        }

        return $condition;
    }
}