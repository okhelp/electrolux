<?php
declare(strict_types=1);

namespace App\modules\products\front\collections;

use App\modules\products\front\models\Product_model;

class Products_search
{
    public static function search(string $search_phrase, int $count = 20, string $order = 'e_lux_desc', int $page = 1): array
    {
        $products_to_return = [];

        $where = '';
        $order_condition = '';
        $offset = $count * ($page - 1);

        if(!empty($search_phrase))
        {
            $where = "
                AND (MATCH(model) AGAINST ('{$search_phrase}*' IN BOOLEAN MODE)
                  OR MATCH(product_code) AGAINST ('{$search_phrase}*' IN BOOLEAN MODE)
                  OR MATCH(product.bar_code) AGAINST ('{$search_phrase}*' IN BOOLEAN MODE)
                  OR MATCH(product_lang.name) AGAINST ('{$search_phrase}*' IN BOOLEAN MODE)
                  OR MATCH(description) AGAINST ('{$search_phrase}*' IN BOOLEAN MODE)
                  OR MATCH(marketing_content) AGAINST ('{$search_phrase}*' IN BOOLEAN MODE))
            ";
        }

        $order_condition = $order == 'e_lux_desc' ? ' price DESC ' : ' price ASC ';

        $products = Product_model::find_by_sql("
            SELECT *, CONCAT('" . (BASE_URL . 'products/product.html?id=') . "', product.id) AS url
            FROM product
            JOIN product_lang ON product_lang.product_id=product.id
            WHERE product_lang.id_service={$_SESSION['id_service']} AND product_lang.id_lang={$_SESSION['id_lang']}
            $where
            ORDER BY $order_condition
            LIMIT $count OFFSET $offset
        ");

        if(!empty($products))
        {
            foreach ($products as $product)
            {
                $products_to_return[$product->{Product_model::COLUMN_ID}] = $product;
            }
        }

        return $products_to_return;
    }
}