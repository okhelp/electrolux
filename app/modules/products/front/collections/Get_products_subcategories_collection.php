<?php
declare(strict_types=1);

namespace App\modules\products\front\collections;

use App\modules\products\ed\objects\Products_categories_tree;
use App\modules\products\front\models\Product_category_model;

class Get_products_subcategories_collection
{
    /**
     * @param string $product_category_id
     * @param int $lang_id
     * @param bool $with_children
     * @return array
     */
    public static function get(string $product_category_id, int $lang_id, bool $with_children): array
    {
        $subcategories_collection = [];

        $subcategories = Products_categories_tree::get($product_category_id, -1, -1, 'without_pages', 0);

        $subcategories_ids = array_map(
            function($subcategory) {
                return $subcategory->id;
            }, $subcategories);

        $without_childrens = '';
        if(!$with_children)
        {
            $without_childrens = Product_category_model::COLUMN_PARENT_ID . " = '" . $product_category_id . "' AND";
        }

        if (!empty($subcategories_ids))
        {
            $subcategories = Product_category_model::find_by_sql(
                "
                    SELECT *
                    FROM " . Product_category_model::TABLE_NAME . "
                    JOIN product_category_lang ON product_category_lang.product_category_id=product_category.id
                    WHERE
                        $without_childrens
                        product_category_lang.id_lang = $lang_id
                        AND product_category.id IN ('" . implode("','", $subcategories_ids) . "')
                ");

            if (!empty($subcategories))
            {
                foreach ($subcategories as $subcategory)
                {
                    $subcategories_collection[$subcategory->{Product_category_model::COLUMN_ELECTROLUX_ID}] = $subcategory;
                }
            }
        }


        return $subcategories_collection;
    }
}