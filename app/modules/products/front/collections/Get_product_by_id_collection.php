<?php

declare(strict_types=1);

namespace App\modules\products\front\collections;

use App\modules\products\front\models\Product_lang_model;
use App\modules\products\front\models\Product_model;

class Get_product_by_id_collection
{
    /**
     * @param $product_id
     * @return array
     */
    public static function get($product_id)
    {
        $product = Product_model::find_by_sql("
          SELECT * 
          FROM product 
          JOIN product_lang ON product.id=product_lang.product_id
          WHERE id='" . $product_id . "' 
        ");

        if (!empty($product))
        {
            return reset($product);
        }
        else
        {
            return [];
        }
    }
}