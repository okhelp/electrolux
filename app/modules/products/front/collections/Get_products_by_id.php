<?php
declare(strict_types=1);

namespace App\modules\products\front\collections;

use App\modules\products\front\models\Product_model;

class Get_products_by_id
{
    public static function get(array $products_ids): array
    {
        $products_to_return = [];

        $products = Product_model::find_by_sql(
            "
            SELECT *
            FROM product
            JOIN product_lang ON product.id=product_lang.product_id
            WHERE product_lang.id_lang = " . $_SESSION['id_lang'] . "
                AND product.id IN (" . implode(',', $products_ids) . ")
        ");

        if (!empty($products))
        {
            foreach ($products as $product)
            {
                $products_to_return[$product->{Product_model::COLUMN_ID}] = $product;
            }
        }

        return $products_to_return;
    }

    public static function get_by_electrolux_id(array $products_ids): array
    {
        $products_to_return = [];

        if(!empty($products_ids))
        {
            $products = Product_model::find_by_sql("
                SELECT *
                FROM product
                JOIN product_lang ON product.id=product_lang.product_id
                WHERE product.electrolux_id IN ('" . implode("','", $products_ids) . "')
            ");

            if(!empty($products))
            {
                $products_to_return = $products;
            }
        }

        return $products_to_return;
    }
}