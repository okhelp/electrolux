<?php

declare(strict_types=1);

namespace App\modules\products\front\collections;

use App\modules\products\front\models\Product_lang_model;

class Get_products_langs_by_products_ids
{
    /**
     * @param array $products_ids
     * @param int $lang_id
     * @return array
     * @throws \Exception
     */
    public static function get(array $products_ids, int $lang_id): array
    {
        $langs = [];

        if (empty($products_ids))
        {
            throw new \Exception('Brak id produktów do wyszukania');
        }

        $products_langs = Product_lang_model::find_by_sql(
            "
            SELECT *
            FROM product_language
            WHERE " . Product_lang_model::COLUMN_PRODUCT_ID . " IN ('" . implode("','", $products_ids) . "') AND " . Product_lang_model::COLUMN_LANG_ID . " = $lang_id
            "
        );

        if (!empty($products_langs))
        {
            foreach ($products_langs as $product_lang)
            {
                $langs[$product_lang->{Product_lang_model::COLUMN_PRODUCT_ID}] = $product_lang;
            }
        }

        return $langs;
    }
}