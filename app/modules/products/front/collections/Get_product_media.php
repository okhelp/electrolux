<?php
declare(strict_types=1);

namespace App\modules\products\front\collections;

use App\ed\model\Product_media;

class Get_product_media
{
    public static function get(int $product_id)
    {
        $medias_to_return = [];

        $medias = Product_media::find_by_sql(
            "
            SELECT *
            FROM product_media
            WHERE id_product=$product_id
        ");

        if (!empty($medias))
        {
            $medias_to_return = $medias;
        }

        return $medias_to_return;
    }
}