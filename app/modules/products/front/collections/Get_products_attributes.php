<?php
declare(strict_types=1);

namespace App\modules\products\front\collections;

use App__Ed__Model__Product_attribute;

class Get_products_attributes
{
    public static function get(array $products_ids, int $lang_id = 1): array
    {
        $products_attributes_to_return = [];

        $products_attributes = App__Ed__Model__Product_attribute::find_by_sql("
            SELECT *
            FROM product_attribute
            JOIN product_attribute_lang ON product_attribute_lang.product_attribute_id=product_attribute.id
            WHERE product_attribute_lang.id_lang = $lang_id
              AND product_attribute.product_id IN (" . implode(',', $products_ids) . ")
              AND product_attribute_lang.id_service = " . $_SESSION['id_service'] . "
        ");

        foreach ($products_attributes as $product_attribute)
        {
            $products_attributes_to_return[$product_attribute->{App__Ed__Model__Product_attribute::COLUMN_ATTRIBUTE_ID}] = $product_attribute;
        }

        return $products_attributes_to_return;
    }
}