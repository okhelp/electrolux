<?php

declare(strict_types=1);

namespace App\modules\products\front\collections;

use App\modules\products\front\models\Product_category_lang_model;

class Get_products_categories_langs_by_categories_ids
{
    /**
     * @param array $categories_ids
     * @param int $lang_id
     * @return array
     * @throws \Exception
     */
    public static function get(array $categories_ids, int $lang_id): array
    {
        $langs = [];

        if (empty($categories_ids))
        {
            throw new \Exception('Brak kategorii do wyszukania');
        }

        $categories_langs = Product_category_lang_model::find_by_sql(
            "
            SELECT *
            FROM product_category_lang
            WHERE " . Product_category_lang_model::COLUMN_PRODUCT_CATEGORY_ID . " IN ('" . implode("','", $categories_ids) . "') 
                AND " . Product_category_lang_model::COLUMN_LANG_ID . " = $lang_id
            "
        );

        if (!empty($categories_langs))
        {
            foreach ($categories_langs as $category_lang)
            {
                $langs[$category_lang->{Product_category_lang_model::COLUMN_PRODUCT_CATEGORY_ID}] = $category_lang;
            }
        }

        return $langs;
    }
}