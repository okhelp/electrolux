<?php

declare(strict_types=1);

namespace App\modules\products\front\collections;

use App\modules\products\front\interfaces\Products_collection_interface;
use App\modules\products\front\models\Product_model;

class Get_all_products_products_collection implements Products_collection_interface
{
    /**
     * @param int $lang_id
     * @return array
     * @throws \Exception
     */
    public static function get(int $lang_id): array
    {
        $products_collection = [];

        $products = Product_model::find_by_sql("
            SELECT *
            FROM product
            JOIN product_lang ON product_lang.product_id=product.id
        ");

        if (!empty($products))
        {
            foreach ($products as $product)
            {
                $products_collection[$product->electrolux_id] = $product;
            }
        }

        return $products_collection;
    }
}