<?php

namespace App\modules\products\front\interfaces;

interface Runnable_interface
{
    public function run();
}