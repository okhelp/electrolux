<?php

namespace App\modules\products\front\interfaces;

interface Products_collection_interface
{
    /**
     * @param int $lang_id
     * @return array
     */
    public static function get(int $lang_id): array;
}