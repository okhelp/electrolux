<?php

namespace App\modules\products\front\interfaces;

interface Xml_element
{
    /**
     * @param \SimpleXMLElement $element
     * @param string $parent_element_id
     */
    public function __construct(\SimpleXMLElement $element, string $parent_element_id);

    /**
     * @param array $existing_categories
     * @param array $existing_products
     * @param array $existing_products_attributes
     * @param int $language_id
     * @return mixed
     */
    public function perform_actions(array $existing_categories, array $existing_products, array $existing_products_attributes, int $language_id);
}