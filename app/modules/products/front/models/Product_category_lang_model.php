<?php

namespace App\modules\products\front\models;

use Lib__Base__Model;

class Product_category_lang_model extends Lib__Base__Model
{
    const COLUMN_PRODUCT_CATEGORY_ID = 'product_category_id';
    const COLUMN_LANG_ID             = 'id_lang';
    const COLUMN_NAME                = 'name';
    const COLUMN_CREATED_AT          = 'created_at';
    const COLUMN_UPDATED_AT          = 'updated_at';

    const TABLE_NAME = 'product_category_language';

    static $table_name = self::TABLE_NAME;
    static $primary_key = self::COLUMN_ID;
}