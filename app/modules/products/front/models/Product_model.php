<?php

namespace App\modules\products\front\models;

use Lib__Base__Model;

class Product_model extends Lib__Base__Model
{
    const STATUS_ACTIVE   = 1;
    const STATUS_INACTIVE = 0;

    const COLUMN_ID                = 'id';
    const COLUMN_ELECTROLUX_ID     = 'electrolux_id';
    const COLUMN_PARENT_ID         = 'parent_id';
    const COLUMN_STATUS            = 'status';
    const COLUMN_CIMG              = 'cimg';
    const COLUMN_BRAND             = 'brand';
    const COLUMN_PRODUCT_CODE      = 'product_code';
    const COLUMN_MODEL             = 'model';
    const COLUMN_BADGES            = 'badges';
    const COLUMN_CATEGORY          = 'category';
    const COLUMN_BAR_CODE          = 'bar_code';
    const COLUMN_POINTS            = 'points';
    const COLUMN_PRICE             = 'price';
    const COLUMN_EXTRA_POINTS      = 'extra_points';
    const COLUMN_MARKETING_CONTENT = 'marketing_content';
    const COLUMN_RELATED_PRODUCTS  = 'related_products';
    const COLUMN_SERVICE_ID        = 'id_service';
    const COLUMN_CREATED_AT        = 'created_at';
    const COLUMN_UPDATED_AT        = 'updated_at';

    const TABLE_NAME = 'product';

    static $table_name = self::TABLE_NAME;
}