<?php

namespace App\modules\products\front\models;

use Lib__Base__Model;

class Product_lang_model extends Lib__Base__Model
{
    const COLUMN_ID         = 'id';
    const COLUMN_PRODUCT_ID = 'product_id';
    const COLUMN_LANG_ID    = 'lang_id';
    const COLUMN_NAME       = 'name';
    const COLUMN_CREATED_AT = 'created_at';
    const COLUMN_UPDATED_AT = 'updated_at';

    const TABLE_NAME = 'product_lang';

    static $table_name = self::TABLE_NAME;
    static $primary_key = self::COLUMN_ID;
}