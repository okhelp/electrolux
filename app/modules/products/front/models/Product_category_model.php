<?php

namespace App\modules\products\front\models;

use Lib__Base__Model;

class Product_category_model extends Lib__Base__Model
{
    const COLUMN_ID            = 'id';
    const COLUMN_ELECTROLUX_ID = 'electrolux_id';
    const COLUMN_PARENT_ID     = 'parent_id';
    const COLUMN_CREATED_AT    = 'created_at';
    const COLUMN_UPDATED_AT    = 'updated_at';

    const TABLE_NAME = 'product_category';

    static $table_name = self::TABLE_NAME;
    static $primary_key = self::COLUMN_ID;
    static $cache = false;
}