<?php

use App\front\models\search\Search_by_phrase;
use App\modules\products\ed\objects\Products_categories_tree;
use App\modules\products\front\collections\Get_all_products_categories_collection;
use App\modules\products\front\collections\Get_product_attributes_with_name_and_value;
use App\modules\products\front\collections\Get_product_by_id_collection;
use App\modules\products\front\collections\Get_product_category_by_electrolux_id;
use App\modules\products\front\collections\Get_product_media;
use App\modules\products\front\collections\Get_products_by_id;
use App\modules\products\front\collections\Get_root_category_id;
use App\modules\products\front\models\Product_model;
use App\modules\products\front\objects\Category_details;
use App\modules\products\front\objects\Filter_subcategories;
use App\modules\products\front\objects\Filters_by_category_id;
use App\modules\products\front\objects\Products_elux_import;
use App\modules\products\front\objects\Products_import;

class App__Modules__Products__Front__Controller__Index extends Lib__Base__Ed_Controller
{
    public function action_index()
    {
        $category_details = Category_details::get_category_data_by_url($_SERVER['REQUEST_URI']);

        $category_filters = Filters_by_category_id::get($category_details['category']->electrolux_id);

        $all_categories = Get_all_products_categories_collection::get($_SESSION['id_lang']);

        $categories_path = Products_categories_tree::build_path($category_details['category']->electrolux_id, $all_categories);

        $category_root = Get_root_category_id::get();

        //wyłączenie w kawiarkach podkategorii
        if(!empty($category_details['category_id']) && $category_details['category_id'] == 872)
        {
            $category_details['subcategories'] = [];
        }

        $this->view->assign('category_filters', $category_filters);
        $this->view->assign('subcategories', Filter_subcategories::filter($category_details['category_id'], $category_details['subcategories']));
        $this->view->assign('products', $category_details['products']);
        $this->view->assign('category_id', $category_details['category_id']);
        $this->view->assign('page', 1);
        $all_category_details = Category_details::get_category_products((int)$category_details['category_id'], 999999999, 'e_lux_desc', [], [],null);
        $this->view->assign('pages_count', count($all_category_details));
        $this->view->assign('items_count', 15);
        $this->template = get_module_template_path('products') . 'category.tpl';

        if(!empty($categories_path))
        {
            $categories_path = array_reverse($categories_path);

            $category_path = '';
            foreach($categories_path as $category)
            {
                if($all_categories[$category]->electrolux_id != $category_root)
                {
                    $category_path .= $all_categories[$category]->seo_name . '/';
                    $this->breadcrumb[BASE_URL . 'products/category/' . $category_path] = $all_categories[$category]->name;
                }
            }
        }
    }

    public function action_start_import()
    {
        try
        {
            $products_importer = new Products_import();
            $products_importer->run();

            echo '<h1>Import został wykonany</h1>';
        }
        catch (Exception $exception)
        {
            echo '<pre>';
            print_r($exception->getMessage());
            echo '</pre>';
        }
    }

    public function action_import_products_elux()
    {
        try
        {
            $products_importer = new Products_elux_import();
            $products_importer->run();

            echo '<h1>Import został wykonany</h1>';
        }
        catch (Exception $exception)
        {
            echo '<pre>';
            print_r($exception->getMessage());
            echo '</pre>';
        }
    }

    public function action_product()
    {
        if (!empty($_GET['id']))
        {
            $product = Get_product_by_id_collection::get($_GET['id']);

            $category = Get_product_category_by_electrolux_id::get($product->{Product_model::COLUMN_PARENT_ID});

            $all_categories = Get_all_products_categories_collection::get($_SESSION['id_lang']);

            $categories_path = Products_categories_tree::build_path($category->electrolux_id, $all_categories);

            $category_root = Get_root_category_id::get();

            $this->view->assign('product', $product);
            $this->view->assign('product_catalogue_card', file_exists(BASE_PATH . 'data/products_catalogue_cards/Datasheet_ ' . $product->model . '.pdf') ?
                BASE_URL . 'data/products_catalogue_cards/Datasheet_%20' . $product->model . '.pdf' : '');
            $this->view->assign('product_attributes', Get_product_attributes_with_name_and_value::get([$_GET['id']]));
            $this->view->assign('related_products', empty($product->{Product_model::COLUMN_RELATED_PRODUCTS}) ? [] :
                Get_products_by_id::get_by_electrolux_id(json_decode($product->{Product_model::COLUMN_RELATED_PRODUCTS})));
            $this->view->assign('product_gallery',
                Get_product_media::get($product->{Product_model::COLUMN_ID}) ?: App__Ed__Model__Product_package_media::get_images($product->{Product_model::COLUMN_ID})
            );
            $this->template = get_module_template_path('products') . 'product.tpl';
            $this->page_title = "Produkt";

            if(!empty($categories_path))
            {
                $categories_path = array_reverse($categories_path);

                $category_path = '';
                foreach($categories_path as $category)
                {
                    if($all_categories[$category]->electrolux_id != $category_root)
                    {
                        $category_path .= $all_categories[$category]->seo_name . '/';
                        $this->breadcrumb[BASE_URL . 'products/category/' . $category_path] = $all_categories[$category]->name;
                    }
                }
            }

            $this->breadcrumb[BASE_URL . 'products/product.html?id=' . $_GET['id']] = $product->name;
        }
        else
        {
            go_back("d|Nie odnaleziono produktu");
        }
    }

    public function action_ajax_products_filter_products_changed()
    {
        if (is_ajax() && !empty($_POST))
        {
            $category_id = empty($_POST['category_id']) ? 0 : (int)$_POST['category_id'];
            $limit = empty($_POST['products_count_select']) ? '12' : $_POST['products_count_select'];
            $order_by = empty($_POST['products_sort_select']) ? 'asc' : $_POST['products_sort_select'];
            $subcategories_ids = empty($_POST['subcategories_ids']) ? [] : $_POST['subcategories_ids'];
            $page = empty($_POST['page']) ? 1 : (int)$_POST['page'];
            $filters = empty($_POST['filters']) ? [] : $_POST['filters'];

            if($limit == 'all')
            {
                $limit = 9999999;
            }

            $products = Category_details::get_category_products($category_id, $limit, $order_by, $subcategories_ids, $filters, $page);

            $smarty = new Smarty();
            $smarty->assign('products', $products);
            $smarty->assign('page', $page);
            $smarty->assign('items_count', $limit);
            $smarty->assign('pages_count', count(Category_details::get_category_products($category_id, 999999999, $order_by, $subcategories_ids, $filters, 1)));

            echo $smarty->fetch(get_module_template_path('products') . '_partials/single_product.tpl');
        }
    }
}