<?php

class App__Modules__Products__Front__Controller__Router
{
    public static function set()
    {
        $router = [];

        $router[] = [
            'uri'        => strlen(BASE_DIR) > 1 ? str_replace(BASE_DIR, '', $_SERVER['REQUEST_URI']) : substr($_SERVER['REQUEST_URI'], 1),
            'controller' => 'App__Modules__Products__Front__Controller__Index',
            'method'     => 'action_index',
        ];

        return $router;
    }
}

