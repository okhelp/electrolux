<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
	<!--[if gte mso 9]>
	<xml>
		<o:OfficeDocumentSettings>
		<o:AllowPNG/>
		<o:PixelsPerInch>96</o:PixelsPerInch>
		</o:OfficeDocumentSettings>
	</xml>
	<![endif]-->
	<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="format-detection" content="date=no" />
	<meta name="format-detection" content="address=no" />
	<meta name="format-detection" content="telephone=no" />
	<meta name="x-apple-disable-message-reformatting" />
    <!--[if !mso]><!-->
	<link href="https://fonts.googleapis.com/css?family=Noto+Sans:400,700&amp;subset=latin-ext" rel="stylesheet">
    <!--<![endif]-->
	<title>Email Template</title>
	<!--[if gte mso 9]>
	<style type="text/css" media="all">
		sup { font-size: 100% !important; }
	</style>
	<![endif]-->


	<style type="text/css" media="screen">
		/* Linked Styles */
		body { padding:0 !important; margin:0 !important; display:block !important; min-width:100% !important; width:100% !important; background:#1e52bd; -webkit-text-size-adjust:none }
		a { color:#000001; text-decoration:none }
		p { padding:0 !important; margin:0 !important }
		img { -ms-interpolation-mode: bicubic; /* Allow smoother rendering of resized image in Internet Explorer */ }
		.mcnPreviewText { display: none !important; }
		.text-footer2 a { color: #1a2452; }

		/* Mobile styles */
		@media only screen and (max-device-width: 480px), only screen and (max-width: 480px) {
			.mobile-shell { width: 100% !important; min-width: 100% !important; }

			.m-center { text-align: center !important; }
			.m-left { text-align: left !important; margin-right: auto !important; }

			.center { margin: 0 auto !important; }
			.content2 { padding: 8px 15px 12px !important; }
			.t-left { float: left !important; margin-right: 30px !important; }
			.t-left-2  { float: left !important; }

			.td { width: 100% !important; min-width: 100% !important; }

			.content { padding: 30px 15px !important; }
			.section { padding: 30px 15px 0px !important; }

			.m-br-15 { height: 15px !important; }
			.mpb5 { padding-bottom: 5px !important; }
			.mpb15 { padding-bottom: 15px !important; }
			.mpb20 { padding-bottom: 20px !important; }
			.mpb30 { padding-bottom: 30px !important; }
			.m-padder { padding: 0px 15px !important; }
			.m-padder2 { padding-left: 15px !important; padding-right: 15px !important; }
			.p70 { padding: 30px 0px !important; }
			.pt70 { padding-top: 30px !important; }
			.p0-15 { padding: 0px 15px !important; }
			.p15 { padding: 15px !important; }
			.p30-15 { padding: 30px 15px !important; }
			.p30-15-0 { padding: 30px 15px 0px 15px !important; }
			.p0-15-30 { padding: 0px 15px 30px 15px !important; }


			.text-footer { text-align: center !important; }

			.m-td,
			.m-hide { display: none !important; width: 0 !important; height: 0 !important; font-size: 0 !important; line-height: 0 !important; min-height: 0 !important; }

			.m-block { display: block !important; }

			.fluid-img img { width: 100% !important; max-width: 100% !important; height: auto !important; }
			.emailImage{
				height:auto !important;
				max-width:600px !important;
				width: 100% !important;
			}
			.column,
			.column-dir,
			.column-top,
			.column-empty,
			.column-top-30,
			.column-top-60,
			.column-empty2,
			.column-bottom { float: left !important; width: 100% !important; display: block !important; }

			.column-empty { padding-bottom: 15px !important; }
			.column-empty2 { padding-bottom: 30px !important; }

			.content-spacing { width: 15px !important; }
		}
	</style>
</head>
<body class="body"style="padding:0 !important; margin:0 !important; display:block !important; min-width:100% !important; width:100% !important; background:#c6c6c6; -webkit-text-size-adjust:none; font-family: 'Noto Sans' !important;">
	<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#c6c6c6">
		<tr>
			<td align="center" valign="top">
				<!-- Main -->
				<table width="650" border="0" cellspacing="0" cellpadding="0" class="mobile-shell">
					<tr>
						<td class="td" style="width:650px; min-width:650px; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal;">
							<!-- Header -->
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td class="p15" style="padding: 40px 0px 12px 0px;">
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<th class="column-top" width="200"style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top;">
													<table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tr></tr>
													</table>
												</th>
											</tr>
										</table>
									</td>
								</tr>
								<!-- END Top bar -->
								<!-- Logo -->
								<tr>
									<td bgcolor="#ffffff" class="p30-15 img-center" style="padding: 30px; border-radius: 0px 0px 0px 0px; font-size:0pt; line-height:0pt; text-align:center;"><a href="{BASE_URL}" target="_blank"><img src="{BASE_URL}data/images/logo.png" width="225" height="84" border="0" alt="" /></a></td>
								</tr>
								<!-- END Logo -->
								<!-- Nav -->
								<tr>
									<td class="text-nav-white" bgcolor="#fff"style="color:#1a2452; font-family:'Noto Sans', Arial, sans-serif; font-size:14px; line-height:22px; text-align:center; text-transform:uppercase; padding:12px 0px;">
										<a href="#" target="_blank" class="link-white"style="color:#1a2452; text-decoration:none;"><span class="link-white"style="color:#1a2452; text-decoration:none;">Program</span></a>
										 &nbsp; &nbsp; &nbsp;<span class="m-hide"> &nbsp; &nbsp; </span>
										<a href="#" target="_blank" class="link-white"style="color:#1a2452; text-decoration:none;"><span class="link-white"style="color:#1a2452; text-decoration:none;">Produkty</span></a>
										 &nbsp; &nbsp; &nbsp;<span class="m-hide"> &nbsp; &nbsp; </span>
										<a href="#" target="_blank" class="link-white"style="color:#1a2452; text-decoration:none;"><span class="link-white"style="color:#1a2452; text-decoration:none;">Szkolenia i Ankiety</span></a>
										 <span class="m-block"><span class="m-hide">&nbsp; &nbsp; &nbsp; </span></span>
										<a href="#" target="_blank" class="link-white"style="color:#1a2452; text-decoration:none;"><span class="link-white"style="color:#1a2452; text-decoration:none;">Nagrody</span></a>
										 &nbsp; &nbsp; &nbsp;<span class="m-hide"> &nbsp; &nbsp; </span>
										<a href="#" target="_blank" class="link-white"style="color:#1a2452; text-decoration:none;"><span class="link-white"style="color:#1a2452; text-decoration:none;">Kontakt</span></a>
										 <span class="m-block"><span class="m-hide"></span></span>
									</td>
								</tr>
								<!-- END Nav -->
							</table>
							<!-- END Header -->

							<!-- Section 1 -->
							<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ebebeb">
								<tr>
									<td class="p30-15" style="padding: 50px 30px 60px;" bgcolor="#ffffff">
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											{if !empty($email_heading)}
												<tr>
													<td style="color:#1a2452; font-size:32px; line-height:36px; padding-bottom:28px;">{$email_heading}</td>
												</tr>
											{/if}
											<tr>
												<td style="color:#1a2452; font-size:16px; line-height:26px;">{$email_content}</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
							<!-- END Section 1 -->

							<!-- Footer -->
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td class="p30-15-0" bgcolor="#1a2452" style="border-radius: 0px 0px 0px 0px; padding: 35px 30px 0px 30px;">
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
										 	<tr>
												<td class="m-padder2 pb30" align="center" style="padding-bottom:30px;">
													<table class="center" border="0" cellspacing="0" cellpadding="0"style="text-align:center;">
														<tr>
															<td colspan="4" width="40"style="font-size:16px; line-height:20px; text-align:center; color: #fff;padding-bottom: 16px;">Obserwuj nas online</td>
														</tr>
														<tr>
															<td class="img" width="40"style="font-size:0pt; line-height:0pt; text-align:left;"><a href="https://www.facebook.com/electroluxpolska/" target="_blank"><img src="{BASE_URL}data/images/Facebook.png" width="13" height="26" border="0" alt="" /></a></td>
															<td class="img" width="50"style="font-size:0pt; line-height:0pt; text-align:left;"><a href="https://www.instagram.com/electrolux_pl/" target="_blank"><img src="{BASE_URL}data/images/Instagram.png" width="30" height="29" border="0" alt="" /></a></td>
															<td class="img" width="50"style="font-size:0pt; line-height:0pt; text-align:left;"><a href="https://twitter.com/electrolux" target="_blank"><img src="{BASE_URL}data/images/Twitter.png" width="32" height="26" border="0" alt="" /></a></td>
															<td class="img" width="30"style="font-size:0pt; line-height:0pt; text-align:left;"><a href="https://www.youtube.com/playlist?list=PL5AFA56CA93C94492" target="_blank"><img src="{BASE_URL}data/images/Youtube.png" width="30" height="20" border="0" alt="" /></a></td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td class="text-footer2 p30-15" style="padding: 30px 15px 15px 15px; color:#737373; font-family:'Noto Sans', Arial,sans-serif; font-size:13px; line-height:17px;">
                                        <multiline>Otrzymałeś/aś tę wiadomość email, ponieważ adres <span class="link-blue"style="color:#1a2452; text-decoration:none;">twój adres</span> został dodany do listy wysyłkowej maili od <span class="link-blue" style="color:#1a2452; text-decoration:none;">Electrolux od Kuchni</span>.<br/>By zarządzać swoimi subskrypcjami <a href="#" target="_blank" class="link-blue"style="color:#1a2452; text-decoration:none;">kliknij tutaj.</a></multiline>
									</td>
                                </tr>
                                <tr>
                                    <td class="text-footer2 p0-15-30" style="padding: 0px 15px 15px 15px; color:#737373; font-family:'Noto Sans', Arial,sans-serif; font-size:13px; line-height:17px;">
										<multiline>Potrzebujesz pomocy? Chcesz podzielić się z nami swoją opinią? <a href="#" target="_blank" class="link-blue"style="color:#1a2452; text-decoration:none;">Skontaktuj się z nami!</a></multiline>
                                    </td>
								</tr>
							</table>
							<!-- END Footer -->
						</td>
					</tr>
				</table>
				<!-- END Main -->

			</td>
		</tr>
	</table>
</body>
</html>
