<div class="container-fluid user_panel_container page_content">
    <div class="row">
        <div class="col-12">
            <div class="user_panel">
                <div class="row">
                    <div class="col">
                    </div>
                </div>
                <div class="row settings">
                    <div class="col-12 col-md-5 mt-3 mt-lg-0 m-auto">
                        <p class="user_panel__title">Prośba o uzupełnienie danych</p>

                        <form action="{BASE_URL}wybor-firmy-p8.html" method="get">
                            <div class="d-flex flex-column custom_form_checkbox">
                                <label class="form__label--1" for="is_main">
                                    <span class="text-info-form mr-2">
                                        <p>Jestem pracownikiem firmy zapraszającej</p>
                                    </span>
                                    <input type="radio"
                                           class="check"
                                           value="1"
                                           name="type_worker" checked>
                                    <label class="custom__label ml-auto" for="">
                                        <svg viewBox="0,0,50,50">
                                            <path d="M5 30 L 20 45 L 45 5"></path>
                                        </svg>
                                    </label>
                                </label>
                                <label class="form__label--1" for="is_main">
                                    <span class="text-info-form mr-2">
                                        <p>Jestem niezależnym kontrahentem</p>
                                    </span>
                                    <input type="radio"
                                           class="check"
                                           value="2"
                                           name="type_worker">
                                    <label class="custom__label ml-auto" for="">
                                        <svg viewBox="0,0,50,50">
                                            <path d="M5 30 L 20 45 L 45 5"></path>
                                        </svg>
                                    </label>
                                </label>
                                <label class="form__label--1" for="is_main">
                                    <span class="text-info-form mr-2">
                                        <p>Jestem pracownikiem zewnętrznej firmy</p>
                                    </span>
                                    <input type="radio"
                                           class="check"
                                           value="3"
                                           name="type_worker">
                                    <label class="custom__label ml-auto" for="">
                                        <svg viewBox="0,0,50,50">
                                            <path d="M5 30 L 20 45 L 45 5"></path>
                                        </svg>
                                    </label>
                                </label>
                            </div>
                            <div class="company_data d-none mt-5">
                                <div class="w-100 align-items-center flex-column flex-md-row">
                                    <div class="custom_form__input w-100">
                                        <input class="custom_form__input--input" type="text" id="user_new_company_name"
                                               name="company[name]" value="" required disabled>
                                        <span class="custom_form__input--placeHolder" data-placeholder="Nazwa firmy"></span>
                                    </div>
                                </div>
                                <div class="w-100 align-items-center flex-column flex-md-row">
                                    <div class="custom_form__input w-100">
                                        <input class="custom_form__input--input" type="number" id="user_new_nip"
                                               name="company[nip]" value="" required disabled>
                                        <span class="custom_form__input--placeHolder" data-placeholder="NIP"></span>
                                    </div>
                                </div>
                                <div class="w-100 align-items-center flex-column flex-md-row">
                                    <div class="custom_form__input w-100">
                                        <input class="custom_form__input--input" type="text" id="user_new_street"
                                               name="address[street]" value="" required disabled>
                                        <span class="custom_form__input--placeHolder" data-placeholder="Ulica"></span>
                                    </div>
                                </div>
                                <div class="w-100 align-items-center flex-column flex-md-row">
                                    <div class="custom_form__input w-100">
                                        <input class="custom_form__input--input" type="number" id="user_new_street_number"
                                               name="address[street_number]" value="" required disabled>
                                        <span class="custom_form__input--placeHolder" data-placeholder="Budynek"></span>
                                    </div>
                                </div>
                                <div class="w-100 align-items-center flex-column flex-md-row">
                                    <div class="custom_form__input w-100">
                                        <input class="custom_form__input--input" type="number" id="user_new_apartment_number"
                                               name="address[apartment_number]" value="" disabled>
                                        <span class="custom_form__input--placeHolder" data-placeholder="Lokal"></span>
                                    </div>
                                </div>
                                <div class="w-100 align-items-center flex-column flex-md-row">
                                    <div class="custom_form__input w-100">
                                        <input class="custom_form__input--input" type="test" id="user_new_post_code"
                                               name="address[post_code]" value="" required disabled>
                                        <span class="custom_form__input--placeHolder" data-placeholder="Kod pocztowy"></span>
                                    </div>
                                </div>
                                <div class="w-100 align-items-center flex-column flex-md-row">
                                    <div class="custom_form__input w-100">
                                        <input class="custom_form__input--input" type="text" id="user_new_city"
                                               name="address[city]" value="" required disabled>
                                        <span class="custom_form__input--placeHolder" data-placeholder="Miasto"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="text-right mt-5 w-100">
                                <button class="btn btn-pills btn-primary pl-4 pr-4" name="submit" value="1">Wyślij</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>

	var $typeWorker = $('input[type="radio"]');
	var $companyName = $('#user_new_company_name');
	function changeWorkerType() {
		if (parseInt($(this).val()) === 1) {
			$companyName.closest('.company_data').removeClass('d-block').addClass('d-none');
            $companyName.closest('.company_data').find('input').prop('disabled', true);
		} else {
			$companyName.closest('.company_data').removeClass('d-none').addClass('d-block');
            $companyName.closest('.company_data').find('input').prop('disabled', false);
		}
	}
	$typeWorker.on('change', changeWorkerType);
	$(document).ready(function () {
		checkInputValue();
	});
</script>
