<div class="container-fluid user_panel_container page_content">
	<div class="row">
		<div class="col-12">
			<div class="user_panel">
				<div class="row">
					<div class="col">
						<p class="user_panel__title">Kontakt z handlowcem</p>
					</div>
				</div>
				{include file="user_dashboard/kontakt/_partials/formularz_kontaktowy.tpl"}
			</div>
		</div>
	</div>
</div>