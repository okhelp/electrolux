<div class="container-fluid user_panel_container page_content">
	<div class="row">
		<div class="col-12">
			<div class="user_panel">

				<div class="row settings row__form">
					<div class="col-12 col-md-5 mt-3 mt-lg-0">
						<div class="row">
							<div class="col">
								<p class="user_panel__title">Prośba o uzupełnienie danych</p>
							</div>
						</div>
						<form action="{BASE_URL}prosba-o-uzupelnienie-danych-p7.html" method="get">
							<div class="custom_form__input" style="width: 100%">
								<input class="custom_form__input--input" type="text" id="name" name="name" required value="{$smarty.session.name}" autocomplete="off">
								<span class="custom_form__input--placeHolder" data-placeholder="Imię"></span>
							</div>
							<div class="custom_form__input" style="width: 100%">
								<input class="custom_form__input--input" type="text" id="surname" name="surname" required value="{$smarty.session.surname}" autocomplete="off">
								<span class="custom_form__input--placeHolder" data-placeholder="Nazwisko"></span>
							</div>
							<div class="text-right mt-4 w-100">
								<button class="btn btn-pills btn-primary pl-4 pr-4" name="submit" value="1">Wyślij</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	$(document).ready(function () {
		checkInputValue();
	});
</script>
