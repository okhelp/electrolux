/*-------------MENU-------------*/
$(document).ready(function() {
    $(".megamenu").on("click", function(e) {
        e.stopPropagation();
    });
    $(document).ready(function () {
        var scrollTop = $(document).scrollTop();
        if (scrollTop > 0) {
            $('#navbar_top, .page').addClass('scrolled');
        }
    });
    $(document).scroll(function (e)
    {
        var scrollTop = $(document).scrollTop();

        if (scrollTop > 0)
        {
            $('#navbar_top, .page').addClass('scrolled');
        } else
        {
            $('#navbar_top, .page').removeClass('scrolled');
        }
    });
    function navbarPoints() {
        if ($(window).width() >= 500) {
            $(".navbar-brand").after($('.navbar_points'));
        } else {
            $(".navbar_points__mobile").append($('.navbar_points'));
        }
    }
    navbarPoints();
    $(window).resize(function() {
        navbarPoints();
    });



    $("#menu").mmenu({
        navbars: [{
            content: [
                'prev',
                'close'
            ]
        }],
        "extensions": [
            "pagedim-black",
            "fullscreen"
        ],
        wrappers: [ 'bootstrap4' ]}, { });

    function vertical_menu_show() {
        $('.dropdown_hide_items').toggleClass('dropdown_show_items');
    }

    $('.mm-menu .Vertical').on('click', function (e) {
        e.preventDefault();
        $(this).closest('.mm-listitem__text').find('.dropdown_hide_items').toggleClass('dropdown_show_items');
    });
});

function initProductListing() {
    /*WIDOK LISTY PRODUKTÓW*/

    /*--------RÓWNA WYSOKOŚć ELEMENTÓW------------*/
    function equalHeight() {
        $('.single_product__price_row').matchHeight();
        $('.product_listing__single_product').matchHeight();
    }

    equalHeight();
    $(window).resize(function () {
        $.fn.matchHeight._update()
    });

    /*Ukrywanie filtrów produktów*/
    /*Extra info produktów na hover*/
    $('.extra_info').tooltipster({
        plugins: ['follower'],
        minWidth: 200,
        maxWidth: 350,
    });
};
    $(document).ready(function() {
        initProductListing();
        function listView() {
            $('.product_listing__row').hide().addClass('product_listing--list').removeClass('product_listing--grid product_listing--noImg').fadeIn();
        }

        function listNoImgView() {
            $('.product_listing__row').hide().removeClass('product_listing--list product_listing--grid').addClass('product_listing--noImg').fadeIn();
        }

        function gridView() {
            $('.product_listing__row').hide().removeClass('product_listing--list product_listing--noImg').addClass('product_listing--grid').fadeIn();
        }

        $('#list').click(function (event) {
            event.preventDefault();
            listView();
            $.fn.matchHeight._update()
        });
        $('#grid').click(function (event) {
            event.preventDefault();
            gridView();
            $.fn.matchHeight._update()
        });
        $('#list_no_img').click(function (event) {
            event.preventDefault();
            listNoImgView();
            $.fn.matchHeight._update()
        });
        $('.product_list_sort__link').click(function () {
            var viewId = $(this).attr('id');
            localStorage.setItem('view', viewId);
        });

        function lastView() {
            var lastViewUser = localStorage.getItem('view');
            if (lastViewUser === 'list') {
                listView();
            } else if (lastViewUser === 'list_no_img') {
                listNoImgView();
            }
            $.fn.matchHeight._update()
        }

        if ($(window).width() >= 991) {
            lastView();
        }

        function showHideFilter() {
            $('.product_list_filter__title').toggleClass('product_list_filter__icon--change');
            $('.product_listing').toggleClass('product_listing--hide');
            $('.product_list_filter').toggleClass('product_list_filter--hide');
        }

        $('#filter_show').on('click', function () {
            $.when(showHideFilter()).done(function () {
                setTimeout(function () {
                    $.fn.matchHeight._update();
                }, 200);
            });
        });
        /*Produkt info - więcej - specyfikacja*/
    $('.product_more__tab_link').on('click', function () {
        var tab_id = $(this).attr('data-tab');
        $('.product_more__photo').addClass('product_more__photo--hide');
        $('.product_more__photo img').fadeOut();

        if ($('.product_more__specification:visible').length <= 0) {
            $("#"+tab_id).fadeIn();
            $('.product_more__photo').addClass('product_more__photo--hide');
            $(this).find('i').toggleClass('icon-plus icon-minus');
        }
        else if ($('#'+tab_id).is(':visible') === true) {
            $('.product_more__specification').hide();
            $('.product_more__photo').removeClass('product_more__photo--hide');
            $('.product_more__photo img').fadeIn();
            $(this).find('i').toggleClass('icon-minus icon-plus');
        }
        else {
            $('.product_more__specification').hide();
            $("#"+tab_id).fadeIn().addClass('current');
            $('.product_more__tab_link i').toggleClass('icon-minus icon-plus');
        }
    });
    function DataTableInit(table) {
        $(table).DataTable({
            "scrollX": true,
            "language": {
                "processing": "Przetwarzanie...",
                "search": "Szukaj:",
                "lengthMenu": "Pokaż _MENU_ pozycji",
                "info": "Pozycje od _START_ do _END_ z _TOTAL_ łącznie",
                "infoEmpty": "Pozycji 0 z 0 dostępnych",
                "infoFiltered": "(filtrowanie spośród _MAX_ dostępnych pozycji)",
                "infoPostFix": "",
                "loadingRecords": "Wczytywanie...",
                "zeroRecords": "Nie znaleziono pasujących pozycji",
                "emptyTable": "Brak danych",
                "paginate": {
                    "first": "Pierwsza",
                    "previous": "Poprzednia",
                    "next": "Następna",
                    "last": "Ostatnia"
                },
                "aria": {
                    "sortAscending": ": aktywuj, by posortować kolumnę rosnąco",
                    "sortDescending": ": aktywuj, by posortować kolumnę malejąco"
                }
            }
        });
    }
    DataTableInit('.user_panel__table');
});
    /*----czy customowy input ma value----*/
function checkInputValue() {
    if ($('.custom_form__input--input').val().trim() !== "") {
        $('.custom_form__input--input').addClass('has-val');
    } else {
        $('.custom_form__input--input').removeClass('has-val');
    }
}
/*----LOADER----*/
function loaderInit() {
    $("#loader").css("display", "flex").hide().fadeIn();
}
function loaderHide() {
    $("#loader").fadeOut();
}
