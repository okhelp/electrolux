{if empty($smarty.session.user_points)}
    {assign var=points value=0}
{else}
    {assign var=points value={$smarty.session.user_points}}
{/if}
{if !empty($items)}
    {foreach $items as $item}
        {if !empty($item->product_id)}
            <div data-item="product" class="product_listing__single_product">
                <div class="row single_product__img_row">
                    <div class="col-12 single_product_info--wrapper">
                        <div class="single_product_info">
                            {if !empty($item->extra_points)}
                                <div class="single_product_info--container">
                                    <p class="single_product_info--label single_product_info--extraPoints">
                                        extra e-luxy
                                    </p>
                                </div>
                            {/if}
                            {if !empty($item->badges) && in_array('new', json_decode($item->badges))}
                                <div class="single_product_info--container">
                                    <p class="single_product_info--label single_product_info--new">
                                        nowość</p>
                                </div>
                            {/if}
                            {if !empty($item->badges) && in_array('series_end', json_decode($item->badges))}
                                <div class="single_product_info--container">
                                    <p class="single_product_info--label single_product_info--end">
                                        koniec
                                        serii</p>
                                </div>
                            {/if}
                            {if !empty($item->badges) && in_array('super_new', json_decode($item->badges))}
                                <div class="single_product_info--container">
                                    <p class="single_product_info--label single_product_info--quality">
                                        super
                                        jakość</p>
                                </div>
                            {/if}
                            {if !empty($item->badges) && in_array('eco', json_decode($item->badges))}
                                <div class="single_product_info--container">
                                    <p class="single_product_info--label single_product_info--quality">
                                        eco
                                    </p>
                                </div>
                            {/if}
                        </div>
                        <img class="single_product__img img-fluid extra_info lazy"
                             data-tooltip-content="#tooltip_content{$item->id}"
                             data-src="{img id_cimg={$item->cimg} width=300 height=300}" src="">
                        <div class="tooltip_templates">
                            <div class="extra_info_tooltip" id="tooltip_content{$item->id}">
                                <div class="container">
                                    <div class="row mb-3">
                                        <div class="col-6">
                                            <p class="single_product__name single_product__name--bold">
                                                {$item->name}
                                            </p>
                                        </div>
                                        <div class="col-6 text-right">
                                            <p class="single_product__points">{$item->points}
                                                E-LUX</p>
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <div class="col-12">
                                            <p class="tooltip_product__subtitle">
                                                {$item->name}
                                            </p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                            <p class="tooltip_product__description">
                                                {$item->description}
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row single_product__price_row">
                    <div class="col-6 single_product__name--container">
                        <p class="single_product__model"><b>{$item->model}</b></p>
                        <p class="single_product__name">{$item->name}</p>
                    </div>
                    <div class="col-6 single_product__price--container">
                        <p class="single_product__points">{$item->points} <span class="text-nowrap">E-LUX</span></p>
                        {if !empty($item->extra_points)}
                            <p class="single_product__extra_points">{$item->extra_points} <span class="text-nowrap">E-LUX</span>
                            </p>
                        {/if}
                        <p class="single_product__price">{$item->price} ZŁ</p>
                    </div>
                </div>
                <div class="row single_product__buttons">
                    <div class="col-12">
                        <a href="{$item->url}"
                           class="btn btn-outline-primary btn-pills single_product__more">
                            więcej
                        </a>
                    </div>
                </div>
            </div>
        {elseif !empty($item->prize_id)}
            {if {$item->price} > 0}
                {math assign=progress equation="x / y * 100" x={$points} y={$item->price} format="%.2f"}
                {math assign=progress_missing equation="y - x" x={$points} y={$item->price}}
            {elseif empty($item->price)}
                {assign var=progress value = false}
                {assign var=progress_missing value = false}
            {/if}
            <div data-item="prize" class="product_listing__single_product {if !empty($progress) && $progress < 100}no_points{/if}">
                <div class="row single_product__img_row">
                    <div class="col-12 single_product_info--wrapper">
                        {if !empty($item->cimg)}
                            <img class="single_product__img img-fluid extra_info lazy"
                                 data-tooltip-content="#tooltip_content1"
                                 data-src="{img id_cimg={$item->cimg} width=300 height=300}">
                        {else}
                            <img class="single_product__img img-fluid extra_info"
                                 data-tooltip-content="#tooltip_content1"
                                 src="{img id_cimg=0 width=300 height=300}">
                        {/if}
                        <div class="tooltip_templates">
                            <div class="extra_info_tooltip" id="tooltip_content1">
                                <div class="container">
                                    <div class="row mb-3">
                                        <div class="col-6">
                                            <p class="single_product__name single_product__name--bold">
                                                {$item->name}
                                            </p>
                                        </div>
                                        <div class="col-6 text-right">
                                            {if !empty($item->price)}
                                                <p class="single_product__points">
                                                    {$item->price}
                                                    E-LUX</p>
                                            {/if}
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <div class="col-12">
                                            <p class="tooltip_product__subtitle">
                                                {$item->sub_name}
                                            </p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                            <p class="tooltip_product__description">
                                                {$item->description}
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {if !empty($progress)}
                    <div class="row progress_wrapper">
                        <div class="col-12">
                            <div class="progress_label">
                                <span>0</span><span>1/2</span><span>p</span>
                            </div>
                            <div class="progress">
                                <div class="progress-bar" role="progressbar" style="width: {$progress}%"
                                     aria-valuenow="{$progress}" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <div class="text-right progress_missing">
                                <span>{if ($progress_missing) < 0}+{math equation="x * y" x=-1 y=$progress_missing}{else}{$progress_missing}{/if}</span>
                            </div>
                        </div>
                    </div>
                {/if}
                <div class="row single_product__price_row">
                    <div class="col-6 single_product__name--container">
                        <p class="single_product__name">{$item->name}</p>
                    </div>
                    <div class="col-6 text-right single_product__price--container">
                        <p class="single_product__points">{if !empty($item->price)}{$item->price}
                                <span class="text-nowrap">E-LUX</span>
                            {/if}</p>

                    </div>
                </div>
                <div class="row single_product__buttons">
                    <div class="col-6 justify-content-center d-flex single_product__buttons--two">
                        <a href="{BASE_URL}prizes/prize.html?id={$item->id}"
                           class="btn btn-outline-primary btn-pills single_product__more">
                            więcej
                        </a>
                    </div>
                    {if !empty($progress) && $progress < 100}
                        <div class="col-6 justify-content-center d-flex single_product__buttons--two">
                            <a href="#"
                               class="btn btn-primary btn-pills single_product__order">
                                dodaj
                            </a>
                        </div>
                    {else}
                        <div class="col-6 justify-content-center d-flex single_product__buttons--two">
                            <a href="{BASE_URL}prizes/prize.html?id={$item->id}#tab-1"
                               class="btn btn-primary btn-pills single_product__order">
                                dodaj
                            </a>
                        </div>
                    {/if}
                </div>
            </div>
        {/if}
    {/foreach}
{else}
    <div class="alert alert-info">
        Brak produktów do wyświetlenia.
    </div>
{/if}
<div class="col-12">
    {pagination all_items=$pages_count per_page=$items_count current_page=$page}
</div>
