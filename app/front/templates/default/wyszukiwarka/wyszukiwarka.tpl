<div class="category-details container-fluid product_list page_content search_results">
    <form action="{BASE_URL}wyszukiwarka">
        <div class="form-group row search_results__form">
            <p class="col-12 mb-2 mb-md-0 col-md-auto search_results__form--label">Wyszukiwanie:</p>
            <div class="col col-xl-6 pr-0 pr-sm-3">
                <input type="text"
                       class="form-control search_results__form--input"
                       name="phrase"
                       placeholder="szukaj"
                       required
                       value="{if !empty($smarty.get.phrase)}{$smarty.get.phrase}{/if}"
                       autocomplete="off">
            </div>
            <div class="col-auto d-flex align-items-center">
                <button class="btn btn-primary btn-pills search_results__form--button">wyszukaj</button>
            </div>
        </div>
    </form>
    <form id="parameters">
        {*<input type="hidden" name="category_id" value="{$category_id}">*}
        <div class="product_list">
            <div class="row">
                <div class="col-lg-3 listing__main_col_small listing__main_col_small--gray d-flex align-items-center">
                    <div class="row">
                        <div class="col-12">
                            <p id="filter_show" class="product_list_filter__title">Filtruj<span
                                        class="product_list_filter__icon_show"><i class="icon-arrow"></i><i
                                            class="icon-filter_cross"></i></span></p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-9 listing__main_col_big listing__main_col_big--gray">
                    <div class="row product_list_sort__row">
                        <div class="col-auto d-none d-md-flex align-items-center">
                            <a id="grid" class="product_list_sort__link" href="#"><i
                                        class="icon-filter_grid filter_grid_icon"></i></a>
                            <a id="list" class="product_list_sort__link" href="#"><i
                                        class="icon-filter_list1 filter_grid_icon"></i></a>
                            <a id="list_no_img" class="product_list_sort__link" href="#"><i
                                        class="icon-filter_list2 filter_grid_icon"></i></a>
                        </div>
                        <div class="col d-flex justify-content-sm-end product_list_sort__options--wrapper">
                            <div class="row w-auto-sm justify-content-sm-end">
                                <div class="col-12 col-sm-auto">
                                    <div class="form-group row mb-0">
                                        <label for="products_count_select"
                                               class="col-auto col-form-label product_list_sort__label">Wyświetl:</label>
                                        <div class="col col-sm-auto sort_select_wrapper">
                                            <select class="form-control" id="products_count_select" name="products_count_select">
                                                <option selected value="12">12</option>
                                                <option value="24">24</option>
                                                <option value="all">Wszystkie</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-auto">
                                    <div class="form-group row mb-0">
                                        <label for="products_sort_select"
                                               class="col-auto col-form-label no-wrap product_list_sort__label">Sortuj według:</label>
                                        <div class="col col-sm-auto sort_select_wrapper">
                                            <select class="form-control" id="products_sort_select" name="products_sort_select">
                                                <option value="e_lux_asc">E-LUX rosnąco</option>
                                                <option value="e_lux_desc" selected>E-LUX malejąco</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {*<div class="product_list_sort__options">*}
                            {*<label class="product_list_sort__label">Wyświetl:</label>*}
                            {*<select id="products_count_select"*}
                            {*class="product_list_sort__select"*}
                            {*name="products_count_select">*}
                            {*<option selected value="20">20</option>*}
                            {*<option value="40">40</option>*}
                            {*<option value="all">Wszystkie</option>*}
                            {*</select>*}
                            {*</div>*}
                            {*<div class="product_list_sort__options">*}
                            {*<label class="product_list_sort__label">Sortuj według:</label>*}
                            {*<select id="products_sort_select"*}
                            {*class="product_list_sort__select"*}
                            {*name="products_sort_select">*}
                            {*<option selected value="popularity">Popularność</option>*}
                            {*<option value="price_asc">Cena rosnąco</option>*}
                            {*<option value="price_desc">Cena malejąco</option>*}
                            {*</select>*}
                            {*</div>*}
                        </div>
                    </div>
                </div>
            </div>
            <div class="row position-relative">
                <div class="col-lg-3 listing__main_col_small product_list_filter product_list_filter--gray pt-4">
                    <div class="row product_list_filter__row">
                        <div class="col-12">
                            <a class="product_list_filter__link_collapse" data-toggle="collapse" href="#filter_card1"
                               role="button" aria-expanded="false" aria-controls="filter_card1">Wyświetlanie wyników <i
                                        class="icon-filter_arrow product_list_filter__icon"></i></a>
                            <div class="row">
                                <div class="col">
                                    <div class="collapse show multi-collapse" id="filter_card1">
                                        <ul id="product_category_select" class="product_list_filter__options">
                                                    <li class="filter_options__wrapper">
                                                        <label class="filter_options__label">
                                                            <input type="radio"
                                                                   class="filter_options__input"
                                                                   name="subcategories_ids"
                                                                   value="all" checked>
                                                            Wszystkie
                                                            <span class="filter_options__helper"></span>
                                                        </label>
                                                    </li>
                                            <li class="filter_options__wrapper">
                                                <label class="filter_options__label">
                                                    <input type="radio"
                                                           class="filter_options__input"
                                                           name="subcategories_ids"
                                                           value="product">
                                                    Produkty
                                                    <span class="filter_options__helper"></span>
                                                </label>
                                            </li>
                                            <li class="filter_options__wrapper">
                                                <label class="filter_options__label">
                                                    <input type="radio"
                                                           class="filter_options__input"
                                                           name="subcategories_ids"
                                                           value="prize">
                                                    Nagrody
                                                    <span class="filter_options__helper"></span>
                                                </label>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-9 listing__main_col_big product_listing pt-4">
                    <div class="row">
                        <div class="col-12">
                            <div id="product_listing" class="row product_listing__row product_listing--grid equalHeightContainer">
                                {include file={"./_partials/single_item2.tpl"}}
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </form>
</div>
<script>
    {literal}
    $(document).ready(function () {
        function paginationButtons() {
            $('.pagination a').on('click', function (e) {
                e.preventDefault();
                var page_number = $(this).data('page');
                sendForm2(page_number);
            });
        }
        paginationButtons();
        $('#products_count_select, #products_sort_select, #product_category_select input[name="subcategories_ids[]"], .filter_options__input').change(function () {
            sendForm();
        });
        function sendForm() {
            loaderInit();
            $.post(BASE_URL + 'wyszukiwarka/filter_changed.html?phrase={/literal}{if !empty($smarty.get.phrase)}{$smarty.get.phrase}{/if}{literal}', $('#parameters').serialize(), function (response) {
                $('#product_listing').html(response);
            }).done(function () {
                initProductListing();
                paginationButtons();
                loaderHide();
            });
        }
        function sendForm2(page_number) {
            loaderInit();
            $.ajax({
                url: BASE_URL + 'wyszukiwarka/filter_changed.html?phrase={/literal}{if !empty($smarty.get.phrase)}{$smarty.get.phrase}{/if}{literal}',
                type: 'POST',
                data: $('#parameters').serialize() + "&page=" + page_number,
                success: function (response) {
                    $('#product_listing').html(response);
                }
            }).done(function () {
                initProductListing();
                paginationButtons();
                loaderHide();
            });
        }
        $('#product_category_select input[name="subcategories_ids[]"]').change(function () {
            var valueCheck = $(this).val();
            console.log(valueCheck);
            if (valueCheck === 'all') {
                $('.product_listing__single_product').fadeIn();
            } else {
                $('.product_listing__single_product').hide();
                $('.product_listing__single_product[data-item="'+ valueCheck +'"]').fadeIn();
            }
        });
        if ($(window).width() >= 991) {
            lastView();
        }
    });
    {/literal}
</script>
