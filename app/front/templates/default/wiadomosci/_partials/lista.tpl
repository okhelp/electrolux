{load_plugin name="bootbox"}

<form id="message_list_form" method="POST">
    <div class="card-body top_menu">

        <h3 class="card-title">{$title}</h3>

        {if $display_actions}
            <div class="btn-group mb-2 mr-2" role="group" aria-label="Button group with nested dropdown">
                <a href="#" data-action_type="delete" class="btn btn-custom font-18 btn-action"><i
                            class="mdi mdi-delete"></i></a>
            </div>
            <a href="#" id="btn_refresh" class="btn btn-custom mr-2 mb-2"><i
                        class="mdi mdi-reload font-18"></i></a>
            <div class="btn-group mb-2" role="group">
                <button id="btnGroupDrop1" type="button" class="btn btn-custom dropdown-toggle"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> więcej opcji
                </button>
                <div class="dropdown-menu flex-column" aria-labelledby="btnGroupDrop1">
                    <a class="dropdown-item btn-action" href="#" data-action_type="important">Oznacz jako ważne</a>
                    <a class="dropdown-item btn-action" href="#" data-action_type="read">Oznacz jako przeczytane</a>
                    <a class="dropdown-item btn-action" href="#" data-action_type="unread">Oznacz jako
                        nieprzeczytane</a>
                </div>
            </div>
        {/if}
    </div>
    <div class="card-body pt-0 card_no_padding">
        <div class="card b-all shadow-none">
            <div class="inbox-center b-all table-responsive">
                <table class="table table-hover no-wrap mb-0 {if $smarty.server.REQUEST_URI|strstr:'panel/wiadomosci.html'}table_inbox{/if}">
                    <thead>
                    <tr>
                        {if $display_actions}
                            <th width="30">
                                <div class="checkbox">
                                    <input type="checkbox" id="checkbox_all" value="check">
                                    <label for="checkbox_all"></label>
                                </div>
                            </th>
                            <th width="20"></th>
                        {/if}
                        <th width="300">Nadawca</th>
                        <th>Temat</th>
                        <th width="50">Data</th>
                    </tr>
                    </thead>
                    <tbody>
                    {foreach from=$list item=mail}
                        {assign var=user value=$mail->get_user_folder($folder)}
                        {assign var=company_data value=$user->get_company_data()}
                        <tr{if $folder == 'inbox' && $mail->view_to == 0} class="unread"{/if}>
                            {if $display_actions}
                                <td style="width:40px">
                                    <div class="checkbox">
                                        <input type="checkbox" class="message_checkbox" name="message_ids[]"
                                               id="checkbox_{$mail->id}" value="{$mail->id}">
                                        <label for="checkbox_{$mail->id}"></label>
                                    </div>
                                </td>
                                <td style="width:40px" class="hidden-xs-down">
                                    <a href="#" data-hash="{$mail->get_important_hash($folder)}"
                                       class="mark_as_important">
                                        <i class="fa fa-star{if $mail->is_important($folder)} text-warning{/if}"></i>
                                    </a>
                                </td>
                            {/if}
                            <td class="hidden-xs-down">
                                <a href="{$mail->get_url($folder)}" class="d-flex align-items-center">
                                    <img class="img-circle" alt="user" src="{user_avatar id={$user->id} width=50 height=50}" style="margin-right: 18px; float: left; max-width: 50px; max-height: 50px;">
                                    <p>{$user->name} {$user->surname}
                                    {if !empty($company_data)}
                                        <br /><span class="text-muted">{$company_data[0]->name}</span>
                                    {/if}</p>
                                </a>
                            </td>
                            <td class="max-texts">

                                <a href="{$mail->get_url($folder)}">
                                    {$mail->subject}
                                    {if !empty($mail->attachments)}
                                        {assign var=attachment value=json_decode($mail->attachments)}
                                        {if !empty($attachment)}
                                            <i class="fa fa-paperclip"></i>
                                        {/if}
                                    {/if}
                                </a>
                            </td>
                            <td class="text-right"> {$mail->ts->format('Y-m-d H:i:s')|strtotime|friendly_date} </td>
                        </tr>
                    {/foreach}

                    </tbody>
                </table>

            </div>
        </div>

        <div class="card border-0 mt-4">
            <div class="ml-auto">
                {pagination all_items=$all_items per_page=$per_page current_page=$page}
            </div>
        </div>

    </div>
</form>

<script type="text/javascript">
    {literal}
        function run_mass_action(action_type)
        {
            $('#message_list_form').append('<input type="hidden" name="action_type" value="'+action_type+'" />');
            $('#message_list_form').submit();
            loaderHide();
        }

        $(document).ready(function(){


            $('#checkbox_all').click(function(){

                if($(this).is(':checked'))
                {
                    $('.message_checkbox').prop('checked', true);
                }
                else
                {
                    $('.message_checkbox').prop('checked', false);
                }

              });

            $('#btn_refresh').click(function(e){
                e.preventDefault();
                location.reload();
            });

            $('.btn-action').click(function(e){
                e.preventDefault();

                //sprawdzam czy jest minimum jeden checkbox zaznaczony
                if($('.message_checkbox:checked').length > 0)
                {
                    var action_type = $(this).attr('data-action_type');

                    //usunięcie (komunikat)
                    if(action_type == 'delete')
                    {
                        var action_message = "Czy na pewno chcesz usunąć zaznaczone wiadomości?";
                    }
                    else if(action_type == 'important')
                    {
                        var action_message = "Czy na pewno chcesz oznaczyć zaznaczone wiadomości jako ważne?";
                    }
                    else if(action_type == 'read')
                    {
                        var action_message = "Czy na pewno chcesz oznaczyć wiadomości jako przeczytane?";
                    }
                    else if(action_type == 'unread')
                    {
                        var action_message = "Czy na pewno chcesz oznaczyć wiadomości jako nieprzeczytane?";
                    }
                    //pokazuję komunikat, po kliknięciu tak wysyłam formularz
                    Swal.fire({
                        text: action_message,
                        type: 'question',
                        confirmButtonText: 'TAK',
                        showCancelButton: true,
                        cancelButtonText: 'NIE'
                    }).then((result) => {
                        if (result.value) {
                            run_mass_action(action_type);
                            loaderInit();
                        } else if (
                            result.dismiss === Swal.DismissReason.cancel
                        ) {
                            return false
                        }
                    })

                }
                else
                {
                    Swal.fire({
                        text: 'Proszę zaznaczyć min. 1 wiadomość.',
                        type: 'info',
                        confirmButtonText: 'Zamknij',
                        showCancelButton: false,
                    });
                }


            });

            $('.mark_as_important').click(function(e){

                e.preventDefault();

                //pobieram hash
                var hash = $(this).attr('data-hash');

                //pobieram obiekt do zmiennej
                var el = $(this).find('.fa-star');

                //określam czy aktualnie jest oznaczona wiadomość jako ważna czy nie
                var is_now_important = el.hasClass('text-warning');

                //określam treść wiadomości potwierdzającą
                var msg_confirm = is_now_important ? "Czy chcesz usunać oznaczenie wiadomości jako ważną?"
                    : "Czy chcesz oznaczyć tą wiadomość jako ważną?";

                Swal.fire({
                    text: msg_confirm,
                    type: 'question',
                    confirmButtonText: 'TAK',
                    showCancelButton: true,
                    cancelButtonText: 'NIE'
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            url: BASE_URL + "wiadomosci/wazne.html",
                            method: "post",
                            dataType: "json",
                            data: "hash=" + hash,
                            success: function(msg){

                                if(is_now_important)
                                {
                                    el.removeClass('text-warning');
                                }
                                else
                                {
                                    el.addClass('text-warning');
                                }
                            }
                        });
                    } else if (
                        result.dismiss === Swal.DismissReason.cancel
                    ) {
                        return false
                    }
                })

            })



        })
    {/literal}
</script>
