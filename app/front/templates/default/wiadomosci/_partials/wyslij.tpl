<form action="{BASE_URL}wiadomosci/wyslij.html" class="form-material" method="POST" enctype="multipart/form-data">
    <div class="card-body">
        <h3 class="card-title">{if !empty($id_message)}Odpisz na wiadomość{else}Napisz nową wiadomość{/if}</h3>
        {if empty($id_message)}
            <div class="form-group">
                <label>Adresat</label>
                <input id="id_to_autocomplete" class="form-control" placeholder="Wpisz imię i nazwisko..." required="required">
                <input id="id_to" type="hidden" name="id_to" />
            </div>

            <div class="form-group">
                <label>Temat wiadomości</label>
                <input class="form-control" placeholder="Wpisz temat wiadomości..." name="subject" required="required">
            </div>
        {/if}

        <div class="form-group">
            {if empty($id_message)}<label>Treść wiadomości</label>{/if}
            <textarea class="textarea_editor form-control" name="description" rows="15" placeholder="Wpisz treść wiadomości..." required="required">{if !empty($id_message)}{App__Ed__Model__Messages__Model::prepare_text_to_replay($id_message)}{/if}</textarea>
        </div>
        <h4><i class="ti-link"></i> Załączniki</h4>
        <div class="custom-file mb-3">
            <input name="file[]" type="file" multiple class="custom-file-input" id="customFile">
            <label class="custom-file-label form-control" for="customFile" data-desc="Przeglądaj"></label>
        </div>

        {if !empty($id_message)}
            <input type="hidden" name="hash" value="{$hash}" />
        {/if}

        <button type="submit" class="btn btn-success mt-3"><i class="fa fa-envelope-o"></i>Wyślij</button>
    </div>
</form>
<link href="{BASE_URL}data/ed/js/ui/jquery-ui.min.css" rel="stylesheet">
<link rel="stylesheet" href="{BASE_URL}data/ed/assets/plugins/html5-editor/bootstrap-wysihtml5.css" />
<script src="{BASE_URL}data/ed/assets/plugins/html5-editor/wysihtml5-0.3.0.js"></script>
<script src="{BASE_URL}data/ed/assets/plugins/html5-editor/bootstrap-wysihtml5.js"></script>
<script src="{BASE_URL}data/ed/assets/plugins/jqueryui/jquery-ui.min.js"></script>

<script>
    {literal}
        $(document).ready(function() {
            $('.textarea_editor').wysihtml5();
        });

        $(document).ready(function() {

            $("#id_to_autocomplete").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: BASE_URL + "user/ajax_pobierz_uzytkownika.html",
                        dataType: "json",
                        method: 'POST',
                        data: {
                            term: request.term
                        },
                        success: function (data) {
                            response(data);
                        }
                    });
                },
                minLength: 2,
                select: function (event, ui) {
                    $('#id_to').val(ui.item.id);
                }
            });
        });
    $('.custom-file input').change(function() {
        var $el = $(this),
            files = $el[0].files,
            label = files[0].name;
        if (files.length > 1) {
            label = label + " i " + String(files.length - 1) + " więcej plik-ów"
        }
        $el.next('.custom-file-label').html(label);
    });

    {/literal}
</script>