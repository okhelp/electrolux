<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta property="og:url" content="{$smarty.server.HTTP_HOST}{$smarty.server.REQUEST_URI}"/>
    <meta property="og:type" content="website"/>
    {if PRODUCTION}
        <meta name="robots" content="index,follow"/>
    {/if}
    {$metatags}
    <title>{if $page_title}{$page_title} - {/if}{$site_title}</title>
    {* Favicon *}
    <link rel="apple-touch-icon" sizes="180x180"
          href="{BASE_URL}data/front/_assets/images/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32"
          href="{BASE_URL}data/front/_assets/images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16"
          href="{BASE_URL}data/front/_assets/images/favicon/favicon-16x16.png">
    <link rel="manifest" href="{BASE_URL}data/front/_assets/images/favicon/site.webmanifest">
    <link rel="mask-icon" href="{BASE_URL}data/front/_assets/images/favicon/safari-pinned-tab.svg" color="#1a2452">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">
    {* CSSy *}
    <link href="{BASE_URL}data/front/_assets/css/vendor/bootstrap/bootstrap.css" rel="stylesheet"/>
    <link href="{BASE_URL}data/front/_assets/css/vendor/glyphter/Glyphter.css" rel="stylesheet"/>
    <link href="{BASE_URL}data/front/_assets/css/vendor/tooltipster/tooltipster.bundle.min.css" rel="stylesheet"/>
    <link href="{BASE_URL}data/front/_assets/css/vendor/tooltipster/tooltipster-follower.min.css" rel="stylesheet"/>
    <link href="{BASE_URL}data/front/_assets/css/vendor/menu/jquery.mmenu.all.css" rel="stylesheet"/>
    <link href="{BASE_URL}data/front/_assets/css/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet"/>
    <link href="{BASE_URL}data/front/_assets/css/vendor/datatables/responsive.bootstrap4.min.css" rel="stylesheet"/>
    <link href="{BASE_URL}data/front/_assets/css/vendor/slick/slick.css" rel="stylesheet"/>
    <link href="{BASE_URL}data/front/_assets/css/vendor/lightgallery/lightgallery.css" rel="stylesheet"/>
    <link href="{BASE_URL}data/front/_assets/css/vendor/toast/jquery.toast.min.css" rel="stylesheet"/>

    {if !empty($smarty.session.user_services)}
        <link href="{BASE_URL}data/front/_assets/css/color_{$smarty.session.user_services[$smarty.session.id_service]['name']}.css"
              rel="stylesheet"/>
    {else}
        <link href="{BASE_URL}data/front/_assets/css/color_electrolux.css" rel="stylesheet"/>
    {/if}
    {*<link href="{BASE_URL}data/front/_assets/css/color_aeg.css" rel="stylesheet"/>*}
    <link href="{BASE_URL}data/front/_assets/css/style.css" rel="stylesheet"/>
    {if is_ie()}
        <link href="{BASE_URL}data/front/_assets/css/style_ie.css" rel="stylesheet"/>
    {/if}
    <link href="{BASE_URL}data/front/_assets/css/style_messages/style_messages.css" rel="stylesheet"/>
    {*<link href="{BASE_URL}data/front/_assets/css/style_with_colors.css" rel="stylesheet"/>*}
    <script src="{BASE_URL}data/front/_assets/js/vendor/jquery.min.js" type="text/javascript"></script>
    <link href="{BASE_URL}data/front/_assets/css/vendor/select2/select2.min.css" rel="stylesheet"/>
    <script src="{BASE_URL}data/front/_assets/js/vendor/select2/select2.full.min.js" type="text/javascript"></script>
    <!-- Matomo -->
    <script type="text/javascript">
        {literal}
				var _paq = window._paq || [];
				/* tracker methods like "setCustomDimension" should be called before "trackPageView" */
				_paq.push(['trackPageView']);
				_paq.push(['enableLinkTracking']);
				(function () {
					var u = "//electroluxodkuchni.pl/matomo/";
					_paq.push(['setTrackerUrl', u + 'matomo.php']);
					_paq.push(['setSiteId', '1']);
					var d = document, g = d.createElement('script'), s = d.getElementsByTagName('script')[0];
					g.type = 'text/javascript';
					g.async = true;
					g.defer = true;
					g.src = u + 'matomo.js';
					s.parentNode.insertBefore(g, s);
				})();
        {/literal}
    </script>
    <!-- End Matomo Code -->

    <!-- Matomo Tag Manager -->
    <script type="text/javascript">
        {literal}
				var _mtm = _mtm || [];
				_mtm.push({'mtm.startTime': (new Date().getTime()), 'event': 'mtm.Start'});
				var d = document, g = d.createElement('script'), s = d.getElementsByTagName('script')[0];
				g.type = 'text/javascript';
				g.async = true;
				g.defer = true;
				g.src = 'https://electroluxodkuchni.pl/matomo/js/container_rxrn9PV4.js';
				s.parentNode.insertBefore(g, s);
        {/literal}
    </script>
    <!-- End Matomo Tag Manager -->

    <!-- Google Tag Manager -->
    <script>
        {literal}
        (function (w, d, s, l, i){w[l]=w[l]||[];w[l].push(

        {'gtm.start': new Date().getTime(),event:'gtm.js'}
                );var f=d.getElementsByTagName(s)[0],
                j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
                })(window, document, 'script', 'dataLayer', 'GTM-M4DLCJ9');
        {/literal}
    </script>
    <!-- End Google Tag Manager -->
</head>
<body>
{if !empty($display_login_form)}
    {include file="welcome/logowanie.tpl"}
{/if}
{include file="_partials/header/navbar.tpl"}
<div id="mm-wrapper" class="page">
    {* ładowanie pliku z templatki *}

    {* okruszki *}
    {*{if isset($breadcrumb) && $which_page != 'main_page'}*}
    {include file="_partials/breadcrumb.tpl"}
    {*{/if}*}


    {* komunikaty systemowe *}
    {include file="_partials/system_message.tpl"}

    {* ładowanie templatki z kontrollera *}
    {*{include file="welcome/oprogramie.tpl"}*}
    {include file=$template_file}

    {* stopka *}
    {include file="_partials/footer/footer.tpl"}
    {include file="_partials/loader/loader.tpl"}
    {*modal konkurs*}
    {*include file="_partials/modals/modal_new_products.tpl"*}
    {if $isContest eq true}
        {include file="_partials/modals/modal_anniversary.tpl"}
    {else}
        {include file="_partials/modals/modal_training_intro.tpl"}
    {/if}

</div>
<script type="text/javascript">
    {literal}
		var BASE_URL = '{/literal}{BASE_URL}{literal}';
    {/literal}
    {if is_admin()}{literal}
		var BASE_ED_URL = '{/literal}{BASE_ED_URL}{literal}';
    {/literal}{/if}{literal}
    {/literal}
</script>
<!-- Scripts -->
<script src="{BASE_URL}data/front/_assets/js/vendor/lazyload/lazyload.js" type="text/javascript"></script>
<script src="{BASE_URL}data/front/_assets/js/vendor/bootstrap/popper.min.js" type="text/javascript"></script>
<script src="{BASE_URL}data/front/_assets/js/vendor/bootstrap/bootstrap.js" type="text/javascript"></script>
<script src="{BASE_URL}data/front/_assets/js/vendor/tooltipster/tooltipster.bundle.min.js"
        type="text/javascript"></script>
<script src="{BASE_URL}data/front/_assets/js/vendor/tooltipster/tooltipster-follower.min.js"
        type="text/javascript"></script>
<script src="{BASE_URL}data/front/_assets/js/vendor/menu/jquery.mmenu.all.js" type="text/javascript"></script>
<script src="{BASE_URL}data/front/_assets/js/vendor/menu/jquery.mmenu.bootstrap4.js" type="text/javascript"></script>
<script src="{BASE_URL}data/front/_assets/js/vendor/truncate/jquery.dotdotdot.js" type="text/javascript"></script>
<script src="{BASE_URL}data/front/_assets/js/vendor/datatables/jquery.dataTables.min.js"
        type="text/javascript"></script>
<script src="{BASE_URL}data/front/_assets/js/vendor/datatables/dataTables.bootstrap4.min.js"
        type="text/javascript"></script>
<script src="{BASE_URL}data/front/_assets/js/vendor/datatables/dataTables.responsive.min.js"
        type="text/javascript"></script>
<script src="{BASE_URL}data/front/_assets/js/vendor/datatables/responsive.bootstrap4.min.js"
        type="text/javascript"></script>
<script src="{BASE_URL}data/front/_assets/js/vendor/sweetalert/sweetalert2.all.min.js" type="text/javascript"></script>
<script src="{BASE_URL}data/front/_assets/js/vendor/sweetalert/polyfill.min.js" type="text/javascript"></script>
<script src="{BASE_URL}data/front/_assets/js/vendor/slick/slick.min.js" type="text/javascript"></script>
<script src="{BASE_URL}data/front/_assets/js/vendor/lightgallery/lightgallery-all.min.js"
        type="text/javascript"></script>
<script src="{BASE_URL}data/front/_assets/js/vendor/toast/jquery.toast.min.js" type="text/javascript"></script>
{if !empty($smarty.session.id)}
    <script src="{BASE_URL}data/front/_assets/js/cart.js" type="text/javascript"></script>
{/if}
<script src="{BASE_URL}data/front/_assets/js/main.js" type="text/javascript"></script>
{* tryb interaktywnej edycji *}
{if is_admin() && $smarty.get && !empty($smarty.get.interactive_mode)}
    {load_plugin name="ckeditor"}
    {load_plugin name="interactive_mode"}
{/if}
<script>
    {literal}
	$(document).ready(function () {
		$('body').on('click', 'a', function () {
			_paq.push(['trackEvent', 'Nawigacja na stronie', 'link', 'HREF: ' + $(this).attr('href') + ', TEXT: ' + $(this).text()]);
		});
	});
    {/literal}
</script>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M4DLCJ9"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
</body>
</html>
