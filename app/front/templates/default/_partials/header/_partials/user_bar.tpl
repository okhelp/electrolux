{* wersja dla zalogowanego użytkownika *}
{if isset($smarty.session.id) && !empty($smarty.session.id)}
	<div class="pull-right">
		<div class="dropdown">
			<a class=dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
				Witaj, <b>{$smarty.session.name}</b>! <span class="caret"></span>
			</a>
			<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
				<li><a href="{BASE_URL}users/konto.html">Moje konto</a></li>
				<li><a href="#">Another action</a></li>
				<li><a href="#">Something else here</a></li>
				<li role="separator" class="divider"></li>
				<li><a href="{BASE_URL}login/wyloguj.html">Wyloguj się</a></li>
			</ul>
		</div>
	</div>
	{* wersja dla niezalogowanego użytkownika *}
{else}
	<a href="#" class="login-toggle login-open pull-right"><i class="icon-before fa fa-user"></i><span class="hidden-xs">Zaloguj się</span></a>
{/if}