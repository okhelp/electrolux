<div id="navbar_top"
     class="navbar_wrapper navbar_top position-fixed w-100 {if $which_page == 'main_page' } main_page_navbar {/if}">
    <div class="navbar navbar-expand-xl navbar-light">
        <a class="navbar-brand" href="{BASE_URL}">
            <img class="img-fluid"
                 src="{if $which_page == 'main_page' } {BASE_URL}data/front/_assets/images/logo_navy_white.svg {else} {BASE_URL}data/front/_assets/images/logo_navy.svg {/if}"/></a>
        <div class="collapse navbar-collapse justify-content-end" id="menu">

            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="{BASE_URL}oprogramie-p5.html">Program</a>
                </li>
                <li class="nav-item dropdown megamenu-li">
                    <a class="dropdown-toggle nav-link" href="#" id="navbarDropdown"
                       data-toggle="dropdown" aria-haspopup="true">Produkty</a>
                    <ul class="dropdown-menu megamenu row" aria-labelledby="navbarDropdown">
                        {foreach $products_menu as $category_name => $subcategories}
                            <li class="dropdown-item megamenu_links col">
                                <a class="dropdown-item dropdown-menu_category Vertical">{$category_name}</a>
                                <ul class="dropdown_hide_items">
                                    {foreach $subcategories as $subcategory}
                                        <li>
                                            <a class="dropdown-item"
                                               href="{$subcategory['url']}">{$subcategory['name']}</a>
                                        </li>
                                    {/foreach}
                                </ul>
                            </li>
                        {/foreach}
                    </ul>
                </li>
                <li class="nav-item">
                    <a class="nav-link training__link_nav" href="https://www.elucidbyelectrolux.com/customer">Szkolenia
                        i Ankiety</a>
                </li>
                <li class="nav-item dropdown megamenu-li">
                    {if !empty($smarty.session.is_company_admin)}
                        <a class="dropdown-toggle nav-link" href="#" id="navbarDropdown"
                           data-toggle="dropdown" aria-haspopup="true">Nagrody</a>
                        <ul class="dropdown-menu megamenu megamenu_prize row" aria-labelledby="navbarDropdown">
                            <li class="dropdown-item megamenu_links"><a class="megamenu_links_prize"
                                                                        href="{BASE_URL}prizes">Katalog Nagród</a></li>
                            <li class="dropdown-item megamenu_links"><a class="megamenu_links_prize"
                                                                        href="{BASE_URL}prizes?annual">Nagrody
                                    Specjalne</a></li>
                        </ul>
                    {else}
                        <a class="nav-link" href="{BASE_URL}prizes">Nagrody</a>
                    {/if}
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{BASE_URL}kontakt-p4.html">Kontakt</a>
                </li>
            </ul>
        </div>
        <div class="nav_icon_wrapper ml-auto">
            <div class="dropdown">
                <a class="nav_icon_link" href="#" role="button" id="user_submenu" data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false">
                    <i class="nav_icon icon-eok_login"></i>
                </a>
                <div class="dropdown-menu user_submenu_drop" aria-labelledby="user_submenu">
                    <a class="dropdown-item" href="{BASE_URL}panel">moje konto</a>
                    <a class="dropdown-item" href="{BASE_URL}panel/wiadomosci.html">wiadomości</a>
                    <a class="dropdown-item" href="{BASE_URL}panel/tutorial.html">tutorial</a>
                    {if !empty($smarty.session.id)}
                        <a class="dropdown-item" href="{BASE_URL}auth/logout.html">wyloguj się</a>
                    {else}
                        <a class="dropdown-item" href="{BASE_URL}auth/login.html">zaloguj się</a>
                    {/if}
                </div>
            </div>
            <a class="nav_icon_link" href="{BASE_URL}cart"><i class="nav_icon icon-eok_cart"></i>
                <span id="cart-count" class="nav_icon_attention" style="display: none;">66</span></a>
            <div class="dropdown">
                <a class="nav_icon_link" href="#" role="button" id="search_bar_dropdown" data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false">
                    <i class="nav_icon icon-eok_search"></i>
                </a>
                <div class="dropdown-menu user_submenu_drop search_bar_dropdown" aria-labelledby="search_bar_dropdown">
                    <form action="{BASE_URL}wyszukiwarka">
                        <input type="text" name="phrase" class="form-control" required placeholder="szukaj...">
                    </form>
                </div>
            </div>
        </div>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar"
                aria-controls="navbars"
                aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
    </div>
</div>
{if !empty($smarty.session.id)}
    <div class="navbar_points__mobile {if $which_page == 'main_page' } main_page_navbar {/if}">
        {if !empty($smarty.session.display_points)}
            <div class="navbar_points">
                <p class="navbar_points__sum">{if empty($smarty.session.user_points)}0{else}{$smarty.session.user_points}{/if}
                    E-LUX</p>
            </div>
        {/if}
        {if !empty($smarty.session.user_services) && count($smarty.session.user_services) > 1}
            <div class="dropdown navbar_points navbar_company_selection mr-auto">
                <a class="navbar_company_selection__label dropdown-toggle" href="#" role="button" id="company_selection"
                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    {$smarty.session.user_services[$smarty.session.id_service]['name']|upper}
                </a>
                <div class="dropdown-menu user_submenu_drop navbar_company_selection_drop"
                     aria-labelledby="company_selection">
                    {foreach $smarty.session.user_services as $user_service}
                        <a class="dropdown-item navbar_company_selection_link"
                           href="{BASE_URL}auth/change_service.html?id_service={$user_service['id']}">{$user_service['name']|upper}</a>
                    {/foreach}
                </div>
            </div>
        {/if}
    </div>
    {if $isContest eq true}
        <div class="btn-konkurs--wrapper">
            <a href="https://designkitchen.electrolux.com/?lang=pl" class="navbar_points btn btn-danger btn-konkurs"
               target="_blank">
                KONKURSY
            </a>
        </div>
    {/if}
{/if}
{if !empty($smarty.session.id)}
    <script>
			$(document).ready(function () {
				$('a.navbar_company_selection_link').on('click', function (e) {
					e.preventDefault();
					var $link = $(this);
					_paq.push(['trackEvent', 'Podział grup towarowych', $link.text(), $link.attr('href')]);
					window.location.href = $link.attr('href');
				});
				//pobranie punktów użytkownika
				// $.get(BASE_URL + 'panel/ajax_get_my_points.html', function(response)
				// {
				//     $('.navbar_points__sum').html(response + '  E-LUX');
				//     $('.navbar_points').fadeIn();
				// });

				//pobranie ilości produktów w koszyku
				// $.get(BASE_URL + 'cart/get_cart_count.html', function(response)
				// {
				//     if(response.length)
				//     {
				//         $('#cart-count').html(response).fadeIn();
				//     }
				// });
			});
    </script>
{/if}
