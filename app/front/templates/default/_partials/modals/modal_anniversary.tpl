{if !empty($smarty.session.user_services)}
    <div class="modal fade mainModal" id="mainModal" tabindex="-1" role="dialog" aria-labelledby="mainModalTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content anniversary__modal-content">

                <div class="modal-body">
                    <div class="close_first"></div>
                    <div class="anniversary">
                        <button type="button" class="aaniversary__close close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <div class="anniversary__popup">
                            <span class="anniversary__popup--img--wrapper">
                                <a href="https://designkitchen.electrolux.com/?lang=pl"><img src="{BASE_URL}data/front/_assets/images/temporary/popup.jpg" class="anniversary__popup--img"></a>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        {literal}
        $(document).ready(function () {
            //odpalenie pierwszego modala (sprawdzam czy jest wymagane otwarcie)
            function checkCookieTraining() {
                if (($.cookie('training_session2') !== '1') && ($.cookie('training2') !== '1')) {
                    $('#mainModal').modal('show');
                    $('.training__link_nav').addClass('new__link')
                } else if ($.cookie('training2') !== '1') {
                    $('.training__link_nav').addClass('new__link')
                }
            }
            //akcja aby otworzył się drugi modal
            $('.close_first').on('click', function () {
                $('#mainModal').modal('hide');
                setTimeout(function () {
                    $('#mainModal_text').modal('show');
                }, 500);
            });

            //ustawiam cookie na sesję aby modal odpalił się tylko raz po zalogowaniu
            $('#mainModal, #mainModal_text').on('hide.bs.modal', function (e) {
                $.cookie('training_session2', '1', {path: '/'});
            });
            checkCookieTraining();

            $('.training__link_nav, .training__link').on('click', function () {
                $.cookie('training2', '1', { expires: 365, path: '/' });
                $.cookie('training_session2', '1', { expires: 365, path: '/' });
            })
        });

        {/literal}
    </script>
{/if}