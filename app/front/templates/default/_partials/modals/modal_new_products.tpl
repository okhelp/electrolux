{if !empty($smarty.session.user_services)}
    <div class="modal fade modal_competition" id="modal_competition" tabindex="-1" role="dialog" aria-labelledby="modal_competition" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" id="modal_competition_close_save" data-dismiss="modal" aria-label="Close">
                        <i class="icon-filter_cross"></i>
                    </button>
                    <img class="img-fluid modal_img modal_img_left" src="{BASE_URL}data/front/_assets/images/temporary/EOK_modal_left.jpg">
                    <div class="modal_right">
                        <p class="modal_right_slogan">
                            For Better living.<br>
                            Designed in Sweden.
                        </p>
                    </div>
                </div>
                <div class="modal-body">
                    <p class="modal_competition_header"><b>INFORMACJA O NOWYCH PRODUKTACH</b></p>
                    <p class="modal_competition_desc">
                        Szanowni Państwo,<br>
                        Miło nam poinformować, że w programie Electrolux Od Kuchni pojawiły się nowe produkty,
                        przede wszystkim w kategorii Chłodziarko-zamrażarki.<br>
                        Zachęcamy do korzystania z portalu, zbierania E-Lux’ów oraz odbierania nagród.
                    </p>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
    <script>
        {literal}
        $(document).ready(function () {
            $('#modal_competition_close_save').on('click', function () {
                $.cookie('new_product', '1', {expires: 365, path: '/'});
            });
            function checkCookieCompetition() {
                if ($.cookie('new_product') !== '1') {
                    $('#modal_competition').modal('show')
                }
            }
            checkCookieCompetition();
        });
        {/literal}
    </script>
{/if}