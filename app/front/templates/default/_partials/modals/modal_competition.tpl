{if !empty($smarty.session.user_services) && $smarty.session.user_services[$smarty.session.id_service]['name'] === 'aeg'}
    <div class="modal fade modal_competition" id="modal_competition" tabindex="-1" role="dialog" aria-labelledby="modal_competition" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" id="modal_competition_close_save" data-dismiss="modal" aria-label="Close">
                        <i class="icon-filter_cross"></i>
                    </button>
                    <img class="img-fluid" src="{BASE_URL}data/front/_assets/images/temporary/aeg_ads.jpg">
                </div>
                <div class="modal-body">
                    <p class="modal_competition_header"><b>WYOSTRZ ZMYSŁY</b></p>
                    <p class="modal_competition_subheader"><b>KONKURS DLA PROJEKTANTÓW</b></p>
                    <p class="modal_competition_desc">Gotowanie na najwyższym poziomie wymaga zaangażowania wszystkich zmysłów: smaku, dotyku, zapachu, słuchu i wzroku. Wyobraź sobie kuchnię, która wyostrza wszystkie zmysły i zaprojektuj idealne wnętrze!
                        Wybierz, co najmniej cztery urządzenia kuchenne AEG i przygotuj projekt kuchni, która „wyostrza zmysły”.
                        Przejdź na stronę konkursu i dowiedź się więcej!
                    </p>
                </div>
                <div class="modal-footer">
                    <a href="https://kitchendesign.aeg.com/pl/" id="modal_competition_link" class="btn btn-pills btn-outline-light">konkurs</a>
                </div>
            </div>
        </div>
    </div>
    <script>
        {literal}
        $(document).ready(function () {
            $('#modal_competition_close_save, #modal_competition_link').on('click', function () {
                $.cookie('competition_aeg', '1', {expires: 365, path: '/'});
            });
            function checkCookieCompetition() {
                if ($.cookie('competition_aeg') !== '1') {
                    $('#modal_competition').modal('show')
                }
            }
            checkCookieCompetition();
        });
        {/literal}
    </script>
{/if}