{if !empty($smarty.session.user_services)}
    <div class="modal fade mainModal" id="mainModal" tabindex="-1" role="dialog" aria-labelledby="mainModalTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="modal-body">
                    <div class="close_first">
                        <img class="img-fluid"
                             src="{BASE_URL}data/front/_assets/images/temporary/eok_szkolenia-tlo.jpg">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade mainModal_text" id="mainModal_text" tabindex="-1" role="dialog" aria-labelledby="Title"
         aria-hidden="true" style="z-index: 1600;">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="modal-body">
                    <p>
                        Szanowni Państwo,
                        <br>
                        <br>
                        Z przyjemnością oddajemy w Wasze ręce specjalnie zaprojektowaną platformę szkoleniową, która
                        pomoże w przystępny i prosty sposób wzbogacić i ugruntować wiedzę o produktach Electrolux. Na
                        platformie zostały umieszczone szkolenia dotyczące naszych produktów, funkcji i innowacyjnych
                        technologii. Zależało nam, aby format szkoleń był zarówno dostosowany do Waszych potrzeb oraz w
                        pełni merytoryczny. Szeroki wachlarz materiałów zaprezentowany w postaci materiałów wideo,
                        webinarów czy szkoleń pozwala na efektywny i interesujący sposób nauki.
                        <br>
                        <br>
                        Wierzymy, że nieustanne doskonalenie znajomości produktów i pogłębianie wiedzy jest bardzo ważne
                        w świecie nowych technologii, ponieważ gwarantuje eksperckie podejście do potrzeb Konsumenta, co
                        jednoznacznie znajduje odzwierciedlenie w Państwa pewnej pozycji na rynku. Współpraca i wzajemne
                        dzielenie się wiedzą umożliwią rozwój w zakresie tematyki produktów Electrolux oraz pomogą
                        budować Państwa sukces poprzez konkurencyjność opartą na uniwersalnej wartości profesjonalizmu.
                        <br>
                        <br>
                    </p>
                    <p>Zapraszamy do udziału w szkoleniach. Aby rozpocząć proszę kliknąć poniższy link:</p>
                    <p><b><a href="https://www.elucidbyelectrolux.com/customer" class="training__link">Szkolenia i ankiety</a></b></p>
                </div>
            </div>
        </div>
    <script>
        {literal}
        $(document).ready(function () {
            //odpalenie pierwszego modala (sprawdzam czy jest wymagane otwarcie)
            function checkCookieTraining() {
                if (($.cookie('training_session') !== '1') && ($.cookie('training') !== '1')) {
                    $('#mainModal').modal('show');
                    $('.training__link_nav').addClass('new__link')
                } else if ($.cookie('training') !== '1') {
                    $('.training__link_nav').addClass('new__link')
                }
            }
            //akcja aby otworzył się drugi modal
            $('.close_first').on('click', function () {
                $('#mainModal').modal('hide');
                setTimeout(function () {
                    $('#mainModal_text').modal('show');
                }, 500);
            });

            //ustawiam cookie na sesję aby modal odpalił się tylko raz po zalogowaniu
            $('#mainModal, #mainModal_text').on('hide.bs.modal', function (e) {
                $.cookie('training_session', '1', {path: '/'});
            });
            checkCookieTraining();

            $('.training__link_nav, .training__link').on('click', function () {
                $.cookie('training', '1', { expires: 365, path: '/' });
                $.cookie('training_session', '1', { expires: 365, path: '/' });
            })
        });
        // $(document).ready(function () {
        //     $('#modal_competition_close_save').on('click', function () {
        //         $.cookie('new_product', '1', {expires: 365, path: '/'});
        //     });
        //     function checkCookieCompetition() {
        //         if ($.cookie('new_product') !== '1') {
        //             $('#modal_competition').modal('show')
        //         }
        //     }
        //     checkCookieCompetition();
        // });
        {/literal}
    </script>
{/if}
