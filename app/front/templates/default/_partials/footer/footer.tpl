<footer class="footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col col-sm-auto footer__col mr-3">
                <p class="footer_list__title">Program</p>
                <ul class="footer_list">
                    <li class="footer_list__point"><a class="footer_list__link" href="{BASE_URL}oprogramie-p5.html">O Programie</a></li>
                    <li class="footer_list__point"><a class="footer_list__link" href="{BASE_URL}jak-to-dziala-p6.html">Jak to działa?</a></li>
                    <li class="footer_list__point"><a class="footer_list__link" href="{BASE_URL}regulamin-p3.html">Regulamin</a></li>
                </ul>
            </div>
            <div class="col col-sm-auto footer__col mr-3">
                <p class="footer_list__title">Konto Klienta</p>
                <ul class="footer_list">
                    <li class="footer_list__point"><a class="footer_list__link" href="{BASE_URL}panel/faq.html">FAQ</a></li>
                    <li class="footer_list__point"><a class="footer_list__link" href="{BASE_URL}panel">Moje konto</a></li>
                    <li class="footer_list__point"><a class="footer_list__link" href="{BASE_URL}panel/kontakt.html">Kontakt z handlowcem</a></li>
                </ul>
            </div>
            <div class="col col-sm-auto footer__col mr-3 ml-md-auto">
                <p class="footer_list__title">Kontakt</p>
                <ul class="footer_list">
                    <li class="footer_list__point"><a class="footer_list__link" href="{BASE_URL}kontakt-p4.html">Skontaktuj się z nami</a></li>
                    <li class="footer_list__point"><a class="footer_list__link" href="#">Newsletter</a></li>
                    <li class="footer_list__point mt-4"><a class="footer_list__link footer_list__link--big" href="mailto:info@electroluxodkuchni.pl"><i class="icon-email icon_footer"></i>info@electroluxodkuchni.pl</a></li>
                </ul>
            </div>
        </div>
    </div>
    <span class="footer_go_up"><i class="icon-next"></i></span>
</footer>
<script>
    $(".footer_go_up").click(function () {
        $("html,body").animate({
            scrollTop: $(".page").offset().top - 200
        }, "1000")
    });
</script>