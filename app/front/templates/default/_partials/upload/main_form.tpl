<link rel="stylesheet" type="text/css" href="{BASE_URL}data/css/_partials/upload/main_form.css" />
<script type="text/javascript" src="{BASE_URL}data/js/upload.js"></script>
{if isset($images) && !empty($images)}
	<script type="text/javascript" src="{BASE_URL}data/js/exif.js"></script>
{/if}
{* ustawiam parametry uploadu *}
<script type="text/javascript">
	{if isset($images) && !empty($images)} {* określam czy to jest upload obrazków, cimgi!!!*}
		{literal}var images = 1 {/literal}
	{/if}
	{if isset($file_limit) && !empty($file_limit)} {* limit wgrywanych plików, domyślnie bez limitu *}
		{literal}var file_limit = {/literal}{$file_limit}
	{/if}
	{if isset($show_thumb) && !empty($show_thumb)} {* czy pokazać miniaturki - dotyczy tylko plików graficznych *}
		{literal}var show_thumb = {/literal}{$show_thumb}
	{/if}
	{if isset($type_files) && !empty($type_files)} {* dozwolone typy plików *}
		{literal}var type_files = '{/literal}{$type_files}{literal}'{/literal}
	{else}
		{literal}var type_files = 'image/jpg,image/png,image/bmp,image/jpeg,image/gif,text/plain,application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/octetstream,application/x-download'{/literal}
	{/if}
	{if isset($max_file_size) && !empty($max_file_size)} {* maksymalny możliwy rozmiar pliku, domyslnie 10 MB *}
		{literal}var max_file_size = {/literal}{$max_file_size}
	{else}
		{literal}var max_file_size = 10240{/literal}
	{/if}
	{if isset($multiple) && !empty($multiple)} {* multiupload *}
		{literal}var multiple = 1{/literal}
	{else}
		{literal}var multiple = 0{/literal}
	{/if}
</script>

<div class="upload-form">
	{* button dodaj plik/pliki *}
	<input type="file" name="{$name}"{if isset($multiple) && !empty($multiple)} multiple{/if} class="hid"/>
	<a class="btn btn-info btn-upload">
		Wgraj {if isset($multiple) && !empty($multiple)}pliki{else}plik{/if}
	</a>
	{* komunikat o przekroczonej ilości plików *}
	{if isset($file_limit)}
		<div class="alert alert-danger" style="display:none">
			Osiągnięto maksymalną ilość wgywanych plików.
		</div>
	{/if}
	{* loader - gif + opis czynności*}
	<div class="upload-loader" style="display:none">
		<img src="{BASE_URL}data/img/ajax-loader-small.gif" alt="" />
		<span></span>
	</div>
	{* pokazuję miniatury do plików graficznych - jezeli mamy wybraną taką opcję *}
	{if isset($show_thumb) && !empty($show_thumb)}
		<div class="file_reader">
			<div class="row"></div>
		</div>
	{/if}
</div>