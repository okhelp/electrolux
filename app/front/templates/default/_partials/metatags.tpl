{if $metatags}

	{* tytuł strony *}
	{if !empty($metatags.page_title)}
		<meta property="og:title" content="{$metatags.page_title}" />
	{/if}

	{* opis strony *}
	{if !empty($metatags.page_description)}
		<meta name="description" content="{$metatags.page_description}" />
		<meta property="og:description" content="{$metatags.page_description}" />
	{/if}

	{* słowa kluczowe *}
	{if !empty($metatags.page_keywords)}
		<meta name="keywords" content="{$metatags.page_keywords}" />
	{/if}

	{* zdjęcie *}
	{if !empty($metatags.page_social_img)}
		<meta property="og:image" content="{$metatags.page_social_img}" />
	{/if}

{else}
{/if}

