{if isset($session_data.flash_session.message)}
	{* aby było prościej, zrzucam tablicę wiadomości do osobnej zmiennej *}
	{assign var="message" value=$session_data.flash_session.message}

	{* określam typ wiadomości, tak aby móc podać odpowiednią klasę *}
	{if $message.type == "g"}
		{assign var="icon" value="success"}
		{assign var="heading" value="Sukces"}
	{elseif $message.type == "w"}
		{assign var="icon" value="warning"}
		{assign var="heading" value="Błąd"}
	{elseif $message.type == "d"}
		{assign var="icon" value="danger"}
		{assign var="heading" value="Uwaga"}
	{/if}

	{* pokazuję komunikat *}

	<script type="text/javascript">
		{literal}
		$(document).ready(function(){
			$.toast({
				heading: '{/literal}{$heading}{literal}',
				text: '{/literal}{$message.description}{literal}',
				showHideTransition: 'slide',
				icon: '{/literal}{$icon}{literal}',
				position: 'top-right'
			})
		})
		{/literal}
	</script>

{/if}