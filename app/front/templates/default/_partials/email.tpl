<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
<div id="email_template" style="font-family:'Open Sans', 'Helvetica Neue', Helvetica, sans-serif;
		width:100%;
		height:100%;
		background-color:#fafafa;">
	<div id="email_content" style="background-color:#fff;
		padding:24px;
		width:470px;
		margin: 0 auto;">
		<table width="100%">
			<tr>
				<td align="left">
					<a href="{BASE_URL}">
						{if !empty($site_logo)}
							<img src="{img id_cimg=$site_logo width=200 height=34}" alt="" />
						{else}
							<img src="{BASE_URL}data/ed/img/goinweb.svg" style="width:200px;" alt="" />
						{/if}
					</a>
				</td>
				<td align="right">
					<div class="wiadomosc_systemowa" style="color:silver;text-align: right; font-size:11px;">wiadomość automatyczna</div>
				</td>
			</tr>
		</table>
		<hr style="background: silver;
		height: 1px;
		border: 0;
		margin-bottom: 18px;
		margin-top: 36px;" />
		
		<div id="email_tresc" style="line-height: 30px;
		color:#444;">
			{$email_content}
		</div>

		<hr style="background: silver;
		height: 1px;
		border: 0;
		margin-bottom: 18px;
		margin-top: 36px;" />
		
		<div id="email_stopka">
			<table width="100%" style="font-size:11px;
		color:#666;">
				<tr>
					<td colspan="2" align="center">
						<div style="color:silver; font-size: 11px; margin-bottom: 12px;">
							Wiadmość została wysłana autoamtycznie, prosimy na nią nie odpowiadać.
						</div>
					</td>
				</tr>
				<tr>
					<td colspan="2" style="text-align: center">
						&copy; goinweb.pl CMS
					</td>
				</tr>
			</table>
		</div>
	</div>
</div>