{if !empty($photos_list)}
    <div class="main_page_slick">
        {foreach from=$photos_list item=photo name=photos_list}
            {* pobieram informację o zdjęciu *}
            {assign var="photo_data" value=$photo->get_photo_data()}
            <div class="main_page_slick__single" style="background-image: url({img id_cimg=$photo->id_cimg})">
            </div>
        {/foreach}
        <div class="slider-buttons">
            <button class="prev-slide-main-more custom__arrow btn">
                <i class="icon-prev"></i>
            </button>
            <button class="next-slide-main-more custom__arrow btn">
                <i class="icon-next"></i>
            </button>
        </div>
    </div>
{/if}