{if !empty($breadcrumb)}
    <div class="breadcrumbs_wrapper">
        <div class="logo-and-nav-wrap">
            <div class="site-nav-wrap">
                <nav class="nav">
                    <div class="single-breadcrumb-wrap">
                    <span class="breadcrumb_single">
                                <a href="{BASE_URL}">
                            Electrolux
                            </a>
                            </span>
                    </div>
                    {foreach name="breadcrumb_list" from=$breadcrumb item=name key=url}
                        <div class="single-breadcrumb-wrap">
                        <span class="sep">
                            <i class="icon-next"></i>
                        </span>
                            <span class="breadcrumb_single">
                                {if $url}<a href="{$url}">{/if}
                                        {$name}
                                {if $url}</a>{/if}
                            </span>
                        </div>
                    {/foreach}
                </nav>
            </div>
        </div>
    </div>
{/if}