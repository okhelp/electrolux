<form type="post">
    <p class="trader_contact__form_title">Formularz kontaktowy</p>
    <div class="custom_form__input">
        <input class="custom_form__input--input"
               type="text" id="user_name_surname"
               name="user_name_surname">
        <span class="custom_form__input--placeHolder"
              data-placeholder="Imię i nazwisko"></span>
    </div>
    <div class="custom_form__input">
        <input class="custom_form__input--input"
               type="text" id="user_title"
               name="user_title">
        <span class="custom_form__input--placeHolder"
              data-placeholder="Tytuł"></span>
    </div>
    <div class="custom_form__input">
                            <textarea class="custom_form__input--input"
                                      id="user_message" name="user_message"></textarea>
        <span class="custom_form__input--placeHolder"
              data-placeholder="Treść"></span>
    </div>
    <div class="text-right mt-4 w-100">
        <button class="btn btn-pills btn-outline-primary mr-3" type="reset" onclick="inputValueReset();">
            wyczyść
        </button>
        <button class="btn btn-pills btn-primary" type="submit"
                formmethod="post" name="submit" value="1">wyślij
        </button>
    </div>
</form>
<script>
    $(document).ready(function () {
        checkInputValue();
        $('button[type=submit]').click(function () {
            loaderInit();
            $.ajax({
                url: 'url',
                type: 'POST',
                data: $('form').serialize()
            }).done(function (response) {
                $.toast({
                    text: 'Wiadomość wysłana',
                    icon: 'success',
                    position: 'top-right',
                    stack: false
                });
            }).always(function () {
                checkInputValue();
                loaderHide();
            }).fail(function () {
                $.toast({
                    text: 'Wystąpił błąd',
                    icon: 'error',
                    position: 'top-right',
                    stack: false
                })
            })
        });
    });
</script>