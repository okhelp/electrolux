<form action="{BASE_URL}panel/add_new_address.html?id={$id}" type="post">
    <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">{$header_text}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="modal-body">
        <div class="custom_form__input">
            <input class="custom_form__input--input {if !empty($address->name)}has-val{/if}" type="text"
                   id="user_address_title" name="name" value="{if !empty($address)}{$address->name}{/if}">
            <span class="custom_form__input--placeHolder" data-placeholder="Nazwa"></span>
        </div>
        <div class="custom_form__input">
            <input class="custom_form__input--input {if !empty($address->street)}has-val{/if}" type="text"
                   id="user_address_street" name="street" value="{if !empty($address)}{$address->street}{/if}">
            <span class="custom_form__input--placeHolder" data-placeholder="Ulica"></span>
        </div>
        <div class="custom_form__input">
            <input class="custom_form__input--input {if !empty($address->street_number)}has-val{/if}" type="text"
                   id="user_address_street_number" name="street_number"
                   value="{if !empty($address)}{$address->street_number}{/if}">
            <span class="custom_form__input--placeHolder" data-placeholder="Nr budynku"></span>
        </div>
        <div class="custom_form__input">
            <input class="custom_form__input--input {if !empty($address->apartment_number)}has-val{/if}" type="text"
                   id="user_address_street_local" name="apartment_number"
                   value="{if !empty($address)}{$address->apartment_number}{/if}">
            <span class="custom_form__input--placeHolder" data-placeholder="Nr lokalu"></span>
        </div>
        <div class="custom_form__input">
            <input class="custom_form__input--input {if !empty($address->city)}has-val{/if}" type="text"
                   id="user_address_street_city" name="city" value="{if !empty($address)}{$address->city}{/if}">
            <span class="custom_form__input--placeHolder" data-placeholder="Miasto"></span>
        </div>
        <div class="custom_form__input">
            <input class="custom_form__input--input {if !empty($address->post_code)}has-val{/if}" type="text"
                   id="user_address_street_zipCode" name="post_code"
                   value="{if !empty($address)}{$address->post_code}{/if}">
            <span class="custom_form__input--placeHolder" data-placeholder="Kod pocztowy"></span>
        </div>
        {if !empty($smarty.session.is_company_admin)}
            <label class="form__label--1" for="public">
             <span class="text-info-form">
                <p>Widoczny współpracownikom</p>
            </span>
                <input type="checkbox" id="user_address_public" class="check" value="1" name="public"
                       {if !empty($address) && $address->public}checked{/if}>
                <label class="custom__label" for="is_main">
                    <svg viewBox="0,0,50,50">
                        <path d="M5 30 L 20 45 L 45 5"></path>
                    </svg>
                </label>
            </label>
        {/if}
        <label class="form__label--1" for="is_main">
             <span class="text-info-form">
                <p>Ustaw jako główny adres</p>
            </span>
            <input type="checkbox" id="user_address_is_main" class="check" value="1" name="is_main" {if !empty($address) && !empty($address->is_main)}checked{/if}>
            <label class="custom__label" for="is_main">
                <svg viewBox="0,0,50,50">
                    <path d="M5 30 L 20 45 L 45 5"></path>
                </svg>
            </label>
        </label>
        <div class="text-right mt-4">
            <button class="btn btn-pills btn-primary" type="submit" formmethod="post" name="save" value="1">Zapisz
            </button>
        </div>
    </div>
</form>