<form action="{BASE_URL}panel/co_workers.html?id={$id}" type="post">
    <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">{$header_text}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="modal-body">
        <div class="custom_form__input">
            <input class="custom_form__input--input {if !empty($user->name)}has-val{/if}" type="text" id="user_name"
                   name="name" value="{if !empty($user)}{$user->name}{/if}">
            <span class="custom_form__input--placeHolder" data-placeholder="Imię"></span>
        </div>
        <div class="custom_form__input">
            <input class="custom_form__input--input {if !empty($user->surname)}has-val{/if}" type="text" id="user_lname"
                   name="surname" value="{if !empty($user)}{$user->surname}{/if}">
            <span class="custom_form__input--placeHolder" data-placeholder="Nazwisko"></span>
        </div>
        <div class="custom_form__input">
            <input class="custom_form__input--input {if !empty($user->email)}has-val{/if}" type="text" id="user_email"
                   name="email" value="{if !empty($user)}{$user->email}{/if}">
            <span class="custom_form__input--placeHolder" data-placeholder="Email"></span>
        </div>
        <div class="text-right mt-4">
            <button class="btn btn-pills btn-primary" type="submit" formmethod="post" name="save" value="1">Zapisz</button>
        </div>
    </div>
</form>