<div class="modal-header">
    <h5 class="modal-title" id="exampleModalLongTitle">Dodaj Współpracownika</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <div class="form-group row">
        <label for="user_name" class="col-sm-2 col-form-label">Imię</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="user_name" placeholder="Imię">
        </div>
    </div>
    <div class="form-group row">
        <label for="user_lname" class="col-sm-2 col-form-label">Nazwisko</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="user_lname" placeholder="Nazwisko">
        </div>
    </div>
    <div class="form-group row">
        <label for="user_email" class="col-sm-2 col-form-label">Email</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="user_email" placeholder="Email">
        </div>
    </div>
</div>