<form action="{BASE_URL}panel/assign_elux.html?id={$id}" type="post">
    <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Przydziel E-LUXY</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="modal-body">
        <div class="d-flex">
        <div class="custom_form__input w-100">
            <input class="custom_form__input--input" type="number" id="user_points"
                   name="points">
            <span class="custom_form__input--placeHolder" data-placeholder="Ilość punktów"></span>
        </div>
            <span class="w-auto text-nowrap align-self-end" style="padding-bottom: 13px">E-LUX</span>
        </div>
        <div class="text-right mt-4">
            <button class="btn btn-pills btn-primary" type="submit" formmethod="post" name="save" value="1">Zapisz</button>
        </div>
    </div>
</form>