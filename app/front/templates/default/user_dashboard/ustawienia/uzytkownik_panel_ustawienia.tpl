<div class="container-fluid user_panel_container page_content">
    {include file="user_dashboard/_partials/uzytkownik_glowny_panel.tpl"}
    <div class="row">
        <div class="col-12">
            <div class="user_panel">
                <div class="row">
                    <div class="col">
                        <p class="user_panel__title">Ustawienia</p>
                    </div>
                </div>
                <div class="row settings">
                    <div class="col-4 d-none d-lg-flex flex-column pr-4">
                        <div class="faq_link_single current_bg" data-target="header_tab_1">
                            Dane konta
                        </div>
                        <div class="faq_link_single" data-target="header_tab_3">
                            Zmiana hasła
                        </div>
                        <div class="faq_link_single" data-target="header_tab_4">
                            Ustawienia podstawowe
                        </div>
                    </div>
                    <div class="col-12 col-lg-8 accordion__wrapper">
                        <div class="accordion current main-accordion" id="header_tab_1">
                            <div class="card main-card">
                                <div class="card-header main_card_header mb-lg-3" id="headingOne">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link" type="button" data-toggle="collapse"
                                                data-target="#main_tab_1" aria-expanded="false"
                                                aria-controls="#main_tab_1">
                                            Dane konta
                                        </button>
                                    </h2>
                                </div>
                                <div id="main_tab_1" class="main_tab collapse" aria-labelledby="headingOne"
                                     data-parent="#header_tab_1">
                                    <div class="settings_description">
                                        <p class="settings_description__text">
                                            Tu widnieją wszystkie podstawowe informacje, których używasz w naszym
                                            portalu. W każdym momencie
                                            możesz zmienić swoje dane. Zadbaj o ich poprawność i aktualność, aby uniknąć
                                            nieporozumień podczas
                                            składania zamówienia czy jego wysyłki. Dane adresowe możesz zmienić z
                                            zakładki ‘Panel’ w sekcji ‘Twoje
                                            Adresy’.
                                        </p>
                                        {if !App__Ed__Model__Acl::has_group('trader')}
                                        <form type="post" action="{BASE_URL}panel/save_user_data.html">{/if}
                                            <div class="settings_single">
                                                <div class="custom_form__input">
                                                    <input class="custom_form__input--input {if !empty($user_data->name)}has-val{/if}"
                                                           type="text" id="user_name"
                                                           required
                                                           name="name"
                                                           value="{if !empty($user_data)}{$user_data->name}{/if}">
                                                    <span class="custom_form__input--placeHolder"
                                                          data-placeholder="Imię"></span>
                                                </div>
                                                <div class="custom_form__input">
                                                    <input class="custom_form__input--input {if !empty($user_data->surname)}has-val{/if}"
                                                           type="text" id="user_surname"
                                                           name="surname"
                                                           required
                                                           value="{if !empty($user_data)}{$user_data->surname}{/if}">
                                                    <span class="custom_form__input--placeHolder"
                                                          data-placeholder="Nazwisko"></span>
                                                </div>
                                                <div class="custom_form__input">
                                                    <input class="custom_form__input--input {if !empty($user_data->email)}has-val{/if}"
                                                           type="email" id="user_email"
                                                           name="email"
                                                           required
                                                           value="{if !empty($user_data)}{$user_data->email}{/if}">
                                                    <span class="custom_form__input--placeHolder"
                                                          data-placeholder="E-mail"></span>
                                                </div>
                                                <div class="custom_form__input">
                                                    <input class="custom_form__input--input {if !empty($user_data->telephone)}has-val{/if}"
                                                           type="text" id="user_telephone"
                                                           name="telephone"
                                                           required
                                                           value="{if !empty($user_data)}{$user_data->telephone}{/if}">
                                                    <span class="custom_form__input--placeHolder"
                                                          data-placeholder="Telefon"></span>
                                                </div>
                                                <div class="custom_form__input">
                                                    <input class="custom_form__input--input {if !empty($company->name)}has-val{/if}"
                                                           type="text" id="user_firm"
                                                           name="firm"
                                                           value="{if !empty($company)}{$company->name}{/if}" readonly>
                                                    <span class="custom_form__input--placeHolder"
                                                          data-placeholder="Firma"></span>
                                                </div>
                                                <div class="custom_form__input">
                                                    <input class="custom_form__input--input {if !empty($company->nip)}has-val{/if}"
                                                           type="text" id="user_nip"
                                                           name="nip" value="{if !empty($company)}{$company->nip}{/if}"
                                                           readonly>
                                                    <span class="custom_form__input--placeHolder"
                                                          data-placeholder="Nip"></span>
                                                </div>
                                                <div class="text-right mt-4 w-100">
                                                    <button class="btn btn-pills btn-primary" type="submit"
                                                            formmethod="post" name="save" value="1">Zapisz
                                                    </button>
                                                </div>
                                            </div>
                                            {if !App__Ed__Model__Acl::has_group('trader')}</form>{/if}
                                    </div>
                                </div>
                            </div>
                        </div>
                    <div class="accordion main-accordion" id="header_tab_3">
                        <div class="card main-card">
                            <div class="card-header main_card_header" id="headingOne">
                                <h2 class="mb-0">
                                    <button class="btn btn-link" type="button" data-toggle="collapse"
                                            data-target="#main_tab_3" aria-expanded="false"
                                            aria-controls="#main_tab_3">
                                        Zmiana hasła
                                    </button>
                                </h2>
                            </div>
                            <div id="main_tab_3" class="main_tab collapse" aria-labelledby="headingOne"
                                 data-parent="#header_tab_3">
                                <div class="settings_description">
                                    <p class="settings_description__text">
                                        Swoje hasło możesz zmienić ze względów bezpieczeństwa. Poniższa zmiana hasła
                                        wymaga znajomości starego
                                        hasła do portalu. Pamiętaj, że w panelu użytkownika portalu
                                        electroluxodkuchni.pl nie ma możliwości uzyskania
                                        aktualnego hasła dostępu.
                                    </p>
                                    {if !App__Ed__Model__Acl::has_group('trader')}
                                    <form type="post" id="change_password_form"
                                          action="{BASE_URL}panel/change_user_password.html">{/if}
                                        <div class="settings_single">
                                            <div class="d-flex w-100 align-items-center flex-column flex-md-row">
                                                <div class="custom_form__input">
                                                    <input class="custom_form__input--input {*{if !empty($user->name)}has-val{/if}*}"
                                                           type="password" id="user_current_password"
                                                           name="current_password"
                                                           value="{*{if !empty($user)}{$user->name}{/if}*}" required>
                                                    <span class="custom_form__input--placeHolder"
                                                          data-placeholder="Aktualne hasło"></span>
                                                </div>
                                                <p id="wrong_current_password" class="ml-0 ml-md-3 text-danger"
                                                   style="display: none;">Błędne aktualne hasło.</p>
                                            </div>
                                            <div class="d-flex w-100">
                                                <div class="custom_form__input">
                                                    <input class="custom_form__input--input {*{if !empty($user->name)}has-val{/if}*}"
                                                           type="password" id="user_new_password"
                                                           name="new_password"
                                                           value="{*{if !empty($user)}{$user->name}{/if}*}" required>
                                                    <span class="custom_form__input--placeHolder"
                                                          data-placeholder="Nowe hasło"></span>
                                                </div>
                                            </div>
                                            <div class="d-flex w-100 align-items-center flex-column flex-md-row">
                                                <div class="custom_form__input">
                                                    <input class="custom_form__input--input {*{if !empty($user->name)}has-val{/if}*}"
                                                           type="password" id="user_new_password_confirm"
                                                           name="new_password_confirm"
                                                           value="{*{if !empty($user)}{$user->name}{/if}*}" required>
                                                    <span class="custom_form__input--placeHolder"
                                                          data-placeholder="Powtórz nowe hasło"></span>
                                                </div>
                                                <p id="wrong_confirm_password" class="ml-0 ml-md-3 text-danger"
                                                   style="display: none;">Hasła nie są identyczne.</p>
                                            </div>
                                            <div class="text-right mt-4 w-100">
                                                <button id="change_password" class="btn btn-pills btn-primary"
                                                        formmethod="post" name="save" value="1">Zapisz
                                                </button>
                                            </div>
                                        </div>
                                        {if !App__Ed__Model__Acl::has_group('trader')}</form>{/if}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="accordion main-accordion" id="header_tab_4">
                        <div class="card main-card">
                            <div class="card-header main_card_header" id="headingOne">
                                <h2 class="mb-0">
                                    <button class="btn btn-link" type="button" data-toggle="collapse"
                                            data-target="#main_tab_4" aria-expanded="false"
                                            aria-controls="#main_tab_4">
                                        Ustawienia podstawowe
                                    </button>
                                </h2>
                            </div>
                            <div id="main_tab_4" class="main_tab collapse" aria-labelledby="headingOne"
                                 data-parent="#header_tab_4">
                                <div class="settings_description">
                                    <p class="settings_description__text">
                                        To ustawienia wyświetlanej treści i powiadomień. Możesz dowolnie skonfigurować
                                        swoje konto tak aby
                                        otrzymywać tylko te powiadomienia z portalu, które cię interesują (np.
                                        powiadomienia o statusie nagród,
                                        newsletter, itp). Domyślnie portal informuje cię o każdej zmianie.
                                    </p>
                                    {if !App__Ed__Model__Acl::has_group('trader')}
                                    <form type="post" action="{BASE_URL}panel/user_settings.html">{/if}
                                        <div class="settings_single flex-column custom_form_checkbox">
                                            <label class="form__label--1" for="is_main">
                                                     <span class="text-info-form">
                                                        <p>Wyświetlanie ilości E-LUX</p>
                                                    </span>
                                                <input type="checkbox"
                                                       class="check"
                                                       value="1"
                                                       name="7"
                                                       {if isset($user_options[7]) && empty($user_options[7]->status)}{else}checked{/if}>
                                                <label class="custom__label ml-auto" for="">
                                                    <svg viewBox="0,0,50,50">
                                                        <path d="M5 30 L 20 45 L 45 5"></path>
                                                    </svg>
                                                </label>
                                            </label>
                                            <label class="form__label--1" for="is_main">
                                                     <span class="text-info-form">
                                                        <p>Powiadomienie o zmianie hasła</p>
                                                    </span>
                                                <input type="checkbox"
                                                       class="check"
                                                       value="1"
                                                       name="2"
                                                       {if isset($user_options[2]) && empty($user_options[2]->status)}{else}checked{/if}>
                                                <label class="custom__label ml-auto" for="">
                                                    <svg viewBox="0,0,50,50">
                                                        <path d="M5 30 L 20 45 L 45 5"></path>
                                                    </svg>
                                                </label>
                                            </label>
                                            <label class="form__label--1" for="is_main">
                                                     <span class="text-info-form">
                                                        <p>Potwierdzenie utworzenia konta wpółpracownika</p>
                                                    </span>
                                                <input type="checkbox"
                                                       class="check"
                                                       value="1"
                                                       name="3"
                                                       {if isset($user_options[3]) && empty($user_options[3]->status)}{else}checked{/if}>
                                                <label class="custom__label ml-auto" for="">
                                                    <svg viewBox="0,0,50,50">
                                                        <path d="M5 30 L 20 45 L 45 5"></path>
                                                    </svg>
                                                </label>
                                            </label>
                                            <label class="form__label--1" for="is_main">
                                                     <span class="text-info-form">
                                                        <p>Potwierdzenie przekazania punktów</p>
                                                    </span>
                                                <input type="checkbox"
                                                       class="check"
                                                       value="1"
                                                       name="4"
                                                       {if isset($user_options[4]) && empty($user_options[4]->status)}{else}checked{/if}>
                                                <label class="custom__label ml-auto" for="">
                                                    <svg viewBox="0,0,50,50">
                                                        <path d="M5 30 L 20 45 L 45 5"></path>
                                                    </svg>
                                                </label>
                                            </label>
                                            <label class="form__label--1" for="is_main">
                                                     <span class="text-info-form">
                                                        <p>Potwierdzenie zgłoszenia sprzedaży</p>
                                                    </span>
                                                <input type="checkbox"
                                                       class="check"
                                                       value="1"
                                                       name="5"
                                                       {if isset($user_options[5]) && empty($user_options[5]->status)}{else}checked{/if}>
                                                <label class="custom__label ml-auto" for="">
                                                    <svg viewBox="0,0,50,50">
                                                        <path d="M5 30 L 20 45 L 45 5"></path>
                                                    </svg>
                                                </label>
                                            </label>
                                            <label class="form__label--1" for="is_main">
                                                     <span class="text-info-form">
                                                        <p>Zbiorcze powiadomienie o statusach zgłoszeń</p>
                                                    </span>
                                                <input type="checkbox"
                                                       class="check"
                                                       value="1"
                                                       name="6"
                                                       {if isset($user_options[6]) && empty($user_options[6]->status)}{else}checked{/if}>
                                                <label class="custom__label ml-auto" for="">
                                                    <svg viewBox="0,0,50,50">
                                                        <path d="M5 30 L 20 45 L 45 5"></path>
                                                    </svg>
                                                </label>
                                            </label>
                                            <label class="form__label--1" for="is_main">
                                                     <span class="text-info-form">
                                                        <p>Zgoda na newsletter</p>
                                                    </span>
                                                <input type="checkbox"
                                                       class="check"
                                                       value="1"
                                                       name="1"
                                                       {if isset($user_options[1]) && empty($user_options[1]->status)}{else}checked{/if}>
                                                <label class="custom__label ml-auto" for="">
                                                    <svg viewBox="0,0,50,50">
                                                        <path d="M5 30 L 20 45 L 45 5"></path>
                                                    </svg>
                                                </label>
                                            </label>
                                            <div class="text-right mt-4 w-100">
                                                <button class="btn btn-pills btn-primary" type="submit"
                                                        formmethod="post" name="save" value="1">Zapisz
                                                </button>
                                            </div>
                                        </div>
                                        {if !App__Ed__Model__Acl::has_group('trader')}</form>{/if}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<script>
	var fired = false;

	function mobileViewUpdate() {
		var viewportWidth = $(window).width();
		if (viewportWidth >= 992) {
			$(".accordion__wrapper:first-child").addClass("current");
			$(".main_tab").addClass("show");
			fired = false;
		} else if (viewportWidth < 992 && !fired) {
			$(".collapse").removeClass("show");
			$(".accordion__wrapper .btn-link").attr('aria-expanded', 'false');
			$('.main-accordion').hide().fadeIn();
			fired = true;
		}
	}

	$(document).ready(function () {
		mobileViewUpdate();
		checkInputValue();
	});
	$(window).resize(function () {
		mobileViewUpdate();
	});
	$('.faq_link_single').on('click', function () {
		var tab_id = $(this).attr('data-target');
		var target_tab = $("#" + tab_id);
		$('.faq_link_single').removeClass('current_bg');
		$(this).addClass('current_bg');
		$('.main-accordion').hide().removeClass('current');
		$(target_tab).fadeIn().addClass('current');
	});


  {if !App__Ed__Model__Acl::has_group('trader')} {* opcje zapisu są wyłączone dla handlowca *}

	$('button[type=submit]').click(function (e) {
		e.preventDefault();
		var closestForm = $(this).parent('div').parent('div').closest('form');
		var closestFormActionUrl = $(this).parent('div').parent('div').closest('form').attr('action');
		console.log($(closestForm).serialize());
		console.log(closestFormActionUrl);
		loaderInit();
		$.ajax({
			url: closestFormActionUrl,
			type: 'POST',
			data: $(closestForm).serialize()
		}).done(function (response) {
			$.toast({
				text: 'Dane zmienione',
				icon: 'success',
				position: 'top-right',
				stack: false
			});
		}).always(function () {
			checkInputValue();
			loaderHide();
		}).fail(function () {
			$.toast({
				text: 'Wystąpił błąd',
				icon: 'error',
				position: 'top-right',
				stack: false
			})
		})
	});

	/*-------ZMIANA HASŁA-------*/
	var currentPassword = $('#user_current_password');
	var newPassword = $('#user_new_password');
	var confirmPassword = $('#user_new_password_confirm');
	var wrongCurrentPassword = $('#wrong_current_password');
	var wrongConfirmPassword = $('#wrong_confirm_password');

	/*--wysyłka haseł + sprawdzenie aktualnego hasła--*/
	$('#change_password').on('click', function (e) {
		e.preventDefault();
		if ($(newPassword).val() === $(confirmPassword).val()) {
			loaderInit();
			$.ajax({
				url: "{BASE_URL}panel/change_user_password.html",
				type: 'POST',
				data: $('#change_password_form').serialize()
			}).done(function (response) {
				if (/*aktualne było ok*/1) {
					$.toast({
						text: 'Hasło zmienione poprawnie',
						icon: 'success',
						position: 'top-right',
						stack: false
					});
				} else {
					$(wrongCurrentPassword).fadeIn();
					$(currentPassword).val('');
					$(currentPassword).on('focusin', function () {
						$(wrongCurrentPassword).fadeOut();
					})
				}
				$("#change_password_form input").val('');
			}).always(function () {
				checkInputValue();
				loaderHide();
			}).fail(function () {
				$.toast({
					text: 'Wystąpił błąd',
					icon: 'error',
					position: 'top-right',
					stack: false
				});
			})
		} else {
			$(wrongConfirmPassword).fadeIn();
		}
	});

	/*--porównywanie haseł przy wpisywaniu--*/
	function compareNewPassword() {
		if ($(newPassword).val() === $(confirmPassword).val()) {
			$(wrongConfirmPassword).fadeOut();
		} else {
			$(wrongConfirmPassword).fadeIn();
		}
	}

	$(newPassword).on('change', function () {
		if ($(confirmPassword).val()) {
			compareNewPassword();
		}
	});
	$(confirmPassword).on('change', function () {
		compareNewPassword();
	});

  {/if}


</script>
