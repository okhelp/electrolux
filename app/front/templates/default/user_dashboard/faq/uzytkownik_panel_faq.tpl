<div class="container-fluid user_panel_container page_content">
    {include file="user_dashboard/_partials/uzytkownik_glowny_panel.tpl"}
    <div class="row">
        <div class="col-12">
            <div class="user_panel">
                <div class="row">
                    <div class="col">
                        <p class="user_panel__title">FAQ - Najczęściej zadawane pytania</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-4 d-none d-lg-flex flex-column pr-4">
                        {foreach $faq_categories as $faq_category}
                            <div class="faq_link_single {if $faq_category@index == 0}current_bg{/if}" data-target="{$faq_category['category']->seo_name}">
                                {$faq_category['category']->name}
                            </div>
                        {/foreach}
                    </div>
                    <div class="col-12 col-lg-8 accordion__wrapper">
                        {foreach $faq_categories as $index => $faq_category}
                            {assign var=first value = $faq_categories|@key}
                            <div class="accordion {if $faq_categories.$first['category']->id == $faq_category['category']->id}current{/if} main-accordion"
                                 id="{$faq_category['category']->seo_name}">
                                <div class="card main-card">
                                    <div class="card-header main_card_header mb-lg-3" id="headingOne">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link" type="button" data-toggle="collapse"
                                                    data-target="#main_tab_{$faq_category['category']->id}" aria-expanded="false"
                                                    aria-controls="#main_tab_{$faq_category['category']->id}">
                                                {$faq_category['category']->name}
                                            </button>
                                        </h2>
                                    </div>
                                    <div id="main_tab_{$faq_category['category']->id}" class="main_tab collapse" aria-labelledby="headingOne"
                                         data-parent="#{$faq_category['category']->seo_name}">
                                    {foreach $faq_category['questions'] as $question}
                                            <div class="card-body card_body_tab">
                                                <div class="accordion" id="accordion_tab_{$question->id}">
                                                    <div class="card">
                                                        <div class="card-header" id="sub_tab_header_{$question->id}">
                                                            <h2 class="mb-0">
                                                                <button class="btn btn-link" type="button"
                                                                        data-toggle="collapse" data-target="#sub_tab_{$question->id}"
                                                                        aria-expanded="false" aria-controls="sub_tab_{$question->id}">
                                                                    {$question->question}
                                                                </button>
                                                            </h2>
                                                        </div>

                                                        <div id="sub_tab_{$question->id}" class="collapse"
                                                             aria-labelledby="sub_tab_header_{$question->id}"
                                                             data-parent="#accordion_tab_{$question->id}">
                                                            <div class="card-body">
                                                                {$question->answer}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                    {/foreach}
                                    </div>
                                </div>
                            </div>
                        {/foreach}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var fired = false;

    function mobileViewUpdate() {
        var viewportWidth = $(window).width();
        if (viewportWidth >= 992) {
            $(".accordion__wrapper:first-child").addClass("current");
            $(".main_tab").addClass("show");
            fired = false;
        } else if (viewportWidth < 992 && !fired) {
            $(".collapse").removeClass("show");
            $(".accordion__wrapper .btn-link").attr('aria-expanded', 'false');
            $('.main-accordion').hide().fadeIn();
            fired = true;
        }
    }

    $(document).ready(function () {
        mobileViewUpdate();
    });
    $(window).resize(function () {
        mobileViewUpdate();
    });
    $('.faq_link_single').on('click', function () {
        var tab_id = $(this).attr('data-target');
        var target_tab = $("#" + tab_id);
        $('.faq_link_single').removeClass('current_bg');
        $(this).addClass('current_bg');
        $('.main-accordion').hide().removeClass('current');
        $(target_tab).fadeIn().addClass('current');
    })
</script>
