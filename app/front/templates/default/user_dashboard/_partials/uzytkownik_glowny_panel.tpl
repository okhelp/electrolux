<div class="regulations__change_info">
    <div class="regulations__change_content">
        <strong>ZMIANY W REGULAMINIE</strong>
        <p style="margin-bottom: 12px">Szanowni Państwo, informujemy, że nastąpiła zmiana regulaminu programu partnerskiego "Electrolux od Kuchni" (zmiany wchodzą w życie w dniu 06.08.2019r).</p>
        <p><a href="{BASE_URL}regulamin-p3.html" style="text-decoration: underline; font-size: 12px; padding-left: 0;">Prosimy o zapoznanie się z nową treścią regulaminu.</a></p>
    </div>
    <a href="#" class="button__regulations"><i class="icon-filter_cross"></i></a>
</div>
{if !App__Ed__Model__Acl::has_group('trader')} {* handlowcy mają dostęp do panelu, ale ograniczony widok *}
    <div class="row user_panel__main">
        <div class="col-auto d-flex align-items-center">
            {if !empty($user_data->id_cimg)}
                <img class="user_panel__main_photo rounded-circle img-fluid" style="max-height: 128px; max-width: 128px;"
                     src="{img id_cimg={$user_data->id_cimg} height=128 width=128}"/>
            {else}
                <div class="user_panel__main_photo rounded-circle img-fluid user-initials"
                     style="max-height: 67px; max-width: 67px;font-size: 30px;padding: 20px;color: #FFF;width: auto;height: auto;line-height: 28px;">
                    {if !empty($user_data->name)}{$user_data->name|substr:0:1}{/if}{if !empty($user_data->surname)}{$user_data->surname|substr:0:1}{/if}
                </div>
            {/if}
            <div class="user_panel__main_data">
                <p class="user_panel__main_data--big">
                    Konto {if $company->user->role == 'admin'}Premium{else}zwykłe{/if}</p>
                <p class="user_panel__main_data--blue">{$user_data->name} {$user_data->surname}</p>
                <p>{if $company->user->role == 'admin'}Premium{else}Klient{/if}</p>
                <p>W klubie od {$user_data->ts->format('Y')}</p>
            </div>
        </div>
        <div class="col-12 col-xl d-flex flex-column flex-sm-row flex-wrap align-items-center justify-content-between mt-3 mt-xl-0">
            {assign var="achieved_eluxes" value="{if empty($users_e_lux[$smarty.session.id])}0{else}{$users_e_lux[$smarty.session.id]}{/if}"}
            {assign var="spent_eluxes" value="{if empty($user_spend_e_lux)}0{else}{$user_spend_e_lux}{/if}"}
            {assign var="extra_eluxes" value="{if empty($users_extra_e_lux)}0{else}{$users_extra_e_lux}{/if}"}
            <div class="d-flex d-md-block flex-column justify-content-between mb-3 mb-lg-0 user_panel__main_points user_panel__main_points--blue">
                <p>ZDOBYTE E-LUXY</p>
                <p class="user_panel__main_points_value">{$achieved_eluxes} <span
                            class="text-nowrap">E-LUX</span></p>
            </div>
            <div class="d-flex d-md-block flex-column justify-content-between mb-3 mb-lg-0 user_panel__main_points user_panel__main_points--lightBlue">
                <p>WYDANE E-LUXY</p>
                <p class="user_panel__main_points_value">{$spent_eluxes} <span class="text-nowrap">E-LUX</span></p>
            </div>
            <div class="d-flex d-md-block flex-column justify-content-between mb-3 mb-sm-0 user_panel__main_points user_panel__main_points--lightBlueMore">
                <p class="text-nowrap">ZDOBYTE EXTRA E-LUXY</p>
                <p class="user_panel__main_points_value">{$extra_eluxes} <span class="text-nowrap">E-LUX</span></p>
            </div>
            {if !empty($smarty.session.is_company_admin)}
                <div class="d-flex d-md-block flex-column justify-content-between user_panel__main_points user_panel__main_points--gray">
                    <p>ROCZNE E-LUXY</p>
                    <p class="user_panel__main_points_value">{$annual_eluxes} <span class="text-nowrap">E-LUX</span></p>
                </div>
            {/if}
        </div>
    </div>
{else} {* widok dla handlowca *}
    <div class="row user_panel__main">
        <div class="col-auto d-flex align-items-center">
            {if !empty($user_data->id_cimg)}
                <img class="user_panel__main_photo rounded-circle img-fluid" style="max-height: 128px; max-width: 128px;"
                     src="{img id_cimg={$user_data->id_cimg} height=128 width=128}"/>
            {else}
                <div class="user_panel__main_photo rounded-circle img-fluid user-initials"
                     style="max-height: 67px; max-width: 67px;font-size: 30px;padding: 20px;color: #FFF;width: auto;height: auto;line-height: 28px;">
                    {if !empty($user_data->name)}{$user_data->name|substr:0:1}{/if}{if !empty($user_data->surname)}{$user_data->surname|substr:0:1}{/if}
                </div>
            {/if}
            <div class="user_panel__main_data">
                <p class="user_panel__main_data--big">
                    Konto Handlowca</p>
                <p>W klubie od {$user_data->ts->format('Y')}</p>
            </div>
        </div>
        <div class="col-12 col-xl d-flex flex-column flex-sm-row flex-wrap align-items-center justify-content-between mt-3 mt-xl-0">
            <div class="d-flex d-md-block flex-column justify-content-between mb-3 mb-lg-0 user_panel__main_points user_panel__main_points--blue">
                <p>ZDOBYTE E-LUXY</p>
                <p class="user_panel__main_points_value">0 <span
                            class="text-nowrap">E-LUX</span></p>
            </div>
            <div class="d-flex d-md-block flex-column justify-content-between mb-3 mb-lg-0 user_panel__main_points user_panel__main_points--lightBlue">
                <p>WYDANE E-LUXY</p>
                <p class="user_panel__main_points_value">0 <span class="text-nowrap">E-LUX</span></p>
            </div>
            <div class="d-flex d-md-block flex-column justify-content-between mb-3 mb-sm-0 user_panel__main_points user_panel__main_points--lightBlueMore">
                <p class="text-nowrap">ZDOBYTE EXTRA E-LUXY</p>
                <p class="user_panel__main_points_value">0 <span class="text-nowrap">E-LUX</span></p>
            </div>
            {if !empty($smarty.session.is_company_admin)}
                <div class="d-flex d-md-block flex-column justify-content-between user_panel__main_points user_panel__main_points--gray">
                    <p>ROCZNE E-LUXY</p>
                    <p class="user_panel__main_points_value">0 <span class="text-nowrap">E-LUX</span></p>
                </div>
            {/if}
        </div>
    </div>
{/if}

<div class="row mb-3 mb-xl-5">
    <div class="col-12">
        <nav class="navbar navbar-expand-lg navbar-light">
            <p class="d-lg-none text-primary">MENU</p>
            <button class="navbar-toggler ml-auto" type="button" data-toggle="collapse" data-target="#user_panel_nav"
                    aria-controls="user_panel_nav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse user_panel_nav" id="user_panel_nav">
                <ul class="navbar-nav nav">
                    <li class="nav-item">
                        <a class="nav-link" href="{BASE_URL}panel">Panel</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{BASE_URL}panel/wiadomosci.html">Wiadomości</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{BASE_URL}panel/zarejestruj_sprzedaz.html">Zarejestruj
                            sprzedaż</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{BASE_URL}panel/historia_zamowien.html">Historia zamówień</a>
                    </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{BASE_URL}panel/nagrody.html">Nagrody</a>
                        </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{BASE_URL}panel/tutorial.html">Tutorial</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{BASE_URL}panel/kontakt.html">Kontakt z handlowcem</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{BASE_URL}panel/ustawienia.html">Ustawienia</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{BASE_URL}panel/faq.html">FAQ</a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
</div>
<script>
    {literal}
    $(document).ready( function() {
        $('.button__regulations').on('click', function (e) {
            e.preventDefault();
            $.cookie('regulations', '2', {expires: 365, path: '/'});
            $('.regulations__change_info').fadeOut();
        });
        function checkCookie() {
            if ($.cookie('regulations') !== '2') {
                $(".regulations__change_info").css("display", "flex").hide().fadeIn();
            }
        }
        checkCookie();
    });
    {/literal}
</script>
