<div class="container-fluid user_panel_container page_content">
    {include file="user_dashboard/_partials/uzytkownik_glowny_panel.tpl"}
    <div class="row">
        <div id="user_panel__tab_content" class="col-12 user_panel__tab_content">
            <div class="user_panel history_order">
                <div class="row">
                    <div class="col">
                        <p class="user_panel__title">Nagrody</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <table id="user_panel__table" class="table nowrap" style="width:100%">
                            <thead>
                            <tr>
                                <th>Numer zamówienia</th>
                                <th>Data</th>
                                <th>Produkt</th>
                                <th>E-lux jedn.</th>
                                <th>Ilość</th>
                                <th>E-Lux wartość</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#user_panel__table').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": "{BASE_URL}panel/get_user_prices.html",
                "type": 'post',
                "data": function (d) {
                    d.records_total = "{count($user_prizes)}";
                    d.user_id = {$user_data->id};
                }
            },
            "columnDefs": [
                {
                    "orderable": false,
                    "targets": [2,3,4,5],
                }
            ],
            "language" : {
                "processing": "Przetwarzanie...",
                "search": "Szukaj:",
                "lengthMenu": "Pokaż _MENU_ pozycji",
                "info": "Pozycje od _START_ do _END_ z _TOTAL_ łącznie",
                "infoEmpty": "Pozycji 0 z 0 dostępnych",
                "infoFiltered": "(filtrowanie spośród _MAX_ dostępnych pozycji)",
                "infoPostFix": "",
                "loadingRecords": "Wczytywanie...",
                "zeroRecords": "Nie znaleziono pasujących pozycji",
                "emptyTable": "Brak danych",
                "paginate": {
                    "first": "Pierwsza",
                    "previous": "Poprzednia",
                    "next": "Następna",
                    "last": "Ostatnia"
                },
                "aria": {
                    "sortAscending": ": aktywuj, by posortować kolumnę rosnąco",
                    "sortDescending": ": aktywuj, by posortować kolumnę malejąco"
                }
            },
        });
    });
</script>
