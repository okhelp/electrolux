<div class="container-fluid user_panel_container page_content">
    {include file="user_dashboard/_partials/uzytkownik_glowny_panel.tpl"}
    <div class="row">
        <div class="col-12">
            <div class="user_panel">
                <div class="row">
                    <div class="col">
                        <p class="user_panel__title">Zarejestruj sprzedaż</p>
                    </div>
                </div>
                {if !App__Ed__Model__Acl::has_group('trader')} {* handlowcy mają dostęp do panelu, ale ograniczony widok *}
                    <form type="post"
                          action="{BASE_URL}panel/sale/register_sale.html"
                          id="register_sale_form"
                          enctype="multipart/form-data">
                {/if}
                    <div class="row">
                        <div class="col-12 registry_sales__inputs mb-0 mb-lg-4">
                            <div class="registry_sales__options">
                                <select class="registry_sales__select" id="brand" name="brand" required>
                                    <option></option>
                                    {foreach $user_brands as $user_brand}
                                        <option value="{$user_brand->id}">{$user_brand->name|upper}</option>
                                    {/foreach}
                                </select>
                            </div>
                            <div class="registry_sales__options">
                                <select class="registry_sales__select" id="category" name="category" required>
                                    <option></option>
                                    {foreach $main_categories as $category}
                                        <option value="{$category->electrolux_id}">{$category->name}</option>
                                    {/foreach}
                                </select>
                            </div>
                            <div class="registry_sales__options">
                                <select class="registry_sales__select" id="subcategory" name="subcategory" required>
                                    <option></option>
                                </select>
                            </div>
                            <div class="registry_sales__options">
                                <select class="registry_sales__select" id="product" name="product" required>
                                    <option></option>
                                </select>
                            </div>
                            <div class="registry_sales__options">
                                <select class="registry_sales__select" id="quantity" name="count" required>
                                    <option></option>
                                    {for $i=1;$i<=10;$i++}
                                        <option value="{$i}">{$i}</option>
                                    {/for}
                                </select>
                            </div>
                        </div>
                        <div class="col-12 registry_sales__inputs registry_sales__inputs--invoice mb-4">
                            <div class="registry_sales__options">
                                <input class="registry_sales__select registry_sales__input" required minlength="8" maxlength="8" name="invoice_number" placeholder="numer faktury" autocomplete="off" value="" data-toggle="tooltip" data-placement="bottom" title="8 cyfr, bez znaków specjalnych">
                            </div>
                            <div class="registry_sales__button">
                                <button type="submit" formmethod="post" class="btn btn-primary registry_sales__button_submit">
                                    <i class="icon-plus"></i></button>
                            </div>
                        </div>
                    </div>
                {if !App__Ed__Model__Acl::has_group('trader')} {* handlowcy mają dostęp do panelu, ale ograniczony widok *}
                    </form>
                    <div class="row">

                    <div class="col-12">
                        {if !empty($order_history)}
                            <table id="user_panel__table" class="user_panel__table table nowrap" style="width:100%">
                                <thead>
                                <tr>
                                    <th>Numer faktury</th>
                                    <th>Model</th>
                                    <th>Data rejestracji</th>
                                    <th>PNC</th>
                                    <th>Cena jedn.</th>
                                    <th>Ilość</th>
                                    <th>Wartość</th>
                                    <th>E-LUXY</th>
                                    <th>Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                {foreach $order_history as $order}
                                    <tr class="{if $order->status == 0}user_panel__table--gray{/if}{if $order->status == 1}user_panel__table--red{/if}{if $order->status == 2}user_panel__table--blue{/if}">
                                        <td>{if $order->invoice_number}{$order->invoice_number}{else}-{/if}</td>
                                        <td>{if !empty($products[$order->id_product])}{$products[$order->id_product]->model}{/if}</td>
                                        <td>{$order->created_at->format('Y-m-d H:i:s')}</td>
                                        <td>{if !empty($products[$order->id_product])}{$products[$order->id_product]->product_code}{/if}</td>
                                        <td>{$order->price} PLN</td>
                                        <td>{$order->count}</td>
                                        <td>{math equation="x * y" x={$order->price} y={$order->count} format="%.2f"} PLN</td>
                                        <td>{$order->points} E-LUX</td>
                                        <td>{if $order->status == 0}Weryfikacja{/if}{if $order->status == 1}Odrzucone{/if}{if $order->status == 2}Zatwierdzone{/if}</td>
                                    </tr>
                                {/foreach}
                                </tbody>
                                <tfoot>
                                </tfoot>
                            </table>
                        {else}
                            <div class="alert alert-info">Nie zarejestrowano potwierdzonych zamówień</div>
                        {/if}
                    </div>
                </div>
                {/if}
            </div>
        </div>
    </div>
</div>
<script>

    $('[data-toggle="tabajax"]').click(function (e) {
        var $this = $(this),
            loadurl = $this.attr('href'),
            targ = $this.attr('data-target');
        $('[data-toggle="tabajax"]').removeClass('active');
        $this.addClass('active');

        $.post(loadurl, function (data) {
            $(targ).html(data);
        });
        // $.ajax({
        //    type: "POST"
        //    url: loadurl,
        //    data: data,
        // }).done(function(data) {
        //     $(targ).html(data)});

        return false;
    });
    $brand = $('#brand');
    $category = $('#category');
    $subcategory = $('#subcategory');
    $products = $('#product');
    $quantity = $('#quantity');

    function chosenInit() {
        $brand.select2({
            placeholder: "Marka",
            language: {
                noResults: function () {
                    return "Brak wyników...";
                }
            }
        });
        $category.select2({
            placeholder: "Kategoria",
            language: {
                noResults: function () {
                    return "Brak wyników...";
                }
            }
        });
        $subcategory.select2({
            placeholder: "Podkategoria",
            language: {
                noResults: function () {
                    return "Brak wyników...";
                }
            }
        });
        $products.select2({
            placeholder: "Model",
            language: {
                noResults: function () {
                    return "Brak wyników...";
                }
            }
        });
        $quantity.select2({
            placeholder: "Ilość",
            language: {
                noResults: function () {
                    return "Brak wyników...";
                }
            }
        });
    }
    chosenInit();
    $(document).on("input", ".registry_sales__input", function() {
        this.value = this.value.replace(/\D/g,'');
    });
    $('#brand, #category').change(function()
    {
        $subcategory.find('option').not(':first').remove();
        $products.find('option').not(':first').remove();
        if($brand.val() && $category.val()){
            loaderInit();
            $.post(BASE_URL + 'panel/sale/get_subcategories.html', $('#register_sale_form').serialize()
            ).done(
            ).done(
                function(response){
                    $subcategory.append(response);
                    loaderHide()})
        }
    });

    $('#subcategory').change(function()
    {
        $products.find('option').not(':first').remove();

        if($brand.val() && $category.val() && $subcategory.val()){
            loaderInit();
            $.post(BASE_URL + 'panel/sale/get_products.html', $('#register_sale_form').serialize()
            ).done(
                function(response){
                    $products.append(response);
                    loaderHide()})
        }
    });
</script>
