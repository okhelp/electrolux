<div class="row user_panel__main user_panel__trader_contact">
    <div class="col-12 col-lg-6">
        {if !empty($supervisor)}
            <div class="row">
                <div class="col-12 d-flex flex-column flex-sm-row align-items-start">
                    <img style="max-height: 128px; max-width: 128px;" class="user_panel__main_photo rounded-circle img-fluid mb-3 mb-sm-0"
                         src="{img id_cimg={$supervisor->id_cimg} height=128 width=128}"/>
                    <div class="user_panel__main_data">
                        <p class="">Handlowiec</p>
                        <p class="user_panel__main_data--blue mb-2">{$supervisor->name} {$supervisor->surname}</p>
                        <p>{$supervisor->get_position_name()}</p>
                        <p>od {$supervisor->ts->format('Y')}</p>
                        <p class="mt-4">Telefon</p>
                        <p class="user_panel__main_data--blue">{$supervisor->telephone}</p>
                        <p>E-mail</p>
                        <p class="user_panel__main_data--blue">{$supervisor->email}</p>
                    </div>
                </div>
            </div>
        {else}
            <div class="alert alert-info">
                Brak przypisanego handlowca.
            </div>
        {/if}
        <div class="row mt-5 subpage_contact">
            <div class="col-12">
                {show_column name="kontakt_tresc"}
            </div>
        </div>
    </div>
    {if !empty($supervisor)}
        <div class="col-12 col-lg-6 mt-3 mt-lg-0">
            <form id="contact-form">
                <p class="trader_contact__form_title">Formularz kontaktowy</p>
                <div class="custom_form__input">
                    <input class="custom_form__input--input"
                           type="text" id="user_name_surname"
                           name="user_name_surname" required>
                    <span class="custom_form__input--placeHolder"
                          data-placeholder="Imię i nazwisko"></span>
                </div>
                <div class="custom_form__input">
                    <input class="custom_form__input--input"
                           type="text" id="user_title"
                           name="user_title" required>
                    <span class="custom_form__input--placeHolder"
                          data-placeholder="Temat"></span>
                </div>
                <div class="custom_form__input">
                            <textarea class="custom_form__input--input"
                                      id="user_message" name="user_message" required></textarea>
                    <span class="custom_form__input--placeHolder"
                          data-placeholder="Treść"></span>
                </div>
                <div class="text-right mt-4 w-100">
                    <button class="btn btn-pills btn-outline-primary mr-3" type="reset" onclick="inputValueReset();">
                        wyczyść
                    </button>
                    <button class="btn btn-pills btn-primary"
                            name="submit" value="1">wyślij
                    </button>
                </div>
                <input type="hidden" name="id_supervisor" value="{$supervisor->id}">
            </form>
        </div>
    {/if}
</div>

<script>
    $(document).ready(function () {
        checkInputValue();
        $('#contact-form').submit(function (e) {
            e.preventDefault();
            loaderInit();
            $.ajax({
                url: BASE_URL + 'wiadomosci/contact_with_supervisor.html',
                type: 'POST',
                data: $('form').serialize()
            }).done(function (response) {
                $.toast({
                    text: 'Wiadomość wysłana',
                    icon: 'success',
                    position: 'top-right',
                    stack: false
                });
            }).always(function () {
                checkInputValue();
                loaderHide();
            }).fail(function () {
                $.toast({
                    text: 'Wystąpił błąd',
                    icon: 'error',
                    position: 'top-right',
                    stack: false
                })
            })
        });
    });
</script>

