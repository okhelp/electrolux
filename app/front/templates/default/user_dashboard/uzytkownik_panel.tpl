<div class="container-fluid user_panel_container page_content">
    {include file="user_dashboard/_partials/uzytkownik_glowny_panel.tpl"}
    <div class="row">
        <div class="col-12">
            <div class="user_panel">
                <div class="row">
                    <div class="col">
                        <p class="user_panel__title">Twój panel</p>
                    </div>
                </div>
                <div class="row mb-3 mb-xl-4">
                    <div class="col-12 col-md-6 col-lg-4 mb-4 mb-lg-0">
                        <div class="user_panel__box user_panel__box--aeg">
                            <div class="row">
                                <div class="col">
                                    <p class="user_panel__box_text_small">firma</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <p class="user_panel__box_text_bold">{$company->name}</p>
                                    <p class="user_panel__box_text_standard">{$company->name}</p>
                                    <p class="user_panel__box_text_standard">NIP {$company->nip}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-4 mb-4 mb-lg-0">
                        <div class="user_panel__box user_panel__box--backgroundBlue">
                            <div class="row">
                                <div class="col">
                                    <p class="user_panel__box_text_small">KONTO</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <p class="user_panel__box_text_bold">
                                        Konto {if $company->user->role == 'admin'}Premium{else}zwykłe{/if}</p>
                                    <p class="user_panel__box_text_standard">
                                        Konto {if $company->user->role == 'admin'}ma prawa{else}nie ma praw{/if}
                                        administratora</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    {foreach $addresses as $address}
                        {if $address->is_main && !empty($address->public)}
                            <div class="col-12 col-md-6 col-lg-4 mb-4 mb-lg-0">
                                <div class="user_panel__box user_panel__box--backgroundLightBlue">
                                    <div class="row">
                                        <div class="col">
                                            <p class="user_panel__box_text_small">ADRES</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <p class="user_panel__box_text_bold">{$address->name}</p>
                                            <p class="user_panel__box_text_standard">
                                                ul. {$address->street} {$address->street_number}
                                                /{$address->apartment_number}</p>
                                            <p class="user_panel__box_text_standard">{$address->post_code} {$address->city}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        {/if}
                    {/foreach}
                </div>
                {if !empty($smarty.session.is_company_admin)}
                    <div class="row">
                        <div class="col">
                            <p class="user_panel__title">Konta Współpracowników</p>
                        </div>
                    </div>
                    <div class="row mb-3 mb-xl-4 user_panel_box_associate_wrap">
                        {foreach $company_users as $company_user}
                            {if $company_user->id_user != $smarty.session.id}
                                <div class="col-12 col-md-6 col-lg-4 mb-4 user_panel_box_associate associate-id-{$company_user->id_user}">
                                    <div class="user_panel__box">
                                        <div class="row">
                                            <div class="col d-flex justify-content-between">
                                                <p class="user_panel__box_text_small">{$company_user->user->login}</p>
                                                {if $company->user->role == 'admin'}
                                                    <p class="user_panel__box_text_small" data-toggle="modal"
                                                       data-target="#user_panel_modal"
                                                       data-url="{BASE_URL}panel/co_workers.html?id={$company_user->id_user}">
                                                        edytuj</p>
                                                {/if}
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col">
                                                <p class="user_panel__box_text_bold">{$company_user->user->name} {$company_user->user->surname}</p>
                                                <p class="user_panel__box_text_standard">
                                                    {if $company_user->user->type_worker == 1}
                                                        Nasz pracownik
                                                    {elseif $company_user->user->type_worker == 2}
                                                        Niezależny kontrahent {$company_user->user->company_name}
                                                    {elseif $company_user->user->type_worker == 3}
                                                        Pracownik firmy {$company_user->user->company_name}
                                                    {/if}
                                                </p>
                                                <p class="user_panel__box_text_standard">
                                                    {if !empty($users_e_lux[$company_user->id_user])}{$users_e_lux[$company_user->id_user]}{else}0{/if}
                                                    <span class="text-nowrap">E-LUX</span>
                                                </p>
                                            </div>
                                        </div>
                                        <div class="row mt-2">
                                            <div class="col text-left">
                                                <p class="user_panel__box_text_small" data-toggle="modal"
                                                   data-target="#user_panel_modal"
                                                   data-url="{BASE_URL}panel/assign_elux.html?id={$company_user->id_user}">
                                                    przydziel e-luxy</p>
                                            </div>
                                            <div class="col text-right">
                                                <p class="user_panel__box_text_small user_delete" style="cursor: pointer;"
                                                   data-id="{$company_user->id_user}">
                                                    usuń użytkownika</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            {/if}
                        {/foreach}
                        {if $company->user->role == 'admin'}
                            <div class="col-12 col-md-6 col-lg-4 mb-4 user_panel_box_associate">
                                <div class="user_panel__box user_panel__box--add" data-toggle="modal"
                                     data-target="#user_panel_modal" data-url="{BASE_URL}panel/co_workers.html?id=0">
                                    <div class="row">
                                        <div class="col d-flex justify-content-between align-items-center">
                                            <p class="user_panel__box_text_bold">Zaproś współpracownika</p><i
                                                    class="icon-plus"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        {/if}
                    </div>
                {/if}
                <div class="row">
                    <div class="col">
                        <p class="user_panel__title">Twoje Adresy</p>
                    </div>
                </div>
                <div class="row user_panel_box_address_wrap">
                    {foreach $addresses as $address}
                        <div class="col-12 col-md-6 col-lg-4 mb-4" data-address="{$address->id}">
                            <div class="user_panel__box user_panel_box_address">
                                <div class="row">
                                    <div class="col d-flex justify-content-between">
                                        {if $address->is_main}
                                            <p class="user_panel__box_text_small">ADRES DOMYŚLNY - {if empty($address->public)}PRYWATNY{else}FIRMOWY{/if}</p>
                                        {/if}
                                        <p class="user_panel__box_text_small ml-auto" data-toggle="modal"
                                           data-target="#user_panel_modal"
                                           data-url="{BASE_URL}panel/add_new_address.html?id={$address->id}">
                                            edytuj</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <p class="user_panel__box_text_bold">{if !empty($address->name)}{$address->name}{else}Adres{/if}</p>
                                        <p class="user_panel__box_text_standard">
                                            ul. {$address->street} {$address->street_number}
                                            /{$address->apartment_number}</p>
                                        <p class="user_panel__box_text_standard">{$address->post_code} {$address->city}</p>
                                    </div>
                                </div>
                                {if ($smarty.session.is_company_admin && empty($address->is_main)) || ($address->id_user==$smarty.session.id && empty($address->is_main))}
                                    <div class="row">
                                        <div class="col">
                                            <p class="user_panel__box_text_small user_address_delete ml-auto text-right"
                                               data-delete="{$address->id}">
                                                usuń</p>
                                        </div>
                                    </div>
                                {/if}
                            </div>
                        </div>
                    {/foreach}
                    <div class="col-12 col-md-6 col-lg-4 mb-4">
                        <div class="user_panel__box user_panel__box--add user_panel_box_address"
                             data-url="{BASE_URL}panel/add_new_address.html?id=0" data-toggle="modal"
                             data-target="#user_panel_modal">
                            <div class="row">
                                <div class="col d-flex justify-content-between align-items-center">
                                    <p class="user_panel__box_text_bold">Dodaj nowy adres</p><i
                                            class="icon-plus"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade user_panel_container_modal" id="user_panel_modal" tabindex="-1" role="dialog" aria-labelledby="user_panel_modal"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <!--miejsce na tpl?-->
        </div>
    </div>
</div>

<script>
    {literal}
    $('#user_panel_modal').on('show.bs.modal', function (e) {
        var $url = e.relatedTarget.dataset.url;
        $('#user_panel_modal').find('.modal-content').empty();

        loaderInit();
        $.post($url, {'data': '2'}).done(function (response) {
            $('#user_panel_modal').find('.modal-content').html(response);
            loaderHide();
        }).done(function () {
            checkInputValue();
        });
    });

    /*---------usunięcie adresu------------*/
    function deleteUserAddress() {
        $('.user_address_delete').on('click', function () {
            var idAddressDelete = $(this).data('delete');

            function sendTargetVal() {
                loaderInit();
                $.ajax({
                    url: BASE_URL + 'panel/remove_address.html',
                    type: 'POST',
                    data: {
                        'id_address_delete': idAddressDelete
                    }
                }).done(function (response) {
                    $.toast({
                        text: 'Adres usunięty',
                        icon: 'success',
                        position: 'top-right',
                        stack: false
                    });
                    $('div[data-address="' + idAddressDelete + '"]').fadeOut(300, function () {
                        $(this).remove();
                    });
                }).always(function () {
                    loaderHide();
                }).fail(function () {
                    $.toast({
                        text: 'Wystąpił błąd',
                        icon: 'error',
                        position: 'top-right',
                        stack: false
                    });
                });
            }

            Swal.fire({
                text: 'Czy napewno chcesz usunąć adres?',
                type: 'warning',
                confirmButtonText: 'TAK',
                showCancelButton: true,
                cancelButtonText: 'NIE'
            }).then(function (result) {
                if (result.value) {
                    sendTargetVal();
                } else if (result.dismiss === Swal.DismissReason.cancel) {
                    return false;
                }
            });
        });
    }
    deleteUserAddress();

    /*---------usunięcie użytkownika------------*/
    function deleteUser() {
        $('.user_delete').on('click', function () {
            var userId = $(this).data('id');

            function sendTargetVal() {
                loaderInit();
                $.ajax({
                    url: BASE_URL + 'panel/delete_co_worker.html',
                    type: 'POST',
                    data: {
                        'id_user': userId
                    }
                }).done(function (response) {
                    $.toast({
                        text: 'Użytkownik usunięty',
                        icon: 'success',
                        position: 'top-right',
                        stack: false
                    });
                    $('div.associate-id-' + userId).fadeOut(300, function () {
                        $(this).remove();
                    });
                }).always(function () {
                    loaderHide();
                }).fail(function () {
                    $.toast({
                        text: 'Wystąpił błąd',
                        icon: 'error',
                        position: 'top-right',
                        stack: false
                    });
                });
            }

            Swal.fire({
                text: 'Czy napewno chcesz usunąć użytkownika?',
                type: 'warning',
                confirmButtonText: 'TAK',
                showCancelButton: true,
                cancelButtonText: 'NIE'
            }).then(function (result) {
                if (result.value) {
                    sendTargetVal();
                } else if (result.dismiss === Swal.DismissReason.cancel) {
                    return false;
                }
            });
        });
    }
    deleteUser();
    {/literal}
</script>