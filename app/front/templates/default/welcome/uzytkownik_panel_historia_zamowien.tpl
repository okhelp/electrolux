<div class="container-fluid user_panel page_content">
    <div class="row user_panel__main">
        <div class="col-4 d-flex align-items-center">
            <img class="user_panel__main_photo img-fluid" src="{BASE_URL}_images/uzytkownik.png"/>
            <div class="user_panel__main_data">
                <p class="user_panel__main_data--big">Konto nadrzędne</p>
                <p class="user_panel__main_data--blue">Paweł Nowak</p>
                <p>Klient / Premium</p>
                <p>W klubie od 2017</p>
            </div>
        </div>
        <div class="col-8 d-flex align-items-center justify-content-between">
            <div class="user_panel__main_points user_panel__main_points--blue">
                <p>ZDOBYTE PUNKTY</p>
                <p class="user_panel__main_points_value">12345 <span class="text-nowrap">E-LUX</span></p>
            </div>
            <div class="user_panel__main_points user_panel__main_points--lightBlue">
                <p>WYDANE PUNKTY</p>
                <p class="user_panel__main_points_value">12345 <span class="text-nowrap">E-LUX</span></p>
            </div>
            <div class="user_panel__main_points user_panel__main_points--gray">
                <p>EXTRA PUNKTY DO ZDOBYCIA</p>
                <p class="user_panel__main_points_value">12345 <span class="text-nowrap">E-LUX</span></p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <nav class="navbar navbar-expand-lg">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                        aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="user_panel_nav">
                    <ul class="navbar-nav nav">
                        <li class="nav-item">
                            <a class="nav-link" href="{BASE_URL}_images/foo.tpl" data-target="#user_panel__tab_content"
                               data-toggle="tabajax">Panel</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#" data-target="#user_panel__tab_content" data-toggle="tabajax">Wiadomości</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="" data-target="#user_panel__tab_content" data-toggle="tabajax">Zarejestruj
                                sprzedaż</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#" data-target="#user_panel__tab_content" data-toggle="tabajax">Historia
                                zamówień</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#" data-target="#user_panel__tab_content" data-toggle="tabajax">Nagrody</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#" data-target="#user_panel__tab_content" data-toggle="tabajax">Tutorial</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#" data-target="#user_panel__tab_content" data-toggle="tabajax">Kontakt
                                z handlowcem</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#" data-target="#user_panel__tab_content" data-toggle="tabajax">Ustawienia</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#" data-target="#user_panel__tab_content" data-toggle="tabajax">FAQ</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
    <div class="row">
        <div id="user_panel__tab_content" class="col-12 user_panel__tab_content">
            <div class="registry_sales history_order">
                <div class="row">
                    <div class="col">
                        <p class="registry_sales__title">Historia zamówień</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <table id="user_panel__table" class="user_panel__table table nowrap" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Numer zamówienia</th>
                                    <th>Data</th>
                                    <th>Produkt</th>
                                    <th>Kod produktu</th>
                                    <th>Cena jedn.</th>
                                    <th>Ilość</th>
                                    <th>Wartość</th>
                                    <th>Punkty</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>2018PN5490018</td>
                                    <td>12 Luty 2018</td>
                                    <td>AirDry Technology Built-in Fullsize Dish Washer</td>
                                    <td>ESL7550RO</td>
                                    <td>459 PLN</td>
                                    <td>4</td>
                                    <td>1 836 PLN</td>
                                    <td>347 PKT</td>
                                </tr>
                                <tr>
                                    <td>2018PN5490018</td>
                                    <td>12 Luty 2018</td>
                                    <td>AirDry Technology Built-in Fullsize Dish Washer</td>
                                    <td>ESL7550RO</td>
                                    <td>459 PLN</td>
                                    <td>4</td>
                                    <td>1 836 PLN</td>
                                    <td>347 PKT</td>
                                </tr>
                                <tr>
                                    <td>2018PN5490018</td>
                                    <td>12 Luty 2018</td>
                                    <td>AirDry Technology Built-in Fullsize Dish Washer</td>
                                    <td>ESL7550RO</td>
                                    <td>459 PLN</td>
                                    <td>4</td>
                                    <td>1 836 PLN</td>
                                    <td>347 PKT</td>
                                </tr>
                                <tr>
                                    <td>2018PN5490018</td>
                                    <td>12 Luty 2018</td>
                                    <td>AirDry Technology Built-in Fullsize Dish Washer</td>
                                    <td>ESL7550RO</td>
                                    <td>459 PLN</td>
                                    <td>4</td>
                                    <td>1 836 PLN</td>
                                    <td>347 PKT</td>
                                </tr>
                                <tr>
                                    <td>2018PN5490018</td>
                                    <td>12 Luty 2018</td>
                                    <td>AirDry Technology Built-in Fullsize Dish Washer</td>
                                    <td>ESL7550RO</td>
                                    <td>459 PLN</td>
                                    <td>4</td>
                                    <td>1 836 PLN</td>
                                    <td>347 PKT</td>
                                </tr>
                                <tr>
                                    <td>2018PN5490018</td>
                                    <td>12 Luty 2018</td>
                                    <td>AirDry Technology Built-in Fullsize Dish Washer</td>
                                    <td>ESL7550RO</td>
                                    <td>459 PLN</td>
                                    <td>4</td>
                                    <td>1 836 PLN</td>
                                    <td>347 PKT</td>
                                </tr>
                            </tbody>
                            <tfoot>
                            </tfoot>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $('[data-toggle="tabajax"]').click(function (e) {
        var $this = $(this),
            loadurl = $this.attr('href'),
            targ = $this.attr('data-target');
        $('[data-toggle="tabajax"]').removeClass('active');
        $this.addClass('active');

        $.post(loadurl, function (data) {
            $(targ).html(data);
        });
        // $.ajax({
        //    type: "POST"
        //    url: loadurl,
        //    data: data,
        // }).done(function(data) {
        //     $(targ).html(data)});

        return false;
    });
</script>
