<div class="container-fluid product_more">
    <div class="row">
        <div class="col-6 d-flex align-items-center product_more__text">
            <div class="row">
                <div class="col-12">
                    <div class="row">
                        <div class="col-12">
                            <p class="product_more__title">Piekarnik 1</p>
                            <p class="product_more__code">EOY5851FAX</p>
                        </div>
                    </div>
                    <div class="row product_more__row_border">
                        <div class="col-6">
                            <p class="product_more__points">8980 <span class="text-nowrap">E-LUX</span></p>
                        </div>
                        <div class="col-6">
                            <p class="product_more__price">1 999 ZŁ</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <p class="product_more__description">Lorem ipsum dolor sit amet, consectetur adipiscing
                                elit.
                                Mauris congue felis sed justo tempus, eu iaculis diam
                                gravida. Duis rhoncus sollicitudin ante sit amet congue.
                                Interdum et malesuada fames ac ante ipsum primis
                                in faucibus. Aenean vehicula justo vitae malesuada.
                                Pellentesque elit ipsum, malesuada eu leo blandit.
                                Lorem ipsum dolor sit amet.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-6 align-items-center justify-content-center product_more__photo">
            <img class="img-fluid" src="{BASE_URL}_images/product_more/Photo.jpg">
        </div>
        <div id="tab-1" class="col-6 product_more__specification product_more__specification--lightBlue">
            <p class="product_more__specification_title">Cechy produktu</p>
            <ul class="product_more__specification_list">
                <li class="product_more__specification_point">
                    <p class="product_more__specification_point--bold">Funkcja SousVide</p>
                    <span>Funkcja SousVide pozwala korzystać w domu z metody wykorzystywanej w najlepszych restauracjach. Wszystkie składniki wraz z przyprawami są zamykane próżniowo, a następnie gotowane na parze w niskiej temperaturze.</span>
                </li>
                <li class="product_more__specification_point">
                    <p class="product_more__specification_point--bold">Wbudowana książka kucharska</p>
                    <span>Umożliwia szybki dostęp do wielu sprawdzonych przepisów, które pozwolą w pełni wykorzystać możliwości piekarnika. Piekarnik automatycznie ustawia odpowiedni tryb pieczenia, czas i temperaturę w zależności od wybranej potrawy.</span>
                </li>
                <li class="product_more__specification_point">
                    <p class="product_more__specification_point--bold">Czyszczenie parowe</p>
                    <span>Para zmiękcza tłuszcz i zabrudzenia, co ułatwia utrzymanie piekarnika w czystości.</span>
                </li>
            </ul>
        </div>
        <div id="tab-2" class="col-6 product_more__specification">
            <p class="product_more__specification_title">Podstawowe<br> dane techniczne</p>
            <ul class="product_more__specification_list">
                <li class="product_more__specification_point"><span class="product_more__specification_point--bold">Sposób czyszczenia: </span>Pyroliza
                </li>
                <li class="product_more__specification_point"><span class="product_more__specification_point--bold">Interfejs: </span>Wysuwane
                </li>
                <li class="product_more__specification_point"><span class="product_more__specification_point--bold">Zakres temperatur: </span>30
                    C - 300 C
                </li>
                <li class="product_more__specification_point"><span class="product_more__specification_point--bold">Wyposażenie: </span>prowadnice
                    TR1LV
                </li>
                <li class="product_more__specification_point"><span class="product_more__specification_point--bold">Pojemność użytkowa: </span>72
                </li>
                <li class="product_more__specification_point"><span class="product_more__specification_point--bold">Wymiary WxSzxG: </span>594x594x568
                </li>
                <li class="product_more__specification_point"><span class="product_more__specification_point--bold">Całkowity pobór mocy: </span>3480
                </li>
            </ul>
            <ul class="product_more__specification_list_files">
                <li class="product_more__specification_list_files_point"><a href="#">karta katologowa produktu</a></li>
                <li class="product_more__specification_list_files_point"><a href="#">karta informacyjna EU</a></li>
                <li class="product_more__specification_list_files_point"><a href="#">rysunek techniczny</a></li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-6 product_more__extra_links product_more__extra_links--lightBlue">
            <span data-tab="tab-1" class="product_more__tab_link"><i
                        class="icon-plus product_more__extra_links--icon product_more__extra_links--iconChange"></i></i>Cechy</span>
        </div>
        <div class="col-6 product_more__extra_links product_more__extra_links--blue">
            <span data-tab="tab-2" class="product_more__tab_link"><i
                        class="icon-plus product_more__extra_links--icon product_more__extra_links--iconChange"></i>Specyfikacja</span>
        </div>
    </div>
</div>

