<div class="position-relative d-flex main_page justify-content-end align-items-center">
    <p class="main_page__header">
        For Better living.<br/>Designed in Sweden.
    </p>
    <div class="main_page__fanpage_container">
        <a href="https://www.facebook.com/electroluxpolska/"><i class="icon-facebook icon_fanpage"></i></a>
        <a href="https://www.instagram.com/electrolux_pl/"><i class="icon-instagram icon_fanpage"></i></a>
        <a href="https://twitter.com/electrolux"><i class="icon-twitter icon_fanpage"></i></a>
        <a href="https://www.youtube.com/playlist?list=PL5AFA56CA93C94492"><i class="icon-youtube icon_fanpage"></i></a>
    </div>
    {*<div class="main_page_slick">*}
        {*<div class="main_page_slick__single">1</div>*}
        {*<div class="main_page_slick__single">2</div>*}
        {*<div class="main_page_slick__single">3</div>*}
        {*<div class="main_page_slick__single">4</div>*}
        {*<div class="slider-buttons">*}
            {*<button class="prev-slide-main-more custom__arrow btn">*}
                {*<i class="icon-prev"></i>*}
            {*</button>*}
            {*<button class="next-slide-main-more custom__arrow btn">*}
                {*<i class="icon-next"></i>*}
            {*</button>*}
        {*</div>*}
    {*</div>*}
    {show_block name="gallery_photos" id="9" template="_partials/main_gallery/main_gallery.tpl"}
</div>

