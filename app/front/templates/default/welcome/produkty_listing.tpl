<div class="container-fluid product_list page_content">
    <div class="row mb-4">
        <div class="col-3 d-flex align-items-center">
            <div class="row">
                <div class="col-12">
                    <p id="filter_show" class="product_list_filter__title">Filtruj Produkty<span class="product_list_filter__icon_show"><i class="icon-arrow"></i><i class="icon-filter_cross"></i></span></p>
                </div>
            </div>
        </div>
        <div class="col-9">
            <div class="row product_list_sort__row">
                <div class="col-4 d-flex align-items-center">
                    <a id="grid" class="product_list_sort__link" href="#"><i
                                class="icon-filter_grid filter_grid_icon"></i></a>
                    <a id="list" class="product_list_sort__link" href="#"><i
                                class="icon-filter_list1 filter_grid_icon"></i></a>
                    <a id="list_no_img" class="product_list_sort__link" href="#"><i
                                class="icon-filter_list2 filter_grid_icon"></i></a>
                </div>
                <div class="col-8 d-flex justify-content-end product_list_sort__options--wrapper">
                    <div class="product_list_sort__options">
                        <label class="product_list_sort__label">Wyświetl:</label>
                        <select class="product_list_sort__select">
                            <option>Wszystkie</option>
                            <option>Wszystkie2</option>
                            <option>Wszystkie3</option>
                        </select>
                    </div>
                    <div class="product_list_sort__options">
                        <label class="product_list_sort__label">Sortuj według:</label>
                        <select class="product_list_sort__select">
                            <option>Popularność</option>
                            <option>Popularność1</option>
                            <option>Popularność2</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-3 product_list_filter">
            <div class="row product_list_filter__row">
                <div class="col-12">
                    <a class="product_list_filter__link_collapse" data-toggle="collapse" href="#filter_card1"
                       role="button" aria-expanded="false" aria-controls="filter_card1">Rodzaj produktu <i
                                class="icon-filter_arrow product_list_filter__icon"></i></a>
                    <div class="row">
                        <div class="col">
                            <div class="collapse show multi-collapse" id="filter_card1">
                                <ul class="product_list_filter__options">
                                    <li class="filter_options__wrapper">
                                        <label class="filter_options__label">
                                            <input type="checkbox" class="filter_options__input" name="" value="">Piekarnik
                                            parowy<span class="filter_options__helper"></span>
                                        </label>
                                    </li>
                                    <li class="filter_options__wrapper">
                                        <label class="filter_options__label">
                                            <input type="checkbox" class="filter_options__input" name="" value="">Piekarnik
                                            tradycyjny<span class="filter_options__helper"></span>
                                        </label>
                                    </li>
                                    <li class="filter_options__wrapper">
                                        <label class="filter_options__label">
                                            <input type="checkbox" class="filter_options__input" name="" value="">Piekarnik
                                            kompaktowy<span class="filter_options__helper"></span>
                                        </label>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row product_list_filter__row">
                <div class="col-12">
                    <a class="product_list_filter__link_collapse" data-toggle="collapse" href="#filter_card2"
                       role="button" aria-expanded="false" aria-controls="filter_card2">Sterowanie <i
                                class="icon-filter_arrow product_list_filter__icon"></i></a>
                    <div class="row">
                        <div class="col">
                            <div class="collapse show multi-collapse" id="filter_card2">
                                <ul class="product_list_filter__options">
                                    <li class="filter_options__wrapper">
                                        <label class="filter_options__label">
                                            <input type="checkbox" class="filter_options__input" name="" value="">manualnie<span
                                                    class="filter_options__helper"></span>
                                        </label>
                                    </li>
                                    <li class="filter_options__wrapper">
                                        <label class="filter_options__label">
                                            <input type="checkbox" class="filter_options__input" name="" value="">panel
                                            cyfrowy<span class="filter_options__helper"></span>
                                        </label>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row product_list_filter__row">
                <div class="col-12">
                    <a class="product_list_filter__link_collapse" data-toggle="collapse" href="#filter_card3"
                       role="button" aria-expanded="false" aria-controls="filter_card3">Rodzaj produktu <i
                                class="icon-filter_arrow product_list_filter__icon"></i></a>
                    <div class="row">
                        <div class="col">
                            <div class="collapse show multi-collapse" id="filter_card3">
                                <ul class="product_list_filter__options">
                                    <li class="filter_options__wrapper">
                                        <label class="filter_options__label">
                                            <input type="checkbox" class="filter_options__input" name="" value="">CombiSteam
                                            Deluxe<span class="filter_options__helper"></span>
                                        </label>
                                    </li>
                                    <li class="filter_options__wrapper">
                                        <label class="filter_options__label">
                                            <input type="checkbox" class="filter_options__input" name="" value="">CombiSteam<span
                                                    class="filter_options__helper"></span>
                                        </label>
                                    </li>
                                    <li class="filter_options__wrapper">
                                        <label class="filter_options__label">
                                            <input type="checkbox" class="filter_options__input" name="" value="">SousVide<span
                                                    class="filter_options__helper"></span>
                                        </label>
                                    </li>
                                    <li class="filter_options__wrapper">
                                        <label class="filter_options__label">
                                            <input type="checkbox" class="filter_options__input" name="" value="">PlusSteam<span
                                                    class="filter_options__helper"></span>
                                        </label>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-9 product_listing">
            <div class="row">
                <div class="col-12">
                    <div class="row product_listing__row product_listing--grid">
                        <div class="product_listing__single_product">
                            <div class="row single_product__img_row">
                                <div class="col-12 single_product_info--wrapper">
                                    <div class="single_product_info">
                                        <div class="single_product_info--container">
                                            <p class="single_product_info--label single_product_info--new">nowość</p>
                                        </div>
                                        <div class="single_product_info--container">
                                            <p class="single_product_info--label single_product_info--end">koniec
                                                serii</p>
                                        </div>
                                        <div class="single_product_info--container">
                                            <p class="single_product_info--label single_product_info--quality">super
                                                jakość</p>
                                        </div>
                                    </div>
                                    <img class="single_product__img img-fluid extra_info"
                                         data-tooltip-content="#tooltip_content1"
                                         src="{BASE_URL}_images/product_list/oven.png">
                                    <div class="tooltip_templates">
                                        <div class="extra_info_tooltip" id="tooltip_content1">
                                            <div class="container">
                                                <div class="row mb-3">
                                                    <div class="col-6">
                                                        <p class="single_product__name single_product__name--bold">Piekarnik 1</p>
                                                    </div>
                                                    <div class="col-6 text-right">
                                                        <p class="single_product__points">890 <span class="text-nowrap">E-LUX</span></p>
                                                    </div>
                                                </div>
                                                <div class="row mb-3">
                                                    <div class="col-12">
                                                        <p class="tooltip_product__subtitle">Piekarnik z funkcją pieczenia 3w1 ”Nazwa funkcji”</p>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12">
                                                        <p class="tooltip_product__description">Lorem ipsum dolor sit amet,
                                                            consectetur adipiscing elit. Etiam vitae
                                                            venenatis odio. Etiam et eros ac quam
                                                            vulputate maximus lorem.
                                                            Donec vel quam nec eros bibendum
                                                            rhoncus eu nec est. In condimentum.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row single_product__price_row">
                                <div class="col-6">
                                    <p class="single_product__name">Piekarnik 1</p>
                                </div>
                                <div class="col-6 text-right single_product__price--container">
                                    <p class="single_product__points">890 <span class="text-nowrap">E-LUX</span></p>
                                    <p class="single_product__price">1 999 ZŁ</p>
                                </div>
                            </div>
                            <div class="row single_product__buttons">
                                <div class="col-6">
                                    <button type="button"
                                            class="btn btn-outline-primary btn-pills single_product__more">więcej
                                    </button>
                                </div>
                                <div class="col-6">
                                    <button type="button" class="btn btn-primary btn-pills single_product__order">
                                        zamów
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="product_listing__single_product">
                            <div class="row single_product__img_row">
                                <div class="col-12 single_product_info--wrapper">
                                    <div class="single_product_info">
                                        <div class="single_product_info--container">
                                            <p class="single_product_info--label single_product_info--new">nowość</p>
                                        </div>
                                        <div class="single_product_info--container">
                                            <p class="single_product_info--label single_product_info--end">koniec
                                                serii</p>
                                        </div>
                                        <div class="single_product_info--container">
                                            <p class="single_product_info--label single_product_info--quality">super
                                                jakość</p>
                                        </div>
                                    </div>
                                    <img class="single_product__img img-fluid extra_info"
                                         data-tooltip-content="#tooltip_content2"
                                         src="{BASE_URL}_images/product_list/oven.png">
                                    <div class="tooltip_templates">
                                        <div class="extra_info_tooltip" id="tooltip_content2">
                                            <div class="container">
                                                <div class="row mb-3">
                                                    <div class="col-6">
                                                        <p class="single_product__name single_product__name--bold">Piekarnik 1</p>
                                                    </div>
                                                    <div class="col-6 text-right">
                                                        <p class="single_product__points">890 <span class="text-nowrap">E-LUX</span></p>
                                                    </div>
                                                </div>
                                                <div class="row mb-3">
                                                    <div class="col-12">
                                                        <p class="tooltip_product__subtitle">Piekarnik z funkcją pieczenia 3w1 ”Nazwa funkcji”</p>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12">
                                                        <p class="tooltip_product__description">Lorem ipsum dolor sit amet,
                                                            consectetur adipiscing elit. Etiam vitae
                                                            venenatis odio. Etiam et eros ac quam
                                                            vulputate maximus lorem.
                                                            Donec vel quam nec eros bibendum
                                                            rhoncus eu nec est. In condimentum.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row single_product__price_row">
                                <div class="col-6">
                                    <p class="single_product__name">Piekarnik 1</p>
                                </div>
                                <div class="col-6 text-right single_product__price--container">
                                    <p class="single_product__points">890 <span class="text-nowrap">E-LUX</span></p>
                                    <p class="single_product__price">1 999 ZŁ</p>
                                </div>
                            </div>
                            <div class="row single_product__buttons">
                                <div class="col-6">
                                    <button type="button"
                                            class="btn btn-outline-primary btn-pills single_product__more">więcej
                                    </button>
                                </div>
                                <div class="col-6">
                                    <button type="button" class="btn btn-primary btn-pills single_product__order">
                                        zamów
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="product_listing__single_product">
                            <div class="row single_product__img_row">
                                <div class="col-12 single_product_info--wrapper">
                                    <div class="single_product_info">
                                        <div class="single_product_info--container">
                                            <p class="single_product_info--label single_product_info--new">nowość</p>
                                        </div>
                                        <div class="single_product_info--container">
                                            <p class="single_product_info--label single_product_info--end">koniec
                                                serii</p>
                                        </div>
                                        <div class="single_product_info--container">
                                            <p class="single_product_info--label single_product_info--quality">super
                                                jakość</p>
                                        </div>
                                    </div>
                                    <img class="single_product__img img-fluid extra_info"
                                         data-tooltip-content="#tooltip_content3"
                                         src="{BASE_URL}_images/product_list/oven.png">
                                    <div class="tooltip_templates">
                                        <div class="extra_info_tooltip" id="tooltip_content3">
                                            <div class="container">
                                                <div class="row mb-3">
                                                    <div class="col-6">
                                                        <p class="single_product__name single_product__name--bold">Piekarnik 1</p>
                                                    </div>
                                                    <div class="col-6 text-right">
                                                        <p class="single_product__points">890 <span class="text-nowrap">E-LUX</span></p>
                                                    </div>
                                                </div>
                                                <div class="row mb-3">
                                                    <div class="col-12">
                                                        <p class="tooltip_product__subtitle">Piekarnik z funkcją pieczenia 3w1 ”Nazwa funkcji”</p>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12">
                                                        <p class="tooltip_product__description">Lorem ipsum dolor sit amet,
                                                            consectetur adipiscing elit. Etiam vitae
                                                            venenatis odio. Etiam et eros ac quam
                                                            vulputate maximus lorem.
                                                            Donec vel quam nec eros bibendum
                                                            rhoncus eu nec est. In condimentum.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row single_product__price_row">
                                <div class="col-6">
                                    <p class="single_product__name">Piekarnik 1</p>
                                </div>
                                <div class="col-6 text-right single_product__price--container">
                                    <p class="single_product__points">890 <span class="text-nowrap">E-LUX</span></p>
                                    <p class="single_product__price">1 999 ZŁ</p>
                                </div>
                            </div>
                            <div class="row single_product__buttons">
                                <div class="col-6">
                                    <button type="button"
                                            class="btn btn-outline-primary btn-pills single_product__more">więcej
                                    </button>
                                </div>
                                <div class="col-6">
                                    <button type="button" class="btn btn-primary btn-pills single_product__order">
                                        zamów
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="product_listing__single_product">
                            <div class="row single_product__img_row">
                                <div class="col-12 single_product_info--wrapper">
                                    <div class="single_product_info">
                                        <div class="single_product_info--container">
                                            <p class="single_product_info--label single_product_info--new">nowość</p>
                                        </div>
                                        <div class="single_product_info--container">
                                            <p class="single_product_info--label single_product_info--end">koniec
                                                serii</p>
                                        </div>
                                        <div class="single_product_info--container">
                                            <p class="single_product_info--label single_product_info--quality">super
                                                jakość</p>
                                        </div>
                                    </div>
                                    <img class="single_product__img img-fluid extra_info"
                                         data-tooltip-content="#tooltip_content4"
                                         src="{BASE_URL}_images/product_list/oven.png">
                                    <div class="tooltip_templates">
                                        <div class="extra_info_tooltip" id="tooltip_content4">
                                            <div class="container">
                                                <div class="row mb-3">
                                                    <div class="col-6">
                                                        <p class="single_product__name single_product__name--bold">Piekarnik 1</p>
                                                    </div>
                                                    <div class="col-6 text-right">
                                                        <p class="single_product__points">890 <span class="text-nowrap">E-LUX</span></p>
                                                    </div>
                                                </div>
                                                <div class="row mb-3">
                                                    <div class="col-12">
                                                        <p class="tooltip_product__subtitle">Piekarnik z funkcją pieczenia 3w1 ”Nazwa funkcji”</p>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12">
                                                        <p class="tooltip_product__description">Lorem ipsum dolor sit amet,
                                                            consectetur adipiscing elit. Etiam vitae
                                                            venenatis odio. Etiam et eros ac quam
                                                            vulputate maximus lorem.
                                                            Donec vel quam nec eros bibendum
                                                            rhoncus eu nec est. In condimentum.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row single_product__price_row">
                                <div class="col-6">
                                    <p class="single_product__name">Piekarnik 1</p>
                                </div>
                                <div class="col-6 text-right single_product__price--container">
                                    <p class="single_product__points">890 <span class="text-nowrap">E-LUX</span></p>
                                    <p class="single_product__price">1 999 ZŁ</p>
                                </div>
                            </div>
                            <div class="row single_product__buttons">
                                <div class="col-6">
                                    <button type="button"
                                            class="btn btn-outline-primary btn-pills single_product__more">więcej
                                    </button>
                                </div>
                                <div class="col-6">
                                    <button type="button" class="btn btn-primary btn-pills single_product__order">
                                        zamów
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="product_listing__single_product">
                            <div class="row single_product__img_row">
                                <div class="col-12 single_product_info--wrapper">
                                    <div class="single_product_info">
                                        <div class="single_product_info--container">
                                            <p class="single_product_info--label single_product_info--new">nowość</p>
                                        </div>
                                        <div class="single_product_info--container">
                                            <p class="single_product_info--label single_product_info--end">koniec
                                                serii</p>
                                        </div>
                                        <div class="single_product_info--container">
                                            <p class="single_product_info--label single_product_info--quality">super
                                                jakość</p>
                                        </div>
                                    </div>
                                    <img class="single_product__img img-fluid extra_info"
                                         data-tooltip-content="#tooltip_content4"
                                         src="{BASE_URL}_images/product_list/oven.png">
                                    <div class="tooltip_templates">
                                        <div class="extra_info_tooltip" id="tooltip_content4">
                                            <div class="container">
                                                <div class="row mb-3">
                                                    <div class="col-6">
                                                        <p class="single_product__name single_product__name--bold">Piekarnik 1</p>
                                                    </div>
                                                    <div class="col-6 text-right">
                                                        <p class="single_product__points">890 <span class="text-nowrap">E-LUX</span></p>
                                                    </div>
                                                </div>
                                                <div class="row mb-3">
                                                    <div class="col-12">
                                                        <p class="tooltip_product__subtitle">Piekarnik z funkcją pieczenia 3w1 ”Nazwa funkcji”</p>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12">
                                                        <p class="tooltip_product__description">Lorem ipsum dolor sit amet,
                                                            consectetur adipiscing elit. Etiam vitae
                                                            venenatis odio. Etiam et eros ac quam
                                                            vulputate maximus lorem.
                                                            Donec vel quam nec eros bibendum
                                                            rhoncus eu nec est. In condimentum.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row single_product__price_row">
                                <div class="col-6">
                                    <p class="single_product__name">Piekarnik 1</p>
                                </div>
                                <div class="col-6 text-right single_product__price--container">
                                    <p class="single_product__points">890 <span class="text-nowrap">E-LUX</span></p>
                                    <p class="single_product__price">1 999 ZŁ</p>
                                </div>
                            </div>
                            <div class="row single_product__buttons">
                                <div class="col-6">
                                    <button type="button"
                                            class="btn btn-outline-primary btn-pills single_product__more">więcej
                                    </button>
                                </div>
                                <div class="col-6">
                                    <button type="button" class="btn btn-primary btn-pills single_product__order">
                                        zamów
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

