<div class="container-fluid awards_main_list page_content">
    <div class="row awards_main_list__row">
        <div class="col-12">
            <p class="awards_main_title">Akcesoria</p>
        </div>
        <div class="product_listing__single_product">
            <div class="row single_product__img_row">
                <div class="col-12 single_product_info--wrapper">
                    <img class="single_product__img img-fluid extra_info"
                         src="{BASE_URL}_images/awards_list/iPhone.jpg">
                </div>
            </div>
            <div class="row single_product__price_row">
                <div class="col-12">
                    <p class="single_product__name">iPhone Gold 64 GB</p>
                    <p class="single_product__points">890 <span class="text-nowrap">E-LUX</span></p>
                </div>
            </div>
            <div class="row single_product__buttons">
                <div class="col-6">
                    <button type="button"
                            class="btn btn-outline-primary btn-pills single_product__more">więcej
                    </button>
                </div>
                <div class="col-6">
                    <button type="button" class="btn btn-primary btn-pills single_product__order">
                        zamów
                    </button>
                </div>
            </div>
        </div>
        <div class="product_listing__single_product">
            <div class="row single_product__img_row">
                <div class="col-12 single_product_info--wrapper">
                    <img class="single_product__img img-fluid extra_info"
                         src="{BASE_URL}_images/awards_list/iPhone.jpg">
                </div>
            </div>
            <div class="row single_product__price_row">
                <div class="col-6">
                    <p class="single_product__name">iPhone Gold 64 GB</p>
                </div>
                <div class="col-6 text-right single_product__price--container">
                    <p class="single_product__points">890 <span class="text-nowrap">E-LUX</span></p>
                </div>
            </div>
            <div class="row single_product__buttons">
                <div class="col-6">
                    <button type="button"
                            class="btn btn-outline-primary btn-pills single_product__more">więcej
                    </button>
                </div>
                <div class="col-6">
                    <button type="button" class="btn btn-primary btn-pills single_product__order">
                        zamów
                    </button>
                </div>
            </div>
        </div>
        <div class="product_listing__single_product">
            <div class="row single_product__img_row">
                <div class="col-12 single_product_info--wrapper">
                    <img class="single_product__img img-fluid extra_info"
                         src="{BASE_URL}_images/awards_list/Zegarek.jpg">
                </div>
            </div>
            <div class="row single_product__price_row">
                <div class="col-6">
                    <p class="single_product__name">Ekskluzywny zegarek</p>
                </div>
                <div class="col-6 text-right single_product__price--container">
                    <p class="single_product__points">890 <span class="text-nowrap">E-LUX</span></p>
                </div>
            </div>
            <div class="row single_product__buttons">
                <div class="col-6">
                    <button type="button"
                            class="btn btn-outline-primary btn-pills single_product__more">więcej
                    </button>
                </div>
                <div class="col-6">
                    <button type="button" class="btn btn-primary btn-pills single_product__order">
                        zamów
                    </button>
                </div>
            </div>
        </div>
        <div class="product_listing__single_product">
            <div class="row single_product__img_row">
                <div class="col-12 single_product_info--wrapper">
                    <img class="single_product__img img-fluid extra_info"
                         src="{BASE_URL}_images/awards_list/Torby.jpg">
                </div>
            </div>
            <div class="row single_product__price_row">
                <div class="col-6">
                    <p class="single_product__name">Zestaw podróżny Porsche Design</p>
                </div>
                <div class="col-6 text-right single_product__price--container">
                    <p class="single_product__points">890 <span class="text-nowrap">E-LUX</span></p>
                </div>
            </div>
            <div class="row single_product__buttons">
                <div class="col-6">
                    <button type="button"
                            class="btn btn-outline-primary btn-pills single_product__more">więcej
                    </button>
                </div>
                <div class="col-6">
                    <button type="button" class="btn btn-primary btn-pills single_product__order">
                        zamów
                    </button>
                </div>
            </div>
        </div>
        <div class="product_listing__single_product">
            <div class="row single_product__img_row">
                <div class="col-12 single_product_info--wrapper">
                    <img class="single_product__img img-fluid extra_info"
                         src="{BASE_URL}_images/awards_list/OffWhite.jpg">
                </div>
            </div>
            <div class="row single_product__price_row">
                <div class="col-6">
                    <p class="single_product__name">Buty Off-White</p>
                </div>
                <div class="col-6 text-right single_product__price--container">
                    <p class="single_product__points">890 <span class="text-nowrap">E-LUX</span></p>
                </div>
            </div>
            <div class="row single_product__buttons">
                <div class="col-6">
                    <button type="button"
                            class="btn btn-outline-primary btn-pills single_product__more">więcej
                    </button>
                </div>
                <div class="col-6">
                    <button type="button" class="btn btn-primary btn-pills single_product__order">
                        zamów
                    </button>
                </div>
            </div>
        </div>
        <div class="product_listing__single_product">
            <div class="row single_product__img_row">
                <div class="col-12 single_product_info--wrapper">
                    <img class="single_product__img img-fluid extra_info"
                         src="{BASE_URL}_images/awards_list/Zegarek.jpg">
                </div>
            </div>
            <div class="row single_product__price_row">
                <div class="col-6">
                    <p class="single_product__name">Ekskluzywny zegarek</p>
                </div>
                <div class="col-6 text-right single_product__price--container">
                    <p class="single_product__points">890 <span class="text-nowrap">E-LUX</span></p>
                </div>
            </div>
            <div class="row single_product__buttons">
                <div class="col-6">
                    <button type="button"
                            class="btn btn-outline-primary btn-pills single_product__more">więcej
                    </button>
                </div>
                <div class="col-6">
                    <button type="button" class="btn btn-primary btn-pills single_product__order">
                        zamów
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="row awards_main_list__row">
        <div class="col-12">
            <p class="awards_main_title">Turystyka</p>
        </div>
        <div class="product_listing__single_product">
            <div class="row single_product__img_row">
                <div class="col-12 single_product_info--wrapper">
                    <img class="single_product__img img-fluid extra_info"
                         src="{BASE_URL}_images/awards_list/surf.jpg">
                </div>
            </div>
            <div class="row single_product__price_row">
                <div class="col-6">
                    <p class="single_product__name">iPhone Gold 64 GB</p>
                </div>
                <div class="col-6 text-right single_product__price--container">
                    <p class="single_product__points">890 <span class="text-nowrap">E-LUX</span></p>
                </div>
            </div>
            <div class="row single_product__buttons">
                <div class="col-6">
                    <button type="button"
                            class="btn btn-outline-primary btn-pills single_product__more">więcej
                    </button>
                </div>
                <div class="col-6">
                    <button type="button" class="btn btn-primary btn-pills single_product__order">
                        zamów
                    </button>
                </div>
            </div>
        </div>
        <div class="product_listing__single_product">
            <div class="row single_product__img_row">
                <div class="col-12 single_product_info--wrapper">
                    <img class="single_product__img img-fluid extra_info"
                         src="{BASE_URL}_images/awards_list/OffWhite.jpg">
                </div>
            </div>
            <div class="row single_product__price_row">
                <div class="col-6">
                    <p class="single_product__name">Ekskluzywny zegarek</p>
                </div>
                <div class="col-6 text-right single_product__price--container">
                    <p class="single_product__points">890 <span class="text-nowrap">E-LUX</span></p>
                </div>
            </div>
            <div class="row single_product__buttons">
                <div class="col-6">
                    <button type="button"
                            class="btn btn-outline-primary btn-pills single_product__more">więcej
                    </button>
                </div>
                <div class="col-6">
                    <button type="button" class="btn btn-primary btn-pills single_product__order">
                        zamów
                    </button>
                </div>
            </div>
        </div>
        <div class="product_listing__single_product">
            <div class="row single_product__img_row">
                <div class="col-12 single_product_info--wrapper">
                    <img class="single_product__img img-fluid extra_info"
                         src="{BASE_URL}_images/awards_list/OffWhite.jpg">
                </div>
            </div>
            <div class="row single_product__price_row">
                <div class="col-6">
                    <p class="single_product__name">Ekskluzywny zegarek</p>
                </div>
                <div class="col-6 text-right single_product__price--container">
                    <p class="single_product__points">890 <span class="text-nowrap">E-LUX</span></p>
                </div>
            </div>
            <div class="row single_product__buttons">
                <div class="col-6">
                    <button type="button"
                            class="btn btn-outline-primary btn-pills single_product__more">więcej
                    </button>
                </div>
                <div class="col-6">
                    <button type="button" class="btn btn-primary btn-pills single_product__order">
                        zamów
                    </button>
                </div>
            </div>
        </div>
        <div class="product_listing__single_product">
            <div class="row single_product__img_row">
                <div class="col-12 single_product_info--wrapper">
                    <img class="single_product__img img-fluid extra_info"
                         src="{BASE_URL}_images/awards_list/Fjord.jpg">
                </div>
            </div>
            <div class="row single_product__price_row">
                <div class="col-6">
                    <p class="single_product__name">Zestaw podróżny Porsche Design</p>
                </div>
                <div class="col-6 text-right single_product__price--container">
                    <p class="single_product__points">890 <span class="text-nowrap">E-LUX</span></p>
                </div>
            </div>
            <div class="row single_product__buttons">
                <div class="col-6">
                    <button type="button"
                            class="btn btn-outline-primary btn-pills single_product__more">więcej
                    </button>
                </div>
                <div class="col-6">
                    <button type="button" class="btn btn-primary btn-pills single_product__order">
                        zamów
                    </button>
                </div>
            </div>
        </div>
        <div class="product_listing__single_product">
            <div class="row single_product__img_row">
                <div class="col-12 single_product_info--wrapper">
                    <img class="single_product__img img-fluid extra_info"
                         src="{BASE_URL}_images/awards_list/surf.jpg">
                </div>
            </div>
            <div class="row single_product__price_row">
                <div class="col-6">
                    <p class="single_product__name">Buty Off-White</p>
                </div>
                <div class="col-6 text-right single_product__price--container">
                    <p class="single_product__points">890 <span class="text-nowrap">E-LUX</span></p>
                </div>
            </div>
            <div class="row single_product__buttons">
                <div class="col-6">
                    <button type="button"
                            class="btn btn-outline-primary btn-pills single_product__more">więcej
                    </button>
                </div>
                <div class="col-6">
                    <button type="button" class="btn btn-primary btn-pills single_product__order">
                        zamów
                    </button>
                </div>
            </div>
        </div>
        <div class="product_listing__single_product">
            <div class="row single_product__img_row">
                <div class="col-12 single_product_info--wrapper">
                    <img class="single_product__img img-fluid extra_info"
                         src="{BASE_URL}_images/awards_list/sweden.jpg">
                </div>
            </div>
            <div class="row single_product__price_row">
                <div class="col-6">
                    <p class="single_product__name">Ekskluzywny zegarek</p>
                </div>
                <div class="col-6 text-right single_product__price--container">
                    <p class="single_product__points">890 <span class="text-nowrap">E-LUX</span></p>
                </div>
            </div>
            <div class="row single_product__buttons">
                <div class="col-6">
                    <button type="button"
                            class="btn btn-outline-primary btn-pills single_product__more">więcej
                    </button>
                </div>
                <div class="col-6">
                    <button type="button" class="btn btn-primary btn-pills single_product__order">
                        zamów
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>