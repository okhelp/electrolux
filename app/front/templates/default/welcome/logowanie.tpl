<div class="user_login">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-auto">
                    <div class="user_login_intro">
                        {if isset($smarty.session.user_services)}
                            {if $smarty.session.user_services[$smarty.session.id_service]['name'] == aeg}
                                <img class="img-fluid user_login_logo" src="{BASE_URL}data/front/_assets/images/logo_aeg.svg">
                            {else}
                                <img class="img-fluid user_login_logo" src="{BASE_URL}data/front/_assets/images/logo.svg">
                            {/if}
                        {else}
                            <img class="img-fluid user_login_logo" src="{BASE_URL}data/front/_assets/images/logo.svg">
                        {/if}
                        <p class="user_login_intro--text">PROGRAM PARTNERSKI FIRMY ELECTROLUX <br>
                            DLA STUDIÓW KUCHENNYCH</p>
                    </div>
                <form action="{BASE_URL}auth/login.html{if !empty($ref)}?ref={$ref}{/if}">
                    <div id="user_login_container" class="user_login_container show">
                    <p class="user_login_title"><i class="login_icon icon-eok_login"></i> Logowanie</p>
                        <p class="login_fail">Login/hasło nieprawidłowe</p>
                        <div class="login_form__input">
                            <input class="login_form__input--input" type="text" name="name" required="required">
                            <span class="login_form__input--placeHolder" data-placeholder="nazwa użytkownika"></span>
                        </div>
                        <div class="login_form__input">
                            <input class="login_form__input--input" type="password" name="password" required="required">
                            <span class="login_form__input--placeHolder" data-placeholder="hasło"></span>
                        </div>
                        <p class="login_form__remind">* nie pamiętasz hasła, <span class="login_form__remember_link">kliknij tutaj</span></p>
                        <button id="login_button" type="submit" formmethod="post" class="btn login_button">zaloguj się</button>
                    </div>
                </form>
                <form action="{BASE_URL}auth/remind_password.html">
                    <div id="user_remind" class="user_login_container">
                        <p class="user_login_title"><i class="login_icon icon-eok_login"></i> Przypomnienie hasła</p>
                        <div class="login_form__input login_form__input_remind">
                            <input class="login_form__input--input" type="text" name="login" required="required">
                            <span class="login_form__input--placeHolder" data-placeholder="Podaj login(NIP)"></span>
                        </div>
                        <div class="login_form__input login_form__input_remind">
                            <input class="login_form__input--input" type="email" name="email" required="required">
                            <span class="login_form__input--placeHolder" data-placeholder="Podaj e-mail"></span>
                        </div>
                        <p class="login_remind__success">Na podany e-mail wysłaliśmy hasło/link?</p>
                        <p class="login_form__remind"><span class="login_form__remember_link">powrót do logowania</span></p>
                        <button type="submit" formmethod="post" class="btn login_button">wyślij</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready( function () {
        $('.login_form__input--input').each(function () {
            $(this).on('blur', function () {
                if ($(this).val().trim() !== "") {
                    $(this).addClass('has-val');
                } else {
                    $(this).removeClass('has-val');
                }
            })
        })
    });
    $('.login_form__remember_link').on('click', function () {
        $('.user_login_container').toggleClass('show');
        if ($('.login_remind__success:visible').length !== 0) {
            $('.login_remind__success').hide();
            $('.login_form__input_remind').show();
        }
    });
    function remindSuccess() {
        $('.login_form__input_remind').hide();
        $('.login_remind__success').fadeIn();
    }
    function loginFail() {
        $('.login_fail').fadeIn();
    }
    $('#login_button').on('click', function () {
        $('.login_fail').fadeOut();
        var d = new Date();
        _paq.push(['trackEvent', 'Logowanie', 'Godzina logowania', d.getHours() + ':' + (d.getMinutes() < 10 ? '0' : '') + d.getMinutes()]);
    })
</script>
