<div class="container-fluid product_more award_more">
    <div class="row">
        <div class="col-6 d-flex align-items-center product_more__text">
            <div class="row">
                <div class="col-12">
                    <div class="row">
                        <div class="col-12">
                            <p class="product_more__title">Poznaj Północną Szwecję</p>
                            <p class="product_more__code">Sweden, Ange</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <p class="product_more__description">Lorem ipsum dolor sit amet, consectetur adipiscing
                                elit.
                                Mauris congue felis sed justo tempus, eu iaculis diam
                                gravida. Duis rhoncus sollicitudin ante sit amet congue.
                                Interdum et malesuada fames ac ante ipsum primis
                                in faucibus. Aenean vehicula justo vitae malesuada.
                                Pellentesque elit ipsum, malesuada eu leo blandit.
                                Lorem ipsum dolor sit amet.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-6 align-items-center justify-content-center product_more__photo">
            <img class="" src="{BASE_URL}_images/award_photo.jpg">
        </div>
        <div id="tab-1" class="col-6 product_more__specification product_more__specification--lightBlue award_more__order">
            <ul class="award_more__order_list">
                <li class="award_more__order_point mb-5">
                    <div class="container">
                        <div class="row mb-3">
                            <div class="col-8">
                                <p class="award_more__order_point_title">Opcja 1</p>
                            </div>
                            <div class="col-4 text-right">
                                <p class="award_more__order_point_points">890 <span class="text-nowrap">E-LUX</span></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-8">
                                <p class="award_more__order_point_details">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris congue
                                    felis sed justo tempus, eu iaculis diam gravida. Duis rhoncus sollicitudin
                                    ante sit amet congue.
                                    Interdum et malesuada fames ac ante ipsum primis.
                                </p>
                            </div>
                            <div class="col-4 award_more__order_buttons d-flex flex-wrap align-items-center justify-content-end justify-content-end">
                                <button type="button"
                                        class="btn btn-outline-light btn-pills award_more__order_more">więcej
                                </button>
                                <button type="button" class="btn btn-primary btn-pills award_more__order_orderLink">
                                    zamów
                                </button>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="award_more__order_point mb-5">
                    <div class="container">
                        <div class="row mb-3">
                            <div class="col-8">
                                <p class="award_more__order_point_title">Opcja 2</p>
                            </div>
                            <div class="col-4 text-right">
                                <p class="award_more__order_point_points">890 <span class="text-nowrap">E-LUX</span></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-8">
                                <p class="award_more__order_point_details">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris congue
                                    felis sed justo tempus, eu iaculis diam gravida. Duis rhoncus sollicitudin
                                    ante sit amet congue.
                                    Interdum et malesuada fames ac ante ipsum primis.
                                </p>
                            </div>
                            <div class="col-4 award_more__order_buttons d-flex flex-wrap align-items-center justify-content-end">
                                <button type="button"
                                        class="btn btn-outline-light btn-pills award_more__order_more">więcej
                                </button>
                                <button type="button" class="btn btn-primary btn-pills award_more__order_orderLink">
                                    zamów
                                </button>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="award_more__order_point mb-5">
                    <div class="container">
                        <div class="row mb-3">
                            <div class="col-8">
                                <p class="award_more__order_point_title">Opcja 3</p>
                            </div>
                            <div class="col-4 text-right">
                                <p class="award_more__order_point_points">890 <span class="text-nowrap">E-LUX</span></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-8">
                                <p class="award_more__order_point_details">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris congue
                                    felis sed justo tempus, eu iaculis diam gravida. Duis rhoncus sollicitudin
                                    ante sit amet congue.
                                    Interdum et malesuada fames ac ante ipsum primis.
                                </p>
                            </div>
                            <div class="col-4 award_more__order_buttons d-flex flex-wrap align-items-center justify-content-end">
                                <button type="button"
                                        class="btn btn-outline-light btn-pills award_more__order_more">więcej
                                </button>
                                <button type="button" class="btn btn-primary btn-pills award_more__order_orderLink">
                                    zamów
                                </button>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
        <div id="tab-2" class="col-6 product_more__specification award_more__details">
            <p class="award_more__details--standard">Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                Mauris congue felis sed justo tempus, eu iaculis diam
                gravida. Duis rhoncus sollicitudin ante sit amet congue.
                Interdum et malesuada fames ac ante ipsum primis
                in faucibus. Aenean vehicula justo vitae malesuada.
                Pellentesque elit ipsum, malesuada eu leo blandit.
                Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet,
                consectetur adipiscing elit. Mauris congue felis sed justo
                tempus, eu iaculis diam gravida. </p>
            <ul class="award_more__details_list">
                <li class="award_more__details_point">Lorem ipsum dolor sit amet</li>
                <li class="award_more__details_point">Consectetur adipiscing elit</li>
                <li class="award_more__details_point">Mauris congue felis sed justo tempus</li>
                <li class="award_more__details_point">Eu iaculis diam gravida</li>
                <li class="award_more__details_point">Duis rhoncus sollicitudin </li>
                <li class="award_more__details_point">Ante sit amet congue</li>
                <li class="award_more__details_point">Interdum et malesuada</li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-6 product_more__extra_links product_more__extra_links--lightBlue">
            <span data-tab="tab-1" class="product_more__tab_link"><i
                        class="icon-plus product_more__extra_links--icon product_more__extra_links--iconChange"></i>Zamów</span>
        </div>
        <div class="col-6 product_more__extra_links product_more__extra_links--blue">
            <span data-tab="tab-2" class="product_more__tab_link"><i
                        class="icon-plus product_more__extra_links--icon product_more__extra_links--iconChange"></i>Szczegóły</span>
        </div>
    </div>
</div>

