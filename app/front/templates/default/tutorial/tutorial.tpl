<div class="container-fluid user_panel_container page_content tutorial">
    {include file="user_dashboard/_partials/uzytkownik_glowny_panel.tpl"}
    <div class="row">
        <div class="col-12">
            <div class="user_panel">
                <p class="user_panel__title">Tutorial</p>
            </div>
            <div class="text-center">
                {if !empty($smarty.session.is_company_admin)}
                <video controls>
                    <source src="{BASE_URL}data/front/_assets/images/video_tutorial/eok_tutorial_konto-nadrzedne.mp4" type="video/mp4">
                    <source src="{BASE_URL}data/front/_assets/images/video_tutorial/eok_tutorial_konto-nadrzedne.ogg" type="video/ogg">
                    <source src="{BASE_URL}data/front/_assets/images/video_tutorial/eok_tutorial_konto-nadrzedne.webm" type="video/webmg">
                    Twoja przeglądarka nie obsługuje video.
                </video>
                {else}
                <video controls>
                    <source src="{BASE_URL}data/front/_assets/images/video_tutorial/eok_tutorial_konto-podrzedne.mp4" type="video/mp4">
                    <source src="{BASE_URL}data/front/_assets/images/video_tutorial/eok_tutorial_konto-podrzedne.ogg" type="video/ogg">
                    <source src="{BASE_URL}data/front/_assets/images/video_tutorial/eok_tutorial_konto-podrzedne.webm" type="video/webmg">
                    Twoja przeglądarka nie obsługuje video.
                </video>
                {/if}
            </div>
        </div>
    </div>
</div>
