<div class="row">
    <!-- Column -->
    <div class="col-md-12 col-lg-4 col-xlg-3">
        <div class="card">
            <div class="card-body">
                <center class="mt-4"> <img src="{user_avatar id={$user_data->id} width=150 height=150}" style="max-width: 150px; max-height: 150px;" alt="{$user_data->name} {$user_data->surname}" class="img-circle">
                    <h4 class="card-title mt-2">{$user_data->name} {$user_data->surname}</h4>
                    <h6 class="card-subtitle">{$user_data->get_position_name()}</h6>
                    <div class="row text-center justify-content-md-center">
                        <div class="col-md-12">
                            <a href="{BASE_URL}wiadomosci/napisz.html?to={$user_data->id}" class="btn btn-success">napisz wiadomość</a>
                        </div>
                    </div>
                </center>
            </div>
            <div>
                <hr> </div>
            <div class="card-body"> <small class="text-muted">E-mail</small>
                <h6>{$user_data->email}</h6>
                {if !empty($user_data->telephone)}
                    <small class="text-muted p-t-30 db">Telefon</small>
                    <h6>{$user_data->telephone}</h6>
                {/if}

            </div>
        </div>
    </div>
    <!-- Column -->
    <!-- Column -->
    <div class="col-md-12 col-lg-8 col-xlg-9">
        <div class="card">

            <!-- Nav tabs -->
            <ul class="nav nav-tabs profile-tab" role="tablist">
                <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#home" role="tab">Powiązane firmy</a> </li>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
                <div class="tab-pane active" id="home" role="tabpanel">
                    <div class="card-body">
                        <div class="alert alert-info">@TODO</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Column -->
</div>
