<div class="container-fluid change__password settings">
    {include file="_partials/system_message.tpl"}
    <div class="row">
        <div class="col-lg-4"></div>
        <div class="col-lg-8 col-12">
            <div class="settings_description pb-0">
                <p class="settings_description__text settings_description__text--big">Wymagana zmiana hasła</p>
            </div>
            </div>
    </div>
    <div class="row">
        <div class="col-lg-4 col-12">
            <div class="change__password_user">
                <img src="{user_avatar id={$smarty.session.id} width=96 height=96}"
                     alt="{$smarty.session.name} {$smarty.session.surname}" width="50" class="img-fluid rounded-circle" style="max-height: 128px; max-width: 128px;">
                <p class="change__password_user_name">{$smarty.session.name} {$smarty.session.surname}</p>
            </div>
        </div>
        <div class="col-lg-8 col-12">
            <div class="main_tab">
                <div class="settings_description">
                    <p class="settings_description__text">
                        Swoje hasło musisz zmienić ze względów bezpieczeństwa. Pamiętaj, że w panelu użytkownika portalu electroluxodkuchni.pl nie ma
                        możliwości uzyskania aktualnego hasła dostępu.
                    </p>
                    <form type="post" id="change_password_form" action="{BASE_URL}auth/change_password.html">
                        <div class="settings_single">
                            <div class="d-flex w-100">
                                <div class="custom_form__input">
                                    <input class="custom_form__input--input {*{if !empty($user->name)}has-val{/if}*}"
                                           type="password" id="user_new_password"
                                           name="password[]" value="{*{if !empty($user)}{$user->name}{/if}*}"
                                           required>
                                    <span class="custom_form__input--placeHolder"
                                          data-placeholder="Nowe hasło"></span>
                                </div>
                            </div>
                            <div class="d-flex w-100 align-items-center flex-column flex-md-row">
                                <div class="custom_form__input">
                                    <input class="custom_form__input--input {*{if !empty($user->name)}has-val{/if}*}"
                                           type="password" id="user_new_password_confirm"
                                           name="password[]" value="{*{if !empty($user)}{$user->name}{/if}*}"
                                           required>
                                    <span class="custom_form__input--placeHolder"
                                          data-placeholder="Powtórz nowe hasło"></span>
                                </div>
                                <p id="wrong_confirm_password" class="ml-0 ml-md-3 text-danger" style="display: none;">
                                    Hasła nie są identyczne.</p>
                            </div>
                            <div class="d-flex flex-column custom_form_checkbox">
                                <label class="form__label--1" for="is_main">
                                                     <span class="text-info-form mr-2">
                                                        <p>Jestem pracownikiem firmy zapraszającej</p>
                                                    </span>
                                    <input type="radio"
                                           class="check"
                                           value="1"
                                           name="type_worker" checked>
                                    <label class="custom__label ml-auto" for="">
                                        <svg viewBox="0,0,50,50">
                                            <path d="M5 30 L 20 45 L 45 5"></path>
                                        </svg>
                                    </label>
                                </label>
                                <label class="form__label--1" for="is_main">
                                                     <span class="text-info-form mr-2">
                                                        <p>Jestem niezależnym kontrahentem</p>
                                                    </span>
                                    <input type="radio"
                                           class="check"
                                           value="2"
                                           name="type_worker">
                                    <label class="custom__label ml-auto" for="">
                                        <svg viewBox="0,0,50,50">
                                            <path d="M5 30 L 20 45 L 45 5"></path>
                                        </svg>
                                    </label>
                                </label>
                                <label class="form__label--1" for="is_main">
                                                     <span class="text-info-form mr-2">
                                                        <p>Jestem pracownikiem zewnętrznej firmy</p>
                                                    </span>
                                    <input type="radio"
                                           class="check"
                                           value="3"
                                           name="type_worker">
                                    <label class="custom__label ml-auto" for="">
                                        <svg viewBox="0,0,50,50">
                                            <path d="M5 30 L 20 45 L 45 5"></path>
                                        </svg>
                                    </label>
                                </label>
                            </div>
                            <div class="d-none w-100 align-items-center flex-column flex-md-row">
                                <div class="custom_form__input">
                                    <input class="custom_form__input--input" type="text" id="user_new_company_name" name="company_name" value="" required disabled>
                                    <span class="custom_form__input--placeHolder" data-placeholder="Nazwa firmy"></span>
                                </div>
                            </div>
                            <div class="text-right mt-4 w-100">
                                <button id="change_password" class="btn btn-pills btn-primary" formmethod="post" name="save" value="1">Zaloguj się
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var $newPassword = $('#user_new_password');
    var $confirmPassword = $('#user_new_password_confirm');
    var $wrongConfirmPassword = $('#wrong_confirm_password');
    var $typeWorker = $('input[type="radio"]');
    var $companyName = $('#user_new_company_name');
    var $changePassword = $('#change_password');
    /*--porównywanie haseł przy wpisywaniu--*/
    function compareNewPassword() {
        if ($newPassword.val() === $confirmPassword.val()) {
            $wrongConfirmPassword.fadeOut();
            $changePassword.prop('disabled', false);
        } else {
            $wrongConfirmPassword.fadeIn();
            $changePassword.prop('disabled', true);
        }
    }
    function changeWorkerType() {
      if (parseInt($(this).val()) === 1) {
        $companyName.closest('.custom_form__input').parent().removeClass('d-flex').addClass('d-none');
        $companyName.prop('disabled', true);
      } else {
        $companyName.closest('.custom_form__input').parent().removeClass('d-none').addClass('d-flex');
        $companyName.prop('disabled', false);
      }
    }
    $newPassword.on('change', function () {
        if($confirmPassword.val()) {
            compareNewPassword();
        }
    });
    $confirmPassword.on('change', compareNewPassword);
    $typeWorker.on('change', changeWorkerType);
    $(document).ready(function () {
      checkInputValue();
    });
</script>
