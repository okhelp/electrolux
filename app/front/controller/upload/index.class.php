<?php

class App__Front__Controller__Upload__Index extends Lib__Base__Controller
{	
	public $common_file = '';
	
	public function action_ajax_upload_file() 
	{
		$upload = new App__Ed__Model__Upload($_POST['type'], $_POST['max_file_size']);
		$upload->send_file($_FILES['upload_file']);
	}
	
	public function action_ajax_remove_file()
	{
		if(!empty($_POST))
		{
			$is_img = (int)$_POST['i'];
			$file_name = $_POST['file_name'];
			
			//wersja dla obrazków, usuwam id_cimg i zarazem wszystkie pozostałe elementy
			if($is_img) 
			{
				App__Ed__Model__Img::remove_cimg($file_name);
			}
			else //wersja dla plików - usuwam plik z dysku
			{
				unlink(BASE_PATH.App__Ed__Model__Encryption::decode($file_name));
			}
			
		}
	}
}