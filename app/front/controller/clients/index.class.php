<?php

use App\front\models\company\Clients_import;

class App__Front__Controller__Clients__Index extends Lib__Base__Controller
{
    public function action_import()
    {
        try
        {
            $products_importer = new Clients_import();
            $products_importer->run();

            echo '<h1>Import został wykonany</h1>';
        }
        catch (Exception $exception)
        {
            echo '<pre>';
            print_r($exception->getMessage());
            echo '</pre>';
        }
    }
}