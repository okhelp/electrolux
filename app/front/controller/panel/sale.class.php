<?php

use App\ed\model\mail\Add_to_queue;
use App\ed\model\mail\Send_email_by_queue;
use App\ed\model\newsletter\Users_options;
use App\ed\model\sale\Register_sale;
use App\ed\model\users\Can_send_email;
use App\modules\products\front\collections\Get_products_assigned_to_category_collection;
use App\modules\products\front\collections\Get_products_subcategories_collection;

class App__Front__Controller__Panel__Sale extends Lib__Base__Controller
{
    public function __construct()
    {
        parent::__construct();

        if (!App__Ed__Model__Acl::has_access('user_panel', 1))
        {
            go_back('w|Brak dostępu.', BASE_URL); die();
        }
    }

    public function action_register_sale()
    {
        $message = 'd|Brak dostępu';

        if (!empty($_POST['brand']) && !empty($_POST['product']) && !empty($_POST['count']) && App__Ed__Model__Acl::has_access('register_sale', 0) && !empty($_POST['invoice_number']))
        {
            $register_sale = new Register_sale((int)$_POST['product'], (int)$_POST['count'], $this->_user->id, $_POST['invoice_number']); 

            $registered = $register_sale->register();

            if($registered && Can_send_email::verify((int)$this->_user->id, Users_options::OPTION_SEND_NEW_SALE_NOTIFICATION))
            {
                $user = App__Ed__Model__Users::find($this->_user->id);
                
                Send_email_by_queue::send(
                    (string)$user->email,
                    'Rejestracja sprzedaży - Electrolux od kuchni',
                    'Rejestracja sprzedaży',
                    "Twoja sprzedaż została zarejestrowana. <br>
Obecnie jest ona weryfikowana. Gdy zostanie zaakceptowana, zostaniesz poinformowany o ilości przyznanych e-luxów.<br>
W razie jakikolwiek wątpliwości zapraszamy do kontaktu z swoim handlowcem."
                );
            }

            $message = $registered ? 'g|Zarejestrowano sprzedaż.' : 'd|Nie zarejestrowano sprzedaży.';
        }

        go_back($message);
    }

    public function action_get_subcategories()
    {
        if(!empty($_POST['brand']) && !empty($_POST['category']))
        {
            $subcategories = Get_products_subcategories_collection::get($_POST['category'], $_SESSION['id_lang'], false);

            $smarty = new Smarty();
            $smarty->assign('options', $subcategories);

            echo $smarty->fetch('user_dashboard/sale/_partials/subcategories_select_options.tpl');
        }
    }

    public function action_get_products()
    {
        if(!empty($_POST['brand']) && !empty($_POST['category']) && !empty($_POST['subcategory']))
        {
            $categories = Get_products_subcategories_collection::get($_POST['subcategory'], $_SESSION['id_lang'], true);

            $categories_ids = [];
            if(!empty($categories))
            {
                $categories_ids = array_map(function($category){ return $category->electrolux_id; }, $categories);
            }

            $products = Get_products_assigned_to_category_collection::get($categories_ids, $_SESSION['id_lang'], 99999999, '', $_POST['brand']);

            $smarty = new Smarty();
            $smarty->assign('options', $products);

            echo $smarty->fetch('user_dashboard/sale/_partials/products_select_options.tpl');
        }
    }
}