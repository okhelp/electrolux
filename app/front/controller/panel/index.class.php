<?php

use App\ed\model\company\Get_by_user_id;
use App\ed\model\company\Get_company_adresses;
use App\ed\model\company\Get_company_supervisor;
use App\ed\model\company\Get_company_users;
use App\ed\model\Company_users_points;
use App\ed\model\faq\Faq_category_model;
use App\ed\model\faq\Get_faq_list;
use App\ed\model\mail\Add_to_queue;
use App\ed\model\mail\Send_email_by_queue;
use App\ed\model\messages\Get_new_messages_count;
use App\ed\model\newsletter\Newsletter_subscription;
use App\ed\model\newsletter\Users_options;
use App\ed\model\points\Get_users_points;
use App\ed\model\sale\Get_sale_history;
use App\ed\model\Sale_model;
use App\ed\model\users\Can_send_email;
use App\ed\model\users\Confirmed_co_workers;
use App\ed\model\users\Get_user_options;
use App\ed\model\users\Register_new_co_worker;
use App\front\models\points\Transfer_points;
use App\modules\order\front\collections\Get_user_prizes_earned;
use App\modules\products\front\collections\Get_products_by_id;
use App\modules\products\front\collections\Get_products_subcategories_collection;

class App__Front__Controller__Panel__Index extends Lib__Base__Controller
{
    public function __construct()
    {
        parent::__construct();

        if (!empty(Lib__Session::get('id')) && !App__Ed__Model__Acl::has_access('user_panel', 1))
        {
            go_back('w|Brak dostępu.', BASE_URL); die();
        }
    }

    public function action_index()
    {
        if(empty((int)Lib__Session::get('id')))
        {
            go_back('w|Brak dostępu.', BASE_URL); die();
        }

        if(App__Ed__Model__Acl::has_group('trader'))
        {
            $this->dashboard_to_trader();
        }
        else
        {
            $this->dashboard_to_customer();
        }

        $this->page_title = "Moje konto";
        $this->breadcrumb = [
            BASE_URL . 'panel' => 'Moje konto',
        ];
    }

    private function dashboard_to_trader()
    {
        $company = App__Ed__Model__Company::find(956);
        $adresses = Get_company_adresses::get($company->id);
        $user_data = App__Ed__Model__Users::find((int)$this->session['id']);

        $this->view->assign('company', $company);
        $this->view->assign('addresses', $adresses);
        $this->view->assign('user_data', $user_data);

        $this->template = 'user_dashboard/uzytkownik_panel_handlowiec.tpl';
    }

    private function dashboard_to_customer()
    {
        $company = Get_by_user_id::get((int)Lib__Session::get('id'));
        $adresses = Get_company_adresses::get($company->id);
        $company_users = Get_company_users::get($company->id);
        $user_data = App__Ed__Model__Users::find((int)$this->session['id']);

        foreach ($adresses as $index => $adress)
        {
            if(empty($adress->public) && $adress->id_user != Lib__Session::get('id'))
            {
                unset($adresses[$index]);
            }
        }

        $company_users_ids = [];
        if (!empty($company_users))
        {
            $company_users_ids = array_map(
                function($company_user) {
                    return $company_user->id_user;
                }, $company_users);
        }

        $users_e_lux = [];
        foreach (array_merge($company_users_ids, [(int)$this->session['id']]) as $userID) {
            $users_e_lux[$userID] = App__Ed__Model__Points_log::getPoints($userID);
        }
        $annual_eluxes = Get_users_points::get_annual([(int)$this->session['id']]);
        $annual_eluxes = !empty($annual_eluxes) ? reset($annual_eluxes) : 0;

        $this->view->assign('company', $company);
        $this->view->assign('addresses', $adresses);
        $this->view->assign('company_users', $company_users);
        $this->view->assign('users_e_lux', $users_e_lux);
        $this->view->assign('annual_eluxes', $annual_eluxes);
        $this->view->assign('user_data', $user_data);
        $this->view->assign('users_extra_e_lux', App__Ed__Model__Points_extra_log::getPoints((int)$this->session['id']));
        $this->view->assign('user_spend_e_lux', App__Ed__Model__Points_log::getSpentPoints((int)$this->session['id']));

        $this->template = 'user_dashboard/uzytkownik_panel.tpl';
    }

    public function action_historia_zamowien()
    {
        if(!App__Ed__Model__Acl::has_access('register_sale', 0))
        {
            go_back('w|Brak dostępu.'); die();
        }

        $company = Get_by_user_id::get((int)$this->session['id']);
        $company_users = Get_company_users::get($company->id);
        $user_data = App__Ed__Model__Users::find((int)$this->session['id']);

        $company_users_ids = [];
        if (!empty($company_users))
        {
            $company_users_ids = array_map(
                function($company_user) {
                    return $company_user->id_user;
                }, $company_users);
        }

        $users_e_lux = [];
        foreach (array_merge($company_users_ids, [(int)$this->session['id']]) as $userID) {
            $users_e_lux[$userID] = App__Ed__Model__Points_log::getPoints($userID);
        }
        $annual_eluxes = Get_users_points::get_annual([(int)$this->session['id']]);
        $annual_eluxes = !empty($annual_eluxes) ? reset($annual_eluxes) : 0;

        $this->view->assign('company', $company);
        $this->view->assign('users_e_lux', $users_e_lux);
        $this->view->assign('annual_eluxes', $annual_eluxes);
        $this->view->assign('user_data', $user_data);
        $this->view->assign('users_extra_e_lux', App__Ed__Model__Points_extra_log::getPoints((int)$this->session['id']));
        $this->view->assign('user_spend_e_lux', App__Ed__Model__Points_log::getSpentPoints((int)$this->session['id']));
//        $this->view->assign('products', $products);

        $this->template = 'user_dashboard/uzytkownik_panel_historia_zamowien.tpl';
        $this->page_title = "Historia zamówień";

        $this->breadcrumb = [
            BASE_URL . 'panel'                        => 'Moje konto',
            BASE_URL . 'panel/historia_zamowien.html' => 'Historia zamówień',
        ];
    }

    public function action_get_sale_history()
    {
        $table = 'sale_history';
        $primaryKey = 'id';
        $columns = [
            ['db' => 'id', 'dt' => 0],
            [
                'db'        => 'created_at',
                'dt'        => 1,
                'formatter' => function($d, $row) {
                    return date('Y-m-d H:i:s', strtotime($d));
                },
            ],
            ['db' => 'name', 'dt' => 2],
            ['db' => 'model', 'dt' => 3],
            ['db' => 'price', 'dt' => 4],
            ['db' => 'count', 'dt' => 5],
            ['db' => 'price_sum', 'dt' => 6],
            ['db' => 'points', 'dt' => 7],
        ];
        if (!empty($_POST['user_id'])) {
            echo json_encode(
                SSP::complex(
                    $_POST,
                    $table,
                    $primaryKey,
                    $columns,
                    null,
                    null,
                    " id_customer={$_POST['user_id']} AND id_service={$this->session['id_service']} AND id_lang={$this->session['id_lang']}")
            );
        } else {
            echo json_encode([]);
        }
    }

    public function action_zarejestruj_sprzedaz()
    {
        if(!App__Ed__Model__Acl::has_access('register_sale', 0) && !App__Ed__Model__Acl::has_group('trader'))
        {
            go_back('w|Brak dostępu.'); die();
        }

        $main_categories = Get_products_subcategories_collection::get('F27CFFF0-5B89-441B-BCF7-7F17D1DB81ED', $_SESSION['id_lang'], false);
        $user_brands = App__Ed__Model__Users__Service::get_user_services((int)$this->session['id']);
        $user_data = App__Ed__Model__Users::find((int)$this->session['id']);
        
        if (!App__Ed__Model__Acl::has_group('trader')) //handlowcy mają dostęp do panelu, ale ograniczony widok
        {
            $order_history = Get_sale_history::get([(int)$this->session['id']]);

            $products = [];

            if (!empty($order_history))
            {
                $products_ids = array_map(
                    function($order) {
                        return $order->{Sale_model::COLUMN_ID_PRODUCT};
                    }, $order_history);

                $products = Get_products_by_id::get($products_ids);
            }



            $company = Get_by_user_id::get((int)$this->session['id']);
            $company_users = Get_company_users::get($company->id);

            $company_users_ids = [];
            if (!empty($company_users))
            {
                $company_users_ids = array_map(
                    function($company_user) {
                        return $company_user->id_user;
                    }, $company_users);
            }

            $users_e_lux = [];
            foreach (array_merge($company_users_ids, [(int)$this->session['id']]) as $userID) {
                $users_e_lux[$userID] = App__Ed__Model__Points_log::getPoints($userID);
            }
            $annual_eluxes = Get_users_points::get_annual([(int)$this->session['id']]);
            $annual_eluxes = !empty($annual_eluxes) ? reset($annual_eluxes) : 0;

            $this->view->assign('company', $company);
            $this->view->assign('users_e_lux', $users_e_lux);
            $this->view->assign('annual_eluxes', $annual_eluxes);
            $this->view->assign('users_extra_e_lux', App__Ed__Model__Points_extra_log::getPoints((int)$this->session['id']));
            $this->view->assign('user_spend_e_lux', App__Ed__Model__Points_log::getSpentPoints((int)$this->session['id']));
            $this->view->assign('order_history', $order_history);
            $this->view->assign('products', $products);
        }



        $this->view->assign('user_data', $user_data);
        $this->view->assign('user_brands', $user_brands);
        $this->view->assign('main_categories', $main_categories);

        $this->template = 'user_dashboard/uzytkownik_panel_sprzedaz.tpl';
        $this->page_title = "Zarejestruj sprzedaż";

        $this->breadcrumb = [
            BASE_URL . 'panel'                           => 'Moje konto',
            BASE_URL . 'panel/zarejestruj_sprzedaz.html' => 'Zarejestruj sprzedaż',
        ];
    }

    public function action_add_new_address()
    {
        if (!empty($_POST['save']) && isset($_GET['id']))
        {
            $company = Get_by_user_id::get((int)$this->session['id']);

            $address = empty($_GET['id']) ? new App__Ed__Model__Company__Address() : App__Ed__Model__Company__Address::find($_GET['id']);
            $address->id_company = $company->id;
            $address->is_main = empty($_POST['is_main']) ? 0 : 1;
            $address->name = $_POST['name'];
            $address->street = $_POST['street'];
            $address->street_number = $_POST['street_number'];
            $address->apartment_number = $_POST['apartment_number'];
            $address->post_code = $_POST['post_code'];
            $address->city = $_POST['city'];
            $address->id_user = $_SESSION['id'];
            $address->public = (int)!empty($_POST['public']);

            if ($address->is_main)
            {
                $company_adresses = Get_company_adresses::get($company->id);

                foreach ($company_adresses as $company_adress)
                {
                    if(empty($address->public) && empty($company_adress->public) && $company_adress->id_user == $address->id_user)
                    {
                        $main_address = App__Ed__Model__Company__Address::find($company_adress->id);
                        $main_address->is_main = 0;
                        $main_address->save();
                    }
                    else if (!empty($company_adress->is_main) && !empty($company_adress->public) && !empty($address->public))
                    {
                        $main_address = App__Ed__Model__Company__Address::find($company_adress->id);
                        $main_address->is_main = 0;
                        $main_address->save();
                    }
                }
            }

            go_back($address->save() ? 'g|Zapisano dane.' : 'Nie udało się zapisać danych.', BASE_URL . 'panel');
        }
        else
        {
            $smarty = new Smarty();
            $smarty->assign('header_text', empty($_GET['id']) ? 'Dodaj adres' : 'Edytuj adres');
            $smarty->assign('id', empty($_GET['id']) ? 0 : $_GET['id']);

            if (!empty($_GET['id']))
            {
                $address = App__Ed__Model__Company__Address::find($_GET['id']);
                $smarty->assign('address', $address);
            }

            echo $smarty->fetch('user_dashboard/edycja/_partials/edycja_adresu.tpl');
        }
    }

    public function action_co_workers()
    {
        if (!empty($_POST['save']) && isset($_GET['id']))
        {
            if (!empty($_GET['id']))
            {
                $user = App__Ed__Model__Users::find($_GET['id']);
                $user->name = $_POST['name'];
                $user->surname = $_POST['surname'];
                $user->email = $_POST['email'];

                go_back($user->save() ? 'g|Zapisano dane.' : 'Nie udało się zapisać danych.', BASE_URL . 'panel');
            }
            else
            {
                try
                {
                    $company = Get_by_user_id::get((int)$this->session['id']);

                    $register_new_co_worker = new Register_new_co_worker((string)$_POST['name'], (string)$_POST['surname'], (string)$_POST['email'], $company);

                    $co_worker_registered = $register_new_co_worker->register();

                    if ($co_worker_registered)
                    {
                        App__Ed__Model__Acl::add_user_to_group($co_worker_registered, 2);

                        $confirmed_co_worker = new Confirmed_co_workers();
                        $confirmed_co_worker->{Confirmed_co_workers::COLUMN_ID_USER} = $co_worker_registered;
                        $confirmed_co_worker->{Confirmed_co_workers::COLUMN_STATUS} = Confirmed_co_workers::STATUS_NOT_CONFIRMED;
                        $confirmed_co_worker->{Confirmed_co_workers::COLUMN_ID_COMPANY} = $company->id;
                        $confirmed_co_worker->save();
                    }

                    if($co_worker_registered && Can_send_email::verify((int)$this->session['id'], Users_options::OPTION_SEND_NEW_ASSOCIATE_NOTIFICATION))
                    {
                        $user = App__Ed__Model__Users::find($co_worker_registered);

                        $email_receipent = App__Ed__Model__Users::find((int)$this->session['id']);

                        $new_user_name = $user->name . ' ' . $user->surname;

                        $company_administrator = App__Ed__Model__Users::find(Lib__Session::get('id'));

                        Send_email_by_queue::send(
                            (string)$email_receipent->email,
                            'Dodałeś nowego współpracownika - Electrolux od kuchni',
                            'Dodałeś nowego współpracownika',
                            "Dodałeś swojego współpracownika.<br>
Gratulujemy. Powiększasz swoje grono i dodałeś nowego współpracownika.<br>
Dodany współpracownik to {$new_user_name}.<br>
Teraz jesteś w stanie dzielić się z nim swoimi punktami. Możesz to zrobić ze swojego panelu użytkownika z zakładki ‘Panel’.<br>
W razie jakikolwiek wątpliwości zapraszamy do kontaktu z swoim handlowcem."
                        );

                        Send_email_by_queue::send(
                            (string)$user->email,
                            'Zostałeś zaproszony do Programu Electrolux od Kuchni!',
                            'Witaj ' . $user->name . '!',
                            "{$company_administrator->name} {$company_administrator->surname} zaprosił cię do Programu Partnerskiego Electrolux od Kuchni.
Program został stworzony dla studiów kuchennych współpracujących z firmą Electrolux. Celem programu jest budowanie wspólnych relacji, wartości oraz umocnienie więzi biznesowych.
<br><br>
Twoje konto wymaga jeszcze weryfikacji i aktywacji przez przypisanego opiekuna.<br>
Możesz się zalogować i przeglądać portal oraz nagrody, ale nie możesz ich jeszcze zamawiać.<br>
W najbliższym czasie opiekun się z Tobą skontaktuje w celu autoryzacji regulaminu.<br>
Po tym, Twoje konto zostanie w pełni aktywowane.<br>
<br><br>
Twoje dane do logowania:<br>
Login: {$user->login}<br>
Hasło: " . App__Ed__Model__Encryption::decode($user->password) . "<br>
<a href='" . BASE_URL . "'>www.electroluxodkuchni.pl</a>"
                        );
                    }

                    go_back((bool)$co_worker_registered ? 'g|Zapisano dane.' : 'Nie udało się zapisać danych.', BASE_URL . 'panel');
                }
                catch (Exception $exception)
                {
                    go_back('d|' . $exception->getMessage(), BASE_URL . 'panel');
                }
            }
        }
        else
        {
            $smarty = new Smarty();
            $smarty->assign('header_text', empty($_GET['id']) ? 'Dodaj współpracownika' : 'Edytuj współpracownika');
            $smarty->assign('id', empty($_GET['id']) ? 0 : $_GET['id']);

            if (!empty($_GET['id']))
            {
                $user = App__Ed__Model__Users::find($_GET['id']);
                $smarty->assign('user', $user);
            }

            echo $smarty->fetch('user_dashboard/edycja/_partials/edycja_wspolpracownika.tpl');
        }
    }

    public function action_assign_elux()
    {
        if (isset($_POST['points']) && !empty($_GET['id']))
        {
            $users_e_lux = [];
            foreach (array_merge([$_GET['id']], [(int)$this->session['id']]) as $userID) {
                $users_e_lux[$userID] = App__Ed__Model__Points_log::getPoints($userID);
            }

            $logged_user_e_lux = empty($users_e_lux[(int)$this->session['id']]) ? 0 : $users_e_lux[(int)$this->session['id']];

            $receiver_e_lux = empty($users_e_lux[(int)$_GET['id']]) ? 0 : $users_e_lux[(int)$_GET['id']];

            if((int)$_POST['points'] < 0)
            {
                if ($receiver_e_lux < abs((int)$_POST['points']))
                {
                    $message = 'd|Użytkownik nie posiada wystarczającej ilości punktów';
                }
                else
                {
                    $transfer_points = new Transfer_points((int)$this->session['id'], (int)$_GET['id'], (int)$_POST['points']);
                    $points_assigned = $transfer_points->transfer();

                    $message = $points_assigned ? 'g|Odebrano E-LUXy' : 'd|Nie udało się odebrać E-LUXów';
                }
            }
            else
            {
                if ($logged_user_e_lux < (int)$_POST['points'])
                {
                    $message = 'd|Nie posiadasz wystarczającej ilości punktów';
                }
                else
                {
                    $transfer_points = new Transfer_points((int)$this->session['id'], (int)$_GET['id'], (int)$_POST['points']);
                    $points_assigned = $transfer_points->transfer();

                    $message = $points_assigned ? 'g|Przypisano E-LUXy' : 'd|Nie udało się przypisać E-LUXów';
                }
            }

            go_back($message, BASE_URL . 'panel');
        }
        else
        {
            $smarty = new Smarty();
            $smarty->assign('id', empty($_GET['id']) ? 0 : $_GET['id']);

            echo $smarty->fetch('user_dashboard/edycja/_partials/przydziel_punkty.tpl');
        }
    }

    public static function action_ajax_get_my_points()
    {
        echo App__Ed__Model__Points_log::getPoints($_SESSION['id']);
    }

    public function action_faq()
    {
        $user_data = App__Ed__Model__Users::find((int)$this->session['id']);

        if (!App__Ed__Model__Acl::has_group('trader')) //handlowcy mają dostęp do panelu, ale ograniczony widok
        {
            $company = Get_by_user_id::get((int)$this->session['id']);
            $company_users = Get_company_users::get($company->id);

            $company_users_ids = [];
            if (!empty($company_users))
            {
                $company_users_ids = array_map(
                    function($company_user) {
                        return $company_user->id_user;
                    }, $company_users);
            }

            foreach (array_merge($company_users_ids, [(int)$this->session['id']]) as $userID) {
                $users_e_lux[$userID] = App__Ed__Model__Points_log::getPoints($userID);
            }
            $annual_eluxes = Get_users_points::get_annual([(int)$this->session['id']]);
            $annual_eluxes = !empty($annual_eluxes) ? reset($annual_eluxes) : 0;

            $this->view->assign('company', $company);
            $this->view->assign('users_e_lux', $users_e_lux);
            $this->view->assign('annual_eluxes', $annual_eluxes);
            $this->view->assign('users_extra_e_lux', App__Ed__Model__Points_extra_log::getPoints((int)$this->session['id']));
            $this->view->assign('user_spend_e_lux', App__Ed__Model__Points_log::getSpentPoints((int)$this->session['id']));
        }

        $faq = Get_faq_list::get($this->session['id_service'], $this->session['id_lang']);
        $this->view->assign('faq_categories', $faq);
        $this->view->assign('user_data', $user_data);

        $this->template = 'user_dashboard/faq/uzytkownik_panel_faq.tpl';
        $this->page_title = "FAQ";

        $this->breadcrumb = [
            BASE_URL . 'panel'          => 'Moje konto',
            BASE_URL . 'panel/faq.html' => 'FAQ',
        ];
    }

    public function action_ustawienia()
    {
        $user_data = App__Ed__Model__Users::find((int)$this->session['id']);

        if(!App__Ed__Model__Acl::has_group('trader'))
        {
            $company = Get_by_user_id::get((int)$this->session['id']);
            $company_users = Get_company_users::get($company->id);

            $company_users_ids = [];
            if (!empty($company_users))
            {
                $company_users_ids = array_map(
                    function($company_user) {
                        return $company_user->id_user;
                    }, $company_users);
            }

            foreach (array_merge($company_users_ids, [(int)$this->session['id']]) as $userID) {
                $users_e_lux[$userID] = App__Ed__Model__Points_log::getPoints($userID);
            }
            $annual_eluxes = Get_users_points::get_annual([(int)$this->session['id']]);
            $annual_eluxes = !empty($annual_eluxes) ? reset($annual_eluxes) : 0;

            $user_options = Get_user_options::get((int)$this->session['id']);

            $this->view->assign('user_options', $user_options);
            $this->view->assign('company', $company);
            $this->view->assign('users_e_lux', $users_e_lux);
            $this->view->assign('annual_eluxes', $annual_eluxes);
            $this->view->assign('users_extra_e_lux', App__Ed__Model__Points_extra_log::getPoints((int)$this->session['id']));
            $this->view->assign('user_spend_e_lux', App__Ed__Model__Points_log::getSpentPoints((int)$this->session['id']));
        }


        $this->view->assign('user_data', $user_data);
        $this->template = 'user_dashboard/ustawienia/uzytkownik_panel_ustawienia.tpl';
        $this->page_title = "Ustawienia";

        $this->breadcrumb = [
            BASE_URL . 'panel'                 => 'Moje konto',
            BASE_URL . 'panel/ustawienia.html' => 'Ustawienia',
        ];
    }

    public function action_nagrody()
    {
        if(App__Ed__Model__Acl::has_group('trader'))
        {
            go_back("d|Brak dostępu");
        }

        $company = Get_by_user_id::get((int)$this->session['id']);
        $company_users = Get_company_users::get($company->id);
        $user_data = App__Ed__Model__Users::find((int)$this->session['id']);

        $company_users_ids = [];
        if (!empty($company_users))
        {
            $company_users_ids = array_map(
                function($company_user) {
                    return $company_user->id_user;
                }, $company_users);
        }

        foreach (array_merge($company_users_ids, [(int)$this->session['id']]) as $userID) {
            $users_e_lux[$userID] = App__Ed__Model__Points_log::getPoints($userID);
        }
        $annual_eluxes = Get_users_points::get_annual([(int)$this->session['id']]);
        $annual_eluxes = !empty($annual_eluxes) ? reset($annual_eluxes) : 0;

        $user_prizes = Get_user_prizes_earned::get((int)$this->session['id']);

        $this->view->assign('user_prizes', $user_prizes);
        $this->view->assign('company', $company);
        $this->view->assign('users_e_lux', $users_e_lux);
        $this->view->assign('annual_eluxes', $annual_eluxes);
        $this->view->assign('user_data', $user_data);
        $this->view->assign('users_extra_e_lux', App__Ed__Model__Points_extra_log::getPoints((int)$this->session['id']));
        $this->view->assign('user_spend_e_lux', App__Ed__Model__Points_log::getSpentPoints((int)$this->session['id']));
        $this->template = 'user_dashboard/nagrody/uzytkownik_panel_nagrody.tpl';
        $this->page_title = "Nagrody";

        $this->breadcrumb = [
            BASE_URL . 'panel'              => 'Moje konto',
            BASE_URL . 'panel/nagrody.html' => 'Nagrody',
        ];
    }

    public function action_get_user_prices()
    {
        $return_data = [
            'recordsTotal'    => 0,
            'recordsFiltered' => 0,
            'data'            => [],
        ];

        if (!empty($_POST))
        {
            $column_map = [
                0 => 'id',
                1 => 'created_at',
                2 => 'name',
                3 => 'price',
                4 => 'count',
            ];
            $user_prizes = Get_user_prizes_earned::get(
                (int)$_POST['user_id'],
                (int)$_POST['start'],
                (int)$_POST['length'],
                $_POST['search']['value'],
                $column_map[$_POST['order'][0]['column']],
                $_POST['order'][0]['dir']
            );

            foreach ($user_prizes as $order)
            {
                foreach ($order->products_decoded as $product)
                {
                    $prize_name = 'Nagroda została usunięta';
                    if(!empty($product->prize->name) && !empty($product->prize_option->title))
                    {
                        $prize_name = $product->prize->name . '-' . $product->prize_option->title;
                    }

                    $return_data['data'][] = [
                        $order->id,
                        $order->created_at->format('Y-m-d H:i:s'),
                        $prize_name,
                        $product->price . ' E-LUX',
                        $product->count,
                        ($product->price * $product->count) . ' E-LUX',
                    ];
                }
            }

            $return_data['recordsTotal'] = $_POST['records_total'];
            $return_data['recordsFiltered'] = $_POST['records_total'];

            echo json_encode($return_data);
        }
        else
        {
            echo json_encode($return_data);
        }
    }

    public function action_kontakt()
    {
        if(App__Ed__Model__Acl::has_group('trader'))
        {
            go_back("d|Brak dostępu");
        }

        $company = Get_by_user_id::get((int)$this->session['id']);
        $company_users = Get_company_users::get($company->id);
        $user_data = App__Ed__Model__Users::find((int)$this->session['id']);

        $supervisor = Get_company_supervisor::get((int)$company->id);

        $company_users_ids = [];
        if (!empty($company_users))
        {
            $company_users_ids = array_map(
                function($company_user) {
                    return $company_user->id_user;
                }, $company_users);
        }

        foreach (array_merge($company_users_ids, [(int)$this->session['id']]) as $userID) {
            $users_e_lux[$userID] = App__Ed__Model__Points_log::getPoints($userID);
        }
        $annual_eluxes = Get_users_points::get_annual([(int)$this->session['id']]);
        $annual_eluxes = !empty($annual_eluxes) ? reset($annual_eluxes) : 0;

        $this->view->assign('supervisor', $supervisor);
        $this->view->assign('company', $company);
        $this->view->assign('users_e_lux', $users_e_lux);
        $this->view->assign('annual_eluxes', $annual_eluxes);
        $this->view->assign('user_data', $user_data);
        $this->view->assign('users_extra_e_lux', App__Ed__Model__Points_extra_log::getPoints((int)$this->session['id']));
        $this->view->assign('user_spend_e_lux', App__Ed__Model__Points_log::getSpentPoints((int)$this->session['id']));
        $this->template = 'user_dashboard/kontakt/uzytkownik_panel_kontakt_z_handlowcem.tpl';
        $this->page_title = "Kontakt z handlowcem";

        $this->breadcrumb = [
            BASE_URL . 'panel'              => 'Moje konto',
            BASE_URL . 'panel/kontakt.html' => 'Kontakt z handlowcem',
        ];
    }

    public function action_newsletter_rezygnacja()
    {
        go_back(
            Newsletter_subscription::unsubscribe((int)$this->_user->id) ? 'g|Zostałeś wypisany z newslettera.' : 'w|Nie zostałeś wypisany z newslettera.',
            BASE_URL . 'panel'
        );
    }

    public function action_save_user_data()
    {
        $data_saved = false;

        if (!empty($_POST))
        {
            $user = App__Ed__Model__Users::find($this->session['id']);
            $user->name = $_POST['name'];
            $user->surname = $_POST['surname'];
            $user->email = $_POST['email'];
            $user->telephone = $_POST['telephone'];

            $data_saved = $user->save();
        }

        http_response_code($data_saved ? 200 : 500);
    }

    public function action_change_user_password()
    {
        $data_saved = false;

        if (!empty($_POST))
        {
            $user = App__Ed__Model__Users::find($this->session['id']);

            $user_current_password = App__Ed__Model__Encryption::decode($user->password);

            if ($user_current_password == $_POST['current_password'] && ($_POST['new_password'] == $_POST['new_password_confirm']))
            {
                $user->password = App__Ed__Model__Encryption::encode($_POST['new_password']);

                $data_saved = $user->save();

                if ($data_saved && Can_send_email::verify((int)$this->session['id'], Users_options::OPTION_SEND_PASSWORD_CHANGE_NOTIFICATION))
                {
                    Send_email_by_queue::send(
                        (string)$user->email,
                        'Zmiana hasła - Electrolux od kuchni',
                        'Zmiana hasła',
                        'Twoje hasło właśnie zostało zmienione!'
                    );
                }
            }
        }

        http_response_code($data_saved ? 200 : 500);
    }

    public function action_user_settings()
    {
        $user_options = Get_user_options::get((int)$this->session['id']);

        $data_saved = false;

        foreach(Users_options::OPTIONS_ARRAY as $option)
        {
            $do_not_display_points = !empty($user_options[$option]) ?
                Users_options::find($user_options[$option]->id) : new Users_options();
            $do_not_display_points->{Users_options::COLUMN_ID_OPTION} = $option;
            $do_not_display_points->{Users_options::COLUMN_ID_USER} = (int)$this->session['id'];
            $do_not_display_points->{Users_options::COLUMN_STATUS} = empty($_POST[$option]) ? Users_options::STATUS_OFF : Users_options::STATUS_ON;
            $data_saved = $do_not_display_points->save();

            if($option == Users_options::OPTION_SHOW_POINTS)
            {
                $_SESSION['display_points'] = $do_not_display_points->{Users_options::COLUMN_STATUS};
            }
        }

        http_response_code($data_saved ? 200 : 500);
    }

    public function action_remove_address()
    {
        $address_removed = false;

        if(!empty($_POST['id_address_delete']))
        {
            $address = App__Ed__Model__Company__Address::find($_POST['id_address_delete']);
            $address->deleted_at = (new DateTime('now'))->format('Y-m-d h:i:s');

            $address_removed = $address->save();
        }

        http_response_code($address_removed ? 200 : 500);
    }

    public function action_wiadomosci()
    {
        if(App__Ed__Model__Acl::has_group('trader'))
        {
            go_back("d|Brak dostępu");
        }
        
        $id_user = (int)Lib__Session::get('id');

        $company = Get_by_user_id::get($id_user);
        $company_users = Get_company_users::get($company->id);
        $user_data = App__Ed__Model__Users::find($id_user);

        $company_users_ids = [];
        if (!empty($company_users))
        {
            $company_users_ids = array_map(
                function($company_user) {
                    return $company_user->id_user;
                }, $company_users);
        }
        $users_e_lux = [];
        foreach (array_merge($company_users_ids, [$id_user]) as $userID) {
            $users_e_lux[$userID] = App__Ed__Model__Points_log::getPoints($userID);
        }
        $annual_eluxes = Get_users_points::get_annual([(int)$this->session['id']]);
        $annual_eluxes = !empty($annual_eluxes) ? reset($annual_eluxes) : 0;

        $this->view->assign('company', $company);
        $this->view->assign('users_e_lux', $users_e_lux);
        $this->view->assign('annual_eluxes', $annual_eluxes);
        $this->view->assign('user_data', $user_data);
        $this->view->assign('users_extra_e_lux', App__Ed__Model__Points_extra_log::getPoints($id_user));
        $this->view->assign('user_spend_e_lux', App__Ed__Model__Points_log::getSpentPoints((int)$this->session['id']));

        $page = !empty($_GET) && !empty($_GET['page']) ? (int)$_GET['page'] : 1;
        $per_page = 5;

        $list = new App__Ed__Model__Messages__List;
        $list->set_where("id_to = " . $id_user);
        $list->set_page($page);
        $list->set_per_page($per_page);
        $list->set_folder('inbox');
        $list = $list->execute();

        $this->view->assign('list', $list['list']);
        $this->view->assign('title', 'Skrzynka odbiorcza');
        $this->view->assign('display_actions', true);
        $this->view->assign('new_messages_count', Get_new_messages_count::get($id_user));
        $this->view->assign('received_active', true);
        $this->view->assign('page', $page);
        $this->view->assign('per_page', $per_page);
        $this->view->assign('all_items', $list['total']);
        $this->view->assign('folder', 'inbox');

        $this->template = 'wiadomosci/odebrane.tpl';
        $this->page_title = "Wiadomości";

        $this->breadcrumb = [
            BASE_URL . 'panel'              => 'Moje konto',
            BASE_URL . 'panel/wiadomosci.html' => 'Wiadomości',
        ];
    }

    public function action_wiadomosci_wazne()
    {
        if(App__Ed__Model__Acl::has_group('trader'))
        {
            go_back("d|Brak dostępu");
        }

        $id_user = (int)Lib__Session::get('id');

        $company = Get_by_user_id::get($id_user);
        $company_users = Get_company_users::get($company->id);
        $user_data = App__Ed__Model__Users::find($id_user);

        $company_users_ids = [];
        if (!empty($company_users))
        {
            $company_users_ids = array_map(
                function($company_user) {
                    return $company_user->id_user;
                }, $company_users);
        }

        $users_e_lux = [];
        foreach (array_merge($company_users_ids, [$id_user]) as $userID) {
            $users_e_lux[$userID] = App__Ed__Model__Points_log::getPoints($userID);
        }
        $annual_eluxes = Get_users_points::get_annual([(int)$this->session['id']]);
        $annual_eluxes = !empty($annual_eluxes) ? reset($annual_eluxes) : 0;

        $this->view->assign('company', $company);
        $this->view->assign('users_e_lux', $users_e_lux);
        $this->view->assign('annual_eluxes', $annual_eluxes);
        $this->view->assign('user_data', $user_data);
        $this->view->assign('users_extra_e_lux', App__Ed__Model__Points_extra_log::getPoints($id_user));
        $this->view->assign('user_spend_e_lux', App__Ed__Model__Points_log::getSpentPoints((int)$this->session['id']));

        $page = !empty($_GET) && !empty($_GET['page']) ? (int)$_GET['page'] : 1;
        $per_page = 5;

        $list = new App__Ed__Model__Messages__List;
        $list->set_where("(id_to = $id_user AND important_to=1) OR (id_from = $id_user AND important_from=1)");
        $list->set_page($page);
        $list->set_per_page($per_page);
        $list->set_folder('inbox');
        $list = $list->execute();

        $this->view->assign('list', $list['list']);
        $this->view->assign('title', 'Skrzynka odbiorcza');
        $this->view->assign('display_actions', true);
        $this->view->assign('new_messages_count', Get_new_messages_count::get($id_user));
        $this->view->assign('important_active', true);
        $this->view->assign('page', $page);
        $this->view->assign('per_page', $per_page);
        $this->view->assign('all_items', $list['total']);
        $this->view->assign('folder', 'inbox');

        $this->template = 'wiadomosci/odebrane.tpl';
        $this->page_title = "Wiadomości";

        $this->breadcrumb = [
            BASE_URL . 'panel'              => 'Moje konto',
            BASE_URL . 'panel/wiadomosci.html' => 'Wiadomości',
        ];
    }

    public function action_wiadomosci_wyslane()
    {
        if(App__Ed__Model__Acl::has_group('trader'))
        {
            go_back("d|Brak dostępu");
        }

        $id_user = (int)Lib__Session::get('id');

        $company = Get_by_user_id::get($id_user);
        $company_users = Get_company_users::get($company->id);
        $user_data = App__Ed__Model__Users::find($id_user);

        $company_users_ids = [];
        if (!empty($company_users))
        {
            $company_users_ids = array_map(
                function($company_user) {
                    return $company_user->id_user;
                }, $company_users);
        }

        $users_e_lux = [];
        foreach (array_merge($company_users_ids, [$id_user]) as $userID) {
            $users_e_lux[$userID] = App__Ed__Model__Points_log::getPoints($userID);
        }
        $annual_eluxes = Get_users_points::get_annual([(int)$this->session['id']]);
        $annual_eluxes = !empty($annual_eluxes) ? reset($annual_eluxes) : 0;

        $this->view->assign('company', $company);
        $this->view->assign('users_e_lux', $users_e_lux);
        $this->view->assign('annual_eluxes', $annual_eluxes);
        $this->view->assign('user_data', $user_data);
        $this->view->assign('users_extra_e_lux', App__Ed__Model__Points_extra_log::getPoints($id_user));
        $this->view->assign('user_spend_e_lux', App__Ed__Model__Points_log::getSpentPoints((int)$this->session['id']));

        $page = !empty($_GET) && !empty($_GET['page']) ? (int)$_GET['page'] : 1;
        $per_page = 5;

        $list = new App__Ed__Model__Messages__List;
        $list->set_where("id_from = $id_user");
        $list->set_page($page);
        $list->set_per_page($per_page);
        $list->set_folder('inbox');
        $list = $list->execute();

        $this->view->assign('list', $list['list']);
        $this->view->assign('title', 'Skrzynka odbiorcza');
        $this->view->assign('display_actions', true);
        $this->view->assign('new_messages_count', Get_new_messages_count::get($id_user));
        $this->view->assign('sent_active', true);
        $this->view->assign('page', $page);
        $this->view->assign('per_page', $per_page);
        $this->view->assign('all_items', $list['total']);
        $this->view->assign('folder', 'inbox');

        $this->template = 'wiadomosci/odebrane.tpl';
        $this->page_title = "Wiadomości";

        $this->breadcrumb = [
            BASE_URL . 'panel'              => 'Moje konto',
            BASE_URL . 'panel/wiadomosci.html' => 'Wiadomości',
        ];
    }

    public function action_wiadomosci_usuniete()
    {
        if(App__Ed__Model__Acl::has_group('trader'))
        {
            go_back("d|Brak dostępu");
        }

        $id_user = (int)Lib__Session::get('id');

        $company = Get_by_user_id::get($id_user);
        $company_users = Get_company_users::get($company->id);
        $user_data = App__Ed__Model__Users::find($id_user);

        $company_users_ids = [];
        if (!empty($company_users))
        {
            $company_users_ids = array_map(
                function($company_user) {
                    return $company_user->id_user;
                }, $company_users);
        }

        $users_e_lux = [];
        foreach (array_merge($company_users_ids, [$id_user]) as $userID) {
            $users_e_lux[$userID] = App__Ed__Model__Points_log::getPoints($userID);
        }
        $annual_eluxes = Get_users_points::get_annual([(int)$this->session['id']]);
        $annual_eluxes = !empty($annual_eluxes) ? reset($annual_eluxes) : 0;

        $this->view->assign('company', $company);
        $this->view->assign('users_e_lux', $users_e_lux);
        $this->view->assign('annual_eluxes', $annual_eluxes);
        $this->view->assign('user_data', $user_data);
        $this->view->assign('users_extra_e_lux', App__Ed__Model__Points_extra_log::getPoints($id_user));
        $this->view->assign('user_spend_e_lux', App__Ed__Model__Points_log::getSpentPoints((int)$this->session['id']));

        $page = !empty($_GET) && !empty($_GET['page']) ? (int)$_GET['page'] : 1;
        $per_page = 5;

        $list = new App__Ed__Model__Messages__List;
        $list->set_where("(id_to = $id_user AND trash_to=1) OR (id_from = $id_user AND trash_from=1)");
        $list->set_page($page);
        $list->set_per_page($per_page);
        $list->set_folder('inbox');
        $list = $list->execute();

        $this->view->assign('list', $list['list']);
        $this->view->assign('title', 'Skrzynka odbiorcza');
        $this->view->assign('display_actions', true);
        $this->view->assign('new_messages_count', Get_new_messages_count::get($id_user));
        $this->view->assign('received_active', true);
        $this->view->assign('page', $page);
        $this->view->assign('per_page', $per_page);
        $this->view->assign('all_items', $list['total']);
        $this->view->assign('folder', 'inbox');

        $this->template = 'wiadomosci/odebrane.tpl';
        $this->page_title = "Wiadomości usunięte";

        $this->breadcrumb = [
            BASE_URL . 'panel'              => 'Moje konto',
            BASE_URL . 'panel/wiadomosci.html' => 'Wiadomości usunięte',
        ];
    }

    public function action_wiadomosci_napisz()
    {
        if(App__Ed__Model__Acl::has_group('trader'))
        {
            go_back("d|Brak dostępu");
        }

        $id_user = (int)Lib__Session::get('id');

        $company = Get_by_user_id::get($id_user);
        $company_users = Get_company_users::get($company->id);
        $user_data = App__Ed__Model__Users::find($id_user);

        $company_users_ids = [];
        if (!empty($company_users))
        {
            $company_users_ids = array_map(
                function($company_user) {
                    return $company_user->id_user;
                }, $company_users);
        }

        $users_e_lux = [];
        foreach (array_merge($company_users_ids, [$id_user]) as $userID) {
            $users_e_lux[$userID] = App__Ed__Model__Points_log::getPoints($userID);
        }
        $annual_eluxes = Get_users_points::get_annual([(int)$this->session['id']]);
        $annual_eluxes = !empty($annual_eluxes) ? reset($annual_eluxes) : 0;

        $this->view->assign('company', $company);
        $this->view->assign('users_e_lux', $users_e_lux);
        $this->view->assign('annual_eluxes', $annual_eluxes);
        $this->view->assign('user_data', $user_data);
        $this->view->assign('users_extra_e_lux', App__Ed__Model__Points_extra_log::getPoints($id_user));
        $this->view->assign('user_spend_e_lux', App__Ed__Model__Points_log::getSpentPoints((int)$this->session['id']));

        $page = !empty($_GET) && !empty($_GET['page']) ? (int)$_GET['page'] : 1;
        $per_page = 5;

        $list = new App__Ed__Model__Messages__List;
        $list->set_where("(id_to = $id_user AND important_to=1) OR (id_from = $id_user AND important_from=1)");
        $list->set_page($page);
        $list->set_per_page($per_page);
        $list->set_folder('inbox');
        $list = $list->execute();

        $this->view->assign('list', $list['list']);
        $this->view->assign('title', 'Skrzynka odbiorcza');
        $this->view->assign('display_actions', true);
        $this->view->assign('new_messages_count', Get_new_messages_count::get($id_user));
        $this->view->assign('important_active', true);
        $this->view->assign('page', $page);
        $this->view->assign('per_page', $per_page);
        $this->view->assign('all_items', $list['total']);
        $this->view->assign('folder', 'inbox');
        $this->template = 'wiadomosci/napisz.tpl';
        $this->page_title = 'Napisz nową wiadomość';
        $this->breadcrumb = [
            BASE_URL . 'panel'              => 'Moje konto',
            BASE_URL . 'panel/wiadomosci_napisz.html' => 'Napisz',
        ];
    }

    public function action_wiadomosci_pokaz()
    {
        if(App__Ed__Model__Acl::has_group('trader'))
        {
            go_back("d|Brak dostępu");
        }
        
        $id_user = (int)Lib__Session::get('id');

        $company = Get_by_user_id::get($id_user);
        $company_users = Get_company_users::get($company->id);
        $user_data = App__Ed__Model__Users::find($id_user);

        $company_users_ids = [];
        if (!empty($company_users))
        {
            $company_users_ids = array_map(
                function($company_user) {
                    return $company_user->id_user;
                }, $company_users);
        }

        $users_e_lux = [];
        foreach (array_merge($company_users_ids, [$id_user]) as $userID) {
            $users_e_lux[$userID] = App__Ed__Model__Points_log::getPoints($userID);
        }
        $annual_eluxes = Get_users_points::get_annual([(int)$this->session['id']]);
        $annual_eluxes = !empty($annual_eluxes) ? reset($annual_eluxes) : 0;

        $this->view->assign('company', $company);
        $this->view->assign('new_messages_count', Get_new_messages_count::get($id_user));
        $this->view->assign('users_e_lux', $users_e_lux);
        $this->view->assign('annual_eluxes', $annual_eluxes);
        $this->view->assign('user_data', $user_data);
        $this->view->assign('users_extra_e_lux', App__Ed__Model__Points_extra_log::getPoints($id_user));
        $this->view->assign('user_spend_e_lux', App__Ed__Model__Points_log::getSpentPoints((int)$this->session['id']));

        $hash = !empty($_GET) && !empty($_GET['m']) ? $_GET['m'] : '';

        //jeżeli nie ma hash'a pokazuję 404
        if(!$hash)
        {
            show_404();
        }

        //odkodowuję hash
        $hash_org = $hash;
        $hash_decoded = App__Ed__Model__Messages__Model::decode_hash($hash);

        //pokazuję tylko wiadomość, która była wysłana, bądź adresowana do użytkownika
        $id_user = (int)Lib__Session::get('id');
        if($id_user == $hash_decoded['id_from'] || $id_user == $hash_decoded['id_to'])
        {
            //pokazuję wiadomość
            $message = App__Ed__Model__Messages::find(
                App__Ed__Model__Messages::where("id = " . $hash_decoded['id_message'])
                                        ->add("id_from = $id_user OR id_to = $id_user")
            );

            if(empty($message))
            {
                show_404();
            }

            $message = reset($message);

            //określam użytkownika jakiego pokazujemy
            if($hash_decoded['folder'] == 'inbox')
            {
                $id_user_show = $message->id_from;
            }
            elseif($hash_decoded['folder'] == 'sent')
            {
                $id_user_show = $message->id_to;
            }

            //jeżeli jest to folder odebrane, a wiadomosc jest nieodczytana, ustawiam jej flagę odczytanej
            if($hash_decoded['folder']  == 'inbox' && $message->view_to == 0)
            {
                $message->set_as_read();
            }

            $this->view->assign('id_message', $hash_decoded['id_message']);
            $this->view->assign('hash', $hash_org);
            $this->view->assign('id_user', $id_user_show);
            $this->view->assign('folder', $hash_decoded['folder']);
            $this->view->assign('message', $message);

            $this->template = 'wiadomosci/pokaz.tpl';
            $this->page_title = $message->subject;
            $this->breadcrumb = [
                BASE_URL . 'panel'              => 'Moje konto',
                '' => 'Pokaż wiadomość',
            ];

        }
        else
        {
            show_403();
        }
    }

    public function action_delete_co_worker()
    {
        $removed = false;

        if(!empty($_POST['id_user']))
        {
            $id_user_to_remove = (int)$_POST['id_user'];

            $company = Get_by_user_id::get((int)$this->session['id']);
            $company_users = Get_company_users::get_by_users_ids($company->id);
            $points = App__Ed__Model__Points_log::getPoints($id_user_to_remove);

            if (!empty($company_users[$id_user_to_remove]) && !empty($company_users[(int)$this->session['id']]) && $company_users[(int)$this->session['id']]->role == 'admin') {
                $transfer_points = new Transfer_points((int)$id_user_to_remove, (int)$this->session['id'], $points);
                $points_assigned = $transfer_points->transfer();
                $company_user = App__Ed__Model__Company__Users::find($company_users[$id_user_to_remove]->id);
                $company_user->deleted_at = (new DateTime('now'))->format('Y-m-d H:i:s');

                $removed = $points_assigned && $company_user->save();
            }
        }

        http_response_code($removed ? 200 : 500);
    }

    public function action_tutorial()
    {
        $id_user = (int)Lib__Session::get('id');
        $user_data = App__Ed__Model__Users::find($id_user);

        if (!App__Ed__Model__Acl::has_group('trader')) //handlowcy mają dostęp do panelu, ale ograniczony widok
        {
            $company = Get_by_user_id::get($id_user);
            $company_users = Get_company_users::get($company->id);

            $company_users_ids = [];
            if (!empty($company_users))
            {
                $company_users_ids = array_map(
                    function($company_user) {
                        return $company_user->id_user;
                    }, $company_users);
            }

            $users_e_lux = [];
            foreach (array_merge($company_users_ids, [$id_user]) as $userID) {
                $users_e_lux[$userID] = App__Ed__Model__Points_log::getPoints($userID);
            }
            $annual_eluxes = Get_users_points::get_annual([(int)$this->session['id']]);
            $annual_eluxes = !empty($annual_eluxes) ? reset($annual_eluxes) : 0;

            $this->view->assign('company', $company);
            $this->view->assign('users_e_lux', $users_e_lux);
            $this->view->assign('annual_eluxes', $annual_eluxes);
            $this->view->assign('users_extra_e_lux', App__Ed__Model__Points_extra_log::getPoints($id_user));
            $this->view->assign('user_spend_e_lux', App__Ed__Model__Points_log::getSpentPoints((int)$this->session['id']));
        }

        $this->view->assign('user_data', $user_data);

        $this->template = 'tutorial/tutorial.tpl';
        $this->page_title = "Tutorial";

    }
}