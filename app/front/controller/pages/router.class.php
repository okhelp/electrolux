<?php

/**
 * Klasa routingu dla kontrolera welcome
 */
class App__Front__Controller__Pages__Router
{
	public static function set()
	{
		$router = [];

		$router[] = array(
			'uri' => str_replace(BASE_DIR,'',$_SERVER['REQUEST_URI']),
			'controller' => 'App__Front__Controller__Pages__Index',
			'method' => 'action_index'
		);
		
		return $router;
	}
}