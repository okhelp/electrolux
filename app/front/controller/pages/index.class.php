<?php

class App__Front__Controller__Pages__Index extends Lib__Base__Controller
{
	protected $_id_category;
	protected $_categories_list;
	protected $_menu;
	protected $_params_url;
	protected $_id_page;
	public $page_data;
	protected $_url;

	public function __construct()
	{
		parent::__construct();

		//tworzę parametry
		$this->_params_url = $this->prepare_params();

		//ustawiam id kategorii
		$this->_id_category = $this->get_id_category();

		//ustawiam id_page
		$this->_id_page = $this->get_id_page();

		//pobieram dane podstrony
		$this->page_data = $this->get_page_data();

		//sprawdzam poprawność adresu url
		$this->validate_url();

		//generuję menu
		$this->_menu = $this->generate_pages_menu();

		//ładuję metatagi
		$this->prepare_metatags();
	}

	private function get_page_data()
	{
		$id_category = $this->_id_category;

		//pobieram dane podstrony
		$page_where = App__Ed__Model__Pages::where("id = " . $this->_id_page);

		if(!empty($id_category))
		{
			$page_where->add("id_category = $id_category");
		}

		$page_where->add("status = 1");
		$page_data = App__Ed__Model__Pages::find($page_where);

		//nie ma takiej podstrony, pokazuję 404
		if(empty($page_data))
		{
			show_404();
		}

		//wybieram pierwszy element z tablicy
		$page_data = reset($page_data);

		//zapisuję poprawny url podstrony
		$this->_url = $page_data->get_url();

		return $page_data;
	}

	public function action_index()
	{
		//tworzę okruszki
		$this->breadcrumb = $this->generate_breadcrumbs();

		//"rozpakowuję opis" - uwzględniam bb_code
		$bbcode = new App__Ed__Model__Pages__Bbcode;
		$bbcode->set_data($this);
		$this->page_data->description = $bbcode->execute();

		//dodaję zmienne do szablonu
		$this->view->assign('menu',!empty($this->_menu) ? array($this->_menu) : array());
		$this->view->assign('page',$this->page_data);



		//ładuje templatkę
        $this->template = 'pages/index.tpl';
		$this->page_title = $this->page_data->title;

	}

	private function get_id_page()
	{
		//konkretna podstrona
		if(isset($this->_params_url['id_page']) && !empty($this->_params_url['id_page']))
		{
			$id_page = $this->_params_url['id_page'];
		}
		//strona kategorii
		else
		{
			//pobieram pierwszą podstronę
			$category = App__Ed__Model__Pages__Category::find($this->_id_category);
			$category_pages = $category->get_pages();
			if(!empty($category_pages))
			{
				$category_pages = reset($category_pages);
				$id_page = $category_pages->id;
			}
			else
			{
				$id_page = 0;
			}
		}

		return $id_page;
	}

	private function get_id_category()
	{
		if(!empty($this->_params_url['categories']))
		{
			$categories_arr = $this->_params_url['categories'];
			$categories_arr = array_reverse($categories_arr);
			$id_category = $this->_params_url['id_category'] = (int)reset($categories_arr);
		}
		else
		{
			$id_category = 0;
		}

		return $id_category;
	}

	private function prepare_params()
	{
		$return = array();

		$uri = str_replace(BASE_DIR,'',$_SERVER['REQUEST_URI']);
		$uri_arr = explode('/',$uri);
		$uri_arr = array_filter($uri_arr);

		foreach($uri_arr as $item_uri)
		{
			//pobieram dane podstrony
			if(strpos($item_uri,'.html') !== FALSE)
			{
				preg_match("/p[0-9]+/m", $item_uri,$find_id);
				if(!empty($find_id))
				{
					$find_id = reset($find_id);
					$find_id = str_replace("p","",$find_id);
					$return['id_page'] = (int)$find_id;
				}
			}
			//pobieram dane kategorii
			else
			{
				$category_where = App__Ed__Model__Pages__Category::where("name_seo = '$item_uri'");
				$category_where->add("status = 1");
				$category_item = App__Ed__Model__Pages__Category::find($category_where);
				if(!empty($category_item))
				{
					$category_item = reset($category_item);
					$return['categories'][] = $category_item->id;
				}
				else //jeżeli nie ma takiej kategorii, daję 404
				{
					show_404();
				}
			}

		}
		return $return;
	}

	private function generate_pages_menu()
	{
		$page_menu = array();

		if(!empty($this->_id_category))
		{
		    $list = App__Ed__Model__Pages__Category__Tree::get_list(0,1, $this->_id_category,'with_page');
			$page_menu = reset($list);
		}

		return $page_menu;
	}

	private function generate_breadcrumbs()
	{
		$list = array();

		if(!empty($this->_params_url['categories']))
		{
			foreach($this->_params_url['categories'] as $id_category)
			{
				$category = App__Ed__Model__Pages__Category::find($id_category);
				$url = str_replace(BASE_URL,"",$category->get_url());
				$list[$url] = $category->name;
			}
		}

		$list[str_replace(BASE_URL,"",$this->page_data->get_url())] = $this->page_data->title;

		return $list;
	}

	private function validate_url()
	{
        $valid_url = $this->_url;

        //dodaję geta do adresu, jeżeli takowy istnieje
//		if(!empty($_GET)) // PROBLEMATYCZNE PRZY PRZESYŁANIU DANYCH GET - EL-113
//		{
//			$get_data = array();
//			foreach ($_GET as $k => $v) {
//				$get_data[] = "$k=$v";
//			}
//
//			$valid_url .= "?" . implode("&", $get_data);
//		}

        $current_url = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://" . $_SERVER['HTTP_HOST']
            . $_SERVER['REQUEST_URI'];

        if (strpos($current_url, $valid_url) === false)
        {
            redirect($this->_url);
        }
	}

	private function prepare_metatags()
	{
		//tytuł strony
		if(!empty($this->page_data->title))
		{
			$this->metatags['page_title'] = $this->page_data->title;
		}

		//opis strony
		if(!empty($this->page_data->desc_meta))
		{
			$this->metatags['page_description'] = $this->page_data->desc_meta;
		}

		//słowa kluczowe
		if(!empty($this->page_data->keywords_meta))
		{
			$this->metatags['page_keywords'] = $this->page_data->keywords_meta;
		}

		//ikonka socialmedia
		if(!empty($this->page_data->social_logo))
		{
			$social_img = App__Ed__Model__Img::get_file(
				array('id_cimg' => $this->page_data->social_logo, 'width' => 320, 'height' => 320)
			);

			//dopisuję datę modyfikacji pliku
			$social_path = str_replace(BASE_URL,BASE_PATH,$social_img);
			$social_path = str_replace('_uploads/','data/uploads/',$social_path);
			$social_img .= "?" . filemtime($social_path);

			$this->metatags['page_social_img'] = $social_img;
		}
	}
}