<?php

use App\ed\model\mail\Add_to_queue;
use App\ed\model\mail\Send_email_by_queue;
use App\ed\model\newsletter\Users_options;
use App\ed\model\users\Can_send_email;
use App\front\models\auth\Change_service;
use App\front\models\auth\Verify_user;
use App\front\models\user\Remind_password;

class App__Front__Controller__Auth__Index extends Lib__Base__Controller
{
    public function action_login()
    {
        if (Verify_user::is_logged())
        {
            go_back("w|Jesteś już zalogowany.");
        }

        if (!empty($_POST))
        {
            if(!empty($_GET['ref']))
            {
                $ref = $_GET['ref'];
                $ref = urldecode($ref);
                $ref = base64_decode($ref);
            }

            $auth = new App__Ed__Model__Users__Auth($_POST['name'], $_POST['password'], 0, empty($ref) ? BASE_URL : BASE_URL . $ref);
            $auth->login();
        }
        else
        {
            $this->view->assign('ref', empty($_GET['ref']) ? '' : $_GET['ref']);
            $this->view->assign('display_login_form', true);
            $this->template = "welcome/index.tpl";
        }
    }

    public function action_logout()
    {
        if (!Verify_user::is_logged())
        {
            go_back('g|Zostałeś już wylogowany.', BASE_URL);
        }
        else
        {
            Lib__Session::remove();

            if (Lib__cookie::get(COOKIENAME_AUTOLOGIN))
            {
                Lib__cookie::remove(COOKIENAME_AUTOLOGIN);
            }

            go_back('g|Zostałeś poprawnie wylogowany.', BASE_URL);
        }
    }

    public function action_remind_password()
    {
        if (Verify_user::is_logged())
        {
            go_back('w|Jesteś już zalogowany.', BASE_URL);
        }
        else
        {
            try
            {
                $remind_password = new Remind_password($_POST['login'], $_POST['email']);
                $remind_password->remind();

                $message = 'g|Wysłano wiadomość.';
            }
            catch(Exception $exception)
            {
                $message = 'd|' . $exception->getMessage();
            }


            go_back($message, BASE_URL);
        }
    }

    public function action_change_password()
    {
        if (!Verify_user::is_logged())
        {
            go_back('d|Nie jesteś zalogowany.', BASE_URL);
        }
        else
        {
            if(!empty($_POST) && !empty($_POST['password']))
            {
                $password_1 = reset($_POST['password']);
                $password_2 = end($_POST['password']);

                if(empty($password_1) && empty($password_2))
                {
                    go_back("d|Proszę uzupełnić wszystkie pola formularza");
                }

                $user = App__Ed__Model__Users::find(Lib__Session::get('id'));

                $password_rodo = new App__Ed__Model__Users__Password_Rodo(
                    $user
                );

                if($password_rodo->change($password_1, $password_2))
                {
                    $back_url = BASE_URL;
                    if (isset($_POST['type_worker'])) {
                        App__Ed__Model__Users::update_all([
                            'set' => [
                                'type_worker' => $_POST['type_worker'],
                                'company_name'  => $_POST['company_name'] ?? '',
                            ],
                            'conditions' => [
                                'id' => $user->id,
                            ]
                        ]);
                    }
                    if(Can_send_email::verify((int)Lib__Session::get('id'), Users_options::OPTION_SEND_NEW_ASSOCIATE_NOTIFICATION))
                    {
                        Send_email_by_queue::send(
                            (string)$user->email,
                            'Zmiana hasła - Electrolux od kuchni',
                            'Zmiana hasła',
                            'Twoje hasło właśnie zostało zmienione!'
                        );
                    }

                    go_back("g|Dziękujemy. Twoje hasło zostało zmienione.", $back_url);
                }

            }
            else
            {
                $this->template = get_app_template_path() . "_partials/zmien_haslo.tpl";
            }
        }
    }

    public function action_change_service()
    {
        try
        {
            $id_service = empty($_GET['id_service']) ? 0 : (int)$_GET['id_service'];

            $change_service = new Change_service($id_service, (int)$this->_user->id);

            $change_service->change();

            go_back('g|Pomyślnie zmieniono firmę.');
        }
        catch (Exception $exception)
        {
            go_back('w|Nie udało się zmienić firmy.');
        }
    }

    public function action_aktywacja()
    {
        if(!empty($_GET['code']))
        {
            $activated = false;

            $code = urlencode($_GET['code']);

            //find code in db and activate user
            $user_by_code = App__Ed__Model__Users::find_by_sql("
                SELECT *
                FROM users
                WHERE activation_token = '" . $code . "' and status=0
            ");

            if(!empty($user_by_code[0]->id))
            {
                $user = App__Ed__Model__Users::find($user_by_code[0]->id);
                $user->status = 1;
                $user->activation_token = '';

                $activated = $user->save();
            }

            go_back($activated ? 'g|Pomyślnie aktywowano konto.' : 'w|Nie udało się aktywować konta.', BASE_URL);
        }
        else
        {
            show_404();
        }
    }
}