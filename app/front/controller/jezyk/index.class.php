<?php

class App__Front__Controller__Jezyk__Index extends Lib__Base__Controller
{
	public function action_zmien()
	{
		//ustawiam id_lang z GETa
		$id_lang = !empty($_GET) && !empty($_GET['id_lang']) ? (int)$_GET['id_lang'] : 0;
		
		if(!empty($id_lang))
		{
			//pobieram id serwisu
			$id_service = Lib__Session::get('id_service');
			
			//sprawdzam czy ten język jest przypisany do serwisu
			$sql = "SELECT l.* FROM language l "
					. "INNER JOIN language_service ls ON ls.id_lang = l.id "
					. "WHERE ls.id_service = $id_service AND l.id = $id_lang "
					. "LIMIT 1";
			$lang_data = App__Ed__Model__Language::find_by_sql($sql);
			
			//jeżeli nie znaleziono języka, kończę komunikatem
			if(empty($lang_data))
			{
				go_back('d|'.
						lang('Podany język nie może zostać ustawiony, '
								. 'ponieważ nie jest przypisany do tego serwisu'));
			}
			
			//zapisuję nowy język do sesji
			Lib__Session::set('id_lang', $id_lang);
			
			go_back('g|'.lang('Język został poprawnie zmieniony'));
		}
		else
		{
			go_back('d|'.lang('Proszę podać język, który mam ustawić.'));
		}
	}
}