<?php

use App\front\models\search\Search_by_phrase;
use App\modules\prizes\front\collections\Prizes_search;
use App\modules\products\front\collections\Products_search;

class App__Front__Controller__Wyszukiwarka__Index extends Lib__Base__Controller
{
    public function action_index()
    {
        $phrase = empty($_GET['phrase']) ? '' : (string)$_GET['phrase'];

        $all = Search_by_phrase::search($phrase);

        $this->view->assign('items', $all);
        $this->view->assign('page', 1);
        $this->view->assign('pages_count', count(Search_by_phrase::search($phrase, 999999999)));
        $this->view->assign('items_count', 12);

        $this->template = "wyszukiwarka/wyszukiwarka.tpl";
    }

    public function action_filter_changed()
    {
        $phrase = empty($_GET['phrase']) ? '' : (string)$_GET['phrase'];

        $count = empty($_POST['products_count_select']) ? 12 : $_POST['products_count_select'] == 'all' ? 999999 : (int)$_POST['products_count_select'];

        $order = empty($_POST['products_sort_select']) || (!empty($_POST['products_sort_select']) && $_POST['products_sort_select'] == 'e_lux_desc') ? 'e_lux_desc' :
            (string)$_POST['products_sort_select'];

        $category = empty($_POST['subcategories_ids']) ? 'all' : (string)$_POST['subcategories_ids'];
        $page = empty($_POST['page']) ? 1 : (int)$_POST['page'];

        $items = [];

        $all_items_count = 0;

        switch ($category)
        {
            case 'all':
                $items = Search_by_phrase::search($phrase, $count, $order, $page);
                $all_items_count = count(Search_by_phrase::search($phrase, 999999999, $order, 1));
                break;
            case 'prize':
                $items = Prizes_search::search($phrase, $count, $order, $page);
                $all_items_count = count(Prizes_search::search($phrase, 999999999, $order, 1));
                break;
            case 'product':
                $items = Products_search::search($phrase, $count, $order, $page);
                $all_items_count = count(Products_search::search($phrase, 999999999, $order, 1));
                break;
        }
        $smarty = new Smarty();
        $smarty->assign('items', $items);
        $smarty->assign('page', $page);
        $smarty->assign('pages_count', $all_items_count);
        $smarty->assign('items_count', $count);

        echo $smarty->fetch('wyszukiwarka/_partials/single_item2.tpl');
    }
}