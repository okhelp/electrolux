<?php

class App__Front__Controller__Formularz_Kontaktowy__Index extends Lib__Base__Controller
{
	public function action_wyslij()
	{
		//ustawiam treść wiadomości
		$message = "Dzień dobry, <br /> "
				. "Poniżej prezentujemy dane przesłane w formularzu kontaktowym. <br />";
		
		//zabezpieczenie przed spamem
		if(!empty($_POST) && $_POST['message_wrapper'] === '')
		{
			unset($_POST['message_wrapper']);
			
			foreach($_POST as $k => $v)
			{
				if($k != 'rodo')
				{
					$message .= "<b>$k</b>: $v <br />";
				}
			}

			if(!empty($_POST['rodo']))
			{
				$message .= "<b>Użytkownik wyraził zgodę</b>:<br /><i>";
				$message .= App__Ed__Model__Columns__Model::show_column('formularz_kontaktowy_rodo') . '</i>';
			}

			$title = "Wiadomość ze strony internetowej";

			//pobieram adres e-mail, na jaki mam wysłać wiadomość
			$settings = App__Ed__Model__Settings__Model::get('glowne')->data;
			$email = $settings->email;

			if(!empty($email))
			{
				App__Ed__Model__Email::send_email($email, $title, $message);
			}
		}
		
		
		
	}
}