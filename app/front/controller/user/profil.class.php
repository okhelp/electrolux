<?php

class App__Front__Controller__User__Profil extends Lib__Base__Ed_Controller
{
	public function action_edytuj()
	{
		$id_profile = isset($_GET['id']) ? (int)$_GET['id'] : 0;
		
		
		//pobieram informację o użytkowniku
		$user_data = !empty($id_profile) ? App__Ed__Model__Users::find_by_id($id_profile) : new App__Ed__Model__Users;
		
		if(!empty($_POST) && $_POST['zapisz'] == 'edytuj')
        {
            unset($_POST['zapisz']);


            //sprawdzam czy uzupełniono podstawowe pola formularza
            if (empty($_POST['name']) || empty($_POST['surname']) || empty($_POST['email']))
            {
                go_back('d|Dane nie zostały zapisane. Prosze uzupełnić wszystkie pola oznaczone *.');
            }

            //sprawdzam czy podany login jest wolny
            if (!empty($_POST['login']) && !empty(App__Ed__Model__Users::find_by_login($_POST['login'])))
            {
                go_back("d|Podany login jest już zajęty. Proszę wprowadzić inny");
            }


            //spawdzam czy wprowadzoo poprawny adres e-mail
            if (filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) === false)
            {
                go_back('d|Dane nie zostały zapisane. Proszę wprowadzić prawidłowy adres e-mail.');
            }

            //jeżeli jest hasło podane to zracam komunikat, że hasła nie są identyczne
            if (!empty($_POST['password']) && !empty($_POST['password']['new']) && !empty($_POST['password']['repeat']))
            {
                //wprowadzone hasła nie są identyczne, daję komunikat, jeżeli są ok to zamieniam klucz w tablicy na zakodowane hasło
                if ($_POST['password']['new'] != $_POST['password']['repeat'])
                {
                    go_back('d|Wprowadzone hasła muszą być identyczne. Dane w formularzu nie zostały zapisane');
                }
                else //koduję hasło
                {
                    $_POST['password'] = App__Ed__Model__Encryption::encode($_POST['password']['new']);
                }
            }
            else
            {
                unset($_POST['password']);
            }

            //avatar
            if (!empty($_POST['upload_1']))
            {
                $_POST['id_cimg'] = $_POST['upload_1'];
                unset($_POST['upload_1']);
                unset($_POST['avatar']);
            }

            //wybór działu
            if (!empty($_POST['selected_item']))
            {
                $_POST['id_up'] = (int)$_POST['selected_item'];
                unset($_POST['selected_item']);
            }

            //przynależność do serwisów
            if (!empty($_POST['service_access']))
            {
                $service_access = $_POST['service_access'];
                unset($_POST['service_access']);
            }
            else
            {
                $service_access = array();
            }
			
			//tworzę obiekt z danych z formularza
			foreach($_POST as $name => $value)
			{
				if(!empty($value))
				{
					$user_data->{$name} = $value;
				}
			}

            $user_data->type = 1; //pracownik, nie dział

			$user_data->save();
			
			//jeżeli jest to nowy użytkownik, to przypisuję mu uprwnienie admina
			if(!$id_profile)
			{
				$acl = new App__Ed__Model__Acl__Group__User;
				$acl->id_group = 1;
				$acl->id_user = $user_data->id;
				$acl->save();
			}

            //ustawiam przypisanie do serwisu
            if(!empty($service_access))
            {
                if(!empty($id_profile))
                {
                    $find_connected_service = App__Ed__Model__Users__Service::find(App__Ed__Model__Users__Service::where("id_user=" . $user_data->id));
                    if(!empty($find_connected_service))
                    {
                        foreach($find_connected_service as $connected_service)
                        {
                            $connected_service->delete();
                        }
                    }
                }
                

                foreach($service_access as $s_access)
                {
                    $service_connect = new App__Ed__Model__Users__Service;
                    $service_connect->id_service = $s_access;
                    $service_connect->id_user = $user_data->id;
                    $service_connect->save();
                }
            }
			
			go_back('g|Dane zostały zapisane. '
					. 'Jeżeli edytowałeś swoje konto, proszę się zalogować ponownie, '
					. 'aby zmiany były widoczne.',BASE_ED_URL . 'user/lista.html');	
			
		}
		
		$this->view->assign('user_data',$user_data);
		
		$this->template = 'user/profil/edytuj.tpl';
		$this->breadcrumb = array(
			'user/lista.html' => 'Pracownicy',
			'' => 'Edytuj profil'
		);
        
        $this->page_title = !empty($id_profile) ? 
				("Edycja pracownika: ".$user_data->name." ".$user_data->surname) : 
					"Dodaj użytkownika";
                
		
	}
	
	public function action_usun()
	{
		$id_user = !empty($_GET) && !empty($_GET['id']) ? (int)$_GET['id'] : 0;
		if(!$id_user)
		{
			go_back("d|Proszę podać id użytkownika.");
		}
		
		//pobieram obiekt użytkownika
		$user = App__Ed__Model__Users::find($id_user);
		
		if(empty($user))
		{
			go_back("d|Podany użytkownik nie został znaleziony w bazie danych.");
		}
		
		//usuwam użytkownika
		if($user->delete())
		{
			//usuwam przypisania do praw
			$user_acl = App__Ed__Model__Acl__Group__User::find(App__Ed__Model__Acl__Group__User::where("id_user = $id_user"));
			if(!empty($user_acl))
			{
				foreach($user_acl as $acl)
				{
					$acl->delete();
				}
			}
			
			
			go_back("g|Użytkownik został poprawnie usunięty");
		}
		else
		{
			go_back("d|Wystąpił błąd podczas usuwania użytkownika.");
		}
		
	}

    public function action_zobacz()
    {
        $id = !empty($_GET) && !empty($_GET['id']) ? (int)$_GET['id'] : 0;

        $user_data = App__Ed__Model__Users::find($id);

        $this->view->assign('user_data', $user_data);

        $this->page_title = "Profil użytkownika: " . $user_data->name . " " . $user_data->surname;
        $this->template = 'user/profil/zobacz.tpl';
        $this->breadcrumb = array(
            'user/lista.html' => 'Pracownicy',
            '' => $this->page_title
        );



    }
}