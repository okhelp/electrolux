<?php

class App__Front__Controller__User__Index extends Lib__Base__Ed_Controller
{
    public function action_ajax_pobierz_uzytkownika()
    {
        $return_list = [];

        if (!empty($_POST) && !empty($_POST['term']))
        {
            $term = $_POST['term'];
            $id_service = (int)Lib__Session::get('id_service');

            $sql = "SELECT u.*, '' as company
                FROM users u 
                INNER JOIN users_service us ON us.id_service = $id_service AND us.id_user = u.id
                WHERE u.type = 1 AND u.status = 1 AND 
                  (u.name LIKE '$term%') OR (u.surname LIKE '$term%') OR (CONCAT_WS(' ',u.name,u.surname) LIKE '$term%') OR (CONCAT_WS(' ',u.surname,u.name) LIKE '$term%')
                
                
                UNION DISTINCT 
                
                SELECT u.*, c.name as company
                FROM users u
                INNER JOIN users_service us ON us.id_service = $id_service AND us.id_user = u.id
                INNER JOIN company_users cu ON cu.id_user = u.id
                INNER JOIN company c ON c.id = cu.id_company
                WHERE u.type = 1 AND u.status = 1 AND (
                  (u.name LIKE '$term%') OR (u.surname LIKE '$term%') OR (CONCAT_WS(' ',u.name,u.surname) LIKE '$term%') OR (CONCAT_WS(' ',u.surname,u.name) LIKE '$term%')
                  OR (c.name LIKE '$term%')
                )
                
                ";

            $list = App__Ed__Model__Users::find_by_sql($sql);

            if (!empty($list))
            {
                foreach ($list as $l)
                {
                    //określam wartość
                    $value = $l->name . " " . $l->surname;

                    if (!empty($l->company))
                    {
                        $value .= " | " . $l->company;
                    }
                    else
                    {
                        $value .= " | PRACOWNIK";
                    }

                    $return_list[] = [
                        'id'    => $l->id,
                        'value' => $value,
                    ];
                }
            }
        }

        echo json_encode($return_list);
    }

    public function action_get_users_credentials()
    {
        if($this->session['id'] == 1)
        {
            $users_array = [];

            $users = App__Ed__Model__Users::find_by_sql("
                SELECT * 
                FROM users 
                WHERE id < 1001 and id > 985
            ");

            if(!empty($users))
            {
                foreach ($users as $user)
                {
                    $users_array[] = [
                        $user->login,
                        "{$user->name} {$user->surname}",
                        App__Ed__Model__Encryption::decode($user->password)
                    ];
                }
            }

            require_once BASE_PATH . 'lib/phpexcel.class.php';

            $doc = new PHPExcel();

            $doc->setActiveSheetIndex(0);

            $doc->getActiveSheet()->fromArray($users_array);

            $filename = 'klienci.xls';

            header('Content-Type: application/vnd.ms-excel');

            header('Content-Disposition: attachment;filename="' . $filename . '"');

            header('Cache-Control: max-age=0'); //no cache

            $objWriter = PHPExcel_IOFactory::createWriter($doc, 'Excel5');

            $objWriter->save('php://output');
        }
    }
}
