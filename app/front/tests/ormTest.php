<?php

class ormTest extends PHPUnit_Framework_TestCase 
{
 
    public function testFindAll()
    {
        $return = '';
        $find_all = App__Ed__Model__Phpunit::find('all');
        if(!empty($find_all))
        {
            $find_all = reset($find_all);
            $return = $find_all->text;
        }
        $this->assertEquals('test', $return);
    }
    
    public function testFindId()
    {
        $item = App__Ed__Model__Phpunit::find(1);
        $return = !empty($item->text) ? $item->text : '';
        $this->assertEquals('test', $return);
    }
    
    public function testFullQuery()
    {
        $return = '';
        $where = App__Ed__Model__Phpunit::where("id=1")->add("text='test'");
        $limit = App__Ed__Model__Phpunit::limit("0,1");
        $order = App__Ed__Model__Phpunit::order("id DESC");
        $find = App__Ed__Model__Phpunit::find($where,$limit,$order);
        if(!empty($find))
        {
            $find = reset($find);
            $return = $find->text;
        }
        $this->assertEquals('test', $return);
    }
    
    public function testInsert()
    {
        $item = new Phpunit;
        $item->text = "phpunit_test";
        $item->save();
        $this->assertEquals("phpunit_test", $item->text);
    }
    
    public function testUpdate()
    {
        $item = App__Ed__Model__Phpunit::find(App__Ed__Model__Phpunit::where("text='phpunit_test'"));
        $item = reset($item);
        $item->text = "phpunit_test_updated";
        $item->save();
        $this->assertEquals("phpunit_test_updated", $item->text);
    }
    
    public function testDelete()
    {
        $items = App__Ed__Model__Phpunit::find(App__Ed__Model__Phpunit::where("text='phpunit_test_updated'"));
        foreach($items as $item)
        {
            $item->delete();
        }
        $items = App__Ed__Model__Phpunit::find(App__Ed__Model__Phpunit::where("text='phpunit_test_updated'"));
    }
 
}
