<?php

class routerTest extends PHPUnit_Framework_TestCase
{
    public function testLoadClass()
    {
        $this->assertTrue(class_exists('Phpunit'));
    }
    
    public function testLoadMainPageClass()
    {
        $url = "http://tvo.tvokazje.pl/index.html";
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HEADER, true);    // we want headers
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_TIMEOUT,10);
        curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.7; rv:7.0.1) Gecko/20100101 Firefox/7.0.1');
        $output = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        $this->assertEquals('200', $httpcode);
    }
}