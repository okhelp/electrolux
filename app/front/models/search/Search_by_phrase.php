<?php
declare(strict_types=1);

namespace App\front\models\search;

use App\modules\products\front\models\Product_model;

class Search_by_phrase
{
    public static function search(string $search_phrase, int $count = 12, string $order = 'e_lux_desc', int $page = 1): array
    {
        $items_to_return = [];

        $where_products = '';
        $where_prizes = '';
        $order_condition = '';
        $offset = $count * ($page - 1);

        if(!empty($search_phrase))
        {
            $where_products = "
                AND ((MATCH(model) AGAINST ('{$search_phrase}*' IN BOOLEAN MODE)
                  OR MATCH(product_code) AGAINST ('{$search_phrase}*' IN BOOLEAN MODE)
                  OR MATCH(product.bar_code) AGAINST ('{$search_phrase}*' IN BOOLEAN MODE)
                  OR MATCH(product_lang.name) AGAINST ('{$search_phrase}*' IN BOOLEAN MODE)
                  OR MATCH(description) AGAINST ('{$search_phrase}*' IN BOOLEAN MODE)
                  OR MATCH(marketing_content) AGAINST ('{$search_phrase}*' IN BOOLEAN MODE)))
                  AND product.status = 1
            ";

            $where_prizes = "
                AND ((MATCH(prize_lang.name) AGAINST ('{$search_phrase}*' IN BOOLEAN MODE)
                  OR MATCH(sub_name) AGAINST ('{$search_phrase}*' IN BOOLEAN MODE)
                  OR MATCH(details) AGAINST ('{$search_phrase}*' IN BOOLEAN MODE)
                  OR MATCH(description) AGAINST ('{$search_phrase}*' IN BOOLEAN MODE)))
                  AND prize.status = 1
            ";
        }

        $order_condition = $order == 'e_lux_desc' ? ' price DESC ' : ' price ASC ';

        $products = Product_model::find_by_sql("
            SELECT 
                product.id, product.id AS product_id, 0 AS prize_id, product.extra_points, product.badges, product.cimg, 
                product_lang.name, product.points, product_lang.description, product.model, CONCAT('" . (BASE_URL . 'products/product.html?id=') . "', product.id) AS url,
                product.price AS price, '' AS sub_name, '' AS description
                FROM product
                JOIN product_lang ON product_lang.product_id=product.id
                WHERE product_lang.id_service={$_SESSION['id_service']} AND product_lang.id_lang={$_SESSION['id_lang']}
                {$where_products}
                  
                  UNION
                  
            SELECT
                prize.id, 0 AS product_id, prize.id AS prize_id, 0 AS extra_points, '' AS badges, 
                (SELECT id_cimg FROM prize_photos WHERE prize_photos.id_prize=prize.id LIMIT 1) AS cimg, 
                prize_lang.name AS name, 0 AS points, '' AS description, '' AS model, CONCAT('" . (BASE_URL . 'prizes/prize.html?id=') . "', prize.id) AS url,
                (SELECT MIN(prize_variant.points) from prize_variant WHERE id_prize=prize.id AND deleted_at IS NULL) AS price, prize_lang.sub_name, prize_lang.description
                FROM prize
                JOIN prize_lang ON prize_lang.prize_id=prize.id
                WHERE prize_lang.id_service={$_SESSION['id_service']} AND prize_lang.id_lang={$_SESSION['id_lang']}
                {$where_prizes}
                
                ORDER BY $order_condition
                LIMIT $count OFFSET $offset
        ");

        if(!empty($products))
        {
            foreach ($products as $product)
            {
                $items_to_return[$product->{Product_model::COLUMN_ID}] = $product;
            }
        }

        return $items_to_return;
    }
}