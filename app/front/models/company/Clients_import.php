<?php
declare(strict_types=1);

namespace App\front\models\company;

use App\ed\model\company\Add_supervisor_to_company;
use App\ed\model\company\Get_company_users;
use App\ed\model\mail\Email_queue_model;
use App\ed\model\mail\Send_email_by_queue;
use App\ed\model\users\Get_subordinate_companies;
use App\modules\products\front\interfaces\Runnable_interface;
use App__Ed__Model__Acl;
use App__Ed__Model__Company;
use App__Ed__Model__Company__Service;
use App__Ed__Model__Company__Users;
use App__Ed__Model__Email;
use App__Ed__Model__Encryption;
use App__Ed__Model__Users;
use App__Ed__Model__Users__Generate_password;
use App__Ed__Model__Users__Service;
use DateTime;
use PHPExcel;
use Smarty;

class Clients_import implements Runnable_interface
{
    const COLUMN_TRADER_NAME           = '0';
    const COLUMN_TRADER_EMAIL          = '1';
    const COLUMN_TRADER_TELEPHONE      = '2';
    const COLUMN_ID_ELECTROLUX         = '3';
    const COLUMN_COMPANY_NAME          = '4';
    const COLUMN_COMPANY_NIP           = '5';
    const COLUMN_INVITED_TO_ELECTROLUX = '6';
    const COLUMN_INVITED_TO_AEG        = '7';
    const COLUMN_CLIENT_NAME           = '8';
    const COLUMN_CLIENT_SURNAME        = '9';
    const COLUMN_CLIENT_EMAIL          = '10';
    const COLUMN_CLIENT_TELEPHONE      = '11';

    /** @var array */
    private $existing_companies;

    /** @var array */
    private $company_services;

    /**
     * @throws \PHPExcel_Reader_Exception
     */
    public function run()
    {
        $this->log('Rozpoczynam import');

        $this->log('Pobieranie pliku aktualizacji');

        $actualization_file = $this->get_actualization_file();

        $this->log('Pobrano plik aktualizacji');

        $this->log('Pobieram klientów istniejących w sytemie');

        $this->existing_companies = $this->get_existing_companies();

        $this->log('Zakończono pobieranie istniejących klientów w systemie');

        $this->log('Pobieram serwisy przypisane do klientów');

        $this->company_services = $this->get_company_services();

        $this->log('Zakończono pobieranie serwisów przypisanych do klientów');

        $this->log('Rozpoczynam aktualizację danych');

        $this->get_company_data($actualization_file);

        $this->log('Import został zakończony');
    }

    /**
     * @return PHPExcel
     * @throws \PHPExcel_Reader_Exception
     */
    private function get_actualization_file(): PHPExcel
    {
        require_once BASE_PATH . 'lib/phpexcel.class.php';

        return \PHPExcel_IOFactory::load(BASE_PATH . "data/uploads/actualization/clients_" . (new DateTime('now'))->format('Ymd') . '.xlsx');
    }

    private function get_existing_companies(): array
    {
        return Get_companies::get_by_nip();
    }

    private function get_company_services(): array
    {
        return Get_company_services::get_all();
    }

    private function get_company_data(PHPExcel $php_excel)
    {
        $sheet = $php_excel->getSheet(0);
        $highest_row = $sheet->getHighestRow();
        $highest_column = $sheet->getHighestColumn();

        for ($row = 6; $row <= $highest_row; $row++)
        {
            $row_data = $sheet->rangeToArray(
                'A' . $row . ':' . $highest_column . $row,
                null,
                true,
                false
            );

            //zakończ jeżeli nie ma danych
            if(empty($row_data[0]) || empty($row_data[0][0]))
            {
                break;
            }

            //zakończ jeżeli nie ma przypisania do żadnego z serwisów
            if(empty($row_data[0][6]) && empty($row_data[0][7]))
            {
                continue;
            }

            $trader = $this->get_trader($row_data[0]);

            $companies_assigned_to_trader = $this->get_assigned_companies_to_trader((int)$trader->id);

            $company = null;

            if (empty($this->existing_companies[$row_data[0][self::COLUMN_COMPANY_NIP]]) && !empty($row_data[0][self::COLUMN_COMPANY_NAME]) && !empty($row_data[0][self::COLUMN_COMPANY_NIP]))
            {
                $nip = intval($row_data[0][self::COLUMN_COMPANY_NIP]);

                if(empty($nip))
                {
                    continue;
                }

                $this->log('Dodaję firmę NIP = ' . $row_data[0][self::COLUMN_COMPANY_NIP]);
                $company = $this->add_company($row_data[0][self::COLUMN_COMPANY_NAME], (string)$nip);

                if(!empty($company))
                {
                    if(empty($companies_assigned_to_trader) || (!empty($companies_assigned_to_trader) && !in_array($company->id, $companies_assigned_to_trader)))
                    {
                        Add_supervisor_to_company::add((int)$trader->id, (int)$company->id);
                    }

                    $this->log('Dodano firmę NIP = ' . $row_data[0][self::COLUMN_COMPANY_NIP]);

//                    $password = App__Ed__Model__Users__Generate_password::run(8, 1, "without_similar_symbols");
                    $password = 'eokhaslo19';

                    $user_data = [
                        'login'             => $company->nip,
                        'password'          => App__Ed__Model__Encryption::encode($password),
                        'id_electrolux'     => empty($row_data[0][self::COLUMN_ID_ELECTROLUX]) ? '' : $row_data[0][self::COLUMN_ID_ELECTROLUX],
                        'name'              => empty($row_data[0][self::COLUMN_CLIENT_NAME]) ? $company->name : $row_data[0][self::COLUMN_CLIENT_NAME],
                        'surname'           => empty($row_data[0][self::COLUMN_CLIENT_SURNAME]) ? '' : $row_data[0][self::COLUMN_CLIENT_SURNAME],
                        'email'             => empty($row_data[0][self::COLUMN_CLIENT_EMAIL]) ? '' : $row_data[0][self::COLUMN_CLIENT_EMAIL],
                        'telephone'         => empty($row_data[0][self::COLUMN_CLIENT_TELEPHONE]) ? '' : $row_data[0][self::COLUMN_CLIENT_TELEPHONE],
                    ];

                    $user = $this->add_user_company((int)$company->id, $user_data);

                    if(!empty($user))
                    {
                        App__Ed__Model__Acl::add_user_to_group($user, 7);

                        if (!empty($user_data['email']))
                        {
                            $smarty = new Smarty();
                            $smarty->assign('login', $user_data['login']);
                            $smarty->assign('password', $password);

                            $content = $smarty->fetch(get_app_template_path() . '_emails/new_premium_user_welcome_mail.tpl');

                            Send_email_by_queue::send(
                                $user_data['email'],
                                'Electrolux od kuchni',
                                '',
                                $content
                            );

                            if(!empty($trader->email))
                            {
                                Send_email_by_queue::send(
                                    $trader->email,
                                    'Nowy klient - Electrolux od kuchni',
                                    '',
                                    $content
                                );
                            }
                        }

                        $this->log('Dodano użytkownika id = ' . $user);
                    }
                    else
                    {
                        $this->log('Nie udało się dodać użytkownika id = ' . $user);
                    }
                }
                else
                {
                    $this->log('Nie dodano firmy NIP = ' . $row_data[0][self::COLUMN_COMPANY_NIP]);
                }
            }
            else
            {
                $company = empty($this->existing_companies[$row_data[0][self::COLUMN_COMPANY_NIP]]) ? [] : $this->existing_companies[$row_data[0][self::COLUMN_COMPANY_NIP]];

                if(!empty($company))
                {

                    $user_data = [
                        'login'             => $company->nip,
                        'id_electrolux'     => empty($row_data[0][self::COLUMN_ID_ELECTROLUX]) ? '' : $row_data[0][self::COLUMN_ID_ELECTROLUX],
                        'email'             => empty($row_data[0][self::COLUMN_CLIENT_EMAIL]) ? '' : $row_data[0][self::COLUMN_CLIENT_EMAIL],
                        'telephone'         => empty($row_data[0][self::COLUMN_CLIENT_TELEPHONE]) ? '' : $row_data[0][self::COLUMN_CLIENT_TELEPHONE],
                    ];

                    $this->add_user_company((int)$company->id, $user_data);

                    $this->log('Pobrano z systemu firmę NIP = ' . $row_data[0][self::COLUMN_COMPANY_NIP]);
                }
                else
                {
                    $this->log('Nie udało się pobrać z systemu firmy NIP = ' . $row_data[0][self::COLUMN_COMPANY_NIP]);
                }
            }

            if(!empty($company) && !empty($row_data[0][self::COLUMN_INVITED_TO_ELECTROLUX]) && empty($this->company_services[$company->id][Get_company_services::ID_SERVICE_ELECTROLUX]))
            {
                $this->log('Dodanie do serwisu Electrolux firmy id = ' . $company->id);

                $this->add_service_to_company((int)$company->id, Get_company_services::ID_SERVICE_ELECTROLUX);

                $this->log('Dodano do serwisu Electrolux firmę id = ' . $company->id);

                $this->log('Dodanie użytkowników do serwisu Electrolux firmy id = ' . $company->id);

                $this->assign_service_to_users((int)$company->id, Get_company_services::ID_SERVICE_ELECTROLUX);

                $this->log('Dodano użytkowników do serwisu Electrolux firmę id = ' . $company->id);

            }

            if(!empty($company) && !empty($row_data[0][self::COLUMN_INVITED_TO_AEG]) && empty($this->company_services[$company->id][Get_company_services::ID_SERVICE_AEG]))
            {
                $this->log('Dodanie do serwisu AEG firmy id = ' . $company->id);

                $this->add_service_to_company((int)$company->id, Get_company_services::ID_SERVICE_AEG);

                $this->log('Dodano do serwisu AEG firmę id = ' . $company->id);

                $this->log('Dodanie użytkowników do serwisu AEG firmy id = ' . $company->id);

                $this->assign_service_to_users((int)$company->id, Get_company_services::ID_SERVICE_AEG);

                $this->log('Dodano użytkowników do serwisu AEG firmę id = ' . $company->id);
            }
        }
    }

    private function get_assigned_companies_to_trader(int $id_trader): array
    {
        $subordinate_companies_to_return = [];

        $subordinate_companies = Get_subordinate_companies::get([$id_trader]);

        if(!empty($subordinate_companies))
        {
            $subordinate_companies_to_return = $subordinate_companies;
        }

        return $subordinate_companies_to_return;
    }

    private function get_trader($row_data): ?App__Ed__Model__Users
    {
        $trader = null;

        $this->log('Poberanie handlowca email = ' . $row_data[self::COLUMN_TRADER_EMAIL]);

        $user = App__Ed__Model__Users::find_by_sql("
            SELECT *
            FROM users
            WHERE email = '{$row_data[self::COLUMN_TRADER_EMAIL]}'
        ");

        if(empty($user[0]))
        {
            $this->log('Nie odnaleziono handlowca email = ' . $row_data[self::COLUMN_TRADER_EMAIL]);

            $login = url_slug($row_data[self::COLUMN_TRADER_NAME]);

            $user_name_exploded = explode(' ', $row_data[self::COLUMN_TRADER_NAME]);

            $user_name = empty($user_name_exploded[1]) ? '' : $user_name_exploded[1];
            $user_surname = empty($user_name_exploded[0]) ? '' : $user_name_exploded[0];

            $password = App__Ed__Model__Users__Generate_password::run(8, 1, "without_similar_symbols");

            $user = new App__Ed__Model__Users;
            $user->id_up = 0;
            $user->id_province = 0;
            $user->id_city = 0;
            $user->id_cimg = 0;
            $user->id_position = 0;
            $user->type = 1;
            $user->login = $login;
            $user->password = App__Ed__Model__Encryption::encode($password);
            $user->name = $user_name;
            $user->surname = $user_surname;
            $user->gender = 0;
            $user->email = $row_data[self::COLUMN_TRADER_EMAIL];
            $user->telephone = empty($row_data[self::COLUMN_TRADER_TELEPHONE]) ? '' : $row_data[self::COLUMN_TRADER_TELEPHONE];
            $user->status = 1;
            $user->activation_token = '';
            $user->ts_change_password = (new DateTime('2010-01-01 00:00:00'))->format('Y-m-d H:i:s');

            $trader_saved = $user->save();

            if($trader_saved)
            {
                $users_service = new App__Ed__Model__Users__Service();
                $users_service->id_user = $user->id;
                $users_service->id_service = Get_company_services::ID_SERVICE_ELECTROLUX;
                $users_service->save();

                $users_service = new App__Ed__Model__Users__Service();
                $users_service->id_user = $user->id;
                $users_service->id_service = Get_company_services::ID_SERVICE_AEG;
                $users_service->save();

                App__Ed__Model__Acl::add_user_to_group($user->id, 6);

                if (!empty($user->email))
                {
                    $smarty = new Smarty();
                    $smarty->assign('login', $login);
                    $smarty->assign('password', $password);

                    Send_email_by_queue::send(
                        $user->email,
                        'Electrolux od kuchni',
                        '',
                        $smarty->fetch(get_app_template_path() . '_emails/new_trader_welcome_mail.tpl')
                    );
                }

                return $user;
            }
        }
        else
        {
            $this->log('Znaleziono handlowca email = ' . $row_data[self::COLUMN_TRADER_EMAIL]);

            $trader = $user[0];
        }

        return $trader;
    }

    private function add_company(string $company_name, string $company_nip): App__Ed__Model__Company
    {
        $company = new App__Ed__Model__Company();
        $company->id_parent = 0;
        $company->name = $company_name;
        $company->nip = $company_nip;
        $company->regon = '';
        $company->id_employee = 0;

        $company->save();

        return $company;
    }

    private function add_service_to_company(int $id_company, int $id_service): int
    {
        $company_service = new App__Ed__Model__Company__Service();
        $company_service->id_company = $id_company;
        $company_service->id_service = $id_service;

        return (int)$company_service->save();
    }

    private function assign_service_to_users(int $id_company, int $id_service)
    {
        foreach ($this->get_company_users($id_company) as $company_user)
        {
            $this->add_service_to_user((int)$company_user->id_user, $id_service);
        }
    }

    private function get_company_users(int $id_company): array
    {
        return Get_company_users::get($id_company);
    }

    private function add_service_to_user(int $id_user, int $id_service): int
    {
        $user_service = new App__Ed__Model__Users__Service();
        $user_service->id_user = $id_user;
        $user_service->id_service = $id_service;

        return (int)$user_service->save();
    }

    private function add_user_company(int $id_company, array $user_data): int
    {
        $find_user = App__Ed__Model__Users::find_by_login($user_data['login']);

        //dodawanie użytkownika
        if(empty($find_user))
        {
            $user = new App__Ed__Model__Users();
            $user->id_up = 0;
            $user->id_province = 0;
            $user->id_city = 0;
            $user->id_cimg = 0;
            $user->id_position = 0;
            $user->type = 1;
            $user->id_electrolux = $user_data['id_electrolux'];
            $user->login = $user_data['login'];
            $user->password = $user_data['password'];
            $user->name = $user_data['name'];
            $user->surname = $user_data['surname'];
            $user->gender = 0;
            $user->email = $user_data['email'];
            $user->telephone = $user_data['telephone'];
            $user->status = 1;
            $user->ts_change_password = (new DateTime('2010-01-01 00:00:00'))->format('Y-m-d H:i:s');

            if ($user->save())
            {
                $company_user = new App__Ed__Model__Company__Users();
                $company_user->id_company = $id_company;
                $company_user->id_user = $user->id;
                $company_user->role = 'admin';

                $company_user->save();

                return (int)$user->id;
            }
        }
        else //edytowanie
        {
            $user = $find_user;

            $user->id_electrolux = $user_data['id_electrolux'];
            $user->save();
        }

        return 0;
    }

    /**
     * @param string $log_message
     * @throws \Exception
     */
    private function log(string $log_message): void
    {
        $fp = fopen(BASE_PATH . 'data/log/clients_import_' . (new DateTime('now'))->format('Ymd') . '.txt', 'a');
        fwrite($fp, (new DateTime('now'))->format('Y-m-d H:i:s') . " - $log_message \n");
        fclose($fp);
    }
}