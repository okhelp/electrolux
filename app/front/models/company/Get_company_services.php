<?php
declare(strict_types=1);

namespace App\front\models\company;

use App__Ed__Model__Company__Service;

class Get_company_services
{
    const ID_SERVICE_ELECTROLUX = 5;
    const ID_SERVICE_AEG        = 6;

    public static function get_all(): array
    {
        $companies_to_return = [];

        $company_services = App__Ed__Model__Company__Service::all();

        if (!empty($company_services))
        {
            foreach ($company_services as $company_service)
            {
                $companies_to_return[$company_service->id_company][$company_service->id_service] = $company_service;
            }
        }

        return $companies_to_return;
    }

    public static function get(int $company_id): array
    {
        $services_to_return = [];

        $company_services = App__Ed__Model__Company__Service::find_by_sql("
            SELECT *
            FROM company_service
            WHERE id_company=$company_id
        ");

        if(!empty($company_services))
        {
            $services_to_return = array_map(function($company_service){ return $company_service->id_service; }, $company_services);
        }

        return $services_to_return;
    }
}