<?php
declare(strict_types=1);

namespace App\front\models\company;

use App__Ed__Model__Company;

class Get_companies
{
    public static function get(): array
    {
        $companies_to_return = [];

        $companies = App__Ed__Model__Company::find_by_sql(
            "
            SELECT *
            FROM company
        ");

        if (!empty($companies))
        {
            $companies_to_return = $companies;
        }

        return $companies_to_return;
    }

    public static function get_by_nip(): array
    {
        $companies_to_return = [];

        $companies = self::get();

        if (!empty($companies))
        {
            foreach ($companies as $company)
            {
                $companies_to_return[$company->nip] = $company;
            }
        }

        return $companies_to_return;
    }
}