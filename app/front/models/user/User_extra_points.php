<?php
declare(strict_types=1);

namespace App\front\models\user;

use App\ed\model\Company_users_points;
use App\ed\model\points\Get_users_points;
use Exception;

class User_extra_points
{
    public static function add(int $id_user, int $points): bool
    {
        $user_points = self::get_user_points($id_user);

        $new_user_points = is_null($user_points) ? new Company_users_points() : Company_users_points::find($user_points->{Company_users_points::COLUMN_ID});
        $new_user_points->{Company_users_points::COLUMN_EXTRA_POINTS} = $new_user_points->{Company_users_points::COLUMN_EXTRA_POINTS} + $points;
        $new_user_points->{Company_users_points::COLUMN_COMPANY_USER_ID} = (int)$id_user;
        \App__Ed__Model__Points_log::add($id_user, $points);
        return $new_user_points->save();
    }

    public static function sub(int $id_user, int $points): bool
    {
        $user_points = self::get_user_points($id_user);

        if (\App__Ed__Model__Points_extra_log::getPoints($id_user) < $points) {
            throw new Exception('Brak wystarczającej ilości punktów');
        }
        $new_user_points = is_null($user_points) ? new Company_users_points() : Company_users_points::find($user_points->{Company_users_points::COLUMN_ID});
        $new_user_points->{Company_users_points::COLUMN_EXTRA_POINTS} = $new_user_points->{Company_users_points::COLUMN_EXTRA_POINTS} - $points;
        $new_user_points->{Company_users_points::COLUMN_COMPANY_USER_ID} = (int)$id_user;
        \App__Ed__Model__Points_extra_log::add($id_user, -$points);
        return $new_user_points->save();
    }

    public static function get_user_points(int $user_id): ?Company_users_points
    {
        $company_user_points = Get_users_points::get([$user_id]);

        if (!empty($company_user_points[$user_id])) {
            return $company_user_points[$user_id];
        }

        return null;
    }
}