<?php
declare(strict_types=1);

namespace App\front\models\user;

use App__Ed__Model__Users;

class Get_user_by_login
{
    public static function get(string $user_login): App__Ed__Model__Users
    {
        $user_data = new App__Ed__Model__Users();

        $user = App__Ed__Model__Users::find_by_sql("
            SELECT *
            FROM users
            WHERE login='$user_login'
        ");

        if(!empty($user))
        {
            $user_data = $user[0];
        }

        return $user_data;
    }
}