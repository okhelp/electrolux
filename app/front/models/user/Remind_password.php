<?php
declare(strict_types=1);

namespace App\front\models\user;

use App\ed\model\mail\Send_email_by_queue;
use App__Ed__Model__Encryption;
use Exception;

class Remind_password
{
    /** @var string */
    private $login;

    /** @var string */
    private $email;

    /** @var \App__Ed__Model__Users */
    private $user_data;

    /**
     * Remind_password constructor.
     * @param string $login
     * @param string $email
     */
    public function __construct(string $login, string $email)
    {
        $this->login = $login;
        $this->email = $email;
        $this->user_data = Get_user_by_login::get($this->login);
    }

    /**
     * @throws Exception
     */
    public function remind()
    {
        if (!empty($this->user_data))
        {
            if ($this->user_data->email == $this->email)
            {
                Send_email_by_queue::send(
                    $this->email,
                    'Przypomnienie hasła',
                    'Przypomnienie hasła',
                    'Twoje hasło to: ' . App__Ed__Model__Encryption::decode($this->user_data->password)
                );
            }
            else
            {
                throw new Exception('Nieprawidłowe dane.');
            }
        }
        else
        {
            throw new Exception('Nie odnaleziono użytkownika.');
        }
    }


}