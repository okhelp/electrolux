<?php
declare(strict_types=1);

namespace App\front\models\user;

use App\ed\model\Company_users_points;
use App\ed\model\points\Get_users_points;
use Exception;

class User_points
{
    public static function add(int $id_user, int $points): bool
    {
        $user_points = self::get_user_points($id_user);

        $new_user_points = is_null($user_points) ? new Company_users_points() : Company_users_points::find($user_points->{Company_users_points::COLUMN_ID});
        $new_user_points->{Company_users_points::COLUMN_POINTS} = $new_user_points->{Company_users_points::COLUMN_POINTS} + $points;
        $new_user_points->{Company_users_points::COLUMN_COMPANY_USER_ID} = (int)$id_user;
        \App__Ed__Model__Points_log::add($id_user, $points);
        return $new_user_points->save();
    }

    public static function sub(int $id_user, int $points): bool
    {
        if (\App__Ed__Model__Points_log::getPoints($id_user) < $points) {
            throw new Exception('Brak wystarczającej ilości punktów');
        }

        $user_points = self::get_user_points($id_user);
        $new_user_points = is_null($user_points) ? new Company_users_points() : Company_users_points::find($user_points->{Company_users_points::COLUMN_ID});
        $new_user_points->{Company_users_points::COLUMN_POINTS} = $new_user_points->{Company_users_points::COLUMN_POINTS} - $points;
        $new_user_points->{Company_users_points::COLUMN_COMPANY_USER_ID} = (int)$id_user;
        \App__Ed__Model__Points_log::add($id_user, -$points);
        return $new_user_points->save();
    }

    /**
     * @param int $id_user
     * @param int $points
     * @return bool
     * @throws Exception
     */
    public static function sub_annual(int $id_user, int $points): bool
    {
        $user_points_annual = reset(\App\ed\model\points\Get_users_points::get_annual([$id_user]));

        if ($user_points_annual < $points) {
            throw new Exception('Brak wystarczającej ilości punktów rocznych');
        }

        $annual_points_log = new \App__Ed__Model__Annual_points_log();
        $annual_points_log->user_id = $id_user;
        $annual_points_log->points = $points;

        return $annual_points_log->save();
    }

    private static function get_user_points(int $user_id): ?Company_users_points
    {
        $company_user_points = Get_users_points::get([$user_id]);

        if (!empty($company_user_points[$user_id])) {
            return $company_user_points[$user_id];
        }

        return null;
    }
}