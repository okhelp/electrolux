<?php
declare(strict_types=1);

namespace App\front\models\user;

use Lib__Base__Model;

class Users_points_transfer_log extends Lib__Base__Model
{
    const COLUMN_ID                     = 'id';
    const COLUMN_ID_SENDER              = 'id_sender';
    const COLUMN_ID_RECEIVER            = 'id_receiver';
    const COLUMN_SENDER_POINTS_BEFORE   = 'sender_points_before';
    const COLUMN_RECEIVER_POINTS_BEFORE = 'receiver_points_before';
    const COLUMN_SENDER_POINTS_AFTER    = 'sender_points_after';
    const COLUMN_RECEIVER_POINTS_AFTER  = 'receiver_points_after';
    const COLUMN_CREATED_AT             = 'created_at';

    const TABLE_NAME = 'users_points_transfer_log';

    static $table_name = self::TABLE_NAME;
    static $primary_key = self::COLUMN_ID;
}