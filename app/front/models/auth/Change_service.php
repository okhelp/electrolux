<?php
declare(strict_types=1);

namespace App\front\models\auth;

use App__Ed__Model__Service;
use App__Ed__Model__Users__Service;
use Exception;

class Change_service
{
    /** @var int */
    private $id_service;

    /** @var int */
    private $id_user;

    /**
     * Change_service constructor.
     * @param int $id_service
     * @param int $id_user
     */
    public function __construct(int $id_service, int $id_user)
    {
        $this->id_service = $id_service;
        $this->id_user = $id_user;
    }

    /**
     * @throws Exception
     */
    public function change()
    {
        if(!$this->is_assigned_to_service() || !$this->change_active_service())
        {
            throw new Exception('Błąd zmiany serwisu');
        }
    }

    private function is_assigned_to_service(): bool
    {
        $is_assigned = App__Ed__Model__Users__Service::find_by_sql("
            SELECT *
            FROM users_service
            WHERE id_user={$this->id_user} AND id_service={$this->id_service}
        ");

        return (bool)!empty($is_assigned);
    }

    private function change_active_service(): bool
    {
        $find_service = App__Ed__Model__Service::find($this->id_service);
        if(!empty($find_service) && !empty($this->id_service))
        {
            App__Ed__Model__Service::change_service($this->id_service);

            return true;
        }
        else
        {
            return false;
        }
    }
}