<?php
declare(strict_types=1);

namespace App\front\models\auth;

use App__Ed__Model__Users__Service;

class Get_user_services
{
    public static function get(int $id_user): array
    {
        $services_to_return = [];

        $user_services = App__Ed__Model__Users__Service::find_by_sql("
            SELECT service.id, service.name
            FROM users_service
            JOIN service ON service.id=users_service.id_service
            WHERE id_user={$id_user}
        ");

        if(!empty($user_services))
        {
            foreach ($user_services as $user_service)
            {
                $services_to_return[$user_service->id] = $user_service;
            }
        }

        return $services_to_return;
    }
}