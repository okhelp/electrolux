<?php
declare(strict_types=1);

namespace App\front\models\auth;

use Lib__Session;

class Verify_user
{
    public static function is_logged(): bool
    {
        $id_user = (int)Lib__Session::get('id');
        return !empty($id_user);
    }

    public static function is_required_login(): bool
    {
        return !self::is_logged() && !self::is_public_url();
    }

    public static function is_public_url(): bool
    {
        $public_urls = [
            BASE_DIR,
            BASE_DIR . 'auth/change_password.html',
            BASE_DIR . 'auth/aktywacja.html?code=' . (empty($_REQUEST['code']) ? '' : urlencode($_REQUEST['code'])),
            BASE_DIR . 'products/start_import.html',
            BASE_DIR . 'clients/import.html',
        ];

        return in_array($_SERVER['REQUEST_URI'], $public_urls);
    }
}