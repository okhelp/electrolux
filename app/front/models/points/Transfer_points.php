<?php
declare(strict_types=1);

namespace App\front\models\points;

use App\ed\model\Company_users_points;
use App\ed\model\mail\Send_email_by_queue;
use App\ed\model\newsletter\Users_options;
use App\ed\model\points\Get_users_points;
use App\ed\model\users\Can_send_email;
use App__Ed__Model__Users;

class Transfer_points
{
    /** @var int */
    private $id_sender;

    /** @var int */
    private $id_receiver;

    /** @var int */
    private $points;

    /**
     * Transfer_points constructor.
     * @param int $id_sender
     * @param int $id_receiver
     * @param int $points
     */
    public function __construct(int $id_sender, int $id_receiver, int $points)
    {
        $this->id_sender = $id_sender;
        $this->id_receiver = $id_receiver;
        $this->points = $points;
    }

    public function transfer()
    {
        $users_e_lux = Get_users_points::get(array_merge([$this->id_receiver], [$this->id_sender]));

        $logged_user_points = empty($users_e_lux[$this->id_sender]) ? new Company_users_points() : Company_users_points::find($users_e_lux[$this->id_sender]->{Company_users_points::COLUMN_ID});
        $logged_user_points->{Company_users_points::COLUMN_COMPANY_USER_ID} = $this->id_sender;
        $logged_user_points->{Company_users_points::COLUMN_POINTS} = empty($logged_user_points->{Company_users_points::COLUMN_POINTS}) ? $this->points : ($logged_user_points->{Company_users_points::COLUMN_POINTS} - $this->points);

        $user_points = empty($users_e_lux[$this->id_receiver]) ? new Company_users_points() : Company_users_points::find($users_e_lux[$this->id_receiver]->{Company_users_points::COLUMN_ID});
        $user_points->{Company_users_points::COLUMN_COMPANY_USER_ID} = $this->id_receiver;
        $user_points->{Company_users_points::COLUMN_POINTS} = empty($user_points->{Company_users_points::COLUMN_POINTS}) ? $this->points : ($user_points->{Company_users_points::COLUMN_POINTS} + $this->points);

        $points_assigned = $user_points->save() && $logged_user_points->save();

        $user = App__Ed__Model__Users::find($this->id_sender);
        $recipient = App__Ed__Model__Users::find($this->id_receiver);
        $recipient_name = implode(' ', [$recipient->name, $recipient->surname]);

        if ($points_assigned) {
            Log_user_points_transfer::add(
                $this->id_sender,
                $this->id_receiver,
                empty($users_e_lux[$this->id_sender]) ? 0 : $users_e_lux[$this->id_sender]->points,
                empty($users_e_lux[$this->id_receiver]) ? 0 : $users_e_lux[$this->id_receiver]->points,
                $logged_user_points->{Company_users_points::COLUMN_POINTS},
                $user_points->{Company_users_points::COLUMN_POINTS}
            );
        }

        \App__Ed__Model__Points_log::add($user->id, $this->points * -1);
        \App__Ed__Model__Points_log::add($recipient->id, $this->points);
        $points = abs($this->points);

        if ($points_assigned && Can_send_email::verify($this->id_sender, Users_options::OPTION_SEND_POINTS_TRANSFER_NOTIFICATION)) {
            if ($this->points < 0) {
                Send_email_by_queue::send(
                    (string)$user->email,
                    'Odebranie e-luxów - Electrolux od kuchni',
                    'Odebranie e-luxów',
                    "Odebrałeś e-luxy współpracownikowi {$recipient_name}.<br>
Ilość odebranych e-luxów: {$points}<br>
Odebrane e-lux zostały z powrotem dodane do twojego salda.<br>
W razie jakikolwiek wątpliwości zapraszamy do kontaktu z swoim handlowcem."
                );
            } else {
                Send_email_by_queue::send(
                    (string)$user->email,
                    'Przekazanie e-luxów - Electrolux od kuchni',
                    'Przekazanie e-luxów',
                    "Udało się! Przekazałeś e-luxy, współpracownikowi {$recipient_name}.<br>
Ilość przekazanych e-luxów: {$this->points}<br>
W razie jakikolwiek wątpliwości zapraszamy do kontaktu z swoim handlowcem."
                );
            }

        }

        if ($points_assigned && Can_send_email::verify($this->id_sender, Users_options::OPTION_SEND_POINTS_TRANSFER_NOTIFICATION)) {
            if ($this->points < 0) {
                Send_email_by_queue::send(
                    (string)$recipient->email,
                    'Odebranie e-luxów - Electrolux od kuchni',
                    'Odebrano Ci e-luxy',
                    "Niestety użytkownik {$user->name} {$user->surname} odebrał Ci e-luxy.<br>
Ilość odebranych e-luxów: {$points}<br>"
                );
            } else {
                Send_email_by_queue::send(
                    (string)$recipient->email,
                    'Otrzymanie e-luxów - Electrolux od kuchni',
                    'Otrzymałeś nowe e-luxy',
                    "Użytkownik {$user->name} {$user->surname} przekazał Ci e-luxy.<br>
Ilość przekazanych e-luxów: {$this->points}<br>
Sprawdź na jakie nagrody już Cię stać."
                );
            }
        }

        return $points_assigned;
    }

}