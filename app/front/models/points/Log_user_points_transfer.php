<?php
declare(strict_types=1);

namespace App\front\models\points;

use App\front\models\user\Users_points_transfer_log;

class Log_user_points_transfer
{
    public static function add(int $id_sender, int $id_receiver, int $sender_points_before, int $receiver_points_before, int $sender_points_after, int $receiver_points_after): bool
    {
        $users_points_transfer_log = new Users_points_transfer_log();
        $users_points_transfer_log->{Users_points_transfer_log::COLUMN_ID_SENDER} = $id_sender;
        $users_points_transfer_log->{Users_points_transfer_log::COLUMN_ID_RECEIVER} = $id_receiver;
        $users_points_transfer_log->{Users_points_transfer_log::COLUMN_SENDER_POINTS_BEFORE} = $sender_points_before;
        $users_points_transfer_log->{Users_points_transfer_log::COLUMN_RECEIVER_POINTS_BEFORE} = $receiver_points_before;
        $users_points_transfer_log->{Users_points_transfer_log::COLUMN_SENDER_POINTS_AFTER} = $sender_points_after;
        $users_points_transfer_log->{Users_points_transfer_log::COLUMN_RECEIVER_POINTS_AFTER} = $receiver_points_after;

        return $users_points_transfer_log->save();
    }
}