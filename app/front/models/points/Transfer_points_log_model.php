<?php
declare(strict_types=1);

namespace App\front\models\points;

use Lib__Base__Model;

class Transfer_points_log_model extends Lib__Base__Model
{
    const TABLE_NAME = 'dashboard_points_transfer';

    const COLUMN_CREATED_AT      = 'created_at';
    const COLUMN_ID_SENDER       = 'id_sender';
    const COLUMN_ID_RECEIVER     = 'id_receiver';
    const COLUMN_SENT_POINTS     = 'sent_points';
    const COLUMN_RECEIVED_POINTS = 'received_points';
    const COLUMN_SENDER_NAME     = 'sender_name';
    const COLUMN_RECEIVER_NAME   = 'receiver_name';

    static $table_name = self::TABLE_NAME;
}