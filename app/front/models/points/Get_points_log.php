<?php
declare(strict_types=1);

namespace App\front\models\points;

use App\front\models\user\Users_points_transfer_log;

class Get_points_log
{
    public static function get(array $users_ids, bool $return_all = false): array
    {
        $points_log_to_return = [];

        $where_condition = '';
        if (!$return_all && !empty($users_ids))
        {
            $where_condition = " WHERE id_sender IN (" . implode(',', $users_ids) . ") OR id_receiver IN (" . implode(',', $users_ids) . ") ";
        }

        $points_log = Transfer_points_log_model::find_by_sql(
            "
                SELECT *
                FROM dashboard_points_transfer
                $where_condition
                ORDER BY created_at DESC
                LIMIT 10
            ");

        if (!empty($points_log))
        {
            $points_log_to_return = $points_log;
        }

        return $points_log_to_return;
    }

    public static function get_by_user(int $id_user)
    {
        return Users_points_transfer_log::find(
            Users_points_transfer_log::where("id_sender = $id_user OR id_receiver = $id_user")
        );
    }
}