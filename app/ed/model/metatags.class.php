<?php

class App__Ed__Model__Metatags
{
	public static function load($meta_data = array())
	{
		//pobieram ustawienia portalu
		$settings = App__Ed__Model__Settings__Model::get('glowne')->data;

		
		//tytuł strony
		if(empty($meta_data['page_title']))
		{
			$meta_data['page_title'] = $settings->site_meta_title;
		}
		
		//opis strony
		if(empty($meta_data['page_description']))
		{
			$meta_data['page_description'] = $settings->site_meta_description;
		}
		
		//słowa kluczowe
		if(empty($meta_data['page_keywords']))
		{
			$meta_data['page_keywords'] = $settings->site_meta_keywords;
		}
		
		//zdjęcie strony
		if(empty($meta_data['page_social_img']))
		{
			$social_cimg = $settings->site_social_logo;
			if(!empty($social_cimg))
			{
				$social_img = App__Ed__Model__Img::get_file(
					array('id_cimg' => $social_cimg, 'width' => 320, 'height' => 320)
				);

				//dopisuję datę modyfikacji pliku
				$social_path = str_replace(BASE_URL,BASE_PATH,$social_img);
				$social_path = str_replace('_uploads/','data/uploads/',$social_path);			
				$social_img .= "?" . filemtime($social_path);

				//dopisać id cimg
				$meta_data['page_social_img'] = $social_img;
			}
		}
		
		//ładowanie widoku
		$smarty = new Smarty;
		$smarty->assign('metatags',$meta_data);
		return $smarty->fetch('_partials/metatags.tpl');
	}
	
	
}
