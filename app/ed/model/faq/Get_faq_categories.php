<?php
declare(strict_types=1);

namespace App\ed\model\faq;

class Get_faq_categories
{
    public static function get(array $categories_ids)
    {
        $categories_to_return = [];

        if(!empty($categories_ids))
        {
            $categories = Faq_category_model::find_by_sql("
                SELECT *
                FROM faq_category
                JOIN faq_category_lang ON faq_category_lang.faq_category_id=faq_category.id
                WHERE faq_category.id IN (" . implode(",", $categories_ids) . ")
            ");

            if(!empty($categories))
            {
                foreach ($categories as $category)
                {
                    $categories_to_return[$category->{Faq_category_model::COLUMN_ID}] = $category;
                }
            }
        }

        return $categories_to_return;
    }
}