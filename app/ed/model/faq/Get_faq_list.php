<?php
declare(strict_types=1);

namespace App\ed\model\faq;

use App\ed\model\users\Get_by_ids;

class Get_faq_list
{
    public static function get(int $id_service, int $id_lang, bool $list_all = false): array
    {
        $faq_to_return = [];

        $status_condition = '';
        if (!$list_all)
        {
            $status_condition = 'AND status=' . Faq_model::STATUS_ACTIVE;
        }

        $faqs = Faq_model::find_by_sql(
            "
            SELECT *
            FROM faq
            JOIN faq_lang ON faq_lang.faq_id=faq.id
            WHERE faq_lang.id_service=$id_service 
                AND faq_lang.id_lang=$id_lang 
                $status_condition
                AND deleted_at IS NULL
        ");

        if (!empty($faqs))
        {
            $faq_ids = array_map(
                function($faq) {
                    return $faq->{Faq_model::COLUMN_ID};
                }, $faqs);

            $faq_creators_ids = array_map(
                function($faq) {
                    return $faq->{Faq_model::COLUMN_ID_CREATOR};
                }, $faqs);

            $faq_categories_ids = array_map(
                function($faq) {
                    return $faq->{Faq_model::COLUMN_ID_PARENT};
                }, $faqs);

            $users = Get_by_ids::get($faq_creators_ids);

            foreach ($faqs as $faq)
            {
                $faq->assign_attribute('user', $users[$faq->{Faq_model::COLUMN_ID_CREATOR}]);

                $faq_to_return[$faq->{Faq_model::COLUMN_ID_PARENT}]['questions'][] = $faq;
            }

            $faq_categories = Get_faq_categories::get($faq_categories_ids);

            foreach ($faq_to_return as $faq_category_id => &$faq_category)
            {
                $faq_category['category'] = empty($faq_categories[$faq_category_id]) ? [] : $faq_categories[$faq_category_id];
            }
        }

        return $faq_to_return;
    }
}