<?php
declare(strict_types=1);

namespace App\ed\model\faq;

use Lib__Base__Model;

class Faq_category_model extends Lib__Base__Model
{
    const COLUMN_ID              = 'id';
    const COLUMN_ID_PARENT       = 'id_parent';
    const COLUMN_CREATED_AT      = 'created_at';
    const COLUMN_UPDATED_AT      = 'updated_at';
    const COLUMN_FAQ_CATEGORY_ID = 'faq_category_id';
    const COLUMN_ID_LANG         = 'id_lang';
    const COLUMN_NAME            = 'name';
    const COLUMN_SEO_NAME        = 'seo_name';
    const COLUMN_ID_SERVICE      = 'id_service';

    const TABLE_NAME = 'faq_category';

    static $table_name = self::TABLE_NAME;
    static $primary_key = self::COLUMN_ID;
}