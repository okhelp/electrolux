<?php
declare(strict_types=1);

namespace App\ed\model\faq;

use Lib__Base__Model;

class Faq_model extends Lib__Base__Model
{
    const COLUMN_ID         = 'id';
    const COLUMN_ID_CREATOR = 'id_creator';
    const COLUMN_ID_PARENT  = 'id_parent';
    const COLUMN_STATUS     = 'status';
    const COLUMN_CREATED_AT = 'created_at';
    const COLUMN_UPDATED_AT = 'updated_at';
    const COLUMN_DELETED_AT = 'deleted_at';
    const COLUMN_FAQ_ID     = 'faq_id';
    const COLUMN_QUESTION   = 'question';
    const COLUMN_ANSWER     = 'answer';
    const COLUMN_ID_SERVICE = 'id_service';
    const COLUMN_ID_LANG    = 'id_lang';

    const STATUS_ACTIVE     = 1;
    const STATUS_NOT_ACTIVE = 0;

    const TABLE_NAME = 'faq';

    static $table_name = self::TABLE_NAME;
    static $primary_key = self::COLUMN_ID;
}