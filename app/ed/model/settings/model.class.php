<?php

class App__Ed__Model__Settings__Model
{

	public static function save($type, $data)
	{
		//pobieam zmienne systemowe
		$id_service = Lib__Session::get('id_service');
		$id_lang = Lib__Session::get('id_lang');

		//sprawdzam czy to jest edycja czy zapis
		$find_settings = self::get($type);

		$settings = !empty($find_settings) ? $find_settings : new App__Ed__Model__Settings;

		//przygotowuję wartość
		if (is_array($data))
		{
			$data = json_encode($data);
		}
		
		//ustawiam wartości
		$settings->id_service = $id_service;
		$settings->id_lang = $id_lang;
		$settings->type = $type;
		$settings->data = $data;

		return $settings->save();
	}

	public static function get($type)
	{
		//pobieam zmienne systemowe
		$id_service = Lib__Session::get('id_service');
		$id_lang = Lib__Session::get('id_lang');

		$settings = App__Ed__Model__Settings::find(
						App__Ed__Model__Settings::where("type='$type'")
								->add("id_lang = $id_lang")
								->add("id_service = $id_service")
		);

		//jeżeli jest ustawienie, parsuje je
		if (!empty($settings))
		{
			$settings = reset($settings);
			//jeżeli jest json to odkodowuję go
			if (json_decode($settings->data) != FALSE)
			{
	
				$settings->data = json_decode($settings->data);
			}
		}
		
		return $settings;
	}

}
