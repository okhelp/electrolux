<?php

class App__Ed__Model__Localization extends Lib__Base__Model
{
	static $table_name = 'localization';
	static $primary_key = 'id';
	static $cache = TRUE;
}