<?php

class App__Ed__Model__Tree
{

	public static function find_main_key($data, $id_element)
	{
		$return_data = array();


		foreach ($data as $key => $item)
		{
			if ($item['id'] == $id_element)
			{
				$id_el = !empty($item['id_parent']) ? $item['id_parent'] : $item['id'];


				$return_data[] = $item;

				if ($item['id_parent'] == 0)
				{
					break;
				}
				else
				{
				    $main_key = self::find_main_key($data, $id_el);
					$return_data[] = reset($main_key);
				}
			}
		}

		return $return_data;
	}

	public static function recursive_unset(&$array, $unwanted_key)
	{
		unset($array[$unwanted_key]);
		foreach ($array as &$value)
		{
			if (is_array($value))
			{
				recursive_unset($value, $unwanted_key);
			}
		}
	}

}
