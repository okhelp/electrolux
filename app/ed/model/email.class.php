<?php

class App__Ed__Model__Email
{
    public static function send_email($email_to, $subject, $email_content)
    {
        //$email_to = 'j.labudda@imset.it';
        //pobieram ustawienia
        $settings = App__Ed__Model__Settings__Model::get('email')->data;

        //ustanawiam połaczenie z serwerem i App__Ed__Model__SMTP oraz definiuję strukturę wysyłki wiadomości
        $phpmailer = new App__Ed__Model__PHPMailer;
        $phpmailer->IsSMTP();
        $phpmailer->Host = trim($settings->smtp);
        $phpmailer->SMTPDebug = 0;
        $phpmailer->SMTPAuth = true;
        $phpmailer->Port = trim($settings->port);
        $phpmailer->Username = trim($settings->username);
        $phpmailer->Password = trim($settings->password);
        $phpmailer->SetFrom(trim($settings->email), trim($settings->name));
        $phpmailer->IsHTML(true);
        $phpmailer->CharSet = "UTF-8";
        $phpmailer->AddAddress($email_to);
        $phpmailer->Subject = $subject;
        $phpmailer->Body = $email_content;

        return $phpmailer->Send();
    }
}
