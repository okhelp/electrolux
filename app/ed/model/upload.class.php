<?php

/**
 * Klasa odpowiedzialna za upload plików
 *
 * @author pawel
 */
class App__Ed__Model__Upload
{
    protected $time;
    protected $dir_path = BASE_PATH . "data/uploads/";
    protected $upload_type;
    protected $accept_extension = [
        'jpg', 'jpeg', 'gif', 'png', 'bmp', 'doc', 'docx', 'rtf', 'xls', 'xslx', 'ppt', 'pptx', 'pdf', 'zip', 'rar', 'gz', 'mp3', 'mp4', 'mov', 'avi', 'svg', 'txt', 'csv',
    ];
    protected $max_file_size = 10240; //wartość w kb

    public function __construct($upload_type, $max_file_size = null)
    {
        $this->time = time();
        $this->upload_type = $upload_type;

        //jeżeli typ do obrazek do struktury katalogów dodaje "soruces"
        if ($this->upload_type == 'images')
        {
            $dir_source = "sources/";
        }
        else
        {
            $dir_source = '';
        }

        $this->dir_path = $this->dir_path . $this->upload_type . '/' . $dir_source . date('Y', $this->time) . '/' . date('m', $this->time) . '/' . date('d', $this->time) . '/';

	    create_dirs($this->dir_path);

        if (!empty($max_file_size))
        {
            $this->max_file_size = $max_file_size;
        }
    }

    public function send_file($file, $return = 0)
    {
        //sprawdzam czy to jest multiupload
        if (is_array($file['name']))
        {
            $multi_return = [];

            foreach (array_keys($file['name']) as $multiple_key)
            {
                $multi_return[] = $this->send_file(
                    [
                        'name'     => $file['name'][$multiple_key],
                        'size'     => $file['size'][$multiple_key],
                        'tmp_name' => $file['tmp_name'][$multiple_key],
                    ], $return);
            }

            if (!empty($return))
            {
                return $multi_return;
            }
        }

        //sprawdzam czy plik nie posiada niebezpiecznych rozszerzeń
        if ($this->check_extension($file['name']))
        {
            //sprawdzam czy plik posiada prawidłowy rozmiar
            if ($this->check_file_size($file['size']))
            {
                //szyfrujemy nazwę pliku
                $file_name = $this->file_name_encode($file['name']);

                //wgrywam plik na dysk serwera
                if (move_uploaded_file($file['tmp_name'], $this->dir_path . $file_name))
                {
                    //dla uploadera wyłącznie plikow graficznych, tworzę id_cimg
                    if ($this->upload_type == 'images')
                    {
                        $id_cimg = App__Ed__Model__Img::create_id_cimg($this->dir_path . $file_name, ['id_cathegory' => 1]);
                        if (!empty($return))
                        {
                            return $id_cimg;
                        }
                        else
                        {
                            echo $id_cimg;
                        }
                    }
                    else //jeżeli to był plik zwracam jego położenie zakodowane
                    {
                        $uploaded_file_path = App__Ed__Model__Encryption::encode($this->dir_path . $file_name);
                        if (!empty($return))
                        {
                            return $uploaded_file_path;
                        }
                        else
                        {
                            echo $uploaded_file_path;
                        }
                    }
                }
            }
            else
            {
                if (!empty($return))
                {
                    return "Plik: {$file['name']} nie został wgrany, ponieważ jego rozmiar przekracza dopuszczalny.";
                }
                else
                {
                    echo "Plik: {$file['name']} nie został wgrany, ponieważ jego rozmiar przekracza dopuszczalny.";
                }
            }
        }
        else
        {
            if (!empty($return))
            {
                return "Plik: {$file['name']} nie został wgrany, ponieważ posiada nieprawidłowe rozszerzenie.";
            }
            else
            {
                echo "Plik: {$file['name']} nie został wgrany, ponieważ posiada nieprawidłowe rozszerzenie.";
            }
        }

    }

    private function check_extension($file_name)
    {
        $extenstion = $this->get_extension($file_name);

        return in_array($extenstion, $this->accept_extension) ? true : false;

    }

    private function get_extension($file_name)
    {
        $file_name = trim($file_name);
        $file_name = strtolower($file_name);
        $file_name_arr = explode('.', $file_name);
        $file_name_arr = array_filter($file_name_arr);
        $file_name_arr = array_reverse($file_name_arr);

        return $file_name_arr[0] ?? '';
    }

    private function check_file_size($size_b)
    {
        $size_kb = $size_b / 1000;

        return $size_kb > $this->max_file_size ? false : true;
    }

    private function file_name_encode($file_name)
    {
        $extension_file = $this->get_extension($file_name);

        $id_cookie = App__Ed__Model__Id_cookie::get();
        $generate_string = App__Ed__Model__Users__Generate_password::run(10, 1, "lower_case,upper_case,numbers,special_symbols");

        $hash = md5($extension_file . time() . $id_cookie . $this->time . $generate_string . $file_name);
        $hash = substr($hash, 0, 30);

        return $hash . ".$extension_file";
    }

    public function copy_remote_file($url, $file_type = 'image', $cathegory = 0)
    {
        $file_extension = '';

        //pobieram nazwę pliku
        if (!empty($url))
        {
            $url_arr = explode('/', $url);
            $url_arr = array_reverse($url_arr);
            $file_name = $url_arr[0];
            unset($url_arr);
        }

        //jeżeli nie ma rozszerzenia do daję .jpg
        if (!$this->check_extension($this->dir_path . $file_name))
        {
            $file_extension = ".jpg";
        }


        //jeżeli jest ten sam plik to pomijam
        $file_database = App__Ed__Model__Img::find_by_file_name(str_replace(BASE_PATH, '', $this->dir_path) . $file_name . $file_extension);

        if (!$file_database)
        {
            //kopiuje na serwer
            copy($url, $this->dir_path . $file_name . $file_extension);
	
            if ($file_type == 'image')
            {
                return App__Ed__Model__Img::create_id_cimg($this->dir_path . $file_name . $file_extension, ['id_cathegory' => $cathegory]);
            }
            else
            {
                return $this->dir_path . $file_name;
            }
        }
        else
        {
            return $file_database->id;
        }


    }

}
