<?php

class App__Ed__Model__Modules__Install
{

	protected $_module_description;
	protected $_id_module;

	protected $_module_tmp_dir;
	protected $_module_main_class_name;
	protected $_module_code_name;
	protected $_copied_files = array();

	public function __construct()
	{
		
	}

	public function get_tmp_module_path()
	{
		return BASE_PATH . 'data/tmp/modules/' . $this->_module_code_name . '/';
	}

    public function set_module_tmp_dir($module_tmp_dir)
    {
        $this->_module_tmp_dir = $module_tmp_dir;
    }


	public function execute()
	{
	    //pobieram nazwę głównej klasy modułu
        if($this->get_module_main_class_name())
        {
            $module_class_name = $this->_module_main_class_name;

            //ustawiam code_name modułu
            $this->_module_code_name = $module_class_name::CODE_NAME;

            //sprawdzam czy moduł jest już zainstalowany
            if(App__Ed__Model__Modules__Model::is_installed($this->_module_code_name))
            {
                //jeżeli jest katalog _copy, to kopiuję pliki zgodnie z ich architekturą, na koniec usuwam ten folder
                $this->_copy_dir_execute();

                //kopiuję pliki modułu
                $this->copy_module_files();

                //zapisuję dane do bazy
                $this->module_save_db();

                //jeżeli w głównej klasie jest metoda install(), odpalam ją
                if(method_exists($this->_module_code_name, 'install'))
                {
                    $module_class_name = $this->_module_code_name;
                    $module_class_name::install();
                }

            }
            else //jeżeli jest już zainstalowany robię wypad
            {
                go_back("d|Moduł jest już zainstalowany.");
            }
        }

        die;

	}

    private function module_save_db()
    {
        $module = new App__Ed__Model__Modules;
        $module->code_name = $this->_module_code_name;
        if(!empty($this->_copied_files))
        {
            $module->files = json_encode($this->_copied_files);
        }
        $module->status = 1; //domyślnie włączam moduł
        $module->not_remove = 0;

        if($module->save())
        {
            //przypisuję moduł do aktualnego serwisu
            $module_service = new App__Ed__Model__Modules__Service;
            $module_service->id_module = $module->id;
            $module_service->id_service = (int)Lib__Session::get('id_service');
            if(!$module_service->save())
            {
                throw new Exception("Wystąpił błąd podczas zapisu przypisania modułu do serwisu.");
            }
        }
        else
        {
            throw new Exception("Wystąpił błąd podczas zapisu informacji o module do bazy danych.");
        }
    }

    private function get_module_main_class_name()
    {
        $module_file_path = $this->_module_tmp_dir . 'index.class.php';
        if(file_exists($module_file_path))
        {
            $st = get_declared_classes();
            include $module_file_path;
            $this->_module_main_class_name = reset(array_values(array_diff_key(get_declared_classes(),$st)));
            return TRUE;
        }
        else
        {
            throw new Exception("Brak pliku z główną klasą modułu.");
        }

        return FALSE;
    }

    private function copy_module_files()
    {
        $iterator = new RecursiveIteratorIterator(new RecursiveDirectoryIterator(($this->_module_tmp_dir)));
        foreach ($iterator as $file)
        {
            if ($file->isDir())
            {
                continue;
            }

            //ustawiam ścieżki plików
            $from = $file->getPathname();
            $to = BASE_PATH . str_replace($this->_module_tmp_dir,'app/modules/' . $this->_module_code_name . '/', $from);

            copy_files_from_dir($from, $to);

            //zapisuję pliki, które zostały skopiowane
            $this->_copied_files[] = $to;
        }

        //po wszystkim usuwam katalog modułu
        remove_dir($this->_module_tmp_dir);
    }

    private function _copy_dir_execute()
    {
        $iterator = new RecursiveIteratorIterator(new RecursiveDirectoryIterator(($this->_module_tmp_dir . '_copy')));
        foreach ($iterator as $file)
        {
            if ($file->isDir())
            {
                continue;
            }

            //ustawiam ścieżki plików
            $from = $file->getPathname();
            $to = BASE_PATH . str_replace($this->_module_tmp_dir.'_copy/','', $from);
            copy_files_from_dir($from, $to);
        }

        //po wszystkim usuwam katalog _copy
        remove_dir($this->_module_tmp_dir . '_copy');
    }


	public function extract_zip($file_path)
	{
		$module_tmp_dir = BASE_PATH . 'data/tmp/modules/';
		
		//jeżeli nie ma katalogu głównego, tworzę go
		if(!is_dir($module_tmp_dir))
		{
			create_dirs($module_tmp_dir);
		}
		
		$zip = new ZipArchive;
		$zip->open($file_path);
		
		$main_dir_name = str_replace('/','',$zip->getNameIndex(0));
		
		// rozpakowuję pliki
		$zip->extractTo($module_tmp_dir);
		
		//ustawiam code_name tak jak nazwa katalogu
		$this->set_module_tmp_dir($module_tmp_dir . $main_dir_name . '/');
		
		//usuwam plik zip
		unlink($file_path);
		
		return TRUE;
	}
}
