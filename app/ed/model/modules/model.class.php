<?php

class App__Ed__Model__Modules__Model
{

	public static function is_enabled($code_name)
	{
		$id_service = (int)Lib__Session::get('id_service');
		$enabled = FALSE;
		$module = App__Ed__Model__Modules::find(
			App__Ed__Model__Modules::where("code_name = '$code_name'")
				->add("status = 1"),
			App__Ed__Model__Modules::join("INNER JOIN modules_service ON modules_service.id_module = modules.id")
		);
		if (!empty($module))
		{
			$enabled = TRUE;
		}

		return $enabled;
	}

	public static function is_installed($code_name)
	{
		$installed = FALSE;
		$module = App__Ed__Model__Modules::find(
						App__Ed__Model__Modules::where("code_name = '$code_name'")
		);
		if (!empty($module))
		{
			$installed = TRUE;
		}

		return $installed;
	}

	public static function enable_module($code_name)
	{
		$module = App__Ed__Model__Modules::find_by_code_name($code_name);
		if (!empty($module))
		{
			$module->status = 1;
			$module->save();
		}
	}

	public static function remove_module($id_module)
	{
		
		if(!is_fisto_admin())
		{
			go_back("d|Brak uprawnień do usuwania");
		}
		
		//pobieram obiekt modułu
		$module = App__Ed__Model__Modules::find($id_module);

		if (empty($module))
		{
			$msg = "d|" . lang("Moduł nie został znaleziony.");
			go_back($msg);
		}

		//pobieram wpisy menu
		$module_menu = App__Ed__Model__Modules__Ed_menu::find(
						App__Ed__Model__Modules__Ed_menu::where("id_module = $id_module")
		);

		if (!empty($module_menu))
		{
			foreach ($module_menu as $menu_item)
			{
				$menu_item->delete();
			}
		}

		//usuwam listę plików która została wgrana przez instalator
		if (!empty($module->files))
		{
			$files = json_decode($module->files);

			foreach ($files as $file)
			{
				$file = BASE_PATH . $file;

				if (is_file($file))
				{
					unlink($file);
				}
			}
		}

		//uruchamiam skrypt usuwający moduł
		$module_core_class_path = BASE_PATH . "data/modules/" . $code_name . $module->code_name . '/';

		if (is_dir($module_core_class_path))
		{
			if (is_file($module_core_class_path . 'uninstall.php'))
			{
				//załączam klasę odinstalowania
				require_once $module_core_class_path . 'uninstall.php';
				$class_name = ucfirst($module->code_name) . '_Uninstall';

				//spprawdzam czy jest prawidłowo zdefiniowana klasa
				if (!class_exists($class_name))
				{
					throw new Exception("Brak klasy: $class_name");
				}

				//tworzę obiekt to usunięcia modulu
				$uninstall = new $class_name;

				//sprawdzam czy jest prawidłowo zdefiniowana metoda do rozpoczęcia
				if (!method_exists($uninstall, 'execute'))
				{
					throw new Exception("Brak zdefiniowanej metody execute() w $class_name");
				}

				//wykouję odinstalowanie
				$uninstall->execute();
			}

			//usuwam folder z ustawieniami systemowymi
			remove_dir($module_core_class_path);
		}

		//usuwam moduł z bazy
		$module->delete();

		$msg = "g|" . lang("Moduł został poprawnie usunięty.");
		go_back($msg);
	}
	
	public static function get_type_modules($type)
	{
		$where = App__Ed__Model__Modules::where("code_name LIKE '$type%'")->add("status = 1");
		return App__Ed__Model__Modules::find($where);
	}

    public function get_id_module($code_name)
    {
        $module = App__Ed__Model__Modules::find_by_code_name($code_name);
        if(!empty($module))
        {
            return $module->id;
        }
        else
        {
            throw new Exception("Moduł $code_name nie jest zainstalowany.");
        }
    }

}
