<?php

class Errors__List 
{
    public static function get_list($type='no_resolve')
    {
        
        $status = $type=='no_resolve' ? 0 : 1;
        $where = Errors::where("status=$status");
        $order = Errors::order("last_time DESC");
        $list = Errors::find($where,$order);

        return !empty($list) ? $list : array();
    }
}