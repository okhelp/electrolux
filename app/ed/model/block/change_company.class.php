<?php

class App__Ed__Model__Block__Change_Company
{
	public function execute()
	{
	    if (isset($_GET['type_worker'])) {
            $id_user = (int)Lib__Session::get('id');
            $user = App__Ed__Model__Users::find($id_user);
	        if (intval($_GET['type_worker']) === 1) {
	            $user->ts_change_company = date('Y-m-d H:i:s');
                $user->save();
            } elseif (isset($_GET['company'], $_GET['address'])) {
	            $company = new App__Ed__Model__Company();
	            foreach ($_GET['company'] as $key => $companyData) {
                    $company->{$key} = $companyData;
                }
                $company->ts = date('Y-m-d H:i:s');
                if ($company->save()) {

                    $companyUsers = App__Ed__Model__Company__Users::find_by_sql("SELECT * FROM company_users WHERE id_user=$user->id");
                    $companyUsers = reset($companyUsers);
                    if (!$companyUsers) {
                        $companyUsers = new App__Ed__Model__Company__Users();
                        $companyUsers->id_user = $user->id;
                        $companyUsers->role = 'user';
                        $companyUsers->created_at = date('Y-m-d H:i:s');
                    } else {
                        $companyUsers = App__Ed__Model__Company__Users::find($companyUsers->id);
                        $companyUsers->updated_at = date('Y-m-d H:i:s');
                    }
                    $companyUsers->id_company = $company->id;
                    if ($companyUsers->save()) {

                        $companyAddress = new App__Ed__Model__Company__Address();
                        foreach ($_GET['address'] as $key => $companyAddressData) {
                            $companyAddress->{$key} = $companyAddressData;
                        }
                        $companyAddress->id_company = $company->id;
                        $companyAddress->id_user = $user->id;
                        $companyAddress->is_main = 1;
                        $companyAddress->public = 0;
                        $companyAddress->ts = date('Y-m-d H:i:s');
                        if ($companyAddress->save()) {

                            $user->type_worker = $_GET['type_worker'];
                            $user->ts_change_company = date('Y-m-d H:i:s');
                            if ($user->save()) {
                                go_back('g|Dane zostały zapisane poprawnie', '/');
                            }
                        }
                    }
                }
            }
        }
	    $smarty = new Smarty;
		return $smarty->fetch("block/change_company.tpl");
	}
}
