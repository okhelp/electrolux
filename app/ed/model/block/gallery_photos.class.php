<?php

use App\modules\gallery\ed\model\Gallery_model;

class App__Ed__Model__Block__Gallery_Photos
{
	public function __construct()
	{
		
	}
	
	public function execute($params)
	{ 
		$photos_list = array();

        if (!empty($params['id']))
        {
            $id_gallery = (int)$params['id'];

            $gallery = Gallery_model::find($id_gallery);
            if (!empty($gallery))
            {
                $photos_list = $gallery->get_photos();

                if ($photos_list['total'] > 0)
                {
                    $photos_list = $photos_list['list'];
                }
            }
        }
		
		//ustawiam wygląd
		if(!isset($params['template']))
		{
			throw new Exception("Proszę podać parametr template= w gallery_photos.");
		}
		
		//zwracam wygląd
		$smarty = new Smarty;
		$smarty->assign('photos_list',$photos_list);
		return $smarty->fetch($params['template']);
	}
}