<?php

class App__Ed__Model__Block__Name_And_Surname
{
	public function execute()
	{
	    if (isset($_GET['name'], $_GET['surname'])) {
            $id_user = (int)Lib__Session::get('id');
	        $user = App__Ed__Model__Users::find($id_user);
            $user->name = $_GET['name'];
            $user->surname = $_GET['surname'];
            $user->save();
            Lib__Session::set(['name' => $user->name, 'surname' => $user->surname]);
            go_back('g|Dane zostały zapisane poprawnie','/');
        }
	    $smarty = new Smarty;
		return $smarty->fetch("block/name_and_surname.tpl");
	}
}
