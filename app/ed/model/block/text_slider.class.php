<?php

class App__Ed__Model__Block__Text_Slider
{
	public function __construct()
	{
		
	}
	
	public function execute($params)
	{ 
		$slider_data = array();
		
		$slider = App__Ed__Model__Text_Slider::find(
					App__Ed__Model__Text_Slider::where('id = ' . $params['id'])
						->add("status = 1")
				);
		
		if(!empty($slider))
		{
			$slider = reset($slider);
			$slider_data = $slider->get_positions();
		}
		
		
		//ustawiam wygląd
		if(!isset($params['template']))
		{
			throw new Exception("Proszę podać parametr template= w gallery.");
		}
		
		//zwracam wygląd
		$smarty = new Smarty;
		$smarty->assign('slider',$slider_data);
		return $smarty->fetch($params['template']);
	}
}