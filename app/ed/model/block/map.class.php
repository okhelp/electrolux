<?php

class App__Ed__Model__Block__Map
{
	public function __construct()
	{
		
	}
	
	public function execute($params)
	{ 
		$localizations = App__Ed__Model__Localization::find(
			App__Ed__Model__Localization::where("status = 1")
		);
		
		//zwracam wygląd
		$smarty = new Smarty;
		$smarty->assign('localizations',$localizations);
		return $smarty->fetch(BASE_PATH . 'data/js/open_maps/open_map.tpl');
	}
}