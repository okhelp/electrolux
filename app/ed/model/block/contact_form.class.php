<?php

use App\ed\model\company\Get_by_user_id;
use App\ed\model\company\Get_company_supervisor;

class App__Ed__Model__Block__Contact_form
{

	public function __construct()
	{
        $company = Get_by_user_id::get((int)$_SESSION['id']);
        $supervisor = Get_company_supervisor::get((int)$company->id);

        $this->supervisor = $supervisor;
	}

	public function execute()
	{
	    $smarty = new Smarty;
        $smarty->assign('supervisor', $this->supervisor);
		return $smarty->fetch("block/contact_form.tpl");
	}

}
