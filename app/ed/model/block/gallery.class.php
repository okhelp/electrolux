<?php

class App__Ed__Model__Block__Gallery
{
	public function __construct()
	{
		
	}
	
	public function execute($params)
	{ 
		$list = array();
		
		$gallery_list = App__Ed__Model__Files::find(
			App__Ed__Model__Files::where("status = 1"),
			App__Ed__Model__Files::order("name ASC")
		);
		
		if(!empty($gallery_list))
		{
			foreach($gallery_list as $gallery)
			{
				$list[] = array(
					'data' => $gallery,
					'photos' => $gallery->get_photos()
				);
			}
		}
		
		//ustawiam wygląd
		if(!isset($params['template']))
		{
			throw new Exception("Proszę podać parametr template= w gallery.");
		}
		
		//zwracam wygląd
		$smarty = new Smarty;
		$smarty->assign('gallery_list',$list);
		return $smarty->fetch($params['template']);
	}
}