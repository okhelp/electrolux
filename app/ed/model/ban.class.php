<?php

/**
 * Klasa odpowiedzialna za blokowanie użytkowników
 * @author Paweł Otłowski
 */
class App__Ed__Model__Ban extends Lib__Base__Model
{
	static $table_name = 'ban';
	static $primary_key = 'id';
	
	static $user_data = array();
	
	/**
	 * pobieranie informacji o użytkowniku
	 * @return type array
	 */
	public static function get_user_data()
	{
		return array(
			'ip' => $_SERVER['REMOTE_ADDR'],
			'host' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
			'id_cookie' => App__Ed__Model__Id_cookie::get()
		);
	}
	
	public static function is_user_banned($user_data=NULL)
	{
		$user_data = $user_data ? $user_data : self::get_user_data();
		
		//buduję zapytanie where
		$where_conditions = array();
		foreach($user_data as $k=>$v)
		{
			$where_conditions[] = "$k='$v'";
		}
		$where = App__Ed__Model__Ban::where(implode(' OR ', $where_conditions));
		
		$where->add("date_stop IS NOT NULL");
		$where->add("date_stop >= '".date('Y-m-d')."'");	
		$ban = App__Ed__Model__Ban::find($where);
	
		return !empty($ban) ? TRUE : FALSE;
	}
}
