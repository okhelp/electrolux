<?php

class App__Ed__Model__Acl__Group extends Lib__Base__Model
{
	static $table_name = "acl_group";
	static $belongs_to = array(array('acl_group_user', 'class_name' => 'App__Ed__Model__Acl__Group__User'));
}