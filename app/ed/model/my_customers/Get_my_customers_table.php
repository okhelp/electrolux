<?php
declare(strict_types=1);

namespace App\ed\model\my_customers;

use App\ed\model\company\Get_company_users;
use App\ed\model\users\Get_subordinate_companies;
use App\front\models\company\Get_companies;
use App__Ed__Model__Acl;

class Get_my_customers_table
{
    public static function run(int $user_id)
    {
        $usersID = [];

        if (!empty($user_id)) {
            $subordinate_companies = (App__Ed__Model__Acl::has_group('admin-electrolux', 0) || App__Ed__Model__Acl::has_group('premium-user', 0)) ?
                array_map(function($company){ return $company->id; }, Get_companies::get()) : Get_subordinate_companies::get([$user_id]);

            if (!empty($subordinate_companies)) {
                $subordinates_companies_users_ids = Get_company_users::get_by_ids(array_map(function($company){ return $company; }, $subordinate_companies));

                foreach ($subordinates_companies_users_ids as $id_company => $subordinate_company_users_ids) {
                    foreach ($subordinate_company_users_ids as $subordinate_user) {
                        $usersID[] = $subordinate_user->id_user;
                    }
                }
            }
        }

        return $usersID;
    }
}