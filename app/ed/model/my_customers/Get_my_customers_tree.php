<?php
declare(strict_types=1);

namespace App\ed\model\my_customers;

use App\ed\model\company\Get_by_id;
use App\ed\model\company\Get_company_users;
use App\ed\model\points\Get_users_points;
use App\ed\model\users\Get_subordinate_companies;
use App\front\models\company\Get_companies;
use App__Ed__Model__Acl;

class Get_my_customers_tree
{
    public static function run(int $user_id)
    {
        $customers_to_return = [];

        if(!empty($user_id))
        {
            $subordinate_companies = (App__Ed__Model__Acl::has_group('admin-electrolux', 0) || App__Ed__Model__Acl::has_group('premium-user', 0)) ?
                array_map(function($company){ return $company->id; }, Get_companies::get()) : Get_subordinate_companies::get([$user_id]);

            if(!empty($subordinate_companies))
            {
                $company_with_users = [];

                $companies = Get_by_id::get($subordinate_companies);
                $subordinates_companies_users_ids = Get_company_users::get_by_ids(array_map(function($company){ return $company; }, $subordinate_companies));

                foreach ($companies as $company)
                {
                    $company_with_users[$company->id] = [
                        'id' => $company->id,
                        'text' => $company->name,
                        'type' => 3,
                        'children' => [],
                    ];
                }

                foreach ($subordinates_companies_users_ids as $id_company => $subordinate_company_users_ids)
                {
                    foreach ($subordinate_company_users_ids as $subordinate_user)
                    {
                        //pobieram ilość punktów
                        $user_points = \App__Ed__Model__Points_log::getPoints($subordinate_user->id_user);
                        $company_with_users[$subordinate_user->id_company]['children'][] = [
                            'id' => $subordinate_user->id_user . '_user',
                            'text' => sprintf(
                                '%s %s | ELUX: %s | <span class="click_show_profile" data-href="%s">zobacz profil</span>',
                                $subordinate_user->user->name, $subordinate_user->user->surname, $user_points, BASE_ED_URL . "my_customers/profile/zobacz.html?id=" . $subordinate_user->id_user),
                            'type' => $subordinate_user->role == 'user' ? 1 : 2,
                        ];

                    }
                }

                if(!empty($company_with_users))
                {
                    foreach ($company_with_users as $company) {
                        $customers_to_return[] = $company;
                    }
                }
            }
        }

        return $customers_to_return;
    }
}