<?php

class App__Ed__Model__Product_package_media extends Lib__Base__Model
{
	static $table_name = 'product_package_media';
	static $primary_key = 'id';
	static $cache = TRUE;
	
	public static function get_images($productID)
	{
		$imgs = App__Ed__Model__Img::find(
            App__Ed__Model__Product_package_media::join("JOIN product_package_media ON img.id=product_package_media.id_cimg"),
		    App__Ed__Model__Product_package_media::where("id_product=$productID")
        );

		return array_map(function ($img) {
		    return (object) [
		        'id_cimg' => $img->id,
		        'type' => 'picture',
                'link' => BASE_URL . $img->file_name,
            ];
        }, $imgs);
	}
}