<?php
declare(strict_types=1);

namespace App\ed\model\company;

use App__Ed__Model__Company;

class Get_by_id
{
    public static function get(array $companies_ids): array
    {
        $companies_to_return = [];

        $companies = App__Ed__Model__Company::find_by_sql(
            "
            SELECT *
            FROM company
            WHERE id IN (" . implode(',', $companies_ids) . ")
        ");

        if(!empty($companies))
        {
            foreach ($companies as $company)
            {
                $companies_to_return[$company->id] = $company;
            }
        }

        return $companies_to_return;
    }
}