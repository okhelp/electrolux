<?php
declare(strict_types=1);

namespace App\ed\model\company;

use App__Ed__Model__Company;
use App__Ed__Model__Company__Users;

class Get_by_user_id
{
    public static function get(int $user_id): App__Ed__Model__Company
    {
        $company_to_return = new App__Ed__Model__Company();

        $company_users = App__Ed__Model__Company__Users::find_by_sql("
            SELECT *
            FROM company_users
            WHERE id_user=$user_id
        ");

        if(!empty($company_users))
        {
            $user_company = $company_users[0];

            $company = Get_by_id::get([$user_company->id_company]);

            if(!empty($company[$user_company->id_company]))
            {
                $company_to_return = $company[$user_company->id_company];
                $company_to_return->assign_attribute('user', $user_company);
            }
        }

        return $company_to_return;
    }
}