<?php
declare(strict_types=1);

namespace App\ed\model\company;

use App__Ed__Model__Company__Address;

class Get_company_adresses
{
    public static function get(int $company_id)
    {
        $adresses_to_return = [];

        if (!empty($company_id))
        {
            $adresses = App__Ed__Model__Company__Address::find_by_sql(
                "
                SELECT *
                FROM company_address
                WHERE id_company=$company_id
                AND deleted_at IS NULL
                ORDER BY public DESC
            ");

            if (!empty($adresses))
            {
                $adresses_to_return = $adresses;
            }
        }

        return $adresses_to_return;
    }

    public static function get_as_array(int $company_id): array
    {
        $addresses_as_array = [];

        $addresses = self::get($company_id);

        if (!empty($addresses))
        {
            foreach ($addresses as $address)
            {
                $addresses_as_array[$address->id] = $address->to_array();
            }
        }

        return $addresses_as_array;
    }
}