<?php
declare(strict_types=1);

namespace App\ed\model\company;

use App__Ed__Model__Company_supervisor;

class Add_supervisor_to_company
{
    public static function add(int $id_supervisor, int $id_company): bool
    {
        $company_superviors = new App__Ed__Model__Company_supervisor();
        $company_superviors->{App__Ed__Model__Company_supervisor::COLUMN_ID_COMPANY} = $id_company;
        $company_superviors->{App__Ed__Model__Company_supervisor::COLUMN_ID_SUPERVISOR} = $id_supervisor;

        $assigned = $company_superviors->save();

        return $assigned;
    }
}