<?php
declare(strict_types=1);

namespace App\ed\model\company;

use App__Ed__Model__Users;

class Get_company_supervisor
{
    public static function get(int $id_company): ?App__Ed__Model__Users
    {
        $supervisor_to_return = null;

        $supervisor = App__Ed__Model__Users::find_by_sql("
            SELECT users.*
            FROM company_supervisor
            JOIN users ON company_supervisor.id_supervisor=users.id
            WHERE company_supervisor.id_company=$id_company
        ");

        if(!empty($supervisor))
        {
            $supervisor_to_return = $supervisor[0];
        }

        return $supervisor_to_return;
    }
}