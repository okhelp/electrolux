<?php
declare(strict_types=1);

namespace App\ed\model\company;

use App\ed\model\users\Get_by_ids;
use App__Ed__Model__Company__Users;

class Get_company_users
{
    public static function get_by_users_ids(int $id_company, bool $all = false): array
    {
        $users_to_return = [];

        $users = self::get($id_company, $all);

        if(!empty($users))
        {
            foreach ($users as $user)
            {
                $users_to_return[$user->id_user] = $user;
            }
        }

        return $users_to_return;
    }

    public static function get(int $id_company, bool $all = false): array
    {
        $users_to_return = [];

        if (!empty($id_company))
        {
            $company_users = self::get_by_ids([$id_company], $all);

            $company_users = !empty($company_users[$id_company]) ? $company_users[$id_company] : [];

            if (!empty($company_users))
            {
                $company_users_ids = array_map(
                    function($company_user) {
                        return $company_user->id_user;
                    }, $company_users);

                $users = Get_by_ids::get($company_users_ids);

                foreach ($company_users as $company_user)
                {
                    $company_user->assign_attribute('user', $users[$company_user->id_user]);
                }

                $users_to_return = $company_users;
            }
        }

        return $users_to_return;
    }

    public static function get_by_ids(array $companies_ids, bool $all = false)
    {
        $data_to_return = [];

        if (!empty($companies_ids))
        {
            $with_deleted_users_condition = $all ? '' : 'AND deleted_at IS NULL';

            $company_users = App__Ed__Model__Company__Users::find_by_sql(
                "
                SELECT *
                FROM company_users
                WHERE id_company IN (" . implode(',', $companies_ids) . ")
                    $with_deleted_users_condition
            ");

            if (!empty($company_users))
            {
                $company_users_ids = array_map(
                    function($company_user) {
                        return $company_user->id_user;
                    },
                    $company_users);

                $users = Get_by_ids::get($company_users_ids);

                foreach ($company_users as $company_user)
                {
                    $company_user->assign_attribute('user', $users[$company_user->id_user]);
                    $data_to_return[$company_user->id_company][] = $company_user;
                }
            }
        }

        return $data_to_return;
    }
}