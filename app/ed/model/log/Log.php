<?php
declare(strict_types=1);

namespace App\ed\model\log;

use DateTime;

class Log
{
    /** @var string */
    private $file_path;

    /**
     * Log constructor.
     * @param $file_path
     */
    public function __construct(string $file_path)
    {
        $this->file_path = $file_path;
    }

    /**
     * @param string $text
     * @throws \Exception
     */
    public function add(string $text)
    {
        $file = fopen($this->file_path, 'a');
        $log_added = fwrite($file, (new DateTime('now'))->format('Y-m-d H:i:s') . " - $text \n");
        fclose($file);

        if(!(bool)$log_added)
        {
            throw new Exception('Nie udało się zapisać logu.');
        }
    }
}