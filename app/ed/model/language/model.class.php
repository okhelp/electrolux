<?php

class App__Ed__Model__Language__Model
{

	public static function set_lang($id_lang)
	{
		//pobieram obiekt języka
		$lang_instance = App__Ed__Model__Language::find($id_lang);
		if (!empty($lang_instance))
		{
			Lib__Session::set('id_lang', $id_lang);
			Lib__Session::set('lang_code', $lang_instance->code);
			Lib__Session::set('lang_date', get_last_modification_dir(
							self::get_folder_path($lang_instance->code))
			);
		}
	}

	public static function get_lang_files($lang_code, $only_name = 1)
	{
		$file_list = array();

		//ustawiam katalog językowy
		$lang_dir = self::get_folder_path($lang_code);

		//jeżeli nie ma katalogu - tworzę go
		self::generate_default_file($lang_dir);

		//pobieram rekursywnie wszystkie pliki z katalogu
		$Directory = new RecursiveDirectoryIterator($lang_dir);
		$Iterator = new RecursiveIteratorIterator($Directory);
		$Regex = new RegexIterator($Iterator, '/^.+\.php$/i', RecursiveRegexIterator::GET_MATCH);

		foreach ($Regex as $files)
		{
			foreach ($files as $file)
			{
				if (!$only_name)
				{
					$file_list[] = $file;
				}
				else
				{
					$file_list[] = self::get_file_name($file);
				}
			}
		}

		return $file_list;
	}

	public static function get_folder_path($lang_code)
	{
		return BASE_PATH . "languages/$lang_code/";
	}

	public static function get_file_name($file_path)
	{
		$file_path = explode('/', $file_path);
		return end($file_path);
	}

	public static function find_lang_data()
	{
		//szukam we wszystkich plikach systemowych lang(string);
	}

	public static function generate_default_file($lang_path)
	{
		//funkcja działa jak nie ma takiego katalogu językowego
		if (!is_dir($lang_path))
		{
			//tworzę katalog
			create_dirs($lang_path);

			//pobieram ścieżkę domyślnego języka (przypisanego do serwisu)
			$default_id_lang = App__Ed__Model__Service::find(Lib__Session::get('id_service'))->id_lang;
			$default_lang = App__Ed__Model__Language::find($default_id_lang);
			$default_lang_path = self::get_folder_path($default_lang->code);

			//kopiuję pliki do nowego katalogu
			copy_files_from_dir($default_lang_path, $lang_path);
			rmdir($lang_path . '/' . $default_lang->code);
			
			//czyszczę pliki
			$Directory = new RecursiveDirectoryIterator($lang_path);
			$Iterator = new RecursiveIteratorIterator($Directory);
			$Regex = new RegexIterator($Iterator, '/^.+\.php$/i', RecursiveRegexIterator::GET_MATCH);

			foreach ($Regex as $files)
			{
				foreach ($files as $file)
				{
					file_put_contents($file, '');
				}
			}
		}

		return;
	}

	public static function get_all_files($lang_code)
	{
		//pobieram ścieżkę języka
		$lang_dir = self::get_folder_path($lang_code);

		//określam tablicę wynikową
		$found_files = array();

		//pobieram rekursywnie wszystkie pliki z katalogu
		$Directory = new RecursiveDirectoryIterator($lang_dir);
		$Iterator = new RecursiveIteratorIterator($Directory);
		$Regex = new RegexIterator($Iterator, '/^.+\.php$/i', RecursiveRegexIterator::GET_MATCH);

		foreach ($Regex as $files)
		{
			foreach ($files as $file)
			{
				$found_files[] = $file;
			}
		}

		return $found_files;
	}

	public static function get_all_new_lang_strings($lang_code)
	{
		//określam tablicę wynikową
		$found_string = array();

		//pobieram wszystkie pliki językowe
		foreach (self::get_all_files($lang_code) as $file)
		{
			require $file;
		}

		//jeżeli nie zdefiniowanego langa, to definiuję pustą tablicę
		if (!isset($lang) && empty($lang))
		{
			$lang = array();
			$lang_keys = array();
		}
		else
		{
			//przygotowuję tablicę z samymi kluczami
			$lang_keys = array_keys($lang);
		}

		//przelatuję kontrolery, helpery, modele w poszukiwaniu lang(string)
		$dirs_to_find = array(
			'controller', 'helpers', 'model', 'templates'
		);

		foreach ($dirs_to_find as $dir_name)
		{
			//template mają inny znacznik - |lang:

			$Directory = new RecursiveDirectoryIterator(BASE_PATH . $dir_name . '/');
			$It = new RecursiveIteratorIterator($Directory);
			
			if($dir_name != 'templates')
			{
				$Regex2 = new RegexIterator($It, '/\.php$/i');
			}
			else
			{
				$Regex2 = new RegexIterator($It, '/\.tpl$/i');
			}
			
			foreach ($Regex2 as $file)
			{
				$open_file = file_get_contents($file);

				if ($dir_name == 'templates')
				{
					$regex = "/{('|\")(.*)('|\")|lang}/";
				}
				else
				{
					$regex = "/lang\((\"|')(.*?)('|\")\)/";
				}

				preg_match_all($regex, $open_file, $m);
				
				if (!empty($m[2]))
				{
					foreach ($m[2] as $word)
					{
						$word = trim($word);
						$word = base64_encode($word);
						if (!empty($word) && !in_array($word, $lang_keys))
						{
							$found_string[$word] = '';
						}
					}
				}

				unset($m);
			}
		}

		return $found_string;
	}

	public static function actualize_session_translate($id_lang)
	{
		//definiuję tablicę wynikową
		$translate_list = array();

		//pobieram kod języka
		$lang_instance = App__Ed__Model__Language::find($id_lang);

		//idę dalej, jak mam język w bazie danych
		if (!empty($lang_instance))
		{
			//pobieram listę plików
			$files_list = self::get_all_files($lang_instance->code);

			//ładuję pliki językowe
			if (!empty($files_list))
			{
				foreach ($files_list as $file)
				{
					require $file;
				}

				//jeżeli zmienna $lang istnieje to jadę dalej
				if (isset($lang) && !empty($lang))
				{
					Lib__Session::set("lang", $lang);
				}
			}
		}

		return $translate_list;
	}

	public static function get_translated_value($string)
	{
		$translated = $string;

		if (Lib__Session::get('lang'))
		{
			$string = base64_encode($string);
			if (!empty(Lib__Session::get('lang')[$string]))
			{
				$translated = Lib__Session::get('lang')[$string];
			}
		}

		return $translated;
	}

}
