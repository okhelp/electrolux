<?php

class App__Ed__Model__Block_menu__Model
{
	public static function get($type)
	{
		$block_list = array();
		
		$list = App__Ed__Model__Block_menu::find(
			App__Ed__Model__Block_menu::where("type='$type'"),
			App__Ed__Model__Block_menu::order("`order` ASC")
		);
		if(!empty($list))
		{
			foreach($list as $k => $v)
			{
				$block_list[$k] = array(
					'id' => $v->id,
					'id_parent' => $v->id_parent,
					'name' => $v->name,
					'url' => $v->url
				);
			}
		}
		
		return !empty($block_list) ? generate_tree($block_list,0,-1,'all') : array();
	}
	
	public static function get_tree($type)
	{
		$smarty = new Smarty;
        $smarty->assign('list',self::get($type));
		$smarty->assign('type',$type);
		
		$template_path = is_ed() ? 
			"block_menu/_partials/tree.tpl" : "_partials/block_menu/tree.tpl";
		
        return $smarty->fetch($template_path);
	}
	
	public static function get_new_order($type)
	{
		$order = 1;

		//odczytuję dane z bazy
		$block_menu = App__Ed__Model__Block_menu::find(
			App__Ed__Model__Block_menu::where("type='$type'"),
			App__Ed__Model__Block_menu::order('`order` DESC'),
			App__Ed__Model__Block_menu::limit('0,1')
		);
		
		
		if(!empty($block_menu))
		{
			$block_menu = reset($block_menu);
			$order = $block_menu->order + 1;
		}

		return $order;
	}
}