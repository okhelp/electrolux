<?php
declare(strict_types=1);

namespace App\ed\model\messages;

use App__Ed__Model__Messages;

class Get_new_messages_count
{
    public static function get(int $id_user): int
    {
        $messages_count = 0;

        $messages = App__Ed__Model__Messages::find_by_sql("
            SELECT COUNT(*) AS messages_count
            FROM messages
            WHERE id_to=$id_user AND view_to=0 AND trash_to=0
        ");

        if(!empty($messages[0]->messages_count))
        {
            $messages_count = (int)$messages[0]->messages_count;
        }

        return $messages_count;
    }
}