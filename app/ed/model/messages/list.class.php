<?php

class App__Ed__Model__Messages__List
{

    protected $_page = 1;
    protected $_per_page = 10;

    protected $_folder;

    protected $_where = array();

    public function __construct()
    {

    }

    public function set_page($page)
    {
        $this->_page = $page;
    }

    public function set_per_page($per_page)
    {
        $this->_per_page = $per_page;
    }

    public function set_folder($folder_name)
    {
        $this->_folder = $folder_name;
    }

    public function set_where($where)
    {
        $this->_where[] = $where;
    }

    public function execute()
    {
        //zmasowane akcje na liście wiadomości
        if(!empty($_POST) && !empty($_POST['message_ids']))
        {
            $mass_action = new App__Ed__Model__Messages__List__Mass_Action($this->_folder, $_POST);
            $mass_action->execute();
        }

        $sql[] = "SELECT SQL_CALC_FOUND_ROWS * FROM messages";

        //nie pokazuję wiadomości, które są usunięte
        $trash_field_name = ($this->_folder == 'inbox') ? "trash_to" : "trash_from";
        $this->_where[] = "$trash_field_name = 0";

        //buduje zapytanie
        $sql[] = "WHERE " . implode(" AND ", $this->_where);
        $sql[] = "ORDER by ts DESC";

        $per_page = $this->_per_page;
        $page = $this->_page - 1;

        if(empty($page))
        {
            $limit = "LIMIT 0," . $per_page;
        }
        else
        {
            $offset = $per_page * $page;
            $limit = "LIMIT $offset,$per_page";
        }

        $sql[] = $limit;

        $list = App__Ed__Model__Messages::find_by_sql(implode(" ", $sql));

        $total_rows = App__Ed__Model__Messages::find_by_sql("SELECT FOUND_ROWS() as total");
        $total_rows = reset($total_rows);
        $total_rows = $total_rows->total;

        return [
          'list' => $list,
          'total' => $total_rows
        ];
    }
}