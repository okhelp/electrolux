<?php

class App__Ed__Model__Messages__List__Mass_Action
{
    protected $_folder;
    protected $_message_ids;
    protected $_action_type;

    public function __construct($folder_name, $post_data)
    {
        $this->_folder = $folder_name;
        $this->_message_ids = $post_data['message_ids'];
        $this->_action_type = $post_data['action_type'];
    }

    public function execute()
    {
        $this->{$this->_action_type}();
    }

    private function get_messages_list()
    {
        $messages_ids = implode(",", $this->_message_ids);
        return App__Ed__Model__Messages::find(App__Ed__Model__Messages::where("id IN ($messages_ids)"));
    }

    private function delete()
    {
        //pobieram listę wiadomości
        $messages_list = $this->get_messages_list();

        if(!empty($messages_list))
        {
            //określam nazwę pola
            $field_name = ($this->_folder == 'inbox') ? "trash_to" : "trash_from";

            foreach($messages_list as $message)
            {
                $message->{$field_name} = 1;
                $message->save();
            }

            $msg = count($this->_message_ids) > 0 ? "Wiadomości zostały usunięte." : "Wiadomość została usunięta.";
            go_back("g|$msg");

        }
    }

    private function important()
    {
        //pobieram listę wiadomości
        $messages_list = $this->get_messages_list();

        if(!empty($messages_list))
        {
            //określam nazwę pola
            $field_name = ($this->_folder == 'inbox') ? "important_to" : "important_from";

            foreach($messages_list as $message)
            {
                $message->{$field_name} = 1;
                $message->save();
            }

            $msg = count($this->_message_ids) > 0 ? "Wiadomości zostały oznaczone jako ważne." : "Wiadomość została oznaczona jako ważna.";
            go_back("g|$msg");
        }

    }

    private function read()
    {
        //pobieram listę wiadomości
        $messages_list = $this->get_messages_list();

        if(!empty($messages_list))
        {
            //określam nazwę pola
            $field_name = ($this->_folder == 'inbox') ? "view_to" : "view_from";

            foreach($messages_list as $message)
            {
                $message->{$field_name} = 1;
                $message->save();
            }

            $msg = count($this->_message_ids) > 0 ? "Wiadomości zostały oznaczone jako przeczytanie." : "Wiadomość została oznaczona jako przeczytane.";
            go_back("g|$msg");
        }
    }

    private function unread()
    {
        //pobieram listę wiadomości
        $messages_list = $this->get_messages_list();

        if(!empty($messages_list))
        {
            //określam nazwę pola
            $field_name = ($this->_folder == 'inbox') ? "view_to" : "view_from";

            foreach($messages_list as $message)
            {
                $message->{$field_name} = 0;
                $message->save();
            }

            $msg = count($this->_message_ids) > 0 ? "Wiadomości zostały oznaczone jako nieprzeczytanie." : "Wiadomość została oznaczona jako nieprzeczytane.";
            go_back("g|$msg");
        }
    }

}