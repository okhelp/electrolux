<?php

class App__Ed__Model__Messages__Model
{
    public static function send($id_from, $id_to, $subject='', $description, $attachments=array())
    {
        $return_data = array();

        if(empty($id_to))
        {
            $return_data['error'] = "Proszę wybrać adresata wiadomości";
        }

        if(empty($subject))
        {
            $return_data['error'] = "Proszę podać temat wiadomości";
        }

        if(empty($description))
        {
            $return_data['error'] = "Proszę podać treść wiadomości";
        }

        if(empty($return_data))
        {
            //jeżeli są załaczniki to robię ich upload
            $uploaded_attachments = array();
            if(!empty($attachments) && !empty($attachments['file']['name'][0]))
            {
                $upload = new App__Ed__Model__Upload('files');
                $uploaded_attachments = $upload->send_file($attachments['file'], 1);;
            }
            $uploaded_attachments = json_encode($uploaded_attachments);

            //zapis wiadomości
            $message = new App__Ed__Model__Messages;
            $message->id_from = $id_from;
            $message->id_to = $id_to;
            $message->subject = $subject;
            $message->description = $description;
            $message->attachments = $uploaded_attachments;
            if($message->save())
            {
                $return_data['success'] = "Wiadomość została poprawnie wysłana";
            }
            else
            {
                $return_data['error'] = "Wystąpił nieoczekiwany błąd przy wysyłce wiadomości. Wiadomość nie została wysłana. Proszę spróbować ponownie.";
            }
        }

        return $return_data;
    }

    public static function decode_hash($hash)
    {
        $hash = urldecode(App__Ed__Model__Encryption::decode($hash));
        $hash = explode("|", $hash);
        return [
            'id_message' => $hash[0],
            'id_from' => $hash[1],
            'id_to' => $hash[2],
            'folder' => $hash[3]
        ];
    }

    public static function prepare_text_to_replay($id_message)
    {

    }
    
}