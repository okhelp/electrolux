<?php

namespace App\ed\model;

use Lib__Base__Model;

class Sale_model extends Lib__Base__Model
{
    const COLUMN_ID = 'id';
    const COLUMN_ID_PRODUCT = 'id_product';
    const COLUMN_INVOICE_NUMBER = 'invoice_number';
    const COLUMN_PRICE = 'price';
    const COLUMN_COUNT = 'count';
    const COLUMN_POINTS = 'points';
    const COLUMN_ID_CUSTOMER = 'id_customer';
    const COLUMN_STATUS = 'status';
    const COLUMN_ANNUAL = 'annual';
    const COLUMN_CREATED_AT = 'created_at';
    const COLUMN_UPDATED_AT = 'updated_at';

    const TABLE_NAME = 'sale';

    const STATUS_VERIFICATION = 0;
    const STATUS_REJECTED = 1;
    const STATUS_CONFIRMED = 2;

    static $table_name = self::TABLE_NAME;
    static $primary_key = self::COLUMN_ID;
}