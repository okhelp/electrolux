<?php

class App__Ed__Model__Language extends Lib__Base__Model
{
	static $table_name = 'language';
	static $primary_key = 'id';
	static $cache = TRUE;
}