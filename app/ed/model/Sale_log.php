<?php
declare(strict_types=1);

namespace App\ed\model;

use Lib__Base__Model;

class Sale_log extends Lib__Base__Model
{
    const COLUMN_ID                 = 'id';
    const COLUMN_ID_PRODUCT         = 'id_product';
    const COLUMN_PRODUCT_MODEL      = 'product_model';
    const COLUMN_COUNT              = 'count';
    const COLUMN_POINTS_BEFORE      = 'points_before';
    const COLUMN_POINTS_AFTER       = 'points_after';
    const COLUMN_CONFIRMING_USER_ID = 'confirming_user_id';
    const COLUMN_CREATED_AT         = 'created_at';

    const TABLE_NAME = 'sale_log';

    static $table_name = self::TABLE_NAME;
    static $primary_key = self::COLUMN_ID;
}