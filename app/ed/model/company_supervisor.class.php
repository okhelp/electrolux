<?php

class App__Ed__Model__Company_supervisor extends Lib__Base__Model
{
    const COLUMN_ID = 'id';
    const COLUMN_ID_SUPERVISOR = 'id_supervisor';
    const COLUMN_ID_COMPANY = 'id_company';
    const COLUMN_CREATED_AT = 'created_at';
    const COLUMN_UPDATED_AT = 'updated_at';

    const TABLE_NAME = 'company_supervisor';

	static $table_name = self::TABLE_NAME;
	static $primary_key = self::COLUMN_ID;
}
