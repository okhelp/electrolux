<?php

class App__Ed__Model__Memcache_log extends Lib__Base__Model
{
    static $table = "memcache_log";
    static $primary_key = 'id';
    static $cache = TRUE;
}