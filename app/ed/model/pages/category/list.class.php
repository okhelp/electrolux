<?php

class App__Ed__Model__Pages__Category__List
{
	public static function get_list($id_parent=0, $params=array('type' => 'active', 'order' => 'name desc'))
    {
        $sort = NULL;
		$where = App__Ed__Model__Pages__Category::where("id_parent=$id_parent");
        
        if(!empty($params['type']) && $params['type'] == 'active')
        {
            $where->add("status=1");
        }
        
        //ustawiam parametr where
        if(!empty($params['where']))
        {
            foreach($params['where'] as $p_where)
            {
                $where->add($p_where);
            }
        }
        
        if(!empty($params['order']))
        {
            $sort = App__Ed__Model__Pages__Category::order($params['order']);
        }
        
        return App__Ed__Model__Pages__Category::find($where,$sort);
    }
}