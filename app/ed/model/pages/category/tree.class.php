<?php
class App__Ed__Model__Pages__Category__Tree
{
    static $cache = FALSE;

    public static function get_ed_tree($start_id_parent = 0, $status, $input_name, $checked_value=NULL,$multi_value=0)
    {
        $smarty = new Smarty;
        $smarty->assign('list',self::get_list($start_id_parent, $status));
        $smarty->assign('input_name',$input_name);
        $smarty->assign('checked_value',self::prepare_checked_value($checked_value));
        $smarty->assign('multi_value',$multi_value);
        return $smarty->fetch("pages/categories/_partials/tree.tpl");
    }

    private static function prepare_checked_value($checked_value)
    {
        //jeżeli jest to obiekt Product__Pages__Category to wydobywam do tablicy
        if($checked_value instanceof Product__Pages__Category)
        {
            $values = array();
            foreach($checked_value as $value)
            {
                $values[] = $value->id_category;
            }
            $checked_value = $values;
        }
        return $checked_value;
    }

    public static function get_list(
        $start_id_parent = 0, $status = 1, $end_id_parent = -1, $mode = 'without_pages',
        $tree = 1
    )
    {
        $where =  App__Ed__Model__Pages__Category::where();

        // -1 oznacza wszystkie możliwe statusy
        if($status != -1)
        {
            $where->add("status=$status");
        }

        $order =  App__Ed__Model__Pages__Category::order("id DESC");

        $arr = array();

        //jeżeli jest włączone cache, odczytuje z cache
        if(self::$cache)
        {
            $cache_key = self::generate_memcache_key(array(
                                                         serialize($where),serialize($order)
                                                     ));

            $cache_data = Lib__Memcache::get($cache_key);
            if(!$cache_data)
            {
                $list =  App__Ed__Model__Pages__Category::find($where, $order);
                Lib__Memcache::set($cache_key,$list,array(
                    'class_name' => get_class(),
                    'key_name' => $cache_key
                ));
            }
            else
            {
                $list = $cache_data;
            }

        }
        else
        {
            $list =  App__Ed__Model__Pages__Category::find($where, $order);
        }

        foreach($list as $category)
        {
            $arr[] = array(
                'id' => $category->id,
                'id_parent' => $category->id_parent,
                'name' => $category->name,
                'name_seo' => $category->name_seo,
                'pages' => ($mode != 'without_pages') ? $category->get_pages() : array(),
                'data' => $category
            );
        }

        return $tree ? generate_tree($arr,$start_id_parent, $end_id_parent) : $arr;
    }

    /**
     * @param array $elements
     * @param int $parentId
     * @return array
     */
    private static function categories_tree(array $elements, $parentId = 0) {
        $branch = array();

        foreach ($elements as $element) {
            if ($element['id_parent'] == $parentId) {
                $children = self::categories_tree($elements, $element['id']);
                if ($children) {
                    $element['children'] = $children;
                }
                $branch[] = $element;
            }
        }

        return $branch;
    }

    /**
     * @param int $category_id
     * @param int $lang_id
     * @return array
     */
    public static function get_categories(int $category_id = 0, $lang_id = 1) : array
    {
        $categories_array = [];

        $categories_to_return = [];

        $categories = App__Ed__Model__Pages_category::find_by_sql("
            SELECT pages_category.id, pages_category.id_parent, pages_category_lang.name AS text, pages_category.status AS type
            FROM pages_category
            JOIN pages_category_lang ON pages_category_lang.pages_category_id=pages_category.id
            WHERE pages_category_lang.id_lang=$lang_id
            ORDER BY pages_category.id_parent DESC;
        ");

        foreach ($categories as $category)
        {
            $categories_array[] = $category->to_array();
        }

        $categories_tree = self::categories_tree($categories_array);

        $arrIt = new RecursiveIteratorIterator(new RecursiveArrayIterator($categories_tree));

        foreach ($arrIt as $sub)
        {
            $subArray = $arrIt->getSubIterator();
            if(empty($category_id)) {
                $categories_to_return[] = iterator_to_array($subArray);
            } else {
                if ($subArray['id'] === $category_id)
                {
                    $categories_to_return[] = iterator_to_array($subArray);
                }
            }
        }

        return $categories_to_return;
    }

    public static function get_categories_list(array $categories, int $category_id = 0, $lang_id = 1): array
    {
        $categories_list = [];

        $categories_array = [];

        if (!empty($categories))
        {
            foreach ($categories as $category)
            {
                $categories_array[] = $category->to_array();
            }

            $categories_tree = self::categories_tree($categories_array);

            $categories_iterator = new RecursiveIteratorIterator(new RecursiveArrayIterator($categories_tree));

            foreach ($categories_iterator as $index => $value)
            {
                if ($index == 'id' && $value == $category_id)
                {
                    $searched_category_data = $categories_iterator->getSubIterator();
                    $categories_list[] = iterator_to_array($searched_category_data);
                    $parent_id = $searched_category_data['id_parent'];
                    for ($count = $categories_iterator->getDepth(); $count && $count--;)
                    {
                        $parent_data = $categories_iterator->getSubIterator($count);
                        foreach ($parent_data as $parent_key => $parent)
                        {
                            if (is_array($parent))
                            {
                                if (!isset($parent['id']))
                                {
                                    continue;
                                }

                                if ($parent['id'] == $parent_id)
                                {
                                    $parent_id = $parent['id_parent'];
                                    $categories_list[] = $parent;
                                }
                            }
                            else
                            {
                                if ($parent_key == 'id' && $parent == $parent_id)
                                {
                                    $categories_list[] = iterator_to_array($parent_data);
                                    $parent_id = $parent_data['id_parent'];
                                }
                            }
                        }
                    }
                    break;
                }
            }
        }

        return $categories_list;
    }

    public static function get_categories_with_pages(int $category_id = 0, $lang_id = 1)
    {
        $categories = self::get_categories($category_id, $lang_id);

        $categories_ids = [];

        $categories_iterator = new RecursiveIteratorIterator(new RecursiveArrayIterator($categories));

        foreach ($categories_iterator as $index => $value)
        {
            if ($index == 'id' && is_numeric($value))
            {
                $categories_ids[] = (int)$value;
            }
        }

        $pages_by_category_id = Pages::get_pages_by_categories_ids($categories_ids);

        return ['categories' => $categories, 'pages' => $pages_by_category_id];
    }

    private static function generate_memcache_key($params=array())
    {
        return get_class().'::'.md5(implode('|',$params));
    }


}