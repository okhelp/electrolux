<?php

class App__Ed__Model__Pages__Bbcode__Gallery_photos
{

	
	public static function prepare($params)
	{ 
		$photos_list = array();
		
		if(!empty($params['id']))
		{
			$id_gallery = (int)$params['id'];
			
			$gallery = App__Ed__Model__Files::find($id_gallery);
			$photos_list = $gallery->get_photos();
			
			if($photos_list['total'] > 0)
			{
				$photos_list = $photos_list['list'];
			}
		}
		
		//ustawiam wygląd
		if(!isset($params['template']))
		{
			throw new Exception("Proszę podać parametr template= w gallery_photos.");
		}
		

		//zwracam wygląd
		$smarty = new Smarty;
		$smarty->assign('photos_list',$photos_list);
		return $smarty->fetch($params['template']);
	}
}
