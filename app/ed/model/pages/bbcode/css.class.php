<?php
class App__Ed__Model__Pages__Bbcode__Css
{
	public static function prepare($data)
	{	
		$url = $data['name'];
		if(strpos($url,'http') !== FALSE)
		{
			$html = '<link href="'.$url.'" rel="stylesheet">';
		}
		else
		{
			$html = '<link href="'.BASE_URL.'_css/'.$url.'" rel="stylesheet">';
		}
		
		return $html;
	}
}