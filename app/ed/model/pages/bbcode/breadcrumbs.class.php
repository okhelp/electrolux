<?php 

class App__Ed__Model__Pages__Bbcode__Breadcrumbs
{
	public static function prepare($name,$controller_data = array())
	{
		$smarty = new Smarty;
	
		if($controller_data)
		{
			$smarty->assign('breadcrumb',$controller_data->breadcrumb);
		}
	
		return $smarty->fetch('_partials/breadcrumb.tpl');
	}
}
