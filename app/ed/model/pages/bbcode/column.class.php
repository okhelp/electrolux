<?php 

class App__Ed__Model__Pages__Bbcode__Column
{
	public static function prepare($data)
	{
		$column_name = $data['name'];	
		return App__Ed__Model__Columns__Model::show_column($column_name);
	}
}