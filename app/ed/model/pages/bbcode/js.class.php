<?php
class App__Ed__Model__Pages__Bbcode__Js
{
	public static function prepare($data)
	{	
		$url = $data['name'];
		if(strpos($url,'http') !== FALSE)
		{
			$html = '<script src="'.$url.'"></script>';
		}
		else
		{
			$html = '<script src="'.BASE_URL.'_js/'.$url.'"></script>';
		}
		
		return $html;
	}
}