<?php
class App__Ed__Model__Pages__Bbcode__Reservation_form
{
	public static function prepare($data)
	{	
		$html = NULL;
		
		$reservation_form = new Reservation_Form();
		
		if(!empty($data) && !empty($data['id_service']))
		{
			$reservation_form->set_id_service((int)$data['id_service']);
		}
		
		$html = $reservation_form->show_form();
		
		return $html;
	}
}