<?php

class App__Ed__Model__Pages__Bbcode
{
	public $controller_data;
	
	public function __construct()
	{
		
	}
	
	public function set_data(App__Front__Controller__Pages__Index $controller)
	{
		$this->controller_data = $controller;
	}
	
	public function execute($string=NULL)
	{ 
		if(!$string)
		{
			$string = $this->controller_data->page_data->description;
			$string = str_replace("]<br />","] ",$string);
		}
		
		//bbcode z parametrem name
		preg_match_all("/\[(.*)\]/m",$string,$bbcodes);

		if(!empty($bbcodes))
		{
			foreach ($bbcodes[1] as $k => $bbcode)
			{
				$bbcode = html_entity_decode($bbcode);
				
				//pobieram nazwę bbcode
				$bbcode_arr = explode(' ', $bbcode);
				$bbcode_name = reset($bbcode_arr);
				
				//wersja z %
				if(strpos($bbcode,'%%') !== FALSE)
				{
					preg_match_all("/\[%%(.*)%%\]/m",$string,$bbcodes_percent);
					if(!empty($bbcodes_percent[1]))
					{
						foreach($bbcodes_percent[1] as $bbcode)
						{
							$bbcode = str_replace('&nbsp;','',$bbcode);
							$bbcode = html_entity_decode($bbcode);

							$class_name = 'App__Ed__Model__Pages__Bbcode__' . ucfirst($bbcode);
			
							if(class_exists($class_name) && method_exists($class_name, 'prepare'))
							{
								$string = str_replace(
									$bbcodes[0][$k],$class_name::prepare($bbcode, $this->controller_data),$string
								);
							}
						}
					}
				}
				//werjsa bez %
				else
				{
					//pobieram dodatkowe wartości, które przekażę do klasy bbcode
					preg_match_all(
						"/(\w+)=[\"']([a-zA-Z0-9_.\/\-:'\"]+)[\"']/m",
						$bbcode,
						$bbcode_params);
					
					$bbcode_params = $this->prepare_params($bbcode_params); 
					$class_name = 'App__Ed__Model__Pages__Bbcode__' . ucfirst($bbcode_name);
					
					if(class_exists($class_name) && method_exists($class_name, 'prepare'))
					{
						
						$string = str_replace(
							$bbcodes[0][$k],$class_name::prepare($bbcode_params),$string
						);
					}
				}
			}
		}
		
		return $string;
	}
	
	private static function prepare_params($data)
	{
		$return_data = array();
		
		if(!empty($data))
		{
			unset($data[0]);
			
			foreach($data[1] as $k=> $param)
			{
				$return_data[$param] = $data[2][$k];
			}
		}
		
		return $return_data;
	}
}
