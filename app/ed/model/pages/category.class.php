<?php

class App__Ed__Model__Pages__Category extends Lib__Base__Model
{
	static $table_name = 'pages_category';
	static $primary_key = 'id';
	static $cache = FALSE; 
	
	public $categories_tree = array();
	
	public function has_childreen()
	{
		$find_childreen = App__Ed__Model__Pages__Category::find(App__Ed__Model__Pages__Category::where("id_parent=".$this->id),App__Ed__Model__Pages__Category::limit("0,1"));
		return !empty($find_childreen) ? TRUE : FALSE;
	}
	
	public function get_url()
	{
		$url = BASE_URL;
		
		if($this->id_parent > 0)
		{
			$tree = 
				App__Ed__Model__Pages__Category__Tree::get_list(0,-1, $this->id,'without_pages');
			$tree = get_tree_array($tree);
			
			$cat_arr = array();
			
			foreach($tree as $item)
			{
				$cat_arr[] = $item['name_seo'];
			}
			
			//buduję url
			$url .= implode('/',$cat_arr);
			
		}
		else
		{
			$url .= !empty($this->name_seo) ? $this->name_seo : '';
		}
		
		if($url != BASE_URL)
		{
			$url .= "/";
		}
		
		return $url;
	}
	
	public function get_categories_tree($menu_items)
	{
		$this->get_categories_tree_recursive($menu_items);
		
		$categories_tree = array();
			
		//filtruję kategorie - stopuje na właściwej
		foreach(array_reverse($this->categories_tree) as $category)
		{
			if($this->id_parent == $category['id_parent'])
			{
				if($category['id'] == $this->id)
				{
					$categories_tree[$category['id']] = $category['name_seo'];	
					break;
				}
			}
			else 
			{
				$categories_tree[$category['id']] = $category['name_seo'];	
			}
		}

		return $categories_tree;
	}
	
	private function get_categories_tree_recursive($menu_items)
	{
		$list = array();
		
		foreach($menu_items as $k=> $items)
		{
			if(!empty($items) && $k == 'children' && is_array($items))
			{
				foreach($items as $k => $item)
				{
					$list = $this->get_categories_tree_recursive($item);					
				}
			}
		}
		
		$this->categories_tree[] = $menu_items;
		return $list;
	}
	
	public function get_pages()
	{
		$cache_key = "Pages__Category__Links::get_pages-".$this->id;
		$cache_data = Lib__Memcache::get($cache_key);
		Lib__Memcache::delete($cache_key);
		
		if(!empty($cache_data)) //pobieram dane z cache
		{
			$return_data = unserialize_data($cache_data);
		}
		else //dodaję dane do cache
		{
			$return_data = array();
			$pages_where = App__Ed__Model__Pages::where("id_category = " . $this->id);
			$pages_where->add("status = 1");
			$pages_order = App__Ed__Model__Pages::order('name','ASC');
			$pages = App__Ed__Model__Pages::find($pages_where);

			if(!empty($pages))
			{
				foreach($pages as $p)
				{
					$return_data[] = $p;
				}
			}
			
			$return_data = serialize($return_data);
			
			//zapisuję do cache
			Lib__Memcache::set($cache_key,$return_data,array(
				'class_name' => get_class(),
				'key_name' => $cache_key
			));
			
			$return_data = unserialize($return_data);
		}
		
		return $return_data;
	}
}