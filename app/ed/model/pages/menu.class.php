<?php

class App__Ed__Model__Pages__Menu
{
	protected $_id_category;
	protected $_id_parent;
	protected $_id_page;
	protected $_mode;
	
	public function __construct($id_category = 0, $id_page = 0, $mode = 'stop_id_parent')
	{
		//przypisuję zmienne z konstruktora
		$this->_id_category = $id_category;
		$this->_id_page = $id_page;
		$this->_id_parent = $this->get_id_parent();
		$this->_mode = $mode;
	}
	
	public function prepare($only_in_main = 1)
	{
		//tworzę punkt stopowy
		if($this->_mode == 'stop_id_parent')
		{
			$stop = $this->_id_parent;
		}
		else 
		{
			$stop = -1;
		}
		
		//tworzę drzewo kategorii
		$category_tree = App__Ed__Model__Pages__Category__Tree::get_list($this->_id_category,1,$stop);
		
		//jeżeli jest to kategoria główna to wybieram tylko jej trzon
		if($only_in_main && $this->_id_parent > 0)
		{
			$category_tree = reset($category_tree);
		}
		
		
		return array_reverse($category_tree);
	}
	
	private function get_id_parent()
	{
		$id_parent = App__Ed__Model__Pages__Category::find($this->_id_category)->id_parent;
		return !empty($id_parent) ? $id_parent : $this->_id_category;
	}
	
	public function get_pages($id_cat=0)
	{
		if(empty($id_cat))
		{
			$id_cat = $this->_id_category;
		}
		
		return App__Ed__Model__Pages::find(where("id_category = $id_cat"));
	}
	
}