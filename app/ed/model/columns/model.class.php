<?php

class App__Ed__Model__Columns__Model
{
	public static function shortcuts()
	{
		return array(
			'[%%adres_strony%%]' => BASE_URL
		);
	}
	
	public static function get_shortcuts_value($shortcut)
	{
		$shortcuts = self::shortcuts();
		return !empty($shortcuts[$shortcut]) ? $shortcuts[$shortcut] : '';
	}
	
	public static function show_column($column_name)
	{
		$return_value = '';
		
		$column = App__Ed__Model__Columns::find(App__Ed__Model__Columns::where("name = '$column_name'"));
		if(!empty($column))
		{
			$column = reset($column);
			$return_value = $column->description;
			
			//zamieniam skróty
			$shortcuts = self::shortcuts();
			foreach($shortcuts as $shortcut => $shortcut_value)
			{
				if(strpos($return_value,$shortcut) !== FALSE)
				{
					$return_value = str_replace($shortcut,$shortcut_value,$return_value);
				}
			}
			
			//definiuję szablon
			$smarty = new Smarty;
			$smarty->assign('name',$column_name);
			$smarty->assign('description',$return_value);
			$return_value = $smarty->fetch('_partials/columns/show_column.tpl');
		}
		
		return $return_value;
	}
	
	public static function save($name,$value)
	{
		$shortcuts = self::shortcuts();
		
		//pobieram nazwę elementu
		$name = str_replace('ed_','',$name);
		
		//sytuacja odwrotna, jeżeli we frazie znajdzie nam wartość skrótu, to zamieniam na skrót :)
		foreach($shortcuts as $shortcut => $shortcut_value)
		{
			if(strpos($value,$shortcut_value) !== FALSE)
			{
				$value = str_replace($shortcut_value,$shortcut,$value);
			}
		}
		
		//tworzę obiekt
		$column = App__Ed__Model__Columns::find(App__Ed__Model__Columns::where("name = '$name'"));
		
		if(!empty($column))
		{
			$column = reset($column);
			$column->description = $value;
			return $column->save();
		}
		
		return FALSE;
	}
}