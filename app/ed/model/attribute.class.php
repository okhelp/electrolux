<?php

class App__Ed__Model__Attribute extends Lib__Base__Model
{
    const COLUMN_ID            = 'id';
    const COLUMN_ELECTROLUX_ID = 'electrolux_id';
    const COLUMN_CREATED_AT    = 'created_at';
    const COLUMN_UPDATED_AT    = 'updated_at';
    const COLUMN_NAME          = 'name';

    const TABLE_NAME = 'attribute';

    static $table_name = self::TABLE_NAME;
    static $primary_key = self::COLUMN_ID;
}
