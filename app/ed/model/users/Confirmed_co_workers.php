<?php
declare(strict_types=1);

namespace App\ed\model\users;

use Lib__Base__Model;

class Confirmed_co_workers extends Lib__Base__Model
{
    const STATUS_NOT_CONFIRMED = 0;
    const STATUS_CONFIRMED     = 1;
    const STATUS_REJECTED      = 2;

    const COLUMN_ID           = 'id';
    const COLUMN_ID_USER      = 'id_user';
    const COLUMN_ID_COMPANY   = 'id_company';
    const COLUMN_STATUS       = 'status';
    const COLUMN_CONFIRMED_BY = 'confirmed_by';
    const COLUMN_CREATED_AT   = 'created_at';
    const COLUMN_UPDATED_AT   = 'updated_at';

    const TABLE_NAME = 'confirmed_co_workers';

    static $table_name = self::TABLE_NAME;
    static $primary_key = self::COLUMN_ID;
}