<?php

class App__Ed__Model__Users__Register
{
    public static function save($register_data, $auto_register=FALSE)
    {
        //sprawdzam czy podano prawidłowy adres e-mail
		if(!filter_var($register_data['email'], FILTER_VALIDATE_EMAIL))
		{
			go_back('d|Podano nieprawidłowy adres e-mail. Proszę wprowadzić poprawny.');
		}
		
		//sprawdzam czy taki użytkownik już istnieje (e-mail)
		$register_data['email'] = trim($register_data['email']);
		if(self::is_user_registered('email',$register_data['email']))
		{
			go_back('d|Użytkownik o podanym adresie e-mail istnieje już w naszym systemie. Jeżeli nie pamiętasz hasła, użyj opcji "przypomnij hasło".');
		}
		
		//sprawdzam czy taki użytkownik już istnieje (login)
		$register_data['login'] = trim($register_data['login']);
		
		if(self::is_user_registered('login',$register_data['login']))
		{
			go_back('d|Podany login jest już zajęty. Proszę wprowadzić inny.');
		}
		
		//nie można używać loginów z frazą "admin"
		if(strpos($register_data['login'],'admin') !== FALSE)
		{
			go_back('d|Login zawiera frazę zastrzeżoną dla administratorów serwisu. Proszę wybrać inną nazwę');
		}
		
		//sprawdzam czy podane hasła sa identyczne
		$register_data['password'] = trim($register_data['password']);
		$register_data['password_repeat'] = trim($register_data['password_repeat']);
		if($register_data['password'] !== $register_data['password_repeat'])
		{
			go_back('d|Podane hasła nie są identyczne. Proszę wprowadzić dwa razy to same hasło.');
		}
		
		//dodaję takiego użytkownika z statusem 0 (zablokowany), ponieważ konto wymaga aktywacji poprzez e-mail
		$user = new App__Ed__Model__Users;
		$user->login = trim($register_data['login']);
		$user->password = App__Ed__Model__Encryption::encode($register_data['password']);
		$user->email = $register_data['email'];
		$user->name = trim($register_data['name']);
		$user->surname = trim($register_data['surname']);
		$user->ts = date('Y-m-d H:i:s');
		if($auto_register) //w przypadku automtycznej rejestracji konta, od razu go aktywujemy
		{
			$user->status = 1;
		}
		$user->save();
		
		//dodaję prawo "użytkownik" dla zarejestrowanego użytkownika
		App__Ed__Model__Acl::add_user_to_group($user->id,2);
		
		//wysyłam maila do użytkownika
        if($register_data['email'] != 'phpunit@goinweb.pl' && !$auto_register)
        {
            $user->send_email('new_user',$user);
        }
		
		if(!$auto_register) //nie robię przekierowania jeżeli jest auto rejestracja konta
		{
			go_back('g|Gratulacje! Konto zostało założone. Na Twój adres e-mail wysłaliśmy wiadomość z linkiem aktywującym konto. Prosimy w niego kliknąć.','index.html');
		}
		
    }
	
	public static function is_user_registered($data_type='login',$data)
	{
		switch($data_type)
		{
			case 'login':
				$user = App__Ed__Model__Users::find_by_login($data);
				if(!empty($user))
				{
					return TRUE;
				}
			break;
			
			case 'email':
				$user = App__Ed__Model__Users::find_by_email($data);
				if(!empty($user))
				{
					return TRUE;
				}
			break;
		}
		
		return FALSE;
	}
}