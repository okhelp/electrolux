<?php

class App__Ed__Model__Users__Email
{
	protected $_user_data;
	protected $_other_params = array();
	protected $_smarty;
	
	public function __construct(App__Ed__Model__Users $user_data, $other_params = array())
	{
		//ładuję obiekt users
		$this->_user_data = $user_data;
		
		//ładuję dodatkowe parametry
		$this->_other_params = $other_params;
		
		//ładuję smarty
		$this->_smarty = new Smarty();
		
		//ładuję do szablonow stałe wartości
		$this->load_data_to_view();
	}
	
	private function load_data_to_view()
	{
		//ładuję obiekt users
		$this->_smarty->assign('user_data',$this->_user_data);
		
		//ładuję dodatkowe parametry jak takowe są
		$this->_smarty->assign('other_params',$this->_other_params);
	}
			
	public function send_new_user()
	{
		//dołączam zmienne do szablonu
		$this->_smarty->assign('activation_link',
               App__Ed__Model__Users__Auth::create_activation_link($this->_user_data->id));
		
		return array(
			'title' => 'Aktywacja konta uzytkownika',
			'view' => $this->_smarty->fetch(
                get_app_template_path() . 'users/_partials/emails/new_user.tpl')
		);
	}
	
	public function send_password()
	{
		return array(
			'title' => 'Przypomnienie hasła użytkownika',
			'view' => $this->_smarty->fetch(
                get_app_template_path() . 'user/_partials/emails/password.tpl')
		);
	}
}