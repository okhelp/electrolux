<?php
declare(strict_types=1);

namespace App\ed\model\users;

use ActiveRecord\DateTime;
use App\ed\model\company\Get_company_users;
use App\front\models\company\Get_company_services;
use App__Ed__Model__Acl;
use App__Ed__Model__Company;
use App__Ed__Model__Company__Users;
use App__Ed__Model__Encryption;
use App__Ed__Model__Users;
use App__Ed__Model__Users__Generate_password;
use App__Ed__Model__Users__Register;
use App__Ed__Model__Users__Service;
use Exception;
use Lib__PDO;

class Register_new_co_worker
{
    private $user_name;

    private $user_surname;

    private $user_email;

    private $company;

    /**
     * Register_new_co_worker constructor.
     * @param string $user_name
     * @param string $user_surname
     * @param string $user_email
     * @param App__Ed__Model__Company $company
     */
    public function __construct(string $user_name, string $user_surname, string $user_email, App__Ed__Model__Company $company)
    {
        $this->user_name = $user_name;
        $this->user_surname = $user_surname;
        $this->user_email = $user_email;
        $this->company = $company;
    }

    /**
     * @return int
     * @throws Exception
     */
    public function register(): int
    {
        $users_count_in_company = $this->get_users_count_in_company();

        if (!filter_var($this->user_email, FILTER_VALIDATE_EMAIL))
        {
            throw new Exception('Podano niepoprawny email');
        }

        $this->user_email = trim($this->user_email);
        if (App__Ed__Model__Users__Register::is_user_registered('email', $this->user_email))
        {
            throw new Exception('Użytkownik o podanym adresie e-mail już istnieje w systemie.');
        }

        $login = $this->company->nip . '_' . ($users_count_in_company + 1);

        if (App__Ed__Model__Users__Register::is_user_registered('login', $login))
        {
            throw new Exception("Podany login {$login} jest już zajęty. Proszę wprowadzić inny.");
        }

        if (strpos($login, 'admin') !== false)
        {
            throw new Exception('Login zawiera frazę zastrzeżoną dla administratorów serwisu. Proszę wybrać inną nazwę');
        }

        $user = new App__Ed__Model__Users;
        $user->login = trim($login);
        $user->password = App__Ed__Model__Encryption::encode($this->generate_random_password());
        $user->email = $this->user_email;
        $user->name = trim($this->user_name);
        $user->surname = trim($this->user_surname);
        $user->status = 1;
        $user->ts = date('Y-m-d H:i:s');
        $user->ts_change_password = (new DateTime('2010-01-01 00:00:00'))->format('Y-m-d H:i:s');

        $is_saved = $user->save();

        if (!$is_saved)
        {
            throw new Exception('Nie udało się dodać użytkownika');
        }

        App__Ed__Model__Acl::add_user_to_group($user->id, 2);

//        if ($this->user_email != 'phpunit@goinweb.pl')
//        {
//            $user->send_email('new_user', $user);
//        }

        $company_services = Get_company_services::get($this->company->id);

        if (!empty($company_services))
        {
            foreach ($company_services as $company_service)
            {
                $pdo = new Lib__PDO;
                $pdo->query(
                    "
                    INSERT INTO users_service ( id_user, id_service )
                       VALUES
                       (" . $user->id . ", " . $company_service . ");
                ");
            }
        }

        $assign_user_to_company = new App__Ed__Model__Company__Users();
        $assign_user_to_company->id_company = $this->company->id;
        $assign_user_to_company->id_user = $user->id;
        $assign_user_to_company->role = 'user';

        return $is_saved && $assign_user_to_company->save() ? (int)$user->id : 0;
    }

    private function get_users_count_in_company(): int
    {
        $count = 0;

        $company_users = Get_company_users::get($this->company->id, true);

        if(!empty($company_users))
        {
            $users = Get_by_ids::get(array_map(function($company_user){ return $company_user->id_user; }, $company_users));

            $users_count = count($company_users);

            $max_value_in_login = $users_count > 0 ? $users_count - 1 : 0;

            foreach ($company_users as $company_user)
            {
                if(empty($users[$company_user->id_user]))
                {
                    continue;
                }

                $values_in_login = explode('_', $users[$company_user->id_user]->login);

                $value = empty($values_in_login[1]) ? 0 : $values_in_login[1];

                if($value > $max_value_in_login)
                {
                    $max_value_in_login = $value;
                }
            }

            $count = $max_value_in_login;
        }

        return (int)$count;
    }

    private function generate_random_password(): string
    {
        return App__Ed__Model__Users__Generate_password::run(8, 1, "lower_case,numbers");
    }
}