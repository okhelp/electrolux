<?php
declare(strict_types=1);

namespace App\ed\model\users;

use App__Ed__Model__Users;

class Get_company_users_list_by_user_type
{
    public static function get(int $id_service, string $user_type): array
    {
        $users = App__Ed__Model__Users::find_by_sql(
            "
                SELECT users.*
                FROM company_users
                JOIN users ON company_users.id_user=users.id
                JOIN users_service ON users_service.id_user=company_users.id_user
                WHERE users_service.id_service=$id_service AND company_users.role='$user_type'
            ");

        return empty($users) ? [] : $users;
    }
}