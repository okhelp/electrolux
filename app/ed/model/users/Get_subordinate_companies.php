<?php
declare(strict_types=1);

namespace App\ed\model\users;

use App__Ed__Model__Company_supervisor;

class Get_subordinate_companies
{
    public static function get(array $supervisors_ids)
    {
        $subordinate_companies_to_return = [];

        if(!empty($supervisors_ids))
        {
            $subordinates = App__Ed__Model__Company_supervisor::find_by_sql("
                SELECT *
                FROM company_supervisor
                WHERE id_supervisor IN (" . implode(',', $supervisors_ids) . ")
            ");

            if($subordinates)
            {
                $subordinate_companies_to_return = array_map(function($subordinate){ return $subordinate->id_company; }, $subordinates);
            }
        }

        return $subordinate_companies_to_return;
    }
}