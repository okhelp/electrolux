<?php

class App__Ed__Model__Users__Assigned_supervisors_list
{
    public static function get()
    {
        $list_to_return = [];

        $assigned_company_supervisors = App__Ed__Model__Company_supervisor::all();

        if (!empty($assigned_company_supervisors))
        {
            $supervisors_ids = array_map(
                function($assign) {
                    return $assign->id_supervisor;
                }, $assigned_company_supervisors);
            $companies_ids = array_map(
                function($assign) {
                    return $assign->id_company;
                }, $assigned_company_supervisors);

            $supervisors = App__Ed__Model__Users::get_by_ids($supervisors_ids);

            $companies = App__Ed__Model__Company::get_by_ids($companies_ids);

            foreach ($assigned_company_supervisors as $assigned_company_supervisor)
            {
                $list_to_return[] = [
                    'id'         => $assigned_company_supervisor->id,
                    'supervisor' => $supervisors[$assigned_company_supervisor->id_supervisor],
                    'company'    => $companies[$assigned_company_supervisor->id_company],
                ];
            }
        }

        return $list_to_return;
    }
}