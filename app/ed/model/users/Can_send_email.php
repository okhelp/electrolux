<?php
declare(strict_types=1);

namespace App\ed\model\users;

use App\ed\model\newsletter\Users_options;

class Can_send_email
{
    public static function verify(int $id_user, int $id_settings): bool
    {
        $user_options = Get_user_options::get($id_user);

        return !isset($user_options[$id_settings]) || (isset($user_options[$id_settings]) && !empty($user_options[$id_settings]->{Users_options::COLUMN_STATUS}));
    }
}