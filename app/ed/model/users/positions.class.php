<?php

class App__Ed__Model__Users__Positions extends Lib__Base__Model
{
    static $table_name = 'users_positions';
    static $primary_key = 'id';
    static $cache = TRUE;
}
