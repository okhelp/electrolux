<?php

class App__Ed__Model__Users__Service extends Lib__Base__Model
{
    static $table_name = "users_service";

    public static function get_user_services(int $user_id)
    {
        $user_services_to_return = [];

        $user_services = self::find_by_sql(
            "
            SELECT *
            FROM users_service
            JOIN service ON service.id=users_service.id_service
            WHERE users_service.id_user=$user_id
        ");

        return empty($user_services) ? $user_services_to_return : $user_services;
    }
}