<?php

class App__Ed__Model__Users__Password_Rodo
{
    protected $_user;

    public function __construct(App__Ed__Model__Users $user)
    {
        $this->_user = $user;
    }

    public function get_last_date()
    {
        return empty($this->_user->ts_change_password) ? $this->_user->ts->format('Y-m-d') : $this->_user->ts_change_password->format('Y-m-d');
    }

    public function check()
    {
        $last_change_date = strtotime($this->get_last_date());
        $today_date = time();

        $days = floor(($today_date - $last_change_date) / (60 * 60 * 24) );

        if($days > 30)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    public function change($new_password_1, $new_password_2)
    {
        if($new_password_1 != $new_password_2)
        {
            go_back("d|Wprowadzone hasła nie są identyczne.");
        }

        //sprawdzam czy hasło nie jest takie same jak obecnie wprowadzone
        $actual_password = $this->_user->password;
        $new_password = App__Ed__Model__Encryption::encode($new_password_1);

        if($actual_password === $new_password)
        {
            go_back("d|Hasło musi być inne niż obecne.");
        }

        //sprawdzam poprawność hasła
        $check_password = self::change_correct_password($new_password_1);
        if(!empty($check_password['status']) && $check_password['status'] == 'error')
        {
            go_back("d|" . $check_password['message']);
        }

        //zapis
        $this->_user->password = App__Ed__Model__Encryption::encode($new_password_1);
        $this->_user->ts_change_password = date('Y-m-d');

        return $this->_user->save();

    }

    public static function change_correct_password($password)
    {
        $return = [
            'status' => 'success',
            'message' => "Hasło prawidłowe"
        ];

        //sprawdzam ilość znaków (min. 8)
        if(strlen($password) < 8)
        {
            $return = [
                'status' => 'error',
                'message' => "Długość hasła musi być min. 8 znaków."
            ];
        }

        //sprawdzam czy hasło zawiera min 1 małą, bądź dużą literę
        if(!((bool)preg_match( '~[a-z]~', $password) && (bool)preg_match( '~[A-Z]~', $password)))
        {
            $return = [
                'status' => 'error',
                'message' => "Hasło powinno zawierać min. 1 dużą, bądź małą literę."
            ];
        }

        //sprawdzam czy hasło zawiera min. 1 literę
        if(!((bool)preg_match( '~[0-9]~', $password)))
        {
            $return = [
                'status' => 'error',
                'message' => "Hasło powinno zawierać min. 1 cyfrę."
            ];
        }

        //sprawdzam czy hasło zawiera min. 1 znak specjalny
        if(!((bool)preg_match('/[\'^£$%&*()}{@#~?><>,|=_+¬-]/', $password)))
        {
            $return = [
                'status' => 'error',
                'message' => "Hasło powinno zawierać min. 1 znak specjalny."
            ];
        }

        return $return;
    }
}