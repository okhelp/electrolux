<?php

class  App__Ed__Model__Users__History_Logged extends Lib__Base__Model
{
    static $table_name = 'users_history_logged';
    static $primary_key = 'id';
    static $cache = TRUE;
}