<?php
declare(strict_types=1);

namespace App\ed\model\users;

use App__Ed__Model__Users;

class Get_by_ids
{
    public static function get(array $users_ids): array
    {
        $users_to_return = [];

        if (!empty($users_ids))
        {
            $users = App__Ed__Model__Users::find_by_sql(
                "
                SELECT *
                FROM users
                WHERE id IN (" . implode(',', $users_ids) . ")
            ");

            if (!empty($users))
            {
                foreach ($users as $user)
                {
                    $users_to_return[$user->id] = $user;
                }
            }
        }

        return $users_to_return;
    }
}