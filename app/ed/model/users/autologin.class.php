<?php

class App__Ed__Model__Users__Autologin
{   
    public static function execute_autologin($ref=NULL)
    {
        $id_autologin = 0;
        
        $cookie_data = Lib__cookie::get(COOKIENAME_AUTOLOGIN);
        
        if(!empty($cookie_data))
        {
            //odczytuję rzeczy z ciastka
            $cookie_data = self::decode_cookie($cookie_data);
            $user_name = $cookie_data['user_name'];
            $password = App__Ed__Model__Encryption::decode($cookie_data['password']);
            
            //loguję użytkownika
            $auth = new App__Ed__Model__Users__Auth($user_name,$password,$ref);
			$auth->login();
            
            if(empty($ref))
            {
                $ref = BASE_URL;
            }
            
            //przekierowuję użytkownika
            redirect($ref);
        }
        
    }
    
    public function set_cookie()
    {
        //ustawiam parametry ciasteczka
        $data = array(
            'user_name' => $_POST['user_name'],
            'password' => App__Ed__Model__Encryption::encode($_POST['password'])
        );
        
        //koduję treść ciasteczka
        $data = self::encode_cookie($data);
        
        //jeżeli jest takie ciacho to go usuwam
        if(Lib__cookie::get(COOKIENAME_AUTOLOGIN))
        {
            Lib__cookie::remove(COOKIENAME_AUTOLOGIN);
        }
        
        //ustawiam nowe ciastko
        Lib__cookie::set(COOKIENAME_AUTOLOGIN,$data);
    }
    
    private function encode_cookie(array $array_data)
    {
        $string = serialize($array_data);
        return App__Ed__Model__Encryption::encode($string);
    }
    
    private function decode_cookie($string)
    {
        $string = App__Ed__Model__Encryption::decode($string);
        return unserialize($string);
    }
    
    
}