<?php

class App__Ed__Model__Users__Remember_Password
{
	protected $_user_data = array();
	protected $_email;
	
	public function __construct($email)
	{
		$this->_email = $email;
		$this->_user_data = $this->get_user_data();
		
	}
	
	private function get_user_data()
	{
		$return_data = array();
		$where = App__Ed__Model__Users::where("email = '{$this->_email}'");
		$user_data = App__Ed__Model__Users::find($where);
		if(!empty($user_data))
		{
			$return_data = reset($user_data);
		}
		
		return $return_data;
	}
	private function get_user_password()
	{
		$password = '';
		
		//pobieram hasło użytkownika
		if(!empty($this->_user_data))
		{
			$password = App__Ed__Model__Encryption::decode($this->_user_data->password);
		}
		
		return $password;
	}
	
	public function execute()
	{
		//pobieram hasło użytkownika, jeżeli jest błędny adres e-mail to wyrzucam błędem
		$user_password = $this->get_user_password();
		
		if(!$user_password)
		{
			go_back("d|Użytkownik o podanym adresie e-mail nie istnieje.");
		}
		
		//wysyłam wiadomość
		$this->_user_data->send_email(
			'password',
			array(
				'password' => $user_password
			)
		);
	}
}