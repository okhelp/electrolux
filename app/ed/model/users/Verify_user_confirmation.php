<?php
declare(strict_types=1);

namespace App\ed\model\users;

class Verify_user_confirmation
{
    public static function verify(int $id_user): bool
    {
        $verified = false;

        if (!empty($id_user))
        {
            $user_confirmation = Confirmed_co_workers::find(
                Confirmed_co_workers::where(Confirmed_co_workers::COLUMN_ID_USER . " = {$id_user} AND status = " . Confirmed_co_workers::STATUS_CONFIRMED)
            );

            $verified = (bool)!empty($user_confirmation);
        }

        return $verified;
    }
}