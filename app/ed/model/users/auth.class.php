<?php

use App\ed\model\company\Get_by_user_id;
use App\ed\model\newsletter\Users_options;
use App\ed\model\points\Get_users_points;
use App\ed\model\users\Can_send_email;
use App\ed\model\users\Get_user_options;
use App\front\models\auth\Get_user_services;

class App__Ed__Model__Users__Auth
{
	protected $user_name;
	protected $password;
    protected $_autologin;
	protected $redirect;
	
	public function __construct($user_name,$password,$autologin=0,$redirect=NULL) 
	{
		$this->user_name = $user_name;
		$this->password = App__Ed__Model__Encryption::encode($password);
        $this->_autologin = $autologin;
		$this->redirect = $redirect ? $redirect : 'index.html';
	}
	
	public function login()
	{   	
		//brak wpisanych danych do logowania
		if (!$this->user_name || !$this->password) {
			go_back('d|Proszę uzupełnić wszystkie pola.','ed/');
		}
		
		//sprawdzam czy taki użytkownik istnieje
		$where = App__Ed__Model__Users::where("login='{$this->user_name}'");
		$where->add("password='{$this->password}'");
		$user = App__Ed__Model__Users::find($where);
        $user = reset($user);
		if (empty($user)) { //sprawdzam poprawność wpisanych danych
		    if (is_ed()) {
                go_back('d|Wprowadzono błędny login, bądź hasło');
            } else {
                go_back('d|Wprowadzono błędny login, bądź hasło', BASE_URL);
            }
		} elseif (!empty($user) && !intval($user->status)) { //użytkwnik jest zablokowany
            self::set_history_logged($user->id,0);
            if (is_ed()) {
                go_back('d|Twoje konto jest zablokowane.');
            } else {
                go_back('d|Twoje konto jest zablokowane.', BASE_URL);
            }
		} elseif(!empty($user)) { //jest taki użytkownik, loguję mu sesję
			$this->set_session($user);
            self::set_history_logged($user->id);

            if (App__Ed__Model__Acl::has_group('premium-user', 0) && (empty($user->name) || empty($user->surname))) {
                redirect(BASE_URL . 'prosba-o-uzupelnienie-danych-p7.html');
                die();
            }

            if (App__Ed__Model__Acl::has_group('user', 0) && is_null($user->ts_change_company)) {
                redirect(BASE_URL . 'wybor-firmy-p8.html');
                die();
            }

			go_back('g|Zostałeś poprawnie zalogowany',$this->redirect);
		}
	}

    final function set_session($user, $history_logger = [])
    {
        $points = App__Ed__Model__Points_log::getPoints($user->id);

        $user_services = Get_user_services::get($user->id);

        $company_user = Get_by_user_id::get((int)$user->id);

        $session_array = [
            'id'               => $user->id,
            'user_name'        => $user->login,
            'name'             => $user->name,
            'surname'          => $user->surname,
            'id_cimg'          => $user->id_cimg,
            'date_birth'       => $user->date_birth ? date('Y-m-d', strtotime($user->date_birth)) : null,
            'history_logger'   => $history_logger,
            'user_points'      => $points,
            'user_services'    => empty($user_services) ? [] : array_map(
                function($service) {
                    return $service->to_array();
                }, $user_services),
            'is_company_admin' => empty($company_user->user) || (!empty($company_user->user) && $company_user->user->role != 'admin') ? 0 : 1,
            'display_points'   => Can_send_email::verify((int)$user->id, Users_options::OPTION_SHOW_POINTS),
        ];

        Lib__Session::set($session_array);

        Lib__Session::set('display_points', $_SESSION['display_points'] && App__Ed__Model__Acl::has_access('user_panel', 0));
    }

    public static function create_activation_link($id_user)
	{
		$user = App__Ed__Model__Users::find_by_id($id_user);
		if(!empty($user))
		{
			$key = urlencode(time().strtolower(App__Ed__Model__Users__Generate_password::run(25,1,"lower_case,numbers")).date('Y'));
			
			//przypisuję klucz do użytkownika
			$user->activation_token = $key;
			$user->save();
		}
		return BASE_URL.'auth/aktywacja.html?code='.$key;
	}

    public static function set_history_logged($id_user, $status = 1)
    {
        $history = new  App__Ed__Model__Users__History_Logged();
        $history->id_user = $id_user;
        $history->ip = $_SERVER['REMOTE_ADDR'];
        $history->host = gethostbyaddr($_SERVER['REMOTE_ADDR']);
        $history->id_cookie = App__Ed__Model__Id_cookie::get();
        $history->user_agent = $_SERVER['HTTP_USER_AGENT'];
        $history->status = $status;
        $history->save();
    }
}