<?php
declare(strict_types=1);

namespace App\ed\model\users;

use App\ed\model\newsletter\Users_options;

class Get_user_options
{
    public static function get(int $id_user): array
    {
        $options_to_return = [];

        $options = Users_options::find_by_sql("
            SELECT *
            FROM users_options
            WHERE id_user = $id_user
        ");

        if(!empty($options))
        {
            foreach ($options as $option)
            {
                $options_to_return[$option->{Users_options::COLUMN_ID_OPTION}] = $option;
            }
        }

        return $options_to_return;
    }
}