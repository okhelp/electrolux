<?php

class Slack
{
	const APP_URL = 'https://hooks.slack.com/services/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';
	
	public static function send_message($message)
	{
		$useragent = "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)";
		$payload = 'payload={"channel": "#notification", "username": "webhookbot", '
				. '"text": "'.$message.'", "icon_emoji": ":ghost:"}';

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_USERAGENT, $useragent); //set our user agent
		curl_setopt($ch, CURLOPT_POST, TRUE); //set how many paramaters to post
		curl_setopt($ch, CURLOPT_URL,self::APP_URL); //set the url we want to use
		curl_setopt($ch, CURLOPT_POSTFIELDS,$payload); 

		curl_exec($ch); //execute and get the results
		curl_close($ch);
	}
}