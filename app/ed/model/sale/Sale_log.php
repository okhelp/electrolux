<?php
declare(strict_types=1);

namespace App\ed\model\sale;

use App\ed\model\Company_users_points;
use App\ed\model\Sale_log as Sale_log_model;
use App\ed\model\Sale_model;
use App\front\models\user\User_points;
use App\modules\products\front\collections\Get_product_by_id_collection;
use App\modules\products\front\models\Product_model;
use Lib__Session;

class Sale_log
{
    /** @var int */
    private $id_user;

    /** @var Sale_model */
    private $sale;

    /**
     * @param int $id_user
     * @param Sale_model $sale
     */
    public function __construct(int $id_user, Sale_model $sale)
    {
        $this->id_user = $id_user;
        $this->sale = $sale;
    }

    public function add(): bool
    {
        $product = Get_product_by_id_collection::get($this->sale->{Sale_model::COLUMN_ID_PRODUCT});

        $user_points = \App__Ed__Model__Points_log::getPoints($this->id_user);

        $sale_log = new Sale_log_model();
        $sale_log->{Sale_log_model::COLUMN_ID_PRODUCT} = $product->{Product_model::COLUMN_ID};
        $sale_log->{Sale_log_model::COLUMN_PRODUCT_MODEL} = $product->{Product_model::COLUMN_MODEL};
        $sale_log->{Sale_log_model::COLUMN_COUNT} = $this->sale->{Sale_model::COLUMN_COUNT};
        $sale_log->{Sale_log_model::COLUMN_POINTS_BEFORE} = $user_points;
        $sale_log->{Sale_log_model::COLUMN_POINTS_AFTER} = $user_points + $this->sale->{Sale_model::COLUMN_POINTS};
        $sale_log->{Sale_log_model::COLUMN_CONFIRMING_USER_ID} = Lib__Session::get('id');

        return $sale_log->save();
    }
}