<?php
declare(strict_types=1);

namespace App\ed\model\sale;

use App\ed\model\Sale_model;

class Get_sale_history
{
    public static function get(array $customers_ids)
    {
        $sale_history_to_return = [];

        if (!empty($customers_ids))
        {
            $sale_history = Sale_model::find_by_sql(
                "
                SELECT *
                FROM sale
                WHERE id_customer IN (" . implode(',', $customers_ids) . ")
            ");

            $sale_history_to_return = $sale_history;
        }

        return $sale_history_to_return;
    }

    public static function get_accepted_sale(array $customers_ids)
    {
        $sale_history_to_return = [];

        if (!empty($customers_ids))
        {
            $sale_history = Sale_model::find_by_sql(
                "
                SELECT *
                FROM sale_history
                WHERE id_customer IN (" . implode(',', $customers_ids) . ")
                    AND status=2
            ");

            $sale_history_to_return = $sale_history;
        }

        return $sale_history_to_return;
    }
}