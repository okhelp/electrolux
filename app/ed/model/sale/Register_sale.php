<?php
declare(strict_types=1);

namespace App\ed\model\sale;

use App\ed\model\Sale_model;
use App\modules\products\front\models\Product_model;

class Register_sale
{
    /** @var int */
    private $product_id;

    /** @var int */
    private $count;

    /** @var int */
    private $id_customer;

    /** @var string */
    private $invoice_number;

    const STATUSES = [
        'do weryfikacji',
        'odrzucone',
        'potwierdzone'
    ];

    /**
     * Register_sale constructor.
     * @param int $product_id
     * @param int $count
     * @param int $id_customer
     */
    public function __construct(int $product_id, int $count, int $id_customer, $invoice_number)
    {
        $this->product_id = $product_id;
        $this->count = $count;
        $this->id_customer = $id_customer;
        $this->invoice_number = $invoice_number;
    }

    public function register(): bool
    {
        $product = Product_model::find_by_sql("
            SELECT *
            FROM ". Product_model::TABLE_NAME ."
            WHERE ". Product_model::COLUMN_ID ." = ". $this->product_id ."
        "); 

        if (!empty($product))
        {
            $product = reset($product);

            $sale = new Sale_model();
            $sale->{Sale_model::COLUMN_ID_PRODUCT} = $this->product_id;
            $sale->{Sale_model::COLUMN_INVOICE_NUMBER} = $this->invoice_number;
            $sale->{Sale_model::COLUMN_STATUS} = Sale_model::STATUS_VERIFICATION;
            $sale->{Sale_model::COLUMN_PRICE} = $product->{Product_model::COLUMN_PRICE};
            $sale->{Sale_model::COLUMN_POINTS} = (int)($product->{Product_model::COLUMN_POINTS} * $this->count);
            $sale->{Sale_model::COLUMN_COUNT} = $this->count;
            $sale->{Sale_model::COLUMN_ID_CUSTOMER} = $this->id_customer;

            return $sale->save();
        }

        return false;
    }

}