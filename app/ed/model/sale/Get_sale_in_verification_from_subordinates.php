<?php
declare(strict_types=1);

namespace App\ed\model\sale;

use App\ed\model\Sale_model;
use App\ed\model\users\Get_subordinate_companies;

class Get_sale_in_verification_from_subordinates
{
    public static function get(array $supervisors_ids)
    {
        $sale_to_return = [];

        if (!empty($supervisors_ids))
        {
            $subordinates_ids = Get_subordinate_companies::get($supervisors_ids);

            if (!empty($subordinates_ids))
            {
                $sales = Sale_model::find_by_sql(
                    "
                    SELECT *
                    FROM sale
                    WHERE id_customer IN (" . implode(',', $subordinates_ids) . ")
                        AND status=0
                ");

                if(!empty($sales))
                {
                    $sale_to_return = $sales;
                }
            }
        }

        return $sale_to_return;
    }
}