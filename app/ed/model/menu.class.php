<?php

class App__Ed__Model__Menu
{
    public static function default_menu()
    {
        $menu_arr['1dashboard'] = [
            [
                'name' => 'Tablica',
                'url'  => BASE_ED_URL . 'dashboard',
            ],
        ];

        $menu_arr['wiadomosci'] = [
            [
                'name' => 'Wiadomości',
                'url'  => '#',
                'submenu' => [
                    0 => [
                        'name' => 'Napisz',
                        'url'  => BASE_ED_URL . 'wiadomosci/napisz.html',
                    ],
                    1 => [
                        'name' => 'Odebrane',
                        'url'  => BASE_ED_URL . 'wiadomosci/odebrane.html',
                    ],
                ],
            ],
        ];

        if (App__Ed__Model__Acl::has_group('admin-electrolux', 0))
        {
            $menu_aray = [
                'bloki_menu'      => [
                    [
                        'name'    => 'Bloki menu',
                        'url'     => '#',
                        'submenu' => [
                            0 => [
                                'name' => 'Górne',
                                'url'  => BASE_ED_URL . 'block_menu/gora.html',
                            ],
                            1 => [
                                'name' => 'Stopka',
                                'url'  => BASE_ED_URL . 'block_menu/stopka.html',
                            ],
                        ],
                    ],
                ],
                'podstrony'       => [
                    [
                        'name'    => 'Podstrony',
                        'url'     => '#',
                        'submenu' => [
                            0 => [
                                'name' => 'Lista',
                                'url'  => BASE_ED_URL . 'pages/lista.html',
                            ],
                            1 => [
                                'name' => 'Kategorie',
                                'url'  => BASE_ED_URL . 'pages/categories/lista.html',
                            ],
                        ],
                    ],
                ],
                'uzytkownicy'     => [
                    [
                        'name'    => 'Użytkownicy',
                        'url'     => '#',
                        'submenu' => [
                            0 => [
                                'name' => 'Lista',
                                'url'  => BASE_ED_URL . 'user/lista.html?type=employers',
                            ],
                            1 => [
                                'name' => 'Uprawnienia',
                                'url'  => BASE_ED_URL . 'user/acl/lista.html',
                            ],
                        ],
                    ],
                ],
                'lokalizacja'     => [
                    [
                        'name' => 'Lokalizacja',
                        'url'  => BASE_ED_URL . 'localization/lista.html',
                    ],
                ],
                'kolumny'         => [
                    [
                        'name' => 'Kolumny',
                        'url'  => BASE_ED_URL . 'columns/lista.html',
                    ],
                ],
                'blokada_dostepu' => [
                    [
                        'name' => 'Blokada dostępu',
                        'url'  => BASE_ED_URL . 'ban/lista.html',
                    ],
                ],
                'tlumaczenie'     => [
                    [
                        'name' => 'Tłumaczenie',
                        'url'  => BASE_ED_URL . 'languages/lista.html',
                    ],
                ],
                'moduly'          => [
                    [
                        'name' => 'Moduły',
                        'url'  => BASE_ED_URL . 'modules/lista.html',
                    ],
                ],
                'menadzer_plikow' => [
                    [
                        'name' => 'Menadżer plików',
                        'url'  => BASE_ED_URL . 'files/lista.html',
                    ],
                ],
                'newsletter'      => [
                    [
                        'name' => 'Newsletter',
                        'url'  => BASE_ED_URL . 'newsletter/lista.html',
                    ],
                ],
                'produkty'        => [
                    [
                        'name'    => 'Produkty',
                        'url'     => '#',
                        'submenu' => [
                            [
                                'name' => 'Lista',
                                'url'  => BASE_ED_URL . 'products/lista.html',
                            ],
                            [
                                'name' => 'Kategorie',
                                'url'  => BASE_ED_URL . 'products/categories/lista.html',
                            ],
                        ],
                    ],
                ],
                'ustawienia'      => [
                    [
                        'name'    => 'Ustawienia',
                        'url'     => '#',
                        'submenu' => [
                            0 => [
                                'name' => 'Ustawienia główne',
                                'url'  => BASE_ED_URL . 'ustawienia/glowne.html',
                            ],
                            1 => [
                                'name' => 'Ustawienia e-mail',
                                'url'  => BASE_ED_URL . 'ustawienia/email.html',
                            ],
                            2 => [
                                'name' => 'Ustawienia wyglądu',
                                'url'  => BASE_ED_URL . 'ustawienia/wyglad.html',
                            ],
                            6 => [
                                'name' => 'Ustawienia zamówień',
                                'url'  => BASE_ED_URL . 'ustawienia/zamowienia.html',
                            ],
                            3 => [
                                'name' => 'Kody śledzace',
                                'url'  => BASE_ED_URL . 'ustawienia/kody_sledzace.html',
                            ],
                            4 => [
                                'name' => 'Optymalizacja',
                                'url'  => BASE_ED_URL . 'ustawienia/optymalizacja.html',
                            ],
                            5 => [
                                'name' => 'Social media',
                                'url'  => BASE_ED_URL . 'ustawienia/social_media.html',
                            ],
                        ],
                    ],
                ],
            ];

            $menu_arr += $menu_aray;
        }

        if (App__Ed__Model__Acl::has_access('subordinate_companies', 0))
        {
            $menu_arr['firmy_podrzedne'] = [
                [
                    'name'    => 'Firmy podrzędne',
                    'url'     => '#',
                    'submenu' => [
                        [
                            'name' => 'Rejestracja sprzedaży',
                            'url'  => BASE_ED_URL . 'company_supervising/',
                        ],
                        [
                            'name' => 'Zamówienia',
                            'url'  => BASE_ED_URL . 'order',
                        ],
                        [
                            'name' => 'Potwierdzanie współpracowników',
                            'url'  => BASE_ED_URL . 'company_supervising/co_workers_list.html',
                        ],
                        [
                            'name' => 'Moi klienci',
                            'url'  => BASE_ED_URL . 'my_customers',
                        ],
                    ],
                ],
            ];
        }

        if (App__Ed__Model__Acl::has_access('faq', 0))
        {
            $menu_arr['faq'] = [
                [
                    'name'    => 'FAQ',
                    'url'     => '#',
                    'submenu' => [
                        0 => [
                            'name' => 'Pytania',
                            'url'  => BASE_ED_URL . 'faq/lista.html',
                        ],
                        1 => [
                            'name' => 'Kategorie',
                            'url'  => BASE_ED_URL . 'faq/categories/lista.html',
                        ],
                    ],
                ],
            ];
        }

        if (App__Ed__Model__Acl::has_access('prizes', 0))
        {
            $menu_arr['nagrody'] = [
                [
                    'name'    => 'Nagrody',
                    'url'     => '#',
                    'submenu' => [
                        [
                            'name' => 'Lista',
                            'url'  => BASE_ED_URL . 'prizes/lista.html',
                        ],
                        [
                            'name' => 'Kategorie',
                            'url'  => BASE_ED_URL . 'prizes/categories/lista.html',
                        ],
                    ],
                ],
            ];
        }

        if (App__Ed__Model__Acl::has_access('statistic', 0))
        {
            $menu_arr['matomo'] = [
                [
                    'name' => 'Matomo',
                    'url' => '#',
                    'submenu' => [
                        [
                            'name' => 'Wszystkie dane',
                            'url' => BASE_URL . 'matomo/index.php',
                            'new_window' => true,
                        ],
                        [
                            'name' => 'Statystyki (Wykresy)',
                            'url' => BASE_ED_URL . 'statistics/index.html',
                        ],
                    ]
                ]
            ];
            $menu_arr['statistic'] = [
                [
                    'name'    => 'Statystyki (EXCEL)',
                    'url'     => BASE_ED_URL . 'statistic/generuj.html',
                ],
            ];
        }


        return $menu_arr;
    }

    public static function display_menu($custom_menu = [])
    {
        $default_menu = self::default_menu();
        $ed_menu = self::get_modules_menu();
        $menu = array_merge($default_menu, $ed_menu);
        ksort($menu);

        return $menu;
    }

    private static function get_modules_menu()
    {
        $id_service = (int)Lib__Session::get('id_service');
        $ed_menu = [];

        $sql = "SELECT mem . *, m.code_name
			FROM modules_ed_menu mem
			INNER JOIN modules m ON m.id = mem.id_module
			INNER JOIN modules_service ms ON m.id=ms.id_module
			WHERE m.status = 1 AND ms.id_service = $id_service
			ORDER BY mem.id_parent ASC, m.name ASC";
        $module_menu = App__Ed__Model__Modules__Ed_menu::find_by_sql($sql);

        if (!empty($module_menu))
        {
            foreach ($module_menu as $menu_item)
            {
                if ($menu_item->id_parent == 0)
                {
                    $ed_menu[$menu_item->code_name][$menu_item->id] = [
                        'name' => $menu_item->name,
                        'url'  => BASE_ED_URL . $menu_item->url,
                    ];
                }
                else
                {
                    $ed_menu[$menu_item->code_name][$menu_item->id_parent]['submenu'][] = [
                        'name' => $menu_item->name,
                        'url'  => BASE_ED_URL . $menu_item->url,
                    ];
                }
            }
        }

        return $ed_menu;
    }
}
