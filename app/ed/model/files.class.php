<?php

class App__Ed__Model__Files extends Lib__Base__Model
{
	static $table_name = 'files';
	static $primary_key = 'id';
	static $cache = TRUE;
	
	public function get_url()
	{
		$file_path = App__Ed__Model__Encryption::decode($this->path);
		$file_path = str_replace('./','',$file_path);
		return BASE_URL . $file_path;
	}
}