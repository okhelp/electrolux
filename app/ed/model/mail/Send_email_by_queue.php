<?php

namespace App\ed\model\mail;

use Smarty;

class Send_email_by_queue
{
    /**
     * @param string $email
     * @param string $subject
     * @param string $email_heading
     * @param string $email_content
     * @param int $priority
     * @return bool
     * @throws \SmartyException
     */
    public static function send(string $email, string $subject, string $email_heading, string $email_content, int $priority = Email_queue_model::PRIORITY_NORMAL): bool
    {
        if(empty($email))
        {
            return false;
        }

        $email = $_SERVER['HTTP_HOST'] == 'localhost' ? $_SERVER['SERVER_ADMIN'] : $email;

        $smarty = new Smarty();
        $smarty->assign('email_heading', $email_heading);
        $smarty->assign('email_content', $email_content);
        $content = $smarty->fetch(get_app_template_path() . '_emails/email.tpl');

        $queue = new Add_to_queue(
            $email,
            $subject,
            $content,
            $priority
        );

        return $queue->add();
    }
}