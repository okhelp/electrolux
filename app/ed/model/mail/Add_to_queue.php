<?php
declare(strict_types=1);

namespace App\ed\model\mail;

class Add_to_queue
{
    /** @var string */
    private $address;

    /** @var string */
    private $subject;

    /** @var string */
    private $content;

    /** @var int */
    private $priority;

    /**
     * Add_to_queue constructor.
     * @param string $address
     * @param string $subject
     * @param string $content
     * @param int $priority
     */
    public function __construct(string $address, string $subject, string $content, int $priority = Email_queue_model::PRIORITY_NORMAL)
    {
        $this->address = $address;
        $this->subject = $subject;
        $this->content = $content;
        $this->priority = $priority;
    }

    public function add(): bool
    {
        $queue = new Email_queue_model();
        $queue->{Email_queue_model::COLUMN_ADDRESS} = $this->address;
        $queue->{Email_queue_model::COLUMN_SUBJECT} = $this->subject;
        $queue->{Email_queue_model::COLUMN_CONTENT} = $this->content;
        $queue->{Email_queue_model::COLUMN_PRIORITY} = $this->priority;

        return $queue->save();
    }
}