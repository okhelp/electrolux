<?php
declare(strict_types=1);

namespace App\ed\model\mail;

use Lib__Base__Model;

class Email_queue_model extends Lib__Base__Model
{
    const STATUS_NEW    = 0;
    const STATUS_FAILED = 1;
    const STATUS_SENT   = 2;

    const PRIORITY_NORMAL      = 3;
    const PRIORITY_HIGH        = 2;
    const PRIORITY_IMMEDIATELY = 1;

    const COLUMN_ID             = 'id';
    const COLUMN_ADDRESS        = 'address';
    const COLUMN_SUBJECT        = 'subject';
    const COLUMN_CONTENT        = 'content';
    const COLUMN_SEND_STATUS    = 'send_status';
    const COLUMN_RETRY_ATTEMPTS = 'retry_attempts';
    const COLUMN_PRIORITY       = 'priority';
    const COLUMN_CREATED_AT     = 'created_at';
    const COLUMN_UPDATED_AT     = 'updated_at';

    const TABLE_NAME = 'email_queue';

    static $table_name = self::TABLE_NAME;
    static $primary_key = self::COLUMN_ID;
}