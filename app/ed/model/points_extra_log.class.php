<?php

class App__Ed__Model__Points_extra_log extends Lib__Base__Model
{
    static $table_name = "points_extra_log";
    static $primary_key = 'id';

    public static function add(int $userId, int $points): bool
    {
        $pointsExtraLog = new self;
        $pointsExtraLog->user_id = $userId;
        $pointsExtraLog->points = $points;
        return $pointsExtraLog->save();
    }

    public static function getPoints(int $userId): int
    {
        $points = self::find_by_sql(<<<SQL
            SELECT SUM(points) as sum_points FROM points_extra_log WHERE user_id = $userId
SQL
        );
        $points = reset($points);
        return $points->sum_points ?? 0;
    }
}