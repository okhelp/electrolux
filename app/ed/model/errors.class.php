<?php

class Errors extends Lib__Base__Model
{
    static $table_name = "errors";
    static $primary_key = 'id';
	
	public function decode_description()
    {
        return base64_decode($this->description);
    }
    
    public function get_user()
    {
        return $this->id_user ? App__Ed__Model__Users::get_data($this->id_user) : NULL;
    }
}