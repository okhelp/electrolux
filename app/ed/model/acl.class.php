<?php
/*
 * Model odpowiadjący za utworzenie praw dostępu dla poszczególnych grup, do odpowienich modułów i akcji. 
 * Klasa statyczna.
 * @author Paweł Otłowski
 */
class App__Ed__Model__Acl
{
	public static function has_group($role_name, $redirect=0)
	{
		$list = NULL;
		$id_user = Lib__Session::get('id');
		if($id_user)
		{
			$join = App__Ed__Model__Acl__Group::join("INNER JOIN acl_group_user ON (acl_group_user.id_group=acl_group.id)");
			$where = App__Ed__Model__Acl__Group::where("acl_group.name='$role_name'");
			$where->add("acl_group_user.id_user=".$id_user);
			$list = !empty(App__Ed__Model__Acl__Group::find($join,$where)[0]) ? App__Ed__Model__Acl__Group::find($join,$where)[0] : [];
		}
		
		if(!empty($redirect) && empty($list))
		{
			go_back('d|Nie posiadasz uprawnień do przeglądania tej części strony');
		}
		elseif(empty($redirect) && empty($list))
		{
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}
	
	public static function has_access($module, $redirect=1)
	{
		$list = NULL;
		$id_user = Lib__Session::get('id');
		if($id_user)
		{
			$join = App__Ed__Model__Acl__Module::join("INNER JOIN acl_group_module ON acl_module.id = acl_group_module.id_module")
					->add("INNER JOIN acl_group_user ON acl_group_user.id_group = acl_group_module.id_group");
			$where = App__Ed__Model__Acl__Module::where("acl_module.name = '$module'")
					->add("acl_group_user.id_user = ".$id_user);
			
			$list = App__Ed__Model__Acl__Module::find($join,$where);
		}
		
		if(!empty($redirect) && empty($list))
		{
			go_back('d|Nie posiadasz uprawnień do przeglądania tej części strony');
		}
		elseif(empty($redirect) && empty($list))
		{
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}
	
	public static function add_user_to_group($id_user,$id_group)
	{
		$connection = new App__Ed__Model__Acl__Group__User();
		$connection->id_user = (int)$id_user;
		$connection->id_group = (int)$id_group;
		$connection->save();
	}
	
	public static function add_user_to_group_module($id_group,$id_module)
	{
		$connection = new App__Ed__Model__Acl__Group__Module;
		$connection->id_group = $id_group;
		$connection->id_module = $id_module;
		$connection->save();
	}
}