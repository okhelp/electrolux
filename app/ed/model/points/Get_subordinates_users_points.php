<?php
declare(strict_types=1);

namespace App\ed\model\points;

use App\ed\model\company\Get_by_id;
use App\ed\model\company\Get_company_users;
use App\ed\model\users\Get_subordinate_companies;
use App\front\models\company\Get_companies;

class Get_subordinates_users_points
{
    public static function get(int $id_supervisor)
    {
        $subordinates_users_points_to_return = [];

        if (!empty($id_supervisor))
        {
            $subordinates_ids = Get_subordinate_companies::get([$id_supervisor]);
            $companies = Get_companies::get();
            if(!empty($companies))
            {
                $subordinates_ids = [];
                foreach($companies as $company)
                {
                    $subordinates_ids[] = $company->id;
                }
            }

            $companies_users = Get_company_users::get_by_ids($subordinates_ids);
            $companies = Get_by_id::get($subordinates_ids);

            foreach ($companies_users as $id_company => $company_users)
            {
                foreach ($company_users as $company_user)
                {
                    if (!empty($company_user->user))
                    {
                        $company_user->user->assign_attribute('company', $companies[$company_user->id_company]);
                        $subordinates_users_points_to_return[$company_user->user->id] = $company_user->user;
                        $subordinates_users_points_to_return[$company_user->user->id]->assign_attribute('points', \App__Ed__Model__Points_log::getPoints($company_user->user->id));
                        $subordinates_users_points_to_return[$company_user->user->id]->assign_attribute('extra_points', \App__Ed__Model__Points_extra_log::getPoints($company_user->user->id));
                    }
                }
            }
        }

        return $subordinates_users_points_to_return;
    }
}