<?php
declare(strict_types=1);

namespace App\ed\model\points;

use App\ed\model\Company_users_points;

class Get_users_points
{
    public static function get(array $company_users_ids): array
    {
        $points_to_return = [];

        if (!empty($company_users_ids))
        {
            $users_points = Company_users_points::find_by_sql(
                "
                    SELECT *
                    FROM company_users_points
                    WHERE company_user_id IN (" . implode(',', $company_users_ids) . ")
                ");

            if(!empty($users_points))
            {
                foreach ($users_points as $user_points)
                {
                    $points_to_return[$user_points->{Company_users_points::COLUMN_COMPANY_USER_ID}] = $user_points;
                }
            }
        }

        return $points_to_return;
    }

    public static function get_annual(array $user_ids): array
    {
        $points_to_return = [];

        if(!empty($user_ids))
        {
            $date = date('Y');
            $in = implode(',', $user_ids);
            $annual_user_points = Company_users_points::find_by_sql(<<<SQL
                SELECT u.id, 
                    COALESCE((SELECT SUM(points) FROM points_extra_log pel WHERE pel.user_id=u.id AND pel.points > 0), 0) + 
                    COALESCE((SELECT SUM(points) FROM points_log pl WHERE pl.user_id=u.id AND pl.points > 0), 0) - 
                    COALESCE((SELECT SUM(points) FROM annual_points_log apl WHERE apl.user_id=u.id), 0) as points 
                FROM users u
                WHERE u.`id` IN ($in)
                GROUP by u.`id`
SQL
            );

            if(!empty($annual_user_points))
            {
                foreach ($annual_user_points as $user_point)
                {
                    $points_to_return[$user_point->id] = $user_point->points ?: 0;
                }
            }
        }

        return $points_to_return;
    }

    public static function get_all(array $user_ids): array
    {
        $points_to_return = [];

        if(!empty($user_ids))
        {
            //punkty ze sprzedaży
            $all_form_sale = Company_users_points::find_by_sql("
                SELECT id_customer, sum(points) as points FROM `sale` 
                WHERE `id_customer` IN (". implode(',', $user_ids) .")  
                    AND `status` = 2 
                GROUP by `id_customer`
            ");

            if(!empty($all_form_sale))
            {
                foreach ($all_form_sale as $user_point)
                {
                    $points_to_return[$user_point->id_customer] = $user_point->points;
                }
            }

            //punkty otrzymane
            $receiver_points = Company_users_points::find_by_sql("
                SELECT id_receiver, SUM(receiver_points_after) as points
                FROM users_points_transfer_log
                WHERE id_receiver IN (". implode(',', $user_ids) .") 
                GROUP by id_receiver
            ");

            if(!empty($receiver_points))
            {
                foreach($receiver_points as $point)
                {
                    $sale_points = !empty($points_to_return[$point->id_receiver]) ? $points_to_return[$point->id_receiver] : 0;
                    $points_to_return[$point->id_receiver] = $sale_points + $point->points;
                }
            }
        }

        return $points_to_return;
    }
}