<?php

class App__Ed__Model__Img extends Lib__Base__Model
{
	static $table_name = 'img';
	static $primary_key = 'id';
    static $cache = TRUE;
	
	/**
	 * Id|nazwa kategorii cimgów
	 * @var array 
	 */
	static $cimg_cathegory = array(
		1 => 'users'
	);
	
	public static function create_id_cimg($file, $params=array())
	{
		//ładuję biblotekę do zmniejszania obórbki zdjęcia
		require_once(BASE_PATH.'lib/php-thumb/ThumbLib.inc.php');

		//pobieram nazwę rozszrzenia pliku
		$pathinfo = pathinfo($file);
		$file_extension = $pathinfo['extension'];

		//fix na pliki svg
		if($file_extension != 'svg' && $file_extension != 'webp')
        {
            //pobieram wielkść obrazka oraz jego położenie na serwerze
            $file = trim($file);
            if(substr($file,0,2) == './')
            {
                $file = substr($file,2); //ucinam "./" na początku
            }

            //określam rodzaj skalowania - adaptacyjnie czy sztywno
            $adaptive = (!isset($params['adaptive']) || !empty($params['adaptive'])) ? 1 : 0;

            //TRZEBA OBRÓCIĆ (@ dlaego, że na ta\ą chwile wspierane są formaty jpg i tif....)
            $exif = @exif_read_data($file);
            if(isset($exif['Orientation']) && !empty($exif['Orientation']))
            {
                $orientation = $exif['Orientation'];
                $image = PhpThumbFactory::create($file);

                switch ($orientation)
                {
                    case 3:
                        $image->rotateImageNDegrees(180);
                        break;
                    case 6:
                        $image->rotateImage('CCW');
                        break;
                    case 8:
                        $image->rotateImage('CW');
                        break;
                    default:
                        break;
                }

                $image->save($file);
            }

            //jeżeli nie mam podanej wielkosci w parametrze, pobieram org wielkosc obrazka
            if(!isset($params['height']) && !isset($params['weight']))
            {
                $img_size = getimagesize($file);
                $img_width = $img_size[0];
                $img_height = $img_size[1];
            }
            else
            {
                $img_height = $params['height'];
                $img_width = $params['width'];

                //tworzę strukturę katalogów i kopiuje plik w odpowiednie miejsce
                $dir_path = './data/uploads/images/'.$img_width.'x'.$img_height.'/';
                create_dirs($dir_path);

                //konwertuję scięzki plików do bezwzględnego położenia na serwerze
                $file_name = $adaptive . "_" .App__Ed__Model__Img::get_name($file);
                try {
                    $cimg = PhpThumbFactory::create($file);
                } catch (Exception $e) {
                    return BASE_DIR . 'data/img/no_photo.png';
                }
                $file = $dir_path.$file_name;

//                if (!file_exists($file)) {
//                    return BASE_DIR . 'data/img/no_photo.png';
//                }
                if($adaptive)
                {
                    $cimg->adaptiveResize($img_width, $img_height);
                }
                else
                {
                    $cimg->resize($img_width, $img_height);
                }

                $cimg->save($file);
                $file = substr($file,2); //ucinam "./" na początku
            }

            //może zdarzyć się przypadek kiedy źródło już istnieje w bazie i istnieje plik, wówczas zwracamy id,
            //jeżeli istnieje w bazie a nie na dysku to usuwamy wpis w bazie. Taki dupochron. Często podczas importów
            //robi sie smietnik.. <Paweł Otlowski>

            $find_file_in_db = App__Ed__Model__Img::find_by_file_name($file);
            if(is_file(BASE_PATH.$file) && !empty($find_file_in_db)) //mamy dane zwracam id_cimg
            {
                if(!empty($params['id_up']))
                {
                    if($params['id_up'] == $find_file_in_db->id_up)
                    {
                        return $find_file_in_db->id;
                    }
                    else
                    {
                        self::remove_cimg($find_file_in_db->id);
                    }
                }
                else
                {
                    return $find_file_in_db->id;
                }
            }

            if(!empty($find_file_in_db) && !is_file(BASE_PATH.$file))
            {
                $find_file_in_db->delete();
            }

        }
        else //info dla svg
        {
            $file = str_replace("./","", $file);
            $img_width = 0;
            $img_height = 0;
            $adaptive = 0;
        }
			

		//zapis do bazy
		$item = new App__Ed__Model__Img;
		$item->id_up = isset($params['id_up']) ? (int)$params['id_up'] : 0;
		$item->id_cathegory = isset($params['id_cathegory']) ? $params['id_cathegory'] : 0;
		$item->file_name = str_replace(BASE_PATH, '', $file);
		$item->width = $img_width;
		$item->height = $img_height;
		$item->adaptive = $adaptive;
		$item->author = '';
		$item->description = '';
		$item->save();
		
		//zapisuję i zwracam id_cimg
		return $item->id;
	}
	
	public static function remove_cimg($id_cimg)
	{
		$source = App__Ed__Model__Img::find_by_id($id_cimg);
		if($source)
		{
			//usuwam plik źródłowy
            if (file_exists(BASE_PATH.$source->file_name)) {
                unlink(BASE_PATH . $source->file_name);
            }
			
			//usuwam wpis w cimgu
			$source->delete();
		}
		
		//szukam powiązań
		$child_list = App__Ed__Model__Img::find(App__Ed__Model__Img::where("id_up=$id_cimg"));
        if(!empty($child))
		{
            foreach($child_list as $child)
            {
                //usuwam plik źródłowy
                if(file_exists(BASE_PATH.$child->file_name))
                {
                    unlink(BASE_PATH.$child->file_name);
                }

                //usuwam wpis w cimgu
                $child->delete();
            }
		}
	}
	
	public static function get_file($params)
	{
		$return = '';
		
		//zamieniam tablicę na zmienne
		$id_cimg = !empty($params['id_cimg']) ? (int)$params['id_cimg'] : 0;
		$width = !empty($params['width']) ? (int)$params['width'] : 0;
		$height = !empty($params['height']) ? (int)$params['height'] : 0;        
		$adaptive = (!isset($params['adaptive']) || !empty($params['adaptive'])) ? 1 : 0;

		$cache_key = 'Img__Cached__Cimg__' . implode('__', $params);

        $cache_data = Lib__Memcache::get($cache_key);
        if($cache_data)
        {
            //return $cache_data;
        }

		//szukam pliku w bazie
		if(!empty($id_cimg) && !empty($width) && !empty($height))
		{
		    $item = App__Ed__Model__Img::find(
						App__Ed__Model__Img::where("id_up=$id_cimg")
							->add("width='$width'")
							->add("height='$height'")
							->add("adaptive = $adaptive")
					);
			//tworzę odbrazek
			if(empty($item))
			{
				//pobieram zrodlo obrazka
				$file = App__Ed__Model__Img::find(App__Ed__Model__Img::where("id=$id_cimg")->add("id_up=0"));
                
                if(!empty($file))
                {
                    $file = reset($file);

                    //pobieram dane pliku
                    $pathinfo = pathinfo($file->file_name);

                    //jeżeli jest to plik svg zwracam jego źródło
                    if($pathinfo['extension'] == 'svg' || $pathinfo['extension'] == 'webp')
                    {
                        return BASE_URL . $file->file_name;
                    }
                    if (!file_exists($file->file_name)) {
                        return BASE_DIR . 'data/img/no_photo.png';
                    }
                    $id_cimg = App__Ed__Model__Img::create_id_cimg($file->file_name, [
                        'width' => $width,
                        'height' => $height,
                        'id_up' => $id_cimg,
						'adaptive' => $adaptive
                    ]);
                    if (!empty($id_cimg) && !is_numeric($id_cimg)) {
                        return $id_cimg;
                    }
					return self::get_file($params);
                }
                else
                {
//                    trigger_error("Brak żródła obrazka dla id_cimg:$id_cimg");
                    return BASE_URL . 'data/img/no_photo.png';
                }
				
			}
			else
			{
				$file = reset($item);
			}
            
            $return = '_uploads/images/'.$width.'x'.$height.'/'.self::get_name($file->file_name);
		}
		
		elseif(!empty($id_cimg))
		{
			//pobieram obrazek z bazy danych
			$item = App__Ed__Model__Img::find($id_cimg);
			if(!empty($item))
			{
				$return = $item->file_name;
			}
		}

        else
        {
            $return = "data/img/no_photo.png";
        }
		
		//jeżeli nie ma obrazka na serwerze pokazuję nophoto
		$file_path = BASE_PATH . str_replace('_uploads/','data/uploads/',$return);
		if(!is_file($file_path))
		{
			$return = "data/img/no_photo.png";
		}

        Lib__Memcache::set(
            $cache_key,
            BASE_URL . $return,
            [
                'class_name' => get_class(),
                'key_name'   => $cache_key,
            ]
        );

        return BASE_URL.$return;
	}
	
	public static function get_name($file=NULL)
	{
		if(!$file)
		{
			$file = $this->file_name;
		}
		$file = explode('/',$file);
		$file = array_reverse($file);
		return $file[0];
	}
	
	public function get_filesize($file=NULL)
	{
		if(!$file)
		{
			$file = $this->file_name;
		}
		
		$file_size = filesize($file);
		
		return round($file_size / 1000000, 2);
	}
}

