<?php

class App__Ed__Model__Modules extends Lib__Base__Model
{
	static $table_name = 'modules';
	static $primary_key = 'id';
	
	public function get_settings($only_instance=0)
	{
		$return_settings = array();
		
		if($this->settings_enable)
		{
			$id_service = (int)Lib__Session::get('id_service');

			$where = Modules__App__Ed__Model__Service::where("id_service = $id_service")
					->add("id_module=" . $this->id);

			$module_service = Modules__App__Ed__Model__Service::find($where);
			
			if(!empty($module_service))
			{
				$module_service = reset($module_service);
				
				if($only_instance)
				{
					return $module_service;
				}
				
				$settings = $module_service->settings;
				
				if(is_json($settings))
				{
					$return_settings = json_decode($settings, TRUE);
				}
				
			}
		}
		
		return $return_settings;
		
	}

    public function get_info()
    {
        $class_name = "App__Modules__" . $this->code_name . '__Index';

        $module_info['name'] = !empty($class_name::NAME) ? $class_name::NAME : '';
        $module_info['description'] = !empty($class_name::DESCRIPTION) ? $class_name::DESCRIPTION : '';
        $module_info['version'] = !empty($class_name::VERSION) ? $class_name::VERSION : '';
        $module_info['author'] = !empty($class_name::AUTHOR) ? $class_name::AUTHOR : '';

        return $module_info;
    }
}