<?php

class Customers__Invoices extends Lib__Base__Model
{
	static $table_name = 'customers_invoices';
	static $primary_key = 'id';
	
	public function get_customer()
	{
		$customer = NULL;
		
		if(!empty($this->id_customer))
		{
			$customer = App__Ed__Model__Customers::find($this->id_customer);
		}
		else
		{
			$customer = new App__Ed__Model__Customers;
		}
		
		return $customer;
	}
	
	public function get_positions()
	{
		$positions = array();
		
		if(!empty($this->position))
		{
			$positions = json_decode($this->position,TRUE);
		}
		
		return $positions;
	}
}