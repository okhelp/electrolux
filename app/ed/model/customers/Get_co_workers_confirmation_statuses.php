<?php
declare(strict_types=1);

namespace App\ed\model\customers;

class Get_co_workers_confirmation_statuses
{
    /**
     * @param int[] $companies_ids
     * @return array
     * @throws \ActiveRecord\RecordNotFound
     */
    public static function get_by_companies_ids(array $companies_ids): array
    {
        $co_workers_statuses_to_return = [];

        if (!empty($companies_ids))
        {
            $co_workers_statuses = Co_workers_confirmation_status::find(
                Co_workers_confirmation_status::where("id_company IN (" . implode(',', $companies_ids) . ")")
            );

            if(!empty($co_workers_statuses))
            {
                $co_workers_statuses_to_return = $co_workers_statuses;
            }
        }

        return $co_workers_statuses_to_return;
    }
}