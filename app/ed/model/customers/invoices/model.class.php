<?php

class Customers__Invoices__Model
{
	public static function get_payments_method()
	{
		return array(
			'gotowka' => 'gotówka',
			'przelew' => 'przelew',
			'online' => 'płatność on-line'
		);
	}
	
	public static function generate_number()
	{
		$number_start = 1;
		$prefix = "FISTO ";
		$separator = "/";
		
		//pobieram listę faktur, które zostały wystawione w tym miesiącu
		$invoices = Customers__Invoices::find(
			Customers__Invoices::where("MONTH(date_issue) = " . date('m'))	
				->add("YEAR(date_issue) = " . date('Y'))
		);
		
		if(!empty($invoices))
		{
			$number = count($invoices);
		}
		else
		{
			$number = $number_start;
		}
		
		return $prefix . $number . $separator . date('m') . $separator . date('Y');
		
	}
	
	public static function calculate_total_values($positions)
	{
		$total_value = array();
		
		if(!empty($positions))
		{
			$total_value['netto'] = 0.00;
			$total_value['vat'] = 0.00;
			$total_value['brutto'] = 0.00;
			
			foreach(json_decode($positions, TRUE) as $item)
			{
				$total_value['netto'] += $item['value_netto'];
				$total_value['vat'] += $item['value_vat'];
				$total_value['brutto'] += $item['value_brutto'];
			}
		}
		
		return $total_value;
		
	}
	
	public static function get_pdf(Customers__Invoices $invoice)
	{
		$file_name = 'FAKTURA ' . str_replace('/','_',$invoice->number);
		PDF::generate($file_name, $invoice, 'customers/faktury/szablon.tpl');
	}
}