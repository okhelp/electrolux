<?php
declare(strict_types=1);

namespace App\ed\model\customers;

use Lib__Base__Model;

class Co_workers_confirmation_status extends Lib__Base__Model
{
    const COLUMN_ID_CO_WORKER   = 'id_co_worker';
    const COLUMN_CO_WORKER_NAME = 'co_worker_name';
    const COLUMN_COMPANY_NAME   = 'company_name';
    const COLUMN_ID_COMPANY     = 'id_company';
    const COLUMN_STATUS         = 'status';
    const COLUMN_CREATED_AT     = 'created_at';
    const COLUMN_UPDATED_AT     = 'updated_at';

    const TABLE_NAME = 'co_workers_confirmation_status';

    static $table_name = self::TABLE_NAME;
}