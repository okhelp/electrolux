<?php

class App__Ed__Model__Error__List
{
    public static function get_list($type='no_resolve')
    {
        
        $status = $type=='no_resolve' ? 0 : 1;
        $where = App__Ed__Model__Error::where("status=$status");
        $order = App__Ed__Model__Error::order("last_time DESC");
        $list = App__Ed__Model__Error::find($where,$order);

        return !empty($list) ? $list : array();
    }
}