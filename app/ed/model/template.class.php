<?php 

class Ed__Template
{
	public static $template_path = BASE_PATH . 'templates/';
	
	public static function get_list($only_demo = false)
	{
		$template_list = array();
		foreach(scandir(self::$template_path) as $template_name)
		{
			if($template_name != '.' && $template_name != '..' && $template_name != 'ed')
			{
				//pokazuję tylko nasze
				if(!empty($only_demo) && !is_int(strpos($template_name,'szablon_')))
				{
					continue;
				}
				
				//pokazuję pozostałe
				if(empty($only_demo) && is_int(strpos($template_name,'szablon_')))
				{
					continue;
				}
				
				$template_list[$template_name] = array(
					'name' => self::get_name($template_name),
					'author' => self::get_author($template_name),
					'screenshot' => self::get_screenshot($template_name),
					'id_default_service' => self::get_id_default_service($template_name),
					'active' => self::is_active($template_name)
				);
			}
		}
		return $template_list;
	}
	
	public static function is_active($template_dir)
	{
		return Lib__Session::get('template') === $template_dir;
	}
	
	public static function get_name($template_dir)
	{
		$name = "Domyślny szablon";
		
		$template_dir = self::$template_path . $template_dir . '/_template/';
		$name_file = $template_dir . 'name.txt';
		if(is_dir($template_dir) && is_file($name_file))
		{
			$name = trim(file_get_contents($name_file));
		}
		
		return $name;
	}
	
	
	public static function get_author($template_dir)
	{
		$author = "&copy; goinweb.pl";
		
		$template_dir = self::$template_path . $template_dir . '/_template/';
		$author_file = $template_dir . 'author.txt';
		if(is_dir($template_dir) && is_file($author_file))
		{
			$author = trim(file_get_contents($author_file));
		}
		
		return $author;
	}
	
	public static function get_screenshot($template_dir)
	{
		$screenshot = NULL;
		
		$template_dir = self::$template_path . $template_dir . '/_template/';
		$screenshot_file = $template_dir . 'screenshot.jpg';
		if(is_dir($template_dir) && is_file($screenshot_file))
		{
			$screenshot = str_replace(BASE_PATH,BASE_URL,$screenshot_file);
		}
		
		return $screenshot;
	}
	
	public static function get_id_default_service($template_dir)
	{
		$id_default_service = 0;
		
		$template_dir = self::$template_path . $template_dir . '/_template/';
		$id_default_service_file = $template_dir . 'id_service.txt';
		if(is_dir($template_dir) && is_file($id_default_service_file))
		{
			$id_default_service = trim(file_get_contents($id_default_service_file));
		}
		
		return $id_default_service;
	}
}