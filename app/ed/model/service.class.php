<?php

class App__Ed__Model__Service extends Lib__Base__Model
{
	static $table_name = 'service';
	static $primary_key = 'id'; 
    
    public static function get_service($service_name='all')
    {
        $items = array();
        //pobieram listę serwisów, które są przypisane do użytkownika
        $services_list = self::get_connected_services();

        if(!empty($services_list))
        {
            $services_list = implode(',', $services_list);

            $where = App__Ed__Model__Service::where("id IN ($services_list)");

            if($service_name != 'all')
            {
                $where->add("name='$service_name'");
                $items = App__Ed__Model__Service::find($where);
            }
            else
            {
                $items = App__Ed__Model__Service::find($where);
            }

            if(!empty($items))
            {
                $items_return = array();
                foreach($items as $k=>$v)
                {
                    $items_return[$v->id] = $v;
                }
                $items = $items_return;
            }
        }


        return $items;
    }
    
    public function get_connect_data($service_name=NULL)
    {
        $return_data = array();
        $services_data = self::get_service($service_name);
        
        foreach($services_data as $r)
        {
            //odkodowuje dane do logowania
            $data = $r->connect_data;
            $data = base64_decode($data);
            $data = unserialize($data);
            
            //zapisuję tablicę z danymi do logowania
            $return_data[$r->name] = $data;
        }
        
        return $return_data;
    }

    public static function get_connected_services($id_user = 0)
    {
        $return_data = array();
        $id_user = !empty($id_user) ? $id_user : (int)Lib__Session::get('id');

        if(!empty($id_user))
        {
            $find = App__Ed__Model__Users__Service::find(App__Ed__Model__Users__Service::where("id_user = " . $id_user));
            if(!empty($find))
            {
                foreach($find as $service)
                {
                    $return_data[] = $service->id_service;
                }
            }
        }

        return $return_data;
    }
	
	public function get_customer()
	{
		return App__Ed__Model__Customers::find($this->id_customer);
	}
	
	public function get_template_path()
	{
		return BASE_PATH . 'templates/' . $this->template .'/';
	}
	
	public function sign_in()
	{
		//dostęp jest tylko dla admina fisto
		if(!is_fisto_admin())
		{
			go_back("d|Brak uprawnień");
		}
		
		//określam id firmy, na jaką ma nastąpić przelogowanie
		$id_company = $this->get_customer()->id;
		
		//sprawdzam do jakiej firmy należy aktualny użytkownik
		$id_company_current_user = App__Ed__Model__Users::find(Lib__Session::get('id'))->id_company;
		
		//jeżeli ma nastąpić przekierowanie do serwisu innej firmy, sprawdzam czy to jest admin fisto
		if($id_company != $id_company_current_user && !is_fisto_admin())
		{
			go_back("d|Brak uprawnień.");
		}
		
		$admin_service = $this->get_service_admin();
		
		if(empty($admin_service))
		{
			go_back("d|Nie znaleziono konta administratora tego serwisu.");
		}
		
		$new_service_lang = App__Ed__Model__Language::find($this->id_lang);
		
		$new_service_data = array(
			'id_service' => $this->id,
			'default_lang' => $this->id_lang,
			'service_name' => $this->name,
			'template' => $this->template
		);
		Lib__Session::set($new_service_data);
		
		$history_logger = Lib__Session::get('history_logger');
		$new_history = array(Lib__Session::get());
		$history_logger = array_merge($history_logger,$new_history);
		Users__Auth::set_session($admin_service,$history_logger);
		
		Lib__Session::remove('id_lang');
		
		
		go_back('g|Zostałeś poprawnie przelogowany', BASE_ED_URL);
	}
	
	public function get_service_admin()
	{
		$return_data = new App__Ed__Model__Users;
		$pdo = new Lib__Pdo;
		$sql = "SELECT u.id 
				FROM users u 
				INNER JOIN acl_group_user agu ON u.id = agu.id_user
				INNER JOIN acl_group_module agm ON agm.id_group = agu.id_group AND id_module = 2
				WHERE agu.id_group = 1 AND u.id_company = " . $this->id_customer;
		$find_admin = $pdo->query($sql);
		
		if(!empty($find_admin))
		{
			$id_admin = reset($find_admin)['id'];
			$user_data = App__Ed__Model__Users::find($id_admin);
			if(!empty($user_data))
			{
				$return_data = $user_data;
			}
		}
		
		return $return_data;
	}
	
	public function get_stats()
    {
		$return_data = array();
		
		if(!empty($this->domain))
		{
			$directadmin = new Directadmin;
			$disk_usage = $directadmin->get_stats($this->domain);
			
			if($disk_usage['type'] == 'success')
			{
				$return_data['disk_usage'] = $disk_usage['description']['disk_usage'];
			}
		}
		
		return $return_data;
	}
	
	public function get_email_password()
	{
		$email_password = '';
		
		if(!empty($this->email_pass))
		{
			$email_password = App__Ed__Model__Encryption::decode($this->email_pass);
		}
		
		return $email_password;
	}

    public static function change_service($id_service)
    {
        //@TODO - sprawdzić czy użytkownik ma prawa do tego serwisu
        Lib__Session::set('id_service', $id_service);

    }
}