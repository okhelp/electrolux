<?php

class App__Ed__Model__Block
{
	public static function get($params)
	{
		if(is_array($params))
		{
			$name = $params['name'];
			unset($params['name']);
		}
		else
		{
			$name = $params;
		}
		$class_name = "App__Ed__Model__Block__".ucfirst($name);
		$class = new $class_name;
		return $class->execute($params);
	}
}