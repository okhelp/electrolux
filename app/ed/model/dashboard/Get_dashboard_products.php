<?php
declare(strict_types=1);

namespace App\ed\model\dashboard;

use SSP;

class Get_dashboard_products
{
    public static function get(int $id_service, string $interval, array $customers_ids, array $supervisors_ids): string
    {
        $table = 'dashboard_products';
        $primaryKey = 'id';
        $columns = [
            ['db' => 'invoice_number', 'dt' => 0],
            ['db' => 'product_name', 'dt' => 1],
            ['db' => 'product_model', 'dt' => 2],
            ['db' => 'sale_status', 'dt' => 3],
            ['db' => 'count', 'dt' => 4],
            ['db' => 'price', 'dt' => 5],
            ['db' => 'points', 'dt' => 6],
            ['db' => 'customer_name', 'dt' => 7],
            ['db' => 'supervisor_name', 'dt' => 8],
            [
                'db'        => 'sale_date',
                'dt'        => 9,
                'formatter' => function($d, $row) {
                    return date('Y-m-d H:i:s', strtotime($d));
                },
            ],
        ];

        $service_condition = " id_service= $id_service ";

        $period_condition = self::parse_interval($interval);

        $customers_condition = empty($customers_ids) ? '' : " AND id_customer IN (" . implode(',', $customers_ids) . ") ";

        $supervisors_condition = empty($supervisors_ids) ? '' : " AND id_supervisor IN (" . implode(',', $supervisors_ids) . ") ";

        $where = implode(' ', [$service_condition, $period_condition, $customers_condition, $supervisors_condition]);

        $response = json_encode(
            SSP::complex(
                $_POST,
                $table,
                $primaryKey,
                $columns,
                null,
                null,
                $where
            )
        );

        return $response;
    }

    private static function parse_interval(string $period): string
    {
        $period_condition = '';

        switch ($period)
        {
            case 'day':
                $period_condition = " AND DATE(sale_date) = CURDATE() ";
                break;
            case 'week':
                $period_condition = " AND sale_date BETWEEN NOW() - INTERVAL 7 DAY AND NOW() ";
                break;
            case 'month':
                $period_condition = " AND sale_date BETWEEN NOW() - INTERVAL 1 MONTH AND NOW() ";
                break;
            case 'year':
                $period_condition = " AND sale_date BETWEEN NOW() - INTERVAL 1 YEAR AND NOW() ";
                break;
        }

        return $period_condition;
    }
}