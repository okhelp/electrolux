<?php
declare(strict_types=1);

namespace App\ed\model\dashboard;

use App\ed\model\Company_users_points;

class Get_top_five
{
    public static function get(string $type, $id_service)
    {
        switch ($type)
        {
            case 'customer':
                return self::get_customers($id_service);
                break;
            case 'supervisor':
                return self::get_supervisors($id_service);
                break;
            case 'product':
                return self::get_products($id_service);
                break;
            case 'prize':
                return self::get_prizes($id_service);
                break;
        }
    }

    private static function get_customers(int $id_service): array
    {
        $customers_to_return = [];

        $customers = Company_users_points::find_by_sql("
            SELECT CONCAT(users.name, ' ', users.surname) AS name, users.id_electrolux
            FROM company_users_points
            JOIN users ON users.id=company_users_points.company_user_id
            JOIN users_service ON users_service.id_user=users.id
            WHERE users_service.id_service=$id_service
            ORDER BY points DESC
            LIMIT 5
        ");

        if(!empty($customers))
        {
            $customers_to_return = $customers;
        }

        return $customers_to_return;
    }

    private static function get_supervisors(int $id_service): array
    {
        $supervisors_to_return = [];

        $supervisors = Company_users_points::find_by_sql("
            SELECT SUM(company_users_points.points), CONCAT(supervisor.name, ' ', supervisor.surname) AS name, users.id_electrolux
            FROM company_users_points
            JOIN users ON users.id=company_users_points.company_user_id
            JOIN users_service ON users_service.id_user=users.id
            JOIN company_users ON company_users.id_user=users.id
            JOIN company_supervisor ON company_supervisor.id_company=company_users.id_company
            JOIN users supervisor ON supervisor.id=company_supervisor.id_supervisor
            WHERE users_service.id_service=$id_service AND users.id NOT IN (1, 1363, 1364)
            GROUP BY supervisor.id
            ORDER BY points DESC
            LIMIT 5
        ");

        if(!empty($supervisors))
        {
            $supervisors_to_return = $supervisors;
        }

        return $supervisors_to_return;
    }

    private static function get_products(int $id_service): array
    {
        $products_to_return = [];

        $products = Company_users_points::find_by_sql("
            SELECT CONCAT(product_name, ' (', product_model, ')') AS name, count(product_id) AS counts
            FROM dashboard_products
            WHERE id_service=$id_service
            GROUP BY name
            ORDER BY counts DESC
            LIMIT 5
        ");

        if(!empty($products))
        {
            $products_to_return = $products;
        }

        return $products_to_return;
    }

    private static function get_prizes(int $id_service): array
    {
        $prizes_to_return = [];

        $prizes = Company_users_points::find_by_sql("
            SELECT CONCAT(prize_name) AS name, count(id_prize) AS counts
            FROM " . DB_NAME . ".dashboard_prizes
            WHERE id_service=$id_service
            GROUP BY name
            ORDER BY counts DESC
            LIMIT 5
        ");

        if(!empty($prizes))
        {
            $prizes_to_return = $prizes;
        }

        return $prizes_to_return;
    }
}