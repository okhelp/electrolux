<?php
declare(strict_types=1);

namespace App\ed\model\dashboard;

use Lib__Base__Model;

class Statistics_model extends Lib__Base__Model
{
    const COLUMN_PREMIUM_USERS_ALL               = 'premium_users_all';
    const COLUMN_PREMIUM_USERS_IN_LAST_MONTH     = 'premium_users_in_last_month';
    const COLUMN_PREMIUM_USERS_IN_PREVIOUS_MONTH = 'premium_users_in_previous_month';
    const COLUMN_USERS_ALL                       = 'users_all';
    const COLUMN_USERS_IN_LAST_MONTH             = 'users_in_last_month';
    const COLUMN_USERS_IN_PREVIOUS_MONTH         = 'users_in_previous_month';
    const COLUMN_SALE_ALL                        = 'sale_all';
    const COLUMN_SALE_IN_LAST_MONTH              = 'sale_in_last_month';
    const COLUMN_SALE_IN_PREVIOUS_MONTH          = 'sale_in_previous_month';
    const COLUMN_ORDERS_ALL                      = 'orders_all';
    const COLUMN_ORDERS_IN_LAST_MONTH            = 'orders_in_last_month';
    const COLUMN_ORDERS_IN_PREVIOUS_MONTH        = 'orders_in_previous_month';
    const COLUMN_PRODUCTS_ALL                    = 'products_all';
    const COLUMN_PRODUCTS_IN_LAST_MONTH          = 'products_in_last_month';
    const COLUMN_PRODUCTS_IN_PREVIOUS_MONTH      = 'products_in_previous_month';
    const COLUMN_PRIZES_ALL                      = 'prizes_all';
    const COLUMN_PRIZES_IN_LAST_MONTH            = 'prizes_in_last_month';
    const COLUMN_PRIZES_IN_PREVIOUS_MONTH        = 'prizes_in_previous_month';
    const COLUMN_CUSTOMERS_ACTIVE                = 'users_active';
    const COLUMN_CUSTOMERS_ALL                   = 'customers_all';

    static $table_name = 'electrolux_dashboard_statistics';
}