<?php
declare(strict_types=1);

namespace App\ed\model\dashboard;

use Lib__Base__Model;

class Dashboard_products_model extends Lib__Base__Model
{
    const COLUMN_PRODUCT_NAME    = 'product_name';
    const COLUMN_PRODUCT_MODEL   = 'product_model';
    const COLUMN_ID_SERVICE      = 'id_service';
    const COLUMN_CUSTOMER_NAME   = 'customer_name';
    const COLUMN_ID_CUSTOMER     = 'id_customer';
    const COLUMN_SALE_STATUS     = 'sale_status';
    const COLUMN_SALE_DATE       = 'sale_date';
    const COLUMN_ID_SUPERVISOR   = 'id_supervisor';
    const COLUMN_SUPERVISOR_NAME = 'supervisor_name';
    const COLUMN_COUNT           = 'count';
    const COLUMN_PRICE           = 'price';
    const COLUMN_POINTS          = 'points';

    const TABLE_NAME = 'dashboard_products';

    static $table_name = self::TABLE_NAME;
}