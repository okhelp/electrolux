<?php
declare(strict_types=1);

namespace App\ed\model\dashboard;

use App\front\models\company\Get_company_services;

class Get_dashboard_statistics
{
    /** @var int */
    private $id_service;

    /**
     * Get_dashboard_statistics constructor.
     * @param int $id_service
     */
    public function __construct(int $id_service)
    {
        $this->id_service = $id_service;
    }

    public function get(): array
    {
        return $this->get_data();
    }

    private function get_data(): array
    {
        $data = [];

        $service_name = $this->id_service == Get_company_services::ID_SERVICE_ELECTROLUX ? 'electrolux' : 'aeg';

        $statistics = Statistics_model::find_by_sql(
            "
            SELECT *
            FROM {$service_name}_dashboard_statistics
        ");

        if (!empty($statistics))
        {
            $statistics = $statistics[0];

            $data = [
                [
                    'label'    => 'Ilość wszystkich kont',
                    'count'    => $statistics->{Statistics_model::COLUMN_CUSTOMERS_ALL}
                ],
                [
                    'label'    => 'Ilość klientów Premium',
                    'count'    => $statistics->{Statistics_model::COLUMN_PREMIUM_USERS_ALL},
                    'percents' => $this->calculate_percents(
                        (int)$statistics->{Statistics_model::COLUMN_PREMIUM_USERS_IN_PREVIOUS_MONTH}, (int)$statistics->{Statistics_model::COLUMN_PREMIUM_USERS_IN_LAST_MONTH}),
                ],
                [
                    'label'    => 'Ilość współpracowników',
                    'count'    => $statistics->{Statistics_model::COLUMN_USERS_ALL},
                    'percents' => $this->calculate_percents(
                        (int)$statistics->{Statistics_model::COLUMN_USERS_IN_PREVIOUS_MONTH}, (int)$statistics->{Statistics_model::COLUMN_USERS_IN_LAST_MONTH}),
                ],
                [
                    'label'    => 'Ilość aktywowanych kont',
                    'count'    => $statistics->{Statistics_model::COLUMN_CUSTOMERS_ACTIVE}
                ],
                [
                    'label'    => 'Rejestracje sprzedaży',
                    'count'    => $statistics->{Statistics_model::COLUMN_SALE_ALL},
                    'percents' => $this->calculate_percents(
                        (int)$statistics->{Statistics_model::COLUMN_SALE_IN_PREVIOUS_MONTH}, (int)$statistics->{Statistics_model::COLUMN_SALE_IN_LAST_MONTH}),
                ],
                [
                    'label'    => 'Zamówienia',
                    'count'    => $statistics->{Statistics_model::COLUMN_ORDERS_ALL},
                    'percents' => $this->calculate_percents(
                        (int)$statistics->{Statistics_model::COLUMN_ORDERS_IN_PREVIOUS_MONTH}, (int)$statistics->{Statistics_model::COLUMN_ORDERS_IN_LAST_MONTH}),
                ],
                [
                    'label'    => 'Ilość produktów',
                    'count'    => $statistics->{Statistics_model::COLUMN_PRODUCTS_ALL},
                    'percents' => $this->calculate_percents(
                        (int)$statistics->{Statistics_model::COLUMN_PRODUCTS_IN_PREVIOUS_MONTH}, (int)$statistics->{Statistics_model::COLUMN_PRODUCTS_IN_LAST_MONTH}),
                ],
                [
                    'label'    => 'Ilość nagród',
                    'count'    => $statistics->{Statistics_model::COLUMN_PRIZES_ALL},
                    'percents' => $this->calculate_percents(
                        (int)$statistics->{Statistics_model::COLUMN_PRIZES_IN_PREVIOUS_MONTH}, (int)$statistics->{Statistics_model::COLUMN_PRIZES_IN_LAST_MONTH}),
                ],

            ];
        }

        return $data;
    }

    private function calculate_percents(int $previous_month, int $actual_month): int
    {
        if(empty($previous_month) && !empty($actual_month))
        {
            $result = (int)$actual_month * 100;
        }
        else if (!empty($previous_month) && empty($actual_month)) {
            $result = (int)-$previous_month * 100;
        }
        else if (!empty($previous_month) && !empty($actual_month))
        {
            $result = (int)(($actual_month - $previous_month) / $previous_month) * 100;
        }
        else {
            $result = 0;
        }

        return $result;
    }

}