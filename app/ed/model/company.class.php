<?php

use App\ed\model\company\Get_company_supervisor;

class App__Ed__Model__Company extends Lib__Base__Model
{
    static $table_name = "company";
    static $primary_key = 'id';

    public static function get_by_ids(array $companies_ids)
    {
        $companies_to_return = [];

        if(!empty($companies_ids))
        {
            $companies = App__Ed__Model__Company::find_by_sql("
                SELECT *
                FROM company
                WHERE id IN (" . implode(',', $companies_ids) . ")
            ");

            foreach($companies as $company)
            {
                $companies_to_return[$company->id] = $company;
            }
        }

        return $companies_to_return;
    }

    public function get_supervisor()
    {
        return Get_company_supervisor::get((int)$this->id);
    }
}