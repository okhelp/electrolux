<?php

class App__Ed__Model__Messages extends Lib__Base__Model
{
    static $table_name = "messages";
    static $primary_key = 'id';

    public function get_user_folder($folder_name)
    {
        if($folder_name == 'inbox')
        {
            $id_user = $this->id_from;
        }
        elseif($folder_name == 'sent')
        {
            $id_user = $this->id_to;
        }

        return App__Ed__Model__Users::find($id_user);
    }

    public function get_user($id_user)
    {
        return App__Ed__Model__Users::find($id_user);
    }

    public function get_url($folder_name)
    {
        $hash = App__Ed__Model__Encryption::encode($this->id . "|" . $this->id_from . "|" . $this->id_to . "|" . $folder_name);
        $hash = urlencode($hash);
        return (is_ed() ? BASE_ED_URL : BASE_URL) . 'wiadomosci/pokaz.html?m=' . $hash;
    }

    public function get_attachments_list()
    {
        $return_data = array();

        if(!empty($this->attachments))
        {
            foreach(json_decode($this->attachments, TRUE) as $attachment)
            {
                $file = App__Ed__Model__Encryption::decode($attachment);
                $return_data[] = $file;
            }
        }

        return $return_data;
    }

    public function set_as_read()
    {
        $message = App__Ed__Model__Messages::find($this->id);
        $message->view_to = 1;
        $message->save();
    }

    public function is_important($folder_name)
    {
        return $folder_name == 'inbox' ? (bool)$this->important_to : (bool)$this->important_from;
    }

    public function get_important_hash($folder_name)
    {
        return App__Ed__Model__Encryption::encode($this->id .'|' . $folder_name);
    }
}