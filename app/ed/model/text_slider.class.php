<?php

class App__Ed__Model__Text_Slider extends Lib__Base__Model
{
	static $table_name = 'text_slider';
	static $primary_key = 'id';
	
	public function get_positions()
	{
		$return_data = array();
		
		if(!empty($this->id) && !empty($this->position))
		{
			$return_data = json_decode($this->position,TRUE);
		}
		
		return $return_data;
	}
}