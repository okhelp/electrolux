<?php

use App\ed\model\mail\Email_queue_model;
use App\ed\model\mail\Send_email_by_queue;

class App__Ed__Model__Users extends Lib__Base__Model
{
    static $table_name = "users";
    static $belongs_to = array(
        array('acl_group_module', 'class_name' => 'App__Ed__Model__Acl__Group__Module'),
        array('acl_group_user', 'class_name' => 'App__Ed__Model__Acl__Group__User')
    );

    public static function is_logged()
    {
        $id_user = Lib__Session::get('id');

//        if(!empty($id_user)) //jeżeli jest zalogowany, sprawdzam hasło pod kątem RODO
//        {
//            $password_rodo = new App__Ed__Model__Users__Password_Rodo(App__Ed__Model__Users::find($id_user));
//            if(uri_segment(2) != 'login' && $password_rodo->check())
//            {
//                //url dla ed
//                if(is_ed())
//                {
//                    $rodo_url = BASE_ED_URL . 'login/zmien_haslo.html';
//                }
//                else
//                {
//                    $rodo_url = BASE_URL . 'login/zmien_haslo.html';
//                }
//
//                redirect($rodo_url);
//            }
//        }

        return $id_user ? TRUE : FALSE;
    }

    public static function id2name($id_user)
    {
        $return = '';
        if(!empty($id_user))
        {
            $item = App__Ed__Model__Users::find_by_id($id_user);
            if(!empty($item))
            {
                $return = $item->name.' '.$item->surname;
            }
        }

        return $return;
    }

    public function send_email($type, $params=array())
    {
        $email = new App__Ed__Model__Users__Email($this,$params);
        $method_name = "send_$type";
        $email_data = $email->{$method_name}();
        Send_email_by_queue::send($this->email, $email_data['title'], $email_data['title'], $email_data['view'], Email_queue_model::PRIORITY_IMMEDIATELY);
    }

    public static function get_data($id_user=0)
    {
        $id_user = !empty($id_user) ? $id_user : (int) Lib__Session::get('id');
        return App__Ed__Model__Users::find($id_user);
    }

    public function has_access_service($id_service=0)
    {
        $return = FALSE;
        $id_service = !empty($id_service) ? $id_service : Lib__Session::get('id_service');

        $find = App__Ed__Model__Users__Service::find(
            App__Ed__Model__Users__Service::where("id_user = " . $this->id)
                                          ->add("id_service = " . $id_service)
        );

        if(!empty($find))
        {
            $return = TRUE;
        }

        return $return;
    }

    public function get_position_name()
    {
        return !empty($this->id_position) ? App__Ed__Model__Users__Positions::find($this->id_position)->name : '';
    }

    public function get_company_data()
    {
        $sql = "SELECT c.* 
                FROM users u 
                INNER JOIN company_users cu ON u.id = cu.id_user
                INNER JOIN company c ON c.id = cu.id_company
                WHERE u.id = " . $this->id;
        return App__Ed__Model__Company::find_by_sql($sql);
    }

    public function get_url()
    {
        return (is_ed() ? BASE_ED_URL : BASE_URL) . "user/profil/zobacz.html?id=" . $this->id;
    }

    public function get_connected_services()
    {
        $return_data = array();
        $find = App__Ed__Model__Users__Service::find(App__Ed__Model__Users__Service::where("id_user = " . $this->id));

        if(!empty($find))
        {
            foreach($find as $connected_service)
            {
                $return_data[] = App__Ed__Model__Service::find($connected_service->id_service);
            }
        }

        return $return_data;

    }

    public function has_first_change_password()
    {
        $ts_start = strtotime('2019-05-06');
        $ts_change_password = !empty($this->ts_change_password) ? $this->ts_change_password->getTimeStamp() : strtotime('0000-00-00');
        return ($ts_change_password >= $ts_start);
    }

}