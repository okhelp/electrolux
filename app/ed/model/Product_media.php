<?php

namespace App\ed\model;

use Lib__Base__Model;

class Product_media extends Lib__Base__Model
{
    const COLUMN_ID         = 'id';
    const COLUMN_ID_PRODUCT = 'id_product';
    const COLUMN_TYPE       = 'type';
    const COLUMN_LINK       = 'link';

    const TABLE_NAME = 'product_media';

    static $table_name = self::TABLE_NAME;
    static $primary_key = self::COLUMN_ID;
}