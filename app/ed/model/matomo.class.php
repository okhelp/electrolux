<?php

class App__Ed__Model__Matomo
{
    private $tokenAuth;
    private $matomoUrl;
    private $categories = [];

    public function __construct()
    {
        $this->tokenAuth = '7a6b7686fa8eeecf114c210bfd61ceea';
        $this->matomoUrl = 'https://electroluxodkuchni.pl/matomo/index.php';
    }

    public function getCategories(): array
    {
        if (!$this->categories) {
            $this->categories = array_map(function ($category) {
                return [
                    'id' => $category["idsubdatatable"],
                    'label' => htmlspecialchars($category["label"], ENT_QUOTES, 'UTF-8'),
                    'visits' => $category['nb_visits']
                ];
            }, $this->getData('Events.getCategory'));
        }
        return $this->categories;
    }

    public function getCategoryByID(int $categoryId): array
    {
        foreach ($this->getCategories() as $category) {
            if ($category['id'] == $categoryId) {
                return $category;
            }
        }
        return [
            'label' => '',
            'visits' => '0',
        ];
    }

    public function getValuesByCategoryID(int $categoryID, string $method = 'Events.getActionFromCategoryId')
    {
        return $this->getData($method, $categoryID, 'eventCategory');
    }

    public function getChart(string $apiModule, string $apiAction, string $graphType = 'horizontalBar', int $idSubtable = null, string $secondaryDimension = null): string
    {
        return $this->getUrl('ImageGraph.get', $idSubtable, $secondaryDimension, $apiModule, $apiAction, $graphType);
    }

    private function getUrl(string $method, int $idSubtable = null, string $secondaryDimension = null, string $apiModule = null, string $apiAction = null, string $graphType = null): string
    {
        $url = $this->matomoUrl;
        $url .= "?module=API&method=$method";
        $url .= "&idSite=1&period=range&date=2019-01-01," . date('Y-m-d');
        $url .= "&format=JSON&filter_limit=200";
        $url .= "&token_auth=$this->tokenAuth";
        if ($idSubtable) {
            $url .= "&idSubtable=$idSubtable";
        }
        if ($secondaryDimension) {
            $url .= "&secondaryDimension=$secondaryDimension&flat=1";
        }
        if ($apiModule) {
            $url .= "&apiModule=$apiModule";
        }
        if ($apiAction) {
            $url .= "&apiAction=$apiAction";
        }
        if ($graphType) {
            $url .= "&graphType=$graphType";
        }
        return $url;
    }

    private function getData(string $method, int $idSubtable = null, string $secondaryDimension = null)
    {
        $url = $this->getUrl($method, $idSubtable, $secondaryDimension);
        return json_decode(file_get_contents($url) ?: '{}',true);
    }
}