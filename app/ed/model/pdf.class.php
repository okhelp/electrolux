<?php

require_once(BASE_PATH . 'third_party/tcpdf/tcpdf.php');

class MY_TCPDF extends TCPDF
{
	//Header PDFa
    public function Header() {}

    // Stropka PDFa
    public function Footer() 
    {
        // stronnicowanie
        $this->SetY(-20);
        $this->SetFont('freesans', '', 6);
        $this->Cell(0, 10, 'Strona: '.$this->getAliasNumPage().' z '.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
        
        // informacja o programie
        $this->SetY(-15);
        $this->SetFont('freesans', '', 6);
        $this->Cell(0, 10, 'FISTO.PL należy do IMSET Sp. z o.o. ul. Wały Piastowskie 1 lok. 1310A, 80-855 Gdańsk, NIP: 583 332 32 50, Sąd Rejonowy Gdańsk-Północ VII Wydział Gosp. KRS 0000750306, Kapitał zakładowy 5000 zł.', 0, false, 'C', 0, '', 0, false, 'T', 'M');
    }
}

class PDF
{
	public static function generate($file_name, $pdf_data, $template)
	{
		$pdf = new MY_TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        // ustawienie informacje na temat wygenerownego dokumentu
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('IMSET sp. z o.o. (fisto.pl)');
        $pdf->SetTitle($file_name);
        $pdf->SetSubject('');
        $pdf->SetKeywords('fisto.pl');

        // wyłączenie nagówku
        $pdf->setPrintHeader(false);

        // ustawienie czcionek w nagłówku i stopce
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // ustawienie domyślnej czccionki w dokumencie
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // ustawienie marginesów
        $pdf->SetMargins(PDF_MARGIN_LEFT, 8, PDF_MARGIN_RIGHT);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // automatyczne tworzenie nowych stron
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // fix - automatyczne skalowanie załacznych obrazków
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // ---------------------------------------------------------

        // ustawienie czcionki (najlepsza: freesans)
        $pdf->SetFont('freesans', '', 10);

        // Tworzę nową stronę
		$pdf->AddPage();
		
		$smarty = new Smarty;
		$smarty->assign('data',$pdf_data);
		$pdf_content = $smarty->fetch($template);
        ob_end_clean();
        
        $pdf->writeHTML($pdf_content, true, false, true, false, '');
		
		

        //Zamykam tworzenie i wymuazam pobranie pliku
//        $pdf->Output($this->txt->bezp($dane['tytul_pliku']).'.pdf', 'I');
        $pdf->Output($file_name.'.pdf', 'D');
	}
}