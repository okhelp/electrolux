<?php

class App__Ed__Model__Points_log extends Lib__Base__Model
{
    static $table_name = "points_log";
    static $primary_key = 'id';

    public static function add(int $userId, int $points): bool
    {
        $pointsLog = new self;
        $pointsLog->user_id = $userId;
        $pointsLog->points = $points;
        return $pointsLog->save();
    }

    public static function getPoints(int $userId): int
    {
        $points = self::find_by_sql(<<<SQL
            SELECT SUM(points) as sum_points FROM points_log WHERE user_id = $userId
SQL
        );
        $points = reset($points);
        return $points->sum_points ?? 0;
    }

    public static function getSpentPoints(int $userId): int
    {
        $points = self::find_by_sql(<<<SQL
            SELECT SUM(-points) as sum_points FROM points_log WHERE user_id = $userId AND points < 0
SQL
        );
        $points = reset($points);
        return $points->sum_points ?? 0;
    }
}