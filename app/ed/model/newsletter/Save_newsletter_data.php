<?php
declare(strict_types=1);

namespace App\ed\model\newsletter;

class Save_newsletter_data
{
    /** @var int */
    private $id_newsletter;

    /** @var */
    private $post_data;

    /** @var int */
    private $id_user;

    /**
     * Save_newsletter_data constructor.
     * @param int $id_newsletter
     * @param array $post_data
     * @param int $id_user
     */
    public function __construct(int $id_newsletter, array $post_data, int $id_user)
    {
        $this->id_newsletter = $id_newsletter;
        $this->post_data = $post_data;
        $this->id_user = $id_user;
    }

    /**
     * @throws \ActiveRecord\RecordNotFound
     */
    public function save(): bool
    {
        $newsletter = empty($this->id_newsletter) ? new Newsletter_model() : Newsletter_model::find($this->id_newsletter);

        $newsletter->{Newsletter_model::COLUMN_ID_USER} = $this->id_user;
        $newsletter->{Newsletter_model::COLUMN_SEND_DATE} = $this->post_data[Newsletter_model::COLUMN_SEND_DATE];
        $newsletter->{Newsletter_model::COLUMN_SEND_TO_ADMIN} = empty($this->post_data[Newsletter_model::COLUMN_SEND_TO_ADMIN]) ? 0 :
            $this->post_data[Newsletter_model::COLUMN_SEND_TO_ADMIN];
        $newsletter->{Newsletter_model::COLUMN_SEND_TO_EMPLOYEE} = empty($this->post_data[Newsletter_model::COLUMN_SEND_TO_EMPLOYEE]) ? 0 :
            $this->post_data[Newsletter_model::COLUMN_SEND_TO_EMPLOYEE];
        $newsletter->{Newsletter_model::COLUMN_STATUS} = $this->post_data[Newsletter_model::COLUMN_STATUS];
        $newsletter->{Newsletter_model::COLUMN_NAME} = $this->post_data[Newsletter_model::COLUMN_NAME];
        $newsletter->{Newsletter_model::COLUMN_TEXT} = $this->post_data[Newsletter_model::COLUMN_TEXT];
        $newsletter->{Newsletter_model::COLUMN_MAIL_TITLE} = $this->post_data[Newsletter_model::COLUMN_MAIL_TITLE];

        return $newsletter->save();
    }
}