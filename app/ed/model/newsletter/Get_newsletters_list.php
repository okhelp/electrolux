<?php
declare(strict_types=1);

namespace App\ed\model\newsletter;

use App\ed\model\users\Get_by_ids;
use App\ed\model\users\Get_company_users_list_by_user_type;

class Get_newsletters_list
{
    public static function get()
    {
        $newsletters_to_return = [];

        $newsletters = Newsletter_model::find_by_sql(
            "
            SELECT *
            FROM newsletter
            JOIN newsletter_lang ON newsletter_lang.newsletter_id=newsletter.id
            WHERE newsletter_lang.id_service = {$_SESSION['id_service']}
        ");

        if (!empty($newsletters))
        {
            $users_ids = array_map(
                function($newsletter) {
                    return $newsletter->{Newsletter_model::COLUMN_ID_USER};
                }, $newsletters);

            $users = Get_by_ids::get($users_ids);

            $employes = Get_company_users_list_by_user_type::get((int)$_SESSION['id_service'], 'user');
            $admins = Get_company_users_list_by_user_type::get((int)$_SESSION['id_service'], 'admin');

            foreach ($newsletters as $newsletter)
            {
                $recipients_count = 0;
                $recipients_count += empty($newsletter->{Newsletter_model::COLUMN_SEND_TO_ADMIN}) ? 0 : count($admins);
                $recipients_count += empty($newsletter->{Newsletter_model::COLUMN_SEND_TO_EMPLOYEE}) ? 0 : count($employes);

                $newsletter->assign_attribute('user', $users[$newsletter->{Newsletter_model::COLUMN_ID_USER}]);
                $newsletter->assign_attribute('recipients_count', $recipients_count);
            }

            $newsletters_to_return = $newsletters;
        }

        return $newsletters_to_return;
    }
}