<?php
declare(strict_types=1);

namespace App\ed\model\newsletter;

use Lib__Base__Model;

class Newsletter_model extends Lib__Base__Model
{
    const COLUMN_ID               = 'id';
    const COLUMN_ID_USER          = 'id_user';
    const COLUMN_SEND_DATE        = 'send_date';
    const COLUMN_SEND_TO_ADMIN    = 'send_to_admin';
    const COLUMN_SEND_TO_EMPLOYEE = 'send_to_employee';
    const COLUMN_STATUS           = 'status';
    const COLUMN_CREATED_AT       = 'created_at';
    const COLUMN_UPDATED_AT       = 'updated_at';
    const COLUMN_DELETED_AT       = 'deleted_at';
    const COLUMN_TEXT             = 'text';
    const COLUMN_NAME             = 'name';
    const COLUMN_MAIL_TITLE       = 'mail_title';

    const STATUS_ACTIVE     = 1;
    const STATUS_NOT_ACTIVE = 0;

    const TABLE_NAME = 'newsletter';

    static $table_name = self::TABLE_NAME;
    static $primary_key = self::COLUMN_ID;
}