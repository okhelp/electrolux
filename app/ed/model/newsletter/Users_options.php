<?php
declare(strict_types=1);

namespace App\ed\model\newsletter;

use Lib__Base__Model;

class Users_options extends Lib__Base__Model
{
    const COLUMN_ID         = 'id';
    const COLUMN_ID_USER    = 'id_user';
    const COLUMN_ID_OPTION  = 'id_option';
    const COLUMN_STATUS     = 'status';
    const COLUMN_CREATED_AT = 'created_at';

    const TABLE_NAME = 'users_options';

    const OPTION_SEND_NEWSLETTER                          = 1;
    const OPTION_SEND_PASSWORD_CHANGE_NOTIFICATION        = 2;
    const OPTION_SEND_NEW_ASSOCIATE_NOTIFICATION          = 3;
    const OPTION_SEND_POINTS_TRANSFER_NOTIFICATION        = 4;
    const OPTION_SEND_NEW_SALE_NOTIFICATION               = 5;
    const OPTION_SEND_ORDER_STATUSES_SUMMARY_NOTIFICATION = 6;
    const OPTION_SHOW_POINTS                              = 7;

    const OPTIONS_ARRAY = [
        self::OPTION_SEND_NEWSLETTER,
        self::OPTION_SEND_PASSWORD_CHANGE_NOTIFICATION,
        self::OPTION_SEND_NEW_ASSOCIATE_NOTIFICATION,
        self::OPTION_SEND_POINTS_TRANSFER_NOTIFICATION,
        self::OPTION_SEND_NEW_SALE_NOTIFICATION,
        self::OPTION_SEND_ORDER_STATUSES_SUMMARY_NOTIFICATION,
        self::OPTION_SHOW_POINTS,
    ];

    const STATUS_ON  = 1;
    const STATUS_OFF = 0;

    static $table_name = self::TABLE_NAME;
    static $primary_key = self::COLUMN_ID;
}