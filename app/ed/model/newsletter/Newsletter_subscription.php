<?php
declare(strict_types=1);

namespace App\ed\model\newsletter;

use ActiveRecord\ConnectionManager;

class Newsletter_subscription
{
    public static function subscribe(int $id_user)
    {
        $connection = ConnectionManager::get_connection();

        $connection->query("
            DELETE FROM users_options WHERE id_user=$id_user AND id_option=" . Users_options::OPTION_SEND_NEWSLETTER . "
        ");
    }

    public static function unsubscribe(int $id_user): bool
    {
        $unsubscribe = new Users_options();
        $unsubscribe->{Users_options::COLUMN_ID_USER} = $id_user;
        $unsubscribe->{Users_options::COLUMN_ID_OPTION} = Users_options::OPTION_SEND_NEWSLETTER;
        $unsubscribe->{Users_options::COLUMN_STATUS} = Users_options::STATUS_ON;

        return $unsubscribe->save();
    }
}