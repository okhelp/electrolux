<?php


class App__Ed__Model__Id_cookie extends Lib__Base__Model
{
	static $table_name = 'cookie';
	static $primary_key = 'id';

    public static function get()
	{
		return Lib__cookie::get('id_cookie');
	}
	
	public static function init()
	{
		$id_cookie = self::get();
		
		//sprawdzam czy id_cookie jest poprawne
		if(!empty($id_cookie))
		{
			$check = App__Ed__Model__Id_cookie::find_by_id($id_cookie);
			if(!$check)
			{
				$id_cookie = 0;
			}
		}

		return !$id_cookie ? self::set() : $id_cookie;
	}
	
	private static function set()
	{
		
		$id_cookie = 0;
		$user_hash = self::generate_user_hash();
		
		//sprawdzam czy hash już ustnieje, jeżeli tak to pobieram z niego id_cookie
		$find_id_cookie = App__Ed__Model__Id_cookie::find_by_user_hash($user_hash);
		
		if($find_id_cookie)
		{
			$id_cookie = $find_id_cookie->id;
		}
		else 
		{
			$cookie = new App__Ed__Model__Id_cookie;
			$cookie->ip = $_SERVER['REMOTE_ADDR'];
			$cookie->user_hash = $user_hash;
			$cookie->save();
			$id_cookie = $cookie->id;
		}
		
		//jeżeli nie ma ciasteczka tworzę id_cookie
		if(!Lib__cookie::get('id_cookie'))
		{
			Lib__cookie::set('id_cookie',$id_cookie, 60*60*24*360);
		}
		
		return $id_cookie;
	}
	
	private static function generate_user_hash()
	{
		$id_user = (int)Lib__Session::get('id');
		
		$string[] = $id_user;
		$string[] = 'jest 22.04 Magda ma Klientkę a ja piszę furazagrosze.pl ;)';
		$string[] = $_SERVER['REMOTE_ADDR'];
		$string[] = $_SERVER['HTTP_USER_AGENT'];
		
		$hash = implode('|',$string); 
		$hash = substr(md5($hash), 10).$id_user;
		
		return $hash;
	}

}