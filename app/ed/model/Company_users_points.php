<?php
declare(strict_types=1);

namespace App\ed\model;

use Lib__Base__Model;

class Company_users_points extends Lib__Base__Model
{
    const COLUMN_ID              = 'id';
    const COLUMN_COMPANY_USER_ID = 'company_user_id';
    const COLUMN_POINTS          = 'points';
    const COLUMN_EXTRA_POINTS    = 'extra_points';
    const COLUMN_CREATED_AT      = 'created_at';
    const COLUMN_UPDATED_AT      = 'updated_at';

    const TABLE_NAME = 'company_users_points';

    static $table_name = self::TABLE_NAME;
    static $primary_key = self::COLUMN_ID;
}