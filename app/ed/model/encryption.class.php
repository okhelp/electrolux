<?php
class App__Ed__Model__Encryption
{   
    private static function encrypt_decrypt($action, $string) 
    {
        $output = false;
        $encrypt_method = "AES-256-CBC";
        $secret_key = '9C&#UU55rv%#a!AwAqaP';
        $secret_iv = 'Siedzę w pracy popijam wodę i słucham despacito..';
        
        // hash
        $key = hash('sha256', $secret_key);

        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        $iv = substr(hash('sha256', $secret_iv), 0, 16);
        if ($action == 'encrypt') 
        {
            $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
            $output = base64_encode($output);
        } 
        elseif($action == 'decrypt') 
        {
            $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
        }
        
        return $output;
    }
    
    public static function encode($string)
    {
        return self::encrypt_decrypt('encrypt', $string);
    }
    
    public static function decode($string)
    {
        return self::encrypt_decrypt('decrypt', $string);
    }
}