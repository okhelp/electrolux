<?php

class App__Ed__Model__Debug_Bar
{
    private function is_active()
    {
        //dla testów włączone cały czass (w domyśle widzi go tylko dział IT)
        return !PRODUCTION;
    }
    
    public static function init($debug_params=array())
    {
		$html = NULL;
		if(!is_ajax())
		{
			$smarty = new Smarty();

			//ładuje parametry jako zmienna
			if(!empty($debug_params))
			{
				foreach($debug_params as $param=>$param_value)
				{
					$smarty->assign($param,$param_value);
				}
			}

			if(!empty($debug_params['sql_queries']))
			{
				$smarty->assign('sql_queries_list',self::prepare_queries_list($debug_params['sql_queries']));
			}
			
			$html = $smarty->fetch(BASE_PATH . 'app/ed/templates/_partials/debug_bar/index.tpl');
		}
        
        return $html;
    }
    
    public static function prepare_queries_list($queries)
    {
        $list = array();
        
        foreach($queries as $query)
        {
            $list[$query] = array_key_exists($query, $list) ? $list[$query] + 1 : 1;
        }
        
        return $list;
    }
}