<?php

class App__Ed__Model__User_log extends Lib__Base__Model
{
    const COLUMN_ID         = 'id';
    const COLUMN_ID_USER    = 'id_user';
    const COLUMN_CREATED_AT = 'created_at';

    const TABLE_NAME = 'user_log';

    static $table_name = self::TABLE_NAME;
    static $primary_key = self::COLUMN_ID;
}
