<?php

class App__Ed__Model__Pages extends Lib__Base__Model
{
	static $table_name = 'pages';
	static $primary_key = 'id';
	
	public function get_category()
	{
		return !empty($this->id_category) ? App__Ed__Model__Pages__Category::find($this->id_category) : new App__Ed__Model__Pages__Category;
	}
	
	public function get_url()
	{
		$url = '';
		
		$category = $this->get_category();
		if(!empty($category))
		{
			$url .= $category->get_url() . url_slug($this->title) . '-p' . $this->id . '.html';
		}
		else
		{
			$url = BASE_URL . url_slug($this->title) . '-p' . $this->id . '.html';
		}
		
		return $url;
	}
}