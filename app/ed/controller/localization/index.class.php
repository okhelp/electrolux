<?php

class App__Ed__Controller__Localization__Index extends Lib__Base__Ed_Controller
{
	public function action_lista()
	{
		//ładuję zmienne do szablonu
		$this->view->assign('list',App__Ed__Model__Localization::find("all"));
		
		//ładuję szablon
		$this->template = 'localization/lista.tpl';
		$this->page_title = "Lokalizacje - lista";
        $this->breadcrumb = array(''=>'Lokalizacje');
	}
	
	public function action_ed()
	{
		$id_localization = !empty($_GET) && !empty($_GET['id']) ? (int)$_GET['id'] : 0;
		$is_ed = !empty($id_localization);
		
		//tworze obiekt lokalizacji
		$localization = !empty($id_localization) ? App__Ed__Model__Localization::find($id_localization) : new App__Ed__Model__Localization;
		
		//zapis do bazy
		if(!empty($_POST))
		{
			foreach($_POST as $k => $v)
			{
				if($k == 'description')
				{
					$v = preg_replace('~[\r\n]+~', '', $v);
				}
				$localization->{$k} = $v;
			}
			
			$localization->save();
			
			//określam komunikat powrotu
			if($is_ed)
			{
				$msg = "Lokalizacja została zmieniona.";
			}
			else
			{
				$msg = "Lokalizacja została dodana.";
			}
			
			go_back("g|$msg",BASE_ED_URL.'localization/lista.html');
		}
		
		//ładuję zmienne do szablonu
		$page_title = empty($id_localization) ? 
			'Dodaj nową lokalizację' : "Edycja lokaliacji: " . $localization->name;
		$this->view->assign('localization',$localization);
		$this->view->assign('markers_list',$this->get_markers());

		
		//ładuję szablon
		$this->template = 'localization/ed.tpl';
		$this->page_title = $page_title;
        $this->breadcrumb = array(
			'localization/lista.tpl'=>'Lokalizacja',
			'' => $page_title
		); 
	}

	public function action_usun()
	{
		$id_localization = !empty($_GET) && !empty($_GET['id']) ? (int)$_GET['id'] : 0;
		
		if(!empty($id_localization))
		{
			//pobieram obiekt lokalizacji
			$localization = App__Ed__Model__Localization::find($id_localization);
			if(empty($localization))
			{
				go_back("d|Błąd. Lokalizacja nie została odnaleziona.");
			}
			
			$localization->delete();
			
			go_back("g|Lokalizacja została usunięta.");
			
		}
		else
		{
			go_back("d|Proszę podać id lokalizacji.");
		}
		
	}
	
	private function get_markers()
	{
		$list = array();
		$dir_path = BASE_PATH . "data/js/open_maps/vendor/img/";
		$files_list = glob("$dir_path*.{jpg,gif,png}",GLOB_BRACE);
		
		if(!empty($files_list))
		{
			foreach($files_list as $file)
			{
				$list[] = str_replace($dir_path,'',$file);
			}
		}
		
		return $list;
	}
}