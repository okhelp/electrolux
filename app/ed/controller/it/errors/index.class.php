<?php

class App__Ed__Controller__It__Errors__Index extends Lib__Base__Controller
{
    public function action_index()
    {
        $this->view->assign('list', App__Ed__Model__Error__List::get_list());
        $this->template = "it/errors/index.tpl";
        $this->page_title = "Panel błędów";
        $this->breadcrumb = array('it'=>'IT','' => 'Panel Błędów');
    }
}