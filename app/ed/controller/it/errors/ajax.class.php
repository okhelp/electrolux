<?php

class App__Ed__Controller__It__Errors__Ajax extends Lib__Base__Controller
{
    public function action_resolve_error()
    {
        if(!empty($_POST) && !empty($_POST['id_error']))
        {
            $error = App__Ed__Model__Error::find((int)$_POST['id_error']);
            $error->status = 1;
            $error->save();
        }
    }
    
    public function action_add_user()
    {
        if(!empty($_POST) && !empty($_POST['id_error']))
        {
            $error = App__Ed__Model__Error::find((int)$_POST['id_error']);
            $error->id_user = (int)Lib__Session::get('id');
            $error->save();
        }
    }
}