<?php

use App\ed\model\messages\Get_new_messages_count;

class App__Ed__Controller__Wiadomosci__Index extends Lib__Base__Ed_Controller
{
    
    public function action_napisz()
    {
        $this->template = 'wiadomosci/napisz.tpl';
        $this->page_title = 'Napisz nową wiadomość';
        $this->breadcrumb = array(
            'wiadomosci/odebrane.html' => 'Wiadomości',
            '' => $this->page_title
        );
    }

    public function action_wyslij()
    {
        if(!empty($_POST))
        {

            if(!empty($_POST['hash']))
            {
                $hash_decoded = App__Ed__Model__Messages__Model::decode_hash($_POST['hash']);

                //pobieram dane wiadomości
                $message = App__Ed__Model__Messages::find($hash_decoded['id_message']);

                //folder wysłane
                if($hash_decoded['folder'] == 'inbox')
                {
                    $subject = "RE: " . $message->subject;
                    $id_to = $hash_decoded['id_from'];
                }
            }
            else
            {
                $subject = $_POST['subject'];
                $id_to = !empty($_POST['id_to']) ? (int)$_POST['id_to'] : 0;
            }

            $id_from = (int)Lib__Session::get('id');
            $description = $_POST['description'];
            $attachments = !empty($_FILES) ? $_FILES : array();

            $send_message = App__Ed__Model__Messages__Model::send($id_from, $id_to, $subject, $description, $attachments);

            if(!empty($send_message['error']))
            {
                go_back("d|" . $send_message['error']);
            }
            else
            {
                go_back("g|" . $send_message['success'], BASE_ED_URL . 'wiadomosci/odebrane.html');
            }

        }
    }

    public function action_odebrane()
    {

        $id_user = (int)Lib__Session::get('id');
        $page = !empty($_GET) && !empty($_GET['page']) ? (int)$_GET['page'] : 1;
        $per_page = 5;

        $list = new App__Ed__Model__Messages__List;
        $list->set_where("id_to = " . $id_user);
        $list->set_page($page);
        $list->set_per_page($per_page);
        $list->set_folder('inbox');
        $list = $list->execute();

        $this->view->assign('list', $list['list']);
        $this->view->assign('title', 'Skrzynka odbiorcza');
        $this->view->assign('display_actions', true);
        $this->view->assign('new_messages_count', Get_new_messages_count::get($id_user));
        $this->view->assign('received_active', true);
        $this->view->assign('page', $page);
        $this->view->assign('per_page', $per_page);
        $this->view->assign('all_items', $list['total']);
        $this->view->assign('folder', 'inbox');

        $this->template = 'wiadomosci/odebrane.tpl';
        $this->page_title = 'Skrzynka odbiorcza';
        $this->breadcrumb = array(
            'wiadomosci/odebrane.html' => 'Wiadomości',
            '' => $this->page_title
        );
    }

    public function action_usuniete()
    {
        $id_user = (int)Lib__Session::get('id');
        $page = !empty($_GET) && !empty($_GET['page']) ? (int)$_GET['page'] : 1;
        $per_page = 5;

        $list = new App__Ed__Model__Messages__List;
        $list->set_where("(id_to = $id_user AND trash_to=1) OR (id_from = $id_user AND trash_from=1)");
        $list->set_page($page);
        $list->set_per_page($per_page);
        $list->set_folder('inbox');
        $list = $list->execute();

        $this->view->assign('title', 'Wiadomości usunięte');
        $this->view->assign('display_actions', false);
        $this->view->assign('list', $list['list']);
        $this->view->assign('new_messages_count', Get_new_messages_count::get($id_user));
        $this->view->assign('deleted_active', true);
        $this->view->assign('page', $page);
        $this->view->assign('per_page', $per_page);
        $this->view->assign('all_items', $list['total']);
        $this->view->assign('folder', 'inbox');

        $this->template = 'wiadomosci/odebrane.tpl';
        $this->page_title = 'Usunięte wiadomości';
        $this->breadcrumb = array(
            'wiadomosci/odebrane.html' => 'Wiadomości',
            '' => $this->page_title
        );
    }

    public function action_wyslane()
    {
        $id_user = (int)Lib__Session::get('id');
        $page = !empty($_GET) && !empty($_GET['page']) ? (int)$_GET['page'] : 1;
        $per_page = 5;

        $list = new App__Ed__Model__Messages__List;
        $list->set_where("id_from = $id_user");
        $list->set_page($page);
        $list->set_per_page($per_page);
        $list->set_folder('inbox');
        $list = $list->execute();

        $this->view->assign('title', 'Wiadomości wysłane');
        $this->view->assign('display_actions', true);
        $this->view->assign('list', $list['list']);
        $this->view->assign('new_messages_count', Get_new_messages_count::get($id_user));
        $this->view->assign('sent_active', true);
        $this->view->assign('page', $page);
        $this->view->assign('per_page', $per_page);
        $this->view->assign('all_items', $list['total']);
        $this->view->assign('folder', 'inbox');

        $this->template = 'wiadomosci/odebrane.tpl';
        $this->page_title = 'Usunięte wiadomości';
        $this->breadcrumb = array(
            'wiadomosci/odebrane.html' => 'Wiadomości',
            '' => $this->page_title
        );
    }

    public function action_wazne_wiadomosci()
    {
        $id_user = (int)Lib__Session::get('id');
        $page = !empty($_GET) && !empty($_GET['page']) ? (int)$_GET['page'] : 1;
        $per_page = 5;

        $list = new App__Ed__Model__Messages__List;
        $list->set_where("(id_to = $id_user AND important_to=1) OR (id_from = $id_user AND important_from=1)");
        $list->set_page($page);
        $list->set_per_page($per_page);
        $list->set_folder('inbox');
        $list = $list->execute();

        $this->view->assign('title', 'Wiadomości ważne');
        $this->view->assign('display_actions', true);
        $this->view->assign('list', $list['list']);
        $this->view->assign('new_messages_count', Get_new_messages_count::get($id_user));
        $this->view->assign('important_active', true);
        $this->view->assign('page', $page);
        $this->view->assign('per_page', $per_page);
        $this->view->assign('all_items', $list['total']);
        $this->view->assign('folder', 'inbox');

        $this->template = 'wiadomosci/odebrane.tpl';
        $this->page_title = 'Usunięte wiadomości';
        $this->breadcrumb = array(
            'wiadomosci/odebrane.html' => 'Wiadomości',
            '' => $this->page_title
        );
    }

    public function action_pokaz()
    {
        $hash = !empty($_GET) && !empty($_GET['m']) ? $_GET['m'] : '';

        //jeżeli nie ma hash'a pokazuję 404
        if(!$hash)
        {
            show_404();
        }

        //odkodowuję hash
        $hash_org = $hash;
        $hash_decoded = App__Ed__Model__Messages__Model::decode_hash($hash);

        //pokazuję tylko wiadomość, która była wysłana, bądź adresowana do użytkownika
        $id_user = (int)Lib__Session::get('id');
        if($id_user == $hash_decoded['id_from'] || $id_user == $hash_decoded['id_to'])
        {
            //pokazuję wiadomość
            $message = App__Ed__Model__Messages::find(
                App__Ed__Model__Messages::where("id = " . $hash_decoded['id_message'])
                    ->add("id_from = $id_user OR id_to = $id_user")
            );

            if(empty($message))
            {
                show_404();
            }

            $message = reset($message);

            //określam użytkownika jakiego pokazujemy
            if($hash_decoded['folder'] == 'inbox')
            {
                $id_user_show = $message->id_from;
            }
            elseif($hash_decoded['folder'] == 'sent')
            {
                $id_user_show = $message->id_to;
            }

            //jeżeli jest to folder odebrane, a wiadomosc jest nieodczytana, ustawiam jej flagę odczytanej
            if($hash_decoded['folder']  == 'inbox' && $message->view_to == 0)
            {
                $message->set_as_read();
            }

            $this->view->assign('id_message', $hash_decoded['id_message']);
            $this->view->assign('hash', $hash_org);
            $this->view->assign('new_messages_count', Get_new_messages_count::get($id_user));
            $this->view->assign('id_user', $id_user_show);
            $this->view->assign('folder', $hash_decoded['folder']);
            $this->view->assign('message', $message);

            //breadcrumb folderu
            if($hash_decoded['folder'] == 'inbox')
            {
                $breadcrumb_folder = [
                  'url' => 'wiadomosci/odebrane.html',
                    'title' => "Wiadomości odebrane"
                ];
            }
            elseif($hash_decoded['folder'] == 'sent')
            {
                $breadcrumb_folder = [
                  'url' => 'wiadomosci/wyslane.html',
                  'title' => "Wiadomości wysłane"
                ];
            }

            $this->template = 'wiadomosci/pokaz.tpl';
            $this->page_title = $message->subject;
            $this->breadcrumb = array(
                $breadcrumb_folder['url'] => $breadcrumb_folder['title'],
                '' => $this->page_title
            );

        }
        else
        {
            show_403();
        }



    }

    public function action_pobierz_zalacznik()
    {
        if(!empty($_GET) && !empty($_GET['file']))
        {
            $file = App__Ed__Model__Encryption::decode(urldecode($_GET['file']));
            $file = BASE_PATH . str_replace("./", "", $file);
            force_download($file);
        }
        else
        {
            show_404();
        }
    }

    public function action_wazne()
    {
        $return = [
          'status' => 'danger',
          'description' => 'Proszę uzupełnić hash'
        ];

        if(!empty($_POST) && !empty($_POST['hash']))
        {
            $hash = App__Ed__Model__Encryption::decode($_POST['hash']);
            $hash_arr = explode('|', $hash);

            $id_user = (int)Lib__Session::get('id');
            $id_message = $hash_arr[0];
            $folder_name = $hash_arr[1];

            $important_field = ($folder_name == 'inbox') ? "important_to" : "important_from";

            $message = App__Ed__Model__Messages::find($id_message);

            if(!empty($message))
            {
                $now_status = $message->{$important_field};
                
                $message->{$important_field} = ($now_status == 1) ? 0 : 1;

                if($message->save())
                {
                    $msg = ($now_status == 1) ? "Wiadomość została odznaczona jako ważna." : "Wiadomość została oznaczona jako ważna";
                    $return = [
                        'status' => 'success',
                        'description' => $msg
                    ];
                }
            }
            else
            {
                $return = [
                    'status' => 'danger',
                    'description' => 'Wiadomość nie została odnaleziona'
                ];
            }

        }

        echo json_encode($return);

    }

}