<?php

class App__Ed__Controller__Theme_Editor__Index extends Lib__Base__Ed_Controller
{

	public function action_save()
	{
		if(!empty($_POST))
		{
			$file_data = $_POST['file_data'];
			$file_path = $this->decode_file_path_url($_POST['file_path']);
			
			if(file_put_contents($file_path,$file_data))
			{
				go_back("g|Plik został zmieniony.");
			}
			else
			{
				go_back("d|Wystąpił błąd podczas zapisu pliku. Proszę sprawdzić uprawnienia zapisu");
			}
		}
	}
	
	public function action_ed()
	{

		$theme = !empty($_GET) && !empty($_GET['theme']) ? $_GET['theme'] : NULL;
		$mode = !empty($_GET) && !empty($_GET['mode']) ? $_GET['mode'] : NULL;
		$file = !empty($_GET) && !empty($_GET['file']) ? $_GET['file'] : NULL;
		
		//pobieram listę plikow
		if (!empty($mode))
		{
			if ($mode == 'javascript')
			{
				$extension = "js";
			}
			elseif($mode == 'smarty')
			{
				$extension = "tpl";
			}
			else
			{
				$extension = $mode;
			}

			$file_list = $this->get_file_list($extension);
			
			$this->view->assign('file_list', $file_list);
			
			if(!empty($file))
			{
				$file_path = $this->decode_file_path_url($file);
				$file_data = file_get_contents($file_path);
				$file_data = htmlentities($file_data);
				
				$this->view->assign('file_data',trim($file_data));
				$this->view->assign('file_name',$this->get_file_name($file_path,$extension));
				
			}
			
		}

		//przekazuję zmienne do szablonu
		$this->view->assign('theme', $theme);
		$this->view->assign('file', $file);
		$this->view->assign('mode', $mode);

		//ustawiam szablon
		$this->page_title = "Edytor plików";
		$this->template = 'theme_editor/ed.tpl';
		$this->breadcrumb = array(
			'' => 'Ustawienia',
			'ustawienia/wyglad.html' => 'Ustawienia wyglądu',
			'theme_editor/ed.html?theme=' . $theme => 'Edytor plików'
		);
	}

	private function get_file_list($extension)
	{
		$file_list = array();
		
		$dir_path = $this->get_dir_path($extension);
			
		
		$it = new RecursiveDirectoryIterator($dir_path);
		$allowed = array("$extension");
		foreach (new RecursiveIteratorIterator($it) as $file)
		{
			if (in_array(substr($file, strrpos($file, '.') + 1), $allowed))
			{
				$file_name = $this->get_file_name($file,$extension);
				$file_list[$this->encode_file_path_url($file)] = $file_name;
			}
		}
		
		ksort($file_list);
		
		return $file_list;
	}

	private function get_dir_path($extension)
	{
		if($extension != 'tpl')
		{
			$dir_path = BASE_PATH . "templates/{$_GET['theme']}/_assets/$extension/";
		}
		else
		{
			$dir_path = BASE_PATH . "templates/{$_GET['theme']}/";
		}
		
		return $dir_path;
	}
	
	private function get_file_name($file,$extension)
	{
		return str_replace($this->get_dir_path($extension), "", $file);
	}

	private function encode_file_path_url($file_path)
	{
		$file_path = base64_encode($file_path);
		$file_path = urlencode($file_path);
		return $file_path;
	}

	private function decode_file_path_url($file_path)
	{
		$file_path = urldecode($file_path);
		$file_path = base64_decode($file_path);
		return $file_path;
	}

}
