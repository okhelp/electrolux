<?php

class App__Ed__Controller__Upload__Index extends Lib__Base__Ed_Controller
{	
	public $common_file = '';
	
	public function action_ajax_upload_file() 
	{
		$upload = new App__Ed__Model__Upload($_POST['type'], $_POST['max_file_size']);
		$upload->send_file($_FILES['upload_file']);
	}
	
	public function action_edit_photo()
	{
		//zapis danych
		if(!empty($_POST))
		{
			$id_cimg = (int)$_POST['id_cimg'];
			
			$cimg = App__Ed__Model__Img::find($id_cimg);
			unset($_POST['id_cimg']);
			
			foreach($_POST as $k => $v)
			{
				$cimg->{$k} = $v;
			}
			
			$cimg->save();
			exit;
		}
		
		
		//pobieram id_cimg
		$id_cimg = !empty($_GET) && !empty($_GET['id_cimg']) ? (int)$_GET['id_cimg'] : 0;
		
		if(!$id_cimg) 
		{
			throw new Exception('Proszę podać id_cimg obrazka.');
		}
		
		//pobieram obiekt obrazka
		$cimg = App__Ed__Model__Img::find($id_cimg);
		
		if(empty($cimg))
		{
			throw new Exception('Wprowadzono nieprawidłowy id_cimg.');
		}
		
		//ładuję instancję smarty
		$smarty = new Smarty;
		
		//przekazuje zmienne do szablonu
		$smarty->assign('id_cimg',$id_cimg);
		$smarty->assign('img',$cimg);
		
		//pokazuję templatkę
		echo $smarty->fetch('_partials/upload/edit_photo.tpl');
	}
	
	public function action_ajax_remove_file()
	{
		if(!empty($_POST))
		{
			$is_img = (int)$_POST['i'];
			$file_name = $_POST['file_name'];
			
			//wersja dla obrazków, usuwam id_cimg i zarazem wszystkie pozostałe elementy
			if($is_img) 
			{
				App__Ed__Model__Img::remove_cimg($file_name);
			}
			else //wersja dla plików - usuwam plik z dysku
			{
				unlink(BASE_PATH.App__Ed__Model__Encryption::decode($file_name));
			}
			
		}
	}
}