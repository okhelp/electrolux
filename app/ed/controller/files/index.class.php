<?php

class App__Ed__Controller__Files__Index extends Lib__Base__Ed_Controller
{
	public function action_lista()
	{
		//pobieram listę plików
		$file_list = App__Ed__Model__Files::find(App__Ed__Model__Files::order('name ASC'));
		
		//przekazuję zmienne do szablonu
		$this->view->assign('list',$file_list);
		
		//ładuję szablon
		$this->template = 'files/lista.tpl';
        $this->page_title = 'lista plików';
		$this->breadcrumb = array(
			'files/lista.html' => 'Menadżer plików',
		);
	}
	
	public function action_dodaj()
	{
		//zapis
		if(!empty($_POST))
		{
			if (!empty($_FILES['file']['name']))
			{
				$upload = new App__Ed__Model__Upload('files');
				$file_path = $upload->send_file($_FILES['file'], 1);
				$file_name = $_FILES['file']['name'];
				
				//jeżeli wystąpił błąd to robię powrót
				if(strpos($file_path,'Plik:') !== FALSE)
				{
					go_back("d|" . $file_path);
					exit;
				}
				//zapis informacji do bazy
				$files = new App__Ed__Model__Files;
				$files->path = $file_path;
				$files->name = $file_name;
				$files->id_user = Lib__Session::get('id');
				
				if($files->save())
				{
					go_back("g|Plik został poprawnie zapisany.",BASE_ED_URL . 'files/lista.html');
				}
				else
				{
					go_back("d|Wystąpił błąd podczas zapisu do bazy danych.",
						BASE_ED_URL . 'files/lista.html');
				}
				
			}
		}
		
		//ładuję szablon
		$this->template = 'files/dodaj.tpl';
        $this->page_title = 'Dodaj nowy plik';
		$this->breadcrumb = array(
			'files/lista.html' => 'Menadżer plików',
			'' => 'Dodaj plik'
		);
	}
	
	public function action_usun()
	{
		$id_file = !empty($_GET) && !empty($_GET['id']) ? (int)$_GET['id'] : 0;
		
		if(empty($id_file))
		{
			go_back("d|Proszę podać id pliku do usunięcia");
		}
		
		//pobieram instancję obiektu pliku
		$file = App__Ed__Model__Files::find($id_file);
		
		if(empty($file))
		{
			go_back("d|Podany plik nie został odnaleziony w bazie danych");
		}
		
		//pobieram jego ścieżkę na serwerze
		$file_path = App__Ed__Model__Encryption::decode($file->path);
		$file_path = str_replace('./','',$file_path);
		$file_path = BASE_PATH . $file_path;
		
		if(!is_file($file_path))
		{
			go_back("d|Podany plik nie został odnaleziony na serwerze.");
		}
		
		//usuwam plik
		if(unlink($file_path))
		{
			if($file->delete())
			{
				go_back("g|Plik został poprawnie usunięty.");
			}
			else
			{
				go_back("d|Wystąpił błąd podczas usuwania pliku z bazy danych.");
			}
		}
		else
		{
			go_back("d|Wystąpił błąd podczas usuwania pliku z serwera.");
		}
		
	}
}