<?php

/**
 * Klasa routingu dla kontrolera welcome
 */
class Controller__Welcome__Router
{
	public function set()
	{
		$router = [];
		
		$router[] = array(
			'uri' => 'login.html',
			'controller' => 'Controller__Welcome__Index',
			'method' => 'action_login'
		);
		
		return $router;
	}
}