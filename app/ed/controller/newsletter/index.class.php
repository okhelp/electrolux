<?php

use App\ed\model\newsletter\Get_newsletters_list;
use App\ed\model\newsletter\Newsletter_model;
use App\ed\model\newsletter\Save_newsletter_data;

class App__Ed__Controller__Newsletter__Index extends Lib__Base__Ed_Controller
{
    public function action_lista()
    {
        $list = Get_newsletters_list::get();

        $this->view->assign('list', $list);

        $this->template = 'newsletter/list.tpl';
        $this->page_title = 'Newsletter';
        $this->breadcrumb = [
            'newsletter/lista.html' => 'Lista newsletterów',
        ];
    }

    public function action_ed()
    {
        if (isset($_GET['id']))
        {
            if (!empty($_POST))
            {
                $newsletter_data = new Save_newsletter_data((int)$_GET['id'], $_POST, (int)$this->_user->id);

                go_back($newsletter_data->save() ? 'g|Zapisano dane.' : 'w|Nie udało się zapisać danych.', BASE_ED_URL . 'newsletter/lista.html');
            }

            if (!empty($_GET['id']))
            {
                $this->view->assign('newsletter', Newsletter_model::find($_GET['id']));
            }

            $this->template = 'newsletter/ed.tpl';
            $this->page_title = 'Edycja';
            $this->breadcrumb = [
                'newsletter/lista.html'                => 'Lista newsletterów',
                'newsletter/ed.html?id=' . $_GET['id'] => 'Edycja',
            ];
        }
        else
        {
            go_back('w|Brak dostępu.');
        }
    }
}