<?php

class App__Ed__Controller__Block_menu__Index extends Lib__Base__Ed_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		
		if(!empty($_POST) && !isset($_POST['ids']))
		{
			//tworzę instację obiektu
			$block_menu = !empty($_POST['id']) ? App__Ed__Model__Block_menu::find($_POST['id']) : new Block_menu;
			
			//pobieram kolejność
			if(!isset($_POST['id']))
			{
				$block_menu->order = App__Ed__Model__Block_menu__Model::get_new_order($_POST['type']);
			}
			$block_menu->type = $_POST['type'];
			$block_menu->id_parent = (int)$_POST['id_parent'];
			$block_menu->name = $_POST['name'];
			$block_menu->url = $_POST['url'];
			
			if($block_menu->save())
			{
				go_back('g|'.lang('Pozycja została dodana.'));
			}
			else 
			{
				go_back('d|'.lang('Wystąpił błąd podczas dodawania pozycji.'));
			}
		}
		
	}
	
	public function action_gora()
	{	
		
		//ładuję zmienne do szablonu
		$this->view->assign('list',App__Ed__Model__Block_menu__Model::get_tree('up'));
		$this->view->assign('type','up');
		
		//ładuję szablon
		$this->template = 'block_menu/lista.tpl';
        $this->page_title = 'Menu górne - edycja';
		$this->breadcrumb = array(
			'' => 'Bloki menu',
			'block_menu/gora.html' => 'Górne menu'
		);
	}
	
	public function action_stopka()
	{	
		
		//ładuję zmienne do szablonu
		$this->view->assign('list',App__Ed__Model__Block_menu__Model::get_tree('down'));
		$this->view->assign('type','down');
		
		//ładuję szablon
		$this->template = 'block_menu/lista.tpl';
        $this->page_title = 'Menu dolne - edycja';
		$this->breadcrumb = array(
			'' => 'Bloki menu',
			'block_menu/stopka.html' => 'Dolne menu'
		);
	}
	
	public function action_ed()
	{
		//ustawiam id_parent - domyslne 0
		$id = !empty($_GET) && !empty($_GET['id']) ? (int)$_GET['id'] : 0;
		$id_parent = !empty($_GET) && !empty($_GET['id_parent']) ? (int)$_GET['id_parent'] : 0;
		$type = $_GET['type'];
		
		//jeżeli jest edycja (mamy podany id_parent, pobieram dane menu
		if(!empty($id))
		{
			$data = App__Ed__Model__Block_menu::find($id);
			$id_parent = $data->id_parent;
		}
		else
		{
			$data = array();
		}
		
		//inicjalizuję widok
		$smarty = new Smarty;
		$smarty->assign('id',$id);
		$smarty->assign('id_parent',$id_parent);
		$smarty->assign('type',$type);
		$smarty->assign('data',$data);
		echo $smarty->fetch('block_menu/ed.tpl');
	}
	
	public function action_usun()
	{
		$id = !empty($_GET) && !empty($_GET['id']) ? (int)$_GET['id'] : 0;
		
		if(!empty($id))
		{
			$block_menu = App__Ed__Model__Block_menu::find($id);
			if(empty($block_menu))
			{
				go_back('d|'.lang('Nie znaleziono pozycji.'));
			}
			
			if($block_menu->delete())
			{
				go_back('g|'.lang('Pozycja została usunięta.'));
			}
			else
			{
				go_back('d|'.lang('Wystąpił błąd podczas usuwania pozycji.'));
			}
		}
		else
		{
			go_back('d|'.lang('Proszę podać id pozycji do usunięcia.'));
		}
	}
	
	public function action_ajax_zmien_kolejnosc()
	{
		if(!empty($_POST) && !empty($_POST['ids']))
		{
			$ids = explode(',',$_POST['ids']);
			
			$i = 0;
			
			foreach($ids as $id_block)
			{
				$block = App__Ed__Model__Block_menu::find($id_block);
				if(!empty($block))
				{
					$i++;
					$block->order = $i;
					$block->save();
				}
			}
		}
	}
}