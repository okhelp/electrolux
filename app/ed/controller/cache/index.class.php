<?php

class App__Ed__Controller__Cache__Index extends Lib__Base__Ed_Controller
{
	public function action_index()
	{
        $param = !empty($_GET) && !empty($_GET['param']) ? $_GET['param'] : 'all';

        if($param == 'all')
        {
            //usuwam cache activrecords
            Lib__Memcache::delete('rp*');

            //pobieram wszystkie klucze
            $memcache_log = App__Ed__Model__Memcache_log::find('all');
            if(!empty($memcache_log))
            {
                foreach($memcache_log as $item)
                {
                    Lib__Memcache::delete($item->key_name);
                }
            }
        }

        go_back("g|Cache został wyczyszczony");
	}
}