<?php 

class App__Ed__Controller__Modules__Index extends Lib__Base__Ed_Controller
{
	public function action_lista()
	{
		$show_mode = !empty($_GET) && isset($_GET['show_mode']) ? (int)$_GET['show_mode'] : -1;
		
		//pobieram listę modułów
		$modules_where = App__Ed__Model__Modules::where();
		
		if($show_mode != -1)
		{
			$modules_where->add("status = $show_mode");
		}
		
		$module_order = App__Ed__Model__Modules::order("name ASC");
		$modules = App__Ed__Model__Modules::find($modules_where,$module_order);
		
		//ładuje zmienne do szablonu
		$this->view->assign('modules',$modules);
		
		//ładuję szablon
		$this->template = 'modules/lista.tpl';
		$this->page_title = 'Lista modułów';
		$this->breadcrumb = array(
			'' => 'Moduły',
			'modules/lista.html' => 'Lista modułów'
		);
	}
	
	public function action_instaluj()
	{
		if(!empty($_POST))
		{
			if(!empty($_FILES) && !empty($_FILES['module_file']['name']))
			{
				//sprawdzam czy plik posiada rozszerzenie zip
				$extension = pathinfo($_FILES['module_file']['name'])['extension'];
				if($extension != 'zip')
				{
					go_back("d|" . lang("Wgywany plik posiada nieprawidłowe rozszerzenie."));
				}
				
				//wgrywam plik na serwer
				$upload = new App__Ed__Model__Upload('file');
				$file = $upload->send_file($_FILES['module_file'], 1);
				$file = App__Ed__Model__Encryption::decode($file);
				$file = str_replace('./data/',BASE_PATH . 'data/',$file);
				
				//tworzę instancje obiektu odpowiedzialnego za instalację modułu
				$install = new App__Ed__Model__Modules__Install;
				
				//rozpakowuję plik zip
				$install->extract_zip($file);
				
				//uruchamiam instalator
				if($install->execute())
				{
					
					$msg = "g|" . lang("Moduł został pomyślnie zainstalowany");
				}
				else
				{
					$msg = "d|" . lang("Wystąpił problem podczas instalacji modułu.");
				}
				
				go_back($msg,BASE_ED_URL . 'modules/lista.html');
			}
			else
			{
				go_back("d|" . lang("Proszę wybrać plik do wgrania."));
			}
		}
		
		//ładuję szablon
		$this->template = 'modules/instaluj.tpl';
		$this->page_title = 'Instalacja modułu';
		$this->breadcrumb = array(
			'modules/lista.html' => 'Lista modułów',
			'' => 'Instalacja'
		);
	}
	
	public function action_usun()
	{
		$id_module = !empty($_GET) && !empty($_GET['id']) ? (int)$_GET['id'] : 0;
		
		if(!empty($id_module))
		{
			App__Ed__Model__Modules__Model::remove_module($id_module);
		}
		else
		{
			$msg = "d|" . lang("Proszę podać id modułu.");
			go_back($msg);
		}
	}
	
	public function action_aktywuj()
	{
		$id_module = !empty($_GET) && !empty($_GET['id_module']) ? (int)$_GET['id_module'] : 0;
		
		if(!empty($id_module))
		{
			$module = App__Ed__Model__Modules::find($id_module);
			if(!$module)
			{
				go_back("d|".lang("Moduł nie został znaleziony"));
			}
			else 
			{
				$module->status = 1;
				if($module->save())
				{
					go_back("g|".lang("Moduł został włączony."));
				}
				else
				{
					go_back("g|".lang(
						"Wystąpił błąd podczas włączania modułu. Moduł nie został wyłączony"));
				}
			}
		}
		else
		{
			go_back("d|" . lang("Proszę podać id modułu."));
		}
	}
	
	public function action_dezaktywuj()
	{
		$id_module = !empty($_GET) && !empty($_GET['id_module']) ? (int)$_GET['id_module'] : 0;
		
		if(!empty($id_module))
		{
			$module = App__Ed__Model__Modules::find($id_module);
			if(!$module)
			{
				go_back("d|".lang("Moduł nie został znaleziony"));
			}
			else 
			{
				$module->status = 0;
				if($module->save())
				{
					go_back("g|".lang("Moduł został wyłączony."));
				}
				else
				{
					go_back("g|".lang(
						"Wystąpił błąd podczas wyłączania modułu. Moduł nie został wyłączony"));
				}
			}
		}
		else
		{
			go_back("d|" . lang("Proszę podać id modułu."));
		}
	}
}