<?php

class App__Ed__Controller__Pages__Categories extends Lib__Base__Ed_Controller
{
    protected $_id_category;

    public function action_index()
    {
        redirect(BASE_URL.'ed/pages/categories/lista.html');
    }

    public function action_lista()
    {
        $this->_id_category = isset($_GET) && !empty($_GET) && !empty($_GET['id_parent']) ? (int)$_GET['id_parent'] : 0;

        $tree = App__Ed__Model__Pages__Category__Tree::get_categories($this->_id_category, 1);

        $this->view->assign('tree', json_encode($tree, JSON_UNESCAPED_UNICODE));

        //ładuję szablon
        $this->template = 'pages/categories/lista.tpl';
        $this->page_title = 'Lista kategorii';
        $this->breadcrumb = $this->generate_breadcrumb();
    }

    public function action_ed()
    {
        $id = !empty($_GET) && !empty($_GET['id']) ? (int)$_GET['id'] : 0;

        //zapis zmian
        if (!empty($_POST['save']) && $_POST['save'] == 1)
        {
            $this->save($id, $_POST);
        }

        //jeżeli jest $id to wiemy, że jest to edycja, a nie dodawanie
        if (!empty($id))
        {
            //pobieram dane kategorii
            $item = App__Ed__Model__Pages__Category::find(
                App__Ed__Model__Pages__Category::where("id=$id")
            );

            //robię wypad jeżeli nie ma takiej kategorii
            if (!$item)
            {
                go_back("d|Proszę wprowadzić poprawny numer id");
            }
            else
            {
                $item = reset($item);
            }
        }


        //ładuję zmienne do widoku
        $page_title = !empty($id) ? 'Edytuj kategorię: ' . $item->name : 'Dodaj kategorię';
        $this->page_title = $page_title;
        $this->breadcrumb = [
            'pages/categories/lista.html' => 'Lista Kategorii',
            ''                            => $page_title,
        ];
        $this->view->assign('id', $id);
        $this->view->assign('item', isset($item) ? $item : null);
        $this->view->assign(
            'category_tree',
            App__Ed__Model__Pages__Category__Tree::get_ed_tree(0, -1, 'id_parent', !empty($item->id_parent) ? $item->id_parent : ''));
        $this->template = "pages/categories/ed.tpl";
    }

    private function save($id_cat=0, array $data)
    {
        if(!empty($data))
        {
            //ustawiam instacje klasy
            $item = !empty($id_cat) ? App__Ed__Model__Pages__Category::find($id_cat) : new App__Ed__Model__Pages__Category;
            $is_ed = !empty($id_cat) ? TRUE : FALSE;

            //zapisuję podstaowe informacje
            $item->id_parent = isset($data['id_parent']) ? (int)$data['id_parent'] : 0;
            $item->name = $data['name'];
            $item->name_seo = url_slug($data['name']);
            $item->status = (int)$data['status'];
            $item->id_user = (int)Lib__Session::get('id');

            if($is_ed)
            {
                $item->ts_ed = date('Y-m-d H:i:s');
            }

            $item->save();

            //pobieram id (przydatne w przypadku pobierania id z nowej kategorii)
            $id_cat = $item->id;

            //czyszczę cache
            Lib__Memcache::delete('Pages__Category::*');
            Lib__Memcache::delete('Pages__Category__Tree::*');
            Lib__Memcache::delete('Pages__Category__Routing::*');
            Lib__Memcache::delete("Pages__Category__Links::*");

            //określam treść komunikatu i robię powrót na strone listy kategorii
            if($is_ed)
            {
                $msg = "g|Dane zostały poprawnie zmienione.";
            }
            else
            {
                $msg = "g|Kategoria została poprawnie dodana.";
            }
            go_back($msg,"ed/pages/categories/lista.html?id_parent=".$item->id_parent);
        }
    }

    public function action_usun()
    {
        $id_category = !empty($_GET) && !empty($_GET['id']) ? (int)$_GET['id'] : 0;

        if($id_category)
        {
            //pobieram pełne menu od tej kategorii
            $category = App__Ed__Model__Pages__Category::find($id_category);

            //sprawdzam czy istnieją przypisane kategrie
            $categories_connected = App__Ed__Model__Pages__Category::find(
                App__Ed__Model__Pages__Category::where("id_parent=$id_category"));
            if(!empty($categories_connected))
            {
                go_back("d|Nie można usunąć kategorii, ponieważ przypisane są do niej inne kategorie.");
            }

            //sprawdzam czy do tej kategorii nie ma podstron
            $categories_pages = App__Ed__Model__Pages::find(App__Ed__Model__Pages::where("id_category = $id_category"));
            if(!empty($categories_pages))
            {
                go_back("d|Nie można usunąć kategorii, poniważ przypisane są do niej strony.");
            }

            //usuwam podstronę
            $category->delete();
            go_back("g|Kategoria została usunięta.");
        }

    }

    private function generate_breadcrumb()
    {
        $bradcrumbs = array();

        //tworzę linki ogólne do podstron kategorii
        $bradcrumbs[] = "Podstrony";
        $bradcrumbs['pages/categories/lista.html'] = "Kategorie";

        if(!empty($this->_id_category))
        {
            $categories_list = App__Ed__Model__Pages__Category__Tree::get_categories();

            $category_id = $this->_id_category;

            $path = [];

            $it = new RecursiveIteratorIterator(
                new RecursiveArrayIterator($categories_list), RecursiveIteratorIterator::SELF_FIRST
            );
            foreach ($it as $value) {
                if (!isset($value['id']) || isset($value['id']) && $value['id']!== $category_id) {
                    continue;
                }

                $path[] = $value;
                for ($count = $it->getDepth(); $count && $count--;) {
                    $parent_data = $it->getSubIterator($count);
                    foreach($parent_data as $parent_key => $parent)
                    {
                        if(is_array($parent)) {
                            if(!isset($parent['id'])) {
                                continue;
                            }

                            if($parent['id'] == $value['id_parent']) {
                                $path[] = $parent;
                            }
                        }
                        else if($parent_key == 'id')
                        {
                            $path[] = $parent_data;
                        }
                    }
                }
                break;
            }

            $bradcrumbs_list = array_reverse($path);

            if(!empty($bradcrumbs_list))
            {
                foreach($bradcrumbs_list as $category_item)
                {
                    $bradcrumbs['pages/categories/lista.html?id_parent='.$category_item['id']]
                        = $category_item['text'];
                }
            }

        }

        return $bradcrumbs;
    }

}

