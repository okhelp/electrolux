<?php

class App__Ed__Controller__Pages__Index extends Lib__Base__Ed_Controller
{

    public function action_index()
    {
        redirect(BASE_URL.'ed/pages/lista.html');
    }

    public function action_lista()
    {
        //pobieram listę podstron
        $list = App__Ed__Model__Pages::find('all');

        //przekazuję dane do szablonu
        $this->view->assign('list',$list);

        //ładuję szablon
        $this->template = 'pages/lista.tpl';
        $this->page_title = 'Lista podstron';
        $this->breadcrumb = array(
            'pages/' => 'Podstrony',
            'pages/lista.html' => 'Lista'
        );
    }

    public function action_ed()
    {
        //określam czy to jest dodawanie, czy edycja, ustawiam id podstrony
        $id = !empty($_GET) && !empty($_GET['id']) ? (int)$_GET['id'] : 0;

        //ustawiam obiekt
        $pages = !empty($id) ? App__Ed__Model__Pages::find($id) : new App__Ed__Model__Pages;

        //zapis danych
        if(!empty($_POST) && !empty($_POST['save']))
        {
            $data_post = $_POST;

            if(isset($data_post['save']))
            {
                unset($data_post['save']);
            }

            //jeżeli mamy upload logotypu na socialmedia
            if(!empty($data_post['upload_1']))
            {
                $data_post['social_logo'] = trim($data_post['upload_1']);
                unset($data_post['upload_1']);
            }
            else
            {
                $data_post['social_logo'] = (int)$data_post->social_logo;
            }

            //określam dane do zapisu
            foreach($data_post as $item_name => $item_value)
            {
                $pages->{$item_name} = $item_value;
            }

            //pola przy edycji
            if($id)
            {
                $pages->ts_ed = date('Y-m-d H:i:s');
            }
            else //pola przy dodawaniu
            {
                $pages->id_user = Lib__Session::get('id');
            }

            //zapis do bazy
            $pages->save();

            //czyszczę cache dla routingu
            Lib__Memcache::delete("Pages__Category__Routing::check");

            //czyszczę cachę dla menu
            Lib__Memcache::delete("Pages__Category__Links::*");

            //określam wiadomość
            $message = !empty($id) ? "Podstrona została zmieniona." : "Podstrona została dodana.";

            //przekierowuje na listę podstron
            go_back("g|$message",'ed/pages/lista.html');
        }

        //ustawiam tytuł strony
        $page_title = !empty($id) ? "Edytuj podstronę: $pages->title" : "Dodaj nową podstronę";

        //ładuję zmienne do szablonu
        $this->view->assign('item',$pages);
        $this->view->assign('page_title',$page_title);
        $this->view->assign('category_tree',
                            App__Ed__Model__Pages__Category__Tree::get_ed_tree(0, -1, 'id_category', !empty($pages->id_category) ? $pages->id_category : '' ));

        //ładuję szablon
        $this->template = 'pages/ed.tpl';
        $this->page_title = $page_title;
        $this->breadcrumb = array(
            'pages/' => 'Podstrony',
            '' => $page_title
        );
    }

    public function action_usun()
    {
        $id_pages = !empty($_GET) && !empty($_GET['id']) ? (int)$_GET['id'] : 0;

        if(!empty($id_pages))
        {
            $pages = App__Ed__Model__Pages::find($id_pages);
            if(!empty($pages))
            {
                if($pages->delete())
                {
                    go_back("g|".lang("Podstrona została poprawnie usunięta."));
                }
                else
                {
                    go_back("d|".lang("Wystąpił błąd podczas usuwania podstrony."));
                }
            }
            else
            {
                go_back("d|".lang("Podstrona nie istnieje."));
            }
        }
        else
        {
            go_back("d|".lang("Proszę podać id podstrony"));
        }
    }

}

