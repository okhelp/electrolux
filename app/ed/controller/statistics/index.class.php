<?php

use App\modules\prizes\ed\models\Prize_model;
use \App\modules\products\front\models\Product_lang_model;

class App__Ed__Controller__Statistics__Index extends Lib__Base__Ed_Controller
{
	public function action_index()
	{
		$matomo = new App__Ed__Model__Matomo();
//		var_dump($matomo->getCategories());die;
		$filters = [
		    'label' => 'Najczęściej używane filtry',
		    'data' => $this->getFilters($matomo),
            'type' => 'bar',
        ];
        $products = [
            'label' => 'Najczęściej oglądane produkty',
            'data' => $this->getProducts($matomo),
            'type' => 'horizontalBar',
        ];
        $prizes = [
            'label' => 'Najczęściej oglądane nagrody',
            'data' => $this->getPrizes($matomo),
            'type' => 'horizontalBar',
        ];
        $navigations = [
            'label' => 'Nawigacja na stronie',
            'data' => $this->getNavigations($matomo),
            'type' => 'horizontalBar',
        ];
		$statistics = [];
		$statistics[] = $matomo->getCategoryByID(5);
		$statistics[] = $matomo->getCategoryByID(6);
		$statistics[] = $matomo->getCategoryByID(11);
        $statistics[] = $matomo->getCategoryByID(13);
		$statistics[] = $matomo->getCategoryByID(12);
		//ładuje zmienne do szablonu
		$this->view->assign('statistics', $statistics); // Podział grup towarowych
        $this->view->assign('charts', [
            'filter' => $filters,
            'product' => $products,
            'prize' => $prizes,
            'navigation' => $navigations,
        ]);
		//ładuję szablon
		$this->template = 'statistics/index.tpl';
		$this->page_title = 'Statystyki';
		$this->breadcrumb = array(
			'' => 'Statystyki'
		);
	}

    private function getMax(array $array = [], int $elements = 5)
    {
        $result = [];
        for ($a = 0; $a < $elements && count($array); $a++) {
            $key = $this->maxValueInArray($array, 'nb_visits');
            $result[] = $array[$key];
            unset($array[$key]);
        }
        return $result;
    }

    private function maxValueInArray(array $array, string $keyToSearch)
    {
        $currentMax = null;
        $keyToReturn = null;
        foreach ($array as $k => $arr) {
            foreach ($arr as $key => $value) {
                if ($key == $keyToSearch && ($value >= $currentMax)) {
                    $currentMax = $value;
                    $keyToReturn = $k;
                }
            }
        }
        return $keyToReturn;
    }

    private function getFilters(App__Ed__Model__Matomo $matomo): array
    {
        $filters = $this->getMax($matomo->getValuesByCategoryID(7, 'Events.getNameFromCategoryId'));
        $filterLabels = [];
        $filterValues = [];
        foreach ($filters as $filter) {
            $filterLabels[] = $filter['label'];
            $filterValues[] = $filter['nb_visits'];
        }
        return [
            'labels' => $filterLabels,
            'values' => $filterValues,
        ];
    }

    private function getNavigations(App__Ed__Model__Matomo $matomo): array
    {
        $navigations = $this->getMax($matomo->getValuesByCategoryID(2, 'Events.getNameFromCategoryId'), 10);
        $filterLabels = [];
        $filterValues = [];
        foreach ($navigations as $navigation) {
            $text = $navigation['label'];
            $text = preg_replace('!\s+!', ' ', trim(substr($text, stripos($text, 'TEXT:') + strlen('TEXT:'))));
            if ($text) {
                $filterLabels[] = $text;
                $filterValues[] = $navigation['nb_visits'];
            }
        }
        return [
            'labels' => $filterLabels,
            'values' => $filterValues,
        ];
    }

    private function getProducts(App__Ed__Model__Matomo $matomo): array
    {
        $products = $this->getMax($matomo->getValuesByCategoryID(4, 'Events.getNameFromCategoryId'), 10);
        $productLabels = [];
        $productValues = [];
        foreach ($products as $product) {
            $sql = <<<SQL
SELECT name
FROM product_lang
WHERE product_id={$product['label']}
LIMIT 1
SQL;
            $productLabels[] = Product_lang_model::find_by_sql($sql)[0]->name ?? 'Produkt usunięty';
            $productValues[] = $product['nb_visits'];
        }
        return [
            'labels' => $productLabels,
            'values' => $productValues,
        ];
    }

    private function getPrizes(App__Ed__Model__Matomo $matomo): array
    {
        $prizes = $this->getMax($matomo->getValuesByCategoryID(3, 'Events.getNameFromCategoryId'), 10);
        $prizeLabels = [];
        $prizeValues = [];
        foreach ($prizes as $prize) {
            $sql = <<<SQL
SELECT `name`
FROM prize
JOIN prize_lang ON prize.id=prize_lang.prize_id
WHERE prize.id={$prize['label']}
LIMIT 1
SQL;
            $prizeLabels[] = Prize_model::find_by_sql($sql)[0]->name ?? 'Nagroda usunięta';
            $prizeValues[] = $prize['nb_visits'];
        }
        return [
            'labels' => $prizeLabels,
            'values' => $prizeValues,
        ];
    }
}