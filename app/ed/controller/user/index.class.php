<?php

class App__Ed__Controller__User__Index extends Lib__Base__Ed_Controller
{

    public function action_index()
    {
        redirect(BASE_URL . 'ed/user/lista.html');
    }

    public function action_lista()
    {
        $id_service = (int)Lib__Session::get('id_service');
        
        //pobieram listę administratorów
        $users = App__Ed__Model__Users::find_by_sql("
            SELECT u.*
            FROM users u
            INNER JOIN users_service us 
              ON us.id_user = u.id
            WHERE u.id IN (
              SELECT id_user FROM acl_group_user WHERE id_group = 1
            ) AND us.id_service = $id_service
            GROUP by u.id
            ORDER by name ASC, type ASC
        ");

        $this->view->assign('users',$users);

        $this->template = 'user/lista.tpl';
        $this->page_title = 'Lista pracowników';
        $this->breadcrumb = [
            'user/'           => 'Konta użytkowników',
            'user/lista.html' => 'Lista',
        ];
    }

    public function action_ajax_pobierz_strukture()
    {
        $structure = new App__Ed__Model__Users__Structure;

        //pokazuję linki do edycji
        if (!empty($_GET) && !empty($_GET['edit_links']))
        {
            $structure->show_edit_links = true;
        }

        //pokazuje tylko działy
        if (!empty($_GET) && !empty($_GET['only_sections']))
        {
            $structure->show_only_sections = true;
        }

        //pokazuję checkboxy
        if (!empty($_GET) && !empty($_GET['show_checkboxes']))
        {
            $structure->show_checkboxes = true;
        }

        //pokazuję radiobuttony
        if (!empty($_GET) && !empty($_GET['show_radiobuttons']))
        {
            $structure->show_radiobuttons = true;
        }

        //zanaczam radio badz checkbox
        if (!empty($_GET) && !empty($_GET['selected_items']))
        {
            $structure->selected_items = json_decode($_GET['selected_items'], true);
        }

        echo json_encode($structure->show());
    }

    public function action_ed_section()
    {
        //zapis
        if (!empty($_POST))
        {
            $id_section = !empty($_POST['id_section']) ? (int)$_POST['id_section'] : 0;
            $id_up = !empty($_POST['id_up']) ? (int)$_POST['id_up'] : 0;

            //nowy dział
            if (empty($id_section) && !empty($id_up))
            {
                $section = new App__Ed__Model__Users;
                $section->name = $_POST['name'];
                $section->id_up = $id_up;
                $section->status = 1;
                $msg = "Dział został poprawnie dodany";
            }

            //edycja
            if (empty($id_up) && !empty($id_section))
            {
                $section = App__Ed__Model__Users::find($id_section);
                if (empty($section))
                {
                    go_back("d|Podany dział nie został znaleziony w bazie danych.");
                }

                $section->name = $_POST['name'];
                $msg = "Dział został poprawnie zmieniony";
            }

            //zapis
            if ($section->save())
            {
                go_back("g|$msg");
            }
            else
            {
                go_back("d|Wystąpił błąd podczas zapisu do bazy danych.");
            }

        }


        $id_section = !empty($_GET) && !empty($_GET['id']) ? (int)$_GET['id'] : 0;
        $id_up = !empty($_GET) && !empty($_GET['id_up']) ? (int)$_GET['id_up'] : 0;

        $section = !empty($id_section) ? App__Ed__Model__Users::find($id_section) : new App__Ed__Model__Users;

        $smarty = new Smarty;
        $smarty->assign('id_section', $id_section);
        $smarty->assign('id_up', $id_up);
        $smarty->assign('item', $section);
        echo $smarty->fetch('user/ed_section.tpl');
    }

    public function action_remove_section()
    {
        $id_section = !empty($_GET) && !empty($_GET['id']) ? (int)$_GET['id'] : 0;

        if (empty($id_section))
        {
            go_back("d|Proszę wprowadzić id działu");
        }

        $section = App__Ed__Model__Users::find($id_section);
        if (empty($section))
        {
            go_back("d|Podany dział nie został odnaleziony w bazie danych");
        }

        //sprawdzam czy istnieje jakies przypisanie do tego działu
        $find_connection = App__Ed__Model__Users::find_by_id_up($id_section);

        if (!empty($find_connection))
        {
            go_back("d|Nie można usunąć tego działu, ponieważ są do niego przypisane inne działy, badź pracownicy");
        }
        else
        {
            if ($section->delete())
            {
                go_back("g|Dział został poprawnie usunięty");
            }
            else
            {
                go_back("d|Wystąpił błąd podczas usuwania działu.");
            }
        }

    }

    public function action_ajax_szukaj_pracownika()
    {
        $return_list = [];

        if (!empty($_POST) && !empty($_POST['term']))
        {

            $term = $_POST['term'];
            $where = App__Ed__Model__Users::where(
                "(name LIKE '$term%') OR (surname LIKE '$term%') OR (CONCAT_WS(' ',name,surname) LIKE '$term%') OR (CONCAT_WS(' ',surname,name) LIKE '$term%')");
            $where->add("type = 1");

            //jeżeli są podane parametry to uzupełniam parametr where
            if (!empty($_POST['params']))
            {
                foreach ($_POST['params'] as $k => $v)
                {
                    $where->add("$k = '$v'");
                }
            }

            //jeżeli jest parametr not
            if (!empty($_POST['not']))
            {
                $where->add("id NOT IN (" . implode($_POST['not']) . ")");
            }

            $limit = App__Ed__Model__Users::limit("0,30");

            $list = App__Ed__Model__Users::find($where, $limit);

            if (!empty($list))
            {
                foreach ($list as $l)
                {
                    $return_list[] = [
                        'id'    => $l->id,
                        'value' => $l->name . " " . $l->surname,
                    ];
                }
            }
        }

        echo json_encode($return_list);
    }

    public function action_ajax_sprawdz_dostepnosc_email()
    {
        $return = [
            'valid'   => false,
            'message' => 'Proszę wprowadzić adres e-mail',
        ];

        if (!empty($_POST) && !empty($_POST['email']))
        {

            //sprawdzam czy adres e-mail został poprawnie wpisany
            if (filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) === false)
            {
                $return = [
                    'valid'   => false,
                    'message' => 'Wprowadzono niepoprawny adres e-mail.',
                ];
            }
            else
            {
                $where = App__Ed__Model__Users::where("email = '{$_POST['email']}'");

                //jezeli getem przychodzi id_user to dodaję go do warunku
                if (!empty($_GET) && !empty($_GET['id_user']))
                {
                    $where->add("id != " . (int)$_GET['id_user']);
                }

                $find_user = App__Ed__Model__Users::find($where);
                if (empty($find_user))
                {
                    $return = [
                        'valid' => true,
                    ];
                }
                else
                {
                    $return = [
                        'valid'   => false,
                        'message' => 'Podany adres e-mail jest zajęty.',
                    ];
                }
            }

        }

        echo json_encode($return);
    }

    public function action_ajax_ustaw_dostepnosc()
    {
        if (!empty($_POST) && !empty($_POST['action']) && $_POST['action'] == 'check')
        {
            $id_user = (int)Lib__Session::get('id');
            $user = App__Ed__Model__Users::find($id_user);
            if (!empty($user))
            {
                $user->last_activity = time();
                $user->save();
            }
        }

    }

    public function action_ajax_pobierz_uzytkownika()
    {
        $return_list = [];

        if (!empty($_POST) && !empty($_POST['term']))
        {
            $term = $_POST['term'];
            $id_service = (int)Lib__Session::get('id_service');

            $sql = "SELECT u.*, '' as company
                FROM users u 
                INNER JOIN users_service us ON us.id_service = $id_service AND us.id_user = u.id
                WHERE u.type = 1 AND u.status = 1 AND 
                  (u.name LIKE '$term%') OR (u.surname LIKE '$term%') OR (CONCAT_WS(' ',u.name,u.surname) LIKE '$term%') OR (CONCAT_WS(' ',u.surname,u.name) LIKE '$term%')
                
                
                UNION DISTINCT 
                
                SELECT u.*, c.name as company
                FROM users u
                INNER JOIN users_service us ON us.id_service = $id_service AND us.id_user = u.id
                INNER JOIN company_users cu ON cu.id_user = u.id
                INNER JOIN company c ON c.id = cu.id_company
                WHERE u.type = 1 AND u.status = 1 AND (
                  (u.name LIKE '$term%') OR (u.surname LIKE '$term%') OR (CONCAT_WS(' ',u.name,u.surname) LIKE '$term%') OR (CONCAT_WS(' ',u.surname,u.name) LIKE '$term%')
                  OR (c.name LIKE '$term%')
                )
                
                ";

            $list = App__Ed__Model__Users::find_by_sql($sql);

            if (!empty($list))
            {
                foreach ($list as $l)
                {
                    //określam wartość
                    $value = $l->name . " " . $l->surname;

                    if (!empty($l->company))
                    {
                        $value .= " | " . $l->company;
                    }
                    else
                    {
                        $value .= " | PRACOWNIK";
                    }

                    $return_list[] = [
                        'id'    => $l->id,
                        'value' => $value,
                    ];
                }
            }
        }

        echo json_encode($return_list);
    }
}
