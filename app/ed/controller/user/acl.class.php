<?php

class App__Ed__Controller__User__Acl extends Lib__Base__Ed_Controller
{
    public function action_lista()
    {
        $this->template = 'user/acl/lista.tpl';
        $this->page_title = 'Uprawnienia';
        $this->breadcrumb = [
            'user/'               => 'Konta użytkowników',
            'user/acl/lista.html' => 'Uprawnienia',
        ];
    }

    public function action_ajax_pokaz_zakladke()
    {
        if (!empty($_POST) && !empty($_POST['zakladka']))
        {
            $zakladka = $_POST['zakladka'];
            $function_name = "show_tab_list_$zakladka";
            $this->{$function_name}();
        }
    }

    private function show_tab_list_groups()
    {
        $list = App__Ed__Model__Acl__Group::find(App__Ed__Model__Acl__Group::order('name ASC'));

        $smarty = new Smarty();
        $smarty->assign('list', $list);
        echo $smarty->fetch('user/acl/_partials/tabs/groups.tpl');
    }

    private function show_tab_list_modules()
    {
        $list = App__Ed__Model__Acl__Module::find(App__Ed__Model__Acl__Module::order('name ASC'));

        $smarty = new Smarty();
        $smarty->assign('list', $list);
        echo $smarty->fetch('user/acl/_partials/tabs/modules.tpl');
    }

    public function action_ed_groups()
    {
        $id_group = !empty($_GET) && !empty($_GET['id']) ? (int)$_GET['id'] : 0;

        //tworzę obiekt
        $group = !empty($id_group) ? App__Ed__Model__Acl__Group::find($id_group) : new App__Ed__Model__Acl__Group;

        $smarty = new Smarty();
        $smarty->assign('item', $group);
        echo $smarty->fetch('user/acl/_partials/tabs/ed_grupa.tpl');
    }


    public function action_zapisz_grupe()
    {
        if (!empty($_POST))
        {
            $id_group = (int)$_POST['id_group'];

            $group = !empty($id_group) ? App__Ed__Model__Acl__Group::find($id_group) : new App__Ed__Model__Acl__Group;
            $group->name = $_POST['name'];
            $group->description = $_POST['description'];

            $msg = !empty($id_group) ? "Dane grupy zostały zmienione." : "Nowa grupa została dodana";

            if ($group->save())
            {
                go_back("g|$msg");
            }
            else
            {
                go_back("d|Wystąpił błąd podczas zapisu do bazy danych");
            }

        }
    }

    public function action_usun_grupe()
    {
        if (!empty($_GET) && !empty($_GET['id']))
        {
            $id_group = (int)$_GET['id'];

            //pobieram obiekt grupy
            $group = App__Ed__Model__Acl__Group::find($id_group);

            if (empty($group))
            {
                go_back("d|Grupa nie została odnaleziona w bazie danych.");
            }

            //sprawdzam czy do grupy jest przypisany moduł
            $find_module_connection = App__Ed__Model__Acl__Group__Module::find_by_id_group($id_group);
            if (!empty($find_module_connection))
            {
                go_back("d|Nie można usunać grupy, ponieważ jest do niej przypisany min. 1 moduł.");
            }

            //sprawdza czy nie ma powiązania pomiędzy użytkownikiem a grupą
            $find_user_connection = App__Ed__Model__Acl__Group__User::find_by_id_group($id_group);
            if (!empty($find_user_connection))
            {
                go_back("d|Nie można usunać grupy, ponieważ jest do niej przypisany min. 1 użytkownik");
            }

            if ($group->delete())
            {
                go_back("g|Grupa została poprawnie usunięta");
            }
            else
            {
                go_back("d|Wystąpił błąd podczas usuwania grupy. Prosze spróbować ponownie");
            }
        }
        else
        {
            go_back("d|Proszę wprowadzić id grupy, którą chcesz usunąć.");
        }
    }

    public function action_ed_modules()
    {
        $id_group = !empty($_GET) && !empty($_GET['id']) ? (int)$_GET['id'] : 0;

        //tworzę obiekt
        $group = !empty($id_group) ? App__Ed__Model__Acl__Module::find($id_group) : new App__Ed__Model__Acl__Module;

        $smarty = new Smarty();
        $smarty->assign('item', $group);
        echo $smarty->fetch('user/acl/_partials/tabs/ed_modules.tpl');
    }

    public function action_zapisz_modul()
    {
        if (!empty($_POST))
        {
            $id_module = (int)$_POST['id_module'];

            $module = !empty($id_module) ? App__Ed__Model__Acl__Module::find($id_module) : new App__Ed__Model__Acl__Module;

            if (!$id_module)
            {
                $module->name = $_POST['name'];
            }

            $module->description = $_POST['description'];

            $msg = !empty($id_module) ? "Dane modułu zostały zmienione." : "Nowy moduł został dodany";

            if ($module->save())
            {
                go_back("g|$msg");
            }
            else
            {
                go_back("d|Wystąpił błąd podczas zapisu do bazy danych");
            }

        }
    }

    private function show_tab_list_user_group()
    {
        $sql = "SELECT agu.id as id_acl, agu.*, ag.*, ag.name as group_name, u.* 
                FROM `acl_group_user` agu
                INNER JOin acl_group ag ON ag.id = agu.id_group
                INNER JOIN users u ON u.id = agu.id_user
                ORDER by agu.id ASC";
        $list = App__Ed__Model__Acl__Group__User::find_by_sql($sql);

        $smarty = new Smarty();
        $smarty->assign('list', $list);
        echo $smarty->fetch('user/acl/_partials/tabs/user_group.tpl');
    }

    public function action_ed_user_group()
    {
        $id_user_group = !empty($_GET) && !empty($_GET['id']) ? (int)$_GET['id'] : 0;

        if (!empty($id_user_group))
        {
            $sql = "SELECT agu.id as id_acl, agu.*, ag.*, ag.name as group_name, u.*
                FROM `acl_group_user` agu
                INNER JOin acl_group ag ON ag.id = agu.id_group
                INNER JOIN users u ON u.id = agu.id_user
                WHERE agu.id = $id_user_group";
            $user_group = App__Ed__Model__Acl__Group__User::find_by_sql($sql);
            $user_group = reset($user_group);
        }
        else
        {
            $user_group = new App__Ed__Model__Acl__Group__User;
        }

        $smarty = new Smarty();
        $smarty->assign('item', $user_group);
        echo $smarty->fetch('user/acl/_partials/tabs/ed_user_group.tpl');
    }

    public function action_zapisz_uzytkownik_grupa()
    {
        if (!empty($_POST))
        {
            $id = !empty($_POST['id']) ? (int)$_POST['id'] : 0;

            //ustawiam obiekt
            $acl_group_user = !empty($id) ? App__Ed__Model__Acl__Group__User::find($id) : new App__Ed__Model__Acl__Group__User;

            $acl_group_user->id_group = (int)$_POST['id_group'];
            $acl_group_user->id_user = (int)$_POST['id_user'];

            if ($acl_group_user->save())
            {
                $msg = !empty($id) ? "Przypisanie zostało poprawnie zaktualizowane" : "Przypisanie zostało dodane.";
                go_back("g|$msg");
            }
            else
            {
                go_back("d|Wystąpił błąd podczas zapisu do bazy danych.");
            }
        }
    }

    public function action_usun_uzytkownik_grupa()
    {
        if (!empty($_GET) && !empty($_GET['id']))
        {
            $id_acl = (int)$_GET['id'];

            if (empty($id_acl))
            {
                go_back("d|Proszę wprowadzić id powiązania");
            }

            $acl = App__Ed__Model__Acl__Group__User::find($id_acl);

            if (empty($acl))
            {
                go_back("d|Podane powiązanie nie zostało odnalezione w bazie danych");
            }

            if ($acl->delete())
            {
                go_back("g|Powiązanie zostało poprawnie usunięte");
            }
            else
            {
                go_back("d|Wystąpił błąd podczas usuwania powiązania");
            }

        }

    }

    private function show_tab_list_group_module()
    {
        $sql = "SELECT agm.id as id_acl, agm.*, am.id as id_module, am.description as module_description, am.*, am.name as module_name, ag.name as group_name, ag.id as id_group, ag.description as group_description, ag.*
                FROM `acl_group_module` agm
                INNER JOin acl_module am ON am.id = agm.id_module
                INNER JOIN acl_group ag ON ag.id = agm.id_group";
        $list = App__Ed__Model__Acl__Group__User::find_by_sql($sql);

        $smarty = new Smarty();
        $smarty->assign('list', $list);
        echo $smarty->fetch('user/acl/_partials/tabs/group_module.tpl');
    }

    public function action_ed_group_module()
    {
        $id_module_group = !empty($_GET) && !empty($_GET['id']) ? (int)$_GET['id'] : 0;

        if (!empty($id_module_group))
        {
            $sql = "SELECT agm.id as id_acl, agm.*, am.id as id_module, am.*, am.name as module_name, ag.name as group_name, ag.id as id_group, ag.* 
                FROM `acl_group_module` agm
                INNER JOin acl_module am ON am.id = agm.id_module
                INNER JOIN acl_group ag ON ag.id = agm.id_group
                WHERE agu.id = $id_module_group";
            $module_group = App__Ed__Model__Acl__Group__Module::find_by_sql($sql);
            $module_group = reset($module_group);
        }
        else
        {
            $module_group = new App__Ed__Model__Acl__Group__Module;
        }

        $smarty = new Smarty();
        $smarty->assign('item', $module_group);
        echo $smarty->fetch('user/acl/_partials/tabs/ed_group_module.tpl');
    }

    public function action_zapisz_grupa_modul()
    {
        if (!empty($_POST))
        {
            $id = !empty($_POST['id']) ? (int)$_POST['id'] : 0;

            //ustawiam obiekt
            $acl_group_module = !empty($id) ? App__Ed__Model__Acl__Group__Module::find($id) : new App__Ed__Model__Acl__Group__Module;

            $acl_group_module->id_group = (int)$_POST['id_group'];
            $acl_group_module->id_module = (int)$_POST['id_module'];

            if ($acl_group_module->save())
            {
                $msg = !empty($id) ? "Przypisanie zostało poprawnie zaktualizowane" : "Przypisanie zostało dodane.";
                go_back("g|$msg");
            }
            else
            {
                go_back("d|Wystąpił błąd podczas zapisu do bazy danych.");
            }
        }
    }

    public function action_usun_grupa_modul()
    {
        if (!empty($_GET) && !empty($_GET['id']))
        {
            $id_acl = (int)$_GET['id'];

            if (empty($id_acl))
            {
                go_back("d|Proszę wprowadzić id powiązania");
            }

            $acl = App__Ed__Model__Acl__Group__Module::find($id_acl);

            if (empty($acl))
            {
                go_back("d|Podane powiązanie nie zostało odnalezione w bazie danych");
            }

            if ($acl->delete())
            {
                go_back("g|Powiązanie zostało poprawnie usunięte");
            }
            else
            {
                go_back("d|Wystąpił błąd podczas usuwania powiązania");
            }

        }

    }

    public function action_ajax_pobierz_nazwe_grupy()
    {
        $return_list = [];

        if (!empty($_POST) && !empty($_POST['term']))
        {
            $term = $_POST['term'];

            $where = App__Ed__Model__Acl__Group::where("name LIKE '$term%'");
            $limit = App__Ed__Model__Acl__Group::limit("0,30");

            $list = App__Ed__Model__Acl__Group::find($where, $limit);
            if (!empty($list))
            {
                foreach ($list as $l)
                {
                    $return_list[] = [
                        'id'    => $l->id,
                        'value' => $l->name,
                    ];
                }
            }
        }

        echo json_encode($return_list);
    }

    public function action_ajax_pobierz_nazwe_modulu()
    {
        $return_list = [];

        if (!empty($_POST) && !empty($_POST['term']))
        {
            $term = $_POST['term'];

            $where = App__Ed__Model__Acl__Module::where("name LIKE '$term%'");
            $limit = App__Ed__Model__Acl__Module::limit("0,30");

            $list = App__Ed__Model__Acl__Module::find($where, $limit);
            if (!empty($list))
            {
                foreach ($list as $l)
                {
                    $return_list[] = [
                        'id'    => $l->id,
                        'value' => $l->name,
                    ];
                }
            }
        }

        echo json_encode($return_list);
    }

    public function action_ajax_pobierz_nazwe_pracownika()
    {
        $return_list = [];

        if (!empty($_POST) && !empty($_POST['term']))
        {

            $term = $_POST['term'];
            $where = App__Ed__Model__Users::where("(name LIKE '$term%') OR (surname LIKE '$term%')");
            $limit = App__Ed__Model__Users::limit("0,30");

            $list = App__Ed__Model__Users::find($where, $limit);

            if (!empty($list))
            {
                foreach ($list as $l)
                {
                    $return_list[] = [
                        'id'    => $l->id,
                        'value' => $l->name . " " . $l->surname,
                    ];
                }
            }
        }

        echo json_encode($return_list);
    }


}