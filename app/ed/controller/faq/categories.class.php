<?php

use App\ed\model\faq\Faq_categories_tree;
use App\ed\model\faq\Faq_category_model;

class App__Ed__Controller__Faq__Categories extends Lib__Base__Ed_Controller
{
    public function action_lista()
    {
        $tree = Faq_categories_tree::get_categories(0, 1, true);

        $this->view->assign('tree', json_encode($tree, JSON_UNESCAPED_UNICODE));
        $this->template = 'faq/categories_list.tpl';
        $this->page_title = "Lista kategorii";
        $this->breadcrumb = ['' => 'Lista kategorii'];
    }

    public function action_ed()
    {
        if (!empty($_POST) && !empty($_GET['id']))
        {
            $category_data = Faq_category_model::find($_GET['id']);
            $category_data->{Faq_category_model::COLUMN_NAME} = $_POST['name'];
            $category_data->{Faq_category_model::COLUMN_SEO_NAME} = url_slug($_POST['name']);

            go_back($category_data->save() ? 'g|Dane zostały zapisane' : 'd|Dane nie zostały zapisane', BASE_ED_URL . 'faq/categories/lista.html');
        }
        else
        {
            if (!empty($_GET['id']))
            {
                $category = Faq_category_model::find($_GET['id']);

                if (!empty($category))
                {
                    $this->view->assign('category', $category);

                    $this->template = 'faq/category_edit.tpl';
                    $this->page_title = "Edycja kategorii";
                }
            }
            else
            {
                go_back('d|Brak dostępu');
            }
        }
    }

    public function action_remove()
    {
        $message = 'd|Brak dostępu';

        if (!empty($_GET['id']))
        {
            $remove_product_category = Faq_category_model::find($_GET['id']);

            if ($remove_product_category->delete())
            {
                $message = "g|Kategoria została usunięta.";
            }
            else
            {
                $message = "d|Kategoria nie została usunięta.";
            }
        }

        go_back($message);
    }

    public function action_create()
    {
        if (!empty($_POST['save']))
        {
            $category_add = new Faq_category_model();
            $category_add->{Faq_category_model::COLUMN_NAME} = $_POST['name'];
            $category_add->{Faq_category_model::COLUMN_SEO_NAME} = url_slug($_POST['name']);

            go_back($category_add->save() ? "g|Kategoria została dodana." : "d|Kategoria nie została dodana.", BASE_ED_URL . 'faq/categories/lista.html');
        }

        $this->template = 'faq/category_add.tpl';
        $this->page_title = "Dodanie kategorii";
    }
}