<?php

use App\ed\model\faq\Faq_categories_tree;
use App\ed\model\faq\Faq_model;
use App\ed\model\faq\Get_faq_list;

class App__Ed__Controller__Faq__Index extends Lib__Base__Ed_Controller
{
    public function __construct()
    {
        parent::__construct();

        if(!App__Ed__Model__Acl::has_access('faq', 0))
        {
            go_back('w|Brak dostępu.'); die();
        }
    }

    public function action_lista()
    {
        $this->view->assign('faqs', Get_faq_list::get($this->_id_service, $this->session['id_lang'], true));

        $this->template = 'faq/questions.tpl';

        $this->breadcrumb = [
            'faq/lista.html' => lang('FAQ'),
        ];
    }

    public function action_ed()
    {
        $id_faq = empty($_GET['id']) ? 0 : (int)$_GET['id'];

        if (!empty($_POST))
        {
            $faq = empty($id_faq) ? new Faq_model() : Faq_model::find($id_faq);
            $faq->{Faq_model::COLUMN_ID_CREATOR} = $this->_user->id;
            $faq->{Faq_model::COLUMN_STATUS} = $_POST['status'];
            $faq->{Faq_model::COLUMN_QUESTION} = $_POST['question'];
            $faq->{Faq_model::COLUMN_ANSWER} = $_POST['answer'];
            $faq->{Faq_model::COLUMN_ID_PARENT} = $_POST['id_parent'];

            go_back($faq->save() ? 'g|Zapisano dane.' : 'w|Nie zapisano danych.', BASE_ED_URL . 'faq/lista.html');
        }

        $faq = empty($id_faq) ? new Faq_model() : Faq_model::find($id_faq);

        $this->view->assign(
            'category_tree', Faq_categories_tree::get_ed_tree(
            0, -1, 'id_parent',
            !empty($faq->{Faq_model::COLUMN_ID_PARENT}) ? $faq->{Faq_model::COLUMN_ID_PARENT} : null)
        );

        $this->view->assign('faq', $faq);

        $this->template = 'faq/question.tpl';

        $this->breadcrumb = [
            'faq/lista.html' => 'FAQ',
            'faq/ed.html?id=' . $id_faq => $id_faq ? 'Edycja pytania' : 'Dodawanie pytania',
        ];
    }

    public function action_usun()
    {
        if(!empty($_GET['id']))
        {
            $faq = Faq_model::find($_GET['id']);
            $faq->{Faq_model::COLUMN_DELETED_AT} = (new DateTime('now'))->format('Y-m-d');

            go_back($faq->save() ? 'g|Pytanie zostało usunięte.' : 'w|Nie udało się usunąć pytania');
        }
        else
        {
            go_back('w|Brak dostępu.');
        }
    }
}