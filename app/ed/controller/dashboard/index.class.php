<?php

use App\ed\model\dashboard\Dashboard_products_model;
use App\ed\model\dashboard\Get_bottom_five;
use App\ed\model\dashboard\Get_dashboard_prizes;
use App\ed\model\dashboard\Get_dashboard_products;
use App\ed\model\dashboard\Get_dashboard_statistics;
use App\ed\model\dashboard\Get_top_five;
use App\ed\model\users\Get_by_ids;
use App\front\models\points\Get_points_log;
use App\ed\model\users\Get_subordinate_companies;
use App\ed\model\company\Get_company_users;
use App\modules\order\ed\models\Order_model;

class App__Ed__Controller__Dashboard__Index extends Lib__Base__Ed_Controller
{
    public function action_index()
    {
        $statistics = new Get_dashboard_statistics((int)$this->session['id_service']);

        $dashboard_products = Dashboard_products_model::find_by_sql(
            "
	        SELECT *
	        FROM dashboard_products
	        WHERE id_service = " . $this->session['id_service'] . "
	    ");

        $products_customers = [];
        if (!empty($dashboard_products))
        {
            foreach ($dashboard_products as $dashboard_product)
            {
                $products_customers[$dashboard_product->id_customer] = [
                    'id'   => $dashboard_product->id_customer,
                    'name' => $dashboard_product->customer_name,
                ];
            }
        }

        $products_supervisors = [];
        if (!empty($dashboard_products))
        {
            foreach ($dashboard_products as $dashboard_product)
            {
                $products_supervisors[$dashboard_product->id_supervisor] = [
                    'id'   => $dashboard_product->id_supervisor,
                    'name' => $dashboard_product->supervisor_name,
                ];
            }
        }

        $this->view->assign('products_customers', $products_customers);
        $this->view->assign('products_supervisors', $products_supervisors);

        $dashboard_prizes = Dashboard_products_model::find_by_sql(
            "
	        SELECT *
	        FROM dashboard_prizes
	        WHERE id_service = " . $this->session['id_service'] . "
	    ");

        $prizes_customers = [];
        if (!empty($dashboard_prizes))
        {
            foreach ($dashboard_prizes as $dashboard_prize)
            {
                $prizes_customers[$dashboard_prize->id_customer] = [
                    'id'   => $dashboard_prize->id_customer,
                    'name' => $dashboard_prize->customer_name,
                ];
            }
        }

        $prizes_supervisors = [];
        if (!empty($dashboard_prizes))
        {
            foreach ($dashboard_prizes as $dashboard_prize)
            {
                $prizes_supervisors[$dashboard_prize->id_supervisor] = [
                    'id'   => $dashboard_prize->id_supervisor,
                    'name' => $dashboard_prize->supervisor_name,
                ];
            }
        }

        $this->view->assign('prizes_customers', $prizes_customers);
        $this->view->assign('prizes_supervisors', $prizes_supervisors);

        $this->view->assign('statistics', $statistics->get());

        $points_transfers = Get_points_log::get([], true);
        $points_senders_users_ids = array_map(function($transfer){ return $transfer->id_sender; }, $points_transfers);
        $points_receivers_users_ids = array_map(function($transfer){ return $transfer->id_receiver; }, $points_transfers);
        $points_transfers_users = Get_by_ids::get(array_unique(array_merge($points_senders_users_ids, $points_receivers_users_ids)));

        $this->view->assign('points_transfers_users', $points_transfers_users);
        $this->view->assign('points_transfers', $points_transfers);

        $this->template = 'dashboard/index.tpl';
        $this->page_title = "Dashboard";
        $this->breadcrumb = ['' => 'Dashboard'];
    }

    public function action_get_unactivated_company()
    {
        $return_data = [];

        //domyślnie, użytkownik
        $users_ids[] = (int)Lib__Session::get('id');

        //jeżeli użytkownik jest z grupy adminów to pobieram grupę użytkowników, które należą do serwisu
        if(!App__Ed__Model__Acl::has_group('trader',0))
        {
            $acl_users = App__Ed__Model__Acl__Group__User::find(
                App__Ed__Model__Acl__Group__User::where("id_group != 2"),
                App__Ed__Model__Acl__Group__User::group("id_user")
            );
            if(!empty($acl_users))
            {
                foreach($acl_users as $acl_user)
                {
                    if(!in_array($acl_user->id_user, $users_ids))
                    {
                        $users_ids[] = $acl_user->id_user;
                    }
                }
            }
        }

        foreach(Get_subordinate_companies::get($users_ids) as $id_company)
        {
            //pomijam firmę electrolux oraz testowe
            if($id_company <= 964 || in_array($id_company,[1,956]))
            {
                continue;
            }
            
            foreach(Get_Company_users::get($id_company) as $company_user)
            {
                if($company_user->role == 'admin' && !$company_user->user->has_first_change_password())
                {
                    //pobieram dane firmy
                    $company = App__Ed__Model__Company::find_by_sql("
                        SELECT c.* 
                        FROM company c 
                        INNER JOIN company_service cs ON cs.id_company = c.id
                        WHERE c.id = $id_company AND cs.id_service = ".(int)Lib__Session::get('id_service')."
                    ");

                    if(!empty($company))
                    {
                        $return_data[] = [
                            'company' => reset($company),
                            'user' => $company_user->user
                        ];
                        
                    }
                    

                }
            }
        }

        $smarty = new Smarty;
        $smarty->assign('unactivated_company', $return_data);
        echo $smarty->fetch('dashboard/_partials/get_unactivated_company.tpl');
    }

    public function action_top_five()
    {
        $top_five = Get_top_five::get($_POST['type'], $this->session['id_service']);

        $smarty = new Smarty();
        $smarty->assign('top_five', $top_five);

        echo $smarty->fetch('dashboard/top_table_body.tpl');
    }

    public function action_bottom_five()
    {
        $top_five = Get_bottom_five::get($_POST['type'], $this->session['id_service']);

        $smarty = new Smarty();
        $smarty->assign('top_five', $top_five);

        echo $smarty->fetch('dashboard/top_table_body.tpl');
    }

    public function action_products()
    {
        $id_service = $this->session['id_service'];
        $interval = $_POST['products_period'];
        $customers_ids = empty($_POST['products_customer']) ? [] : [$_POST['products_customer']];
        $supervisors_ids = empty($_POST['products_supervisor']) ? [] : [$_POST['products_supervisor']];

        echo Get_dashboard_products::get($id_service, $interval, $customers_ids, $supervisors_ids);
    }

    public function action_prizes()
    {
        $id_service = $this->session['id_service'];
        $interval = $_POST['products_period'];
        $customers_ids = empty($_POST['products_customer']) ? [] : [$_POST['products_customer']];
        $supervisors_ids = empty($_POST['products_supervisor']) ? [] : [$_POST['products_supervisor']];

        echo Get_dashboard_prizes::get($id_service, $interval, $customers_ids, $supervisors_ids);
    }

    public function action_targets()
    {
        //maybe later
    }

    public function action_points()
    {
        $response = '';

        if(!empty($_POST['id_user']))
        {
            $points_transfers = Get_points_log::get([(int)$_POST['id_user']]);

            if(!empty($points_transfers))
            {
                $smarty = new Smarty();
                $smarty->assign('points_transfers', $points_transfers);

                echo $smarty->fetch('dashboard/points_transfer_body.tpl');
            }
        }

        echo $response;
    }

    public function action_sync_points()
    {
        $year = date('Y');

        foreach (App__Ed__Model__Users::all() as $user) {
            $points_to_return = 0;
            $orders = Order_model::find_by_sql(<<<SQL
            SELECT products
            FROM `order`
            WHERE id_user=$user->id AND status!=17 AND YEAR(created_at)='$year'
SQL
            );

            if (!empty($orders)) {
                foreach ($orders as $order) {
                    $products_details = empty($order->products) ? [] : json_decode($order->products);

                    if (!empty($products_details)) {
                        foreach ($products_details as $product_details) {
                            if (!empty($product_details->price) && !empty($product_details->count)) {
                                $points_to_return += (int)$product_details->price * $product_details->count;
                            }
                        }
                    }
                }
            }
            if ($points_to_return != 0) {
                App__Ed__Model__Points_log::add($user->id, $points_to_return);
                App__Ed__Model__Points_log::add($user->id, -$points_to_return);
                echo "$user->name => $points_to_return<br />";
            }
        }
    }
} 