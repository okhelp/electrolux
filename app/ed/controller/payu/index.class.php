<?php
class Controller__Payu__Index extends Lib__Base__Controller
{
	
	public function __construct() 
	{
		parent::__construct();
		
		//ładowanie konfiguracji połączenia
		Payu::init();
	}
	
	public function action_nowa_platnosc()
	{
		//parametry
		$params = $_GET['q'];
		$params = urldecode($params);
		$params = base64_decode($params);
		$params = unserialize($params);
		Lib__Session::set('pay_params',$params);
		
		$order = array();
		$order['notifyUrl'] = BASE_URL.'payu/validate_transaction.html';
		$order['continueUrl'] = BASE_URL.'payu/register_pay.html';

		$order['customerIp'] = $_SERVER['REMOTE_ADDR'];
		$order['merchantPosId'] = OpenPayU_Configuration::getOauthClientId()
				? OpenPayU_Configuration::getOauthClientId() 
				: OpenPayU_Configuration::getMerchantPosId();
		$order['description'] = 'Nowa płatność';
		$order['currencyCode'] = 'PLN';
		$order['extOrderId'] = uniqid('', true);
		
		//opisuję produkt
		$order['products'][0]['name'] = $params['name'];
		$order['products'][0]['unitPrice'] = $params['price']*100;
		$order['products'][0]['quantity'] = 1;
		
		$order['totalAmount'] = $params['price']*100;
		
		//otwieram płatność
		$response = OpenPayU_Order::create($order);
		$order_id = $response->getResponse()->orderId;
		Lib__Session::set('payu_orderid',$order_id);
		
		//rejestracja transakcji w bazie
		PayU::register_transaction($order_id);
		
		//przekierowuję na stronę payu
		redirect($response->getResponse()->redirectUri);	
	}
	
	public function action_validate_transaction()
	{
		if ($_SERVER['REQUEST_METHOD'] == 'POST') 
		{
			$body = file_get_contents('php://input');
			$data = trim($body);

			if (!empty($data)) 
			{
				$result = OpenPayU_Order::consumeNotification($data);
				$order_id = $result->getResponse()->order->orderId;
			}

			if ($order_id) 
			{
				$order = OpenPayU_Order::retrieve($order_id);

				//zapis informacji o zakończonej transakcji
				PayU::register_transaction($order_id);

				if($order->getStatus() == 'SUCCESS')
				{
					//pokazanie, że jest ok :-)
					header("HTTP/1.1 200 OK");
				}
			}
			
		}
	}
	
	public function action_register_pay()
	{	
		//odczytuję z sesji order_idb   
		$order_id = Lib__Session::get('payu_orderid');
		
		//maskowanie podstrony, nie ma odpowiednich danych w sesji to wyrzucam 404
		if(!$order_id) show_404(); 

		//pobieram dane transakcji
		$order_info = Payu::read_transaction_info($order_id);
		$status = $order_info->status;
		
		//rejestruje status transakcji w bazie (wykonuje ponowne połączenie do payu - dla pewności)
		Payu::register_transaction($order_id);
		
		//usuwam z sesji order_id
		Lib__Session::remove('payu_orderid');
			
		//pobieram i usuwam hash służący do przekierowania na stronę oznaczenia płatności
		$pay_params = Lib__Session::get('pay_params');
		Lib__Session::remove('pay_params');
		
		//płatność zrealizowana, dodaję żetony do użytkownika
		if($status == 'COMPLETED')
		{
			Users__Wallet::add_tokens($pay_params['id_user'],$pay_params['quantity']);
		}
		
		//może być sytuacja, że nie z payu nie przyszła zwrotka o statusie transakcji, stąd też nie ma możliwości aktywowania punktów,
		//robi to cron, który odpalany jest co 5 minut.
	}

}