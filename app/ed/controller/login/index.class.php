<?php

class App__Ed__Controller__Login__Index extends Lib__Base__Ed_Controller
{
	public function action_zaloguj()
	{
		//dostęp tylko dla uzytkowników niezalogowanych
		if(App__Ed__Model__Users::is_logged())
		{
			go_back("d|Jesteś już zalogowany.");
		}
		
		if(!empty($_POST))
		{
            //jeżeli jest zaznaczona opcja "zapamiętaj mnie" - ustawiam ciastko na rok
            if(isset($_POST['autologin']) && !empty($_POST['autologin']))
            {
                App__Ed__Model__Users__Autologin::set_cookie();
            }
            
            $ref = !empty($_POST['ref']) ? $_POST['ref'] : NULL;
			$auth = new App__Ed__Model__Users__Auth($_POST['user_name'],$_POST['password'],0,$ref);
            $auth->login();
		}
	}
	
	public function action_przypomnij_haslo()
	{
		//dostęp tylko dla uzytkowników niezalogowanych
		if(App__Ed__Model__Users::is_logged())
		{
			show_404();
		}
		
		
		if(!empty($_POST) && !empty($_POST['email']))
		{
			$email = $_POST['email'];
			
			//dla pewności filtruje wprowadzony adres e-mail
			if(!is_email($email))
			{
				go_back("d|Proszę wprowadzić poprawny adres e-mail");
			}
			
			//odzyskuję hasło
			$remember_password = new App__Ed__Model__Users__Remember_Password($email);
			$remember_password->execute();
			
			//przekierowuje na poprzednią stronę
			go_back("g|Na podany adres e-mail została wysłana wiadomość z danymi do logowania.");
		}
		else
		{
			show_404();
		}
	}
	
	public function action_wyloguj()
	{
		//dostęp tylko dla uzytkowników zalogowanych
		if(!App__Ed__Model__Users::is_logged())
		{
			show_404();
		}
		
		//usuwam sesje
		Lib__Session::remove();
        
        //jeżeli jest ciasteczko autologowanie - usuwam
        if(Lib__cookie::get(COOKIENAME_AUTOLOGIN))
        {
            Lib__cookie::remove(COOKIENAME_AUTOLOGIN);
        }
		
		//komunikat o wylogowaniu
		go_back('g|Zostałeś poprawnie wylogowany','/');
	}

    public function action_zmien_haslo()
    {
        if(!empty($_POST) && !empty($_POST['password']))
        {
            $password_1 = reset($_POST['password']);
            $password_2 = end($_POST['password']);

            if(empty($password_1) && empty($password_2))
            {
                go_back("d|Proszę uzupełnić wszystkie pola formularza");
            }

            $password_rodo = new App__Ed__Model__Users__Password_Rodo(
                App__Ed__Model__Users::find(Lib__Session::get('id'))
            );

            if($password_rodo->change($password_1, $password_2))
            {
                if(is_ed())
                {
                    $back_url = BASE_ED_URL . "dashboard";
                }
                else
                {
                    $back_url = BASE_URL;
                }

                go_back("g|Dziękujemy. Twoje hasło zostało zmienione.", $back_url);
            }

        }

        $this->common_file = 'common_login.tpl';
        $this->template = "_partials/zmien_haslo.tpl";
        
    }
}