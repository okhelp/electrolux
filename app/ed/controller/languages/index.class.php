<?php

class App__Ed__Controller__Languages__Index extends Lib__Base__Ed_Controller
{

	public function action_lista()
	{
		//pobieram listę dostępnych języków
		$list = App__Ed__Model__Language::find('all');
		
		//ładuję zmienne do szablonu
		$this->view->assign('list',$list);
		
		//ładuję szablon
		$this->template = 'languages/lista.tpl';
		$this->page_title = 'Lista dostęnych języków';
		$this->breadcrumb = array(
			'' => 'Języki',
			'languages/lista.html' => 'Lista dostępnych języków'
		);
	}
	
	public function action_ed()
	{
		$id = !empty($_GET) && !empty($_GET['id']) ? (int)$_GET['id'] : 0;
		
		//tworzę obiekt
		$language = !empty($id) ? App__Ed__Model__Language::find($id) : new App__Ed__Model__Language;
		
		//robię zapis
		if(!empty($_POST))
		{
			foreach($_POST as $k => $v)
			{
				$language->{$k} = $v;
			}
			
			if($language->save())
			{
				if(!empty($id)) //edycja
				{
					$msg = "g|".lang("Język został zmieniony.");
				}
				else
				{
					//tworzę strukturę katalgów
					$lang_path = App__Ed__Model__Language__Model::get_folder_path($language->code);
					App__Ed__Model__Language__Model::generate_default_file($lang_path);
					
					$msg = "g|".lang("Język został dodany");
				}
			}
			else
			{
				$msg = "d|".lang("Wystąpił błąd podczas zapisu.");
			}
			
			go_back($msg,BASE_ED_URL.'languages/lista.html');
		}
		
		//ładuję zmienne do szablonu
		$this->view->assign('lang',$language);
		
		//ładuję szablon
		$title = !empty($id) ? 'Edycja języka: ' . $language->name : 'Dodaj nowy język';
		$this->template = 'languages/ed.tpl';
		$this->page_title = $title;
		$this->breadcrumb = array(
			'languages/lista.html' => 'Języki',
			'' => $title
		);
	}
	
	public function action_usun()
	{
		$id = !empty($_GET) && !empty($_GET['id']) ? (int)$_GET['id'] : 0;
		
		if(!empty($id))
		{
			$lang = App__Ed__Model__Language::find($id);
			
			if(!empty($lang))
			{
				
				//usuwam pliki języka
				$file_path = App__Ed__Model__Language__Model::get_folder_path($lang->code);
				remove_dir($file_path);
				
				if($lang->delete())
				{
					$msg = "g|Język został usunięty.";
				}
				else
				{
					$msg = "d|Wystąpił błąd podczas usuwania języka.";
				}
			}
			else
			{
				$msg = "d|Język nie został znaleziony.";
			}
			
			go_back($msg);
			
		}
		else
		{
			go_back("d|".lang("Proszę podać id języka."));
		}
	}
	
	public function action_tlumacz()
	{
		$id = !emptY($_GET) && !empty($_GET['id']) ? (int)$_GET['id'] : 0;
		$file = !empty($_GET) && !empty($_GET['file']) ? $_GET['file'] : NULL;
		
		//robię wypad jeżeli nie podano parametru id
		if(empty($id))
		{
			go_back("d|Proszę podać id języka.");
		}
		
		
		//tworzę obiekt języka
		$lang_data = App__Ed__Model__Language::find($id);
		
		//jeżeli nie ma języka w bazie robię wypad
		if(empty($lang_data))
		{
			go_back("d|Nie znaleziono języka w bazie.");
		}
		$language_folder_path = App__Ed__Model__Language__Model::get_folder_path($lang_data->code);
		
		//zapis
		if(!empty($_POST))
		{
			$file_path = $language_folder_path.$file;
			
			$file_content = "<?php \n";
			
			foreach($_POST as $k => $v)
			{
				if(!empty($v))
				{
					$v = trim($v);
					$v = str_replace('"','\"',$v);
					$file_content .= "$" . "lang['$k'] = \"$v\"; \n";	
				}
			}
			
			$file_content .= "?>";
			
			if(file_put_contents($file_path, $file_content))
			{	
				go_back("g|Tłumaczenie zostało zapisne.");
			}
			else
			{
				go_back("d|Wystąpił błąd podczas zapisu tłumaczenia");
			}
		}
		
		
		//wersja dla wyboru pliku
		if(empty($file))
		{
			//pobieram 
			$files = App__Ed__Model__Language__Model::get_lang_files($lang_data->code,1);
			$this->view->assign('files',$files);
			$mode = "select_file";
		}
		else //wersja dla tłumaczenia konkretnego pliku 
		{
			//załączam plik z tłumaczeniem
			require $language_folder_path . $file;
			
			if(!isset($lang) && empty($lang))
			{
				$lang = array();
			}
			
			
			//jeżeli wybrany plik to main.php, to aktualizuję dane
			if($file == 'main.php')
			{
				$new_words = App__Ed__Model__Language__Model::get_all_new_lang_strings($lang_data->code);
				if(!empty($new_words))
				{
					$lang = array_merge($lang,$new_words);
				}
			}
			
			$this->view->assign('lang',$lang);
			$mode = "translate_file";
		}
		
		//ładuję zmienne do szablonu
		$this->view->assign('mode',$mode);
		
		//ładuję szablon
		$title = 'Tłumaczenie języka: ' . $lang_data->name;
		$this->template = 'languages/tlumacz.tpl';
		$this->page_title = $title;
		
		$this->breadcrumb['languages/lista.html'] = 'Języki';
		if(!empty($id))
		{
			$this->breadcrumb["languages/tlumacz.html?id=$id"] = $title;
		}
		if(!empty($file))
		{
			$this->breadcrumb[''] = "Plik: $file";
		}
		
	}

}
