<?php

use App\modules\order\ed\collections\Get_order_statuses;
use App\modules\order\ed\objects\Add_status;
use App\modules\order\ed\objects\Edit_status;
use App\modules\order\ed\objects\Remove_status;
use App\modules\order\front\objects\Shortcodes;

class App__Ed__Controller__Ustawienia__Order_status extends Lib__Base__Ed_Controller
{
    public function action_add_new_status()
    {
        if (!empty($_POST['name']))
        {
            $add_status = new Add_status($this->session['id_lang'], $this->session['id_service'], $_POST);

            go_back($add_status->add() ? 'g|Status został zapisany.' : 'w|Status nie został zapisany.', BASE_ED_URL . 'ustawienia/zamowienia.html');
        }
        else
        {
            $smarty = new Smarty();

            echo $smarty->fetch('ustawienia/_partials/add_status.tpl');
        }
    }

    public function action_edit_status()
    {
        if(!empty($_POST['id_status']) && !empty($_POST['name']))
        {
            $edit_order = new Edit_status((int)$_POST['id_status'], $_POST);

            go_back($edit_order->edit() ? 'g|Dane zostały zapisane.' : 'w|Dane nie zostały zapisane.', BASE_ED_URL . 'ustawienia/zamowienia.html');
        }
        else
        {
            if(empty($_GET['id']))
            {
                go_back('w|Brak dostępu.', BASE_ED_URL . 'ustawienia/zamowienia.html');
            }
            else
            {
                $status = Get_order_statuses::get_by_status_id($_GET['id']);

                $smarty = new Smarty();
                $smarty->assign('status', $status);
                $smarty->assign('shortcodes', Shortcodes::shortcode_list());

                echo $smarty->fetch('ustawienia/_partials/edit_status.tpl');
            }
        }
    }

    public function action_remove_status()
    {
        if(!empty($_GET['id']))
        {
            $remove_status = new Remove_status((int)$_GET['id']);

            go_back($remove_status->remove() ? 'g|Status został usunięty.' : 'w|Status nie został usunięty.', BASE_ED_URL . 'ustawienia/zamowienia.html');
        }
        else
        {
            go_back('d|Brak dostępu.');
        }
    }
}
