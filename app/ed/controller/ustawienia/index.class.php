<?php

use App\modules\order\ed\collections\Get_order_statuses;

class App__Ed__Controller__Ustawienia__Index extends Lib__Base__Ed_Controller
{

	public function action_glowne()
	{
		$settings = App__Ed__Model__Settings__Model::get('glowne')->data;

		if (!empty($_POST))
		{
			$data = $_POST;

			//jeżeli mamy upload logo to wgrywam zdjęcie
			if (!empty($_FILES['site_logo']['name']))
			{
				$upload = new App__Ed__Model__Upload('images');
				$data['site_logo'] = $upload->send_file($_FILES['site_logo'], 1);
			}
			else
			{
				$data['site_logo'] = $settings->site_logo;
			}

			//jeżeli mamy upload logotypu na socialmedia
			if(!empty($data['upload_2']))
			{
				$data['site_social_logo'] = trim($data['upload_2']);
				unset($data['upload_2']);
			}
			else
			{
				$data['site_social_logo'] = $settings->site_social_logo;
			}
			
			if (App__Ed__Model__Settings__Model::save('glowne', $data))
			{
				go_back('g|'.lang('Ustawienia zostały pomyślnie zapisane.'));
			}
			else
			{
				go_back('d|'.lang('Wystąpił błąd podczas zapisywania ustawień.'));
			}
		}
		
		//przekazuję zmienne do szablonu
		$this->view->assign('settings', $settings);

		//ustawiam szablon
		$this->page_title = "Ustawienia główne";
		$this->template = 'ustawienia/glowne.tpl';
		$this->breadcrumb = array(
			'' => 'Ustawienia',
			'ustawienia/glowne.html' => 'Główne'
		);
	}
	
	public function action_email()
	{
		//zapisuję ustawienia
		if(!empty($_POST))
		{
			if(App__Ed__Model__Settings__Model::save('email', $_POST))
			{
				go_back('g|'.lang('Ustawienia zostały pomyślnie zapisane.'));
			}
			else
			{
				go_back('d|'.lang('Wystąpił podczas zapisywania ustawień.'));
			}
		}
		
		//pobieram zmienne do szablonu
		$this->view->assign('settings', App__Ed__Model__Settings__Model::get('email')->data);
		
		//ustawiam szablon
		$this->page_title = "Ustawienia poczty e-mail";
		$this->template = 'ustawienia/email.tpl';
		$this->breadcrumb = array(
			'' => 'Ustawienia',
			'ustawienia/email.html' => 'Poczta e-mail'
		);
	}
	
	public function action_wyglad()
	{
		//pobieram listę templatek
		$template_list = Ed__Template::get_list();
		
		//zmiana statusu
		if(!empty($_GET) && !empty($_GET['set_status']) && !empty($_GET['template']))
		{
			$new_status = (int)$_GET['set_status'];
			$template_dir = (int)$_GET['template'];
			
			//jeżeli jest tylko jeden szablon to robię wypad
			if(count($template_list) == 1)
			{
				go_back("d|".
					lang("Nie można zmienić statusu, ponieważ dostępny jest tylko jeden szablon"));
			}
			
			//ustawiam domyślną templatkę
			Lib__Session::set('template', $template_dir);
			
			go_back("g|".lang("Szablon został ustawiony jako domyślny."));
		}
		
		
		//ładuję zmienne do szablonu
		$this->view->assign('list', $template_list);
		
		
		//ustawiam szablon
		$this->page_title = "Ustawienia wyglądu";
		$this->template = 'ustawienia/wyglad.tpl';
		$this->breadcrumb = array(
			'' => 'Ustawienia',
			'ustawienia/wyglad.html' => 'Ustawienia wyglądu'
		);
	}
	
	public function action_kody_sledzace()
	{
		//zapisuję ustawienia
		if(!empty($_POST))
		{
			if(App__Ed__Model__Settings__Model::save('kody_sledzace', $_POST))
			{
				go_back('g|'.lang('Ustawienia zostały pomyślnie zapisane.'));
			}
			else
			{
				go_back('d|'.lang('Wystąpił podczas zapisywania ustawień.'));
			}
		}
		
		$settings =  App__Ed__Model__Settings__Model::get('kody_sledzace');
		
		//pobieram zmienne do szablonu
		$this->view->assign('settings',!empty($settings) ? $settings->data : NULL);
		
		//ustawiam szablon
		$this->page_title = "Kody śledzące";
		$this->template = 'ustawienia/kody_sledzace.tpl';
		$this->breadcrumb = array(
			'' => 'Ustawienia',
			'ustawienia/kody_sledzace.html' => 'Kody śledzące'
		);
	}
	
	public function action_optymalizacja()
	{
		//zapisuję ustawienia
		if(!empty($_POST))
		{
			if(App__Ed__Model__Settings__Model::save('optymalizacja', $_POST))
			{
				go_back('g|'.lang('Ustawienia zostały pomyślnie zapisane.'));
			}
			else
			{
				go_back('d|'.lang('Wystąpił podczas zapisywania ustawień.'));
			}
		}
		
		$settings = App__Ed__Model__Settings__Model::get('optymalizacja');
		
		//pobieram zmienne do szablonu
		$this->view->assign('settings',!empty($settings) ? $settings->data : NULL);
		
		//ustawiam szablon
		$this->page_title = "Optymalizacja";
		$this->template = 'ustawienia/optymalizacja.tpl';
		$this->breadcrumb = array(
			'' => 'Ustawienia',
			'ustawienia/optymalizacja.html' => 'Optymalizacja'
		);
	}
	
	public function action_social_media()
	{
		//zapisuję ustawienia
		if(!empty($_POST))
		{
			if(App__Ed__Model__Settings__Model::save('social_media', $_POST))
			{
				go_back('g|'.lang('Ustawienia zostały pomyślnie zapisane.'));
			}
			else
			{
				go_back('d|'.lang('Wystąpił podczas zapisywania ustawień.'));
			}
		}
		
		$settings = App__Ed__Model__Settings__Model::get('social_media');
		
		//pobieram zmienne do szablonu
		$this->view->assign('settings',!empty($settings) ? $settings->data : NULL);
		
		//ustawiam szablon
		$this->page_title = "Social media";
		$this->template = 'ustawienia/social_media.tpl';
		$this->breadcrumb = array(
			'' => 'Ustawienia',
			'ustawienia/social_media.html' => 'Social media'
		);
	}

    public function action_zamowienia()
    {
        $order_statuses = Get_order_statuses::get_all($this->_id_service, $this->session['id_lang']);

        $this->view->assign('order_statuses', $order_statuses);

        $this->page_title = "Zamówienia";
        $this->template = 'ustawienia/zamowienia.tpl';
        $this->breadcrumb = [
            ''                           => 'Ustawienia',
            'ustawienia/zamowienia.html' => 'Zamówienia',
        ];
    }
}
