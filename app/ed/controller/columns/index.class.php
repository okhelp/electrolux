<?php

class App__Ed__Controller__Columns__Index extends Lib__Base__Ed_Controller
{
	public function action_lista()
	{
		//pobieram listę wszystkich zdefiniowanych kolumn
		$list = App__Ed__Model__Columns::find(App__Ed__Model__Columns::order('name ASC'));
		
		//ładuję zmienne do szablonu
		$this->view->assign('list',$list);
		
		//ładuję szablon
		$this->template = 'columns/lista.tpl';
        $this->page_title = 'Lista kolumn';
		$this->breadcrumb = array(
			'' => 'Kolumny',
			'columns/lista.html' => 'Lista kolumn'
		);
	}
	
	public function action_ed()
	{
		//pobieram id
		$id = !empty($_GET) && !empty($_GET['id']) ? (int)$_GET['id'] : 0;
		
		//tworzę obiekt
		$column = !empty($id) ? App__Ed__Model__Columns::find($id) : new App__Ed__Model__Columns;
		
		//zapis
		if(!empty($_POST))
		{
			if(!$id)
			{
				$column->name = $_POST['name'];
			}
			
			$column->description = $_POST['description'];
			$column->status = (int)$_POST['status'];
			
			if($column->save())
			{
				$back_url = BASE_ED_URL . 'columns/lista.html';
				if(empty($id))
				{
					go_back('g|'.lang('Kolumna została dodana.'),$back_url);
				}
				else
				{
					go_back('g|'.lang('Kolumna została zmieniona.'),$back_url);
				}
			}
			else 
			{
				go_back('d|'.lang('Wystąpił błąd podczas zapisu.'),$back_url);
			}
		}
		
		//ładuję zmienne do szablonu
		$this->view->assign('id',$id);
		$this->view->assign('column',$column);
		$this->view->assign('shortcuts',array_keys(App__Ed__Model__Columns__Model::shortcuts()));
		
		//ładuję szablon
		$title = !empty($id) ? "Edycja kolumny: {$column->name}" : "Dodawanie kolumny";
		$this->template = 'columns/ed.tpl';
        $this->page_title = $title;
		$this->breadcrumb = array(
			'columns/lista.html' => 'Kolumny',
			'' => $title
		);
	}
	
	public function action_usun()
	{
		$id_column = !empty($_GET) && !empty($_GET['id']) ? (int)$_GET['id'] : 0;
		
		if(!empty($id_column))
		{
			$column = App__Ed__Model__Columns::find($id_column);
			
			if(empty($column))
			{
				go_back("d|".lang("Kolumna nie została znaleziona."));
			}
			else
			{
				if($column->delete())
				{
					go_back("g|".lang("Kolumna została usunięta."),
						BASE_ED_URL.'columns/lista.html');
				}
				else
				{
					go_back("d|". lang("Wystąpił błąd podczas usuwania kolumny.")
						,BASE_ED_URL.'columns/lista.html');
				}
			}
		}
		else
		{
			go_back("d|".lang("Proszę podać id id kolumny."));
		}
	}
	
	public function action_ajax_sprawdz_dostepnosc()
	{
		if(!empty($_POST) && !empty($_POST['name']))
		{
			$column = App__Ed__Model__Columns::find(App__Ed__Model__Columns::where("name = '{$_POST['name']}'"));
			if(!empty($column))
			{
				echo 'yes';
			}
			else
			{
				echo 'no';
			}
		}
		else
		{
			echo 'error';
		}
	}
	
	public function action_ajax_zapisz()
	{
		if(!empty($_POST) && !empty($_POST['sekcja']) && !empty($_POST['val']))
		{
			$save = App__Ed__Model__Columns__Model::save($_POST['sekcja'],$_POST['val']);
			
			if($save)
			{
				echo 'Kolumna została zmieniona.';
			}
			else
			{
				echo 'Wystąpił błąd podczas zapisu kolumny';
			}
		}
	}
}