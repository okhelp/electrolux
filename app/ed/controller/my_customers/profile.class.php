<?php

use App\ed\model\company\Get_by_user_id;
use App\ed\model\company\Get_company_adresses;
use App\ed\model\company\Get_company_supervisor;
use App\ed\model\company\Get_company_users;
use App\ed\model\points\Get_users_points;
use App\ed\model\sale\Get_sale_history;
use App\ed\model\Sale_model;
use App\modules\order\front\collections\Get_user_prizes_earned;
use App\modules\products\front\collections\Get_products_by_id;
use App\front\models\points\Get_points_log;

class App__Ed__Controller__My_Customers__Profile extends Lib__Base__Ed_Controller
{
    public function action_zobacz()
    {
        if(!empty($_GET) && !empty($_GET['id']))
        {
            $id_user = (int)$_GET['id'];

            //pobieram obiekt uzytkownika
            $company = Get_by_user_id::get($id_user);
            if(empty($company))
            {
                go_back("d|Podany użytkownik nie istnieje");
            }

            $adresses = Get_company_adresses::get($company->id);
            $company_users = Get_company_users::get($company->id);
            $user_data = App__Ed__Model__Users::find($id_user);

            foreach ($adresses as $index => $adress)
            {
                if(empty($adress->public) && $adress->id_user != Lib__Session::get('id'))
                {
                    unset($adresses[$index]);
                }
            }

            $company_users_ids = [];
            if (!empty($company_users))
            {
                $company_users_ids = array_map(
                    function($company_user) {
                        return $company_user->id_user;
                    }, $company_users);
            }
            $users_e_lux = [];
            foreach (array_merge($company_users_ids, [$id_user]) as $userID) {
                $users_e_lux[$userID] = App__Ed__Model__Points_log::getPoints($userID);
            }
            $annual_eluxes = Get_users_points::get_annual([$id_user]);
            $annual_eluxes = !empty($annual_eluxes) ? reset($annual_eluxes) : 0;

            //historia zamówień
            $order_history = Get_sale_history::get([$id_user]);

            $products = [];

            if (!empty($order_history))
            {
                $products_ids = array_map(
                    function($order) {
                        return $order->{Sale_model::COLUMN_ID_PRODUCT};
                    }, $order_history);

                $products = Get_products_by_id::get($products_ids);
            }

            //nagrody
            $user_prizes = Get_user_prizes_earned::get($id_user);

            //handlowiec
            $supervisor = Get_company_supervisor::get((int)$company->id);

            $this->view->assign('id_user', $id_user);
            $this->view->assign('company', $company);
            $this->view->assign('addresses', $adresses);
            $this->view->assign('company_users', $company_users);
            $this->view->assign('users_e_lux', $users_e_lux);
            $this->view->assign('annual_eluxes', $annual_eluxes);
            $this->view->assign('user_data', $user_data);
            $this->view->assign('user_spend_e_lux', App__Ed__Model__Points_log::getSpentPoints($id_user));
            $this->view->assign('order_history', $order_history);
            $this->view->assign('user_prizes', $user_prizes);
            $this->view->assign('supervisor', $supervisor);
            $this->view->assign('points_log', Get_points_log::get_by_user($id_user));

            $this->template = 'my_customers/profile/zobacz.tpl';
            $this->page_title = "Profil użytkownika: " . $user_data->name . " " . $user_data->surname;
            $this->breadcrumb = [
                'my_customers' => 'Moi klienci',
                '' => $this->page_title
            ];

        }
        else
        {
            go_back("d|Proszę podać id użytkownika");
        }
    }
}