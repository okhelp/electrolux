<?php

use App\ed\model\points\Get_users_points;
use App\ed\model\my_customers\Get_my_customers_table;

class App__Ed__Controller__My_customers__Index extends Lib__Base__Ed_Controller
{
    public function action_index()
    {
//        $tree = Get_my_customers_tree::run((int)$this->session['id']);

//        $this->view->assign('tree', empty($tree) ? '' : json_encode($tree, JSON_UNESCAPED_UNICODE));

        $this->template = 'my_customers/index.tpl';
        $this->page_title = "Moi klienci";
        $this->breadcrumb = [
            '' => 'Firmy podrzędne',
            'my_customers' => 'Moi klienci'
        ];
    }

    public function action_get_customers_list()
    {
        $usersID = Get_my_customers_table::run((int)$this->session['id']);
        $table = 'company_users';
        $primaryKey = "id";
        $columns = [
            ['db' => 'id_user', 'dt' => 0],
            [
                'db'        => 'name',
                'dt'        => 1,
                'formatter' => function($d, $row) {
                    return $d;
                },
            ],
            [
                'db'        => 'surname',
                'dt'        => 2,
                'formatter' => function($d, $row) {
                    return $d;
                },
            ],
            [
                'db'        => 'id_company',
                'dt'        => 3,
                'formatter' => function($d, $row) {
                    $companies = App__Ed__Model__Company::get_by_ids([$d]);
                    $companies = !empty($companies) ? $companies[$d]->name : '-';
                    return $companies;
                },
            ],
            [
                'db'        => 'points',
                'dt'        => 4,
                'formatter' => function($d, $row) {
                    return $d ?? 0;
                },
            ],
            [
                'db'        => 'id_user',
                'dt'        => 5,
                'formatter' => function($d, $row) {
                    return App__Ed__Model__Points_log::getSpentPoints($d);
                },
            ],
            [
                'db'        => 'extra_points',
                'dt'        => 6,
                'formatter' => function($d, $row) {
                    return $d ?? 0;
                },
            ],
            [
                'db'        => 'id_user',
                'dt'        => 7,
                'formatter' => function($d, $row) {
                    $annual_eluxes = Get_users_points::get_annual([$d]);
                    return !empty($annual_eluxes) ? reset($annual_eluxes) : 0;
                },
            ],
            [
                'db'        => 'ts',
                'dt'        => 8,
                'formatter' => function($d, $row) {
                    return !empty($d) ? date('Y-m-d H:i:s', strtotime($d)) : '';
                },
            ],
            [
                'db'        => 'date_modyf',
                'dt'        => 9,
                'formatter' => function($d, $row) {
                    return !empty($d) ? date('Y-m-d H:i:s', strtotime($d)) : '';
                },
            ],
            [
                'db'        => 'id_user',
                'dt'        => 10,
                'formatter' => function($d, $row) {
                    return '<a href="' . BASE_ED_URL . 'my_customers/profile/zobacz.html?id=' . $d . '" class="btn btn-info ">zobacz</a>';
                },
            ],
        ];

        $where = " id_user IN (" . implode(',', $usersID) . ") ";
        $join = " JOIN users ON id_user = users.id LEFT JOIN company_users_points ON id_user=company_user_id ";

        echo json_encode(
            SSP::complex(
                $_POST,
                $table,
                $primaryKey,
                $columns,
                $join,
                null,
                $where)
        );
    }
}