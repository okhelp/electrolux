<?php

use App\ed\model\points\Get_subordinates_users_points;
use App\front\models\user\User_extra_points;

class App__Ed__Controller__Company_supervising__Assign_extra_points extends Lib__Base__Ed_Controller
{
    public function action_index()
    {
        $subordinates_points = Get_subordinates_users_points::get($this->_user->id);

        $this->view->assign('subordinates_points', $subordinates_points);
        $this->template = 'company_supervising/subordinates_points_list.tpl';
    }

    public function action_assign()
    {
        $message = 'd|Brak dostępu.';

        if (!empty($_POST['extra_points']) && !empty($_POST['subordinate_id']) && !empty($this->_user->id))
        {
            $message = User_extra_points::add((int)$_POST['subordinate_id'], (int)$_POST['extra_points']) ? 'g|Punkty zostały przypisane.' : 'd|Punkty nie zostały przypisane.';
        }

        go_back($message, BASE_ED_URL . 'company_supervising/assign_extra_points');
    }

}