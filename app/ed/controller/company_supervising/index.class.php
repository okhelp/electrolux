<?php

use ActiveRecord\ConnectionManager;
use App\ed\model\company\Get_company_users;
use App\ed\model\customers\Co_workers_confirmation_status;
use App\ed\model\mail\Send_email_by_queue;
use App\ed\model\sale\Sale_log;
use App\ed\model\Sale_model;
use App\ed\model\users\Confirmed_co_workers;
use App\ed\model\users\Get_subordinate_companies;
use App\front\models\user\User_points;

class App__Ed__Controller__Company_supervising__Index extends Lib__Base__Ed_Controller
{
    public function __construct()
    {
        parent::__construct();

        if (!App__Ed__Model__Acl::has_access('subordinate_companies', 0))
        {
            go_back('w|Brak dostępu.');
            die();
        }
    }

    public function action_index()
    {
        $this->view->assign('is_admin', App__Ed__Model__Acl::has_group('admin-electrolux', 0));

        $this->template = 'company_supervising/sale_in_verification.tpl';
    }

    public function action_get_subordinates_sale()
    {
        $subordinates_ids = Get_subordinate_companies::get([$this->_user->id]);
        $subordinates_companies_users_ids = Get_company_users::get_by_ids($subordinates_ids);

        $users_ids = [];

        foreach ($subordinates_companies_users_ids as $subordinate_company)
        {
            foreach ($subordinate_company as $subordinate_company_user)
            {
                $users_ids[] = $subordinate_company_user->id_user;
            }
        }



        $table = 'sale_history';
        $primaryKey = 'id';
        $columns = [
            ['db' => 'customer_name', 'dt' => 0],
            ['db' => 'invoice_number', 'dt' => 1],
            ['db' => 'model', 'dt' => 2],
            ['db' => 'price', 'dt' => 3],
            ['db' => 'count', 'dt' => 4],
            ['db' => 'price_sum', 'dt' => 5],
            ['db' => 'points', 'dt' => 6],
            [
                'db'        => 'created_at',
                'dt'        => 7,
                'formatter' => function($d, $row) {
                    return date('Y-m-d H:i:s', strtotime($d));
                },
            ],
            [
                'db'        => 'status',
                'dt'        => 8,
                'formatter' => function($d, $row) {
                    return \App\ed\model\sale\Register_sale::STATUSES[$d];
                },
            ],
            [
                'db'        => 'id',
                'dt'        => 9,
                'formatter' => function($d, $row) {
                    $status_condition = isset($_POST['status']) ? (int)$_POST['status'] : 0;
                    
                    if($status_condition == 0)
                    {
                            return '
                                <a href="' . BASE_ED_URL . 'company_supervising/confirm_sale.html?id=' . $d . '"
                                    class="btn btn-info confirm"
                                    data-confirm-text="Czy na pewno chcesz potwierdzić?">zatwierdź</a>
                                <a href="' . BASE_ED_URL . 'company_supervising/reject_sale.html?id=' . $d . '"
                                    class="btn btn-danger confirm"
                                    data-confirm-text="Czy na pewno chcesz odrzucić?">odrzuć</a>
                            ';

                    }
                    else
                    {
                        return '-';
                    }
                },
            ],
        ];

        $customer_condition = empty($_POST['customer']) ? '' : ' AND customer_name LIKE "%' . $_POST['customer'] . '%" ';

        $date_from_condition = empty($_POST['date_from']) ? '' : ' AND created_at > "' . $_POST['date_from'] . '"';

        $date_to_condition = empty($_POST['date_to']) ? '' : ' AND created_at < "' . $_POST['date_to'] . '"';

        $administrator_condition = empty($_POST['administrator']) ? '' : ' AND supervisor_name LIKE "%' . $_POST['administrator'] . '%" ';

        $subordinates_condition = (App__Ed__Model__Acl::has_group('admin-electrolux', 0) || App__Ed__Model__Acl::has_group('premium-user', 0)) ?
            '' : ("id_customer IN (" . implode(',', $users_ids) . ") AND");

        $invoice_number = !empty($_POST['invoice_number']) ? "%{$_POST['invoice_number']}%" : "";

        $status_condition = isset($_POST['status']) ? (int)$_POST['status'] : 0;


        $where = "$subordinates_condition status=$status_condition $customer_condition $date_from_condition $date_to_condition $administrator_condition $invoice_number";

        echo json_encode(
            SSP::complex(
                $_POST,
                $table,
                $primaryKey,
                $columns,
                null,
                null,
                $where)
        );
    }

    public function action_confirm_sale()
    {
        $message = 'd|Brak dostępu';

        if (!empty($_GET['id']))
        {
            $sale = Sale_model::find($_GET['id']);
            $sale->{Sale_model::COLUMN_STATUS} = Sale_model::STATUS_CONFIRMED;

            $connection = ConnectionManager::get_connection();
            $connection->transaction();

            $transaction_completed = $sale->save() && User_points::add($sale->{Sale_model::COLUMN_ID_CUSTOMER}, (int)$sale->{Sale_model::COLUMN_POINTS});

            if ($transaction_completed)
            {
                $log = new Sale_log($sale->{Sale_model::COLUMN_ID_CUSTOMER}, $sale);
                $log->add();

                $connection->commit();

                $email_receipent = App__Ed__Model__Users::find($sale->{Sale_model::COLUMN_ID_CUSTOMER});

                $sale_registration_date = $sale->{Sale_model::COLUMN_CREATED_AT}->format('d-m-Y');

                $sale_confirm_date = (new DateTime('now'))->format('d-m-Y');

                $sale_points = $sale->{Sale_model::COLUMN_POINTS};

                Send_email_by_queue::send(
                    (string)$email_receipent->email,
                    'Potwierdzenie sprzedaży - Electrolux od kuchni',
                    'Potwierdzenie sprzedaży',
                    "Twoja sprzedaż została potwierdzona. <br>
W dniu {$sale_registration_date} zarejestrowałeś swoją sprzedaż. <br>
W dniu {$sale_confirm_date} została ona potwierdzona. <br>
Ilość przyznanych e-luxów za zamówienie to {$sale_points}.<br>
W razie jakikolwiek wątpliwości zapraszamy do kontaktu z swoim handlowcem."
                );
            }
            else
            {
                $connection->rollback();
            }

            $message = $transaction_completed ? 'g|Potwierdzono sprzedaż.' : 'd|Nie udało się potwierdzić sprzedaży.';
        }

        go_back($message);
    }

    public function action_reject_sale()
    {
        $message = 'd|Brak dostępu';

        if (!empty($_GET['id']))
        {
            $sale = Sale_model::find($_GET['id']);
            $sale->{Sale_model::COLUMN_STATUS} = Sale_model::STATUS_REJECTED;

            $sale_rejected = $sale->save();

            if ($sale_rejected)
            {
                $email_receipent = App__Ed__Model__Users::find($sale->{Sale_model::COLUMN_ID_CUSTOMER});

                $sale_registration_date = $sale->{Sale_model::COLUMN_CREATED_AT}->format('d-m-Y');

                $sale_confirm_date = (new DateTime('now'))->format('d-m-Y');

                Send_email_by_queue::send(
                    (string)$email_receipent->email,
                    'Odrzucenie sprzedaży - Electrolux od kuchni',
                    'Odrzucenie sprzedaży',
                    "Twoja sprzedaż została odrzucona.<br>
W dniu {$sale_registration_date} zarejestrowałeś swoją sprzedaż. <br>
W dniu {$sale_confirm_date} została ona odrzucona. <br>
W razie jakikolwiek wątpliwości zapraszamy do kontaktu z swoim handlowcem."
                );
            }

            $message = $sale_rejected ? 'g|Odrzucono sprzedaż.' : 'd|Nie udało się odrzucić sprzedaży.';
        }

        go_back($message);
    }

    public function action_co_workers_list()
    {
        $this->template = 'company_supervising/co_workers_list.tpl';
        $this->page_title = "Potwierdzanie współpracowników";
        $this->breadcrumb = [
            '' => 'Firmy podrzędne',
            'company_supervising/co_workers_list.html' => 'Potwierdzanie współpracowników'
        ];
    }

    public function action_get_co_workers_list()
    {
        $id_user = (int)$this->session['id'];
        $subordinate_companies = Get_subordinate_companies::get([$id_user]);

        $table = 'co_workers_confirmation_status';
        $primaryKey = 'id';
        $columns = [
            ['db' => 'co_worker_name', 'dt' => 0],
            ['db' => 'company_name', 'dt' => 1],
            [
                'db'        => 'status',
                'dt'        => 2,
                'formatter' => function($d, $row) {
                    switch ($d)
                    {
                        case Confirmed_co_workers::STATUS_CONFIRMED:
                            return 'potwierdzony';
                            break;
                        case Confirmed_co_workers::STATUS_REJECTED:
                            return 'odrzucony';
                            break;
                        case Confirmed_co_workers::STATUS_NOT_CONFIRMED:
                            return 'nie potwierdzony';
                            break;
                    }
                },
            ],
            [
                'db'        => 'created_at',
                'dt'        => 3,
                'formatter' => function($d, $row) {
                    return date('Y-m-d H:i:s', strtotime($d));
                },
            ],
            [
                'db'        => 'updated_at',
                'dt'        => 4,
                'formatter' => function($d, $row) {
                    return empty($d) ? '' : date('Y-m-d H:i:s', strtotime($d));
                },
            ],
            [
                'db'        => 'id',
                'dt'        => 5,
                'formatter' => function($d, $row) {
                    $accept_button = '<a href="' . BASE_ED_URL . 'company_supervising/accept_co_worker.html?id=' . $d . '"
                                        class="btn btn-info confirm"
                                        data-confirm-text="Czy na pewno chcesz potwierdzić?">zatwierdź</a>';
                    $reject_button = '<a href="' . BASE_ED_URL . 'company_supervising/reject_co_worker.html?id=' . $d . '"
                                       class="btn btn-danger confirm"
                                       data-confirm-text="Czy na pewno chcesz odrzucić?">odrzuć</a>';

                    switch ($row[Co_workers_confirmation_status::COLUMN_STATUS])
                    {
                        case Confirmed_co_workers::STATUS_CONFIRMED:
                            return $reject_button;
                            break;
                        case Confirmed_co_workers::STATUS_REJECTED:
                            return $accept_button;
                            break;
                        case Confirmed_co_workers::STATUS_NOT_CONFIRMED:
                            return $accept_button . $reject_button;
                            break;
                    }
                },
            ],
        ];

        $where = "id_company IN (" . implode(',', $subordinate_companies) . ")";

        echo json_encode(
            SSP::complex(
                $_POST,
                $table,
                $primaryKey,
                $columns,
                null,
                null,
                $where)
        );
    }

    public function action_accept_co_worker()
    {
        $message = 'w|Brak dostępu.';

        if(!empty($_GET['id']))
        {
            $id_co_worker_confirmation = (int)$_GET['id'];
            $id_user = (int)$this->session['id'];
            $subordinate_companies = Get_subordinate_companies::get([$id_user]);

            $co_worker_confirmation = Confirmed_co_workers::find($id_co_worker_confirmation);

            if(!empty($co_worker_confirmation) && in_array($co_worker_confirmation->{Confirmed_co_workers::COLUMN_ID_COMPANY}, $subordinate_companies))
            {
                $co_worker_confirmation->{Confirmed_co_workers::COLUMN_STATUS} = Confirmed_co_workers::STATUS_CONFIRMED;
                $co_worker_confirmation->{Confirmed_co_workers::COLUMN_CONFIRMED_BY} = $id_user;

                $data_saved = $co_worker_confirmation->save();

                $user = App__Ed__Model__Users::find($co_worker_confirmation->{Confirmed_co_workers::COLUMN_ID_USER});

                if($data_saved && !empty($user->email))
                {
                    Send_email_by_queue::send(
                        (string)$user->email,
                        'Aktywacja konta - Electrolux od kuchni',
                        "Witaj {$user->name} {$user->surname}",
                        "Mamy dobre wieści. Zweryfikowaliśmy twoje dane i twoje konto zostało w pełni aktywowane.<br>
Teraz możesz już zamawiać nagrody<br>
<br>
<a href='" . BASE_URL . "'>Przejdź do portalu</a>"
                    );
                }

                $message = $data_saved ? 'g|Pomyślnie potwierdzono współpracownika.' : 'w|Nie udało się potwierdzić współpracownika.';
            }
            else
            {
                $message = 'w|Nie odnaleziono lub nie masz dostępu do potwierdzenia współpracownika.';
            }
        }

        go_back($message);
    }

    public function action_reject_co_worker()
    {
        $message = 'w|Brak dostępu.';

        if(!empty($_GET['id']))
        {
            $id_co_worker_confirmation = (int)$_GET['id'];
            $id_user = (int)$this->session['id'];
            $subordinate_companies = Get_subordinate_companies::get([$id_user]);

            $co_worker_confirmation = Confirmed_co_workers::find($id_co_worker_confirmation);

            if(!empty($co_worker_confirmation) && in_array($co_worker_confirmation->{Confirmed_co_workers::COLUMN_ID_COMPANY}, $subordinate_companies))
            {
                $co_worker_confirmation->{Confirmed_co_workers::COLUMN_STATUS} = Confirmed_co_workers::STATUS_REJECTED;
                $co_worker_confirmation->{Confirmed_co_workers::COLUMN_CONFIRMED_BY} = $id_user;

                $data_saved = $co_worker_confirmation->save();

                $message = $data_saved ? 'g|Pomyślnie odrzucono współpracownika.' : 'w|Nie udało się odrzucić współpracownika.';
            }
            else
            {
                $message = 'w|Nie odnaleziono lub nie masz dostępu do odrzucenia współpracownika.';
            }
        }

        go_back($message);
    }
}