<?php

class App__Ed__Controller__Ban__Index extends Lib__Base__Ed_Controller
{
	public function action_lista()
	{
		//pobieram listę wszystkich zdefiniowanych kolumn
		$list = App__Ed__Model__Ban::find(App__Ed__Model__Ban::order('id DESC'));
		
		//ładuję zmienne do szablonu
		$this->view->assign('list',$list);
		
		//ładuję szablon
		$this->template = 'ban/lista.tpl';
        $this->page_title = 'Lista blokad';
		$this->breadcrumb = array(
			'' => 'Blokada dostępu',
			'ban/lista.html' => 'Lista blokad'
		);
	}
	
	public function action_ed()
	{
		//pobieram id
		$id = !empty($_GET) && !empty($_GET['id']) ? (int)$_GET['id'] : 0;
		
		//tworzę obiekt
		$ban = !empty($id) ? App__Ed__Model__Ban::find($id) : new App__Ed__Model__Ban;
		
		//robię zapis
		if(!empty($_POST))
		{
			//lecę po wszystkich polach
			foreach($_POST as $k => $v)
			{
				$ban->{$k} = $v;
			}
			
			//zapisuję i daję odpowiedni komunikat
			if($ban->save())
			{
				if(!empty($id))
				{
					$msg = "g|".lang("Dane zostały zmienione");
				}
				else
				{
					$msg = "d|".lang("Blokada została nałożona");
				}
				
			}
			else
			{
				$msg = "d|".lang("Wytąpił błąd podczas zapisu.");
			}
			
			go_back($msg,BASE_ED_URL.'ban/lista.html');
		}
		
		//ładuję zmienne do szablonu
		$this->view->assign('id',$id);
		$this->view->assign('ban',$ban);
		
		//ładuję szablon
		$title = !empty($id) ? "Edycja blokady" : "Dodawanie blokady";
		$this->template = 'ban/ed.tpl';
        $this->page_title = $title;
		$this->breadcrumb = array(
			'ban/lista.html' => 'Blokada dostępu',
			'' => $title
		);
	}
	
	public function action_usun()
	{
		$id_block = !empty($_GET) && !empty($_GET['id']) ? (int)$_GET['id'] : 0;
		
		if(!empty($id_block))
		{
			$block = App__Ed__Model__Ban::find($id_block);
			
			if(empty($block))
			{
				go_back("d|".lang("Blokada nie została znaleziona."));
			}
			else
			{
				if($block->delete())
				{
					go_back("g|".lang("Blokada została usunięta."),
						BASE_ED_URL.'ban/lista.html');
				}
				else
				{
					go_back("d|". lang("Wystąpił błąd podczas usuwania blokady.")
						,BASE_ED_URL.'ban/lista.html');
				}
			}
		}
		else
		{
			go_back("d|".lang("Proszę podać id id blokady."));
		}
	}
}