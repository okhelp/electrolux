<?php

class App__Ed__Controller__Index extends Lib__Base__Ed_Controller
{
	
	public function action_index() 
	{
        redirect('ed/dashboard');
		$this->template = 'welcome/index.tpl';
		$this->breadcrumb = array(''=>'Dashboard');
	}
	
	public function action_login()
	{
        //jeżeli jest użytkownik zalogowany to przenosze na dashboard
        if(Lib__Session::get('id'))
        {
            go_back('w|Jesteś już zalogowany...',redirect('ed/dashboard'));
        }
        
		$this->template = '_partials/login.tpl';
		$this->common_file = 'common_login.tpl';
		
		//jeżeli nastąpiło przekierowanie z innej strony przekazuję ten adres do późniejszego przekierowania
		$ref = NULL;
		if(!empty($_GET['ref']))
		{
			$ref = $_GET['ref'];
			$ref = urldecode($ref);
			$ref = base64_decode($ref);
		}
        
		$this->view->assign('ref',$ref);
	}

    public function action_change_service()
    {
        if(!empty($_GET) && !empty($_GET['id_service']))
        {
            $id_service = (int)$_GET['id_service'];

            //sprawdzam czy serwis występuje w bazie danych
            $find_service = App__Ed__Model__Service::find($id_service);
            if(!empty($id_service))
            {
                App__Ed__Model__Service::change_service($id_service);
                go_back("g|Serwis został zmieniony.");
            }
            else
            {
                go_back("d|Serwis o podanym id nie jest zdefioniowany.");
            }

        }
    }
}

