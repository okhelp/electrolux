<form method="POST" enctype="multipart/form-data">
	<div class="form-group">
		<label>Wybierz plik z dysku:</label>
		<div class="alert alert-info">
			Proszę wybrać plik .zip, który został sporządzony zgodnie z dokumentacją dostarczoną przez <strong><a href="http://goinweb.pl">goinweb.pl</a></strong>
		</div>
		<input type="file" class="form-control" name="module_file" required="required" />
		<input type="hidden" name="action" value="install"/>
	</div>
	<div class="form-group">
		<button type="submit" class="btn btn-dafault">Zapisz</button>
	</div>
</form>