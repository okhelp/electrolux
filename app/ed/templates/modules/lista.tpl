{load_plugin name="datatable"}

<div class="row">
	<div class="col-md-8">
		<a href="{BASE_ED_URL}modules/instaluj.html" class="btn btn-info">Zainstaluj</a>
	</div>

	<div class="col-md-1 text-right" style="padding-top:8px">
		<strong>Pokaż</strong>:
	</div>
	<div class="col-md-3">
		<select id="change_show_mode" class="form-control">
			<option value="-1"{if !isset($smarty.get.show_mode) || (isset($smarty.get.show_mode) && $smarty.get.show_mode == -1)} selected{/if}>wszystkie</option>
			<option value="1"{if isset($smarty.get.show_mode) && $smarty.get.show_mode == 1} selected{/if}>zainstalowane</option>
			<option value="0"{if isset($smarty.get.show_mode) && $smarty.get.show_mode == 0} selected{/if}>wyłączone</option>
		</select>

	</div>
</div>

<p>
<hr />
</p>

{* lista modułów *}

<table class="datatable">
	<thead>
		<tr>
			<th>Nazwa</th>
			<th>Opis</th>
			<th>Status</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		{foreach from=$modules item=module}
			{assign var=module_info value=$module->get_info()}
			<tr>
				<td>
					<strong>{$module_info.name}</strong>
					<small>
						{if $module_info.version}
							<br /><b>Wersja</b>: {$module_info.version}
						{/if}
						{if $module_info.author}
							<br /><b>Autor</b>: {$module_info.author}
						{/if}
					</small>
				</td>
				<td>
					<small>
						{$module_info.description}
					</small>
				</td>
				<td>
					{if $module->status}
						<span class="label label-success">aktywny</span>
					{else}
						<span class="label label-danger">nieaktywny</span>
					{/if}
				</td>
				<td class="text-right">
					{if !$module->not_remove}
						{if $module->status}
							<a href="{BASE_ED_URL}modules/dezaktywuj.html?id_module={$module->id}" class='btn btn-danger confirm' data-confirm-text="Czy na pewno chcesz dezaktywować ten moduł?">dezaktywuj</a>
						{else}
							<a href="{BASE_ED_URL}modules/aktywuj.html?id_module={$module->id}" class='btn btn-success confirm' data-confirm-text="Czy na pewno chcesz aktywować ten moduł?">aktywuj</a>
						{/if}
						<a href="{BASE_ED_URL}modules/usun.html?id={$module->id}" class='btn btn-info confirm' data-confirm-text="Czy na pewno chcesz usunąć ten moduł?">usuń</a>
					{else}
						<div class="alert alert-info text-center">
							<b>Moduł systemowy.</b><br />
							Brak możlwiości dezaktywacji i usunięcia.
						</div>
					{/if}
				</td>
			</tr>
		{/foreach}
	</tbody>
</table>

<script type="text/javascript">
	{literal}
		$(document).ready(function() 
		{
			$('#change_show_mode').on('change',function(){
				window.location.href = BASE_ED_URL + 'modules/lista.html?show_mode=' + $(this).val();
			})
		})
	{/literal}
</script>
