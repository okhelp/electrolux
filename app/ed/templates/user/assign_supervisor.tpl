{load_plugin name="chosen"}

<form action="" method="POST">
    <div class="form-group">
        <label>Klient:</label>
        <select name="client" required class="required">
            <option value=""></option>
            {foreach $companies as $company}
                <option value="{$company->id}">{$company->name}</option>
            {/foreach}
        </select>
    </div>
    <div class="form-group">
        <label>Opiekun:</label>
        <select name="supervisor" required class="required">
            <option value=""></option>
            {foreach $supervisors as $supervisor}
                <option value="{$supervisor->id}">{$supervisor->name} {$supervisor->surname}</option>
            {/foreach}
        </select>
    </div>
    <button name="save" value="1">Przypisz</button>
</form>
