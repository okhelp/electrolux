<form method="POST" id="f">
	<div class="form-group">
		<label>Login:</label>
		{if !empty($user_data->login)}
			{$user_data->login}
		{else}
			<input type="text" name="login" class="form-control" required="required" class="required" />
		{/if}
	</div>
	<div class="form-group">
		<label>Imię:</label>
		<input type="text" name="name" value="{$user_data->name}" class="form-control" required="required" class="required" />
	</div>
	<div class="form-group">
		<label>Nazwisko:</label>
		<input type="text" name="surname" value="{$user_data->surname}" class="form-control" required="required" class="required" />
	</div>
	<div class="form-group">
		<label>E-mail:</label>
		<input type="email" name="email" value="{$user_data->email}" class="form-control" required="required" class="required" />
	</div>
	<div class="form-group">
		<label>Hasło:</label>
		<div class="alert alert-info">
			Jeżeli chcesz zmienić hasło uzupełnij pole poniżej.
		</div>
		<input type="password" name="password[new]" value="" class="form-control" placeholder="Nowe hasło"/> <br />
		<input type="password" name="password[repeat]" value="" class="form-control" placeholder="Nowe hasło (powtórz)"/>
	</div>
	<div class="form-group">
		<label>Avatar:</label>
		{if $user_data->id_cimg}
			<div id="avatar">
				<div class="row">
					<div class="col-md-12">
						<img src="{img id_cimg=$user_data->id_cimg width=150 height=150}" alt="" />
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<p>
							<a href="#" id="avatar-do-change" class="btn btn-info">Zmień</a>
						</p>
					</div>
				</div>
			</div>
		{/if}
		<div id="avatar-change"{if $user_data->id_cimg} style="display:none"{/if}>
			{include file="_partials/upload/images_form.tpl" name="avatar" file_limit=1 show_thumb=1}
		</div>
	</div>
		
	<div class="form-group">
		<label>Status:</label>
		<select name="status" class="form-control">
			<option value="1"{if empty($user_data) || (!empty($user_data) && $user_data->status == 1)} selected="selected"{/if}>Aktywny</option>
			<option value="0"{if !empty($user_data) && $user_data->status == 0} selected="selected"{/if}>Niekatywny</option>
		</select>
	</div>
	
	<div class="form-group">
		<button type="submit" name="zapisz" value="edytuj" class="btn btn-submit">Zapisz</button>
	</div>
</form>
<script type="text/javascript">
	{if $user_data->id_cimg}
		{literal}
			$(document).ready(function() 
			{
				$('#avatar-do-change').click(function(e){
					$('#avatar').slideUp(function(){
						$('#avatar-change').slideDown();
					})
					e.preventDefault();
				})
			})
		{/literal}
	{/if}
</script>