<form method="POST" action="{BASE_ED_URL}user/acl/zapisz_uzytkownik_grupa.html">
    <div class="form-group">
        <label>Grupa</label>
        <input type="text" id="group_name" class="form-control" value="{if !empty($item->group_name)}{$item->group_name}{/if}" required />
        <input type="hidden" name="id_group" id="id_group" value="{if !empty($item->id_group)}{$item->id_group}{/if}" />
    </div>
    <div class="form-group">
        <label>Pracownik</label>
        <input type="text" id="user_name" class="form-control" value="{if !empty($item->name)}{$item->name} {$item->surname}{/if}" />
        <input type="hidden" name="id_user" id="id_user" value="{if !empty($item->id_user)}{$item->id_user}{/if}" />
    </div>
    <div class="form-group">
        <input type="hidden" name="id" value="{if !empty($item->id_acl)}{$item->id_acl}{else}0{/if}">
        <button type="submit" class="btn btn-primary">Zapisz</button>
    </div>
</form>

<script type="text/javascript">
    {literal}
        $(document).ready(function(){
            $("#group_name").autocomplete({
                source: function (request, response)
                {
                    $.ajax({
                        url: BASE_ED_URL + "user/acl/ajax_pobierz_nazwe_grupy.html",
                        dataType: "json",
                        method: 'POST',
                        data: {
                            term: request.term
                        },
                        success: function (data)
                        {
                            response(data);
                        }
                    });
                },
                minLength: 2,
                select: function (event, ui)
                {
                    $('#id_group').val(ui.item.id);
                }
            });

            $("#user_name").autocomplete({
                source: function (request, response)
                {
                    $.ajax({
                        url: BASE_ED_URL + "user/acl/ajax_pobierz_nazwe_pracownika.html",
                        dataType: "json",
                        method: 'POST',
                        data: {
                            term: request.term
                        },
                        success: function (data)
                        {
                            response(data);
                        }
                    });
                },
                minLength: 2,
                select: function (event, ui)
                {
                    $('#id_user').val(ui.item.id);
                }
            });

        })
    {/literal}

</script>