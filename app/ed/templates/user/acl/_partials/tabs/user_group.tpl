<div class="table-responsive">
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>#id</th>
            <th>Grupa</th>
            <th>Opis grupy</th>
            <th>Pracownik</th>
        </tr>
        </thead>
        <tbody>
        {foreach from=$list item=l}
            <tr>
                <td>{$l->id_acl}</td>
                <td>{$l->group_name}</td>
                <td>{$l->description}</td>
                <td>{$l->name} {$l->surname}</td>
                <td class="text-center">
                    <a href="#" onClick="show_modal_ajax('Edytuj powiązanie', '{BASE_ED_URL}user/acl/ed_user_group.html?id={$l->id_acl}')">[edytuj]</a>
                    <a href="#" data-href="{BASE_ED_URL}user/acl/usun_uzytkownik_grupa.html?id={$l->id_acl}" onclick="delete_item($(this), 'Czy na pewno chcesz usunąć to powiązanie?');">[usuń]</a>
                </td>
            </tr>
        {/foreach}
        </tbody>
    </table>
</div>