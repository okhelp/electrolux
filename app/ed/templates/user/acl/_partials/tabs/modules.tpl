<div class="table-responsive">
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>#id</th>
            <th>nazwa</th>
            <th>opis</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        {foreach from=$list item=l}
            <tr>
                <td>{$l->id}</td>
                <td>{$l->name}</td>
                <td>{$l->description}</td>
                <td class="text-center">
                    <a href="#" onClick="show_modal_ajax('Edytuj moduł', '{BASE_ED_URL}user/acl/ed_modules.html?id={$l->id}')">[edytuj]</a>
                    <a href="{BASE_ED_URL}user/acl/usun_modul.html?id={$l->id}" onClick="return confirm('Czy na pewno chcesz usunąć ten moduł?')">[usuń]</a>
                </td>
            </tr>
        {/foreach}
        </tbody>
    </table>
</div>