<form method="POST" action="{BASE_ED_URL}user/acl/zapisz_grupa_modul.html">
    <div class="form-group">
        <label>Grupa</label>
        <input type="text" id="group_name" class="form-control" value="{if !empty($item->group_name)}{$item->group_name}{/if}" required />
        <input type="hidden" name="id_group" id="id_group" value="{if !empty($item->id_group)}{$item->id_group}{/if}" />
    </div>
    <div class="form-group">
        <label>Moduł</label>
        <input type="text" id="module_name" class="form-control" value="{if !empty($item->module_name)}{$item->module_name}{/if}" />
        <input type="hidden" name="id_module" id="id_module" value="{if !empty($item->id_module)}{$item->id_module}{/if}" />
    </div>
    <div class="form-group">
        <input type="hidden" name="id" value="{if !empty($item->id_acl)}{$item->id_acl}{else}0{/if}">
        <button type="submit" class="btn btn-primary">Zapisz</button>
    </div>
</form>

<script type="text/javascript">
    {literal}
    $(document).ready(function(){
        $("#group_name").autocomplete({
            source: function (request, response)
            {
                $.ajax({
                    url: BASE_ED_URL + "user/acl/ajax_pobierz_nazwe_grupy.html",
                    dataType: "json",
                    method: 'POST',
                    data: {
                        term: request.term
                    },
                    success: function (data)
                    {
                        response(data);
                    }
                });
            },
            minLength: 2,
            select: function (event, ui)
            {
                $('#id_group').val(ui.item.id);
            }
        });

        $("#module_name").autocomplete({
            source: function (request, response)
            {
                $.ajax({
                    url: BASE_ED_URL + "user/acl/ajax_pobierz_nazwe_modulu.html",
                    dataType: "json",
                    method: 'POST',
                    data: {
                        term: request.term
                    },
                    success: function (data)
                    {
                        response(data);
                    }
                });
            },
            minLength: 2,
            select: function (event, ui)
            {
                $('#id_module').val(ui.item.id);
            }
        });

    })
    {/literal}

</script>