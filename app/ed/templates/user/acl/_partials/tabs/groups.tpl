<div class="table-responsive">
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>#id</th>
            <th>nazwa</th>
            <th>opis</th>
        </tr>
        </thead>
        <tbody>
        {foreach from=$list item=l}
            <tr>
                <td>{$l->id}</td>
                <td>{$l->name}</td>
                <td>{$l->description}</td>
                <td class="text-center">
                    <a href="#" onClick="show_modal_ajax('Edytuj grupę', '{BASE_ED_URL}user/acl/ed_groups.html?id={$l->id}')">[edytuj]</a>
                    <a href="#" data-href="{BASE_ED_URL}user/acl/usun_grupe.html?id={$l->id}" onclick="delete_item($(this), 'Czy na pewno chcesz usunąć tą grupę?');">[usuń]</a>
                </td>
            </tr>
        {/foreach}
        </tbody>
    </table>
</div>