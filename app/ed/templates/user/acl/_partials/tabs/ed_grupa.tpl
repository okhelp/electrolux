<form method="POST" action="{BASE_ED_URL}user/acl/zapisz_grupe.html">
    <div class="form-group">
        <label>Nazwa</label>
        <input type="text" name="name" class="form-control" value="{if !empty($item->name)}{$item->name}{/if}" required />
    </div><div class="form-group">
        <label>Opis</label>
        <input type="text" name="description" class="form-control" value="{if !empty($item->description)}{$item->description}{/if}" />
    </div>
    <div class="form-group">
        <input type="hidden" name="id_group" value="{if !empty($item->id)}{$item->id}{else}0{/if}">
        <button type="submit" class="btn btn-primary">Zapisz</button>
    </div>
</form>