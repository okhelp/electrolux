{load_plugin name="bootbox"}

<ul id="acl_tabs" class="nav nav-tabs" role="tablist">
    <li class="nav-item">
        <a class="nav-link active show" id="modules-tab" data-toggle="tab" href="#groups-1" role="tab" aria-controls="groups-1" aria-selected="false">Grupy</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="modules-tab" data-toggle="tab" href="#modules-1" role="tab" aria-controls="modules-1" aria-selected="false">Moduły</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="user_group-tab" data-toggle="tab" href="#user_group-1" role="tab" aria-controls="user_group-1" aria-selected="false">Użytkownik => Grupa</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="group_module-tab" data-toggle="tab" href="#group_module-1" role="tab" aria-controls="group_module-1" aria-selected="false">Grupa => Moduł</a>
    </li>
</ul>

<div class="tab-content">

    <div class="tab-pane fade show active" id="groups-1" role="tabpanel" aria-labelledby="groups-tab"></div>
    <div class="tab-pane fade" id="modules-1" role="tabpanel" aria-labelledby="modules-tab"></div>
    <div class="tab-pane fade" id="user_group-1" role="tabpanel" aria-labelledby="user_group-tab"></div>
    <div class="tab-pane fade" id="group_module-1" role="tabpanel" aria-labelledby="group_module-tab"></div>


    <a href="#" id="dodaj-btn" class="btn btn-info btn-default">+ dodaj</a>

</div>

<script type="text/javascript">
    {literal}

    function delete_item(el, text)
    {
        href = el.data('href');

        console.log(href);

        bootbox.confirm({
            title: '',
            message: text,
            buttons: {
                confirm: {
                    label: 'Tak',
                    className: 'btn-info'
                },
                cancel: {
                    label: 'Nie',
                    className: 'btn-danger'
                }
            },
            callback: function(result) {
                if (result) {
                    document.location.href = href;
                }
            }
        });
    }

    function show_modal_ajax(title, ajax_url)
    {
        // dodaję tytuł
        $('#modal_ajax .modal-title').text(title);

        //dodaję loader
        $('#modal_ajax .modal-body').html('<div class="text-center"><img src="' + BASE_URL + '/data/ed/img/ajax_loader.gif" alt="" /></div>');

        //otwieram okienko
        $('#modal_ajax').modal('show');

        //Ładuję treść z linku
        $.ajax({
            type: "GET",
            url: ajax_url,
            success: function (msg)
            {
                $('#modal_ajax .modal-body div:first').slideUp();
                $('#modal_ajax .modal-body').html(msg);
            }
        });
    }

        $(document).ready(function(){

            //akcja na pokazywanie tresci zakladek
            $('#acl_tabs li').click(function(){
                var tab_name = $(this).find('a').attr('aria-controls');
                tab_name = tab_name.replace('-1','');

                show_tab_content(tab_name);

            })

            //akcja na button dodaj
            $('#dodaj-btn').click(function(e){
                e.preventDefault();
                add_items(get_tab_name());
            });

            //pokazuję content w pierwszej zakładki
            show_tab_content(get_tab_name());


        });

        function get_tab_name()
        {
            var tab_type = $('#acl_tabs li a.active').attr('aria-controls');
            return tab_type.replace('-1','');
        }

        function show_tab_content(tab_name)
        {
            $.ajax({
                type: "POST",
                url: BASE_ED_URL + "user/acl/ajax_pokaz_zakladke.html",
                data: "zakladka=" + tab_name,
                success: function (data) {
                    var tab_id = tab_name + '-1';
                    $('#' + tab_id).html(data);
                },
            });
        }

        function add_items(type)
        {
            show_modal_ajax('Dodaj element', BASE_ED_URL + 'user/acl/ed_' + type + '.html');
        }

    {/literal}
</script>