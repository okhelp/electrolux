{load_plugin name="datatable"}

<a href="{BASE_ED_URL}user/profil/edytuj.html" class="btn btn-info">Dodaj pracownika</a>

<table class='datatable'>
	<thead>
		<tr>
			<th>#id</th>
			<th>Login</th>
			<th>E-mail</th>
			<th>Imię</th>
			<th>Nazwisko</th>
			<th>Data utworzenia</th>
			<th>Status</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
        {foreach from=$users item=user}
            <tr>
                <td>{$user->id}</td>
                <td>{$user->login}</td>
                <td>{$user->email}</td>
                <td>{$user->name}</td>
                <td>{$user->surname}</td>
                <td>{$user->ts->getTimeStamp()|friendly_date}</td>
                <td>{if $user->status}<span class="badge badge-success">aktywny</span>{else}<span class="badge badge-danger">nieaktywny</span>{/if}</td>
                <td>
                    <a href="{BASE_ED_URL}user/profil/edytuj.html?id={$user->id}" class="btn btn-primary">edytuj</a>
                </td>
            </tr>
        {/foreach}
	</tbody>
</table>