{load_plugin name="datatable"}

<a href="{BASE_ED_URL}user/przypisz_opiekuna.html" class="btn btn-info">Dodaj</a>

<table class='datatable'>
    <thead>
    <tr>
        <th>Opiekun</th>
        <th>Firma</th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    {foreach from=$assigned_supervisors item=l}
        <tr>
            <td>{$l['supervisor']->name} {$l['supervisor']->surname}</td>
            <td>{$l['company']->name}</td>
            <td class="text-right">
                <a href="{BASE_ED_URL}user/usun_przypisanie_opiekuna.html?id={$l['id']}" class='btn btn-danger confirm'
                   data-confirm-text="Czy na pewno chcesz usunąć przypisanie?">usuń</a>
            </td>
        </tr>
    {/foreach}
    </tbody>
</table>

