<ul class="nav nav-tabs" role="tablist">
	<li class='active'>
		<a href="#tab_acl_groups" role="tab" data-toggle="tab">
			Grupy
		</a>
	</li>
	<li>
		<a href="#tab_acl_level" role="tab" data-toggle="tab">
			Poziomy uprawnień
		</a>
	</li>
</ul>
<div class="tab-content">
	<div class="tab-pane active" id="tab_acl_groups">
		grupy
	</div>
	<div class="tab-pane" id="tab_acl_level">
		{include file="user/_partials/acl/level.tpl"}
	</div>
</div>