<link rel="stylesheet" type="text/css" href="{BASE_URL}data/ed/css/dashboard.css">
<div class="row grid-margin">
    <div class="col-12">
        <div class="card card-statistics">
            <div class="card-body">
                <div class="d-flex flex-column flex-md-row align-items-center" style="flex-wrap: wrap">
                    {foreach $statistics as $statistics_element}
                        <div class="statistics-item">
                            <p>
                                {$statistics_element['label']}
                            </p>
                            <h2>{$statistics_element['visits']}</h2>
                        </div>
                    {/foreach}
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{BASE_URL}data/js/Chart.js/Chart.min.js"></script>
<div class="row grid-margin">

    {foreach $charts as $index => $chart}
        <div class="col-6">
            <canvas id="{$index}"></canvas>
        </div>
        <script>
            {literal}
                    new Chart(document.getElementById("{/literal}{$index}{literal}").getContext('2d'), {
                        type: '{/literal}{$chart.type}{literal}',
                        data: {
                            labels: {/literal}{$chart.data.labels|@json_encode}{literal},
                            datasets: [{
                                label: "{/literal}{$chart.label}{literal}",
                                data: {/literal}{$chart.data.values|@json_encode}{literal},
                                borderWidth: 1
                            }]
                        },
                        options: {
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        beginAtZero: true
                                    }
                                }]
                            }
                        }
                    });
            {/literal}
        </script>
    {/foreach}
</div>