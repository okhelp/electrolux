<form method="POST">
	<div class="form-group">
		<label>Nazwa:</label>
		<input type="text" name="name" class="form-control" value="{if $lang->name}{$lang->name}{/if}" required="required" />
	</div>
	<div class="form-group">
		<label>Kod:</label>
		<div class="alert alert-info">
			Kod jest również nazwą katalogu z plikami odpowidzialnymi za tłumaczenia.
		</div>
		<input type="text" name="code" class="form-control" value="{if $lang->code}{$lang->code}{/if}" required="required" />
	</div>
	<div class="form-group">
		<label>Symbol waluty:</label>
		<input type="text" name="currency" class="form-control" value="{if $lang->currency}{$lang->currency}{/if}" />
	</div>
	<div class="form-group">
		<button type="submit" class="btn btn-dafault">Zapisz</button>
	</div>
</form>