{load_plugin name="datatable"}

<a href="{BASE_ED_URL}languages/ed.html" class="btn btn-info">Dodaj</a>

<table class='datatable'>
	<thead>
		<tr>
			<th>#id</th>
			<th>nazwa</th>
			<th>kod</th>
			<th>waluta</th>
			<th></th>
		</tr>
	</thead> 
	<tbody>
		{foreach from=$list item=l}
			<tr>
				<td>{$l->id}</td>
				<td>{$l->name}</td>
				<td>{$l->code}</td>
				<td>{$l->currency}</td>
				<td class="text-right">
					<a href="{BASE_ED_URL}languages/ed.html?id={$l->id}" class='btn btn-default'>edytuj</a>
					
					{if $l->id != 1} {* pierwszego języka nie usuwamy *}
						<a href="{BASE_ED_URL}languages/tlumacz.html?id={$l->id}" class='btn btn-info'>Tłumacz</a>
						<a href="{BASE_ED_URL}languages/usun.html?id={$l->id}" class='btn btn-danger confirm' data-confirm-text="Czy na pewno chcesz usunąć ten język?">usuń</a>
					{/if}
				</td>
			</tr>
		{/foreach}
	</tbody>
</table>

