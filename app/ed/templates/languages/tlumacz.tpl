{* wybieranie pliku do tłumaczenia *}
{if $mode == 'select_file'}
	<div class="row">
		<div class="col-md-4 col-md-offset-4 col-xs-12">
			<form method='GET'>
				<input type="hidden" name="id" value="{$smarty.get.id}" />
				<div class="form-group">
					<label>Wybierz plik do tłumaczenia:</label>
					<select name='file' class='form-control'>
						{foreach from=$files item=file}
							<option value="{$file}">{$file}</option>
						{/foreach}
					</select>
				</div>
				<div class="form-group text-center">
					<button type="submit" class="btn btn-submit">Pokaż plik</button>
				</div>
			</form>
		</div>
	</div>
{/if}

{* tłumaczenie pliku *}
{if $mode == 'translate_file'}
	<fieldset>
		<legend>Edytowany plik: {$smarty.get.file}</legend>
		{if $smarty.get.file == 'main.php'}
			<div class="alert alert-info">
				Nowe frazy zostały automatycznie wczytane.
			</div>
		{/if}
		{if $lang}
			<form method="POST">
				<table class='table table-responsive'>
					<thead>
						<tr>
							<th>Fraza oryginalna</th>
							<th>Tłumaczenie</th>
						</tr>
					</thead>
					<tbody>
						{foreach from=$lang key=original item=translated}
							<tr>
								<td>{$original|base64_decode}</td>
								<td><input type="text" name="{$original}" value="{$translated}" class="form-control" /></td>
							</tr>
						{/foreach}
					</tbody>
				</table>
				<button type="submit" class="btn btn-submit">Tłumacz</button>
			</form>
		{else}
			<div class="alert alert-danger">
				Brak fraz do wyświetlenia.
			</div>
		{/if}
		
	</fieldset>
{/if}