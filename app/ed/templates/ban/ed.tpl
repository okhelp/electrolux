{load_plugin name="datepicker"}
<form method="POST">
	<div class="form-group">
		<label>Adres IP:</label>
		<input type="text" name="ip" value="{if $ban && $ban->ip}{$ban->ip}{/if}" class="form-control" />
	</div>
	<div class="form-group">
		<label>Host:</label>
		<input type="text" name="host" value="{if $ban && $ban->host}{$ban->host}{/if}" class="form-control" />
	</div>
	<div class="form-group">
		<label>ID_COOKIE:</label>
		<input type="text" name="id_cookie" value="{if $ban && $ban->id_cookie}{$ban->id_cookie}{/if}" class="form-control" />
	</div>
	<div class="form-group">
		<label>Koniec blokady:</label>
		<input type="text" name="date_stop" value="{if $ban && $ban->date_stop}{$ban->date_stop->format('Y-m-d')}{/if}" class="form-control datepicker" required="required" readonly="readonly" />
	</div>
	<div class="form-group">
		<button type="submit" class="btn btn-submit">Zapisz</button>
	</div>
</form>