{load_plugin name="datatable"}
<a href="ed.html" class="btn btn-info">Dodaj</a>
<table class='datatable'>
	<thead>
		<tr>
			<th>#id</th>
			<th>adres ip</th>
			<th>host</th>
			<th>id_cookie</th>
			<th>zakończenie blokady</th>
			<th>data dodania</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		{foreach from=$list item=l}
			<tr>
				<td>{$l->id}</td>
				<td>{if $l->ip}{$l->ip}{else}-{/if}</td>
				<td>{if $l->host}{$l->host}{else}-{/if}</td>
				<td>{if $l->id_cookie}{$l->id_cookie}{else}-{/if}</td>
				<td>{if $l->date_stop}{$l->date_stop->format('Y-m-d')}{else}-{/if}</td>
				<td>{if $l->ts}{$l->ts->format('Y-m-d H:i:s')}{else}-{/if}</td>
				<td class="text-right">
					<a href="{BASE_ED_URL}ban/ed.html?id={$l->id}" class='btn btn-default'>edytuj</a>
					<a href="{BASE_ED_URL}ban/usun.html?id={$l->id}" class='btn btn-danger confirm' data-confirm-text="Czy na pewno chcesz usunąć tą blokadę?">usuń</a>
				</td>
			</tr>
		{/foreach}
	</tbody>
</table>
	
