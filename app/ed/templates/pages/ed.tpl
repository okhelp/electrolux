{load_plugin name="ckeditor"}

<form method="POST">

	<div class="form-group">
		<label>Tytuł podstrony:</label>
		<input type="text" name="title" value="{if $item}{$item->title}{/if}" class="form-control" required="required" />
	</div>

	<div class="form-group">
		<label>Treść podstrony:</label>
		<div class="alert alert-info">
			<ul>
				<li>
					&rsaquo; <b>Jak wstawić kolumnę?</b> [column name="nazwa_kolumny"]
				</li>
				<li>
					&rsaquo; <b>Jak wstawić plik css?</b> [css name="plik.css"]
				</li>
				<li>
					&rsaquo; <b>Jak wstawić plik js?</b> [js name="plik.js"]
				</li>
			</ul>
		</div>
		<textarea name="description" class="ckeditor">{if $item}{$item->description}{/if}</textarea>
	</div>

	<div class="form-group">
		<label>Kategorie:</label>
		<div class="alert alert-info">
			Jeżeli nie wybierzesz kategorii, podstrona będzie przypisana bezpośrednio do serwisu.
		</div>
		{$category_tree}
	</div>

	<div class="form-group">
		<label>Status:</label>
		<select name="status" class="form-control">
			<option value="1"{if $item && !empty($item->status)}selected='selected'{/if}>Włączona</option>
			<option value="0"{if $item && empty($item->status)}selected='selected'{/if}>Wyłączona</option>
		</select>
	</div>

	<fieldset>
		<legend>SEO (metatagi)</legend>
		<div class="form-group">
			<label>Opis:</label>
			<input type="text" name="desc_meta" value="{if $item}{$item->desc_meta}{/if}" class="form-control" />
		</div>
		<div class="form-group">
			<label>Słowa kluczowe:</label>
			<input type="text" name="keywords_meta" value="{if $item}{$item->keywords_meta}{/if}" class="form-control" />
		</div>
		<div class="form-group">
			<label>Social logo: <small>Rozmiar: <b>320x320px</b></small></label>
			{if $item && $item->social_logo}
				<div id="social-logo">
					<div class="row">
						<div class="col-md-12">
							<img class="img-thumbnail" src="{img id_cimg=$item->social_logo width=320 height=320}" alt="" style="margin: 12px 0" />
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<p>
								<a href="#" id="social-logo-do-change" class="btn btn-info">Zmień</a>
							</p>
						</div>
					</div>
				</div>
			{/if}
			<div id="social-logo-change"{if $item && $item->social_logo} style="display:none"{/if}>
				{include file="_partials/upload/images_form.tpl" name="social_logo" file_limit=1 show_thumb=1}
			</div>
		</div>
	</fieldset>

	<div class="form-group">
		<button type="submit" name="save" value="1" class="btn btn-dafault">Zapisz</button>
	</div>

</form>

<script type="text/javascript">
	{if $settings && $item->social_logo}
		{literal}
			$(document).ready(function ()
			{
				$('#social-logo-do-change').click(function (e)
				{
					$('#social-logo').slideUp(function ()
					{
						$('#social-logo-change').slideDown();
					})
					e.preventDefault();
				})
			})
		{/literal}
	{/if}
</script>