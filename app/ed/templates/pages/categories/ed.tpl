<form method="POST">
	<div class="form-group">
		<label>Nazwa:</label>
		<input type="text" class="form-control" name="name" required="required" value="{if $item}{$item->name}{/if}" />
	</div>
	<div class="form-group">
		<label>Status:</label>
		<select name="status" class="form-control">
			<option value="1"{if !empty($item) && $item->status == 1} selected="selected"{/if}>aktywna</option>
			<option value="0"{if !empty($item) && $item->status == 0} selected="selected"{/if}>nieaktywna</option>
		</select>
	</div>
	<div class="form-group">
		<label>Wybierz podkategorię</label>
		{$category_tree}
	</div>
	<button type="submit" name="save" value="1" class="btn btn-dafault">Zapisz</button>
</form>