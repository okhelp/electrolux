{load_plugin name="datatable"}

<a href="{BASE_ED_URL}pages/ed.html" class="btn btn-info">Dodaj</a>

<table class="datatable">
	<thead>
	<tr>
		<th>#id</th>
		<th>Nazwa</th>
		<th>Kategoria</th>
		<th>Data dodania</th>
		<th>Autor</th>
		<th>Status</th>
		<th></th>
		<th></th>
	</tr>
	</thead>
	<tbody>
	{foreach from=$list item=l}
		{assign var="category" value=$l->get_category()}
		<tr>
			<td>{$l->id}</td>
			<td>{$l->title}</td>
			<td>{if $category}{$category->name}{else}-{/if}</td>
			<td>{$l->ts->format('Y-m-d H:i:s')}</td>
			<td>{$l->id_user|id2name}</td>
			<td>
				{if $l->status == 1}
					<span class="label label-success">aktywna</span>
				{else}
					<span class="label label-danger">nieaktywna</span>
				{/if}
			</td>
			<td class="text-center">
				<a href="#" data-url="<pre>{$l->get_url()}</pre>" class='show-url btn btn-info'>pokaż adres url</a>
			</td>
			<td class="text-right">
				<a href="{BASE_ED_URL}pages/ed.html?id={$l->id}" class='btn btn-default'>edytuj</a>
				<a href="{BASE_ED_URL}pages/usun.html?id={$l->id}" class='btn btn-danger confirm' data-confirm-text="Czy na pewno chcesz usunąć tą podstronę?">usuń</a>
			</td>
		</tr>
	{/foreach}
	</tbody>
</table>