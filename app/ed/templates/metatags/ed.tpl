<h3 class="box-title">
	{if $id}
		Edytuj tag
	{else}
		Dodaj tag
	{/if}
</h3>

<form method="POST" id="f">
	<div class="form-group">
		<label>Nazwa:</label>
		<input type="text" name="name" value="{if $id}{$item->name}{/if}" class="form-control required" required="required" class="" />
	</div>
	<div class="form-group">
		<label>Opis tagu:</label>
		<textarea class="form-control required" name="site_description" required="required">{if $id}{$item->site_description}{/if}</textarea>
	</div>
	<div class="form-group">
		<label>Przykładowy link:</label>
		<div class="alert alert-info">
			Pamiętaj o <strong>http://</strong> na początku linku.
		</div>
		<input type="text" name="demo_page" value="{if $id}{$item->demo_page}{/if}" class="form-control required" required="required" class="" />
	</div>
	<div class="form-group">
		<label>Tytuł strony:</label>
		<input type="text" name="title" value="{if $id}{$item->title}{/if}" class="form-control required" required="required" class="" />
	</div>
	<div class="form-group">
		<label>Opis strony (meta tags):</label>
		<textarea class="form-control required" name="meta_descripton" required="required">{if $id}{$item->meta_descripton}{/if}</textarea>
	</div>
	<div class="form-group">
		<label>Słowa kluczowe (meta tags):</label>
		<textarea class="form-control required" name="meta_keywords" required="required">{if $id}{$item->meta_keywords}{/if}</textarea>
	</div>
	<div class="form-group">
		<label>Obrazek (social media):</label>
		{if $id && $item->id_cimg}
			<div id="avatar">
				<div class="row">
					<div class="col-md-12">
						<img src="{img id_cimg=$item->id_cimg width=315 height=315}" alt="" />
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<p>
							<a href="#" id="avatar-do-change" class="btn btn-info">Zmień</a>
						</p>
					</div>
				</div>
			</div>
		{/if}
		<div id="avatar-change"{if $id && $item->id_cimg} style="display:none"{/if}>
			{include file="_partials/upload/images_form.tpl" name="avatar" file_limit=1 show_thumb=1}
		</div>
	</div>
	<div class="form-group">
		<button type="submit" name="zapisz" value="save" class="btn btn-submit">Zapisz</button>
	</div>
</form>