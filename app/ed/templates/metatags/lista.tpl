{load_plugin name="datatable"}

<table class='datatable'>
	<thead>
		<tr>
			<th>#id</th>
			<th>nazwa</th>
			<th>opis</th>
			<th>przykładowa strona</th>
			<th>wprowadził(a)</th>
			<th>data utworzenia</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		{foreach from=$list item=l}
			<tr>
				<td>{$l->id}</td>
				<td>{$l->name}</td>
				<td>{$l->site_description}</td>
				<td><a href="{$l->demo_page}" target='_blank'>przejdź</a></td>
				<td>{id2name id=$l->id_user}</td>
				<td>{$l->ts->format('Y-m-d H:i:s')}</td>
				<td>
					<a href="ed.html?id={$l->id}" class='btn btn-default'>edytuj</a>
					<a href="usun.html?id={$l->id}" class='btn btn-danger' onclick="return confirm('Czy na pewno chcesz usunąć tą pozycję?')">usuń</a>
				</td>
			</tr>
		{/foreach}
	</tbody>
</table>
	
<a href="ed.html" class="btn btn-info">Dodaj</a>

