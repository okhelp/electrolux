

$(document).ready(function () {
    var $gallery = $('#gallery_works');
    var $boxes = $('.list__item');
//    $boxes.hide();

    $gallery.imagesLoaded({background: true}, function () {
        $boxes.fadeIn();

        $gallery.isotope({
            itemSelector: '.list__item',
        });
    });

    $('button').on('click', function () {
        var filterValue = $(this).attr('data-filter');
        $gallery.isotope({filter: filterValue});
        $gallery.data('lightGallery').destroy(true);
        $gallery.lightGallery({
            selector: filterValue.replace('*', '')
        });
    });
});

$(document).ready(function () {
    $('#gallery_works').lightGallery({

    });
});

