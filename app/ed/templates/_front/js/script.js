$(document).ready(function ()
{
    var navbar_menu_mobile_html;
    var navbar_menu_default_html = $('#menu').html();

    $(window).resize(function ()
    {

        if ($(window).width() <= 1006)
        {
            if(!navbar_menu_mobile_html)
            {
                $("#menu").mmenu({
                    extensions: ["position-bottom", "fullscreen", "theme-black", "listview-50", "fx-panels-slide-up", "fx-listitems-drop", "border-offset"],
                    navbar: {
                        title: ""
                    },

                    }, {});
                var API = $("#menu").data( "mmenu" );
                $(".mmenu__link").click(function() {
                    API.close();
                });
                $(".mh-head.mm-sticky").mhead({
                    scroll: {
                        hide: 200
                    }
                });
                $(".mh-head:not(.mm-sticky)").mhead({
                    scroll: false
                });
            }


            navbar_menu_mobile_html = $('#menu').html();
            $('#my-menu').html(navbar_menu_mobile_html);
            $('a[data-scroll]').click(function (e)
            {
                e.preventDefault();

                var scroll_to_id = $(this).attr('data-scroll');
                scroll_to_div(scroll_to_id);


            });

        }
        else
        {
            $('#my-menu').html(navbar_menu_default_html);

        }

    }).resize();
});

$(document).scroll(function (e)
{
    var scrollTop = $(document).scrollTop();

    if (scrollTop > 0)
    {
        $('header').addClass('scrolled');
    } else
    {
        $('header').removeClass('scrolled');
    }


});

var $headerHeight = $('.header').innerHeight();
$(document).ready(function ()
{
    $("#mm-my-menu").css( { height: `calc(100% - ${$headerHeight}px)` } );
});
var $headerScrollHeight = $('.header').height();
$(window).scroll(function ()
{
    if ($(this).scrollTop() === 0)
    {
        $("#mm-my-menu").css( { height: `calc(100% - ${$headerHeight}px)` } );

    }
    else {
        $("#mm-my-menu").css({height: `calc(100% - ${$headerScrollHeight}px)`});
    }
});

$(document).ready(function ()
{
    var navbar_height = $('header').height();
    $('.go-down').click(function ()
    {

        $('html, body').animate({
            scrollTop: $("#s2").offset().top - navbar_height
        }, 1500);
    });
    $('.up-to').click(function () {
        $('html, body').animate({
            scrollTop: $("body").offset().top
        }, 1500);
    });
});

// dodawanie klasy do inputa gdy jest wpisana jakaś wartość
$(document).ready(function ()
{
    $('.input2').each(function ()
    {
        $(this).on('blur', function ()
        {
            if ($(this).val().trim() != "")
            {
                $(this).addClass('has-val');
            } else
            {
                $(this).removeClass('has-val');
            }
        })
    })
});

$(document).ready(function () {
    $('#slider_products').slick({
        dots: true,
        infinite: true,
        speed: 600,
        slidesToShow: 1,
        adaptiveHeight: true,
        prevArrow: '<button class="prev-slick-arrow"><i  class="flaticon-up-arrow"></i></button>',
        nextArrow: '<button class="next-slick-arrow"><i  class="flaticon-down-arrow"></i></button>',
        vertical: true,
        customPaging: function (slider, i) {
            var numeration = (i + 1);
            if (numeration < 10) {
                numeration = '0' + numeration;
            }
            return '<button type="button">' + numeration + '</button>';
        },
        responsive: [
            {
                breakpoint: 1060,
                settings: {
                    vertical: false,
                    dots: false,
                    arrows: false
                }
            },
            {
                breakpoint: 799,
                settings: {
                    dots: false,
                    arrows: false,
                    vertical: false
                }
            }
        ]
    });
});
$(document).ready(function () {
    $('#lazy-slider').slick({
        dots: true,
        infinite: true,
        speed: 600,
        slidesToShow: 3,
        slidesToScroll: 1,
        adaptiveHeight: true,
        prevArrow: '<button class="prev-slick-arrow"><i  class="flaticon-back"></i></button>',
        nextArrow: '<button class="next-slick-arrow"><i  class="flaticon-next"></i></button>',
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    vertical: false,
                    dots: false,
                    arrows: false
                }
            },
            {
                breakpoint: 600,
                settings: {
                    dots: false,
                    arrows: false,
                    vertical: false,
                    slidesToShow: 2,
                }
            },
            {
                breakpoint: 400,
                settings: {
                    dots: false,
                    arrows: false,
                    vertical: false,
                    slidesToShow: 1,
                }
            }
        ]
    });
});