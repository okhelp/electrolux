!function () {
    var href = location.href;
    var pgurl = href.substr(href.lastIndexOf('/') + 1);
    var currentpage = 'a[href="' + pgurl + '"]';
    $(currentpage).addClass('active_page');
    if (pgurl !== 'index.html') {
        $('.header').addClass('scrolled')
    }
    if (pgurl === 'index.html') {
        $(document).scroll(function (e)
        {
            var scrollTop = $(document).scrollTop();

            if (scrollTop > 0)
            {
                $('header').addClass('scrolled');
            } else
            {
                $('header').removeClass('scrolled');
            }
        });
    }
}();
$(document).ready(function () {
    $('.nav-side-text button.is-active').eq(0).click();
    $('.list__item').addClass('hover_photo')
});

$(document).ready(function ()
{
    var navbar_menu_mobile_html;
    var navbar_menu_default_html = $('#menu').html();

    $(window).resize(function ()
    {

        if ($(window).width() <= 1006)
        {
            if(!navbar_menu_mobile_html)
            {
                $("#menu").mmenu({
                    extensions: ["position-bottom", "fullscreen", "theme-white", "listview-50", "fx-panels-slide-up", "fx-listitems-drop", "border-offset"],
                    navbar: {
                        title: ""
                    },
                   }, {});
                var API = $("#menu").data( "mmenu" );
                $(".mmenu__link").click(function() {
                    API.close();
                });
                $(".mh-head.mm-sticky").mhead({
                    scroll: {
                        hide: 200
                    }
                });
                $(".mh-head:not(.mm-sticky)").mhead({
                    scroll: false
                });
            }


            navbar_menu_mobile_html = $('#menu').html();
            $('#my-menu').html(navbar_menu_mobile_html);

        }
        else
        {
            $('#my-menu').html(navbar_menu_default_html);

        }

    }).resize();
});

var $headerHeight = $('.header').innerHeight();
$(document).ready(function ()
{
  $("#mm-my-menu").css( { height: `calc(100% - ${$headerHeight}px)` } );
});
var $headerScrollHeight = $('.header').height();
$(window).scroll(function ()
{
  if ($(this).scrollTop() === 0)
  {
    $("#mm-my-menu").css( { height: `calc(100% - ${$headerHeight}px)` } );

  }
  else {
    $("#mm-my-menu").css({height: `calc(100% - ${$headerScrollHeight}px)`});
  }
  });

//Nadanie klasy active w menu

var navChildren = $(".item").children();
var aArray = [];
for (var i = 0; i < navChildren.length; i++) {
  var aChild = navChildren[i];
  var ahref = $(aChild).attr('href');
  aArray.push(ahref);
}


// dodawanie klasy do inputa gdy jest wpisana jakaś wartość
$(document).ready(function ()
{
    $('.input2').each(function ()
    {
        $(this).on('blur', function ()
        {
            if ($(this).val().trim() != "")
            {
                $(this).addClass('has-val');
            } else
            {
                $(this).removeClass('has-val');
            }
        })
    })
});

$(document).ready(function () {
    $('#slider_products').slick({
        dots: true,
        infinite: true,
        speed: 600,
        slidesToShow: 1,
        adaptiveHeight: true,
        prevArrow: '<button class="prev-slick-arrow"><i  class="flaticon-up-arrow"></i></button>',
        nextArrow: '<button class="next-slick-arrow"><i  class="flaticon-down-arrow"></i></button>',
        vertical: true,
        customPaging: function (slider, i) {
            var numeration = (i + 1);
            if (numeration < 10) {
                numeration = '0' + numeration;
            }
            return '<button type="button">' + numeration + '</button>';
        },
        responsive: [
            {
                breakpoint: 1060,
                settings: {
                    dots: false,
                    arrows: false,
                    vertical: false
                }
            }
        ]
    });
});
$(document).ready(function () {
    $('#lazy-slider').slick({
        dots: true,
        infinite: true,
        speed: 600,
        slidesToShow: 3,
        slidesToScroll: 1,
        adaptiveHeight: true,
        prevArrow: '<button class="prev-slick-arrow"><i  class="flaticon-back"></i></button>',
        nextArrow: '<button class="next-slick-arrow"><i  class="flaticon-next"></i></button>',
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    vertical: false,
                    dots: false,
                    arrows: false
                }
            },
            {
                breakpoint: 600,
                settings: {
                    dots: false,
                    arrows: false,
                    vertical: false,
                    slidesToShow: 2,
                }
            },
            {
                breakpoint: 400,
                settings: {
                    dots: false,
                    arrows: false,
                    vertical: false,
                    slidesToShow: 1,
                }
            }
        ]
    });
});
var $navbar_height = $('header').height();
var $scroll = $('.scroll');
function scrollToSection() {
    $('body, html').animate({
        scrollTop: $(this.hash).offset().top - $navbar_height
    }, 800);
}
$scroll.on('click', scrollToSection);





