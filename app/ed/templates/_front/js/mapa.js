function load_map()
{
    if ($('#map-open').length > 0)
    {
        map = new OpenLayers.Map("map-open");
        map.addLayer(new OpenLayers.Layer.OSM());

        var lonLat = new OpenLayers.LonLat(18.646512, 54.358347)
            .transform(
                new OpenLayers.Projection("EPSG:4326"),
                map.getProjectionObject()
            );

        var zoom = 17;

        var markers = new OpenLayers.Layer.Markers("Markers");
        map.addLayer(markers);

        markers.addMarker(new OpenLayers.Marker(lonLat));
        map.setCenter(lonLat, zoom);
    }

}
load_map();
