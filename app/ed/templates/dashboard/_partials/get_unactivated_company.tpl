{if !empty($unactivated_company)}
    {load_plugin name="datatable"}

    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="card-title d-flex">
                    <div class="col">Firmy, które nie dokonały pierwszej zmiany hasła ({count($unactivated_company)})</div>
                </div>
                <table class="datatable">
                    <thead>
                    <tr>
                        <th>#id użytkownika</th>
                        <th>#id firmy</th>
                        <th>Nazwa</th>
                        <th>NIP (login)</th>
                        <th>Handlowiec</th>
                    </thead>
                    <tbody>
                    {foreach from=$unactivated_company item=uc}
                        {assign var=supervisor value=$uc['company']->get_supervisor()}
                        <tr>
                            <td>{$uc['user']->id}</td>
                            <td>{$uc['company']->id}</td>
                            <td>{$uc['company']->name}</td>
                            <td>{$uc['company']->nip}</td>
                            <td><b>{$supervisor->name} {$supervisor->surname}</b><br /><b>e-mail:</b> {$supervisor->email}{if !empty($supervisor->telephone)}<br /> <b>tel.:</b> {$supervisor->telephone}{/if}</td>
                        </tr>
                    {/foreach}
                    </tbody>
                </table>
            </div>
        </div>
    </div>
{/if}