{if !empty($top_five)}
    <table class="table sortable-table">
        <thead>
        <tr>
            <th>#</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        {foreach $top_five as $index => $top_five_elment}
            <tr>
                <td>{$index + 1}</td>
                {if !empty($top_five_elment->id_electrolux)}
                    <td>{$top_five_elment->id_electrolux}</td>
                {else}
                    <td></td>
                {/if}
                <td>{$top_five_elment->name}</td>
            </tr>
        {/foreach}
        </tbody>
    </table>
{else}
    <div class="alert alert-info">
        Brak danych
    </div>
{/if}
