{if !empty($points_transfers)}
    {foreach $points_transfers as $points_transfer}
        <tr>
            <td>{$points_transfer->created_at->format('Y-m-d H:i:s')}</td>
            <td>
                {abs($points_transfer->sent_points)}
            </td>
            <td>
                {if $points_transfer->sent_points > 0}
                    {$points_transfer->sender_name}
                {else}
                    {$points_transfer->receiver_name}
                {/if}
            </td>
            <td>
                {if $points_transfer->sent_points > 0}
                    {$points_transfer->receiver_name}
                {else}
                    {$points_transfer->sender_name}
                {/if}
            </td>
        </tr>
    {/foreach}
{else}
    <div class="alert alert-info">
        Brak danych
    </div>
{/if}