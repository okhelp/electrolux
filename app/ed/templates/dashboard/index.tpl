{load_plugin name="datatable"}
{load_plugin name="chosen"}

<link rel="stylesheet" type="text/css" href="{BASE_URL}data/ed/css/dashboard.css">

<div class="row grid-margin">
    <div class="col-12">
        <div class="card card-statistics">
            <div class="card-body">
                <div class="d-flex flex-column flex-md-row align-items-center" style="flex-wrap: wrap">
                    {foreach $statistics as $statistics_element}
                        <div class="statistics-item">
                            <p>
                                {$statistics_element['label']}
                            </p>
                            <h2>{$statistics_element['count']}</h2>
                            {if !empty($statistics_element['percents'])}
                                <label class="badge badge-outline-{if $statistics_element['percents'] > 0}success{else}danger{/if} badge-pill">
                                    {$statistics_element['percents']}
                                    % {if $statistics_element['percents'] > 0}wzrost{else}spadek{/if}
                                </label>
                            {/if}
                        </div>
                    {/foreach}
                </div>
            </div>
        </div>
    </div>
</div>

{* pokazuję listę firm, ktore nie dokonały pierwszej zmiany hasła *}
<div class="row grid-margin" id="get_unactivated_company">
    <div class="col-12 text-center">
        <svg style="width:60px;" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/>
        </svg>
        <p>
            <small>Proszę czekać... <br />Trwa ładowanie listy firm, które nie dokonały pierwszej zmiany hasła.</small>
        </p>
    </div>
</div>

<div class="row grid-margin">
    <div class="col-6">
        <div class="card">
            <div class="card-body">
                <div class="card-title d-flex">
                    <div class="col">Top 5</div>
                    <div class="col">
                        <a href="#"
                           data-type="customer"
                           class="btn btn-outline-primary btn-pills single_product__more top_five_filter active">
                            klient
                        </a>
                    </div>
                    <div class="col">
                        <a href="#"
                           data-type="supervisor"
                           class="btn btn-outline-primary btn-pills single_product__more top_five_filter">
                            handlowiec
                        </a>
                    </div>
                    <div class="col">
                        <a href="#"
                           data-type="product"
                           class="btn btn-outline-primary btn-pills single_product__more top_five_filter">
                            produkt
                        </a>
                    </div>
                    <div class="col">
                        <a href="#"
                           data-type="prize"
                           class="btn btn-outline-primary btn-pills single_product__more top_five_filter">
                            nagroda
                        </a>
                    </div>
                </div>
                <div class="row">
                    <div id="top-five-preloader">
                        <svg class="circular" viewBox="25 25 50 50">
                            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"></circle>
                        </svg>
                    </div>
                    <div id="top-five-container" class="table-sorter-wrapper col-lg-12 table-responsive">

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-6">
        <div class="card">
            <div class="card-body">
                <div class="card-title d-flex">
                    <div class="col">Bottom 5</div>
                    <div class="col">
                        <a href="#"
                           data-type="customer"
                           class="btn btn-outline-primary btn-pills single_product__more bottom_five_filter active">
                            klient
                        </a>
                    </div>
                    <div class="col">
                        <a href="#"
                           data-type="supervisor"
                           class="btn btn-outline-primary btn-pills single_product__more bottom_five_filter">
                            handlowiec
                        </a>
                    </div>
                    <div class="col">
                        <a href="#"
                           data-type="product"
                           class="btn btn-outline-primary btn-pills single_product__more bottom_five_filter">
                            produkt
                        </a>
                    </div>
                    <div class="col">
                        <a href="#"
                           data-type="prize"
                           class="btn btn-outline-primary btn-pills single_product__more bottom_five_filter">
                            nagroda
                        </a>
                    </div>
                </div>
                <div class="row">
                    <div id="bottom-five-preloader">
                        <svg class="circular" viewBox="25 25 50 50">
                            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"></circle>
                        </svg>
                    </div>
                    <div id="bottom-five-container" class="table-sorter-wrapper col-lg-12 table-responsive">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row grid-margin">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="card-title d-flex">
                    <div class="col">Produkty</div>
                    <div class="col">
                        <select name="" id="products-period" class="chosen" data-placeholder="WYBÓR OKRESU">
                            <option value="day">Dzień</option>
                            <option value="week">Tydzień</option>
                            <option value="month">Miesiąc</option>
                            <option value="year">Rok</option>
                        </select>
                    </div>
                    <div class="col">
                        <select name="" id="products-customer" class="chosen" data-placeholder="WYBÓR KLIENTA">
                            <option value=""></option>
                            {foreach $products_customers as $products_customer}
                                <option value="{$products_customer['id']}">{$products_customer['name']}</option>
                            {/foreach}
                        </select>
                    </div>
                    <div class="col">
                        <select name="" id="products-supervisor" class="chosen" data-placeholder="WYBÓR HANDLOWCA">
                            <option value=""></option>
                            {foreach $products_supervisors as $products_supervisor}
                                <option value="{$products_supervisor['id']}">{$products_supervisor['name']}</option>
                            {/foreach}
                        </select>
                    </div>
                </div>
                <table id="products-table">
                    <thead>
                    <tr>
                        <th>Numer faktury</th>
                        <th>Nazwa</th>
                        <th>Model</th>
                        <th>Status</th>
                        <th>Ilość</th>
                        <th>Cena</th>
                        <th>Punkty</th>
                        <th>Klient</th>
                        <th>Opiekun</th>
                        <th>Data rejestracji</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="row grid-margin">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="card-title d-flex">
                    <div class="col">Nagrody</div>
                    <div class="col">
                        <select name="" id="prizes-period" class="chosen" data-placeholder="WYBÓR OKRESU">
                            <option value="day">Dzień</option>
                            <option value="week">Tydzień</option>
                            <option value="month">Miesiąc</option>
                            <option value="year">Rok</option>
                        </select>
                    </div>
                    <div class="col">
                        <select name="" id="prizes-customer" class="chosen" data-placeholder="WYBÓR KLIENTA">
                            <option value=""></option>
                            {foreach $prizes_customers as $prizes_customer}
                                <option value="{$prizes_customer['id']}">{$prizes_customer['name']}</option>
                            {/foreach}
                        </select>
                    </div>
                    <div class="col">
                        <select name="" id="prizes-supervisor" class="chosen" data-placeholder="WYBÓR HANDLOWCA">
                            <option value=""></option>
                            {foreach $prizes_supervisors as $prizes_supervisor}
                                <option value="{$prizes_supervisor['id']}">{$prizes_supervisor['name']}</option>
                            {/foreach}
                        </select>
                    </div>
                </div>
                <table id="prizes-table">
                    <thead>
                    <tr>
                        <th>Nazwa</th>
                        <th>Klient</th>
                        <th>Handlowiec</th>
                        <th>Data zamówienia</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

{*------------obecnie brak wyznaczania celi--------------*}
{*<div class="row grid-margin">*}
{*    <div class="col-12">*}
{*        <div class="card">*}
{*            <div class="card-body">*}
{*                <h4 class="card-title">Target / Cel</h4>*}
{*                <div class="row">*}
{*                    <div class="table-sorter-wrapper col-lg-12 table-responsive">*}
{*                        <table class="table table-striped">*}
{*                            <thead>*}
{*                            <tr>*}
{*                                <th>#</th>*}
{*                                <th>First Name</th>*}
{*                                <th>Last Name</th>*}
{*                                <th>Progress</th>*}
{*                                <th>Amount</th>*}
{*                                <th>Deadline</th>*}
{*                            </tr>*}
{*                            </thead>*}
{*                            <tbody>*}
{*                            <tr>*}
{*                                <td>1</td>*}
{*                                <td>Herman Beck</td>*}
{*                                <td>John</td>*}
{*                                <td>*}
{*                                    <div class="progress progress-md">*}
{*                                        <div class="progress-bar bg-success" role="progressbar" style="width: 25%"*}
{*                                             aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>*}
{*                                    </div>*}
{*                                </td>*}
{*                                <td>$456.00</td>*}
{*                                <td>12 May 2017</td>*}
{*                            </tr>*}
{*                            <tr>*}
{*                                <td>2</td>*}
{*                                <td>Herman Beck</td>*}
{*                                <td>Conway</td>*}
{*                                <td>*}
{*                                    <div class="progress progress-md">*}
{*                                        <div class="progress-bar bg-success" role="progressbar" style="width: 25%"*}
{*                                             aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>*}
{*                                    </div>*}
{*                                </td>*}
{*                                <td>$965.00</td>*}
{*                                <td>13 May 2017</td>*}
{*                            </tr>*}
{*                            <tr>*}
{*                                <td>3</td>*}
{*                                <td>John Richards</td>*}
{*                                <td>Alex</td>*}
{*                                <td>*}
{*                                    <div class="progress progress-md">*}
{*                                        <div class="progress-bar bg-success" role="progressbar" style="width: 25%"*}
{*                                             aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>*}
{*                                    </div>*}
{*                                </td>*}
{*                                <td>$255.00</td>*}
{*                                <td>14 May 2017</td>*}
{*                            </tr>*}
{*                            <tr>*}
{*                                <td>4</td>*}
{*                                <td>John Richards</td>*}
{*                                <td>Jason</td>*}
{*                                <td>*}
{*                                    <div class="progress progress-md">*}
{*                                        <div class="progress-bar bg-success" role="progressbar" style="width: 25%"*}
{*                                             aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>*}
{*                                    </div>*}
{*                                </td>*}
{*                                <td>$975.00</td>*}
{*                                <td>15 May 2017</td>*}
{*                            </tr>*}
{*                            <tr>*}
{*                                <td>5</td>*}
{*                                <td>Messsy max</td>*}
{*                                <td>Back</td>*}
{*                                <td>*}
{*                                    <div class="progress progress-md">*}
{*                                        <div class="progress-bar bg-success" role="progressbar" style="width: 25%"*}
{*                                             aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>*}
{*                                    </div>*}
{*                                </td>*}
{*                                <td>$298.00</td>*}
{*                                <td>16 May 2017</td>*}
{*                            </tr>*}
{*                            </tbody>*}
{*                        </table>*}
{*                    </div>*}
{*                </div>*}
{*            </div>*}
{*        </div>*}
{*    </div>*}
{*</div>*}

<div class="row grid-margin">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="card-title">
                    <div class="col">Ruchy na koncie</div>
                    <div class="col-3 mt-3">
                        <select id="points-client-select" class="chosen" data-placeholder="WYBÓR KLIENTA">
                            <option value=""></option>
                            {foreach $points_transfers_users as $points_transfer_user}
                                <option value="{$points_transfer_user->id}">{$points_transfer_user->name} {$points_transfer_user->surname}</option>
                            {/foreach}
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="table-sorter-wrapper col-lg-12 table-responsive">
                        <div id="points-preloader" style="display: none;">
                            <svg class="circular" viewBox="25 25 50 50">
                                <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"></circle>
                            </svg>
                        </div>
                        <table id="points-table" class="table sortable-table">
                            <thead>
                            <tr>
                                <th class="sortStyle">Data<i class="fa fa-angle-down"></i></th>
                                <th class="sortStyle">E-luxy<i class="fa fa-angle-down"></i></th>
                                <th class="sortStyle">Od<i class="fa fa-angle-down"></i></th>
                                <th class="sortStyle">Do<i class="fa fa-angle-down"></i></th>
                            </tr>
                            </thead>
                            <tbody>
                                {include file="dashboard/points_transfer_body.tpl"}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{BASE_URL}data/ed/js/jq.tablesort.js"></script>
<script>
    if ($('#sortable-table-1').length) {
        $('#sortable-table-1').tablesort();
    }

    products_table = $('#products-table').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": "{BASE_ED_URL}dashboard/products.html",
            "type": 'post',
            "data": function (d) {
                d.products_period = $('#products-period').val(),
                    d.products_customer = $('#products-customer').val(),
                    d.products_supervisor = $('#products-supervisor').val()
            }
        },
        "columnDefs": [
            {
                "orderable": false,
                "targets": [2, 8],
            },
            {
                "searchable": false,
                "targets": [2, 8],
            }
        ],
        "language": {
            "processing": "Przetwarzanie...",
            "search": "Szukaj:",
            "lengthMenu": "Pokaż _MENU_ pozycji",
            "info": "Pozycje od _START_ do _END_ z _TOTAL_ łącznie",
            "infoEmpty": "Pozycji 0 z 0 dostępnych",
            "infoFiltered": "(filtrowanie spośród _MAX_ dostępnych pozycji)",
            "infoPostFix": "",
            "loadingRecords": "Wczytywanie...",
            "zeroRecords": "Nie znaleziono pasujących pozycji",
            "emptyTable": "Brak danych",
            "paginate": {
                "first": "Pierwsza",
                "previous": "Poprzednia",
                "next": "Następna",
                "last": "Ostatnia"
            },
            "aria": {
                "sortAscending": ": aktywuj, by posortować kolumnę rosnąco",
                "sortDescending": ": aktywuj, by posortować kolumnę malejąco"
            }
        },
    });

    $(document).ready(function(){
        $.ajax({
            url: BASE_ED_URL + "dashboard/get_unactivated_company.html",
            method: "get",
            success: function(msg){
                $('#get_unactivated_company').html(msg);
            }
        });
    });



    $('#products-period, #products-customer, #products-supervisor').change(function () {
        products_table.search('').draw();
    });

    prizes_table = $('#prizes-table').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": "{BASE_ED_URL}dashboard/prizes.html",
            "type": 'post',
            "data": function (d) {
                d.products_period = $('#prizes-period').val(),
                    d.products_customer = $('#prizes-customer').val(),
                    d.products_supervisor = $('#prizes-supervisor').val()
            }
        },
        "columnDefs": [
            {
                "orderable": false,
                "targets": [3],
            },
            {
                "searchable": false,
                "targets": [3],
            }
        ],
        "language": {
            "processing": "Przetwarzanie...",
            "search": "Szukaj:",
            "lengthMenu": "Pokaż _MENU_ pozycji",
            "info": "Pozycje od _START_ do _END_ z _TOTAL_ łącznie",
            "infoEmpty": "Pozycji 0 z 0 dostępnych",
            "infoFiltered": "(filtrowanie spośród _MAX_ dostępnych pozycji)",
            "infoPostFix": "",
            "loadingRecords": "Wczytywanie...",
            "zeroRecords": "Nie znaleziono pasujących pozycji",
            "emptyTable": "Brak danych",
            "paginate": {
                "first": "Pierwsza",
                "previous": "Poprzednia",
                "next": "Następna",
                "last": "Ostatnia"
            },
            "aria": {
                "sortAscending": ": aktywuj, by posortować kolumnę rosnąco",
                "sortDescending": ": aktywuj, by posortować kolumnę malejąco"
            }
        },
    });

    $('#prizes-period, #prizes-customer, #prizes-supervisor').change(function () {
        prizes_table.search('').draw();
    });

    {literal}
        function top_five_filter() {
            var filter = $('.top_five_filter.active').data('type');

            if (filter) {
                $('#top-five-preloader').show();
                $.ajax({
                    url: BASE_ED_URL + 'dashboard/top_five.html',
                    type: 'POST',
                    data: {type: filter}
                }).done(function (response) {
                    $('#top-five-preloader').hide();
                    $('#top-five-container').html(response);
                });
            }
        }

        $('.top_five_filter').click(function (e) {
            e.preventDefault();
            $('.top_five_filter').removeClass('active');
            $(this).addClass('active');
            top_five_filter();
        });

        top_five_filter();

        function bottom_five_filter() {
            var filter = $('.bottom_five_filter.active').data('type');

            if (filter) {
                $('#bottom-five-preloader').show();
                $.ajax({
                    url: BASE_ED_URL + 'dashboard/bottom_five.html',
                    type: 'POST',
                    data: {type: filter}
                }).done(function (response) {
                    $('#bottom-five-preloader').hide();
                    $('#bottom-five-container').html(response);
                });
            }
        }

        $('.bottom_five_filter').click(function (e) {
            e.preventDefault();
            $('.bottom_five_filter').removeClass('active');
            $(this).addClass('active');
            bottom_five_filter();
        });

        bottom_five_filter();

        $('#points-client-select').change(function(){
            $('#points-preloader').show();
            $.ajax({
                url: BASE_ED_URL + 'dashboard/points.html',
                type: 'POST',
                data: {id_user: $('#points-client-select').val()}
            }).done(function (response) {
                $('#points-preloader').hide();
                $('#points-table tbody').html(response);
            });
        });
    {/literal}
</script>
