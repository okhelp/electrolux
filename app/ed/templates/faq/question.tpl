{load_plugin name="ckeditor"}

{if !empty($faq)}
    <form method="POST">
        <div class="form-group">
            <label>Pytanie</label>
            <input type="text" class="form-control" name="question" required="required"
                   value="{if !empty($faq->question)}{$faq->question}{/if}"/>
        </div>
        <div class="form-group">
            <label>Odpowiedź</label>
            <textarea type="text" class="ckeditor" name="answer" required="required">
                {if !empty($faq->answer)}{$faq->answer}{/if}
            </textarea>
        </div>
        <div class="form-group">
            <label>Status</label>
            <select name="status">
                <option value="0" {if empty($faq->status)}selected{/if}>Nieaktywne</option>
                <option value="1" {if !empty($faq->status)}selected{/if}>Aktywne</option>
            </select>
        </div>
        <div class="form-group">
            <div id="accordion">
                <div class="card">
                    <div class="card-header" id="headingOne">
                        <h5 class="mb-0">
                            <a class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                Wybór kategorii nadrzędnej
                            </a>
                        </h5>
                    </div>

                    <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                        <div class="card-body">
                            {$category_tree}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <button type="submit" name="save" value="1" class="btn btn-primary">Zapisz</button>
    </form>
{else}
    <div class="alert alert-info">
        Nie odnaleziono pytania
    </div>
{/if}