{load_plugin name="datatable"}
{load_plugin name="bootbox"}

<a href="{BASE_ED_URL}faq/ed.html" class="btn btn-info">Dodaj</a>

<table class='datatable'>
    <thead>
    <tr>
        <th>#id</th>
        <th>Pytanie</th>
        <th>Twórca</th>
        <th>Kategoria</th>
        <th>Status</th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    {foreach $faqs as $index => $faq_category}
        {foreach $faq_category['questions'] as $faq}
            <tr>
                <td>{$faq->id}</td>
                <td>{$faq->question}</td>
                <td>{$faq->id_creator}</td>
                <td>{$faqs[$index]['category']->name}</td>
                <td>
                    {if $faq->status == 1}
                        <span class="label label-success">aktywna</span>
                    {else}
                        <span class="label label-danger">nieaktywna</span>
                    {/if}
                </td>
                <td class="text-right">
                    <a href="{BASE_ED_URL}faq/ed.html?id={$faq->id}" class='btn btn-default'>edytuj</a>
                    <a href="{BASE_ED_URL}faq/usun.html?id={$faq->id}" class='btn btn-danger confirm'
                       data-confirm-text="Czy na pewno chcesz usunąć tą galerię?">usuń</a>
                </td>
            </tr>
        {/foreach}
    {/foreach}
    </tbody>
</table>
