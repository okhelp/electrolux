{if !empty($category)}
    <form method="POST">
        <div class="form-group">
            <label>Nazwa:</label>
            <input type="text" class="form-control" name="name" required="required"
                   value="{if !empty($category->name)}{$category->name}{/if}"/>
        </div>
        <input type="hidden" name="category_lang_id" value="{$category->id}">
        <button type="submit" name="save" value="1" class="btn btn-dafault">Zapisz</button>
    </form>
{else}
    <div class="alert alert-info">
        Nie odnaleziono kategorii
    </div>
{/if}