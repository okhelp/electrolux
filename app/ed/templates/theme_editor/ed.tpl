<style type="text/css">
	{literal}
		#files_list {border-top:1px solid #eee; margin-left: 6px; padding-top:6px; }
		#files_list li {padding: 4px 6px}
	{/literal}
</style>
<div class="row">
	<div class="col-md-3">
		<fieldset>
			<legend>Wybierz plik</legend>

			<div class="row" style="margin-bottom: 12px">
				<div class="col-md-12 text-center">
					<strong>Wybierz rodzaj plików</strong>: 
					<a href="{BASE_ED_URL}theme_editor/ed.html?theme={$theme}&mode=smarty"{if isset($mode) && $mode == 'smarty'} style="font-weight: bold"{/if}>smarty</a> /
					<a href="{BASE_ED_URL}theme_editor/ed.html?theme={$theme}&mode=javascript"{if isset($mode) && $mode == 'javascript'} style="font-weight: bold"{/if}>js</a> / 
					<a href="{BASE_ED_URL}theme_editor/ed.html?theme={$theme}&mode=css"{if isset($mode) && $mode == 'css'} style="font-weight: bold"{/if}>css</a> 
				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
					{if isset($file_list)}
						
						<ul id="files_list">
							{foreach from=$file_list key=file_path item=file_name}
								<li>
									&raquo;	<a href="{BASE_ED_URL}theme_editor/ed.html?theme={$theme}&mode={$mode}&file={$file_path}"{if $file === $file_path|unescape:'url'} style="font-weight:bold"{/if}>{$file_name}</a>
								</li>
							{/foreach}
						</ul>
					{/if}
				</div>
			</div>

		</fieldset>
	</div>
				
				
	<div class="col-md-9">
		
		{if $file && !empty($file_data)}
			<form id="theme_editor_form" method="POST" action="{BASE_ED_URL}theme_editor/save.html">
				<div class="row">
					<div class="col-md-12">
						<fieldset>
							<legend>Edytuj plik: {$file_name}</legend>
							<pre id="editor">{$file_data}</pre>
						</fieldset>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<button type="button" class="btn btn-success">Zapisz</button>
					</div>
				</div>
				<input type="hidden" name="file_data" />
				<input type="hidden" name="file_path" value="{$file}" />
			</form>
		{else}
			<div class="row">
				<div class="col-md-12">
					<div class="alert alert-info">
						{if !$mode}
							Proszę wybrać rodzaj pliku.
						{else}
							Proszę wybrać plik do edycji.
						{/if}
					</div>
				</div>
			</div>
		{/if}

	</div>
</div>


{* ładuję edytor *}
{if $mode}
	{load_plugin name="theme_editor" mode="{$mode}"}
{/if}

{* zapisuję dane z textarea *}
<script type="text/javascript">
	{literal}
		$(document).ready(function() 
		{
			$('#theme_editor_form button').click(function(){
				var file_data = editor.getValue();
				$(this).closest('form').find('input[name=file_data]').val(file_data);
				$(this).closest('form').submit();
			})
		})
	{/literal}
</script>