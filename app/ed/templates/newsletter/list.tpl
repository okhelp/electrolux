{load_plugin name="datatable"}

<a href="{BASE_ED_URL}newsletter/ed.html?id=0" class="btn btn-info">Dodaj</a>

<table class='datatable'>
    <thead>
    <tr>
        <th>#id</th>
        <th>Nazwa</th>
        <th>Utworzył</th>
        <th>Data wysyłki</th>
        <th>Liczba odbiorców</th>
        <th>Status</th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    {foreach $list as $list_element}
        <tr>
            <td>{$list_element->id}</td>
            <td>{$list_element->name}</td>
            <td>{$list_element->user->name} {$list_element->user->surname}</td>
            <td>{$list_element->send_date->format('Y-m-d')}</td>
            <td>{$list_element->recipients_count}</td>
            <td>
                {if $list_element->status == 1}
                    <span class="label label-success">aktywny</span>
                {else}
                    <span class="label label-danger">nieaktywny</span>
                {/if}
            </td>
            <td class="text-right">
                <a href="{BASE_ED_URL}newsletter/ed.html?id={$list_element->id}" class='btn btn-default'>edytuj</a>
            </td>
        </tr>
    {/foreach}
    </tbody>
</table>
