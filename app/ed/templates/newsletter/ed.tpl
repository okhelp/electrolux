{load_plugin name="ckeditor"}
{load_plugin name="datepicker"}

<form action="">
    <div class="custom_form__input">
        <input class="custom_form__input--input {if !empty($newsletter->name)}has-val{/if}"
               type="text"
               id="status_name"
               name="name"
               value="{if !empty($newsletter->name)}{$newsletter->name}{/if}">
        <span class="custom_form__input--placeHolder" data-placeholder="Nazwa"></span>
    </div>
    <div class="custom_form__input">
        <input class="custom_form__input--input  {if !empty($newsletter->mail_title)}has-val{/if}"
               type="text"
               id="mail-title"
               name="mail_title"
               value="{if !empty($newsletter->mail_title)}{$newsletter->mail_title}{/if}">
        <span class="custom_form__input--placeHolder" data-placeholder="Tytuł maila"></span>
    </div>
    <div class="form-group mt-3">
        <label>Treść</label>
        <textarea name="text" class="ckeditor">{if !empty($newsletter->text)}{$newsletter->text}{/if}</textarea>
    </div>
    <div class="form-group mt-3">
        <label>Data wysyłki</label>
        <input type="text" name="send_date" value="{if !empty($newsletter->send_date)}{$newsletter->send_date->format('Y-m-d')}{/if}" class="form-control datepicker" required="required" readonly="readonly" />
    </div>
    <div class="form-group mt-3">
        <label>Status</label>
        <select name="status">
            <option value="0" {if empty($newsletter->status)}selected{/if}>Nieaktywny</option>
            <option value="1" {if !empty($newsletter->status)}selected{/if}>Aktywny</option>
        </select>
    </div>
    <div class="form-check mt-3">
        <input id="send-email-checkbox" type="checkbox" class="form-check-input" name="send_to_admin" value="1" {if !empty($newsletter->send_to_admin)}checked{/if}>
        <label class="form-check-label">Wyślij do kont nadrzędnych</label>
    </div>
    <div class="form-check">
        <input id="send-email-checkbox" type="checkbox" class="form-check-input" name="send_to_employee" value="1" {if !empty($newsletter->send_to_employee)}checked{/if}>
        <label class="form-check-label">Wyślij do pracowników</label>
    </div>
    <button type="submit" formmethod="post" name="submit" value="1" class="btn btn-primary mt-3">Zapisz</button>
</form>