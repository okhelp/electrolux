<p>
	<a href="#" class="btn btn-info add_main_menu">+ dodaj menu</a>
</p>

{$list}

<script type="text/javascript">
	{literal}

		function add_modal(id_parent)
		{
			show_modal_ajax('dodaj pozycję', BASE_ED_URL + 'block_menu/ed.html?id_parent=' + id_parent + '&type={/literal}{$type}{literal}');
		}

		function edit_modal(id)
		{
			show_modal_ajax('edytuj pozycję', BASE_ED_URL + 'block_menu/ed.html?id=' + id + '&type={/literal}{$type}{literal}');
		}

		$(document).ready(function ()
		{
			//dodawanie menu z przysku na górze (zawsze id_parent = 0)
			$('.add_main_menu').click(function (e)
			{
				e.preventDefault();
				add_modal(0);
			});

			//dodawanie
			$('.add').click(function (e)
			{
				e.preventDefault();
				var id_parent = $(this).closest('li').attr('data-id');
				add_modal(id_parent);
			});

			//edycja
			$('.edit').click(function ()
			{
				var id = $(this).closest('li').attr('data-id');
				edit_modal(id);
			});

			//sortowanie
			$(".tree ul").sortable({
				handle: '.move',
				stop: function (event, ui)
				{
					var ids = new Array;
					var id_parent = ui.item.attr('data-id_parent');
					$('li[data-id_parent=' + id_parent + ']').each(function ()
					{
						ids.push($(this).attr('data-id'));
					})
					
					//wysyłam ajaxa
					$.ajax({
						type     : "POST",
						url      : BASE_ED_URL + "block_menu/ajax_zmien_kolejnosc.html",
						data     : "ids=" + ids,
						success : function(msg) {

						},
						complete : function(r) {
							$.notify('Kolejność została zmieniona.');
						}
					});
				}
			});
		})
	{/literal}
</script>