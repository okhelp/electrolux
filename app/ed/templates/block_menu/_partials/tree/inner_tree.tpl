<ul>
    {foreach from=$data item=d}
        <li data-id_parent="{$d.id_parent}" data-id="{$d.id}">
            <span>{$d.name}</span>
			&nbsp;
			<a href="#" class="move"><i class="fa fa-arrows"></i></a>
			&nbsp;
			<a href="#" class="add"><i class="fa fa-plus"></i></a>
			&nbsp;
			<a href="#" class="edit"><i class="fa fa-pencil"></i></a>
			&nbsp;
			<a href="{BASE_ED_URL}block_menu/usun.html?id={$d.id}" class='confirm' data-confirm-text='Czy napewno chcesz usunąć to menu?'><i class="fa fa-trash-o"></i></a>
				{if !empty($d.children)}
					{display_tree data=$d.children template = 'block_menu/_partials/tree/inner_tree.tpl'}
				{/if}
        </li>
    {/foreach}
</ul>

