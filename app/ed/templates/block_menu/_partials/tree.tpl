{load_plugin name="tree"}

{if $list}
    <div class="tree well">
        {display_tree data=$list template = 'block_menu/_partials/tree/inner_tree.tpl'}
    </div>
{else}
    <div class="alert alert-warning">
        Brak dostępnego menu
    </div>
{/if}
