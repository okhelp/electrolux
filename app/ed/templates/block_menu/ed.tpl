<form method="POST">
	<div class="form-group">
		<label>Nazwa:</label>
		<input type="text" name="name" value="{if $data && $data->name}{$data->name}{/if}" class="form-control" required="required" />
	</div>
	<div class="form-group">
		<label>Adres URL:</label>
		<input type="text" name="url" value="{if $data && $data->url}{$data->url}{/if}" class="form-control" required="required" />
	</div>
	<div class="form-group">
		<input type="hidden" name="id" value="{$id}" />
		<input type="hidden" name="id_parent" value="{$id_parent}" />
		<input type="hidden" name="type" value="{$type}" />
		<button type="submit" class="btn btn-submit">Zapisz</button>
	</div>
</form>