{load_plugin name="datatable"}

<table class='datatable'>
    <thead>
    <tr>
        <th>Klient</th>
        <th>Firma</th>
        <th>NIP (login)</th>
        <th>Ilość punktów</th>
        <th>Ilość extra punktów</th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    {foreach $subordinates_points as $subordinate}
        <tr>
            <td>{$subordinate->name} {$subordinate->surname}</td>
            <td>{$subordinate->company->name}</td>
            <td>{$subordinate->login}</td>
            <td>{if !empty($subordinate->points)}{$subordinate->points}{else}0{/if}</td>
            <td>{if !empty($subordinate->extra_points)}{$subordinate->extra_points}{else}0{/if}</td>
            <td class="text-center">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-id-subordinate="{$subordinate->id}" data-target="#my-modal">
                    Przypisz E-LUXY
                </button>
            </td>
        </tr>
    {/foreach}
    </tbody>
</table>

<div class="modal" id="my-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{BASE_ED_URL}company_supervising/assign_extra_points/assign.html" method="post">
                <div class="modal-header">
                    <h5 class="modal-title">Przypisywanie extra E-LUX</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Zamknij">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Ilość dodanych punktów</label>
                        <input class="form-control" type="number" value="0" min="0" name="extra_points">
                    </div>
                    <input type="hidden" name="subordinate_id" value="">
                </div>
                <div class="modal-footer">
                    <button type="submit" formmethod="post" class="btn btn-primary">Dodaj</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Anuluj</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    $('#my-modal').on('show.bs.modal', function(e) {
        var idSubordinate = $(e.relatedTarget).data('id-subordinate');

        $(e.currentTarget).find('input[name="subordinate_id"]').val(idSubordinate);
    });
</script>

