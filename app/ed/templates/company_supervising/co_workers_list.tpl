{load_plugin name="datatable"}

<table id='datatable'>
    <thead>
        <tr>
            <th>Współpracownik</th>
            <th>Firma</th>
            <th>Status</th>
            <th>Data_dodania</th>
            <th>Data_modyfikacji</th>
            <th></th>
        </tr>
    </thead>
    <tbody></tbody>
</table>

<script>
    $(document).ready(function () {
        table = $('#datatable').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": "{BASE_ED_URL}company_supervising/get_co_workers_list.html",
                "type": 'post',
            },
            "columnDefs": [
                {
                    "orderable": false,
                    "targets": [5],
                },
                {
                    "searchable": false,
                    "targets": [3,4,5],
                }
            ],
            "order": [[ 3, "desc" ]],
            "language": {
                "processing": "Przetwarzanie...",
                "search": "Szukaj:",
                "lengthMenu": "Pokaż _MENU_ pozycji",
                "info": "Pozycje od _START_ do _END_ z _TOTAL_ łącznie",
                "infoEmpty": "Pozycji 0 z 0 dostępnych",
                "infoFiltered": "(filtrowanie spośród _MAX_ dostępnych pozycji)",
                "infoPostFix": "",
                "loadingRecords": "Wczytywanie...",
                "zeroRecords": "Nie znaleziono pasujących pozycji",
                "emptyTable": "Brak danych",
                "paginate": {
                    "first": "Pierwsza",
                    "previous": "Poprzednia",
                    "next": "Następna",
                    "last": "Ostatnia"
                },
                "aria": {
                    "sortAscending": ": aktywuj, by posortować kolumnę rosnąco",
                    "sortDescending": ": aktywuj, by posortować kolumnę malejąco"
                }
            },
        });
    });
</script>
