{load_plugin name="datatable"}
{load_plugin name="datepicker"}
<form>
    <div class="row">
        <div class="col-6 col-xl">
            <div class="form-group">
                <label>Klient</label>
                <input id="customer" type="text" name="verification_firm" class="form-control form-control-sm" placeholder="">
            </div>
        </div>
        <div class="col-6 col-xl">
            <div class="form-group">
                <label>Numer faktury</label>
                <input id="invoice_number" type="text" name="invoice_number" class="form-control form-control-sm" placeholder="">
            </div>
        </div>
        <div class="col-6 col-xl">
            <div class="form-group">
                <label>Data od</label>
                <input id="date-from" type="text" name="verification_date_from" class="form-control form-control-sm datepicker" autocomplete="off" placeholder="">
            </div>
        </div>
        <div class="col-6 col-xl">
            <div class="form-group">
                <label>Data do</label>
                <input id="date-to" type="text" name="verification_date_to" class="form-control form-control-sm datepicker" autocomplete="off" placeholder="">
            </div>
        </div>
        <div class="col-6 col-xl">
            <div class="form-group">
                <label>Opiekun</label>
                <input type="text"
                       name="verification_guardian"
                       class="form-control form-control-sm"
                       placeholder=""
                       {if !$is_admin}disabled{/if}
                       id="administrator">
            </div>
        </div>
        <div class="col-6 col-xl">
            <label>Status</label>
            <select class="form-control" name="status" id="status">
                <option value="0">do weryfikacji</option>
                <option value="2">potwierdzone</option>
                <option value="1">odrzucone</option>
            </select>
        </div>
    </div>
    <div class="row">
        <div class="col-12 text-right">
            <button type="submit" class="btn btn-primary btn-block ml-auto" style="width: 100px">Filtruj</button>
        </div>
    </div>
</form>
<table id='datatable'>
    <thead>
    <tr>
        <th>Klient</th>
        <th>Numer faktury</th>
        <th>Model</th>
        <th>Cena za szt.</th>
        <th>Ilość</th>
        <th>Cena łącznie</th>
        <th>E-LUXY</th>
        <th>Data dodania</th>
        <th>Status</th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    </tbody>
</table>

<script>
    $(document).ready(function () {
        table = $('#datatable').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": "{BASE_ED_URL}company_supervising/get_subordinates_sale.html",
                "type": 'post',
                "data": function (d) {
                    d.customer = $('#customer').val(),
                    d.invoice_number = $('#invoice_number').val(),
                    d.date_from = $('#date-from').val(),
                    d.date_to = $('#date-to').val(),
                    d.administrator = $('#administrator').val(),
                    d.status = $('#status').val()
                }
            },
            "columnDefs": [
                {
                    "orderable": false,
                    "targets": [7],
                },
                {
                    "searchable": false,
                    "targets": [6, 7],
                }
            ],
            "language" : {
                "processing": "Przetwarzanie...",
                "search": "Szukaj:",
                "lengthMenu": "Pokaż _MENU_ pozycji",
                "info": "Pozycje od _START_ do _END_ z _TOTAL_ łącznie",
                "infoEmpty": "Pozycji 0 z 0 dostępnych",
                "infoFiltered": "(filtrowanie spośród _MAX_ dostępnych pozycji)",
                "infoPostFix": "",
                "loadingRecords": "Wczytywanie...",
                "zeroRecords": "Nie znaleziono pasujących pozycji",
                "emptyTable": "Brak danych",
                "paginate": {
                    "first": "Pierwsza",
                    "previous": "Poprzednia",
                    "next": "Następna",
                    "last": "Ostatnia"
                },
                "aria": {
                    "sortAscending": ": aktywuj, by posortować kolumnę rosnąco",
                    "sortDescending": ": aktywuj, by posortować kolumnę malejąco"
                }
            },
        });

        $('button[type=submit]').click(function (e) {
            e.preventDefault();
            table.search('').draw();
        });
    });
</script>
