
{*		{intval($params.all_pages)|@var_dump}*}
{*		{$params.current_page|@var_dump}*}
{if $params.all_pages > 1}

	{*{assign var=packages value=round($params.all_pages/5)}*}
	{*{$packages|@var_dump}*}
<nav>
	<ul class="pagination justify-content-center">
		{if $params.current_page > 1}
			<li class="page-item">
				<a class="page-link" data-page="{$params.current_page-1}" href="{set_get_arg name=$params.prefix value=$params.current_page-1}" aria-label="Poprzednia">
					Poprzednia
				</a>
			</li>
		{/if}

		{if $params.all_pages <= 5}
			{assign var=start_page value=1}
			{assign var=stop_page value=$params.all_pages}
			{*** pierwsze 5 stron ***}
		{elseif $params.current_page <= 3}
			{assign var=start_page value=1}
			{assign var=stop_page value=5}

			{** ostatnie 5 stron**}
		{elseif $params.current_page > $params.all_pages - 5 }
			{assign var=max_page value = 4 - ($params.all_pages - $params.current_page)}
			{assign var=start_page value=$params.current_page - $max_page}
			{assign var=stop_page value=$params.all_pages}
			{* strony środkowe*}
		{else}
			{assign var=start_page value=$params.current_page - 3}
			{assign var=stop_page value=$params.current_page + 2}
		{/if}



		{for $i=$start_page to $stop_page}
			<li class="page-item{if $i == $params.current_page} active{/if}">
				<a class="page-link" data-page="{$i}" href="{set_get_arg name=$params.prefix value=$i}">{$i}</a>
			</li>
		{/for}

		{if $params.current_page != $params.all_pages}
			<li class="page-item">
				<a class="page-link" data-page="{$params.current_page+1}" href="{set_get_arg name=$params.prefix value=$params.current_page+1}" aria-label="Następna">
					Następna
				</a>
			</li>
		{/if}
	</ul>
</nav>
{/if}



