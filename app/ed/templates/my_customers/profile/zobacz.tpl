{load_plugin name="datatable"}

<div class="row">
    <div class="col-md-3">

        <div class="card">
            <div class="card-body">

                <div class="row">
                    <div class="col-md-12">
                        <div class="text-center border-bottom">
                            {if !empty($user_data->id_cimg)}
                                <img class="img-lg rounded-circle mb-3"
                                     src="{img id_cimg={$user_data->id_cimg} height=128 width=128}"/>
                            {/if}

                            <h3>{$user_data->name} {$user_data->surname}</h3>

                            <p>
                                konto {if $company->user->role == 'admin'}premium{else}zwykłe{/if}</p>
                            </p>

                            <p>
                                <small>
                                    {if $company->user->role == 'admin'}premium{else}klient{/if} |  w klubie od {$user_data->ts->format('Y')}
                                </small>
                            </p>
                            {* dane do logowania - widoczne tylko dla adminów *}
                            {if App__Ed__Model__Acl::has_group('admin-electrolux') || App__Ed__Model__Acl::has_group('premium-user')}
                                <p>
                                    <a href="#" class="btn btn-success" data-toggle="modal" data-target="#exampleModalCenter">pokaż dane do logowania</a>
                                </p>
                                <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLongTitle">Dane do logowania: {$user_data->name} {$user_data->surname}</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <table class="table table-bordered">
                                                <tr>
                                                    <td width="50%" style="background: #fafafa; font-weight:bold!important"><b>Login:</b></td>
                                                    <td width="50%">{$user_data->login}</td>
                                                </tr>
                                                <tr>
                                                    <td width="50%" style="background: #fafafa; font-weight:bold!important"><b>Hasło:</b></td>
                                                    <td width="50%">{App__Ed__Model__Encryption::decode($user_data->password)}</td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Zamknij</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {/if}
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="border-bottom py-4">
                            <h5 style="text-transform: uppercase">Dane firmy</h5>
                            <p>
                                {$company->name} <br />
                                NIP {$company->nip}
                            </p>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="border-bottom py-4">
                            <h5 style="text-transform: uppercase">HANDLOWIEC</h5>
                            <p>
                                {$supervisor->name} {$supervisor->surname}
                            </p>
                            <p>
                                E-mail: {$supervisor->email} <br />
                                Telefon: {$supervisor->telephone}
                            </p>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>

    <div class="col-md-9">

        <div class="row">
            <div class="col-12 grid-margin">
                <div class="card">
                    <div class="card-body">
                        <div class="d-md-flex justify-content-between align-items-center">
                            {assign var="achieved_eluxes" value="{if empty($users_e_lux[$id_user])}0{else}{$users_e_lux[$id_user]->points}{/if}"}
                            {assign var="spent_eluxes" value="{if empty($user_spend_e_lux)}0{else}{$user_spend_e_lux}{/if}"}
                            {assign var="extra_eluxes" value="{if empty($users_e_lux[$id_user])}0{else}{$users_e_lux[$id_user]->extra_points}{/if}"}

                            <div class="d-flex align-items-center mb-3 mb-md-0">
                                <button class="btn btn-social-icon btn-google btn-rounded">
                                    <i class="fa fa-ellipsis-v"></i>
                                </button>
                                <div class="ml-4">
                                    <h5 class="mb-0">ZDOBYTE E-LUXY</h5>
                                    <p class="mb-0">{$achieved_eluxes} E-LUX</p>
                                </div>
                            </div>
                            <div class="d-flex align-items-center mb-3 mb-md-0">
                                <button class="btn btn-social-icon btn-google btn-rounded">
                                    <i class="fa fa-ellipsis-v"></i>
                                </button>
                                <div class="ml-4">
                                    <h5 class="mb-0">WYDANE E-LUXY</h5>
                                    <p class="mb-0">{$spent_eluxes} E-LUX</p>
                                </div>
                            </div>
                            <div class="d-flex align-items-center mb-3 mb-md-0">
                                <button class="btn btn-social-icon btn-google btn-rounded">
                                    <i class="fa fa-ellipsis-v"></i>
                                </button>
                                <div class="ml-4">
                                    <h5 class="mb-0">ZDOBYTE EXTRA E-LUXY</h5>
                                    <p class="mb-0">{$extra_eluxes} E-LUX</p>
                                </div>
                            </div>
                            <div class="d-flex align-items-center">
                                <button class="btn btn-social-icon btn-google btn-rounded">
                                    <i class="fa  fa-ellipsis-v"></i>
                                </button>
                                <div class="ml-4">
                                    <h5 class="mb-0">ROCZNE E-LUXY</h5>
                                    <p class="mb-0">{$annual_eluxes} E-LUX</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">

                <div class="card">
                    <div class="card-body">

                        <h4 class="card-title">ADRESY</h4>
                        <div class="row">
                            {if !empty($addresses)}
                                {foreach $addresses as $address}
                                    <div class="col-md-4">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h3>{$address->name}</h3>
                                                <p>
                                                    {$address->street} {$address->street_number}{if !empty($address->apartment_number)}/{$address->apartment_number}{/if} <br />
                                                    {$address->post_code} {$address->city}
                                                </p>
                                                <p>
                                                    <small>
                                                        <strong>Data utworzenia</strong>: {$address->ts->format('Y-m-d H:i:s')}
                                                    </small>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                {/foreach}
                            {else}
                                <div class="col-md-12">
                                    <div class="alert alert-info">Brak zdefiniowanych adresów</div>
                                </div>
                            {/if}
                        </div>

                    </div>
                </div>

            </div>
        </div>

    </div>

</div>



<div class="row">
    <div class="col-md-12">

        <div class="card">
            <div class="card-body">

                <h4 class="card-title">KONTA WSPÓŁPRACOWNIKÓW</h4>
                {if !empty($company_users) && count($company_users) > 1}

                    <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>
                                #ID
                            </th>
                            <th>
                                IMIĘ I NAZWISKO
                            </th>
                            <th>
                                LOGIN
                            </th>
                            <th>
                                E-LUXY
                            </th>
                            <th>
                                DATA UTWORZENIA
                            </th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                            {foreach from=$company_users item=user}
                                {if $user->id_user != $id_user}
                                    <tr>
                                        <td>
                                            {$user->user->id}
                                        </td>
                                        <td>
                                            {$user->user->name} {$user->user->surname}
                                        </td>
                                        <td>
                                            {$user->user->login}
                                        </td>
                                        <td>
                                            {if empty($users_e_lux[$user->user->id])}0{else}{$users_e_lux[$user->user->id]->points}{/if}
                                        </td>
                                        <td>
                                            {$user->user->ts->format('Y-m-s H:i:s')}
                                        </td>
                                        <td class="text-center">
                                            <a href="{BASE_ED_URL}my_customers/profile/zobacz.html?id={$user->user->id}" class="btn btn-primary mb-2">zobacz profil</a>
                                        </td>
                                    </tr>
                                {/if}
                            {/foreach}
                        </tbody>
                    </table>
                </div>
                {else}
                    <div class="alert alert-info">Brak utworzonych kont współpracowników.</div>
                {/if}
            </div>
        </div>

    </div>
</div>


<div class="row">
    <div class="col-md-12">

        <div class="card">
            <div class="card-body">
                <h3 class="card-title">HISTORIA SPRZEDAŻY</h3>

                {if !empty($order_history)}
                    <div class="row">

                        <div class="col-12">
                            {if !empty($order_history)}
                                <table id="user_panel__table" class="datatable user_panel__table table nowrap" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th>Numer faktury</th>
                                        <th>Model</th>
                                        <th>Data rejestracji</th>
                                        <th>PNC</th>
                                        <th>Cena jedn.</th>
                                        <th>Ilość</th>
                                        <th>Wartość</th>
                                        <th>E-LUXY</th>
                                        <th>Status</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        {foreach $order_history as $order}
                                            <tr class="{if $order->status == 0}user_panel__table--gray{/if}{if $order->status == 1}user_panel__table--red{/if}{if $order->status == 2}user_panel__table--blue{/if}">
                                                <td>{if $order->invoice_number}{$order->invoice_number}{else}-{/if}</td>
                                                <td>{if !empty($products[$order->id_product])}{$products[$order->id_product]->model}{/if}</td>
                                                <td>{$order->created_at->format('Y-m-d H:i:s')}</td>
                                                <td>{if !empty($products[$order->id_product])}{$products[$order->id_product]->product_code}{/if}</td>
                                                <td>{$order->price} PLN</td>
                                                <td>{$order->count}</td>
                                                <td>{math equation="x * y" x={$order->price} y={$order->count} format="%.2f"} PLN</td>
                                                <td>{$order->points} E-LUX</td>
                                                <td>{if $order->status == 0}Weryfikacja{/if}{if $order->status == 1}Odrzucone{/if}{if $order->status == 2}Zatwierdzone{/if}</td>
                                            </tr>
                                        {/foreach}
                                    </tbody>
                                    <tfoot>
                                    </tfoot>
                                </table>
                            {else}
                                <div class="alert alert-info">Nie zarejestrowano potwierdzonych zamówień</div>
                            {/if}
                        </div>
                    </div>
                {else}
                    <div class="alert alert-info">Użytkownik nie posiada historii sprzedaży.</div>
                {/if}
            </div>
        </div>




    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">


                <h3 class="card-title">HISTORIA ZAMÓWIEŃ</h3>

                <div class="row">
                    <div class="col-12">
                        <table id="user_panel__table_order" class="table nowrap" style="width:100%">
                            <thead>
                            <tr>
                                <th>Numer zamówienia</th>
                                <th>Data</th>
                                <th>Model</th>
                                <th>PNC</th>
                                <th>Cena jedn.</th>
                                <th>Ilość</th>
                                <th>Wartość</th>
                                <th>E-LUXY</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                    </div>
                </div>

                <script>
                    $('[data-toggle="tabajax"]').click(function (e) {
                        var $this = $(this),
                            loadurl = $this.attr('href'),
                            targ = $this.attr('data-target');
                        $('[data-toggle="tabajax"]').removeClass('active');
                        $this.addClass('active');

                        $.post(loadurl, function (data) {
                            $(targ).html(data);
                        });
                        // $.ajax({
                        //    type: "POST"
                        //    url: loadurl,
                        //    data: data,
                        // }).done(function(data) {
                        //     $(targ).html(data)});

                        return false;
                    });

                    $(document).ready(function () {
                        $('#user_panel__table_order').DataTable({
                            "processing": true,
                            "serverSide": true,
                            "ajax": {
                                "url": "{BASE_URL}panel/get_sale_history.html",
                                "type": 'post',
                                "data": function (d) {
                                    d.records_total = "200";
                                    d.user_id = {$user_data->id};
                                }
                            },
                            "language" : {
                                "processing": "Przetwarzanie...",
                                "search": "Szukaj:",
                                "lengthMenu": "Pokaż _MENU_ pozycji",
                                "info": "Pozycje od _START_ do _END_ z _TOTAL_ łącznie",
                                "infoEmpty": "Pozycji 0 z 0 dostępnych",
                                "infoFiltered": "(filtrowanie spośród _MAX_ dostępnych pozycji)",
                                "infoPostFix": "",
                                "loadingRecords": "Wczytywanie...",
                                "zeroRecords": "Nie znaleziono pasujących pozycji",
                                "emptyTable": "Brak danych",
                                "paginate": {
                                    "first": "Pierwsza",
                                    "previous": "Poprzednia",
                                    "next": "Następna",
                                    "last": "Ostatnia"
                                },
                                "aria": {
                                    "sortAscending": ": aktywuj, by posortować kolumnę rosnąco",
                                    "sortDescending": ": aktywuj, by posortować kolumnę malejąco"
                                }
                            },
                        });
                    });
                </script>

            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">

                <h3 class="card-title">NAGRODY</h3>

                <div class="row">
                    <div class="col-12">
                        <table id="user_panel__table_prizes" class="table nowrap" style="width:100%">
                            <thead>
                            <tr>
                                <th>Numer zamówienia</th>
                                <th>Data</th>
                                <th>Produkt</th>
                                <th>E-lux jedn.</th>
                                <th>Ilość</th>
                                <th>E-Lux wartość</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                    </div>
                </div>

                <script>
                    $(document).ready(function () {
                        $('#user_panel__table_prizes').DataTable({
                            "processing": true,
                            "serverSide": true,
                            "ajax": {
                                "url": "{BASE_URL}panel/get_user_prices.html",
                                "type": 'post',
                                "data": function (d) {
                                    d.records_total = "{count($user_prizes)}";
                                    d.user_id = {$user_data->id};
                                }
                            },
                            "columnDefs": [
                                {
                                    "orderable": false,
                                    "targets": [2,3,4,5],
                                }
                            ],
                            "language" : {
                                "processing": "Przetwarzanie...",
                                "search": "Szukaj:",
                                "lengthMenu": "Pokaż _MENU_ pozycji",
                                "info": "Pozycje od _START_ do _END_ z _TOTAL_ łącznie",
                                "infoEmpty": "Pozycji 0 z 0 dostępnych",
                                "infoFiltered": "(filtrowanie spośród _MAX_ dostępnych pozycji)",
                                "infoPostFix": "",
                                "loadingRecords": "Wczytywanie...",
                                "zeroRecords": "Nie znaleziono pasujących pozycji",
                                "emptyTable": "Brak danych",
                                "paginate": {
                                    "first": "Pierwsza",
                                    "previous": "Poprzednia",
                                    "next": "Następna",
                                    "last": "Ostatnia"
                                },
                                "aria": {
                                    "sortAscending": ": aktywuj, by posortować kolumnę rosnąco",
                                    "sortDescending": ": aktywuj, by posortować kolumnę malejąco"
                                }
                            },
                        });
                    });
                </script>

            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h3 class="card-title">TRANSFER PUNKTÓW</h3>

                {if !empty($points_log)}

                    <table class="datatable">
                        <thead>
                            <th>KTO</th>
                            <th>KOMU</th>
                            <th>ILE</th>
                            <th>DATA</th>
                        </thead>
                        <tbody>
                            {foreach from=$points_log item=pl}
                                <tr>
                                    <td>{$pl->id_sender|id2name}</td>
                                    <td>{$pl->id_receiver|id2name}</td>
                                    <td>{({$pl->sender_points_before} - {$pl->sender_points_after})}</td>
                                    <td>{$pl->created_at->format('Y-m-d H:i:s')}</td>
                                </tr>
                            {/foreach}
                        </tbody>
                    </table>

                {else}
                    <div class="alert alert-info">Brak danych.</div>
                {/if}

            </div>
        </div>
    </div>
</div>