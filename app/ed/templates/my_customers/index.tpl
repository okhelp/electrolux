{load_plugin name="datatable"}

<table id="datatable">
    <thead>
    <tr>
        <th>ID</th>
        <th>Imię</th>
        <th>Nazwisko</th>
        <th>Firma</th>
        <th>Zdobyte E-LUXY</th>
        <th>Wydane E-LUXY</th>
        <th>Zdobyte extra E-LUXY</th>
        <th>Roczne E-LUXY</th>
        <th>Data dodania</th>
        <th>Data modyfikacji</th>
        <th>Akcja</th>
    </tr>
    </thead>
    <tbody></tbody>
</table>

<script>
  $(document).ready(function () {
    $('#datatable').DataTable({
      "processing": true,
      "serverSide": true,
      "ajax": {
        "url": "{BASE_ED_URL}my_customers/get_customers_list.html",
        "type": 'post',
      },
      "columnDefs": [
        {
          "orderable": false,
          "targets": [5],
        },
        {
          "searchable": false,
          "targets": [3,4,5],
        }
      ],
      "order": [[ 3, "desc" ]],
      "language": {
        "processing": "Przetwarzanie...",
        "search": "Szukaj:",
        "lengthMenu": "Pokaż _MENU_ pozycji",
        "info": "Pozycje od _START_ do _END_ z _TOTAL_ łącznie",
        "infoEmpty": "Pozycji 0 z 0 dostępnych",
        "infoFiltered": "(filtrowanie spośród _MAX_ dostępnych pozycji)",
        "infoPostFix": "",
        "loadingRecords": "Wczytywanie...",
        "zeroRecords": "Nie znaleziono pasujących pozycji",
        "emptyTable": "Brak danych",
        "paginate": {
          "first": "Pierwsza",
          "previous": "Poprzednia",
          "next": "Następna",
          "last": "Ostatnia"
        },
        "aria": {
          "sortAscending": ": aktywuj, by posortować kolumnę rosnąco",
          "sortDescending": ": aktywuj, by posortować kolumnę malejąco"
        }
      },
    });
  });
</script>

{*{load_plugin name="jstree"}*}

{*<style type="text/css">*}
{*    {literal}*}
{*        .click_show_profile {font-weight: bold; pointer-events: auto!important;};*}
{*    {/literal}*}
{*</style>*}

{*{if !empty($tree)}*}
{*<div class="row">*}
{*    <div class="col-md-4 col-sm-8 col-xs-8">*}
{*        <input type="text" value="" id="demo_q" placeholder="Wyszukaj">*}
{*    </div>*}
{*</div>*}
{*<div class="row" style="margin-top: 15px; margin-left: 5px;">*}
{*    <div id="jst"></div>*}
{*</div>*}
{*<script>*}
{*    {literal}*}
{*    $(function () {*}
{*        $('#jst').jstree({*}
{*            'core': {*}
{*                'data': {/literal}{$tree}{literal},*}
{*                check_callback: true,*}
{*            },*}
{*            "types": {*}
{*                "2": {"icon": "mdi mdi-account text-danger", "valid_children": []},*}
{*                "1": {"icon": "mdi mdi-account text-primary", "valid_children": []},*}
{*                "3": {"icon": "mdi mdi-label text-info", "valid_children": []},*}
{*            },*}
{*            "plugins": ["search", "state", "types", "wholerow"]*}
{*        });*}

{*        $('#jst').on('ready.jstree', function () {*}
{*            $('body').on( 'click', '.click_show_profile', function(e){*}
{*                e.preventDefault();*}
{*                window.location.href = $(this).attr('data-href');*}
{*            })*}
{*        });*}



{*    });*}

{*    $(function () {*}
{*        let to = false;*}
{*        $('#demo_q').keyup(function () {*}
{*            if (to) {*}
{*                clearTimeout(to);*}
{*            }*}
{*            to = setTimeout(function () {*}
{*                let v = $('#demo_q').val();*}
{*                $('#jst').jstree(true).search(v);*}
{*            }, 250);*}
{*        });*}

{*    });*}
{*    {/literal}*}
{*</script>*}
{*{else}*}
{*    <div class="alert alert-warning">*}
{*        Brak przypisanych klientów.*}
{*    </div>*}
{*{/if}*}
