{if $params.all_pages > 0}
    <ul class="pagination">
        {if $params.current_page > 1}
            <li class="page-item">
                <a class="page-link" href="{set_get_arg name=$params.prefix value=$params.current_page-1}" aria-label="Poprzednia">
                    <span aria-hidden="true">&laquo;</span>
                    <span class="sr-only">Poprzednia</span>
                </a>
            </li>
        {/if}

        {for $i=1 to $params.all_pages}
            <li class="page-item{if $i == $params.current_page} active{/if}">
                <a class="page-link" href="{set_get_arg name=$params.prefix value=$i}">{$i}</a>
            </li>
        {/for}

        {if $params.current_page != $params.all_pages}
            <li class="page-item">
                <a class="page-link" href="{set_get_arg name=$params.prefix value=$params.current_page+1}" aria-label="Następna">
                    <span aria-hidden="true">&raquo;</span>
                    <span class="sr-only">Następna</span>
                </a>
            </li>
        {/if}
    </ul>
{/if}