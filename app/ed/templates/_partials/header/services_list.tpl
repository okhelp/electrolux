{if !empty($services_list) && count($services_list) > 1}
    <ul class="navbar-nav my-lg-0">
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                {$services_list[$smarty.session.id_service]->name|upper} <i class="fa fa-caret-down"></i>
            </a>

            {foreach from=$services_list item=service}
                {if $smarty.session.id_service != $service->id}
                    <div class="dropdown-menu dropdown-menu-right animated fadeIn">
                        <a class="dropdown-item" href="{BASE_ED_URL}change_service.html?id_service={$service->id}">{$service->name|upper}</a>
                    </div>
                {/if}
            {/foreach}


        </li>
    </ul>
{/if}