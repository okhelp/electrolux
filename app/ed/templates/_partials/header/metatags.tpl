<!-- Search Engine -->
<meta name="description" content="{$metatags->meta_descripton}">
<meta name="keywords" content="{$metatags->meta_keywords}">
{if $metatags->id_cimg}
	<meta name="image" content="{img id_cimg=$metatags->id_cimg width=315 height=315}?{$metatags->ts->format('YmdHis')}">
{/if}

<!-- Schema.org for Google -->
<meta itemprop="name" content="{$metatags->meta_descripton}">
<meta itemprop="description" content="{$metatags->meta_descripton}">
{if $metatags->id_cimg}
	<meta itemprop="image" content="{img id_cimg=$metatags->id_cimg width=315 height=315}?{$metatags->ts->format('YmdHis')}">
{/if}

<!-- Twitter -->
<meta name="twitter:card" content="summary">
<meta name="twitter:title" content="{$metatags->title}">
<meta name="twitter:description" content="{$metatags->meta_descripton}">
{if $metatags->id_cimg}
	<meta name="twitter:image:src" content="{img id_cimg=$metatags->id_cimg width=315 height=315}?{$metatags->ts->format('YmdHis')}">
{/if}
<!-- Open Graph general (Facebook, Pinterest & Google+) -->
<meta name="og:title" content="{$metatags->title}">
<meta name="og:description" content="{$metatags->meta_descripton}">
{if $metatags->id_cimg}
	<meta name="og:image" content="{img id_cimg=$metatags->id_cimg width=315 height=315}?{$metatags->ts->format('YmdHis')}">
{/if}

<meta name="og:url" content="http://{$smarty.server.HTTP_HOST}{$smarty.server.REQUEST_URI}">
<meta name="og:site_name" content="furazagrosze.pl">
<meta name="og:locale" content="pl_PL">
<meta name="og:type" content="website">
<title>{$metatags->title}</title>
