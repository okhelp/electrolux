{if !empty($module_menu)}
	<ul id="sidebarnav">
		{foreach from=$module_menu item=mm_arr}
			{foreach from=$mm_arr item=mm}
				
				<li>
					<a href="{if !empty($mm.submenu)}#{else}{$mm.url}{/if}"{if !empty($mm.submenu)} class="has-arrow" aria-expanded="false"{/if}{if isset($mm.new_window) && $mm.new_window} target="_blank"{/if}>
						{if !empty($mm.submenu)}<span class="hide-menu">{/if}
						<span>{$mm.name}</span>
						{if !empty($mm.submenu)}</span>{/if}
					</a>
					{if !empty($mm.submenu)}
						<ul aria-expanded="false" class="collapse">
							{foreach from=$mm.submenu item=$smenu}
								<li>
									<a href="{$smenu.url}"{if isset($smenu.new_window) && $smenu.new_window} target="_blank"{/if}>
										<span>{$smenu.name}</span>
									</a>
									{if !empty($smenu.submenu)}
										<ul aria-expanded="false" class="collapse">
											{foreach from=$smenu.submenu item=$ssmenu}
												<a href="{$ssmenu.url}"{if isset($smenu.new_window) && $smenu.new_window} target="_blank"{/if}>{$ssmenu.name}</a>
											{/foreach}
										</ul>
									{/if}
								</li>
								{$smenu.submenu = null}
							{/foreach}
						</ul>
					{/if}
				</li>
			{/foreach}
			
		{/foreach}
	</ul>
{/if}