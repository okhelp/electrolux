<div id="interactive_mode">
	<div class="interactive_mode_content">
		<a class="interactive_mode_logo_gw" href="https://goinweb.pl" target="_blank">
			<img src="{BASE_URL}data/ed/img/logo-white-gw.png" alt="" />
			<span>tryb interaktywnej edycji</span>
		</a>
		<div class="interactive_mode_controls">
			<a href="{BASE_ED_URL}" id="interactive_mode_close">zamknij</a>
		</div>
		<div style="clear:both"></div>
	</div>

</div>