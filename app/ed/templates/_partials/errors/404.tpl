<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1.0">
        <title>Błąd 404 - IMSET CMS</title>
        <style type="text/css">
			{literal}
				html, body {padding:0; margin:0; font-family: 'Open Sans', sans-serif; color:#313131}
				a {color:#313131}
				.container {margin: 0 auto; max-width: 1140px; margin-bottom:36px}
				.left {display: table-cell; vertical-align: middle;}
				.right {display: table-cell; text-align: right;}
				.c1 {clear:both}
				h1, h3{color:#313131; text-transform: uppercase; font-weight: 800 }
				h1 {font-size:50px; text-align: center}
				h3 {font-size:36px}
				h1 span, h3 span {color:#006bd6;}
				.nav {display:table; width:100%; border-bottom: 2px solid #efefef; padding:24px;}
				.content {display: table; width:100%;}
				.footer {display:table; width:100%; border-top: 2px solid #efefef; padding:24px;}
				p {text-align: center; font-weight: 300;}
				.footer {font-weight: 300; font-size: 13px;}
				.footer span {font-size:11px;}


				@media only screen and (min-width:1701px) and (max-width:1919px){}

				@media only screen and (min-width:1500px) and (max-width:1700px){}

				@media only screen and (min-width:1280px) and (max-width:1499px){}

				h3 {margin-right: 10px;}
				.right{padding-right: 83px;}
				.container {margin-bottom:32px;padding:25px;}
				html,body {overflow-x: hidden;}


				@media only screen and (min-width:1024px) and (max-width:1279px){}
				@media only screen and (min-width:800px) and (max-width:1023px){
					.right {padding:0px;} 
					.content {padding-bottom:10px;} 
					.footer span {font-size: 10px;}
					.content .left img{width: 180px;}
					.nav{padding:0;}
					.footer{padding: 0px;padding-top: 15px;}
				}

				@media only screen and (min-width:480px) and (max-width:799px){

					h3{padding-left: 70px;margin-right:0px;font-size: 30px;padding: 10px;}
					.container .nav .left a img{width: 130px;}
					.right {padding-right: 0px;}
					.container .content .left img{width: 130px;}
					.footer span {font-size: 11px;} 
					.container .content .left img{}
					.container {margin-bottom: 14px;padding: 15px;}
					.content{display: flex;flex-direction: column;}
					.container .content .left{text-align:center;}
					h1{font-size: 30px;}
					.container .footer .right a {display: inline;}
					.nav{padding: 10px;}
					.footer {display: block;font-weight: 300;font-size: 13px;text-align:center;padding: 0;padding-top:36px;}
					.footer .left {display: block;}
					.footer .right{display:block;padding: 0;text-align: center;padding-top:36px;}
					.container .nav .left a img{padding-top:9px;}
				}
				@media only screen and (max-width:479px){

					html,body{margin:10px;}

					.nav .left a img{margin-top: 25px;width: 140px;}
					.nav {display:block;padding:5px;}
					.nav .left{display:block;padding:0;text-align:center;}
					.nav .right{display:block;}
					.nav .left a img{width:170px;}
					.content .left img{display:none;}             
					.right{padding: 0px;}
					h3{font-size: 22px;margin: 36px 0;font-size: 22px;text-align: center;}
					.container{margin-bottom: 0px;}

					.footer {display: block;font-weight: 300;font-size: 10px;text-align:center;padding: 0;padding-top:36px;}
					.footer .left {display: block;}
					.footer .right{display:block;padding: 0;text-align: center;padding-top:36px;}
					h1 {font-size: 30px;}
					.footer span{font-size: 9px;}
					.nav .left a img{padding-left:0px;}
					.content .right p{font-size: 14px;}


				}
			{/literal}


        </style>
    </head>
    <body>

        <div class="container">
            <div class="nav">
                <div class="left">
                    <a href="http://imset.pl/">
                        <img src="{BASE_URL}data/ed/img/imset_logo.svg" alt="" style="width: 200px;" />
                    </a>
                </div>
                <div class="right">
                    <h3>Błąd<span>404</span></h3>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="content">
                <div class="left">
                    <img src="data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTkuMC4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDUxMiA1MTIiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDUxMiA1MTI7IiB4bWw6c3BhY2U9InByZXNlcnZlIiB3aWR0aD0iMjU2cHgiIGhlaWdodD0iMjU2cHgiPgo8Zz4KCTxnPgoJCTxwYXRoIGQ9Ik0zNjEsNDUyaC0zMHYtMzBIMTgxdjMwaC0zMGMtMTYuNTY5LDAtMzAsMTMuNDMxLTMwLDMwdjE1YzAsOC4yODQsNi43MTYsMTUsMTUsMTVoMjQwYzguMjg0LDAsMTUtNi43MTYsMTUtMTV2LTE1ICAgIEMzOTEsNDY1LjQzMSwzNzcuNTY5LDQ1MiwzNjEsNDUyeiIgZmlsbD0iIzMxMzEzMSIvPgoJPC9nPgo8L2c+CjxnPgoJPGc+CgkJPHJlY3QgeD0iMjQxIiB5PSIxMjEiIHdpZHRoPSIzMCIgaGVpZ2h0PSI5MSIgZmlsbD0iIzMxMzEzMSIvPgoJPC9nPgo8L2c+CjxnPgoJPGc+CgkJPHBhdGggZD0iTTAsMzMydjE1YzAsMjQuODE0LDIwLjE4Niw0NSw0NSw0NWg0MjJjMjQuODE0LDAsNDUtMjAuMTg2LDQ1LTQ1di0xNUgweiIgZmlsbD0iIzMxMzEzMSIvPgoJPC9nPgo8L2c+CjxnPgoJPGc+CgkJPHBhdGggZD0iTTQ2NywwSDQ1QzIwLjE4NiwwLDAsMjAuMTg2LDAsNDV2MjU3aDUxMlY0NUM1MTIsMjAuMTg2LDQ5MS44MTQsMCw0NjcsMHogTTE4MSwyMjdjMCw4LjI5MS02LjcwOSwxNS0xNSwxNSAgICBzLTE1LTYuNzA5LTE1LTE1di00NmgtNDVjLTguMjkxLDAtMTUtNi43MDktMTUtMTV2LTYwYzAtOC4yOTEsNi43MDktMTUsMTUtMTVzMTUsNi43MDksMTUsMTV2NDVoMzB2LTQ1YzAtOC4yOTEsNi43MDktMTUsMTUtMTUgICAgczE1LDYuNzA5LDE1LDE1VjIyN3ogTTMwMSwyMjdjMCw4LjI5MS02LjcwOSwxNS0xNSwxNWgtNjBjLTguMjkxLDAtMTUtNi43MDktMTUtMTVWMTA2YzAtOC4yOTEsNi43MDktMTUsMTUtMTVoNjAgICAgYzguMjkxLDAsMTUsNi43MDksMTUsMTVWMjI3eiBNNDIxLDIyN2MwLDguMjkxLTYuNzA5LDE1LTE1LDE1Yy04LjI5MSwwLTE1LTYuNzA5LTE1LTE1di00NmgtNDVjLTguMjkxLDAtMTUtNi43MDktMTUtMTV2LTYwICAgIGMwLTguMjkxLDYuNzA5LTE1LDE1LTE1YzguMjkxLDAsMTUsNi43MDksMTUsMTV2NDVoMzB2LTQ1YzAtOC4yOTEsNi43MDktMTUsMTUtMTVjOC4yOTEsMCwxNSw2LjcwOSwxNSwxNVYyMjd6IiBmaWxsPSIjMzEzMTMxIi8+Cgk8L2c+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPC9zdmc+Cg==">
                </div>
                <div class="right">
                    <h1>;<span>(</span></h1>
                    <p>
                        <b>Podany zasób nie został znaleziony.</b>
                    </p>
                    <p>
                        Upewnij się, że wprowadzono poprawny adres url.<br>Jeżeli problem będzie się ponawiał, skonaktuj się z administratorem strony.
                    </p>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="footer">
                <div class="left">
                    © 2018 <b>IMSET sp. z o.o. (www.imset.it)</b><br>
                    <span>Strona internetowa funkcjonuje dzięki autorskiemu systemowi zarządzania treścią CMS - IMSET CMS.</span>
                </div>
                <div class="right">
                    <a href="mailto:office@imset.it">office@imset.it</a>
                </div>
            </div>
        </div>


    </body>
</html>