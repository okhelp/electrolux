<form class="form-horizontal form-material" method="POST" action="{BASE_ED_URL}login/zmien_haslo.html">
    <h3 class="box-title mb-3">{'Wymagana zmiana hasła'|lang}</h3>
    <div class="form-group">
        <div class="col-xs-12 text-center">
            <img src="{img id_cimg={$_settings->site_logo} width=120 height=60}"
                 alt="{$service_data->name|upper}" class="dark-logo" style="height:60px; margin-bottom:12px"/>
            <p class="text-muted">{'Twoje hasło straciło ważność, proszę o zmianę.'|lang}</p>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-12">
            {include file="_partials/system_message.tpl"}
        </div>
    </div>

    <div class="form-group">
        <div class="col-xs-6">
            <span class="round"><img src="{user_avatar id={$smarty.session.id} width=50 height=50}" alt="{$smarty.session.name} {$smarty.session.surname}" width="50"></span>
        </div>
        <div class="col-xs-6">
            <strong>{$smarty.session.name} {$smarty.session.surname}</strong>
        </div>
    </div>

    <div class="form-group ">
        <div class="col-xs-12">
            <input name="password[]" type="password" class="form-control" placeholder="{'Nowe hasło'|lang}" required="" autofocus>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-12">
            <input name="password[]" type="password" class="form-control" placeholder="{'Powtórz hasło'|lang}" required="required">
        </div>
    </div>
    <div class="form-group text-center mt-3">
        <div class="col-xs-12">
            <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">{'Zaloguj się'|lang}</button>
        </div>
    </div>
</form>