<form class="form-horizontal form-material" id="loginform" method="POST" action="{BASE_ED_URL}login/zaloguj.html">
	<h3 class="box-title mb-3">{'Zaloguj się'|lang}</h3>
	<div class="form-group">
		<div class="col-xs-12 text-center">
			<img src="{img id_cimg={$_settings->site_logo} width=120 height=60}"
				 alt="{$service_data->name|upper}" class="dark-logo" style="height:60px"/>
		</div>
	</div>
	<div class="form-group">
		<div class="col-xs-12">
			{include file="_partials/system_message.tpl"}
		</div>
	</div>
	<div class="form-group ">
		<div class="col-xs-12">
			<input name="user_name" type="text" class="form-control" placeholder="{'Login'|lang}" required="" autofocus>
		</div>
	</div>
	<div class="form-group">
		<div class="col-xs-12">
			<input name="password" type="password" class="form-control" placeholder="{'Hasło'|lang}" required="required">
		</div>
	</div>
	<div class="form-group">
		<div class="col-md-12">
			<div class="checkbox checkbox-primary float-left pt-0">
				<input id="checkbox-signup" type="checkbox" value="1" name="autologin">
				<label for="checkbox-signup"> {'Zapamiętaj mnie'|lang}</label>
			</div>
			<a href="javascript:void(0)" id="to-recover" class="text-dark float-right"><i class="fa fa-lock mr-1"></i> {'Zapomniałeś(aś) hasła'|lang}</a>
		</div>
	</div>
	<div class="form-group text-center mt-3">
		<div class="col-xs-12">
			{if $ref}
				<input type="hidden" name="ref" value="{$ref}" />
			{/if}
			<button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">{'Zaloguj się'|lang}</button>
		</div>
	</div>
</form>
<form class="form-horizontal" id="recoverform" method="POST" action="{BASE_ED_URL}login/przypomnij_haslo.html">
	<div class="form-group ">
		<div class="col-xs-12">
			<h3>{'Przypomnij hasło'|lang}</h3>
		</div>
	</div>
	<div class="form-group">
		<div class="col-xs-12 text-center">
			<img src="{img id_cimg={$_settings->site_logo} width=120 height=60}"
				 alt="{$service_data->name|upper}" style="height:60px; margin-bottom:12px"/>
			<p class="text-muted">{'Wpisz poniżej swój adres e-mail. Wyślemy na niego Twoje hasło.'|lang}</p>
		</div>
	</div>
	<div class="form-group ">
		<div class="col-xs-12">
			<input class="form-control" type="email" required="" name="email" placeholder="Email"> </div>
	</div>
	<div class="form-group text-center mt-3">
		<div class="col-xs-12">
			<button class="btn btn-primary btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Przypomnij</button>
		</div>
	</div>
</form>