{if isset($session_data.flash_session.message)}
	{* aby było prościej, zrzucam tablicę wiadomości do osobnej zmiennej *}
	{assign var="message" value=$session_data.flash_session.message}

	{* określam typ wiadomości, tak aby móc podać odpowiednią klasę *}
	{if $message.type == "g"}
		{assign var="class_name" value="success"}
	{else if $message.type == "w"}
		{assign var="class_name" value="warning"}
	{else if $message.type == "d"}
		{assign var="class_name" value="danger"}
	{/if}

	{* pokazuję komunikat *}
	<div class="row">
		<div class="col-md-12">
			<div class="alert alert-{$class_name}">
				{$message.description}
			</div>
		</div>
	</div>
{/if}