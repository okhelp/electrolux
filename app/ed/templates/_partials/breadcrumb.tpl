<div class="row">
	<div class="col-md-12">
		<ol class="breadcrumb">
			<li class="breadcrumb-item">
				<a href="{BASE_ED_URL}">
					Electrolux
					</a>
			</li>
			{foreach name="breadcrumb_list" from=$breadcrumb item=name key=url}
				<li class="breadcrumb-item{if $smarty.foreach.breadcrumb_list.last} active{/if}">
					{if $url}<a href="{BASE_ED_URL}{$url}">{/if}
					{$name}
					{if $url}</a>{/if}
				</li>
			{/foreach}
		</ol>
	</div>
</div>