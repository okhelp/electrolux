{*** ograniczam dołączanie plików js przy wielokrotnym wykonaniu uploadera ***}
{if !isset($reinstance) || (isset($reinstance) && $reinstance == 0)}
    <link rel="stylesheet" type="text/css" href="{BASE_URL}data/{if is_ed()}ed/{/if}css/_partials/upload/main_form.css" />
    <script type="text/javascript" src="{BASE_URL}data/{if is_ed()}ed/{/if}js/upload.js"></script>
{/if}
{if isset($images) && !empty($images)}
	<script type="text/javascript" src="{BASE_URL}data/{if is_ed()}ed/{/if}js/exif.js"></script>
{/if}
{* ustawiam parametry uploadu *}
<script type="text/javascript">
	{if isset($images) && !empty($images)} {* określam czy to jest upload obrazków, cimgi!!!*}
		{literal}var images = 1 {/literal}
	{/if}
	{if isset($file_limit) && !empty($file_limit)} {* limit wgrywanych plików, domyślnie bez limitu *}
		{literal}var file_limit = {/literal}{$file_limit}
	{/if}
	{if isset($show_thumb) && !empty($show_thumb)} {* czy pokazać miniaturki - dotyczy tylko plików graficznych *}
		{literal}var show_thumb = {/literal}{$show_thumb}
	{/if}
	{if isset($type_files) && !empty($type_files)} {* dozwolone typy plików *}
		{literal}var type_files = '{/literal}{$type_files}{literal}'{/literal}
	{else}
		{literal}var type_files = 'image/jpg,image/png,image/bmp,image/jpeg,image/gif,text/plain,application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/octetstream,application/x-download'{/literal}
	{/if}
	{if isset($max_file_size) && !empty($max_file_size)} {* maksymalny możliwy rozmiar pliku, domyslnie 10 MB *}
		{literal}var max_file_size = {/literal}{$max_file_size}
	{else}
		{literal}var max_file_size = 10240{/literal}
	{/if}
	{if isset($multiple) && !empty($multiple)} {* multiupload *}
		{literal}var multiple = 1{/literal}
	{else}
		{literal}var multiple = 0{/literal}
	{/if}
</script>

<div class="upload-form" data-instance_number="{$instance_number}">
	{* button dodaj plik/pliki *}
	<input type="file" name="{$name}"{if isset($multiple) && !empty($multiple)} multiple{/if} style="display:none"/>
	<a class="btn btn-info btn-upload">
		Wgraj {if isset($multiple) && !empty($multiple)}pliki{else}plik{/if}
	</a>
	{* komunikat o przekroczonej ilości plików *}
	{if isset($file_limit)}
		<div class="alert alert-danger" style="display:none">
			Osiągnięto maksymalną ilość wgywanych plików.
		</div>
	{/if}
	{* loader - gif + opis czynności*}
	<div class="upload-loader" style="display:none">
		<img src="{BASE_URL}data/img/ajax-loader-small.gif" alt="" />
		<span></span>
	</div>
	{* pokazuję miniatury do plików graficznych - jezeli mamy wybraną taką opcję *}
	{if isset($show_thumb) && !empty($show_thumb)}
		<div class="container-fluid file_reader">
            <div class="row">
                {* prezentuje listing obrazków, które już są dodane *}
                {if !empty($uploaded_data)}
					{foreach from=$uploaded_data item=$item_data}
						<div class="col-md-2 thumbnail" data-file="images.png" style="opacity: 1;">
							<div class="desc">
								<small><b>Plik</b>: {App__Ed__Model__Img::get_name($item_data->file_name)}<br>
								<b>Rozmiar</b>: {$item_data->get_filesize()} MB</small>
							</div>
							<div class="file_settings text-center">
							</div>
							<div class="img">
								<img src="{img id_cimg=$item_data->id width="250" height="250"}" class="img-responsive" />
							</div>
							{if isset($multiple) && !empty($multiple)}
								<input value="{$item_data->id}" name="upload_{$instance_number}[]" type="hidden">
							{else}
								<input value="{$item_data->id}" name="upload_{$instance_number}" type="hidden">
							{/if}
						</div>
					{/foreach}
					
					
					
					<script type="text/javascript">
						$(document).ready(function() 
						{
							{literal}
								var el = $('.upload-form[data-instance_number={/literal}{$instance_number}{literal}]').find('.file_reader .row');
			
								if(multiple)
								{
									el.find('.thumbnail .file_settings').append('<a href="#" class="move_upload_file" title="przesuń plik"><i class="fa fa-arrows-alt"></i></a>&nbsp;&nbsp;');
									//inicjalizuje sortowanie
									el.sortable({
										handle: ".fa-arrows-alt"
									}).disableSelection();;
								}
								
								el.find('.thumbnail .file_settings').append('<a href="#" class="edit_upload_file" title="usuń plik"><i class="fa fa-pencil"></i></a>&nbsp;&nbsp;');
								el.find('.thumbnail .file_settings').append('<a href="#" class="remove_upload_file" title="usuń plik"><i class="fa fa-minus-square"></i></a>');

								//dodaję akcję na usuniecie pliku
								el.find('.thumbnail .remove_upload_file').click(function(e){
									remove_file($(this));
									e.preventDefault();
								});
								
								//dodaję akcję na edycję pliku
								el.find('.thumbnail .edit_upload_file').click(function(e){
									edit_file($(this));
									e.preventDefault();
								});
								
							{/literal}
						})
					</script>
					
                {/if}
            </div>
		</div>
	{/if}
</div>
