{if !isset($instance_number)}
	{assign var=instance_number value=1}
{/if}
{if !isset($uploaded_data)}
	{assign var=uploaded_data value=''}
{/if}
{if !isset($reinstance)}
	{assign var=reinstance value=0}
{/if}

{include file="_partials/upload/main_form.tpl" type_files="image/jpg,image/png,image/bmp,image/jpeg,image/gif" instance_number=$instance_number uploaded_data=$uploaded_data images=1 reinstance=$reinstance}