<h3>Edytuj zdjęcie</h3>
<form id="edit_{$id_cimg}">
	<div class="form-group">
		<label>Krótki opis:</label>
		<div class="alert alert-info">
			Można stosować kod HTML.
		</div>
		<p>
			<textarea type="text" name="description" class="form-control">{$img->description}</textarea>
		</p>
	</div>
	<div class="form-group">
		<label>Autor:</label>
		<div class="alert alert-info">
			Proszę nie pisać przedrostka "fot.", ponieważ zostanie on automatycznie dopisany.
		</div>
		<input type="text" name="author" class="form-control" value="{$img->author}" />
	</div>
	<button id="save_desc_photo" type="button" class="btn btn-submit">Zapisz</button>
</form>
	
{* wysłanie formularza *}
<script type="text/javascript">
	{literal}
		$(document).ready(function() 
		{
			$('#save_desc_photo').click(function(){
				$.ajax({
				type     : "POST",
				url      : BASE_ED_URL + "upload/edit_photo.html",
				data     : "id_cimg={/literal}{$id_cimg}{literal}&" + $('#edit_{/literal}{$id_cimg}{literal}').serialize(),
				success : function(msg) {
					close_modal_ajax();
					$.notify('Zmiany zostały zapisane.');
				}
			});
			})
		})
	{/literal}
</script>