{load_plugin name="datatable"}
<script type="text/javascript">
    $(document).ready(function(){
    
        $('.resolved').click(function(e){
            var id_error = $(this).closest('tr').attr('data-id');
            resolve_error(id_error);
            e.preventDefault();
        })
        
        $('.add_user').click(function(e){
            var id_error = $(this).closest('tr').attr('data-id');
            add_user(id_error);
            e.preventDefault();
        })

        reload_page();
    })
    
    function resolve_error(id_error)
    {
        $.ajax({
            url: BASE_URL+"ed/it/errors/ajax/resolve_error.html",
            type: "POST",
            data: "id_error="+id_error,
            success: function (r) 
            {
                $('tr[data-id='+id_error+']').fadeOut(function(){
                    $(this).remove();
                })
            }
        });
    }
    
    function add_user(id_error)
    {
        $.ajax({
            url: BASE_URL+"ed/it/errors/ajax/add_user.html",
            type: "POST",
            data: "id_error="+id_error,
            success: function (r) 
            {
                location.reload();
            }
        });
    }

    function reload_page()
    {
        setTimeout(function () {
            location.reload();
        }, 60 * 1000);
    }
    
</script>

<div class="row">
    <div class="col-md-12">
        
        <div class="alert alert-info">
            Widok na stan z: <strong>{$smarty.now|date_format:"%d.%m.%Y, %H:%M:%S"}</strong>
        </div>
        
        <h3>Panel błędów ({$list|@count})</h3>
        
        <table class="datatable">
            <thead>
                <tr>
                    <th>#id</th>
                    <th width="200">Opis</th>
                    <th>Wystąpień</th>
                    <th>Pierwsze wystąpienie</th>
                    <th>Ostatnie wystąpienie</th>
                    <th>Odpowiedzialny</th>
                    <th>Rozwiązanie</th>
                </tr>
            </thead>
            <tbody>
                {if $list}
                    {foreach from=$list item=item}
                        <tr data-id="{$item->id}">
                            <td>{$item->id}</td>
                            <td><a onclick="alert('{$item->decode_description()|replace:"'":''}')">{$item->decode_description()}</a></td>
                            <td>{$item->hits}</td>
                            <td>{$item->first_time|date_format:"%d.%m.%Y, %H:%M:%S"}</td>
                            <td>{$item->last_time|date_format:"%d.%m.%Y, %H:%M:%S"}</td>
                            <td style="text-align:center">
                                {if $item->id_user}
                                    {assign var=user_data value=$item->get_user()}
                                    <img src="{img id_cimg={$user_data->id_cimg} width=35 height=35}" alt="{$user_data->login}" style="display:inline-block; margin-right:4px;" />
                                    <strong>
                                        {$user_data->name} {$user_data->surname}
                                    </strong>
                                {else}
                                    <a href="#" class="btn btn-info add_user">
                                        Przypisz
                                    </a>
                                {/if}
                            </td>
                            <td>
                                <a href="#" class="btn btn-success resolved">Rozwiązane!</a>
                            </td>
                        </tr>
                    {/foreach}
                {/if}
            </tbody>
        </table>

        
    </div>
</div>