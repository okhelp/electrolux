{load_plugin name="ckeditor"}

<form method="POST">
	<div class="form-group">
		<label>Nazwa kolumny:</label>
		{if !$id}
			<div class="alert alert-info">
				Pamiętaj żeby podać unikalną nazwę. Po wpisaniu system automatycznie zweryfikuje jej poprawność.
			</div>
		{/if}
		<input type="text" name="name" class="form-control" value="{if $column && $column->name}{$column->name}{/if}" required="required" {if $id}disabled="disabled"{/if} />
	</div>
	<div class="form-group">
		<label>Treść kolumny:</label>
		{if $shortcuts}
			<div class="alert alert-info">
				<strong>Dostępne skróty:</strong>
				{foreach from=$shortcuts item=shortcut}
					<a href="#" data-shortcut="{$shortcut}" class="add_shortcut">{$shortcut}</a>
				{/foreach}
			</div>
		{/if}
		<textarea id="description" name="description" class="ckeditor">{if $column && !empty($column->description)}{$column->description}{/if}</textarea>
	</div>
	<div class="form-group">
		<label>Status:</label>
		<select name="status" class="form-control">
			<option value="1"{if $id && $column->status == 1} selected{/if}>aktywna</option>
			<option value="0"{if $id && $column->status == 0} selected{/if}>nieaktywna</option>
		</select>
	</div>
	<button type="submit" class="btn btn-submit">Zapisz</button>
</form>

{if $shortcuts}
	<script type="text/javascript">
		{literal}
			$(document).ready(function ()
			{
				$('.add_shortcut').click(function (e)
				{
					e.preventDefault();
					var shortcut = $(this).attr('data-shortcut');
					CKEDITOR.instances.description.insertText(shortcut);
				})
			})
		{/literal}
	</script>
{/if}

{if !$id}
	<script type="text/javascript">
		{literal}
			$(document).ready(function ()
			{
				$('input[name]').on('change',function ()
				{
					var el = $(this);
					var input_val = el.val();
					if(input_val != '')
					{
						$.ajax({
							type     : "POST",
							url      : BASE_ED_URL + "columns/ajax_sprawdz_dostepnosc.html",
							data     : "name=" + input_val,
							success : function(msg) {
								if(msg == 'yes')
								{
									alert('Podana nazwa jest już zajęta. Proszę wporwadzić inną.')
									//czyszczę inputa
									el.val('');
								}
								else if(msg == 'error')
								{
									alert('Wystąpił błąd. Proszę spróbować ponownie.')
								}
							},
							complete : function(r) {

							}
						});
					}
				})
			})
		{/literal}
	</script>
{/if}