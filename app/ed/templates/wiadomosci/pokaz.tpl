{load_plugin name="bootbox"}

{assign var=user value=$message->get_user($id_user)}
{assign var=company_data value=$user->get_company_data()}
{assign var=attachments value=$message->get_attachments_list()}

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="row">
                <div class="col-xlg-2 col-lg-4 col-md-5">
                    {include file="wiadomosci/_partials/menu.tpl"}
                </div>
                <div class="col-xlg-10 col-lg-8 col-md-7">
                    <div class="card-body pt-0">
                        <div class="card b-all shadow-none">

                            <div class="card-body">
                                <div class="btn-group mb-2 mr-2" role="group" aria-label="Button group with nested dropdown">
                                    <a href="#" class="btn btn-secondary font-18 text-dark show_reply_form_btn" data-toggle="tooltip" title="" data-original-title="Odpowiedź">
                                        <i class="mdi mdi-loop"></i>
                                    </a>
                                </div>

                                <div id="delete" class="btn-group mb-2 mr-2" role="group" aria-label="Button group with nested dropdown">
                                    <a href="#" class="btn btn-secondary font-18 text-dark" data-toggle="tooltip" title="" data-original-title="Usuń wiadomość">
                                        <i class="mdi mdi-delete"></i>
                                    </a>
                                </div>

                            </div>
                            <div>
                                <hr class="mt-0">
                            </div>
                            <div class="card-body">
                                <div class="d-flex no-block mb-3">
                                    <h1 class="card-title mb-0">{$message->subject}</h1>
                                    <div class="ml-auto">
                                        <span class="badge badge-success ml-auto">
                                            {$message->ts->format('Y-m-d H:i:s')|strtotime|friendly_date}
                                        </span>
                                    </div>
                                </div>
                                <div class="d-flex mb-3">
                                    <a href="{$user->get_url()}" class="d-flex align-items-center">
                                        <img src="{user_avatar id={$user->id} width=50 height=50}" alt="user" class="img-circle" style="max-width: 50px; max-height: 50px;"/>
                                        <div class="pl-3">
                                            <h6 class="mb-0">{$user->name} {$user->surname}</h6>
                                            {if !empty($company_data)}
                                                <small class="text-muted">{$company_data[0]->name}</small>
                                            {/if}
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div>
                                <hr class="mt-0">
                            </div>
                            <div class="card-body mb-4">{$message->description}</div>
                            <div>
                                <hr class="mt-0">
                            </div>

                            {if !empty($attachments)}
                                <div class="card-body">
                                    <h4><i class="fa fa-paperclip mr-2 mb-2"></i> Załączniki <span>({$attachments|@count})</span></h4>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <ul>
                                                {foreach from=$attachments item=attachment}
                                                    {assign var=attachment_fileinfo value=$attachment|pathinfo}
                                                    <li>
                                                        <a href="{BASE_ED_URL}wiadomosci/pobierz_zalacznik.html?file={App__Ed__Model__Encryption::encode($attachment)|urlencode}">{$attachment_fileinfo.basename}</a>
                                                    </li>
                                                {/foreach}
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <hr class="mt-0">
                                </div>
                            {/if}

                            {* odpisywanie tylko dla wiadomości z folderu odebrane *}
                            {if $folder == 'inbox'}
                                <div id="reply_form_content" class="card-body">
                                    <div class="b-all mt-1 p-3">
                                        <p class="pb-3"><a href="#" class="show_reply_form_btn">Kliknij tutaj aby odpowiedź na tą wiadomość.</a></p>
                                        <div id="reply_form" style="display: none">{include file="wiadomosci/_partials/wyslij.tpl" id_message=$id_message}</div>
                                    </div>
                                </div>
                            {/if}

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    {literal}
        $(document).ready(function () {

            $('#delete').click(function(e){
                e.preventDefault();

                bootbox.confirm({
                    title: "Komunikat",
                    message: "Czy na pewno chcesz usunąć tą wiadomość?",
                    buttons: {
                        cancel: {
                            label: '<i class="fa fa-times"></i> Nie'
                        },
                        confirm: {
                            label: '<i class="fa fa-check"></i> Tak'
                        }
                    },
                    callback: function (result) {
                        if(result)
                        {
                            $.post(
                                BASE_URL + 'panel/wiadomosci.html', {"message_ids": [{/literal}{$id_message}{literal}], "action_type": "delete"}, function(){
                                    location.href = BASE_ED_URL + 'wiadomosci/odebrane.html';
                                }
                            );
                        }
                    }
                });

            });

            $('.show_reply_form_btn').click(function(e){
                e.preventDefault();

                $('#reply_form_content p').fadeOut(function(){

                    $('#reply_form').fadeIn(function(){
                        $('html,body').animate({
                            scrollTop: $("#reply_form").offset().top
                        }, 'slow');
                    });


                });

            });


        })
    {/literal}
</script>
