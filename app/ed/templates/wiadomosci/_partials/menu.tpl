<div class="card-body inbox-panel">
    <a href="{BASE_ED_URL}wiadomosci/napisz.html" class="btn btn-danger mb-3 p-2 btn-block waves-effect waves-light">Napisz
        wiadomość</a>
    <ul class="list-group list-group-full">
        <li class="list-group-item {if !empty($received_active)}active{/if}">
            <a href="{BASE_ED_URL}wiadomosci/odebrane.html">
                <i class="mdi mdi-gmail"></i> Odebrane
            </a>
            {if isset($new_messages_count)}
                <span class="badge badge-success ml-auto">{$new_messages_count}</span>
            {/if}
        </li>
        <li class="list-group-item {if !empty($important_active)}active{/if}">
            <a href="{BASE_ED_URL}wiadomosci/wazne_wiadomosci.html">
                <i class="mdi mdi-star"></i> Ważne
            </a>
        </li>
        <li class="list-group-item {if !empty($sent_active)}active{/if}">
            <a href="{BASE_ED_URL}wiadomosci/wyslane.html">
                <i class="mdi mdi-file-document-box"></i> Wysłane
            </a>
        </li>
        <li class="list-group-item {if !empty($deleted_active)}active{/if}">
            <a href="{BASE_ED_URL}wiadomosci/usuniete.html">
                <i class="mdi mdi-delete"></i> Kosz
            </a>
        </li>
    </ul>
    {*<h3 class="card-title mt-5">Lista pracowników</h3>*}
    {*<div class="list-group b-0 mail-list">*}
        {*<a href="#" class="list-group-item">*}
            {*<span class="fa fa-circle text-info mr-2"></span>Work*}
        {*</a>*}
        {*<a href="#" class="list-group-item">*}
            {*<span class="fa fa-circle text-warning mr-2"></span>Family*}
        {*</a>*}
        {*<a href="#" class="list-group-item">*}
            {*<span class="fa fa-circle text-purple mr-2"></span>Private*}
        {*</a>*}
        {*<a href="#" class="list-group-item">*}
            {*<span class="fa fa-circle text-danger mr-2"></span>Friends*}
        {*</a>*}
        {*<a href="#" class="list-group-item">*}
            {*<span class="fa fa-circle text-success mr-2"></span>Corporate*}
        {*</a>*}
    {*</div>*}
</div>