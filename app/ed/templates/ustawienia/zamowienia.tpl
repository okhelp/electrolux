{load_plugin name="datatable"}
{load_plugin name="bootbox"}

<div class="card">
    <div class="card-header">
        Statusy zamówień
    </div>
    <div class="card-body">
        <button class="btn btn-primary"
                data-url="{BASE_ED_URL}ustawienia/order_status/add_new_status.html"
                data-toggle="modal"
                data-target="#order_status_modal">
            Dodaj status
        </button>

        <table class='datatable mt-3'>
            <thead>
            <tr>
                <th>Nazwa</th>
                <th>Systemowy</th>
                <th>Wysyłka powiadomienia po zmianie statusu</th>
                <th>Data dodania</th>
                <th>Data modyfikacji</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            {foreach $order_statuses as $status}
                {if empty($status->deleted_at)}
                    <tr>
                        <td>{$status->name}</td>
                        <td>{if empty($status->is_default)}Nie{else}Tak{/if}</td>
                        <td>{if empty($status->send_notification)}Nie{else}Tak{/if}</td>
                        <td>{if empty($status->is_default)}{$status->created_at->format('Y:m:d H:i:s')}{else}-{/if}</td>
                        <td>{if !empty($status->updated_at)}{$status->updated_at->format('Y:m:d H:i:s')}{else}-{/if}</td>
                        <td class="text-center">
                            <button class="btn btn-primary"
                                    data-url="{BASE_ED_URL}ustawienia/order_status/edit_status.html?id={$status->id}"
                                    data-toggle="modal"
                                    data-target="#order_status_modal">
                                Edytuj
                            </button>
                            {if empty($status->is_default)}
                                <a href="{BASE_ED_URL}ustawienia/order_status/remove_status.html?id={$status->id}"
                                   class="btn btn-danger confirm"
                                   data-confirm-text="Czy na pewno chcesz usunąć status '{$status->name}'?">
                                    Usuń
                                </a>
                            {/if}
                        </td>
                    </tr>
                {/if}
            {/foreach}
            </tbody>
        </table>
    </div>
</div>


<div class="modal fade" id="order_status_modal" tabindex="-1" role="dialog" aria-labelledby="order_status_modal"
     aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">asd</div>
    </div>
</div>

<script>
    {literal}
    $('#order_status_modal').on('show.bs.modal', function (e) {
        var $url = e.relatedTarget.dataset.url;
        $('#order_status_modal').find('.modal-content').empty();

        // loaderInit();
        $.get($url)
            .done(function (response) {
                $('#order_status_modal').find('.modal-content').html(response);
                // loaderHide();
                checkInputValue();
            });
    });
    {/literal}
</script>