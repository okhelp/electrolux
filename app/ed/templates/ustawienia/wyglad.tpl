{load_plugin name="datatable"}

<div class="alert alert-info">
	Aby wgrać szablon należy skopiować pliki do katalogu <i>templates/</i>. <br />Usunięcie jest analogiczne - wystarczy usunąć folder.
</div>

<table class='datatable'>
	<thead>
		<tr>
			<th></th>
			<th>nazwa</th>
			<th>katalog</th>
			<th>status</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		{foreach from=$list item=l key=$dir_name}
			<tr>
				<td>
					{if $l.screenshot}
						<img src="{$l.screenshot}" alt="{$l.name}" class="img-responsive" style="width:100px;" />
					{/if}
				</td>
				<td>
					<b>{$l.name}</b>
				</td>
				<td>{$dir_name}</td>
				<td>
					{if $l.active}
						<span class="label label-success">aktywny</span>
					{else}
						<span class="label label-danger">nieaktywny</span>
					{/if}
				</td>
				<td class="text-right">
					<a href="{BASE_ED_URL}theme_editor/ed.html?theme={$dir_name}" class='btn btn-default'>edytuj</a>
					{if !$l.active}
						<a href="{BASE_ED_URL}ustawienia/wyglad.html?set_status=1&template={$dir_name}" class='btn btn-success confirm' data-confirm-text="Czy na pewno chcesz włączyć ten szablon?">ustaw jako domyślny</a>
					{/if}
				</td>
			</tr>
		{/foreach}
	</tbody>
</table>
	
