{load_plugin name="ckeditor"}

<form action="{BASE_ED_URL}ustawienia/order_status/add_new_status.html" type="post">
    <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Dodawanie statusu zamówienia</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="modal-body">
        <div class="custom_form__input">
            <input class="custom_form__input--input"
                   type="text"
                   id="status_name"
                   name="name"
                   value="">
            <span class="custom_form__input--placeHolder" data-placeholder="Nazwa statusu"></span>
        </div>
        <div class="form-check mt-3">
            <input id="send-email-checkbox" type="checkbox" class="form-check-input" name="send_notification" value="1">
            <label class="form-check-label">Wyślij e-mail do klienta, kiedy zmieni się status zamówienia</label>
        </div>
        <div id="email-container" class="hide mt-3">
            <div class="form-group">
                <label>Dostępne skróty</label>
                <div id="shortcut-list" class="alert alert-info">
                    <a href="#" style="display:inline-block; margin-right: 12px; margin-bottom: 12px;">%%data%%</a>
                    <a href="#" style="display:inline-block; margin-right: 12px; margin-bottom: 12px;">%%data_realizacji%%</a>
                    <a href="#" style="display:inline-block; margin-right: 12px; margin-bottom: 12px;">%%nazwa_uzytkownika%%</a>
                    <a href="#" style="display:inline-block; margin-right: 12px; margin-bottom: 12px;">%%numer_zamowienia%%</a>
                    <a href="#" style="display:inline-block; margin-right: 12px; margin-bottom: 12px;">%%nazwa_statusu%%</a>
                </div>
            </div>
            <div class="form-group">
                <label>Treść wysłanej wiadomości</label>
                <textarea name="notification_text" class="ckeditor" id="ckeditor"></textarea>
            </div>
        </div>
        <div class="text-right mt-4">
            <button class="btn btn-pills btn-primary" type="submit" formmethod="post" name="save" value="1">Dodaj
            </button>
        </div>
    </div>
</form>

<script>
    CKEDITOR.replace('ckeditor');

    $('#send-email-checkbox').change(function(){
        if ($(this).is(":checked")) {
            $('#email-container').show();
        } else {
            $('#email-container').hide();
        }
    });

    $(document).ready(function(){

        $('#shortcut-list a').click(function() {
            add_shortcut($(this).text());
        })

    })

    function add_shortcut(shortcut)
    {
        CKEDITOR.instances['ckeditor'].insertHtml(shortcut);
    }
</script>