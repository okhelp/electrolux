{load_plugin name="ckeditor"}

<form action="{BASE_ED_URL}ustawienia/order_status/edit_status.html" type="post">
    <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Edycja statusu zamówienia</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="modal-body">
        <div class="custom_form__input">
            <input class="custom_form__input--input {if !empty($status->name)}has-val{/if}"
                   type="text"
                   id="status_name"
                   name="name"
                   value="{$status->name}">
            <span class="custom_form__input--placeHolder" data-placeholder="Nazwa statusu"></span>
        </div>
        <div class="form-check mt-3">
            <input id="send-email-checkbox" type="checkbox" class="form-check-input" name="send_notification" value="1" {if !empty($status->send_notification)}checked{/if}>
            <label class="form-check-label">Wyślij e-mail do klienta, kiedy zmieni się status zamówienia</label>
        </div>
        <div id="email-container" class="hide mt-3">
            <div class="form-group">
                <label>Dostępne skróty</label>
                <div id="shortcut-list" class="alert alert-info">
                    {foreach $shortcodes as $shortcode}
                    <a href="#" style="display:inline-block; margin-right: 12px; margin-bottom: 12px;">{$shortcode}</a>
                    {/foreach}
                </div>
            </div>
            <div class="form-group">
                <label>Treść wysłanej wiadomości</label>
                <textarea name="notification_text" class="ckeditor" id="ckeditor">{$status->notification_text}</textarea>
            </div>
        </div>
        <input type="hidden" name="id_status" value="{$status->id}">
        <div class="text-right mt-4">
            <button class="btn btn-pills btn-primary" type="submit" formmethod="post" name="save" value="1">Zapisz</button>
        </div>
    </div>
</form>

<script>
    CKEDITOR.replace('ckeditor');

    $('#send-email-checkbox').change(function(){
        if ($(this).is(":checked")) {
            $('#email-container').show();
        } else {
            $('#email-container').hide();
        }
    });

    $('#send-email-checkbox').trigger("change");

    $(document).ready(function(){

        $('#shortcut-list a').click(function() {
            add_shortcut($(this).text());
        })

    })

    function add_shortcut(shortcut)
    {
        CKEDITOR.instances['ckeditor'].insertHtml(shortcut);
    }
</script>