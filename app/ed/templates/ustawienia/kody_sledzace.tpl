<form method="POST">
	
	<div class="form-group">
		<label>Przed {'</head>'|escape}:</label>
		<textarea type="text" name="end_head" class="form-control required" style="height:500px;">{if !empty($settings) && $settings->end_head}{$settings->end_head}{/if}</textarea>
	</div>
	
	<div class="form-group">
		<label>Po {'<body>'|escape}:</label>
		<textarea type="text" name="start_body" class="form-control required" style="height:500px;">{if !empty($settings) && $settings->start_body}{$settings->start_body}{/if}</textarea>
	</div>
	
	<button type="submit" class="btn btn-success">Zapisz</button>
</form>