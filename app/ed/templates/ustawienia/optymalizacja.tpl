<form method="POST">
	
	<div class="form-group">
		<label>Kompresja kodu html:</label>
		<select name="compress_html" class="form-control">
			<option value="1"{if !empty($settings) && !empty($settings->compress_html)} selected="selected"{/if}>Tak</option>
			<option value="0"{if !empty($settings) && empty($settings->compress_html)} selected="selected"{/if}>Nie</option>
		</select>
	</div>
	
	<div class="form-group">
		<label>Kompresja plików CSS i JS:</label>
		<select name="compress_css_js" class="form-control">
			<option value="1"{if !empty($settings) && !empty($settings->compress_css_js)} selected="selected"{/if}>Tak</option>
			<option value="0"{if !empty($settings) && empty($settings->compress_css_js)} selected="selected"{/if}>Nie</option>
		</select>
	</div>
	
	
	<button type="submit" class="btn btn-success">Zapisz</button>
</form>