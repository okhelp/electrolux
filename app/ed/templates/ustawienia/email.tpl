<form method="POST">
	<div class="form-group">
		<label>Serwer SMTP:</label>
		<input type="text" name="smtp" class="form-control required" required="required" value="{if $settings->smtp}{$settings->smtp}{/if}"/>
	</div>
	<div class="form-group">
		<label>Port SMTP:</label>
		<input type="text" name="port" class="form-control required" required="required" value="{if $settings->port}{$settings->port}{/if}" onkeyup="this.value=this.value.replace(/\D/g,'')"/>
	</div>
	<div class="form-group">
		<label>Użytkownik:</label>
		<input type="text" name="username" class="form-control required" required="required" value="{if $settings->username}{$settings->username}{/if}"/>
	</div>
	<div class="form-group">
		<label>Hasło:</label>
		<input type="password" name="password" class="form-control required" required="required" value="{if $settings->password}{$settings->password}{/if}"/>
	</div>
	<div class="form-group">
		<label>Adres e-mail:</label>
		<input type="email" name="email" class="form-control required" required="required" value="{if $settings->email}{$settings->email}{/if}"/>
	</div>
	<div class="form-group">
		<label>Nazwa nadawcy</label>
		<input type="text" name="name" class="form-control required" required="required" value="{if $settings->name}{$settings->name}{/if}" />
	</div>
	<button type="submit" class="btn btn-success">Zapisz</button>
</form>