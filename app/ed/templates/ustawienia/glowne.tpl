{load_plugin name="ckeditor"}
<form method="POST" enctype="multipart/form-data">
	<div class="form-group">
		<label>Nazwa serwisu:</label>
		<input type="text" name="service_name" class="form-control required" required="required" value="{if $settings->service_name}{$settings->service_name}{/if}"/>
	</div>
	<div class="form-group">
		<label>Tytuł strony:</label>
		<input type="text" name="site_title" class="form-control required" required="required" value="{if $settings->site_title}{$settings->site_title}{/if}" />
	</div>
	<div class="form-group">
		<label>Logo:</label>
		<div class="alert alert-info">
			Aby logo było czytelne na każdej rozdzielczości ekranu, zalecamy używanie logotypu w formacie .svg.
		</div>

		{if $settings && $settings->site_logo}
			<div id="logo">
				<div class="row">
					<div class="col-md-12">
						<img class="img-thumbnail" src="{img id_cimg=$settings->site_logo width=120 height=120}" alt="" style="margin: 12px 0" />
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<p>
							<a href="#" id="logo-do-change" class="btn btn-info">Zmień</a>
						</p>
					</div>
				</div>
			</div>
		{/if}
		<div id="logo-change"{if $settings && $settings->site_logo} style="display:none"{/if}>
			<input type="file" name="site_logo" class="form-control"  />
		</div>
	</div>
	<div class="form-group">
		<label>Social logo: <small>Rozmiar: <b>320x320px</b></small></label>
		{if $settings && $settings->site_social_logo}
			<div id="social-logo">
				<div class="row">
					<div class="col-md-12">
						<img class="img-thumbnail" src="{img id_cimg=$settings->site_social_logo width=320 height=320}" alt="" style="margin: 12px 0" />
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<p>
							<a href="#" id="social-logo-do-change" class="btn btn-info">Zmień</a>
						</p>
					</div>
				</div>
			</div>
		{/if}
		<div id="social-logo-change"{if $settings && $settings->site_social_logo} style="display:none"{/if}>
			{include file="_partials/upload/images_form.tpl" name="site_social_logo" file_limit=1 show_thumb=1}
		</div>
	</div>
		
	<div class="form-group">
		<label>Meta tytuł:</label>
		<input type="text" name="site_meta_title" class="form-control required" required="required" value="{if !empty($settings->site_meta_title)}{$settings->site_meta_title}{/if}" />
	</div>
	
	<div class="form-group">
		<label>Meta opis:</label>
		<input type="text" name="site_meta_description" class="form-control required" required="required" value="{if !empty($settings->site_meta_description)}{$settings->site_meta_description}{/if}" />
	</div>
	
	<div class="form-group">
		<label>Meta słowa kluczowe:</label>
		<input type="text" name="site_meta_keywords" class="form-control required" required="required" value="{if !empty($settings->site_meta_keywords)}{$settings->site_meta_keywords}{/if}" />
	</div>
	
	<div class="form-group">
		<label>Komunikat COOKIES</label>
		<p>
			<select class="form-control{if isset($settings->cookie_info_status) && !empty($settings->cookie_info_status)} active{/if}" name="cookie_info_status">
				<option value="1"{if isset($settings->cookie_info_status) && !empty($settings->cookie_info_status)} selected="selected"{/if}>aktywny</option>
				<option value="0"{if empty($settings->cookie_info_status)} selected="selected"{/if}>nieaktywny</option>
			</select>
		</p>
		<div id="textarea_cookie_info" style="display:none">
			<textarea name="cookie_info" class="ckeditor">{if !empty($settings->cookie_info)}{$settings->cookie_info}{/if}</textarea>
		</div>
	</div>


	<div class="form-group">
		<label>Konkursy:</label>
		<input type="checkbox" name="is_contest" class="checkbox"{if !empty($settings->is_contest)} checked{/if} />
	</div>

	<button type="submit" class="btn btn-success">Zapisz</button>

</form>

<script type="text/javascript">
	{* logotyp strony *}
	{if $settings && $settings->site_logo}
		{literal}
			$(document).ready(function ()
			{
				$('#logo-do-change').click(function (e)
				{
					$('#logo').slideUp(function ()
					{
						$('#logo-change').slideDown();
					})
					e.preventDefault();
				})
			})
		{/literal}
	{/if}
		
	{* zdjęcie do social media *}	
	{if $settings && $settings->site_social_logo}
		{literal}
			$(document).ready(function ()
			{
				$('#social-logo-do-change').click(function (e)
				{
					$('#social-logo').slideUp(function ()
					{
						$('#social-logo-change').slideDown();
					})
					e.preventDefault();
				})
			})
		{/literal}
	{/if}
		
	{* komunikat cookies *}
	{literal}
		$(document).ready(function() 
		{
			if($('select[name=cookie_info_status]').val() == 1)
			{
				$('#textarea_cookie_info').show();
			}
			
			$('select[name=cookie_info_status]').on('change',function(){
				var cookie_info_status = $(this).val();
				if($('select[name=cookie_info_status]').val() == 1)
				{
					$('#textarea_cookie_info').fadeIn();
				}
				else
				{
					$('#textarea_cookie_info').fadeOut();
				}
			})
		})
	{/literal}
</script>