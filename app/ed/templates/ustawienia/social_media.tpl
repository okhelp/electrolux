<form method="POST">
	
	<div class="form-group">
		<label>Facebook:</label>
		<input type="text" name="facebook" class="form-control" value="{if !empty($settings) && !empty($settings->facebook)}{$settings->facebook}{/if}" />
	</div>
	<div class="form-group">
		<label>Twitter:</label>
		<input type="text" name="twitter" class="form-control" value="{if !empty($settings) && !empty($settings->twitter)}{$settings->twitter}{/if}" />
	</div>
	<div class="form-group">
		<label>Instagram:</label>
		<input type="text" name="instagram" class="form-control" value="{if !empty($settings) && !empty($settings->instagram)}{$settings->instagram}{/if}" />
	</div>
	<div class="form-group">
		<label>Google+:</label>
		<input type="text" name="google_plus" class="form-control" value="{if !empty($settings) && !empty($settings->google_plus)}{$settings->google_plus}{/if}" />
	</div>
	
	
	<button type="submit" class="btn btn-success">Zapisz</button>
</form>