<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="../assets/images/favicon.png">
    <title>{if $page_title}{$page_title} - {/if}IMSET.IT CMS</title>

    <link href="{BASE_URL}data/ed/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="{BASE_URL}data/ed/css/style.css" rel="stylesheet">

    {* dodatkowy styl css serwisu *}
    {if !empty($service_data->css)}
        <link href="{BASE_URL}data/ed/css/colors/{$service_data->css}" id="theme" rel="stylesheet">
    {/if}

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<!-- ============================================================== -->
<!-- Preloader - style you can find in spinners.css -->
<!-- ============================================================== -->
<div class="preloader">
    <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
</div>
<!-- ============================================================== -->
<!-- Main wrapper - style you can find in pages.scss -->
<!-- ============================================================== -->
<section id="wrapper">
    <div class="login-register" style="background-image:url({BASE_URL}data/ed/assets/images/background/login-register.jpg);">
        <div class="login-box card">
            <div class="card-body">
                {include file=$template_file}
            </div>
        </div>
    </div>

</section>
<!-- ============================================================== -->
<!-- End Wrapper -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- All Jquery -->
<!-- ============================================================== -->

<script src="{BASE_URL}data/ed/assets/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap tether Core JavaScript -->
<script src="{BASE_URL}data/ed/assets/plugins/bootstrap/js/popper.min.js"></script>
<script src="{BASE_URL}data/ed/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<!-- slimscrollbar scrollbar JavaScript -->
<script src="{BASE_URL}data/ed/js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="{BASE_URL}data/ed/js/waves.js"></script>
<!--Menu sidebar -->
<script src="{BASE_URL}data/ed/js/sidebarmenu.js"></script>
<!--stickey kit -->
<script src="{BASE_URL}data/ed/assets/plugins/sticky-kit-master/dist/sticky-kit.min.js"></script>
<!--Custom JavaScript -->
<script src="{BASE_URL}data/ed/js/custom.min.js"></script>

</body>

</html>