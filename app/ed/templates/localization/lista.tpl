{load_plugin name="datatable"}

<a href="ed.html" class="btn btn-info">Dodaj</a>

<table class='datatable'>
	<thead>
		<tr>
			<th>#id</th>
			<th>nazwa</th>
			<th>status</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		{foreach from=$list item=l}
			<tr>
				<td>{$l->id}</td>
				<td>{$l->name}</td>
				<td>
					{if $l->status == 1}
						<span class="label label-success">aktywny</span>
					{else}
						<span class="label label-danger">nieaktywny</span>
					{/if}
				</td>
				<td class="text-right">
					<a href="{BASE_ED_URL}localization/ed.html?id={$l->id}" class='btn btn-default'>edytuj</a>
					<a href="{BASE_ED_URL}localization/usun.html?id={$l->id}" class='btn btn-danger confirm' data-confirm-text="Czy na pewno chcesz usunąć tą lokalizację?">usuń</a>
				</td>
			</tr>
		{/foreach}
	</tbody>
</table>
