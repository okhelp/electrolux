{load_plugin name="ckeditor"}
<form method="POST">
	<div class="form-group">
		<label>Nazwa:</label>
		<input type="text" name="name" class="form-control required" required="required" value="{if $localization && !empty($localization->name)}{$localization->name}{/if}"/>
	</div>
	<div class="form-group">
		<label>Szerokość geograficzna:</label>
		<input type="text" name="lat" class="form-control required" required="required" value="{if $localization && !empty($localization->lat)}{$localization->lat}{/if}"/>
	</div>
	<div class="form-group">
		<label>Długość geograficzna:</label>
		<input type="text" name="lng" class="form-control required" required="required" value="{if $localization && !empty($localization->lng)}{$localization->lng}{/if}"/>
	</div>
	
	<div class="form-group">
		<label>Marker:</label>
		{if !empty($markers_list)}
			<select name="marker" class="form-control">
				{foreach from=$markers_list item=marker}
					<option value="{$marker}">{$marker}</option>
				{/foreach}
			</select>
		{else}
			<div class="alert alert-danger">Proszę wgrać min. 1 plik markera.</div>
		{/if}
	</div>
	
	<div class="form-group">
		<label>Opis markera:</label>
		<textarea name="description" class="ckeditor">{if $localization && !empty($localization->description)}{$localization->description}{/if}</textarea>
	</div>

	<div class="form-group">
		<label>Status:</label>
		<select name="status" class="form-control">
			<option value="1"{if $localization && $localization->status == 1} selected{/if}>włączona</option>
			<option value="0"{if $localization && $localization->status == 0} selected{/if}>wyłączona</option>
		</select>
	</div>
	{if !empty($markers_list)}
		<button type="submit" class="btn btn-success">Zapisz</button>
	{/if}
</form>


