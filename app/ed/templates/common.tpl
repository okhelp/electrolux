<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="robots" content="noindex">
    <meta name="author" content="imset.it">

    <title>{if $page_title}{$page_title} - {/if}IMSET.IT CMS</title>
    <!-- PLUGINS -->
    <link href="{BASE_URL}data/ed/assets/plugins/html5-editor/bootstrap-wysihtml5.css" rel="stylesheet">
    <link href="{BASE_URL}data/ed/assets/plugins/dropzone-master/dist/dropzone.js" rel="stylesheet">
    <!-- Bootstrap core CSS -->
    <link href="{BASE_URL}data/ed/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="{BASE_URL}data/ed/js/ui/jquery-ui.min.css" rel="stylesheet">
    <link href="{BASE_URL}data/ed/css/style.css" rel="stylesheet">

    {* dodatkowy styl css serwisu *}
    {if !empty($service_data->css)}
        <link href="{BASE_URL}data/ed/css/colors/{$service_data->css}" id="theme" rel="stylesheet">
    {/if}


    <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->

    <!-- js placed at the end of the document so the pages load faster -->
    <script type="text/javascript">
        {literal}
        var BASE_URL = '{/literal}{BASE_URL}{literal}';
        var BASE_ED_URL = '{/literal}{BASE_ED_URL}{literal}';
        {/literal}
    </script>

    <script src="{BASE_URL}data/ed/assets/plugins/jquery/jquery.min.js"></script>
    <script src="{BASE_URL}data/ed/assets/plugins/jqueryui/jquery-ui.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{BASE_URL}data/ed/assets/plugins/bootstrap/js/popper.min.js"></script>
    <script src="{BASE_URL}data/ed/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="{BASE_URL}data/ed/js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="{BASE_URL}data/ed/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="{BASE_URL}data/ed/js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="{BASE_URL}data/ed/assets/plugins/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <!--Custom JavaScript -->
    <script src="{BASE_URL}data/ed/assets/plugins/html5-editor/bootstrap-wysihtml5.js"></script>
    <script src="{BASE_URL}data/ed/assets/plugins/dropzone-master/dist/dropzone.js"></script>
    <script src="{BASE_URL}data/ed/js/custom.min.js"></script>


</head>

<body class="fix-header card-no-border">
<!-- preloader -->
<div class="preloader">
    <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/>
    </svg>
</div>

<!-- okienko ajaxowe (modalne) -->
<div class="modal fade" id="modal_ajax" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body"></div>
        </div>
    </div>
</div>

<div id="main-wrapper">
    <!-- ============================================================== -->
    <!-- Topbar header - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <header class="topbar">
        <nav class="navbar top-navbar navbar-expand-md navbar-light">
            <!-- ============================================================== -->
            <!-- Logo -->
            <!-- ============================================================== -->
            <div class="navbar-header">
                <a class="navbar-brand" href="{BASE_ED_URL}">
                    <!-- Logo text -->
                    <span>
                         <!-- dark Logo text -->
                         <img src="{img id_cimg={$_settings->site_logo} width=120 height=60}"
                              alt="{$service_data->name|upper}" class="dark-logo" style="height:38px"/>
                     </span>
                </a>

            </div>
            <!-- ============================================================== -->
            <!-- End Logo -->
            <!-- ============================================================== -->
            <div class="navbar-collapse">
                <!-- ============================================================== -->
                <!-- toggle and nav items -->
                <!-- ============================================================== -->
                <ul class="navbar-nav mr-auto mt-md-0 ">
                    <!-- ============================================================== -->
                    <!-- Comment -->
                    <!-- ============================================================== -->
                    <!-- ============================================================== -->
                    <!-- End Comment -->
                    <!-- ============================================================== -->
                    <!-- ============================================================== -->
                    <!-- Messages -->
                    <!-- ============================================================== -->
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="#" id="2"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i
                                    class="mdi mdi-email"></i>
                            <div class="notify"><span class="heartbit"></span> <span class="point"></span></div>
                        </a>
                        <div class="dropdown-menu mailbox animated fadeIn" aria-labelledby="2">
                            <ul>
                                <li>
                                    <div class="drop-title">Masz {if empty($messages['total'])}0{else}{$messages['total']}{/if} nowe wiadomości</div>
                                </li>
                                <li>
                                    <div class="message-center">
                                        {if !empty($messages['list'])}
                                            {foreach $messages['list'] as $message}
                                                {assign var=user value=$message->get_user_folder('inbox')}
                                        <a href="{$message->get_url('inbox')}">
                                            <div class="user-img"><img src="{user_avatar id={$message->id_from} width=50 height=50}" alt="user"
                                                                       class="img-circle"><span
                                                        class="float-right"></span></div>
                                            <div class="mail-contnet">
                                                <h5>{$user->name} {$user->surname}</h5> <span
                                                        class="mail-desc">{$message->subject}</span> <span
                                                        class="time">{$message->ts->format('Y-m-d H:i:s')}</span></div>
                                        </a>
                                            {/foreach}
                                        {/if}
                                    </div>
                                </li>
                                <li>
                                    <a class="nav-link text-center" href="{BASE_ED_URL}wiadomosci/odebrane.html"> <strong>Zobacz wszystkie</strong> <i class="fa fa-angle-right"></i> </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <!-- ============================================================== -->
                    <!-- End Messages -->
                    <!-- ============================================================== -->
                    <!-- ============================================================== -->
                    <!-- Messages -->
                    <!-- ============================================================== -->
                    <!-- ============================================================== -->
                    <!-- End Messages -->
                    <!-- ============================================================== -->
                </ul>
                <!-- ============================================================== -->
                <!-- User profile and search -->
                <!-- ============================================================== -->

                {include file="_partials/header/services_list.tpl"}


            </div>
        </nav>
    </header>
    <!-- ============================================================== -->
    <!-- End Topbar header -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Left Sidebar - style you can find in sidebar.scss  -->
    <!-- ============================================================== -->
    {if !empty(uri_segment(2)) && uri_segment(2) != 'it'}
    <aside class="left-sidebar">
        <!-- Sidebar scroll-->
        <div class="scroll-sidebar">
            <!-- User profile -->
            <div class="user-profile">
                <!-- User profile image -->

                <div class="profile-img"><img src="{user_avatar id={$smarty.session.id} width=50 height=50}" alt="{$smarty.session.name} {$smarty.session.surname}"/></div>
                <!-- User profile text-->
                <div class="profile-text"><a href="#" class="dropdown-toggle link u-dropdown" data-toggle="dropdown"
                                             role="button" aria-haspopup="true" aria-expanded="true">{$smarty.session.name} {$smarty.session.surname}<span
                                class="caret"></span></a>
                    <div class="dropdown-menu animated flipInY">
{*                        <a href="#" class="dropdown-item"><i class="ti-user"></i> My Profile</a>*}
{*                        <a href="#" class="dropdown-item"><i class="ti-wallet"></i> My Balance</a>*}
                        <a href="{BASE_ED_URL}wiadomosci/odebrane.html" class="dropdown-item"><i class="ti-email"></i> Skrzynka odbiorcza</a>
{*                        <div class="dropdown-divider"></div>*}
{*                        <a href="#" class="dropdown-item"><i class="ti-settings"></i> Account Setting</a>*}
{*                        <div class="dropdown-divider"></div>*}
                        <a href="{BASE_ED_URL}login/wyloguj.html" class="dropdown-item"><i class="fa fa-power-off"></i> Wyloguj</a>
                    </div>
                </div>
            </div>
            <!-- End User profile text-->
            <!-- Sidebar navigation-->
            <nav class="sidebar-nav">
                {include file="_partials/header/module_menu.tpl"}
            </nav>
            <!-- End Sidebar navigation -->
        </div>
        <!-- End Sidebar scroll-->
        <!-- Bottom points-->
        <div class="sidebar-footer">
            <!-- item-->
{*            <a href="#" class="link" data-toggle="tooltip" title="Settings"><i class="ti-settings"></i></a>*}
            <!-- item-->
            <a href="{BASE_ED_URL}wiadomosci/odebrane.html" class="link" data-toggle="tooltip" title="Skrzynka odbiorcza"><i class="mdi mdi-gmail"></i></a>
            <!-- item-->
            <a href="{BASE_ED_URL}login/wyloguj.html" class="link" data-toggle="tooltip" title="Wyloguj"><i class="mdi mdi-power"></i></a>
        </div>
        <!-- End Bottom points-->
    </aside>
    {/if}
    <!-- ============================================================== -->
    <!-- End Left Sidebar - style you can find in sidebar.scss  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Page wrapper  -->
    <!-- ============================================================== -->
    <div class="page-wrapper">
        <!-- ============================================================== -->
        <!-- Container fluid  -->
        <!-- ============================================================== -->
        <div class="container-fluid">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-12 col-12 align-self-center">
                    {if !empty($page_title)}
                        <h3 class="text-themecolor mb-0 mt-0">{$page_title}</h3>
                    {/if}

                    {if !empty($breadcrumb)}
                        {include file="_partials/breadcrumb.tpl"}
                    {/if}

                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Start Page Content -->
            <!-- ============================================================== -->
            {if !empty($template_file)}
                <div class="row">
                    <div class="col-12">
                        {include file=$template_file}
                    </div>
                </div>
            {/if}


            </div>
            <!-- ============================================================== -->
            <!-- End PAge Content -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->

        </div>
        <!-- ============================================================== -->
        <!-- End Container fluid  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- footer -->
        <!-- ============================================================== -->
        <footer class="footer">
            © by electroluxodkuchni.pl
        </footer>
        <!-- ============================================================== -->
        <!-- End footer -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Page wrapper  -->
    <!-- ============================================================== -->
</div>


<!--common script for all pages-->
<script src="{BASE_URL}data/ed/js/common-scripts.js"></script>


</body>

</html>
