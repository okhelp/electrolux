<?php
if($_SERVER['REMOTE_ADDR'] == '85.222.111.54')
{

    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
}
/** 
 * Plik wejsciowy blue framework
 */

session_start();
   
//czas ładowania strony
$time = microtime();
$time = explode(' ', $time);
$time = $time[1] + $time[0];
$start_time_load = $time;
unset($time);

//ustawienie głównego kodowania
ini_set('default_charset', 'utf-8');
mb_internal_encoding('utf-8');
mb_detect_order('utf-8');
 
//ładowanie wszystkich klass
require_once __DIR__.'/lib/autoload.php';

//werja developerska - usuwam z pamięci zapytania
if(!PRODUCTION)
{
    Lib__Session::remove('sql_queries');
}

//ładowanie zabezpieczeń systemu
Lib__Security::secure_uri();

//nadanie id_cookie uzytkownikowi
$id_cookie = App__Ed__Model__Id_cookie::init();

//banowanie użytkowników
if(App__Ed__Model__Ban::is_user_banned())
{
	show_403();
}
//var_dump(App__Ed__Model__Encryption::decode("aWh3VGJmN2RML0dqYmtTdVIvL2s4UT09"));die;
//ładowanie kontrollerów
$router = new Lib__Router();
$router->setBasePath(BASE_DIR);
$router->controller_load();

//pasek deweloperski
if(!PRODUCTION)
{
    //czas ładowania strony
    $time = microtime();
    $time = explode(' ', $time);
    $time = $time[1] + $time[0];
    $finish_time_load = $time;
    unset($time);
    
    $debug_params = array(
        'time_load' => round(($finish_time_load - $start_time_load), 4),
        'memory_usage' => (memory_get_peak_usage(true)/1024/1024),
        'sql_queries' => Lib__Session::get('sql_queries'),
        'called_controler_name' => Lib__Session::get('called_controler_name')
    );
    
    //debugbar nie pokazuje się w ajaxach
    if(!is_request())
    {
        echo App__Ed__Model__Debug_Bar::init($debug_params);
    }
}