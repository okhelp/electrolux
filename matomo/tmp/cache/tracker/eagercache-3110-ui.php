<?php return array (
  'lifetime' => 1575571370,
  'data' => 
  array (
    'PluginCorePluginsAdminMetadata' => 
    array (
      'description' => 'CorePluginsAdmin_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.11.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginCoreAdminHomeMetadata' => 
    array (
      'description' => 'CoreAdminHome_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.11.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginCoreHomeMetadata' => 
    array (
      'description' => 'CoreHome_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.11.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginWebsiteMeasurableMetadata' => 
    array (
      'description' => 'Analytics for the web: lets you measure and analyze Websites.',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.11.0',
      'theme' => false,
      'require' => 
      array (
      ),
      'name' => 'WebsiteMeasurable',
    ),
    'PluginIntranetMeasurableMetadata' => 
    array (
      'description' => 'Analytics for the web: lets you measure and analyze intranet websites.',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.11.0',
      'theme' => false,
      'require' => 
      array (
      ),
      'name' => 'IntranetMeasurable',
    ),
    'PluginDiagnosticsMetadata' => 
    array (
      'description' => 'Performs diagnostics to check that Matomo is installed and runs correctly.',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.11.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginCoreVisualizationsMetadata' => 
    array (
      'description' => 'CoreVisualizations_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.11.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginProxyMetadata' => 
    array (
      'description' => 'Proxy services',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.11.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginAPIMetadata' => 
    array (
      'description' => 'API_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.11.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginExamplePluginMetadata' => 
    array (
      'description' => 'Matomo Platform showcase: how to create widgets, menus, scheduled tasks, a custom archiver, plugin tests, and an AngularJS component.',
      'homepage' => '',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'email' => '',
          'homepage' => '',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '0.1.0',
      'theme' => false,
      'require' => 
      array (
        'piwik' => '>=3.0.0-b1,<4.0.0-b1',
      ),
      'name' => 'ExamplePlugin',
      'support' => 
      array (
        'email' => '',
        'issues' => '',
        'forum' => '',
        'irc' => '',
        'wiki' => '',
        'source' => '',
        'docs' => '',
        'rss' => '',
      ),
      'keywords' => 
      array (
      ),
    ),
    'PluginWidgetizeMetadata' => 
    array (
      'description' => 'Widgetize_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.11.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginTransitionsMetadata' => 
    array (
      'description' => 'Transitions_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.11.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginLanguagesManagerMetadata' => 
    array (
      'description' => 'LanguagesManager_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.11.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginActionsMetadata' => 
    array (
      'description' => 'Actions_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.11.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginDashboardMetadata' => 
    array (
      'description' => 'Dashboard_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.11.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginMultiSitesMetadata' => 
    array (
      'description' => 'MultiSites_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.11.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginReferrersMetadata' => 
    array (
      'description' => 'Referrers_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.11.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginUserLanguageMetadata' => 
    array (
      'description' => 'UserLanguage_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.11.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginDevicesDetectionMetadata' => 
    array (
      'description' => 'DevicesDetection_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.11.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginGoalsMetadata' => 
    array (
      'description' => 'Goals_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.11.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginEcommerceMetadata' => 
    array (
      'description' => 'Ecommerce_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.11.0',
      'theme' => false,
      'require' => 
      array (
      ),
      'name' => 'Ecommerce',
    ),
    'PluginSEOMetadata' => 
    array (
      'description' => 'SEO_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.11.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginEventsMetadata' => 
    array (
      'description' => 'Events_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.11.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginUserCountryMetadata' => 
    array (
      'description' => 'UserCountry_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.11.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginGeoIp2Metadata' => 
    array (
      'description' => 'GeoIp2_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.11.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginVisitsSummaryMetadata' => 
    array (
      'description' => 'VisitsSummary_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.11.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginVisitFrequencyMetadata' => 
    array (
      'description' => 'VisitFrequency_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.11.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginVisitTimeMetadata' => 
    array (
      'description' => 'VisitTime_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.11.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginVisitorInterestMetadata' => 
    array (
      'description' => 'VisitorInterest_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.11.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginExampleAPIMetadata' => 
    array (
      'description' => 'Matomo Platform showcase: how to create an API for your plugin to let your users export your data in multiple formats.',
      'homepage' => 'https://matomo.org',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'email' => 'hello@matomo.org',
          'homepage' => 'https://matomo.org',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '1.0',
      'theme' => false,
      'require' => 
      array (
      ),
      'name' => 'ExampleAPI',
      'keywords' => 
      array (
        0 => 'example',
        1 => 'api',
      ),
    ),
    'PluginRssWidgetMetadata' => 
    array (
      'description' => 'Matomo Platform showcase: how to create a new widget that displays a user submitted RSS feed.',
      'homepage' => 'https://matomo.org',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'email' => 'hello@matomo.org',
          'homepage' => 'https://matomo.org',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '1.0',
      'theme' => false,
      'require' => 
      array (
      ),
      'name' => 'RssWidget',
      'keywords' => 
      array (
        0 => 'example',
        1 => 'feed',
        2 => 'widget',
      ),
    ),
    'PluginFeedbackMetadata' => 
    array (
      'description' => 'Feedback_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.11.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginMonologMetadata' => 
    array (
      'description' => 'Adds logging capabilities to Matomo.',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.11.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginLoginMetadata' => 
    array (
      'description' => 'Login_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.11.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginTwoFactorAuthMetadata' => 
    array (
      'description' => 'TwoFactorAuth_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.11.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginUsersManagerMetadata' => 
    array (
      'description' => 'UsersManager_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.11.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginSitesManagerMetadata' => 
    array (
      'description' => 'SitesManager_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.11.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginInstallationMetadata' => 
    array (
      'description' => 'Installation_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.11.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginCoreUpdaterMetadata' => 
    array (
      'description' => 'CoreUpdater_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.11.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginCoreConsoleMetadata' => 
    array (
      'description' => 'CoreConsole_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.11.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginScheduledReportsMetadata' => 
    array (
      'description' => 'ScheduledReports_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.11.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginUserCountryMapMetadata' => 
    array (
      'description' => 'UserCountryMap_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.11.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginLiveMetadata' => 
    array (
      'description' => 'Live_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.11.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginCustomVariablesMetadata' => 
    array (
      'description' => 'CustomVariables_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.11.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginPrivacyManagerMetadata' => 
    array (
      'description' => 'PrivacyManager_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.11.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginImageGraphMetadata' => 
    array (
      'description' => 'ImageGraph_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.11.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginAnnotationsMetadata' => 
    array (
      'description' => 'Annotations_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.11.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginMobileMessagingMetadata' => 
    array (
      'description' => 'MobileMessaging_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.11.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginOverlayMetadata' => 
    array (
      'description' => 'Overlay_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.11.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginSegmentEditorMetadata' => 
    array (
      'description' => 'SegmentEditor_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.11.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginInsightsMetadata' => 
    array (
      'description' => 'Insights_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.11.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginMorpheusMetadata' => 
    array (
      'description' => 'Morpheus is the default theme of Matomo 3 designed to help you focus on your analytics. In Greek mythology, Morpheus is the God of dreams. In the Matrix movie, Morpheus is the leader of the rebel forces who fight to awaken humans from a dreamlike reality called The Matrix. ',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.11.0',
      'theme' => true,
      'require' => 
      array (
      ),
      'name' => 'Morpheus',
      'stylesheet' => 'stylesheets/main.less',
    ),
    'PluginContentsMetadata' => 
    array (
      'description' => 'Contents_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.11.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginBulkTrackingMetadata' => 
    array (
      'description' => 'Provides ability to send several Tracking API requests in one bulk request. Makes importing a lot of data in Matomo faster.',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.11.0',
      'theme' => false,
      'require' => 
      array (
      ),
      'name' => 'BulkTracking',
    ),
    'PluginResolutionMetadata' => 
    array (
      'description' => 'Resolution_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.11.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginDevicePluginsMetadata' => 
    array (
      'description' => 'DevicePlugins_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.11.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginHeartbeatMetadata' => 
    array (
      'description' => 'Handles ping tracker requests.',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.11.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginIntlMetadata' => 
    array (
      'description' => 'Intl_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.11.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginMarketplaceMetadata' => 
    array (
      'description' => 'Marketplace_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.11.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginProfessionalServicesMetadata' => 
    array (
      'description' => 'Provides widgets to learn about Professional services and products for Matomo.',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.11.0',
      'theme' => false,
      'require' => 
      array (
      ),
      'name' => 'ProfessionalServices',
    ),
    'PluginUserIdMetadata' => 
    array (
      'description' => 'UserId_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.11.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginCustomPiwikJsMetadata' => 
    array (
      'description' => 'CustomPiwikJs_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.11.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginTourMetadata' => 
    array (
      'description' => 'Tour_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.11.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginTagManagerMetadata' => 
    array (
      'description' => 'TagManager_PluginDescription',
      'homepage' => 'https://matomo.org',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'email' => 'hello@matomo.org',
          'homepage' => 'https://matomo.org',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '0.2.6',
      'theme' => false,
      'require' => 
      array (
        'piwik' => '>=3.6.0-b4,<4.0.0-b1',
      ),
      'name' => 'TagManager',
      'support' => 
      array (
        'email' => 'hello@matomo.org',
        'issues' => 'https://github.com/matomo-org/tag-manager/issues',
        'forum' => 'https://forum.matomo.org',
        'irc' => '',
        'wiki' => 'https://github.com/matomo-org/tag-manager/wiki',
        'source' => 'https://github.com/matomo-org/tag-manager',
        'docs' => 'https://matomo.org/docs/tag-manager',
        'rss' => '',
      ),
      'keywords' => 
      array (
        0 => 'tag',
        1 => 'manager',
        2 => 'script',
        3 => 'analytics',
        4 => 'marketing',
        5 => 'trigger',
        6 => 'variable',
        7 => 'container',
        8 => 'embed',
        9 => 'tracking',
      ),
      'license_file' => '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/LICENSE',
    ),
    'PluginCorePluginsAdminColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginCoreAdminHomeColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginCoreHomeColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/CoreHome/Columns/IdSite.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\IdSite',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/CoreHome/Columns/UserId.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\UserId',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/CoreHome/Columns/VisitFirstActionMinute.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\VisitFirstActionMinute',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/CoreHome/Columns/VisitFirstActionTime.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\VisitFirstActionTime',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/CoreHome/Columns/VisitGoalBuyer.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\VisitGoalBuyer',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/CoreHome/Columns/VisitGoalConverted.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\VisitGoalConverted',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/CoreHome/Columns/VisitId.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\VisitId',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/CoreHome/Columns/VisitIp.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\VisitIp',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/CoreHome/Columns/VisitLastActionDate.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\VisitLastActionDate',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/CoreHome/Columns/VisitLastActionDayOfMonth.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\VisitLastActionDayOfMonth',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/CoreHome/Columns/VisitLastActionDayOfWeek.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\VisitLastActionDayOfWeek',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/CoreHome/Columns/VisitLastActionDayOfYear.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\VisitLastActionDayOfYear',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/CoreHome/Columns/VisitLastActionMinute.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\VisitLastActionMinute',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/CoreHome/Columns/VisitLastActionMonth.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\VisitLastActionMonth',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/CoreHome/Columns/VisitLastActionQuarter.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\VisitLastActionQuarter',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/CoreHome/Columns/VisitLastActionSecond.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\VisitLastActionSecond',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/CoreHome/Columns/VisitLastActionTime.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\VisitLastActionTime',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/CoreHome/Columns/VisitLastActionWeekOfYear.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\VisitLastActionWeekOfYear',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/CoreHome/Columns/VisitLastActionYear.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\VisitLastActionYear',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/CoreHome/Columns/VisitTotalTime.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\VisitTotalTime',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/CoreHome/Columns/VisitorDaysSinceFirst.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\VisitorDaysSinceFirst',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/CoreHome/Columns/VisitorDaysSinceOrder.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\VisitorDaysSinceOrder',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/CoreHome/Columns/VisitorFingerprint.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\VisitorFingerprint',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/CoreHome/Columns/VisitorId.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\VisitorId',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/CoreHome/Columns/VisitorReturning.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\VisitorReturning',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/CoreHome/Columns/VisitsCount.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\VisitsCount',
    ),
    'PluginWebsiteMeasurableColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginIntranetMeasurableColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginDiagnosticsColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginCoreVisualizationsColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginProxyColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginAPIColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginExamplePluginColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginWidgetizeColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginTransitionsColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginLanguagesManagerColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginActionsColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Actions/Columns/EntryPageTitle.php' => 'Piwik\\Plugins\\Actions\\Columns\\EntryPageTitle',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Actions/Columns/EntryPageUrl.php' => 'Piwik\\Plugins\\Actions\\Columns\\EntryPageUrl',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Actions/Columns/ExitPageTitle.php' => 'Piwik\\Plugins\\Actions\\Columns\\ExitPageTitle',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Actions/Columns/ExitPageUrl.php' => 'Piwik\\Plugins\\Actions\\Columns\\ExitPageUrl',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Actions/Columns/VisitTotalActions.php' => 'Piwik\\Plugins\\Actions\\Columns\\VisitTotalActions',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Actions/Columns/VisitTotalInteractions.php' => 'Piwik\\Plugins\\Actions\\Columns\\VisitTotalInteractions',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Actions/Columns/VisitTotalSearches.php' => 'Piwik\\Plugins\\Actions\\Columns\\VisitTotalSearches',
    ),
    'PluginDashboardColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginMultiSitesColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginReferrersColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Referrers/Columns/Campaign.php' => 'Piwik\\Plugins\\Referrers\\Columns\\Campaign',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Referrers/Columns/Keyword.php' => 'Piwik\\Plugins\\Referrers\\Columns\\Keyword',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Referrers/Columns/ReferrerName.php' => 'Piwik\\Plugins\\Referrers\\Columns\\ReferrerName',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Referrers/Columns/ReferrerType.php' => 'Piwik\\Plugins\\Referrers\\Columns\\ReferrerType',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Referrers/Columns/ReferrerUrl.php' => 'Piwik\\Plugins\\Referrers\\Columns\\ReferrerUrl',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Referrers/Columns/Website.php' => 'Piwik\\Plugins\\Referrers\\Columns\\Website',
    ),
    'PluginUserLanguageColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/UserLanguage/Columns/Language.php' => 'Piwik\\Plugins\\UserLanguage\\Columns\\Language',
    ),
    'PluginDevicesDetectionColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/DevicesDetection/Columns/BrowserEngine.php' => 'Piwik\\Plugins\\DevicesDetection\\Columns\\BrowserEngine',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/DevicesDetection/Columns/BrowserName.php' => 'Piwik\\Plugins\\DevicesDetection\\Columns\\BrowserName',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/DevicesDetection/Columns/BrowserVersion.php' => 'Piwik\\Plugins\\DevicesDetection\\Columns\\BrowserVersion',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/DevicesDetection/Columns/DeviceBrand.php' => 'Piwik\\Plugins\\DevicesDetection\\Columns\\DeviceBrand',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/DevicesDetection/Columns/DeviceModel.php' => 'Piwik\\Plugins\\DevicesDetection\\Columns\\DeviceModel',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/DevicesDetection/Columns/DeviceType.php' => 'Piwik\\Plugins\\DevicesDetection\\Columns\\DeviceType',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/DevicesDetection/Columns/Os.php' => 'Piwik\\Plugins\\DevicesDetection\\Columns\\Os',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/DevicesDetection/Columns/OsVersion.php' => 'Piwik\\Plugins\\DevicesDetection\\Columns\\OsVersion',
    ),
    'PluginGoalsColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginEcommerceColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginSEOColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginEventsColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Events/Columns/TotalEvents.php' => 'Piwik\\Plugins\\Events\\Columns\\TotalEvents',
    ),
    'PluginUserCountryColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/UserCountry/Columns/City.php' => 'Piwik\\Plugins\\UserCountry\\Columns\\City',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/UserCountry/Columns/Country.php' => 'Piwik\\Plugins\\UserCountry\\Columns\\Country',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/UserCountry/Columns/Latitude.php' => 'Piwik\\Plugins\\UserCountry\\Columns\\Latitude',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/UserCountry/Columns/Longitude.php' => 'Piwik\\Plugins\\UserCountry\\Columns\\Longitude',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/UserCountry/Columns/Provider.php' => 'Piwik\\Plugins\\UserCountry\\Columns\\Provider',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/UserCountry/Columns/Region.php' => 'Piwik\\Plugins\\UserCountry\\Columns\\Region',
    ),
    'PluginGeoIp2Columns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/GeoIp2/Columns/Region.php' => 'Piwik\\Plugins\\GeoIp2\\Columns\\Region',
    ),
    'PluginVisitsSummaryColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginVisitFrequencyColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginVisitTimeColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/VisitTime/Columns/LocalMinute.php' => 'Piwik\\Plugins\\VisitTime\\Columns\\LocalMinute',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/VisitTime/Columns/LocalTime.php' => 'Piwik\\Plugins\\VisitTime\\Columns\\LocalTime',
    ),
    'PluginVisitorInterestColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/VisitorInterest/Columns/VisitsByDaysSinceLastVisit.php' => 'Piwik\\Plugins\\VisitorInterest\\Columns\\VisitsByDaysSinceLastVisit',
    ),
    'PluginExampleAPIColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginRssWidgetColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginFeedbackColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginMonologColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginLoginColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginTwoFactorAuthColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginUsersManagerColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginSitesManagerColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginInstallationColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginCoreUpdaterColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginCoreConsoleColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginScheduledReportsColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginUserCountryMapColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginLiveColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginCustomVariablesColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/CustomVariables/Columns/Base.php' => 'Piwik\\Plugins\\CustomVariables\\Columns\\Base',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/CustomVariables/Columns/CustomVariableName.php' => 'Piwik\\Plugins\\CustomVariables\\Columns\\CustomVariableName',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/CustomVariables/Columns/CustomVariableValue.php' => 'Piwik\\Plugins\\CustomVariables\\Columns\\CustomVariableValue',
    ),
    'PluginPrivacyManagerColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginImageGraphColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginAnnotationsColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginMobileMessagingColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginOverlayColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginSegmentEditorColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginInsightsColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginMorpheusColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginContentsColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginBulkTrackingColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginResolutionColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Resolution/Columns/Resolution.php' => 'Piwik\\Plugins\\Resolution\\Columns\\Resolution',
    ),
    'PluginDevicePluginsColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/DevicePlugins/Columns/PluginCookie.php' => 'Piwik\\Plugins\\DevicePlugins\\Columns\\PluginCookie',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/DevicePlugins/Columns/PluginDirector.php' => 'Piwik\\Plugins\\DevicePlugins\\Columns\\PluginDirector',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/DevicePlugins/Columns/PluginFlash.php' => 'Piwik\\Plugins\\DevicePlugins\\Columns\\PluginFlash',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/DevicePlugins/Columns/PluginGears.php' => 'Piwik\\Plugins\\DevicePlugins\\Columns\\PluginGears',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/DevicePlugins/Columns/PluginJava.php' => 'Piwik\\Plugins\\DevicePlugins\\Columns\\PluginJava',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/DevicePlugins/Columns/PluginPdf.php' => 'Piwik\\Plugins\\DevicePlugins\\Columns\\PluginPdf',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/DevicePlugins/Columns/PluginQuickTime.php' => 'Piwik\\Plugins\\DevicePlugins\\Columns\\PluginQuickTime',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/DevicePlugins/Columns/PluginRealPlayer.php' => 'Piwik\\Plugins\\DevicePlugins\\Columns\\PluginRealPlayer',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/DevicePlugins/Columns/PluginSilverlight.php' => 'Piwik\\Plugins\\DevicePlugins\\Columns\\PluginSilverlight',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/DevicePlugins/Columns/PluginWindowsMedia.php' => 'Piwik\\Plugins\\DevicePlugins\\Columns\\PluginWindowsMedia',
    ),
    'PluginHeartbeatColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginIntlColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginMarketplaceColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginProfessionalServicesColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginUserIdColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/UserId/Columns/UserId.php' => 'Piwik\\Plugins\\UserId\\Columns\\UserId',
    ),
    'PluginCustomPiwikJsColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginTourColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginTagManagerColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginCorePluginsAdminColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginCoreAdminHomeColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginCoreHomeColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/CoreHome/Columns/LinkVisitActionId.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\LinkVisitActionId',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/CoreHome/Columns/LinkVisitActionIdPages.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\LinkVisitActionIdPages',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/CoreHome/Columns/ServerMinute.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\ServerMinute',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/CoreHome/Columns/ServerTime.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\ServerTime',
    ),
    'PluginWebsiteMeasurableColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginIntranetMeasurableColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginDiagnosticsColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginCoreVisualizationsColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginProxyColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginAPIColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginExamplePluginColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginWidgetizeColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginTransitionsColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginLanguagesManagerColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginActionsColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Actions/Columns/ActionType.php' => 'Piwik\\Plugins\\Actions\\Columns\\ActionType',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Actions/Columns/ActionUrl.php' => 'Piwik\\Plugins\\Actions\\Columns\\ActionUrl',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Actions/Columns/ClickedUrl.php' => 'Piwik\\Plugins\\Actions\\Columns\\ClickedUrl',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Actions/Columns/DownloadUrl.php' => 'Piwik\\Plugins\\Actions\\Columns\\DownloadUrl',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Actions/Columns/IdPageview.php' => 'Piwik\\Plugins\\Actions\\Columns\\IdPageview',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Actions/Columns/InteractionPosition.php' => 'Piwik\\Plugins\\Actions\\Columns\\InteractionPosition',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Actions/Columns/PageGenerationTime.php' => 'Piwik\\Plugins\\Actions\\Columns\\PageGenerationTime',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Actions/Columns/PageTitle.php' => 'Piwik\\Plugins\\Actions\\Columns\\PageTitle',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Actions/Columns/PageUrl.php' => 'Piwik\\Plugins\\Actions\\Columns\\PageUrl',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Actions/Columns/SearchKeyword.php' => 'Piwik\\Plugins\\Actions\\Columns\\SearchKeyword',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Actions/Columns/TimeSpentRefAction.php' => 'Piwik\\Plugins\\Actions\\Columns\\TimeSpentRefAction',
    ),
    'PluginDashboardColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginMultiSitesColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginReferrersColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginUserLanguageColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginDevicesDetectionColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginGoalsColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginEcommerceColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginSEOColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginEventsColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Events/Columns/EventAction.php' => 'Piwik\\Plugins\\Events\\Columns\\EventAction',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Events/Columns/EventCategory.php' => 'Piwik\\Plugins\\Events\\Columns\\EventCategory',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Events/Columns/EventName.php' => 'Piwik\\Plugins\\Events\\Columns\\EventName',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Events/Columns/EventUrl.php' => 'Piwik\\Plugins\\Events\\Columns\\EventUrl',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Events/Columns/EventValue.php' => 'Piwik\\Plugins\\Events\\Columns\\EventValue',
    ),
    'PluginUserCountryColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginGeoIp2Columns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginVisitsSummaryColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginVisitFrequencyColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginVisitTimeColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginVisitorInterestColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginExampleAPIColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginRssWidgetColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginFeedbackColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginMonologColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginLoginColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginTwoFactorAuthColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginUsersManagerColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginSitesManagerColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginInstallationColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginCoreUpdaterColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginCoreConsoleColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginScheduledReportsColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginUserCountryMapColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginLiveColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginCustomVariablesColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/CustomVariables/Columns/SearchCategory.php' => 'Piwik\\Plugins\\CustomVariables\\Columns\\SearchCategory',
    ),
    'PluginPrivacyManagerColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginImageGraphColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginAnnotationsColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginMobileMessagingColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginOverlayColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginSegmentEditorColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginInsightsColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginMorpheusColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginContentsColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Contents/Columns/ContentInteraction.php' => 'Piwik\\Plugins\\Contents\\Columns\\ContentInteraction',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Contents/Columns/ContentName.php' => 'Piwik\\Plugins\\Contents\\Columns\\ContentName',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Contents/Columns/ContentPiece.php' => 'Piwik\\Plugins\\Contents\\Columns\\ContentPiece',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Contents/Columns/ContentTarget.php' => 'Piwik\\Plugins\\Contents\\Columns\\ContentTarget',
    ),
    'PluginBulkTrackingColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginResolutionColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginDevicePluginsColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginHeartbeatColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginIntlColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginMarketplaceColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginProfessionalServicesColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginUserIdColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginCustomPiwikJsColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginTourColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginTagManagerColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginCorePluginsAdminColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginCoreAdminHomeColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginCoreHomeColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginWebsiteMeasurableColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginIntranetMeasurableColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginDiagnosticsColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginCoreVisualizationsColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginProxyColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginAPIColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginExamplePluginColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginWidgetizeColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginTransitionsColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginLanguagesManagerColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginActionsColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginDashboardColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginMultiSitesColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginReferrersColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginUserLanguageColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginDevicesDetectionColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginGoalsColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Goals/Columns/IdGoal.php' => 'Piwik\\Plugins\\Goals\\Columns\\IdGoal',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Goals/Columns/Revenue.php' => 'Piwik\\Plugins\\Goals\\Columns\\Revenue',
    ),
    'PluginEcommerceColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Ecommerce/Columns/Items.php' => 'Piwik\\Plugins\\Ecommerce\\Columns\\Items',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Ecommerce/Columns/Order.php' => 'Piwik\\Plugins\\Ecommerce\\Columns\\Order',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Ecommerce/Columns/Revenue.php' => 'Piwik\\Plugins\\Ecommerce\\Columns\\Revenue',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Ecommerce/Columns/RevenueDiscount.php' => 'Piwik\\Plugins\\Ecommerce\\Columns\\RevenueDiscount',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Ecommerce/Columns/RevenueShipping.php' => 'Piwik\\Plugins\\Ecommerce\\Columns\\RevenueShipping',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Ecommerce/Columns/RevenueSubtotal.php' => 'Piwik\\Plugins\\Ecommerce\\Columns\\RevenueSubtotal',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Ecommerce/Columns/RevenueTax.php' => 'Piwik\\Plugins\\Ecommerce\\Columns\\RevenueTax',
    ),
    'PluginSEOColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginEventsColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginUserCountryColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginGeoIp2Columns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginVisitsSummaryColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginVisitFrequencyColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginVisitTimeColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginVisitorInterestColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginExampleAPIColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginRssWidgetColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginFeedbackColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginMonologColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginLoginColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginTwoFactorAuthColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginUsersManagerColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginSitesManagerColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginInstallationColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginCoreUpdaterColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginCoreConsoleColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginScheduledReportsColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginUserCountryMapColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginLiveColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginCustomVariablesColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginPrivacyManagerColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginImageGraphColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginAnnotationsColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginMobileMessagingColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginOverlayColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginSegmentEditorColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginInsightsColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginMorpheusColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginContentsColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginBulkTrackingColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginResolutionColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginDevicePluginsColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginHeartbeatColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginIntlColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginMarketplaceColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginProfessionalServicesColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginUserIdColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginCustomPiwikJsColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginTourColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginTagManagerColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'AllDimensionModifyTime' => 
    array (
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Actions/Columns/ActionType.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Actions/Columns/ActionUrl.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Actions/Columns/ClickedUrl.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Actions/Columns/DestinationPage.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Actions/Columns/DownloadUrl.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Actions/Columns/EntryPageTitle.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Actions/Columns/EntryPageUrl.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Actions/Columns/ExitPageTitle.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Actions/Columns/ExitPageUrl.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Actions/Columns/IdPageview.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Actions/Columns/InteractionPosition.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Actions/Columns/Keyword.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Actions/Columns/KeywordwithNoSearchResult.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Actions/Columns/Metrics/AveragePageGenerationTime.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Actions/Columns/Metrics/AverageTimeOnPage.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Actions/Columns/Metrics/BounceRate.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Actions/Columns/Metrics/ExitRate.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Actions/Columns/PageGenerationTime.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Actions/Columns/PageTitle.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Actions/Columns/PageUrl.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Actions/Columns/SearchCategory.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Actions/Columns/SearchDestinationPage.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Actions/Columns/SearchKeyword.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Actions/Columns/SearchNoResultKeyword.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Actions/Columns/TimeSpentRefAction.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Actions/Columns/VisitTotalActions.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Actions/Columns/VisitTotalInteractions.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Actions/Columns/VisitTotalSearches.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Contents/Columns/ContentInteraction.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Contents/Columns/ContentName.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Contents/Columns/ContentPiece.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Contents/Columns/ContentTarget.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Contents/Columns/Metrics/InteractionRate.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/CoreHome/Columns/IdSite.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/CoreHome/Columns/LinkVisitActionId.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/CoreHome/Columns/LinkVisitActionIdPages.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/CoreHome/Columns/Metrics/ActionsPerVisit.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/CoreHome/Columns/Metrics/AverageTimeOnSite.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/CoreHome/Columns/Metrics/BounceRate.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/CoreHome/Columns/Metrics/CallableProcessedMetric.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/CoreHome/Columns/Metrics/ConversionRate.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/CoreHome/Columns/Metrics/EvolutionMetric.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/CoreHome/Columns/Metrics/VisitsPercent.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/CoreHome/Columns/ServerMinute.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/CoreHome/Columns/ServerTime.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/CoreHome/Columns/UserId.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/CoreHome/Columns/VisitFirstActionMinute.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/CoreHome/Columns/VisitFirstActionTime.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/CoreHome/Columns/VisitGoalBuyer.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/CoreHome/Columns/VisitGoalConverted.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/CoreHome/Columns/VisitId.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/CoreHome/Columns/VisitIp.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/CoreHome/Columns/VisitLastActionDate.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/CoreHome/Columns/VisitLastActionDayOfMonth.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/CoreHome/Columns/VisitLastActionDayOfWeek.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/CoreHome/Columns/VisitLastActionDayOfYear.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/CoreHome/Columns/VisitLastActionMinute.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/CoreHome/Columns/VisitLastActionMonth.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/CoreHome/Columns/VisitLastActionQuarter.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/CoreHome/Columns/VisitLastActionSecond.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/CoreHome/Columns/VisitLastActionTime.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/CoreHome/Columns/VisitLastActionWeekOfYear.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/CoreHome/Columns/VisitLastActionYear.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/CoreHome/Columns/VisitTotalTime.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/CoreHome/Columns/VisitorDaysSinceFirst.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/CoreHome/Columns/VisitorDaysSinceOrder.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/CoreHome/Columns/VisitorFingerprint.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/CoreHome/Columns/VisitorId.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/CoreHome/Columns/VisitorReturning.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/CoreHome/Columns/VisitsCount.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/CustomVariables/Columns/Base.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/CustomVariables/Columns/CustomVariableName.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/CustomVariables/Columns/CustomVariableValue.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/CustomVariables/Columns/SearchCategory.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/DevicePlugins/Columns/DevicePluginColumn.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/DevicePlugins/Columns/Plugin.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/DevicePlugins/Columns/PluginCookie.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/DevicePlugins/Columns/PluginDirector.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/DevicePlugins/Columns/PluginFlash.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/DevicePlugins/Columns/PluginGears.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/DevicePlugins/Columns/PluginJava.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/DevicePlugins/Columns/PluginPdf.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/DevicePlugins/Columns/PluginQuickTime.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/DevicePlugins/Columns/PluginRealPlayer.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/DevicePlugins/Columns/PluginSilverlight.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/DevicePlugins/Columns/PluginWindowsMedia.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/DevicesDetection/Columns/Base.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/DevicesDetection/Columns/BrowserEngine.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/DevicesDetection/Columns/BrowserName.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/DevicesDetection/Columns/BrowserVersion.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/DevicesDetection/Columns/DeviceBrand.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/DevicesDetection/Columns/DeviceModel.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/DevicesDetection/Columns/DeviceType.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/DevicesDetection/Columns/Os.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/DevicesDetection/Columns/OsVersion.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Ecommerce/Columns/BaseConversion.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Ecommerce/Columns/Items.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Ecommerce/Columns/Order.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Ecommerce/Columns/ProductCategory.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Ecommerce/Columns/ProductName.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Ecommerce/Columns/ProductPrice.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Ecommerce/Columns/ProductQuantity.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Ecommerce/Columns/ProductSku.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Ecommerce/Columns/Revenue.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Ecommerce/Columns/RevenueDiscount.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Ecommerce/Columns/RevenueShipping.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Ecommerce/Columns/RevenueSubtotal.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Ecommerce/Columns/RevenueTax.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Events/Columns/EventAction.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Events/Columns/EventCategory.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Events/Columns/EventName.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Events/Columns/EventUrl.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Events/Columns/EventValue.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Events/Columns/Metrics/AverageEventValue.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Events/Columns/TotalEvents.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/ExampleLogTables/Columns/GroupAttributeAdmin.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/ExampleLogTables/Columns/UserAttributeGender.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/ExampleTracker/Columns/ExampleActionDimension.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/ExampleTracker/Columns/ExampleConversionDimension.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/ExampleTracker/Columns/ExampleDimension.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/ExampleTracker/Columns/ExampleVisitDimension.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/GeoIp2/Columns/Region.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Goals/Columns/DaysToConversion.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Goals/Columns/IdGoal.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Goals/Columns/Metrics/AverageOrderRevenue.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Goals/Columns/Metrics/AveragePrice.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Goals/Columns/Metrics/AverageQuantity.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Goals/Columns/Metrics/GoalConversionRate.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Goals/Columns/Metrics/GoalSpecific/AverageOrderRevenue.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Goals/Columns/Metrics/GoalSpecific/ConversionRate.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Goals/Columns/Metrics/GoalSpecific/Conversions.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Goals/Columns/Metrics/GoalSpecific/ItemsCount.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Goals/Columns/Metrics/GoalSpecific/Revenue.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Goals/Columns/Metrics/GoalSpecific/RevenuePerVisit.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Goals/Columns/Metrics/GoalSpecificProcessedMetric.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Goals/Columns/Metrics/ProductConversionRate.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Goals/Columns/Metrics/RevenuePerVisit.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Goals/Columns/Revenue.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Goals/Columns/VisitsUntilConversion.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/MultiSites/Columns/Metrics/EcommerceOnlyEvolutionMetric.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/MultiSites/Columns/Website.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Provider/Columns/Provider.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Referrers/Columns/Base.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Referrers/Columns/Campaign.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Referrers/Columns/Keyword.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Referrers/Columns/Referrer.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Referrers/Columns/ReferrerName.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Referrers/Columns/ReferrerType.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Referrers/Columns/ReferrerUrl.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Referrers/Columns/SearchEngine.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Referrers/Columns/SocialNetwork.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Referrers/Columns/Website.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Referrers/Columns/WebsitePage.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Resolution/Columns/Configuration.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/Resolution/Columns/Resolution.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/UserCountry/Columns/Base.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/UserCountry/Columns/City.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/UserCountry/Columns/Continent.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/UserCountry/Columns/Country.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/UserCountry/Columns/Latitude.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/UserCountry/Columns/Longitude.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/UserCountry/Columns/Provider.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/UserCountry/Columns/Region.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/UserId/Columns/UserId.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/UserLanguage/Columns/Language.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/VisitFrequency/Columns/Metrics/ReturningMetric.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/VisitTime/Columns/DayOfTheWeek.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/VisitTime/Columns/LocalMinute.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/VisitTime/Columns/LocalTime.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/VisitorInterest/Columns/PagesPerVisit.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/VisitorInterest/Columns/VisitDuration.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/VisitorInterest/Columns/VisitsByDaysSinceLastVisit.php' => 1569830915,
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/VisitorInterest/Columns/VisitsbyVisitNumber.php' => 1569830915,
    ),
    'PluginCorePluginsAdminRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginCoreAdminHomeRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginCoreHomeRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginWebsiteMeasurableRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginIntranetMeasurableRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginDiagnosticsRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginCoreVisualizationsRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginProxyRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginAPIRendererPiwik\\API\\ApiRenderer' => 
    array (
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/API/Renderer/Console.php' => 'Piwik\\Plugins\\API\\Renderer\\Console',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/API/Renderer/Csv.php' => 'Piwik\\Plugins\\API\\Renderer\\Csv',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/API/Renderer/Html.php' => 'Piwik\\Plugins\\API\\Renderer\\Html',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/API/Renderer/Json.php' => 'Piwik\\Plugins\\API\\Renderer\\Json',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/API/Renderer/Json2.php' => 'Piwik\\Plugins\\API\\Renderer\\Json2',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/API/Renderer/Original.php' => 'Piwik\\Plugins\\API\\Renderer\\Original',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/API/Renderer/Php.php' => 'Piwik\\Plugins\\API\\Renderer\\Php',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/API/Renderer/Rss.php' => 'Piwik\\Plugins\\API\\Renderer\\Rss',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/API/Renderer/Tsv.php' => 'Piwik\\Plugins\\API\\Renderer\\Tsv',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/API/Renderer/Xml.php' => 'Piwik\\Plugins\\API\\Renderer\\Xml',
    ),
    'PluginExamplePluginRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginWidgetizeRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginTransitionsRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginLanguagesManagerRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginActionsRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginDashboardRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginMultiSitesRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginReferrersRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginUserLanguageRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginDevicesDetectionRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginGoalsRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginEcommerceRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginSEORendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginEventsRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginUserCountryRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginGeoIp2RendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginVisitsSummaryRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginVisitFrequencyRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginVisitTimeRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginVisitorInterestRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginExampleAPIRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginRssWidgetRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginFeedbackRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginMonologRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginLoginRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginTwoFactorAuthRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginUsersManagerRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginSitesManagerRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginInstallationRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginCoreUpdaterRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginCoreConsoleRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginScheduledReportsRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginUserCountryMapRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginLiveRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginCustomVariablesRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginPrivacyManagerRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginImageGraphRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginAnnotationsRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginMobileMessagingRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginOverlayRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginSegmentEditorRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginInsightsRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginMorpheusRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginContentsRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginBulkTrackingRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginResolutionRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginDevicePluginsRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginHeartbeatRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginIntlRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginMarketplaceRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginProfessionalServicesRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginUserIdRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginCustomPiwikJsRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginTourRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginTagManagerRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginCorePluginsAdminTasksPiwik\\Plugin\\Tasks' => false,
    'PluginCoreAdminHomeTasksPiwik\\Plugin\\Tasks' => 'Piwik\\Plugins\\CoreAdminHome\\Tasks',
    'PluginCoreHomeTasksPiwik\\Plugin\\Tasks' => false,
    'PluginWebsiteMeasurableTasksPiwik\\Plugin\\Tasks' => false,
    'PluginIntranetMeasurableTasksPiwik\\Plugin\\Tasks' => false,
    'PluginDiagnosticsTasksPiwik\\Plugin\\Tasks' => false,
    'PluginCoreVisualizationsTasksPiwik\\Plugin\\Tasks' => false,
    'PluginProxyTasksPiwik\\Plugin\\Tasks' => false,
    'PluginAPITasksPiwik\\Plugin\\Tasks' => false,
    'PluginExamplePluginTasksPiwik\\Plugin\\Tasks' => 'Piwik\\Plugins\\ExamplePlugin\\Tasks',
    'PluginWidgetizeTasksPiwik\\Plugin\\Tasks' => false,
    'PluginTransitionsTasksPiwik\\Plugin\\Tasks' => false,
    'PluginLanguagesManagerTasksPiwik\\Plugin\\Tasks' => false,
    'PluginActionsTasksPiwik\\Plugin\\Tasks' => false,
    'PluginDashboardTasksPiwik\\Plugin\\Tasks' => false,
    'PluginMultiSitesTasksPiwik\\Plugin\\Tasks' => false,
    'PluginReferrersTasksPiwik\\Plugin\\Tasks' => 'Piwik\\Plugins\\Referrers\\Tasks',
    'PluginUserLanguageTasksPiwik\\Plugin\\Tasks' => false,
    'PluginDevicesDetectionTasksPiwik\\Plugin\\Tasks' => false,
    'PluginGoalsTasksPiwik\\Plugin\\Tasks' => false,
    'PluginEcommerceTasksPiwik\\Plugin\\Tasks' => false,
    'PluginSEOTasksPiwik\\Plugin\\Tasks' => false,
    'PluginEventsTasksPiwik\\Plugin\\Tasks' => false,
    'PluginUserCountryTasksPiwik\\Plugin\\Tasks' => 'Piwik\\Plugins\\UserCountry\\Tasks',
    'PluginGeoIp2TasksPiwik\\Plugin\\Tasks' => 'Piwik\\Plugins\\GeoIp2\\Tasks',
    'PluginVisitsSummaryTasksPiwik\\Plugin\\Tasks' => false,
    'PluginVisitFrequencyTasksPiwik\\Plugin\\Tasks' => false,
    'PluginVisitTimeTasksPiwik\\Plugin\\Tasks' => false,
    'PluginVisitorInterestTasksPiwik\\Plugin\\Tasks' => false,
    'PluginExampleAPITasksPiwik\\Plugin\\Tasks' => false,
    'PluginRssWidgetTasksPiwik\\Plugin\\Tasks' => false,
    'PluginFeedbackTasksPiwik\\Plugin\\Tasks' => false,
    'PluginMonologTasksPiwik\\Plugin\\Tasks' => false,
    'PluginLoginTasksPiwik\\Plugin\\Tasks' => 'Piwik\\Plugins\\Login\\Tasks',
    'PluginTwoFactorAuthTasksPiwik\\Plugin\\Tasks' => false,
    'PluginUsersManagerTasksPiwik\\Plugin\\Tasks' => 'Piwik\\Plugins\\UsersManager\\Tasks',
    'PluginSitesManagerTasksPiwik\\Plugin\\Tasks' => false,
    'PluginInstallationTasksPiwik\\Plugin\\Tasks' => false,
    'PluginCoreUpdaterTasksPiwik\\Plugin\\Tasks' => 'Piwik\\Plugins\\CoreUpdater\\Tasks',
    'PluginCoreConsoleTasksPiwik\\Plugin\\Tasks' => false,
    'PluginScheduledReportsTasksPiwik\\Plugin\\Tasks' => 'Piwik\\Plugins\\ScheduledReports\\Tasks',
    'PluginUserCountryMapTasksPiwik\\Plugin\\Tasks' => false,
    'PluginLiveTasksPiwik\\Plugin\\Tasks' => false,
    'PluginCustomVariablesTasksPiwik\\Plugin\\Tasks' => false,
    'PluginPrivacyManagerTasksPiwik\\Plugin\\Tasks' => 'Piwik\\Plugins\\PrivacyManager\\Tasks',
    'PluginImageGraphTasksPiwik\\Plugin\\Tasks' => false,
    'PluginAnnotationsTasksPiwik\\Plugin\\Tasks' => false,
    'PluginMobileMessagingTasksPiwik\\Plugin\\Tasks' => false,
    'PluginOverlayTasksPiwik\\Plugin\\Tasks' => false,
    'PluginSegmentEditorTasksPiwik\\Plugin\\Tasks' => false,
    'PluginInsightsTasksPiwik\\Plugin\\Tasks' => false,
    'PluginMorpheusTasksPiwik\\Plugin\\Tasks' => false,
    'PluginContentsTasksPiwik\\Plugin\\Tasks' => false,
    'PluginBulkTrackingTasksPiwik\\Plugin\\Tasks' => false,
    'PluginResolutionTasksPiwik\\Plugin\\Tasks' => false,
    'PluginDevicePluginsTasksPiwik\\Plugin\\Tasks' => false,
    'PluginHeartbeatTasksPiwik\\Plugin\\Tasks' => false,
    'PluginIntlTasksPiwik\\Plugin\\Tasks' => false,
    'PluginMarketplaceTasksPiwik\\Plugin\\Tasks' => 'Piwik\\Plugins\\Marketplace\\Tasks',
    'PluginProfessionalServicesTasksPiwik\\Plugin\\Tasks' => false,
    'PluginUserIdTasksPiwik\\Plugin\\Tasks' => false,
    'PluginCustomPiwikJsTasksPiwik\\Plugin\\Tasks' => 'Piwik\\Plugins\\CustomPiwikJs\\Tasks',
    'PluginTourTasksPiwik\\Plugin\\Tasks' => false,
    'PluginTagManagerTasksPiwik\\Plugin\\Tasks' => 'Piwik\\Plugins\\TagManager\\Tasks',
    'PluginCorePluginsAdminReleaseChannelPiwik\\UpdateCheck\\ReleaseChannel' => 
    array (
    ),
    'PluginCoreAdminHomeReleaseChannelPiwik\\UpdateCheck\\ReleaseChannel' => 
    array (
    ),
    'PluginCoreHomeReleaseChannelPiwik\\UpdateCheck\\ReleaseChannel' => 
    array (
    ),
    'PluginWebsiteMeasurableReleaseChannelPiwik\\UpdateCheck\\ReleaseChannel' => 
    array (
    ),
    'PluginIntranetMeasurableReleaseChannelPiwik\\UpdateCheck\\ReleaseChannel' => 
    array (
    ),
    'PluginDiagnosticsReleaseChannelPiwik\\UpdateCheck\\ReleaseChannel' => 
    array (
    ),
    'PluginCoreVisualizationsReleaseChannelPiwik\\UpdateCheck\\ReleaseChannel' => 
    array (
    ),
    'PluginProxyReleaseChannelPiwik\\UpdateCheck\\ReleaseChannel' => 
    array (
    ),
    'PluginAPIReleaseChannelPiwik\\UpdateCheck\\ReleaseChannel' => 
    array (
    ),
    'PluginExamplePluginReleaseChannelPiwik\\UpdateCheck\\ReleaseChannel' => 
    array (
    ),
    'PluginWidgetizeReleaseChannelPiwik\\UpdateCheck\\ReleaseChannel' => 
    array (
    ),
    'PluginTransitionsReleaseChannelPiwik\\UpdateCheck\\ReleaseChannel' => 
    array (
    ),
    'PluginLanguagesManagerReleaseChannelPiwik\\UpdateCheck\\ReleaseChannel' => 
    array (
    ),
    'PluginActionsReleaseChannelPiwik\\UpdateCheck\\ReleaseChannel' => 
    array (
    ),
    'PluginDashboardReleaseChannelPiwik\\UpdateCheck\\ReleaseChannel' => 
    array (
    ),
    'PluginMultiSitesReleaseChannelPiwik\\UpdateCheck\\ReleaseChannel' => 
    array (
    ),
    'PluginReferrersReleaseChannelPiwik\\UpdateCheck\\ReleaseChannel' => 
    array (
    ),
    'PluginUserLanguageReleaseChannelPiwik\\UpdateCheck\\ReleaseChannel' => 
    array (
    ),
    'PluginDevicesDetectionReleaseChannelPiwik\\UpdateCheck\\ReleaseChannel' => 
    array (
    ),
    'PluginGoalsReleaseChannelPiwik\\UpdateCheck\\ReleaseChannel' => 
    array (
    ),
    'PluginEcommerceReleaseChannelPiwik\\UpdateCheck\\ReleaseChannel' => 
    array (
    ),
    'PluginSEOReleaseChannelPiwik\\UpdateCheck\\ReleaseChannel' => 
    array (
    ),
    'PluginEventsReleaseChannelPiwik\\UpdateCheck\\ReleaseChannel' => 
    array (
    ),
    'PluginUserCountryReleaseChannelPiwik\\UpdateCheck\\ReleaseChannel' => 
    array (
    ),
    'PluginGeoIp2ReleaseChannelPiwik\\UpdateCheck\\ReleaseChannel' => 
    array (
    ),
    'PluginVisitsSummaryReleaseChannelPiwik\\UpdateCheck\\ReleaseChannel' => 
    array (
    ),
    'PluginVisitFrequencyReleaseChannelPiwik\\UpdateCheck\\ReleaseChannel' => 
    array (
    ),
    'PluginVisitTimeReleaseChannelPiwik\\UpdateCheck\\ReleaseChannel' => 
    array (
    ),
    'PluginVisitorInterestReleaseChannelPiwik\\UpdateCheck\\ReleaseChannel' => 
    array (
    ),
    'PluginExampleAPIReleaseChannelPiwik\\UpdateCheck\\ReleaseChannel' => 
    array (
    ),
    'PluginRssWidgetReleaseChannelPiwik\\UpdateCheck\\ReleaseChannel' => 
    array (
    ),
    'PluginFeedbackReleaseChannelPiwik\\UpdateCheck\\ReleaseChannel' => 
    array (
    ),
    'PluginMonologReleaseChannelPiwik\\UpdateCheck\\ReleaseChannel' => 
    array (
    ),
    'PluginLoginReleaseChannelPiwik\\UpdateCheck\\ReleaseChannel' => 
    array (
    ),
    'PluginTwoFactorAuthReleaseChannelPiwik\\UpdateCheck\\ReleaseChannel' => 
    array (
    ),
    'PluginUsersManagerReleaseChannelPiwik\\UpdateCheck\\ReleaseChannel' => 
    array (
    ),
    'PluginSitesManagerReleaseChannelPiwik\\UpdateCheck\\ReleaseChannel' => 
    array (
    ),
    'PluginInstallationReleaseChannelPiwik\\UpdateCheck\\ReleaseChannel' => 
    array (
    ),
    'PluginCoreUpdaterReleaseChannelPiwik\\UpdateCheck\\ReleaseChannel' => 
    array (
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/CoreUpdater/ReleaseChannel/Latest2XBeta.php' => 'Piwik\\Plugins\\CoreUpdater\\ReleaseChannel\\Latest2XBeta',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/CoreUpdater/ReleaseChannel/Latest2XStable.php' => 'Piwik\\Plugins\\CoreUpdater\\ReleaseChannel\\Latest2XStable',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/CoreUpdater/ReleaseChannel/LatestBeta.php' => 'Piwik\\Plugins\\CoreUpdater\\ReleaseChannel\\LatestBeta',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/CoreUpdater/ReleaseChannel/LatestStable.php' => 'Piwik\\Plugins\\CoreUpdater\\ReleaseChannel\\LatestStable',
    ),
    'PluginCoreConsoleReleaseChannelPiwik\\UpdateCheck\\ReleaseChannel' => 
    array (
    ),
    'PluginScheduledReportsReleaseChannelPiwik\\UpdateCheck\\ReleaseChannel' => 
    array (
    ),
    'PluginUserCountryMapReleaseChannelPiwik\\UpdateCheck\\ReleaseChannel' => 
    array (
    ),
    'PluginLiveReleaseChannelPiwik\\UpdateCheck\\ReleaseChannel' => 
    array (
    ),
    'PluginCustomVariablesReleaseChannelPiwik\\UpdateCheck\\ReleaseChannel' => 
    array (
    ),
    'PluginPrivacyManagerReleaseChannelPiwik\\UpdateCheck\\ReleaseChannel' => 
    array (
    ),
    'PluginImageGraphReleaseChannelPiwik\\UpdateCheck\\ReleaseChannel' => 
    array (
    ),
    'PluginAnnotationsReleaseChannelPiwik\\UpdateCheck\\ReleaseChannel' => 
    array (
    ),
    'PluginMobileMessagingReleaseChannelPiwik\\UpdateCheck\\ReleaseChannel' => 
    array (
    ),
    'PluginOverlayReleaseChannelPiwik\\UpdateCheck\\ReleaseChannel' => 
    array (
    ),
    'PluginSegmentEditorReleaseChannelPiwik\\UpdateCheck\\ReleaseChannel' => 
    array (
    ),
    'PluginInsightsReleaseChannelPiwik\\UpdateCheck\\ReleaseChannel' => 
    array (
    ),
    'PluginMorpheusReleaseChannelPiwik\\UpdateCheck\\ReleaseChannel' => 
    array (
    ),
    'PluginContentsReleaseChannelPiwik\\UpdateCheck\\ReleaseChannel' => 
    array (
    ),
    'PluginBulkTrackingReleaseChannelPiwik\\UpdateCheck\\ReleaseChannel' => 
    array (
    ),
    'PluginResolutionReleaseChannelPiwik\\UpdateCheck\\ReleaseChannel' => 
    array (
    ),
    'PluginDevicePluginsReleaseChannelPiwik\\UpdateCheck\\ReleaseChannel' => 
    array (
    ),
    'PluginHeartbeatReleaseChannelPiwik\\UpdateCheck\\ReleaseChannel' => 
    array (
    ),
    'PluginIntlReleaseChannelPiwik\\UpdateCheck\\ReleaseChannel' => 
    array (
    ),
    'PluginMarketplaceReleaseChannelPiwik\\UpdateCheck\\ReleaseChannel' => 
    array (
    ),
    'PluginProfessionalServicesReleaseChannelPiwik\\UpdateCheck\\ReleaseChannel' => 
    array (
    ),
    'PluginUserIdReleaseChannelPiwik\\UpdateCheck\\ReleaseChannel' => 
    array (
    ),
    'PluginCustomPiwikJsReleaseChannelPiwik\\UpdateCheck\\ReleaseChannel' => 
    array (
    ),
    'PluginTourReleaseChannelPiwik\\UpdateCheck\\ReleaseChannel' => 
    array (
    ),
    'PluginTagManagerReleaseChannelPiwik\\UpdateCheck\\ReleaseChannel' => 
    array (
    ),
    'PluginCorePluginsAdminContextPiwik\\Plugins\\TagManager\\Context\\BaseContext' => 
    array (
    ),
    'PluginCoreAdminHomeContextPiwik\\Plugins\\TagManager\\Context\\BaseContext' => 
    array (
    ),
    'PluginCoreHomeContextPiwik\\Plugins\\TagManager\\Context\\BaseContext' => 
    array (
    ),
    'PluginWebsiteMeasurableContextPiwik\\Plugins\\TagManager\\Context\\BaseContext' => 
    array (
    ),
    'PluginIntranetMeasurableContextPiwik\\Plugins\\TagManager\\Context\\BaseContext' => 
    array (
    ),
    'PluginDiagnosticsContextPiwik\\Plugins\\TagManager\\Context\\BaseContext' => 
    array (
    ),
    'PluginCoreVisualizationsContextPiwik\\Plugins\\TagManager\\Context\\BaseContext' => 
    array (
    ),
    'PluginProxyContextPiwik\\Plugins\\TagManager\\Context\\BaseContext' => 
    array (
    ),
    'PluginAPIContextPiwik\\Plugins\\TagManager\\Context\\BaseContext' => 
    array (
    ),
    'PluginExamplePluginContextPiwik\\Plugins\\TagManager\\Context\\BaseContext' => 
    array (
    ),
    'PluginWidgetizeContextPiwik\\Plugins\\TagManager\\Context\\BaseContext' => 
    array (
    ),
    'PluginTransitionsContextPiwik\\Plugins\\TagManager\\Context\\BaseContext' => 
    array (
    ),
    'PluginLanguagesManagerContextPiwik\\Plugins\\TagManager\\Context\\BaseContext' => 
    array (
    ),
    'PluginActionsContextPiwik\\Plugins\\TagManager\\Context\\BaseContext' => 
    array (
    ),
    'PluginDashboardContextPiwik\\Plugins\\TagManager\\Context\\BaseContext' => 
    array (
    ),
    'PluginMultiSitesContextPiwik\\Plugins\\TagManager\\Context\\BaseContext' => 
    array (
    ),
    'PluginReferrersContextPiwik\\Plugins\\TagManager\\Context\\BaseContext' => 
    array (
    ),
    'PluginUserLanguageContextPiwik\\Plugins\\TagManager\\Context\\BaseContext' => 
    array (
    ),
    'PluginDevicesDetectionContextPiwik\\Plugins\\TagManager\\Context\\BaseContext' => 
    array (
    ),
    'PluginGoalsContextPiwik\\Plugins\\TagManager\\Context\\BaseContext' => 
    array (
    ),
    'PluginEcommerceContextPiwik\\Plugins\\TagManager\\Context\\BaseContext' => 
    array (
    ),
    'PluginSEOContextPiwik\\Plugins\\TagManager\\Context\\BaseContext' => 
    array (
    ),
    'PluginEventsContextPiwik\\Plugins\\TagManager\\Context\\BaseContext' => 
    array (
    ),
    'PluginUserCountryContextPiwik\\Plugins\\TagManager\\Context\\BaseContext' => 
    array (
    ),
    'PluginGeoIp2ContextPiwik\\Plugins\\TagManager\\Context\\BaseContext' => 
    array (
    ),
    'PluginVisitsSummaryContextPiwik\\Plugins\\TagManager\\Context\\BaseContext' => 
    array (
    ),
    'PluginVisitFrequencyContextPiwik\\Plugins\\TagManager\\Context\\BaseContext' => 
    array (
    ),
    'PluginVisitTimeContextPiwik\\Plugins\\TagManager\\Context\\BaseContext' => 
    array (
    ),
    'PluginVisitorInterestContextPiwik\\Plugins\\TagManager\\Context\\BaseContext' => 
    array (
    ),
    'PluginExampleAPIContextPiwik\\Plugins\\TagManager\\Context\\BaseContext' => 
    array (
    ),
    'PluginRssWidgetContextPiwik\\Plugins\\TagManager\\Context\\BaseContext' => 
    array (
    ),
    'PluginFeedbackContextPiwik\\Plugins\\TagManager\\Context\\BaseContext' => 
    array (
    ),
    'PluginMonologContextPiwik\\Plugins\\TagManager\\Context\\BaseContext' => 
    array (
    ),
    'PluginLoginContextPiwik\\Plugins\\TagManager\\Context\\BaseContext' => 
    array (
    ),
    'PluginTwoFactorAuthContextPiwik\\Plugins\\TagManager\\Context\\BaseContext' => 
    array (
    ),
    'PluginUsersManagerContextPiwik\\Plugins\\TagManager\\Context\\BaseContext' => 
    array (
    ),
    'PluginSitesManagerContextPiwik\\Plugins\\TagManager\\Context\\BaseContext' => 
    array (
    ),
    'PluginInstallationContextPiwik\\Plugins\\TagManager\\Context\\BaseContext' => 
    array (
    ),
    'PluginCoreUpdaterContextPiwik\\Plugins\\TagManager\\Context\\BaseContext' => 
    array (
    ),
    'PluginCoreConsoleContextPiwik\\Plugins\\TagManager\\Context\\BaseContext' => 
    array (
    ),
    'PluginScheduledReportsContextPiwik\\Plugins\\TagManager\\Context\\BaseContext' => 
    array (
    ),
    'PluginUserCountryMapContextPiwik\\Plugins\\TagManager\\Context\\BaseContext' => 
    array (
    ),
    'PluginLiveContextPiwik\\Plugins\\TagManager\\Context\\BaseContext' => 
    array (
    ),
    'PluginCustomVariablesContextPiwik\\Plugins\\TagManager\\Context\\BaseContext' => 
    array (
    ),
    'PluginPrivacyManagerContextPiwik\\Plugins\\TagManager\\Context\\BaseContext' => 
    array (
    ),
    'PluginImageGraphContextPiwik\\Plugins\\TagManager\\Context\\BaseContext' => 
    array (
    ),
    'PluginAnnotationsContextPiwik\\Plugins\\TagManager\\Context\\BaseContext' => 
    array (
    ),
    'PluginMobileMessagingContextPiwik\\Plugins\\TagManager\\Context\\BaseContext' => 
    array (
    ),
    'PluginOverlayContextPiwik\\Plugins\\TagManager\\Context\\BaseContext' => 
    array (
    ),
    'PluginSegmentEditorContextPiwik\\Plugins\\TagManager\\Context\\BaseContext' => 
    array (
    ),
    'PluginInsightsContextPiwik\\Plugins\\TagManager\\Context\\BaseContext' => 
    array (
    ),
    'PluginMorpheusContextPiwik\\Plugins\\TagManager\\Context\\BaseContext' => 
    array (
    ),
    'PluginContentsContextPiwik\\Plugins\\TagManager\\Context\\BaseContext' => 
    array (
    ),
    'PluginBulkTrackingContextPiwik\\Plugins\\TagManager\\Context\\BaseContext' => 
    array (
    ),
    'PluginResolutionContextPiwik\\Plugins\\TagManager\\Context\\BaseContext' => 
    array (
    ),
    'PluginDevicePluginsContextPiwik\\Plugins\\TagManager\\Context\\BaseContext' => 
    array (
    ),
    'PluginHeartbeatContextPiwik\\Plugins\\TagManager\\Context\\BaseContext' => 
    array (
    ),
    'PluginIntlContextPiwik\\Plugins\\TagManager\\Context\\BaseContext' => 
    array (
    ),
    'PluginMarketplaceContextPiwik\\Plugins\\TagManager\\Context\\BaseContext' => 
    array (
    ),
    'PluginProfessionalServicesContextPiwik\\Plugins\\TagManager\\Context\\BaseContext' => 
    array (
    ),
    'PluginUserIdContextPiwik\\Plugins\\TagManager\\Context\\BaseContext' => 
    array (
    ),
    'PluginCustomPiwikJsContextPiwik\\Plugins\\TagManager\\Context\\BaseContext' => 
    array (
    ),
    'PluginTourContextPiwik\\Plugins\\TagManager\\Context\\BaseContext' => 
    array (
    ),
    'PluginTagManagerContextPiwik\\Plugins\\TagManager\\Context\\BaseContext' => 
    array (
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Context/AndroidContext.php' => 'Piwik\\Plugins\\TagManager\\Context\\AndroidContext',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Context/WebContext.php' => 'Piwik\\Plugins\\TagManager\\Context\\WebContext',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Context/iOSContext.php' => 'Piwik\\Plugins\\TagManager\\Context\\iOSContext',
    ),
    'PluginCorePluginsAdminTemplate/TagPiwik\\Plugins\\TagManager\\Template\\Tag\\BaseTag' => 
    array (
    ),
    'PluginCoreAdminHomeTemplate/TagPiwik\\Plugins\\TagManager\\Template\\Tag\\BaseTag' => 
    array (
    ),
    'PluginCoreHomeTemplate/TagPiwik\\Plugins\\TagManager\\Template\\Tag\\BaseTag' => 
    array (
    ),
    'PluginWebsiteMeasurableTemplate/TagPiwik\\Plugins\\TagManager\\Template\\Tag\\BaseTag' => 
    array (
    ),
    'PluginIntranetMeasurableTemplate/TagPiwik\\Plugins\\TagManager\\Template\\Tag\\BaseTag' => 
    array (
    ),
    'PluginDiagnosticsTemplate/TagPiwik\\Plugins\\TagManager\\Template\\Tag\\BaseTag' => 
    array (
    ),
    'PluginCoreVisualizationsTemplate/TagPiwik\\Plugins\\TagManager\\Template\\Tag\\BaseTag' => 
    array (
    ),
    'PluginProxyTemplate/TagPiwik\\Plugins\\TagManager\\Template\\Tag\\BaseTag' => 
    array (
    ),
    'PluginAPITemplate/TagPiwik\\Plugins\\TagManager\\Template\\Tag\\BaseTag' => 
    array (
    ),
    'PluginExamplePluginTemplate/TagPiwik\\Plugins\\TagManager\\Template\\Tag\\BaseTag' => 
    array (
    ),
    'PluginWidgetizeTemplate/TagPiwik\\Plugins\\TagManager\\Template\\Tag\\BaseTag' => 
    array (
    ),
    'PluginTransitionsTemplate/TagPiwik\\Plugins\\TagManager\\Template\\Tag\\BaseTag' => 
    array (
    ),
    'PluginLanguagesManagerTemplate/TagPiwik\\Plugins\\TagManager\\Template\\Tag\\BaseTag' => 
    array (
    ),
    'PluginActionsTemplate/TagPiwik\\Plugins\\TagManager\\Template\\Tag\\BaseTag' => 
    array (
    ),
    'PluginDashboardTemplate/TagPiwik\\Plugins\\TagManager\\Template\\Tag\\BaseTag' => 
    array (
    ),
    'PluginMultiSitesTemplate/TagPiwik\\Plugins\\TagManager\\Template\\Tag\\BaseTag' => 
    array (
    ),
    'PluginReferrersTemplate/TagPiwik\\Plugins\\TagManager\\Template\\Tag\\BaseTag' => 
    array (
    ),
    'PluginUserLanguageTemplate/TagPiwik\\Plugins\\TagManager\\Template\\Tag\\BaseTag' => 
    array (
    ),
    'PluginDevicesDetectionTemplate/TagPiwik\\Plugins\\TagManager\\Template\\Tag\\BaseTag' => 
    array (
    ),
    'PluginGoalsTemplate/TagPiwik\\Plugins\\TagManager\\Template\\Tag\\BaseTag' => 
    array (
    ),
    'PluginEcommerceTemplate/TagPiwik\\Plugins\\TagManager\\Template\\Tag\\BaseTag' => 
    array (
    ),
    'PluginSEOTemplate/TagPiwik\\Plugins\\TagManager\\Template\\Tag\\BaseTag' => 
    array (
    ),
    'PluginEventsTemplate/TagPiwik\\Plugins\\TagManager\\Template\\Tag\\BaseTag' => 
    array (
    ),
    'PluginUserCountryTemplate/TagPiwik\\Plugins\\TagManager\\Template\\Tag\\BaseTag' => 
    array (
    ),
    'PluginGeoIp2Template/TagPiwik\\Plugins\\TagManager\\Template\\Tag\\BaseTag' => 
    array (
    ),
    'PluginVisitsSummaryTemplate/TagPiwik\\Plugins\\TagManager\\Template\\Tag\\BaseTag' => 
    array (
    ),
    'PluginVisitFrequencyTemplate/TagPiwik\\Plugins\\TagManager\\Template\\Tag\\BaseTag' => 
    array (
    ),
    'PluginVisitTimeTemplate/TagPiwik\\Plugins\\TagManager\\Template\\Tag\\BaseTag' => 
    array (
    ),
    'PluginVisitorInterestTemplate/TagPiwik\\Plugins\\TagManager\\Template\\Tag\\BaseTag' => 
    array (
    ),
    'PluginExampleAPITemplate/TagPiwik\\Plugins\\TagManager\\Template\\Tag\\BaseTag' => 
    array (
    ),
    'PluginRssWidgetTemplate/TagPiwik\\Plugins\\TagManager\\Template\\Tag\\BaseTag' => 
    array (
    ),
    'PluginFeedbackTemplate/TagPiwik\\Plugins\\TagManager\\Template\\Tag\\BaseTag' => 
    array (
    ),
    'PluginMonologTemplate/TagPiwik\\Plugins\\TagManager\\Template\\Tag\\BaseTag' => 
    array (
    ),
    'PluginLoginTemplate/TagPiwik\\Plugins\\TagManager\\Template\\Tag\\BaseTag' => 
    array (
    ),
    'PluginTwoFactorAuthTemplate/TagPiwik\\Plugins\\TagManager\\Template\\Tag\\BaseTag' => 
    array (
    ),
    'PluginUsersManagerTemplate/TagPiwik\\Plugins\\TagManager\\Template\\Tag\\BaseTag' => 
    array (
    ),
    'PluginSitesManagerTemplate/TagPiwik\\Plugins\\TagManager\\Template\\Tag\\BaseTag' => 
    array (
    ),
    'PluginInstallationTemplate/TagPiwik\\Plugins\\TagManager\\Template\\Tag\\BaseTag' => 
    array (
    ),
    'PluginCoreUpdaterTemplate/TagPiwik\\Plugins\\TagManager\\Template\\Tag\\BaseTag' => 
    array (
    ),
    'PluginCoreConsoleTemplate/TagPiwik\\Plugins\\TagManager\\Template\\Tag\\BaseTag' => 
    array (
    ),
    'PluginScheduledReportsTemplate/TagPiwik\\Plugins\\TagManager\\Template\\Tag\\BaseTag' => 
    array (
    ),
    'PluginUserCountryMapTemplate/TagPiwik\\Plugins\\TagManager\\Template\\Tag\\BaseTag' => 
    array (
    ),
    'PluginLiveTemplate/TagPiwik\\Plugins\\TagManager\\Template\\Tag\\BaseTag' => 
    array (
    ),
    'PluginCustomVariablesTemplate/TagPiwik\\Plugins\\TagManager\\Template\\Tag\\BaseTag' => 
    array (
    ),
    'PluginPrivacyManagerTemplate/TagPiwik\\Plugins\\TagManager\\Template\\Tag\\BaseTag' => 
    array (
    ),
    'PluginImageGraphTemplate/TagPiwik\\Plugins\\TagManager\\Template\\Tag\\BaseTag' => 
    array (
    ),
    'PluginAnnotationsTemplate/TagPiwik\\Plugins\\TagManager\\Template\\Tag\\BaseTag' => 
    array (
    ),
    'PluginMobileMessagingTemplate/TagPiwik\\Plugins\\TagManager\\Template\\Tag\\BaseTag' => 
    array (
    ),
    'PluginOverlayTemplate/TagPiwik\\Plugins\\TagManager\\Template\\Tag\\BaseTag' => 
    array (
    ),
    'PluginSegmentEditorTemplate/TagPiwik\\Plugins\\TagManager\\Template\\Tag\\BaseTag' => 
    array (
    ),
    'PluginInsightsTemplate/TagPiwik\\Plugins\\TagManager\\Template\\Tag\\BaseTag' => 
    array (
    ),
    'PluginMorpheusTemplate/TagPiwik\\Plugins\\TagManager\\Template\\Tag\\BaseTag' => 
    array (
    ),
    'PluginContentsTemplate/TagPiwik\\Plugins\\TagManager\\Template\\Tag\\BaseTag' => 
    array (
    ),
    'PluginBulkTrackingTemplate/TagPiwik\\Plugins\\TagManager\\Template\\Tag\\BaseTag' => 
    array (
    ),
    'PluginResolutionTemplate/TagPiwik\\Plugins\\TagManager\\Template\\Tag\\BaseTag' => 
    array (
    ),
    'PluginDevicePluginsTemplate/TagPiwik\\Plugins\\TagManager\\Template\\Tag\\BaseTag' => 
    array (
    ),
    'PluginHeartbeatTemplate/TagPiwik\\Plugins\\TagManager\\Template\\Tag\\BaseTag' => 
    array (
    ),
    'PluginIntlTemplate/TagPiwik\\Plugins\\TagManager\\Template\\Tag\\BaseTag' => 
    array (
    ),
    'PluginMarketplaceTemplate/TagPiwik\\Plugins\\TagManager\\Template\\Tag\\BaseTag' => 
    array (
    ),
    'PluginProfessionalServicesTemplate/TagPiwik\\Plugins\\TagManager\\Template\\Tag\\BaseTag' => 
    array (
    ),
    'PluginUserIdTemplate/TagPiwik\\Plugins\\TagManager\\Template\\Tag\\BaseTag' => 
    array (
    ),
    'PluginCustomPiwikJsTemplate/TagPiwik\\Plugins\\TagManager\\Template\\Tag\\BaseTag' => 
    array (
    ),
    'PluginTourTemplate/TagPiwik\\Plugins\\TagManager\\Template\\Tag\\BaseTag' => 
    array (
    ),
    'PluginTagManagerTemplate/TagPiwik\\Plugins\\TagManager\\Template\\Tag\\BaseTag' => 
    array (
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Tag/AddThisTag.php' => 'Piwik\\Plugins\\TagManager\\Template\\Tag\\AddThisTag',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Tag/BingUETTag.php' => 'Piwik\\Plugins\\TagManager\\Template\\Tag\\BingUETTag',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Tag/BugsnagTag.php' => 'Piwik\\Plugins\\TagManager\\Template\\Tag\\BugsnagTag',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Tag/CustomHtmlTag.php' => 'Piwik\\Plugins\\TagManager\\Template\\Tag\\CustomHtmlTag',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Tag/CustomImageTag.php' => 'Piwik\\Plugins\\TagManager\\Template\\Tag\\CustomImageTag',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Tag/DriftTag.php' => 'Piwik\\Plugins\\TagManager\\Template\\Tag\\DriftTag',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Tag/EmarsysTag.php' => 'Piwik\\Plugins\\TagManager\\Template\\Tag\\EmarsysTag',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Tag/EtrackerTag.php' => 'Piwik\\Plugins\\TagManager\\Template\\Tag\\EtrackerTag',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Tag/FacebookPixelTag.php' => 'Piwik\\Plugins\\TagManager\\Template\\Tag\\FacebookPixelTag',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Tag/GoogleAnalyticsUniversalTag.php' => 'Piwik\\Plugins\\TagManager\\Template\\Tag\\GoogleAnalyticsUniversalTag',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Tag/HoneybadgerTag.php' => 'Piwik\\Plugins\\TagManager\\Template\\Tag\\HoneybadgerTag',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Tag/LinkedinInsightTag.php' => 'Piwik\\Plugins\\TagManager\\Template\\Tag\\LinkedinInsightTag',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Tag/LivezillaDynamicTag.php' => 'Piwik\\Plugins\\TagManager\\Template\\Tag\\LivezillaDynamicTag',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Tag/MatomoTag.php' => 'Piwik\\Plugins\\TagManager\\Template\\Tag\\MatomoTag',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Tag/PingdomRUMTag.php' => 'Piwik\\Plugins\\TagManager\\Template\\Tag\\PingdomRUMTag',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Tag/RaygunTag.php' => 'Piwik\\Plugins\\TagManager\\Template\\Tag\\RaygunTag',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Tag/SentryRavenTag.php' => 'Piwik\\Plugins\\TagManager\\Template\\Tag\\SentryRavenTag',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Tag/ShareaholicTag.php' => 'Piwik\\Plugins\\TagManager\\Template\\Tag\\ShareaholicTag',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Tag/TawkToTag.php' => 'Piwik\\Plugins\\TagManager\\Template\\Tag\\TawkToTag',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Tag/ThemeColorTag.php' => 'Piwik\\Plugins\\TagManager\\Template\\Tag\\ThemeColorTag',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Tag/VisualWebsiteOptimizerTag.php' => 'Piwik\\Plugins\\TagManager\\Template\\Tag\\VisualWebsiteOptimizerTag',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Tag/ZendeskChatTag.php' => 'Piwik\\Plugins\\TagManager\\Template\\Tag\\ZendeskChatTag',
    ),
    'PluginCorePluginsAdminTemplate/VariablePiwik\\Plugins\\TagManager\\Template\\Variable\\BaseVariable' => 
    array (
    ),
    'PluginCoreAdminHomeTemplate/VariablePiwik\\Plugins\\TagManager\\Template\\Variable\\BaseVariable' => 
    array (
    ),
    'PluginCoreHomeTemplate/VariablePiwik\\Plugins\\TagManager\\Template\\Variable\\BaseVariable' => 
    array (
    ),
    'PluginWebsiteMeasurableTemplate/VariablePiwik\\Plugins\\TagManager\\Template\\Variable\\BaseVariable' => 
    array (
    ),
    'PluginIntranetMeasurableTemplate/VariablePiwik\\Plugins\\TagManager\\Template\\Variable\\BaseVariable' => 
    array (
    ),
    'PluginDiagnosticsTemplate/VariablePiwik\\Plugins\\TagManager\\Template\\Variable\\BaseVariable' => 
    array (
    ),
    'PluginCoreVisualizationsTemplate/VariablePiwik\\Plugins\\TagManager\\Template\\Variable\\BaseVariable' => 
    array (
    ),
    'PluginProxyTemplate/VariablePiwik\\Plugins\\TagManager\\Template\\Variable\\BaseVariable' => 
    array (
    ),
    'PluginAPITemplate/VariablePiwik\\Plugins\\TagManager\\Template\\Variable\\BaseVariable' => 
    array (
    ),
    'PluginExamplePluginTemplate/VariablePiwik\\Plugins\\TagManager\\Template\\Variable\\BaseVariable' => 
    array (
    ),
    'PluginWidgetizeTemplate/VariablePiwik\\Plugins\\TagManager\\Template\\Variable\\BaseVariable' => 
    array (
    ),
    'PluginTransitionsTemplate/VariablePiwik\\Plugins\\TagManager\\Template\\Variable\\BaseVariable' => 
    array (
    ),
    'PluginLanguagesManagerTemplate/VariablePiwik\\Plugins\\TagManager\\Template\\Variable\\BaseVariable' => 
    array (
    ),
    'PluginActionsTemplate/VariablePiwik\\Plugins\\TagManager\\Template\\Variable\\BaseVariable' => 
    array (
    ),
    'PluginDashboardTemplate/VariablePiwik\\Plugins\\TagManager\\Template\\Variable\\BaseVariable' => 
    array (
    ),
    'PluginMultiSitesTemplate/VariablePiwik\\Plugins\\TagManager\\Template\\Variable\\BaseVariable' => 
    array (
    ),
    'PluginReferrersTemplate/VariablePiwik\\Plugins\\TagManager\\Template\\Variable\\BaseVariable' => 
    array (
    ),
    'PluginUserLanguageTemplate/VariablePiwik\\Plugins\\TagManager\\Template\\Variable\\BaseVariable' => 
    array (
    ),
    'PluginDevicesDetectionTemplate/VariablePiwik\\Plugins\\TagManager\\Template\\Variable\\BaseVariable' => 
    array (
    ),
    'PluginGoalsTemplate/VariablePiwik\\Plugins\\TagManager\\Template\\Variable\\BaseVariable' => 
    array (
    ),
    'PluginEcommerceTemplate/VariablePiwik\\Plugins\\TagManager\\Template\\Variable\\BaseVariable' => 
    array (
    ),
    'PluginSEOTemplate/VariablePiwik\\Plugins\\TagManager\\Template\\Variable\\BaseVariable' => 
    array (
    ),
    'PluginEventsTemplate/VariablePiwik\\Plugins\\TagManager\\Template\\Variable\\BaseVariable' => 
    array (
    ),
    'PluginUserCountryTemplate/VariablePiwik\\Plugins\\TagManager\\Template\\Variable\\BaseVariable' => 
    array (
    ),
    'PluginGeoIp2Template/VariablePiwik\\Plugins\\TagManager\\Template\\Variable\\BaseVariable' => 
    array (
    ),
    'PluginVisitsSummaryTemplate/VariablePiwik\\Plugins\\TagManager\\Template\\Variable\\BaseVariable' => 
    array (
    ),
    'PluginVisitFrequencyTemplate/VariablePiwik\\Plugins\\TagManager\\Template\\Variable\\BaseVariable' => 
    array (
    ),
    'PluginVisitTimeTemplate/VariablePiwik\\Plugins\\TagManager\\Template\\Variable\\BaseVariable' => 
    array (
    ),
    'PluginVisitorInterestTemplate/VariablePiwik\\Plugins\\TagManager\\Template\\Variable\\BaseVariable' => 
    array (
    ),
    'PluginExampleAPITemplate/VariablePiwik\\Plugins\\TagManager\\Template\\Variable\\BaseVariable' => 
    array (
    ),
    'PluginRssWidgetTemplate/VariablePiwik\\Plugins\\TagManager\\Template\\Variable\\BaseVariable' => 
    array (
    ),
    'PluginFeedbackTemplate/VariablePiwik\\Plugins\\TagManager\\Template\\Variable\\BaseVariable' => 
    array (
    ),
    'PluginMonologTemplate/VariablePiwik\\Plugins\\TagManager\\Template\\Variable\\BaseVariable' => 
    array (
    ),
    'PluginLoginTemplate/VariablePiwik\\Plugins\\TagManager\\Template\\Variable\\BaseVariable' => 
    array (
    ),
    'PluginTwoFactorAuthTemplate/VariablePiwik\\Plugins\\TagManager\\Template\\Variable\\BaseVariable' => 
    array (
    ),
    'PluginUsersManagerTemplate/VariablePiwik\\Plugins\\TagManager\\Template\\Variable\\BaseVariable' => 
    array (
    ),
    'PluginSitesManagerTemplate/VariablePiwik\\Plugins\\TagManager\\Template\\Variable\\BaseVariable' => 
    array (
    ),
    'PluginInstallationTemplate/VariablePiwik\\Plugins\\TagManager\\Template\\Variable\\BaseVariable' => 
    array (
    ),
    'PluginCoreUpdaterTemplate/VariablePiwik\\Plugins\\TagManager\\Template\\Variable\\BaseVariable' => 
    array (
    ),
    'PluginCoreConsoleTemplate/VariablePiwik\\Plugins\\TagManager\\Template\\Variable\\BaseVariable' => 
    array (
    ),
    'PluginScheduledReportsTemplate/VariablePiwik\\Plugins\\TagManager\\Template\\Variable\\BaseVariable' => 
    array (
    ),
    'PluginUserCountryMapTemplate/VariablePiwik\\Plugins\\TagManager\\Template\\Variable\\BaseVariable' => 
    array (
    ),
    'PluginLiveTemplate/VariablePiwik\\Plugins\\TagManager\\Template\\Variable\\BaseVariable' => 
    array (
    ),
    'PluginCustomVariablesTemplate/VariablePiwik\\Plugins\\TagManager\\Template\\Variable\\BaseVariable' => 
    array (
    ),
    'PluginPrivacyManagerTemplate/VariablePiwik\\Plugins\\TagManager\\Template\\Variable\\BaseVariable' => 
    array (
    ),
    'PluginImageGraphTemplate/VariablePiwik\\Plugins\\TagManager\\Template\\Variable\\BaseVariable' => 
    array (
    ),
    'PluginAnnotationsTemplate/VariablePiwik\\Plugins\\TagManager\\Template\\Variable\\BaseVariable' => 
    array (
    ),
    'PluginMobileMessagingTemplate/VariablePiwik\\Plugins\\TagManager\\Template\\Variable\\BaseVariable' => 
    array (
    ),
    'PluginOverlayTemplate/VariablePiwik\\Plugins\\TagManager\\Template\\Variable\\BaseVariable' => 
    array (
    ),
    'PluginSegmentEditorTemplate/VariablePiwik\\Plugins\\TagManager\\Template\\Variable\\BaseVariable' => 
    array (
    ),
    'PluginInsightsTemplate/VariablePiwik\\Plugins\\TagManager\\Template\\Variable\\BaseVariable' => 
    array (
    ),
    'PluginMorpheusTemplate/VariablePiwik\\Plugins\\TagManager\\Template\\Variable\\BaseVariable' => 
    array (
    ),
    'PluginContentsTemplate/VariablePiwik\\Plugins\\TagManager\\Template\\Variable\\BaseVariable' => 
    array (
    ),
    'PluginBulkTrackingTemplate/VariablePiwik\\Plugins\\TagManager\\Template\\Variable\\BaseVariable' => 
    array (
    ),
    'PluginResolutionTemplate/VariablePiwik\\Plugins\\TagManager\\Template\\Variable\\BaseVariable' => 
    array (
    ),
    'PluginDevicePluginsTemplate/VariablePiwik\\Plugins\\TagManager\\Template\\Variable\\BaseVariable' => 
    array (
    ),
    'PluginHeartbeatTemplate/VariablePiwik\\Plugins\\TagManager\\Template\\Variable\\BaseVariable' => 
    array (
    ),
    'PluginIntlTemplate/VariablePiwik\\Plugins\\TagManager\\Template\\Variable\\BaseVariable' => 
    array (
    ),
    'PluginMarketplaceTemplate/VariablePiwik\\Plugins\\TagManager\\Template\\Variable\\BaseVariable' => 
    array (
    ),
    'PluginProfessionalServicesTemplate/VariablePiwik\\Plugins\\TagManager\\Template\\Variable\\BaseVariable' => 
    array (
    ),
    'PluginUserIdTemplate/VariablePiwik\\Plugins\\TagManager\\Template\\Variable\\BaseVariable' => 
    array (
    ),
    'PluginCustomPiwikJsTemplate/VariablePiwik\\Plugins\\TagManager\\Template\\Variable\\BaseVariable' => 
    array (
    ),
    'PluginTourTemplate/VariablePiwik\\Plugins\\TagManager\\Template\\Variable\\BaseVariable' => 
    array (
    ),
    'PluginTagManagerTemplate/VariablePiwik\\Plugins\\TagManager\\Template\\Variable\\BaseVariable' => 
    array (
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Variable/ConstantVariable.php' => 'Piwik\\Plugins\\TagManager\\Template\\Variable\\ConstantVariable',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Variable/CookieVariable.php' => 'Piwik\\Plugins\\TagManager\\Template\\Variable\\CookieVariable',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Variable/CustomJsFunctionVariable.php' => 'Piwik\\Plugins\\TagManager\\Template\\Variable\\CustomJsFunctionVariable',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Variable/DataLayerVariable.php' => 'Piwik\\Plugins\\TagManager\\Template\\Variable\\DataLayerVariable',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Variable/DomElementVariable.php' => 'Piwik\\Plugins\\TagManager\\Template\\Variable\\DomElementVariable',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Variable/EtrackerConfigurationVariable.php' => 'Piwik\\Plugins\\TagManager\\Template\\Variable\\EtrackerConfigurationVariable',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Variable/JavaScriptVariable.php' => 'Piwik\\Plugins\\TagManager\\Template\\Variable\\JavaScriptVariable',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Variable/MatomoConfigurationVariable.php' => 'Piwik\\Plugins\\TagManager\\Template\\Variable\\MatomoConfigurationVariable',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Variable/MetaContentVariable.php' => 'Piwik\\Plugins\\TagManager\\Template\\Variable\\MetaContentVariable',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Variable/PreConfigured/BrowserLanguageVariable.php' => 'Piwik\\Plugins\\TagManager\\Template\\Variable\\PreConfigured\\BrowserLanguageVariable',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Variable/PreConfigured/ClickClassesVariable.php' => 'Piwik\\Plugins\\TagManager\\Template\\Variable\\PreConfigured\\ClickClassesVariable',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Variable/PreConfigured/ClickDestinationUrlVariable.php' => 'Piwik\\Plugins\\TagManager\\Template\\Variable\\PreConfigured\\ClickDestinationUrlVariable',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Variable/PreConfigured/ClickElement.php' => 'Piwik\\Plugins\\TagManager\\Template\\Variable\\PreConfigured\\ClickElement',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Variable/PreConfigured/ClickIdVariable.php' => 'Piwik\\Plugins\\TagManager\\Template\\Variable\\PreConfigured\\ClickIdVariable',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Variable/PreConfigured/ClickNodeNameVariable.php' => 'Piwik\\Plugins\\TagManager\\Template\\Variable\\PreConfigured\\ClickNodeNameVariable',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Variable/PreConfigured/ClickTextVariable.php' => 'Piwik\\Plugins\\TagManager\\Template\\Variable\\PreConfigured\\ClickTextVariable',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Variable/PreConfigured/ContainerIdVariable.php' => 'Piwik\\Plugins\\TagManager\\Template\\Variable\\PreConfigured\\ContainerIdVariable',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Variable/PreConfigured/ContainerRevisionVariable.php' => 'Piwik\\Plugins\\TagManager\\Template\\Variable\\PreConfigured\\ContainerRevisionVariable',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Variable/PreConfigured/ContainerVersionVariable.php' => 'Piwik\\Plugins\\TagManager\\Template\\Variable\\PreConfigured\\ContainerVersionVariable',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Variable/PreConfigured/DnsLookupTimeVariable.php' => 'Piwik\\Plugins\\TagManager\\Template\\Variable\\PreConfigured\\DnsLookupTimeVariable',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Variable/PreConfigured/EnvironmentVariable.php' => 'Piwik\\Plugins\\TagManager\\Template\\Variable\\PreConfigured\\EnvironmentVariable',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Variable/PreConfigured/ErrorLineVariable.php' => 'Piwik\\Plugins\\TagManager\\Template\\Variable\\PreConfigured\\ErrorLineVariable',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Variable/PreConfigured/ErrorMessageVariable.php' => 'Piwik\\Plugins\\TagManager\\Template\\Variable\\PreConfigured\\ErrorMessageVariable',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Variable/PreConfigured/ErrorUrlVariable.php' => 'Piwik\\Plugins\\TagManager\\Template\\Variable\\PreConfigured\\ErrorUrlVariable',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Variable/PreConfigured/FirstDirectoryVariable.php' => 'Piwik\\Plugins\\TagManager\\Template\\Variable\\PreConfigured\\FirstDirectoryVariable',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Variable/PreConfigured/FormClassesVariable.php' => 'Piwik\\Plugins\\TagManager\\Template\\Variable\\PreConfigured\\FormClassesVariable',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Variable/PreConfigured/FormDestionationVariable.php' => 'Piwik\\Plugins\\TagManager\\Template\\Variable\\PreConfigured\\FormDestionationVariable',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Variable/PreConfigured/FormElement.php' => 'Piwik\\Plugins\\TagManager\\Template\\Variable\\PreConfigured\\FormElement',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Variable/PreConfigured/FormIdVariable.php' => 'Piwik\\Plugins\\TagManager\\Template\\Variable\\PreConfigured\\FormIdVariable',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Variable/PreConfigured/FormNameVariable.php' => 'Piwik\\Plugins\\TagManager\\Template\\Variable\\PreConfigured\\FormNameVariable',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Variable/PreConfigured/HistoryHashNewPathVariable.php' => 'Piwik\\Plugins\\TagManager\\Template\\Variable\\PreConfigured\\HistoryHashNewPathVariable',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Variable/PreConfigured/HistoryHashNewSearchVariable.php' => 'Piwik\\Plugins\\TagManager\\Template\\Variable\\PreConfigured\\HistoryHashNewSearchVariable',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Variable/PreConfigured/HistoryHashNewUrlVariable.php' => 'Piwik\\Plugins\\TagManager\\Template\\Variable\\PreConfigured\\HistoryHashNewUrlVariable',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Variable/PreConfigured/HistoryHashNewVariable.php' => 'Piwik\\Plugins\\TagManager\\Template\\Variable\\PreConfigured\\HistoryHashNewVariable',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Variable/PreConfigured/HistoryHashOldPathVariable.php' => 'Piwik\\Plugins\\TagManager\\Template\\Variable\\PreConfigured\\HistoryHashOldPathVariable',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Variable/PreConfigured/HistoryHashOldSearchVariable.php' => 'Piwik\\Plugins\\TagManager\\Template\\Variable\\PreConfigured\\HistoryHashOldSearchVariable',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Variable/PreConfigured/HistoryHashOldUrlVariable.php' => 'Piwik\\Plugins\\TagManager\\Template\\Variable\\PreConfigured\\HistoryHashOldUrlVariable',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Variable/PreConfigured/HistoryHashOldVariable.php' => 'Piwik\\Plugins\\TagManager\\Template\\Variable\\PreConfigured\\HistoryHashOldVariable',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Variable/PreConfigured/HistorySourceVariable.php' => 'Piwik\\Plugins\\TagManager\\Template\\Variable\\PreConfigured\\HistorySourceVariable',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Variable/PreConfigured/IsoDateVariable.php' => 'Piwik\\Plugins\\TagManager\\Template\\Variable\\PreConfigured\\IsoDateVariable',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Variable/PreConfigured/LocalDateVariable.php' => 'Piwik\\Plugins\\TagManager\\Template\\Variable\\PreConfigured\\LocalDateVariable',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Variable/PreConfigured/LocalHourVariable.php' => 'Piwik\\Plugins\\TagManager\\Template\\Variable\\PreConfigured\\LocalHourVariable',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Variable/PreConfigured/LocalTimeVariable.php' => 'Piwik\\Plugins\\TagManager\\Template\\Variable\\PreConfigured\\LocalTimeVariable',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Variable/PreConfigured/PageHashVariable.php' => 'Piwik\\Plugins\\TagManager\\Template\\Variable\\PreConfigured\\PageHashVariable',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Variable/PreConfigured/PageHostnameVariable.php' => 'Piwik\\Plugins\\TagManager\\Template\\Variable\\PreConfigured\\PageHostnameVariable',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Variable/PreConfigured/PageLoadTimeTotal.php' => 'Piwik\\Plugins\\TagManager\\Template\\Variable\\PreConfigured\\PageLoadTimeTotal',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Variable/PreConfigured/PageOriginVariable.php' => 'Piwik\\Plugins\\TagManager\\Template\\Variable\\PreConfigured\\PageOriginVariable',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Variable/PreConfigured/PagePathVariable.php' => 'Piwik\\Plugins\\TagManager\\Template\\Variable\\PreConfigured\\PagePathVariable',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Variable/PreConfigured/PageRenderTimeVariable.php' => 'Piwik\\Plugins\\TagManager\\Template\\Variable\\PreConfigured\\PageRenderTimeVariable',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Variable/PreConfigured/PageTitleVariable.php' => 'Piwik\\Plugins\\TagManager\\Template\\Variable\\PreConfigured\\PageTitleVariable',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Variable/PreConfigured/PageUrlVariable.php' => 'Piwik\\Plugins\\TagManager\\Template\\Variable\\PreConfigured\\PageUrlVariable',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Variable/PreConfigured/PreviewModeVariable.php' => 'Piwik\\Plugins\\TagManager\\Template\\Variable\\PreConfigured\\PreviewModeVariable',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Variable/PreConfigured/RandomNumberVariable.php' => 'Piwik\\Plugins\\TagManager\\Template\\Variable\\PreConfigured\\RandomNumberVariable',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Variable/PreConfigured/ReferrerVariable.php' => 'Piwik\\Plugins\\TagManager\\Template\\Variable\\PreConfigured\\ReferrerVariable',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Variable/PreConfigured/ScreenHeight.php' => 'Piwik\\Plugins\\TagManager\\Template\\Variable\\PreConfigured\\ScreenHeight',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Variable/PreConfigured/ScreenHeightAvailable.php' => 'Piwik\\Plugins\\TagManager\\Template\\Variable\\PreConfigured\\ScreenHeightAvailable',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Variable/PreConfigured/ScreenWidth.php' => 'Piwik\\Plugins\\TagManager\\Template\\Variable\\PreConfigured\\ScreenWidth',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Variable/PreConfigured/ScreenWidthAvailable.php' => 'Piwik\\Plugins\\TagManager\\Template\\Variable\\PreConfigured\\ScreenWidthAvailable',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Variable/PreConfigured/ScrollHorizontalPercentageVariable.php' => 'Piwik\\Plugins\\TagManager\\Template\\Variable\\PreConfigured\\ScrollHorizontalPercentageVariable',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Variable/PreConfigured/ScrollLeftPixelVariable.php' => 'Piwik\\Plugins\\TagManager\\Template\\Variable\\PreConfigured\\ScrollLeftPixelVariable',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Variable/PreConfigured/ScrollSourceVariable.php' => 'Piwik\\Plugins\\TagManager\\Template\\Variable\\PreConfigured\\ScrollSourceVariable',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Variable/PreConfigured/ScrollTopPixelVariable.php' => 'Piwik\\Plugins\\TagManager\\Template\\Variable\\PreConfigured\\ScrollTopPixelVariable',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Variable/PreConfigured/ScrollVerticalPercentageVariable.php' => 'Piwik\\Plugins\\TagManager\\Template\\Variable\\PreConfigured\\ScrollVerticalPercentageVariable',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Variable/PreConfigured/SeoCanonicalUrlVariable.php' => 'Piwik\\Plugins\\TagManager\\Template\\Variable\\PreConfigured\\SeoCanonicalUrlVariable',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Variable/PreConfigured/SeoNumH1.php' => 'Piwik\\Plugins\\TagManager\\Template\\Variable\\PreConfigured\\SeoNumH1',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Variable/PreConfigured/SeoNumH2.php' => 'Piwik\\Plugins\\TagManager\\Template\\Variable\\PreConfigured\\SeoNumH2',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Variable/PreConfigured/UserAgentVariable.php' => 'Piwik\\Plugins\\TagManager\\Template\\Variable\\PreConfigured\\UserAgentVariable',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Variable/PreConfigured/UtcDateVariable.php' => 'Piwik\\Plugins\\TagManager\\Template\\Variable\\PreConfigured\\UtcDateVariable',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Variable/PreConfigured/VisibleElementClassesVariable.php' => 'Piwik\\Plugins\\TagManager\\Template\\Variable\\PreConfigured\\VisibleElementClassesVariable',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Variable/PreConfigured/VisibleElementIdVariable.php' => 'Piwik\\Plugins\\TagManager\\Template\\Variable\\PreConfigured\\VisibleElementIdVariable',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Variable/PreConfigured/VisibleElementNodeNameVariable.php' => 'Piwik\\Plugins\\TagManager\\Template\\Variable\\PreConfigured\\VisibleElementNodeNameVariable',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Variable/PreConfigured/VisibleElementTextVariable.php' => 'Piwik\\Plugins\\TagManager\\Template\\Variable\\PreConfigured\\VisibleElementTextVariable',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Variable/PreConfigured/VisibleElementUrlVariable.php' => 'Piwik\\Plugins\\TagManager\\Template\\Variable\\PreConfigured\\VisibleElementUrlVariable',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Variable/PreConfigured/WeekdayVariable.php' => 'Piwik\\Plugins\\TagManager\\Template\\Variable\\PreConfigured\\WeekdayVariable',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Variable/ReferrerUrlVariable.php' => 'Piwik\\Plugins\\TagManager\\Template\\Variable\\ReferrerUrlVariable',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Variable/TimeSinceLoadVariable.php' => 'Piwik\\Plugins\\TagManager\\Template\\Variable\\TimeSinceLoadVariable',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Variable/UrlParameterVariable.php' => 'Piwik\\Plugins\\TagManager\\Template\\Variable\\UrlParameterVariable',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Variable/UrlVariable.php' => 'Piwik\\Plugins\\TagManager\\Template\\Variable\\UrlVariable',
    ),
    'PluginCorePluginsAdminTemplate/TriggerPiwik\\Plugins\\TagManager\\Template\\Trigger\\BaseTrigger' => 
    array (
    ),
    'PluginCoreAdminHomeTemplate/TriggerPiwik\\Plugins\\TagManager\\Template\\Trigger\\BaseTrigger' => 
    array (
    ),
    'PluginCoreHomeTemplate/TriggerPiwik\\Plugins\\TagManager\\Template\\Trigger\\BaseTrigger' => 
    array (
    ),
    'PluginWebsiteMeasurableTemplate/TriggerPiwik\\Plugins\\TagManager\\Template\\Trigger\\BaseTrigger' => 
    array (
    ),
    'PluginIntranetMeasurableTemplate/TriggerPiwik\\Plugins\\TagManager\\Template\\Trigger\\BaseTrigger' => 
    array (
    ),
    'PluginDiagnosticsTemplate/TriggerPiwik\\Plugins\\TagManager\\Template\\Trigger\\BaseTrigger' => 
    array (
    ),
    'PluginCoreVisualizationsTemplate/TriggerPiwik\\Plugins\\TagManager\\Template\\Trigger\\BaseTrigger' => 
    array (
    ),
    'PluginProxyTemplate/TriggerPiwik\\Plugins\\TagManager\\Template\\Trigger\\BaseTrigger' => 
    array (
    ),
    'PluginAPITemplate/TriggerPiwik\\Plugins\\TagManager\\Template\\Trigger\\BaseTrigger' => 
    array (
    ),
    'PluginExamplePluginTemplate/TriggerPiwik\\Plugins\\TagManager\\Template\\Trigger\\BaseTrigger' => 
    array (
    ),
    'PluginWidgetizeTemplate/TriggerPiwik\\Plugins\\TagManager\\Template\\Trigger\\BaseTrigger' => 
    array (
    ),
    'PluginTransitionsTemplate/TriggerPiwik\\Plugins\\TagManager\\Template\\Trigger\\BaseTrigger' => 
    array (
    ),
    'PluginLanguagesManagerTemplate/TriggerPiwik\\Plugins\\TagManager\\Template\\Trigger\\BaseTrigger' => 
    array (
    ),
    'PluginActionsTemplate/TriggerPiwik\\Plugins\\TagManager\\Template\\Trigger\\BaseTrigger' => 
    array (
    ),
    'PluginDashboardTemplate/TriggerPiwik\\Plugins\\TagManager\\Template\\Trigger\\BaseTrigger' => 
    array (
    ),
    'PluginMultiSitesTemplate/TriggerPiwik\\Plugins\\TagManager\\Template\\Trigger\\BaseTrigger' => 
    array (
    ),
    'PluginReferrersTemplate/TriggerPiwik\\Plugins\\TagManager\\Template\\Trigger\\BaseTrigger' => 
    array (
    ),
    'PluginUserLanguageTemplate/TriggerPiwik\\Plugins\\TagManager\\Template\\Trigger\\BaseTrigger' => 
    array (
    ),
    'PluginDevicesDetectionTemplate/TriggerPiwik\\Plugins\\TagManager\\Template\\Trigger\\BaseTrigger' => 
    array (
    ),
    'PluginGoalsTemplate/TriggerPiwik\\Plugins\\TagManager\\Template\\Trigger\\BaseTrigger' => 
    array (
    ),
    'PluginEcommerceTemplate/TriggerPiwik\\Plugins\\TagManager\\Template\\Trigger\\BaseTrigger' => 
    array (
    ),
    'PluginSEOTemplate/TriggerPiwik\\Plugins\\TagManager\\Template\\Trigger\\BaseTrigger' => 
    array (
    ),
    'PluginEventsTemplate/TriggerPiwik\\Plugins\\TagManager\\Template\\Trigger\\BaseTrigger' => 
    array (
    ),
    'PluginUserCountryTemplate/TriggerPiwik\\Plugins\\TagManager\\Template\\Trigger\\BaseTrigger' => 
    array (
    ),
    'PluginGeoIp2Template/TriggerPiwik\\Plugins\\TagManager\\Template\\Trigger\\BaseTrigger' => 
    array (
    ),
    'PluginVisitsSummaryTemplate/TriggerPiwik\\Plugins\\TagManager\\Template\\Trigger\\BaseTrigger' => 
    array (
    ),
    'PluginVisitFrequencyTemplate/TriggerPiwik\\Plugins\\TagManager\\Template\\Trigger\\BaseTrigger' => 
    array (
    ),
    'PluginVisitTimeTemplate/TriggerPiwik\\Plugins\\TagManager\\Template\\Trigger\\BaseTrigger' => 
    array (
    ),
    'PluginVisitorInterestTemplate/TriggerPiwik\\Plugins\\TagManager\\Template\\Trigger\\BaseTrigger' => 
    array (
    ),
    'PluginExampleAPITemplate/TriggerPiwik\\Plugins\\TagManager\\Template\\Trigger\\BaseTrigger' => 
    array (
    ),
    'PluginRssWidgetTemplate/TriggerPiwik\\Plugins\\TagManager\\Template\\Trigger\\BaseTrigger' => 
    array (
    ),
    'PluginFeedbackTemplate/TriggerPiwik\\Plugins\\TagManager\\Template\\Trigger\\BaseTrigger' => 
    array (
    ),
    'PluginMonologTemplate/TriggerPiwik\\Plugins\\TagManager\\Template\\Trigger\\BaseTrigger' => 
    array (
    ),
    'PluginLoginTemplate/TriggerPiwik\\Plugins\\TagManager\\Template\\Trigger\\BaseTrigger' => 
    array (
    ),
    'PluginTwoFactorAuthTemplate/TriggerPiwik\\Plugins\\TagManager\\Template\\Trigger\\BaseTrigger' => 
    array (
    ),
    'PluginUsersManagerTemplate/TriggerPiwik\\Plugins\\TagManager\\Template\\Trigger\\BaseTrigger' => 
    array (
    ),
    'PluginSitesManagerTemplate/TriggerPiwik\\Plugins\\TagManager\\Template\\Trigger\\BaseTrigger' => 
    array (
    ),
    'PluginInstallationTemplate/TriggerPiwik\\Plugins\\TagManager\\Template\\Trigger\\BaseTrigger' => 
    array (
    ),
    'PluginCoreUpdaterTemplate/TriggerPiwik\\Plugins\\TagManager\\Template\\Trigger\\BaseTrigger' => 
    array (
    ),
    'PluginCoreConsoleTemplate/TriggerPiwik\\Plugins\\TagManager\\Template\\Trigger\\BaseTrigger' => 
    array (
    ),
    'PluginScheduledReportsTemplate/TriggerPiwik\\Plugins\\TagManager\\Template\\Trigger\\BaseTrigger' => 
    array (
    ),
    'PluginUserCountryMapTemplate/TriggerPiwik\\Plugins\\TagManager\\Template\\Trigger\\BaseTrigger' => 
    array (
    ),
    'PluginLiveTemplate/TriggerPiwik\\Plugins\\TagManager\\Template\\Trigger\\BaseTrigger' => 
    array (
    ),
    'PluginCustomVariablesTemplate/TriggerPiwik\\Plugins\\TagManager\\Template\\Trigger\\BaseTrigger' => 
    array (
    ),
    'PluginPrivacyManagerTemplate/TriggerPiwik\\Plugins\\TagManager\\Template\\Trigger\\BaseTrigger' => 
    array (
    ),
    'PluginImageGraphTemplate/TriggerPiwik\\Plugins\\TagManager\\Template\\Trigger\\BaseTrigger' => 
    array (
    ),
    'PluginAnnotationsTemplate/TriggerPiwik\\Plugins\\TagManager\\Template\\Trigger\\BaseTrigger' => 
    array (
    ),
    'PluginMobileMessagingTemplate/TriggerPiwik\\Plugins\\TagManager\\Template\\Trigger\\BaseTrigger' => 
    array (
    ),
    'PluginOverlayTemplate/TriggerPiwik\\Plugins\\TagManager\\Template\\Trigger\\BaseTrigger' => 
    array (
    ),
    'PluginSegmentEditorTemplate/TriggerPiwik\\Plugins\\TagManager\\Template\\Trigger\\BaseTrigger' => 
    array (
    ),
    'PluginInsightsTemplate/TriggerPiwik\\Plugins\\TagManager\\Template\\Trigger\\BaseTrigger' => 
    array (
    ),
    'PluginMorpheusTemplate/TriggerPiwik\\Plugins\\TagManager\\Template\\Trigger\\BaseTrigger' => 
    array (
    ),
    'PluginContentsTemplate/TriggerPiwik\\Plugins\\TagManager\\Template\\Trigger\\BaseTrigger' => 
    array (
    ),
    'PluginBulkTrackingTemplate/TriggerPiwik\\Plugins\\TagManager\\Template\\Trigger\\BaseTrigger' => 
    array (
    ),
    'PluginResolutionTemplate/TriggerPiwik\\Plugins\\TagManager\\Template\\Trigger\\BaseTrigger' => 
    array (
    ),
    'PluginDevicePluginsTemplate/TriggerPiwik\\Plugins\\TagManager\\Template\\Trigger\\BaseTrigger' => 
    array (
    ),
    'PluginHeartbeatTemplate/TriggerPiwik\\Plugins\\TagManager\\Template\\Trigger\\BaseTrigger' => 
    array (
    ),
    'PluginIntlTemplate/TriggerPiwik\\Plugins\\TagManager\\Template\\Trigger\\BaseTrigger' => 
    array (
    ),
    'PluginMarketplaceTemplate/TriggerPiwik\\Plugins\\TagManager\\Template\\Trigger\\BaseTrigger' => 
    array (
    ),
    'PluginProfessionalServicesTemplate/TriggerPiwik\\Plugins\\TagManager\\Template\\Trigger\\BaseTrigger' => 
    array (
    ),
    'PluginUserIdTemplate/TriggerPiwik\\Plugins\\TagManager\\Template\\Trigger\\BaseTrigger' => 
    array (
    ),
    'PluginCustomPiwikJsTemplate/TriggerPiwik\\Plugins\\TagManager\\Template\\Trigger\\BaseTrigger' => 
    array (
    ),
    'PluginTourTemplate/TriggerPiwik\\Plugins\\TagManager\\Template\\Trigger\\BaseTrigger' => 
    array (
    ),
    'PluginTagManagerTemplate/TriggerPiwik\\Plugins\\TagManager\\Template\\Trigger\\BaseTrigger' => 
    array (
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Trigger/AllDownloadsClickTrigger.php' => 'Piwik\\Plugins\\TagManager\\Template\\Trigger\\AllDownloadsClickTrigger',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Trigger/AllElementsClickTrigger.php' => 'Piwik\\Plugins\\TagManager\\Template\\Trigger\\AllElementsClickTrigger',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Trigger/AllLinksClickTrigger.php' => 'Piwik\\Plugins\\TagManager\\Template\\Trigger\\AllLinksClickTrigger',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Trigger/CustomEventTrigger.php' => 'Piwik\\Plugins\\TagManager\\Template\\Trigger\\CustomEventTrigger',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Trigger/DomReadyTrigger.php' => 'Piwik\\Plugins\\TagManager\\Template\\Trigger\\DomReadyTrigger',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Trigger/ElementVisibilityTrigger.php' => 'Piwik\\Plugins\\TagManager\\Template\\Trigger\\ElementVisibilityTrigger',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Trigger/FormSubmitTrigger.php' => 'Piwik\\Plugins\\TagManager\\Template\\Trigger\\FormSubmitTrigger',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Trigger/FullscreenTrigger.php' => 'Piwik\\Plugins\\TagManager\\Template\\Trigger\\FullscreenTrigger',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Trigger/HistoryChangeTrigger.php' => 'Piwik\\Plugins\\TagManager\\Template\\Trigger\\HistoryChangeTrigger',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Trigger/JavaScriptErrorTrigger.php' => 'Piwik\\Plugins\\TagManager\\Template\\Trigger\\JavaScriptErrorTrigger',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Trigger/PageViewTrigger.php' => 'Piwik\\Plugins\\TagManager\\Template\\Trigger\\PageViewTrigger',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Trigger/ScrollReachTrigger.php' => 'Piwik\\Plugins\\TagManager\\Template\\Trigger\\ScrollReachTrigger',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Trigger/TimerTrigger.php' => 'Piwik\\Plugins\\TagManager\\Template\\Trigger\\TimerTrigger',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Trigger/WindowLeaveTrigger.php' => 'Piwik\\Plugins\\TagManager\\Template\\Trigger\\WindowLeaveTrigger',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Trigger/WindowLoadedTrigger.php' => 'Piwik\\Plugins\\TagManager\\Template\\Trigger\\WindowLoadedTrigger',
      '/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/Template/Trigger/WindowUnloadTrigger.php' => 'Piwik\\Plugins\\TagManager\\Template\\Trigger\\WindowUnloadTrigger',
    ),
    'PluginDBStatsMetadata' => 
    array (
      'description' => 'DBStats_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.11.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginExampleCommandMetadata' => 
    array (
      'description' => 'Matomo Platform showcase: how to create a console command.',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '0.1.0',
      'theme' => false,
      'require' => 
      array (
      ),
      'name' => 'ExampleCommand',
    ),
    'PluginExampleLogTablesMetadata' => 
    array (
      'description' => 'Matomo Platform showcase: how to create custom log tables.',
      'homepage' => '',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'email' => '',
          'homepage' => '',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '0.1.0',
      'theme' => false,
      'require' => 
      array (
        'piwik' => '>=3.0.0-b1,<4.0.0-b1',
      ),
      'name' => 'ExampleLogTables',
      'support' => 
      array (
        'email' => '',
        'issues' => '',
        'forum' => '',
        'irc' => '',
        'wiki' => '',
        'source' => '',
        'docs' => '',
        'rss' => '',
      ),
      'keywords' => 
      array (
      ),
    ),
    'PluginExampleReportMetadata' => 
    array (
      'description' => 'Matomo Platform showcase: how to define and display a data report.',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'email' => '',
          'homepage' => '',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '0.1.0',
      'theme' => false,
      'require' => 
      array (
      ),
      'name' => 'ExampleReport',
    ),
    'PluginExampleSettingsPluginMetadata' => 
    array (
      'description' => 'Matomo Platform showcase: how to define and how to access plugin settings.',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '0.1.0',
      'theme' => false,
      'require' => 
      array (
      ),
      'name' => 'ExampleSettingsPlugin',
    ),
    'PluginExampleThemeMetadata' => 
    array (
      'description' => 'Matomo Platform showcase: example of how to create a simple Theme.',
      'homepage' => '',
      'authors' => 
      array (
        0 => 
        array (
          'name' => '',
          'email' => '',
          'homepage' => '',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '0.1.0',
      'theme' => true,
      'require' => 
      array (
        'piwik' => '>=3.0.0-b1,<4.0.0-b1',
      ),
      'name' => 'ExampleTheme',
      'stylesheet' => 'stylesheets/theme.less',
      'keywords' => 
      array (
      ),
      'support' => 
      array (
        'email' => '',
        'issues' => '',
        'forum' => '',
        'irc' => '',
        'source' => '',
        'docs' => '',
        'wiki' => '',
        'rss' => '',
      ),
    ),
    'PluginExampleTrackerMetadata' => 
    array (
      'description' => 'Matomo Platform showcase: how to track additional custom data creating new database table columns.',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'email' => '',
          'homepage' => '',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '0.1.0',
      'theme' => false,
      'require' => 
      array (
      ),
      'name' => 'ExampleTracker',
    ),
    'PluginExampleUIMetadata' => 
    array (
      'description' => 'Matomo Platform showcase: how to display data tables, graphs, and the UI framework.',
      'homepage' => 'http://piwik.org',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'email' => 'hello@matomo.org',
          'homepage' => 'http://matomo.org',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '1.0.1',
      'theme' => false,
      'require' => 
      array (
        'piwik' => '>=3.3.0',
      ),
      'name' => 'ExampleUI',
      'keywords' => 
      array (
        0 => 'example',
        1 => 'framework',
        2 => 'platform',
        3 => 'ui',
        4 => 'visualization',
      ),
    ),
    'PluginExampleVisualizationMetadata' => 
    array (
      'description' => 'Matomo Platform showcase: how to create a new custom data visualization.',
      'homepage' => 'https://matomo.org',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'The Matomo Team',
          'email' => 'hello@matomo.org',
          'homepage' => 'https://matomo.org',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '0.1.0',
      'theme' => false,
      'require' => 
      array (
      ),
      'name' => 'ExampleVisualization',
      'keywords' => 
      array (
        0 => 'SimpleTable',
      ),
    ),
    'PluginMobileAppMeasurableMetadata' => 
    array (
      'description' => 'Analytics for Mobile: lets you measure and analyze Mobile Apps with an optimized perspective of your mobile data.',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.11.0',
      'theme' => false,
      'require' => 
      array (
      ),
      'name' => 'MobileAppMeasurable',
    ),
    'PluginProviderMetadata' => 
    array (
      'description' => 'Provider_PluginDescription',
      'homepage' => 'https://matomo.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Matomo',
          'homepage' => 'https://matomo.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'version' => '3.11.0',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
  ),
);