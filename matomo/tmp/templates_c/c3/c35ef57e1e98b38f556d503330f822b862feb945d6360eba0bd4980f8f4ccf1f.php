<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @PrivacyManager/usersOptOut.twig */
class __TwigTemplate_9dcc032c62416b758e291a37d563a87bc57f3543540a9d41577b5de934698c32 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("admin.twig", "@PrivacyManager/usersOptOut.twig", 1);
        $this->blocks = [
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "admin.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 3
        ob_start();
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["PrivacyManager_GDPR"]), "html", null, true);
        $context["title"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_content($context, array $blocks = [])
    {
        // line 6
        echo "    <div piwik-content-block content-title=\"Let users opt-out of tracking\">
        <div piwik-opt-out-customizer piwikurl=\"";
        // line 7
        echo \Piwik\piwik_escape_filter($this->env, ($context["piwikUrl"] ?? $this->getContext($context, "piwikUrl")), "html", null, true);
        echo "\" language=\"";
        echo \Piwik\piwik_escape_filter($this->env, ($context["language"] ?? $this->getContext($context, "language")), "html", null, true);
        echo "\">

        </div>
    </div>

    ";
        // line 12
        if (($context["isSuperUser"] ?? $this->getContext($context, "isSuperUser"))) {
            // line 13
            echo "        <div piwik-content-block
             id=\"DNT\"
             content-title=\"";
            // line 15
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["PrivacyManager_DoNotTrack_SupportDNTPreference"]), "html_attr");
            echo "\">
            <p>
                ";
            // line 17
            if (($context["dntSupport"] ?? $this->getContext($context, "dntSupport"))) {
                // line 18
                echo "                    <strong>";
                echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["PrivacyManager_DoNotTrack_Enabled"]), "html", null, true);
                echo "</strong>
                    <br/>
                    ";
                // line 20
                echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["PrivacyManager_DoNotTrack_EnabledMoreInfo"]), "html", null, true);
                echo "
                ";
            } else {
                // line 22
                echo "                    ";
                echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["PrivacyManager_DoNotTrack_Disabled"]), "html", null, true);
                echo " ";
                echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["PrivacyManager_DoNotTrack_DisabledMoreInfo"]), "html", null, true);
                echo "
                ";
            }
            // line 24
            echo "            </p>

            <div piwik-form ng-controller=\"DoNotTrackPreferenceController as doNotTrack\">

                ";
            // line 29
            echo "                <div piwik-field uicontrol=\"radio\" name=\"doNotTrack\"
                     ng-model=\"doNotTrack.enabled\"
                     options=\"";
            // line 31
            echo \Piwik\piwik_escape_filter($this->env, twig_jsonencode_filter(($context["doNotTrackOptions"] ?? $this->getContext($context, "doNotTrackOptions"))), "html", null, true);
            echo "\"
                     value=\"";
            // line 32
            if (($context["dntSupport"] ?? $this->getContext($context, "dntSupport"))) {
                echo "1";
            } else {
                echo "0";
            }
            echo "\"
                     inline-help=\"";
            // line 33
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["PrivacyManager_DoNotTrack_Description"]), "html_attr");
            echo "\">
                </div>

                <div piwik-save-button onconfirm=\"doNotTrack.save()\" saving=\"doNotTrack.isLoading\"></div>

            </div>

        </div>
    ";
        }
    }

    public function getTemplateName()
    {
        return "@PrivacyManager/usersOptOut.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  114 => 33,  106 => 32,  102 => 31,  98 => 29,  92 => 24,  84 => 22,  79 => 20,  73 => 18,  71 => 17,  66 => 15,  62 => 13,  60 => 12,  50 => 7,  47 => 6,  44 => 5,  40 => 1,  36 => 3,  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'admin.twig' %}

{% set title %}{{ 'PrivacyManager_GDPR'|translate }}{% endset %}

{% block content %}
    <div piwik-content-block content-title=\"Let users opt-out of tracking\">
        <div piwik-opt-out-customizer piwikurl=\"{{ piwikUrl }}\" language=\"{{ language }}\">

        </div>
    </div>

    {% if isSuperUser %}
        <div piwik-content-block
             id=\"DNT\"
             content-title=\"{{ 'PrivacyManager_DoNotTrack_SupportDNTPreference'|translate|e('html_attr') }}\">
            <p>
                {% if dntSupport %}
                    <strong>{{ 'PrivacyManager_DoNotTrack_Enabled'|translate }}</strong>
                    <br/>
                    {{ 'PrivacyManager_DoNotTrack_EnabledMoreInfo'|translate }}
                {% else %}
                    {{ 'PrivacyManager_DoNotTrack_Disabled'|translate }} {{ 'PrivacyManager_DoNotTrack_DisabledMoreInfo'|translate }}
                {% endif %}
            </p>

            <div piwik-form ng-controller=\"DoNotTrackPreferenceController as doNotTrack\">

                {# {{ {'module':'PrivacyManager','nonce':nonce,'action':action} | urlRewriteWithParameters }}#DNT #}
                <div piwik-field uicontrol=\"radio\" name=\"doNotTrack\"
                     ng-model=\"doNotTrack.enabled\"
                     options=\"{{ doNotTrackOptions|json_encode }}\"
                     value=\"{% if dntSupport %}1{% else %}0{% endif %}\"
                     inline-help=\"{{ 'PrivacyManager_DoNotTrack_Description'|translate|e('html_attr') }}\">
                </div>

                <div piwik-save-button onconfirm=\"doNotTrack.save()\" saving=\"doNotTrack.isLoading\"></div>

            </div>

        </div>
    {% endif %}
{% endblock %}
", "@PrivacyManager/usersOptOut.twig", "/home/httpd/electroluxodkuchni.pl/matomo/plugins/PrivacyManager/templates/usersOptOut.twig");
    }
}
