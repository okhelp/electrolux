<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @UsersManager/_userInfoChangedEmail.twig */
class __TwigTemplate_706cef259bc816327c571e3e707cf41eec39d7a7ed9f13586ad7a7cc49deeabf extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<p>";
        echo call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_HelloUser", (("<strong>" . ($context["accountName"] ?? $this->getContext($context, "accountName"))) . "</strong>")]);
        echo "</p>

";
        // line 3
        if ((($context["type"] ?? $this->getContext($context, "type")) == "email")) {
            // line 4
            echo "<p>";
            echo call_user_func_array($this->env->getFilter('translate')->getCallable(), ["UsersManager_EmailChangedEmail1", (("<strong>" . ($context["newEmail"] ?? $this->getContext($context, "newEmail"))) . "</strong>")]);
            echo ".</p>

<p>";
            // line 6
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["UsersManager_EmailChangedEmail2", ($context["deviceDescription"] ?? $this->getContext($context, "deviceDescription")), ($context["ipAddress"] ?? $this->getContext($context, "ipAddress"))]), "html", null, true);
            echo " ";
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["UsersManager_IfThisWasYouIgnoreIfNot"]), "html", null, true);
            echo "</p>
";
        } elseif ((        // line 7
($context["type"] ?? $this->getContext($context, "type")) == "password")) {
            // line 8
            echo "<p>";
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["UsersManager_PasswordChangedEmail", ($context["deviceDescription"] ?? $this->getContext($context, "deviceDescription")), ($context["ipAddress"] ?? $this->getContext($context, "ipAddress"))]), "html", null, true);
            echo "</p>

<p>";
            // line 10
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["UsersManager_IfThisWasYouIgnoreIfNot"]), "html", null, true);
            echo "</p>
";
        }
        // line 12
        echo "
<p>";
        // line 13
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_ThankYouForUsingMatomo"]), "html", null, true);
        echo "!
<br/>";
        // line 14
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_TheMatomoTeam"]), "html", null, true);
        echo "</p>";
    }

    public function getTemplateName()
    {
        return "@UsersManager/_userInfoChangedEmail.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  70 => 14,  66 => 13,  63 => 12,  58 => 10,  52 => 8,  50 => 7,  44 => 6,  38 => 4,  36 => 3,  30 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("<p>{{ 'General_HelloUser'|translate('<strong>' ~ accountName ~ '</strong>')|raw }}</p>

{% if type == 'email' %}
<p>{{ 'UsersManager_EmailChangedEmail1'|translate('<strong>' ~ newEmail ~ '</strong>')|raw }}.</p>

<p>{{ 'UsersManager_EmailChangedEmail2'|translate(deviceDescription, ipAddress) }} {{ 'UsersManager_IfThisWasYouIgnoreIfNot'|translate }}</p>
{% elseif type == 'password' %}
<p>{{ 'UsersManager_PasswordChangedEmail'|translate(deviceDescription, ipAddress) }}</p>

<p>{{ 'UsersManager_IfThisWasYouIgnoreIfNot'|translate }}</p>
{% endif %}

<p>{{ 'General_ThankYouForUsingMatomo'|translate }}!
<br/>{{ 'General_TheMatomoTeam'|translate }}</p>", "@UsersManager/_userInfoChangedEmail.twig", "/home/httpd/electroluxodkuchni.pl/matomo/plugins/UsersManager/templates/_userInfoChangedEmail.twig");
    }
}
