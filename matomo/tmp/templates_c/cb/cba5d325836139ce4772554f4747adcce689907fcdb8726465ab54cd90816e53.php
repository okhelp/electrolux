<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @Feedback/index.twig */
class __TwigTemplate_0b5ff743c1dc9506b32b566e44f40c64ea32db2b7b0bafa0f74068a2bd8dcf43 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("dashboard.twig", "@Feedback/index.twig", 1);
        $this->blocks = [
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "dashboard.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 3
        $context["test_piwikUrl"] = "http://demo.matomo.org/";
        // line 4
        ob_start();
        echo \Piwik\piwik_escape_filter($this->env, ((($context["piwikUrl"] ?? $this->getContext($context, "piwikUrl")) == "http://demo.matomo.org/") || (($context["piwikUrl"] ?? $this->getContext($context, "piwikUrl")) == "https://demo.matomo.org/")), "html", null, true);
        $context["isPiwikDemo"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 6
        ob_start();
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_AboutPiwikX", ($context["piwikVersion"] ?? $this->getContext($context, "piwikVersion"))]), "html", null, true);
        $context["headline"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 8
    public function block_content($context, array $blocks = [])
    {
        // line 9
        echo "
    <div id=\"feedback-faq\" class=\"admin\">
        <div piwik-content-block
             content-title=\"";
        // line 12
        echo \Piwik\piwik_escape_filter($this->env, ($context["headline"] ?? $this->getContext($context, "headline")), "html_attr");
        echo "\"
             feature=\"";
        // line 13
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_Help"]), "html_attr");
        echo "\">
            <p>";
        // line 14
        echo call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_MatomoIsACollaborativeProjectYouCanContributeAndDonateNextRelease", "<a target='_blank' rel='noreferrer noopener' href='https://matomo.org'>", "</a>", "<a target='_blank' rel='noreferrer noopener' href='https://matomo.org/get-involved/'>", "</a>", "<a href='#donate'>", "</a>", "<a target='_blank' rel='noreferrer noopener' href='https://matomo.org/team/'>", "</a>"]);
        // line 23
        echo "
            </p>
        </div>

        <div piwik-content-block content-title=\"";
        // line 27
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Feedback_CommunityHelp"]), "html_attr");
        echo "\">
            <p> &bull; ";
        // line 28
        echo call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Feedback_ViewUserGuides", "<a target='_blank' rel='noreferrer noopener' href='https://matomo.org/docs/'>", "</a>"]);
        echo ".</p>
            <p> &bull; ";
        // line 29
        echo call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Feedback_ViewAnswersToFAQ", "<a target='_blank' rel='noreferrer noopener' href='https://matomo.org/faq/'>", "</a>"]);
        echo ".</p>
            <p> &bull; ";
        // line 30
        echo call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Feedback_VisitTheForums", "<a target='_blank' rel='noreferrer noopener' href='https://forum.matomo.org/'>", "</a>"]);
        echo ".</p>
            <p> &bull; ";
        // line 31
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["API_LearnAboutCommonlyUsedTerms2"]), "html", null, true);
        echo " <a href=\"";
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFunction('linkTo')->getCallable(), [["module" => "API", "action" => "glossary"]]), "html", null, true);
        echo "\">(";
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["API_Glossary"]), "html", null, true);
        echo ")</a></p>
        </div>

        <div piwik-content-block content-title=\"";
        // line 34
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Feedback_ProfessionalHelp"]), "html_attr");
        echo "\">
            <p>";
        // line 35
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Feedback_ProfessionalServicesIntro"]), "html", null, true);
        echo "</p>

            <p>";
        // line 37
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Feedback_ProfessionalServicesOfferIntro"]), "html", null, true);
        echo "</p>
            <p> &bull; ";
        // line 38
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Feedback_ProfessionalServicesReviewPiwikSetup"]), "html", null, true);
        echo "</p>
            <p> &bull; ";
        // line 39
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Feedback_ProfessionalServicesOptimizationMaintenance"]), "html", null, true);
        echo "</p>
            <p> &bull; ";
        // line 40
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Feedback_ProfessionalServicesPhoneEmailSupport"]), "html", null, true);
        echo "</p>
            <p> &bull; ";
        // line 41
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Feedback_ProfessionalServicesTraining"]), "html", null, true);
        echo "</p>
            <p> &bull; <a  rel='noreferrer noopener' target='_blank' href='https://matomo.org/recommends/premium-plugins'>";
        // line 42
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Feedback_ProfessionalServicesPremiumFeatures"]), "html", null, true);
        echo "</a></p>
            <p> &bull; ";
        // line 43
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Feedback_ProfessionalServicesCustomDevelopment"]), "html", null, true);
        echo "</p>
            <p> &bull; ";
        // line 44
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Feedback_ProfessionalServicesAnalystConsulting"]), "html", null, true);
        echo "</p>

            <form target=\"_blank\" action=\"https://matomo.org/support/\">
                <input type=\"hidden\" name=\"pk_campaign\" value=\"App_Help\">
                <input type=\"hidden\" name=\"pk_source\" value=\"Piwik_App\">
                <input type=\"hidden\" name=\"pk_medium\" value=\"App_ContactUs_button\">
                <br />
                <input type=\"submit\" class=\"btn\" value=\"";
        // line 51
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Feedback_ContactUs"]), "html", null, true);
        echo "\">
            </form>
        </div>

        <div piwik-content-block content-title=\"Premium products\">
            <p>Grow your business, understand your audience better and increase your sales and conversions with a premium plugin:
            <p> &bull;  <a rel='noreferrer noopener' target='_blank' href='https://matomo.org/recommends/users-flow/'>Users Flow</a>: a visual representation of the most popular paths your users take through your website or app.</p>
            <p> &bull;  <a rel='noreferrer noopener' target='_blank' href='https://matomo.org/recommends/ab-testing-learn-more/'>A/B Testing</a>: compare different versions of your websites or apps and detect the winning variation.</p>
            <p> &bull;  <a rel='noreferrer noopener' target='_blank' href='https://matomo.org/recommends/conversion-funnels'>Funnels</a>: identify and understand where your visitors drop off in your conversion funnels.</p>
            <p> &bull;  <a rel='noreferrer noopener' target='_blank' href='https://matomo.org/recommends/form-analytics/'>Form Analytics</a>: increase conversions and get better leads from your website forms.</p>
            <p> &bull;  <a rel='noreferrer noopener' target='_blank' href='https://matomo.org/recommends/media-analytics-website/'>Video and Audio Analytics</a>: powerful insights into how your audience watches your videos and listens to your audio.</p>
            <p> &bull;  <a rel='noreferrer noopener' target='_blank' href='https://matomo.org/recommends/roll-up-reporting/'>Roll-Up Reporting</a>: aggregate data from multiple websites, apps and shops into a Roll-Up site to gain new insights.</p>
            <p> &bull;  <a rel='noreferrer noopener' target='_blank' href='https://matomo.org/recommends/search-keywords-performance/'>Search Keywords Performance</a>: all keywords searched by your users on Google+Bing+Yahoo into your Matomo reports.</p>
            <p> &bull;  <a rel='noreferrer noopener' target='_blank' href='https://matomo.org/recommends/activity-log/'>Audit log</a>: better security and problem diagnostic with a detailed audit log of Matomo user activities.</p>
            <p> &bull;  <a rel='noreferrer noopener' target='_blank' href='https://matomo.org/recommends/white-label/'>White Label</a>: give your clients access to their analytics reports where all Matomo-branded widgets are removed.</p>
            <p> &bull;  <strong><a rel='noreferrer noopener' target='_blank' href='https://matomo.org/recommends/premium-plugins'>All premium plugins.</a></strong></p>
        </div>

        <div piwik-content-block content-title=\"";
        // line 69
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Feedback_DoYouHaveBugReportOrFeatureRequest"]), "html_attr");
        echo "\">
            <p>";
        // line 70
        echo call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Feedback_HowToCreateTicket", "<a target='_blank' rel='noreferrer noopener' href='https://developer.matomo.org/guides/core-team-workflow#submitting-a-bug-report'>", "</a>", "<a target='_blank' rel='noreferrer noopener' href='https://developer.matomo.org/guides/core-team-workflow#submitting-a-feature-request'>", "</a>", "<a target='_blank' rel='noreferrer noopener' href='https://github.com/matomo-org/matomo/issues'>", "</a>", "<a target='_blank' rel='noreferrer noopener' href='https://github.com/matomo-org/matomo/issues/new'>", "</a>"]);
        // line 79
        echo "</p>
        </div>

        <div piwik-content-block content-title=\"";
        // line 82
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Feedback_ReviewMatomoTitle"]), "html_attr");
        echo "\">
            <div ng-include=\"'plugins/Feedback/angularjs/feedback-popup/review-links.directive.html'\"></div>
        </div>

        <div class=\"footer\">
            <ul class=\"social\">
                <li>
                    <a target=\"_blank\" rel=\"noreferrer noopener\" href=\"https://matomo.org/newsletter/\"><img class=\"icon\" src=\"plugins/Feedback/images/newsletter.svg\"></a>
                    <a target=\"_blank\" rel=\"noreferrer noopener\" href=\"https://matomo.org/newsletter/\">Newsletter</a>
                </li>
                <li>
                    <a target=\"_blank\" rel=\"noreferrer noopener\" href=\"https://www.facebook.com/Matomo.org\"><img class=\"icon\" src=\"plugins/Morpheus/icons/dist/socials/facebook.com.png\"></a>
                    <a target=\"_blank\" rel=\"noreferrer noopener\" href=\"https://www.facebook.com/Matomo.org\">Facebook</a>
                </li>
                <li>
                    <a target=\"_blank\" rel=\"noreferrer noopener\" href=\"https://twitter.com/matomo_org\"><img class=\"icon\" src=\"plugins/Morpheus/icons/dist/socials/twitter.com.png\"></a>
                    <a target=\"_blank\" rel=\"noreferrer noopener\" href=\"https://twitter.com/matomo_org\">Twitter</a>
                </li>
                <li>
                    <a target=\"_blank\" rel=\"noreferrer noopener\" href=\"https://www.linkedin.com/groups/867857/\"><img class=\"icon\" src=\"plugins/Morpheus/icons/dist/socials/linkedin.com.png\"></a>
                    <a target=\"_blank\" rel=\"noreferrer noopener\" href=\"https://www.linkedin.com/groups/867857/\">Linkedin</a>
                </li>
                <li>
                    <a target=\"_blank\" rel=\"noreferrer noopener\" href=\"https://github.com/matomo-org/matomo\"><img class=\"icon\" src=\"plugins/Morpheus/icons/dist/socials/github.com.png\"></a>
                    <a target=\"_blank\" rel=\"noreferrer noopener\" href=\"https://github.com/matomo-org/matomo\">GitHub</a>
                </li>
            </ul>
            <ul class=\"menu\">
                <li><a target=\"_blank\" rel=\"noreferrer noopener\" href=\"https://matomo.org/blog/\">Blog</a></li>
                <li><a target=\"_blank\" rel=\"noreferrer noopener\" href=\"https://matomo.org/get-involved/\">Get involved</a></li>
                <li><a target=\"_blank\" rel=\"noreferrer noopener\" href=\"https://developer.matomo.org\">Developers</a></li>
                <li><a target=\"_blank\" rel=\"noreferrer noopener\" href=\"https://plugins.matomo.org\">Marketplace</a></li>
                <li><a target=\"_blank\" rel=\"noreferrer noopener\" href=\"https://matomo.org/thank-you-all/\">Credits</a></li>
            </ul>
            <p class=\"claim\"><small>";
        // line 116
        echo call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Feedback_PrivacyClaim", "<a target='_blank' rel='noreferrer noopener' href='https://matomo.org/privacy/'>", "</a>"]);
        // line 119
        echo "</small></p>
        </div>
    </div>
";
    }

    public function getTemplateName()
    {
        return "@Feedback/index.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  218 => 119,  216 => 116,  179 => 82,  174 => 79,  172 => 70,  168 => 69,  147 => 51,  137 => 44,  133 => 43,  129 => 42,  125 => 41,  121 => 40,  117 => 39,  113 => 38,  109 => 37,  104 => 35,  100 => 34,  90 => 31,  86 => 30,  82 => 29,  78 => 28,  74 => 27,  68 => 23,  66 => 14,  62 => 13,  58 => 12,  53 => 9,  50 => 8,  46 => 1,  42 => 6,  38 => 4,  36 => 3,  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'dashboard.twig' %}

{% set test_piwikUrl='http://demo.matomo.org/' %}
{% set isPiwikDemo %}{{ piwikUrl == 'http://demo.matomo.org/' or piwikUrl == 'https://demo.matomo.org/'}}{% endset %}

{% set headline %}{{ 'General_AboutPiwikX'|translate(piwikVersion) }}{% endset %}

{% block content %}

    <div id=\"feedback-faq\" class=\"admin\">
        <div piwik-content-block
             content-title=\"{{ headline|e('html_attr') }}\"
             feature=\"{{ 'General_Help'|translate|e('html_attr') }}\">
            <p>{{ 'General_MatomoIsACollaborativeProjectYouCanContributeAndDonateNextRelease'|translate(
                \"<a target='_blank' rel='noreferrer noopener' href='https://matomo.org'>\",
                \"</a>\",
                \"<a target='_blank' rel='noreferrer noopener' href='https://matomo.org/get-involved/'>\",
                \"</a>\",
                \"<a href='#donate'>\",
                \"</a>\",
                \"<a target='_blank' rel='noreferrer noopener' href='https://matomo.org/team/'>\",
                \"</a>\"
                )|raw }}
            </p>
        </div>

        <div piwik-content-block content-title=\"{{ 'Feedback_CommunityHelp'|translate|e('html_attr') }}\">
            <p> &bull; {{ 'Feedback_ViewUserGuides'|translate(\"<a target='_blank' rel='noreferrer noopener' href='https://matomo.org/docs/'>\",\"</a>\")|raw }}.</p>
            <p> &bull; {{ 'Feedback_ViewAnswersToFAQ'|translate(\"<a target='_blank' rel='noreferrer noopener' href='https://matomo.org/faq/'>\",\"</a>\")|raw }}.</p>
            <p> &bull; {{ 'Feedback_VisitTheForums'|translate(\"<a target='_blank' rel='noreferrer noopener' href='https://forum.matomo.org/'>\",\"</a>\")|raw }}.</p>
            <p> &bull; {{ 'API_LearnAboutCommonlyUsedTerms2'|translate }} <a href=\"{{ linkTo({'module':\"API\",'action':\"glossary\"}) }}\">({{ 'API_Glossary'|translate }})</a></p>
        </div>

        <div piwik-content-block content-title=\"{{ 'Feedback_ProfessionalHelp'|translate|e('html_attr') }}\">
            <p>{{ 'Feedback_ProfessionalServicesIntro'|translate }}</p>

            <p>{{ 'Feedback_ProfessionalServicesOfferIntro'|translate }}</p>
            <p> &bull; {{ 'Feedback_ProfessionalServicesReviewPiwikSetup'|translate }}</p>
            <p> &bull; {{ 'Feedback_ProfessionalServicesOptimizationMaintenance'|translate }}</p>
            <p> &bull; {{ 'Feedback_ProfessionalServicesPhoneEmailSupport'|translate }}</p>
            <p> &bull; {{ 'Feedback_ProfessionalServicesTraining'|translate }}</p>
            <p> &bull; <a  rel='noreferrer noopener' target='_blank' href='https://matomo.org/recommends/premium-plugins'>{{ 'Feedback_ProfessionalServicesPremiumFeatures'|translate }}</a></p>
            <p> &bull; {{ 'Feedback_ProfessionalServicesCustomDevelopment'|translate }}</p>
            <p> &bull; {{ 'Feedback_ProfessionalServicesAnalystConsulting'|translate }}</p>

            <form target=\"_blank\" action=\"https://matomo.org/support/\">
                <input type=\"hidden\" name=\"pk_campaign\" value=\"App_Help\">
                <input type=\"hidden\" name=\"pk_source\" value=\"Piwik_App\">
                <input type=\"hidden\" name=\"pk_medium\" value=\"App_ContactUs_button\">
                <br />
                <input type=\"submit\" class=\"btn\" value=\"{{ 'Feedback_ContactUs'|translate }}\">
            </form>
        </div>

        <div piwik-content-block content-title=\"Premium products\">
            <p>Grow your business, understand your audience better and increase your sales and conversions with a premium plugin:
            <p> &bull;  <a rel='noreferrer noopener' target='_blank' href='https://matomo.org/recommends/users-flow/'>Users Flow</a>: a visual representation of the most popular paths your users take through your website or app.</p>
            <p> &bull;  <a rel='noreferrer noopener' target='_blank' href='https://matomo.org/recommends/ab-testing-learn-more/'>A/B Testing</a>: compare different versions of your websites or apps and detect the winning variation.</p>
            <p> &bull;  <a rel='noreferrer noopener' target='_blank' href='https://matomo.org/recommends/conversion-funnels'>Funnels</a>: identify and understand where your visitors drop off in your conversion funnels.</p>
            <p> &bull;  <a rel='noreferrer noopener' target='_blank' href='https://matomo.org/recommends/form-analytics/'>Form Analytics</a>: increase conversions and get better leads from your website forms.</p>
            <p> &bull;  <a rel='noreferrer noopener' target='_blank' href='https://matomo.org/recommends/media-analytics-website/'>Video and Audio Analytics</a>: powerful insights into how your audience watches your videos and listens to your audio.</p>
            <p> &bull;  <a rel='noreferrer noopener' target='_blank' href='https://matomo.org/recommends/roll-up-reporting/'>Roll-Up Reporting</a>: aggregate data from multiple websites, apps and shops into a Roll-Up site to gain new insights.</p>
            <p> &bull;  <a rel='noreferrer noopener' target='_blank' href='https://matomo.org/recommends/search-keywords-performance/'>Search Keywords Performance</a>: all keywords searched by your users on Google+Bing+Yahoo into your Matomo reports.</p>
            <p> &bull;  <a rel='noreferrer noopener' target='_blank' href='https://matomo.org/recommends/activity-log/'>Audit log</a>: better security and problem diagnostic with a detailed audit log of Matomo user activities.</p>
            <p> &bull;  <a rel='noreferrer noopener' target='_blank' href='https://matomo.org/recommends/white-label/'>White Label</a>: give your clients access to their analytics reports where all Matomo-branded widgets are removed.</p>
            <p> &bull;  <strong><a rel='noreferrer noopener' target='_blank' href='https://matomo.org/recommends/premium-plugins'>All premium plugins.</a></strong></p>
        </div>

        <div piwik-content-block content-title=\"{{ 'Feedback_DoYouHaveBugReportOrFeatureRequest'|translate|e('html_attr') }}\">
            <p>{{ 'Feedback_HowToCreateTicket'|translate(
            \"<a target='_blank' rel='noreferrer noopener' href='https://developer.matomo.org/guides/core-team-workflow#submitting-a-bug-report'>\",
            \"</a>\",
            \"<a target='_blank' rel='noreferrer noopener' href='https://developer.matomo.org/guides/core-team-workflow#submitting-a-feature-request'>\",
            \"</a>\",
            \"<a target='_blank' rel='noreferrer noopener' href='https://github.com/matomo-org/matomo/issues'>\",
            \"</a>\",
            \"<a target='_blank' rel='noreferrer noopener' href='https://github.com/matomo-org/matomo/issues/new'>\",
            \"</a>\"
            )|raw }}</p>
        </div>

        <div piwik-content-block content-title=\"{{'Feedback_ReviewMatomoTitle'|translate|e('html_attr') }}\">
            <div ng-include=\"'plugins/Feedback/angularjs/feedback-popup/review-links.directive.html'\"></div>
        </div>

        <div class=\"footer\">
            <ul class=\"social\">
                <li>
                    <a target=\"_blank\" rel=\"noreferrer noopener\" href=\"https://matomo.org/newsletter/\"><img class=\"icon\" src=\"plugins/Feedback/images/newsletter.svg\"></a>
                    <a target=\"_blank\" rel=\"noreferrer noopener\" href=\"https://matomo.org/newsletter/\">Newsletter</a>
                </li>
                <li>
                    <a target=\"_blank\" rel=\"noreferrer noopener\" href=\"https://www.facebook.com/Matomo.org\"><img class=\"icon\" src=\"plugins/Morpheus/icons/dist/socials/facebook.com.png\"></a>
                    <a target=\"_blank\" rel=\"noreferrer noopener\" href=\"https://www.facebook.com/Matomo.org\">Facebook</a>
                </li>
                <li>
                    <a target=\"_blank\" rel=\"noreferrer noopener\" href=\"https://twitter.com/matomo_org\"><img class=\"icon\" src=\"plugins/Morpheus/icons/dist/socials/twitter.com.png\"></a>
                    <a target=\"_blank\" rel=\"noreferrer noopener\" href=\"https://twitter.com/matomo_org\">Twitter</a>
                </li>
                <li>
                    <a target=\"_blank\" rel=\"noreferrer noopener\" href=\"https://www.linkedin.com/groups/867857/\"><img class=\"icon\" src=\"plugins/Morpheus/icons/dist/socials/linkedin.com.png\"></a>
                    <a target=\"_blank\" rel=\"noreferrer noopener\" href=\"https://www.linkedin.com/groups/867857/\">Linkedin</a>
                </li>
                <li>
                    <a target=\"_blank\" rel=\"noreferrer noopener\" href=\"https://github.com/matomo-org/matomo\"><img class=\"icon\" src=\"plugins/Morpheus/icons/dist/socials/github.com.png\"></a>
                    <a target=\"_blank\" rel=\"noreferrer noopener\" href=\"https://github.com/matomo-org/matomo\">GitHub</a>
                </li>
            </ul>
            <ul class=\"menu\">
                <li><a target=\"_blank\" rel=\"noreferrer noopener\" href=\"https://matomo.org/blog/\">Blog</a></li>
                <li><a target=\"_blank\" rel=\"noreferrer noopener\" href=\"https://matomo.org/get-involved/\">Get involved</a></li>
                <li><a target=\"_blank\" rel=\"noreferrer noopener\" href=\"https://developer.matomo.org\">Developers</a></li>
                <li><a target=\"_blank\" rel=\"noreferrer noopener\" href=\"https://plugins.matomo.org\">Marketplace</a></li>
                <li><a target=\"_blank\" rel=\"noreferrer noopener\" href=\"https://matomo.org/thank-you-all/\">Credits</a></li>
            </ul>
            <p class=\"claim\"><small>{{ 'Feedback_PrivacyClaim'|translate(
                    \"<a target='_blank' rel='noreferrer noopener' href='https://matomo.org/privacy/'>\",
                    \"</a>\"
                )|raw}}</small></p>
        </div>
    </div>
{% endblock %}
", "@Feedback/index.twig", "/home/httpd/electroluxodkuchni.pl/matomo/plugins/Feedback/templates/index.twig");
    }
}
