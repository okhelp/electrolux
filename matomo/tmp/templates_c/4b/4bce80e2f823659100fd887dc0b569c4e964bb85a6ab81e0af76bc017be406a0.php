<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @ProfessionalServices/promoExperiments.twig */
class __TwigTemplate_f4b354395a4d26d65c403e7f175821d3e18198546b1685e0fe6398c81a7a15fd extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<p style=\"margin-top:3em\" class=\" alert-info alert\">Did you know?
    With <a target=\"_blank\" rel=\"noreferrer noopener\" href=\"https://matomo.org/recommends/ab-testing-learn-more/\">A/B Testing for Matomo</a> you can immediately increase conversions and sales by creating different versions of a page to see which one grows your business.
</p>
";
    }

    public function getTemplateName()
    {
        return "@ProfessionalServices/promoExperiments.twig";
    }

    public function getDebugInfo()
    {
        return array (  30 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("<p style=\"margin-top:3em\" class=\" alert-info alert\">Did you know?
    With <a target=\"_blank\" rel=\"noreferrer noopener\" href=\"https://matomo.org/recommends/ab-testing-learn-more/\">A/B Testing for Matomo</a> you can immediately increase conversions and sales by creating different versions of a page to see which one grows your business.
</p>
", "@ProfessionalServices/promoExperiments.twig", "/home/httpd/electroluxodkuchni.pl/matomo/plugins/ProfessionalServices/templates/promoExperiments.twig");
    }
}
