<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @ProfessionalServices/promoFunnel.twig */
class __TwigTemplate_c59a684a32d343fd7e58bfc3ca417a634d20ca3e7e181240e85549798efcf2f8 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<p style=\"margin-top:3em;margin-bottom:3em\" class=\"alert-info alert\">Did you know?
    A Funnel defines a series of actions that you expect your visitors to take on their way to converting a goal.
    <br/>With <a target=\"_blank\" rel=\"noreferrer noopener\" href=\"https://matomo.org/recommends/conversion-funnel/\">Funnels for Matomo</a>,
    you can easily determine your funnel and see where your visitors drop off and how to focus efforts to increase your conversions.
</p>
";
    }

    public function getTemplateName()
    {
        return "@ProfessionalServices/promoFunnel.twig";
    }

    public function getDebugInfo()
    {
        return array (  30 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("<p style=\"margin-top:3em;margin-bottom:3em\" class=\"alert-info alert\">Did you know?
    A Funnel defines a series of actions that you expect your visitors to take on their way to converting a goal.
    <br/>With <a target=\"_blank\" rel=\"noreferrer noopener\" href=\"https://matomo.org/recommends/conversion-funnel/\">Funnels for Matomo</a>,
    you can easily determine your funnel and see where your visitors drop off and how to focus efforts to increase your conversions.
</p>
", "@ProfessionalServices/promoFunnel.twig", "/home/httpd/electroluxodkuchni.pl/matomo/plugins/ProfessionalServices/templates/promoFunnel.twig");
    }
}
