<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @TagManager/manageContainers.twig */
class __TwigTemplate_288b78056bce8b6e94e0db01bf5d157d0e4d259aa6202938e427638f2d9b7cce extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@TagManager/tagmanager.twig", "@TagManager/manageContainers.twig", 1);
        $this->blocks = [
            'topcontrols' => [$this, 'block_topcontrols'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "@TagManager/tagmanager.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 3
        $context["title"] = call_user_func_array($this->env->getFilter('translate')->getCallable(), ["TagManager_ManageX", call_user_func_array($this->env->getFilter('translate')->getCallable(), ["TagManager_Containers"])]);
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_topcontrols($context, array $blocks = [])
    {
        // line 6
        echo "
    <div class=\"top_bar_sites_selector piwikTopControl\">
        <div piwik-siteselector show-selected-site=\"true\" class=\"sites_autocomplete\"></div>
    </div>

    <div class=\"piwikTopControl\">
        <div piwik-container-selector ";
        // line 12
        if (((isset($context["container"]) || array_key_exists("container", $context)) &&  !twig_test_empty(($context["container"] ?? $this->getContext($context, "container"))))) {
            echo "container-name=\"";
            echo \Piwik\piwik_escape_filter($this->env, $this->getAttribute(($context["container"] ?? $this->getContext($context, "container")), "name", []), "html", null, true);
            echo "\"";
        }
        echo "></div>
    </div>

";
    }

    // line 17
    public function block_content($context, array $blocks = [])
    {
        // line 18
        echo "    <div piwik-container-manage>
    </div>
";
    }

    public function getTemplateName()
    {
        return "@TagManager/manageContainers.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  69 => 18,  66 => 17,  54 => 12,  46 => 6,  43 => 5,  39 => 1,  37 => 3,  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% extends '@TagManager/tagmanager.twig' %}

{% set title = 'TagManager_ManageX'|translate('TagManager_Containers'|translate) %}

{% block topcontrols %}

    <div class=\"top_bar_sites_selector piwikTopControl\">
        <div piwik-siteselector show-selected-site=\"true\" class=\"sites_autocomplete\"></div>
    </div>

    <div class=\"piwikTopControl\">
        <div piwik-container-selector {% if container is defined and container is not empty %}container-name=\"{{ container.name }}\"{% endif %}></div>
    </div>

{% endblock %}

{% block content %}
    <div piwik-container-manage>
    </div>
{% endblock %}", "@TagManager/manageContainers.twig", "/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/templates/manageContainers.twig");
    }
}
