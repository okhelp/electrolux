<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @MobileMessaging/reportParametersScheduledReports.twig */
class __TwigTemplate_ec94ef37f21cbf72a5ae4bb59e5481c99d0987cfaa828494cb5d871ad707c4aa extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        $context["macro"] = $this->loadTemplate("@MobileMessaging/macros.twig", "@MobileMessaging/reportParametersScheduledReports.twig", 1);
        // line 2
        echo "
<div ng-show=\"manageScheduledReport.report.type == 'mobile'\">
    ";
        // line 4
        echo $context["macro"]->getselectPhoneNumbers(($context["phoneNumbers"] ?? $this->getContext($context, "phoneNumbers")), "manageScheduledReport", "", true);
        echo "
</div>

<script>
\$(function () {
    resetReportParametersFunctions['mobile'] = function (report) {
        report.phoneNumbers = [];
        report.formatmobile = 'sms';
    };

    updateReportParametersFunctions['mobile'] = function (report) {
        if (report.parameters && report.parameters.phoneNumbers) {
            report.phoneNumbers = report.parameters.phoneNumbers;
        }
        report.formatmobile = 'sms';
    };

    getReportParametersFunctions['mobile'] = function (report) {
        var parameters = {};

        // returning [''] when no phone numbers are selected avoids the \"please provide a value for 'parameters'\" error message
        parameters.phoneNumbers = report.phoneNumbers && report.phoneNumbers.length > 0 ? report.phoneNumbers : [''];

        return parameters;
    };
});
</script>";
    }

    public function getTemplateName()
    {
        return "@MobileMessaging/reportParametersScheduledReports.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  36 => 4,  32 => 2,  30 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% import '@MobileMessaging/macros.twig' as macro %}

<div ng-show=\"manageScheduledReport.report.type == 'mobile'\">
    {{ macro.selectPhoneNumbers(phoneNumbers, 'manageScheduledReport', '', true) }}
</div>

<script>
\$(function () {
    resetReportParametersFunctions['mobile'] = function (report) {
        report.phoneNumbers = [];
        report.formatmobile = 'sms';
    };

    updateReportParametersFunctions['mobile'] = function (report) {
        if (report.parameters && report.parameters.phoneNumbers) {
            report.phoneNumbers = report.parameters.phoneNumbers;
        }
        report.formatmobile = 'sms';
    };

    getReportParametersFunctions['mobile'] = function (report) {
        var parameters = {};

        // returning [''] when no phone numbers are selected avoids the \"please provide a value for 'parameters'\" error message
        parameters.phoneNumbers = report.phoneNumbers && report.phoneNumbers.length > 0 ? report.phoneNumbers : [''];

        return parameters;
    };
});
</script>", "@MobileMessaging/reportParametersScheduledReports.twig", "/home/httpd/electroluxodkuchni.pl/matomo/plugins/MobileMessaging/templates/reportParametersScheduledReports.twig");
    }
}
