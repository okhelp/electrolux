<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @ScheduledReports/_addReport.twig */
class __TwigTemplate_8a6ab58efd07d437c7fe7429763aaf621dea0ee92a85dfa02f9bdf3b9e74f68c extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<div piwik-content-block
     content-title=\"";
        // line 2
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["ScheduledReports_CreateAndScheduleReport"]), "html_attr");
        echo "\"
     class=\"entityAddContainer\"
     ng-if=\"manageScheduledReport.showReportForm\">
    <div class='clear'></div>
    <form id='addEditReport' piwik-form ng-submit=\"manageScheduledReport.submitReport()\">

        <div piwik-field uicontrol=\"text\" name=\"website\"
             data-title=\"";
        // line 9
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_Website"]), "html_attr");
        echo "\"
             data-disabled=\"true\"
             value=\"";
        // line 11
        echo call_user_func_array($this->env->getFilter('rawSafeDecoded')->getCallable(), [($context["siteName"] ?? $this->getContext($context, "siteName"))]);
        echo "\">
        </div>

        <div piwik-field uicontrol=\"textarea\" name=\"report_description\"
             data-title=\"";
        // line 15
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_Description"]), "html_attr");
        echo "\"
             ng-model=\"manageScheduledReport.report.description\"
             inline-help=\"";
        // line 17
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["ScheduledReports_DescriptionOnFirstPage"]), "html_attr");
        echo "\">
        </div>

        ";
        // line 20
        if (($context["segmentEditorActivated"] ?? $this->getContext($context, "segmentEditorActivated"))) {
            // line 21
            echo "            <div id=\"reportSegmentInlineHelp\" class=\"inline-help-node\">
                ";
            // line 22
            ob_start();
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["SegmentEditor_DefaultAllVisits"]), "html", null, true);
            $context["SegmentEditor_DefaultAllVisits"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
            // line 23
            echo "                ";
            ob_start();
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["SegmentEditor_AddNewSegment"]), "html", null, true);
            $context["SegmentEditor_AddNewSegment"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
            // line 24
            echo "                ";
            echo call_user_func_array($this->env->getFilter('translate')->getCallable(), ["ScheduledReports_Segment_Help", "<a href=\"./\" rel=\"noreferrer noopener\" target=\"_blank\">", "</a>", ($context["SegmentEditor_DefaultAllVisits"] ?? $this->getContext($context, "SegmentEditor_DefaultAllVisits")), ($context["SegmentEditor_AddNewSegment"] ?? $this->getContext($context, "SegmentEditor_AddNewSegment"))]);
            echo "
            </div>

            <div piwik-field uicontrol=\"select\" name=\"report_segment\"
                 ng-model=\"manageScheduledReport.report.idsegment\"
                 options=\"";
            // line 29
            echo \Piwik\piwik_escape_filter($this->env, twig_jsonencode_filter(($context["savedSegmentsById"] ?? $this->getContext($context, "savedSegmentsById"))), "html", null, true);
            echo "\"
                 data-title=\"";
            // line 30
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["SegmentEditor_ChooseASegment"]), "html_attr");
            echo "\"
                 inline-help=\"#reportSegmentInlineHelp\">
            </div>
        ";
        }
        // line 34
        echo "
        <div id=\"emailScheduleInlineHelp\" class=\"inline-help-node\">
            ";
        // line 36
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["ScheduledReports_WeeklyScheduleHelp"]), "html", null, true);
        echo "
            <br/>
            ";
        // line 38
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["ScheduledReports_MonthlyScheduleHelp"]), "html", null, true);
        echo "
        </div>

        <div piwik-field uicontrol=\"select\" name=\"report_schedule\"
             options=\"";
        // line 42
        echo \Piwik\piwik_escape_filter($this->env, twig_jsonencode_filter(($context["periods"] ?? $this->getContext($context, "periods"))), "html", null, true);
        echo "\"
             ng-model=\"manageScheduledReport.report.period\"
             ng-change=\"manageScheduledReport.report.periodParam = manageScheduledReport.report.period === 'never' ? null : manageScheduledReport.report.period\"
             data-title=\"";
        // line 45
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["ScheduledReports_EmailSchedule"]), "html_attr");
        echo "\"
             inline-help=\"#emailScheduleInlineHelp\">
        </div>

        <div id=\"emailReportPeriodInlineHelp\" class=\"inline-help-node\">
            ";
        // line 50
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["ScheduledReports_ReportPeriodHelp"]), "html", null, true);
        echo "
            <br/><br/>
            ";
        // line 52
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["ScheduledReports_ReportPeriodHelp2"]), "html", null, true);
        echo "
        </div>

        <div piwik-field uicontrol=\"select\" name=\"report_period\"
             options=\"";
        // line 56
        echo \Piwik\piwik_escape_filter($this->env, twig_jsonencode_filter(($context["paramPeriods"] ?? $this->getContext($context, "paramPeriods"))), "html", null, true);
        echo "\"
             ng-model=\"manageScheduledReport.report.periodParam\"
             title=\"";
        // line 58
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["ScheduledReports_ReportPeriod"]), "html_attr");
        echo "\"
             inline-help=\"#emailReportPeriodInlineHelp\"
        >
        </div>

        <div piwik-field uicontrol=\"select\" name=\"report_hour\"
             options=\"manageScheduledReport.reportHours\"
             ng-change=\"manageScheduledReport.updateReportHourUtc()\"
             ng-model=\"manageScheduledReport.report.hour\"
             ";
        // line 67
        if ((($context["timezoneOffset"] ?? $this->getContext($context, "timezoneOffset")) != 0)) {
            echo "inline-help=\"#reportHourHelpText\"";
        }
        // line 68
        echo "             data-title=\"";
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["ScheduledReports_ReportHour", "X"]), "html_attr");
        echo "\">
        </div>

        ";
        // line 71
        if ((($context["timezoneOffset"] ?? $this->getContext($context, "timezoneOffset")) != 0)) {
            // line 72
            echo "            <div id=\"reportHourHelpText\" class=\"inline-help-node\">
                <span ng-bind=\"manageScheduledReport.report.hourUtc\"></span>
            </div>
        ";
        }
        // line 76
        echo "
        <div piwik-field uicontrol=\"select\" name=\"report_type\"
             options=\"";
        // line 78
        echo \Piwik\piwik_escape_filter($this->env, twig_jsonencode_filter(($context["reportTypeOptions"] ?? $this->getContext($context, "reportTypeOptions"))), "html", null, true);
        echo "\"
             ng-model=\"manageScheduledReport.report.type\"
             ng-change=\"manageScheduledReport.changedReportType()\"
             ";
        // line 81
        if ((twig_length_filter($this->env, ($context["reportTypes"] ?? $this->getContext($context, "reportTypes"))) == 1)) {
            echo "disabled=\"true\"";
        }
        // line 82
        echo "             data-title=\"";
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["ScheduledReports_ReportType"]), "html_attr");
        echo "\">
        </div>

        ";
        // line 85
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["reportFormatsByReportTypeOptions"] ?? $this->getContext($context, "reportFormatsByReportTypeOptions")));
        foreach ($context['_seq'] as $context["reportType"] => $context["reportFormats"]) {
            // line 86
            echo "            <div piwik-field uicontrol=\"select\" name=\"report_format\"
                 class=\"";
            // line 87
            echo \Piwik\piwik_escape_filter($this->env, $context["reportType"], "html", null, true);
            echo "\"
                 ng-model=\"manageScheduledReport.report.format";
            // line 88
            echo \Piwik\piwik_escape_filter($this->env, $context["reportType"], "html", null, true);
            echo "\"
                 ng-show=\"manageScheduledReport.report.type == '";
            // line 89
            echo \Piwik\piwik_escape_filter($this->env, $context["reportType"], "html", null, true);
            echo "'\"
                 options=\"";
            // line 90
            echo \Piwik\piwik_escape_filter($this->env, twig_jsonencode_filter($context["reportFormats"]), "html", null, true);
            echo "\"
                 data-title=\"";
            // line 91
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["ScheduledReports_ReportFormat"]), "html_attr");
            echo "\">
            </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['reportType'], $context['reportFormats'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 94
        echo "
        ";
        // line 95
        echo call_user_func_array($this->env->getFunction('postEvent')->getCallable(), ["Template.reportParametersScheduledReports"]);
        echo "

        <div ng-show=\"manageScheduledReport.report.type == 'email' && manageScheduledReport.report.formatemail !== 'csv'\">
            <div piwik-field uicontrol=\"select\" name=\"display_format\" class=\"email\"
                 ng-model=\"manageScheduledReport.report.displayFormat\"
                 options=\"";
        // line 100
        echo \Piwik\piwik_escape_filter($this->env, twig_jsonencode_filter(($context["displayFormats"] ?? $this->getContext($context, "displayFormats"))), "html", null, true);
        echo "\"
                 introduction=\"";
        // line 101
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["ScheduledReports_AggregateReportsFormat"]), "html_attr");
        echo "\">
            </div>

            <div piwik-field uicontrol=\"checkbox\" name=\"report_evolution_graph\"
                 class=\"report_evolution_graph\"
                 ng-model=\"manageScheduledReport.report.evolutionGraph\"
                 ng-show=\"manageScheduledReport.report.displayFormat == '2' || manageScheduledReport.report.displayFormat == '3'\"
                 data-title=\"";
        // line 108
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["ScheduledReports_EvolutionGraph", 5]), "html_attr");
        echo "\">
            </div>

            <div
                class=\"row evolution-graph-period\"
                ng-show=\"manageScheduledReport.report.displayFormat == '1' || manageScheduledReport.report.displayFormat == '2' || manageScheduledReport.report.displayFormat == '3'\"
            >
                <div class=\"col s12\">
                    <input id=\"report_evolution_period_for_each\" name=\"report_evolution_period_for\" type=\"radio\" checked value=\"each\" ng-model=\"manageScheduledReport.report.evolutionPeriodFor\" />
                    <label for=\"report_evolution_period_for_each\" piwik-translate=\"ScheduledReports_EvolutionGraphsShowForEachInPeriod\">
                        <strong>::</strong>::";
        // line 118
        echo "{{ manageScheduledReport.getFrequencyPeriodSingle() }}";
        echo "
                    </label>
                </div>
                <div class=\"col s12\">
                    <input id=\"report_evolution_period_for_prev\" name=\"report_evolution_period_for\" type=\"radio\" value=\"prev\" ng-model=\"manageScheduledReport.report.evolutionPeriodFor\" />
                    <label for=\"report_evolution_period_for_prev\">
                        ";
        // line 124
        echo "{{ 'ScheduledReports_EvolutionGraphsShowForPreviousN'|translate:manageScheduledReport.getFrequencyPeriodPlural() }}";
        echo ":
                        <input type=\"number\" name=\"report_evolution_period_n\" ng-model=\"manageScheduledReport.report.evolutionPeriodN\" />
                    </label>
                </div>
            </div>
        </div>

        <div class=\"row\">
            <h3 class=\"col s12\">";
        // line 132
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["ScheduledReports_ReportsIncluded"]), "html", null, true);
        echo "</h3>
        </div>

        ";
        // line 135
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["reportsByCategoryByReportType"] ?? $this->getContext($context, "reportsByCategoryByReportType")));
        foreach ($context['_seq'] as $context["reportType"] => $context["reportsByCategory"]) {
            // line 136
            echo "            <div name='reportsList' class='row ";
            echo \Piwik\piwik_escape_filter($this->env, $context["reportType"], "html", null, true);
            echo "'
                 ng-show=\"manageScheduledReport.report.type == '";
            // line 137
            echo \Piwik\piwik_escape_filter($this->env, $context["reportType"], "html", null, true);
            echo "'\">

                ";
            // line 139
            if ($this->getAttribute(($context["allowMultipleReportsByReportType"] ?? $this->getContext($context, "allowMultipleReportsByReportType")), $context["reportType"], [], "array")) {
                // line 140
                echo "                    ";
                $context["reportInputType"] = "checkbox";
                // line 141
                echo "                ";
            } else {
                // line 142
                echo "                    ";
                $context["reportInputType"] = "radio";
                // line 143
                echo "                ";
            }
            // line 144
            echo "
                ";
            // line 145
            $context["countCategory"] = 0;
            // line 146
            echo "
                ";
            // line 147
            $context["newColumnAfter"] = (int) floor(((twig_length_filter($this->env, $context["reportsByCategory"]) + 1) / 2));
            // line 148
            echo "
                <div class='col s12 m6'>
                    ";
            // line 150
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($context["reportsByCategory"]);
            foreach ($context['_seq'] as $context["category"] => $context["reports"]) {
                // line 151
                echo "                    ";
                if (((($context["countCategory"] ?? $this->getContext($context, "countCategory")) >= ($context["newColumnAfter"] ?? $this->getContext($context, "newColumnAfter"))) && (($context["newColumnAfter"] ?? $this->getContext($context, "newColumnAfter")) != 0))) {
                    // line 152
                    echo "                    ";
                    $context["newColumnAfter"] = 0;
                    // line 153
                    echo "                </div>
                <div class='col s12 m6'>
                    ";
                }
                // line 156
                echo "                    <h3 class='reportCategory'>";
                echo \Piwik\piwik_escape_filter($this->env, $context["category"], "html", null, true);
                echo "</h3>
                    <ul class='listReports'>
                        ";
                // line 158
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($context["reports"]);
                foreach ($context['_seq'] as $context["_key"] => $context["report"]) {
                    // line 159
                    echo "                            <li>
                                <input type='";
                    // line 160
                    echo \Piwik\piwik_escape_filter($this->env, ($context["reportInputType"] ?? $this->getContext($context, "reportInputType")), "html", null, true);
                    echo "' id=\"";
                    echo \Piwik\piwik_escape_filter($this->env, $context["reportType"], "html", null, true);
                    echo \Piwik\piwik_escape_filter($this->env, $this->getAttribute($context["report"], "uniqueId", []), "html", null, true);
                    echo "\" report-unique-id='";
                    echo \Piwik\piwik_escape_filter($this->env, $this->getAttribute($context["report"], "uniqueId", []), "html", null, true);
                    echo "'
                                       name='";
                    // line 161
                    echo \Piwik\piwik_escape_filter($this->env, $context["reportType"], "html", null, true);
                    echo "Reports'/>
                                <label for=\"";
                    // line 162
                    echo \Piwik\piwik_escape_filter($this->env, $context["reportType"], "html", null, true);
                    echo \Piwik\piwik_escape_filter($this->env, $this->getAttribute($context["report"], "uniqueId", []), "html", null, true);
                    echo "\">
                                    ";
                    // line 163
                    echo call_user_func_array($this->env->getFilter('rawSafeDecoded')->getCallable(), [$this->getAttribute($context["report"], "name", [])]);
                    echo "
                                    ";
                    // line 164
                    if (($this->getAttribute($context["report"], "uniqueId", []) == "MultiSites_getAll")) {
                        // line 165
                        echo "                                        <div class=\"entityInlineHelp\">";
                        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["ScheduledReports_ReportIncludeNWebsites", ($context["countWebsites"] ?? $this->getContext($context, "countWebsites"))]), "html", null, true);
                        // line 166
                        echo "</div>
                                    ";
                    }
                    // line 168
                    echo "                                </label>
                            </li>
                        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['report'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 171
                echo "                        ";
                $context["countCategory"] = (($context["countCategory"] ?? $this->getContext($context, "countCategory")) + 1);
                // line 172
                echo "                    </ul>
                    <br/>
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['category'], $context['reports'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 175
            echo "                </div>
            </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['reportType'], $context['reportsByCategory'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 178
        echo "
        <input type=\"hidden\" id=\"report_idreport\" ng-model=\"manageScheduledReport.editingReportId\">

        <div ng-value=\"manageScheduledReport.saveButtonTitle\"
               onconfirm=\"manageScheduledReport.submitReport()\"
               piwik-save-button></div>

        <div class='entityCancel'>
            ";
        // line 186
        echo call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_OrCancel", "<a class='entityCancelLink' ng-click='manageScheduledReport.showListOfReports()'>", "</a>"]);
        echo "
        </div>

    </form>
</div>
";
    }

    public function getTemplateName()
    {
        return "@ScheduledReports/_addReport.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  433 => 186,  423 => 178,  415 => 175,  407 => 172,  404 => 171,  396 => 168,  392 => 166,  389 => 165,  387 => 164,  383 => 163,  378 => 162,  374 => 161,  365 => 160,  362 => 159,  358 => 158,  352 => 156,  347 => 153,  344 => 152,  341 => 151,  337 => 150,  333 => 148,  331 => 147,  328 => 146,  326 => 145,  323 => 144,  320 => 143,  317 => 142,  314 => 141,  311 => 140,  309 => 139,  304 => 137,  299 => 136,  295 => 135,  289 => 132,  278 => 124,  269 => 118,  256 => 108,  246 => 101,  242 => 100,  234 => 95,  231 => 94,  222 => 91,  218 => 90,  214 => 89,  210 => 88,  206 => 87,  203 => 86,  199 => 85,  192 => 82,  188 => 81,  182 => 78,  178 => 76,  172 => 72,  170 => 71,  163 => 68,  159 => 67,  147 => 58,  142 => 56,  135 => 52,  130 => 50,  122 => 45,  116 => 42,  109 => 38,  104 => 36,  100 => 34,  93 => 30,  89 => 29,  80 => 24,  75 => 23,  71 => 22,  68 => 21,  66 => 20,  60 => 17,  55 => 15,  48 => 11,  43 => 9,  33 => 2,  30 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("<div piwik-content-block
     content-title=\"{{ 'ScheduledReports_CreateAndScheduleReport'|translate|e('html_attr') }}\"
     class=\"entityAddContainer\"
     ng-if=\"manageScheduledReport.showReportForm\">
    <div class='clear'></div>
    <form id='addEditReport' piwik-form ng-submit=\"manageScheduledReport.submitReport()\">

        <div piwik-field uicontrol=\"text\" name=\"website\"
             data-title=\"{{ 'General_Website'|translate|e('html_attr') }}\"
             data-disabled=\"true\"
             value=\"{{ siteName|rawSafeDecoded }}\">
        </div>

        <div piwik-field uicontrol=\"textarea\" name=\"report_description\"
             data-title=\"{{ 'General_Description'|translate|e('html_attr') }}\"
             ng-model=\"manageScheduledReport.report.description\"
             inline-help=\"{{ 'ScheduledReports_DescriptionOnFirstPage'|translate|e('html_attr') }}\">
        </div>

        {% if segmentEditorActivated %}
            <div id=\"reportSegmentInlineHelp\" class=\"inline-help-node\">
                {% set SegmentEditor_DefaultAllVisits %}{{ 'SegmentEditor_DefaultAllVisits'|translate }}{% endset %}
                {% set SegmentEditor_AddNewSegment %}{{ 'SegmentEditor_AddNewSegment'|translate }}{% endset %}
                {{ 'ScheduledReports_Segment_Help'|translate('<a href=\"./\" rel=\"noreferrer noopener\" target=\"_blank\">','</a>',SegmentEditor_DefaultAllVisits,SegmentEditor_AddNewSegment)|raw }}
            </div>

            <div piwik-field uicontrol=\"select\" name=\"report_segment\"
                 ng-model=\"manageScheduledReport.report.idsegment\"
                 options=\"{{ savedSegmentsById|json_encode }}\"
                 data-title=\"{{ 'SegmentEditor_ChooseASegment'|translate|e('html_attr') }}\"
                 inline-help=\"#reportSegmentInlineHelp\">
            </div>
        {% endif %}

        <div id=\"emailScheduleInlineHelp\" class=\"inline-help-node\">
            {{ 'ScheduledReports_WeeklyScheduleHelp'|translate }}
            <br/>
            {{ 'ScheduledReports_MonthlyScheduleHelp'|translate }}
        </div>

        <div piwik-field uicontrol=\"select\" name=\"report_schedule\"
             options=\"{{ periods|json_encode }}\"
             ng-model=\"manageScheduledReport.report.period\"
             ng-change=\"manageScheduledReport.report.periodParam = manageScheduledReport.report.period === 'never' ? null : manageScheduledReport.report.period\"
             data-title=\"{{ 'ScheduledReports_EmailSchedule'|translate|e('html_attr') }}\"
             inline-help=\"#emailScheduleInlineHelp\">
        </div>

        <div id=\"emailReportPeriodInlineHelp\" class=\"inline-help-node\">
            {{ 'ScheduledReports_ReportPeriodHelp'|translate }}
            <br/><br/>
            {{ 'ScheduledReports_ReportPeriodHelp2'|translate }}
        </div>

        <div piwik-field uicontrol=\"select\" name=\"report_period\"
             options=\"{{ paramPeriods|json_encode }}\"
             ng-model=\"manageScheduledReport.report.periodParam\"
             title=\"{{ 'ScheduledReports_ReportPeriod'|translate|e('html_attr') }}\"
             inline-help=\"#emailReportPeriodInlineHelp\"
        >
        </div>

        <div piwik-field uicontrol=\"select\" name=\"report_hour\"
             options=\"manageScheduledReport.reportHours\"
             ng-change=\"manageScheduledReport.updateReportHourUtc()\"
             ng-model=\"manageScheduledReport.report.hour\"
             {% if timezoneOffset != 0 %}inline-help=\"#reportHourHelpText\"{% endif %}
             data-title=\"{{ 'ScheduledReports_ReportHour'|translate('X')|e('html_attr') }}\">
        </div>

        {% if timezoneOffset != 0 %}
            <div id=\"reportHourHelpText\" class=\"inline-help-node\">
                <span ng-bind=\"manageScheduledReport.report.hourUtc\"></span>
            </div>
        {% endif %}

        <div piwik-field uicontrol=\"select\" name=\"report_type\"
             options=\"{{ reportTypeOptions|json_encode }}\"
             ng-model=\"manageScheduledReport.report.type\"
             ng-change=\"manageScheduledReport.changedReportType()\"
             {% if reportTypes|length == 1 %}disabled=\"true\"{% endif %}
             data-title=\"{{ 'ScheduledReports_ReportType'|translate|e('html_attr') }}\">
        </div>

        {% for reportType, reportFormats in reportFormatsByReportTypeOptions %}
            <div piwik-field uicontrol=\"select\" name=\"report_format\"
                 class=\"{{ reportType }}\"
                 ng-model=\"manageScheduledReport.report.format{{ reportType }}\"
                 ng-show=\"manageScheduledReport.report.type == '{{ reportType }}'\"
                 options=\"{{ reportFormats|json_encode }}\"
                 data-title=\"{{ 'ScheduledReports_ReportFormat'|translate|e('html_attr') }}\">
            </div>
        {% endfor %}

        {{ postEvent(\"Template.reportParametersScheduledReports\") }}

        <div ng-show=\"manageScheduledReport.report.type == 'email' && manageScheduledReport.report.formatemail !== 'csv'\">
            <div piwik-field uicontrol=\"select\" name=\"display_format\" class=\"email\"
                 ng-model=\"manageScheduledReport.report.displayFormat\"
                 options=\"{{ displayFormats|json_encode }}\"
                 introduction=\"{{ 'ScheduledReports_AggregateReportsFormat'|translate|e('html_attr') }}\">
            </div>

            <div piwik-field uicontrol=\"checkbox\" name=\"report_evolution_graph\"
                 class=\"report_evolution_graph\"
                 ng-model=\"manageScheduledReport.report.evolutionGraph\"
                 ng-show=\"manageScheduledReport.report.displayFormat == '2' || manageScheduledReport.report.displayFormat == '3'\"
                 data-title=\"{{ 'ScheduledReports_EvolutionGraph'|translate(5)|e('html_attr') }}\">
            </div>

            <div
                class=\"row evolution-graph-period\"
                ng-show=\"manageScheduledReport.report.displayFormat == '1' || manageScheduledReport.report.displayFormat == '2' || manageScheduledReport.report.displayFormat == '3'\"
            >
                <div class=\"col s12\">
                    <input id=\"report_evolution_period_for_each\" name=\"report_evolution_period_for\" type=\"radio\" checked value=\"each\" ng-model=\"manageScheduledReport.report.evolutionPeriodFor\" />
                    <label for=\"report_evolution_period_for_each\" piwik-translate=\"ScheduledReports_EvolutionGraphsShowForEachInPeriod\">
                        <strong>::</strong>::{{ \"{{ manageScheduledReport.getFrequencyPeriodSingle() }}\" }}
                    </label>
                </div>
                <div class=\"col s12\">
                    <input id=\"report_evolution_period_for_prev\" name=\"report_evolution_period_for\" type=\"radio\" value=\"prev\" ng-model=\"manageScheduledReport.report.evolutionPeriodFor\" />
                    <label for=\"report_evolution_period_for_prev\">
                        {{ \"{{ 'ScheduledReports_EvolutionGraphsShowForPreviousN'|translate:manageScheduledReport.getFrequencyPeriodPlural() }}\" }}:
                        <input type=\"number\" name=\"report_evolution_period_n\" ng-model=\"manageScheduledReport.report.evolutionPeriodN\" />
                    </label>
                </div>
            </div>
        </div>

        <div class=\"row\">
            <h3 class=\"col s12\">{{ 'ScheduledReports_ReportsIncluded'|translate }}</h3>
        </div>

        {% for reportType, reportsByCategory in reportsByCategoryByReportType %}
            <div name='reportsList' class='row {{ reportType }}'
                 ng-show=\"manageScheduledReport.report.type == '{{ reportType }}'\">

                {% if allowMultipleReportsByReportType[reportType] %}
                    {% set reportInputType='checkbox' %}
                {% else %}
                    {% set reportInputType='radio' %}
                {% endif %}

                {% set countCategory=0 %}

                {% set newColumnAfter=(reportsByCategory|length + 1)//2 %}

                <div class='col s12 m6'>
                    {% for category, reports in reportsByCategory %}
                    {% if countCategory >= newColumnAfter and newColumnAfter != 0 %}
                    {% set newColumnAfter=0 %}
                </div>
                <div class='col s12 m6'>
                    {% endif %}
                    <h3 class='reportCategory'>{{ category }}</h3>
                    <ul class='listReports'>
                        {% for report in reports %}
                            <li>
                                <input type='{{ reportInputType }}' id=\"{{ reportType }}{{ report.uniqueId }}\" report-unique-id='{{ report.uniqueId }}'
                                       name='{{ reportType }}Reports'/>
                                <label for=\"{{ reportType }}{{ report.uniqueId }}\">
                                    {{ report.name|rawSafeDecoded }}
                                    {% if report.uniqueId=='MultiSites_getAll' %}
                                        <div class=\"entityInlineHelp\">{{ 'ScheduledReports_ReportIncludeNWebsites'|translate(countWebsites)
                                            }}</div>
                                    {% endif %}
                                </label>
                            </li>
                        {% endfor %}
                        {% set countCategory=countCategory+1 %}
                    </ul>
                    <br/>
                    {% endfor %}
                </div>
            </div>
        {% endfor %}

        <input type=\"hidden\" id=\"report_idreport\" ng-model=\"manageScheduledReport.editingReportId\">

        <div ng-value=\"manageScheduledReport.saveButtonTitle\"
               onconfirm=\"manageScheduledReport.submitReport()\"
               piwik-save-button></div>

        <div class='entityCancel'>
            {{ 'General_OrCancel'|translate(\"<a class='entityCancelLink' ng-click='manageScheduledReport.showListOfReports()'>\",\"</a>\")|raw }}
        </div>

    </form>
</div>
", "@ScheduledReports/_addReport.twig", "/home/httpd/electroluxodkuchni.pl/matomo/plugins/ScheduledReports/templates/_addReport.twig");
    }
}
