<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @PrivacyManager/getDatabaseSize.twig */
class __TwigTemplate_e1eb04e3800d5084b37d09c317b6d28ea82ae8a38d6ec5ab3b8907f7f627141d extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<p>";
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["PrivacyManager_CurrentDBSize"]), "html", null, true);
        echo ": ";
        echo \Piwik\piwik_escape_filter($this->env, $this->getAttribute(($context["dbStats"] ?? $this->getContext($context, "dbStats")), "currentSize", []), "html", null, true);
        echo "</p>
";
        // line 2
        if ($this->getAttribute(($context["dbStats"] ?? null), "sizeAfterPurge", [], "any", true, true)) {
            // line 3
            echo "    <p>";
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["PrivacyManager_EstimatedDBSizeAfterPurge"]), "html", null, true);
            echo ": <strong>";
            echo \Piwik\piwik_escape_filter($this->env, $this->getAttribute(($context["dbStats"] ?? $this->getContext($context, "dbStats")), "sizeAfterPurge", []), "html", null, true);
            echo "</strong></p>
";
        }
        // line 5
        if ($this->getAttribute(($context["dbStats"] ?? null), "spaceSaved", [], "any", true, true)) {
            // line 6
            echo "    <p>";
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["PrivacyManager_EstimatedSpaceSaved"]), "html", null, true);
            echo ": ";
            echo \Piwik\piwik_escape_filter($this->env, $this->getAttribute(($context["dbStats"] ?? $this->getContext($context, "dbStats")), "spaceSaved", []), "html", null, true);
            echo "</p>
";
        }
    }

    public function getTemplateName()
    {
        return "@PrivacyManager/getDatabaseSize.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 6,  47 => 5,  39 => 3,  37 => 2,  30 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("<p>{{ 'PrivacyManager_CurrentDBSize'|translate }}: {{ dbStats.currentSize }}</p>
{% if dbStats.sizeAfterPurge is defined %}
    <p>{{ 'PrivacyManager_EstimatedDBSizeAfterPurge'|translate }}: <strong>{{ dbStats.sizeAfterPurge }}</strong></p>
{% endif %}
{% if dbStats.spaceSaved is defined %}
    <p>{{ 'PrivacyManager_EstimatedSpaceSaved'|translate }}: {{ dbStats.spaceSaved }}</p>
{% endif %}
", "@PrivacyManager/getDatabaseSize.twig", "/home/httpd/electroluxodkuchni.pl/matomo/plugins/PrivacyManager/templates/getDatabaseSize.twig");
    }
}
