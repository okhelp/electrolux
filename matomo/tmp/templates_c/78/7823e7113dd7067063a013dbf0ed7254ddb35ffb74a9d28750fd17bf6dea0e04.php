<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @TagManager/gettingStarted.twig */
class __TwigTemplate_f9698840ec26a05517aa6047fd40f2d17c257daf70a2250dc6e6700876f08578 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@TagManager/tagmanager.twig", "@TagManager/gettingStarted.twig", 1);
        $this->blocks = [
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "@TagManager/tagmanager.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 3
        ob_start();
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["TagManager_GettingStarted"]), "html", null, true);
        $context["title"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_content($context, array $blocks = [])
    {
        // line 6
        echo "<div class=\"tagManagerGettingStarted\">
    <h2>";
        // line 7
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["TagManager_GettingStarted"]), "html", null, true);
        echo "</h2>
    <div piwik-content-block content-title=\"What is a Tag Manager?\">
        <p>
            Matomo Tag Manager lets you manage and unify all your tracking and marketing tags.
            Tags are also known as snippets or pixels. Such tags are typically JavaScript code or HTML and let
            you integrate various features into your site in just a few clicks, for example:
        </p>

        <ul>
            <li>Tracking analytics data</li>
            <li>Conversion Tracking</li>
            <li>Newsletter signups</li>
            <li>Exit popups and surveys</li>
            <li>Remarketing</li>
            <li>Social widgets</li>
            <li>Affiliates</li>
            <li>Ads</li>
            <li>and more</li>
        </ul>

        <p>
            <br />
            This can be achieved using these main components:
        </p>
        <ul>
            <li>Tags - A snippet of code (usually JavaScript or HTML) which will be added to your site.</li>
            <li>Triggers - Defines when a tag should be fired.</li>
            <li>Variables - Lets you retrieve data which can be used by tags and triggers.</li>
        </ul>
    </div>
    <div piwik-content-block content-title=\"Why or when do I need a Tag Manager?\">
        <p>
            A Tag Manager makes your life easier when you want to modify any of these snippets on your website
            as you will no longer need a developer to make the needed changes for you. Instead of waiting for
            someone to make these changes and to deploy your website, you can now easily make the needed changes yourself.
            This lets you not only bring changes to the market faster, but also reduces cost.
            <br /><br />
            It also comes in handy if you embed many third-party snippets into your website and want to bring in some order to
            oversee all the snippets that are embedded and have a convenient way to manage them.
            <br /><br />
            The Tag Manager also makes sure all that all snippets are implemented correctly and loaded in the right way
            for faster performance.
        </p>

    </div>

    ";
        // line 53
        if (($context["canEdit"] ?? $this->getContext($context, "canEdit"))) {
            // line 54
            echo "    <div piwik-content-block content-title=\"How do I get started?\">
        <p>If you haven't created a container yet, <a href=\"";
            // line 55
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFunction('linkTo')->getCallable(), [["module" => "TagManager", "action" => "manageContainers"]]), "html", null, true);
            echo "#?idContainer=0\">create a container now</a>. Next you need to copy/paste the code for the container into your website, this is a simple HTML snippet. From this point, this code will
            load all other snippets and you usually won't need to make any changes to your website anymore.
            <br /><br />
            Now you can add one or multiple tags to your container. If you have embedded a tag manually into your site in the past, you should at the same time also remove all previously added snippets from your website as they will be then loaded through the tag manager.
        </p>
    </div>
    ";
        }
        // line 62
        echo "
    <div piwik-content-block content-title=\"What if a tag, trigger, or variable I need is not supported yet?\">
        <p>
            There are custom tags, triggers, and variables available to let you implement pretty much any use case you need.
            <br /><br />
            If you use features regulary which are not available yet, or you have a product you want to integrate into the Tag Manager,
            please check out our
            <a href=\"https://developer.matomo.org/guides/tagmanager/settingup\" target=\"_blank\" rel=\"noreferrer noopener\">developer documentation</a> on how to add your own tags, triggers, and variables. It is really easy.
        </p>

    </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "@TagManager/gettingStarted.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  114 => 62,  104 => 55,  101 => 54,  99 => 53,  50 => 7,  47 => 6,  44 => 5,  40 => 1,  36 => 3,  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% extends '@TagManager/tagmanager.twig' %}

{% set title %}{{ 'TagManager_GettingStarted'|translate }}{% endset %}

{% block content %}
<div class=\"tagManagerGettingStarted\">
    <h2>{{ 'TagManager_GettingStarted'|translate }}</h2>
    <div piwik-content-block content-title=\"What is a Tag Manager?\">
        <p>
            Matomo Tag Manager lets you manage and unify all your tracking and marketing tags.
            Tags are also known as snippets or pixels. Such tags are typically JavaScript code or HTML and let
            you integrate various features into your site in just a few clicks, for example:
        </p>

        <ul>
            <li>Tracking analytics data</li>
            <li>Conversion Tracking</li>
            <li>Newsletter signups</li>
            <li>Exit popups and surveys</li>
            <li>Remarketing</li>
            <li>Social widgets</li>
            <li>Affiliates</li>
            <li>Ads</li>
            <li>and more</li>
        </ul>

        <p>
            <br />
            This can be achieved using these main components:
        </p>
        <ul>
            <li>Tags - A snippet of code (usually JavaScript or HTML) which will be added to your site.</li>
            <li>Triggers - Defines when a tag should be fired.</li>
            <li>Variables - Lets you retrieve data which can be used by tags and triggers.</li>
        </ul>
    </div>
    <div piwik-content-block content-title=\"Why or when do I need a Tag Manager?\">
        <p>
            A Tag Manager makes your life easier when you want to modify any of these snippets on your website
            as you will no longer need a developer to make the needed changes for you. Instead of waiting for
            someone to make these changes and to deploy your website, you can now easily make the needed changes yourself.
            This lets you not only bring changes to the market faster, but also reduces cost.
            <br /><br />
            It also comes in handy if you embed many third-party snippets into your website and want to bring in some order to
            oversee all the snippets that are embedded and have a convenient way to manage them.
            <br /><br />
            The Tag Manager also makes sure all that all snippets are implemented correctly and loaded in the right way
            for faster performance.
        </p>

    </div>

    {% if canEdit %}
    <div piwik-content-block content-title=\"How do I get started?\">
        <p>If you haven't created a container yet, <a href=\"{{ linkTo({module: 'TagManager', 'action': 'manageContainers'}) }}#?idContainer=0\">create a container now</a>. Next you need to copy/paste the code for the container into your website, this is a simple HTML snippet. From this point, this code will
            load all other snippets and you usually won't need to make any changes to your website anymore.
            <br /><br />
            Now you can add one or multiple tags to your container. If you have embedded a tag manually into your site in the past, you should at the same time also remove all previously added snippets from your website as they will be then loaded through the tag manager.
        </p>
    </div>
    {% endif %}

    <div piwik-content-block content-title=\"What if a tag, trigger, or variable I need is not supported yet?\">
        <p>
            There are custom tags, triggers, and variables available to let you implement pretty much any use case you need.
            <br /><br />
            If you use features regulary which are not available yet, or you have a product you want to integrate into the Tag Manager,
            please check out our
            <a href=\"https://developer.matomo.org/guides/tagmanager/settingup\" target=\"_blank\" rel=\"noreferrer noopener\">developer documentation</a> on how to add your own tags, triggers, and variables. It is really easy.
        </p>

    </div>
</div>
{% endblock %}
", "@TagManager/gettingStarted.twig", "/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/templates/gettingStarted.twig");
    }
}
