<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @Transitions/transitions.twig */
class __TwigTemplate_17541ba3dbf1edc4c3ffce0a82fa0c4beeab90451e33e59edf0d5961c108ddc0 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        if ( !($context["isWidget"] ?? $this->getContext($context, "isWidget"))) {
            echo "<div piwik-content-block
                          help-text=\"";
            // line 2
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Transitions_FeatureDescription"]), "html_attr");
            echo "\"
                          help-url=\"https://matomo.org/docs/transitions/\"
                          content-title=\"";
            // line 4
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Transitions_Transitions"]), "html_attr");
            echo "\">";
        }
        // line 5
        echo "
<div id=\"transitions_report\" ";
        // line 6
        if (($context["isWidget"] ?? $this->getContext($context, "isWidget"))) {
            echo "class=\"widgetBody\"";
        }
        echo " ng-controller=\"TransitionSwitcherController as transitionSwitcher\">
    <div class=\"row\"> 
        <div class=\"col s12 m3\">
            <div piwik-field uicontrol=\"select\" name=\"actionType\"
                 ng-model=\"transitionSwitcher.actionType\"
                 ng-change=\"transitionSwitcher.onActionTypeChange(transitionSwitcher.actionType)\"
                 data-title=\"";
        // line 12
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Actions_ActionType"]), "html_attr");
        echo "\"
                 full-width=\"true\"
                 options='transitionSwitcher.actionTypeOptions'>
            </div>
        </div>
        <div class=\"col s12 m9\">
            <div piwik-field uicontrol=\"select\" name=\"actionName\"
                 ng-model=\"transitionSwitcher.actionName\"
                 ng-change=\"transitionSwitcher.onActionNameChange(transitionSwitcher.actionName)\"
                 data-title=\"";
        // line 21
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Transitions_TopX", 100]), "html_attr");
        echo "\"
                 full-width=\"true\"
                 data-disabled=\"!transitionSwitcher.isEnabled\"
                 options='transitionSwitcher.actionNameOptions'>
            </div>
        </div>
    </div>

    <div piwik-activity-indicator loading=\"transitionSwitcher.isLoading\"></div>

    <div class=\"loadingPiwik\" ng-show=\"!transitionSwitcher.isLoading\" style=\"display:none\" id=\"transitions_inline_loading\">
        <img src=\"plugins/Morpheus/images/loading-blue.gif\" alt=\"\"/> <span>";
        // line 32
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_LoadingData"]), "html", null, true);
        echo "</span>
    </div>

    <div class=\"popoverContainer\" ng-show=\"!transitionSwitcher.isLoading && transitionSwitcher.isEnabled\">

    </div>

    <div id=\"Transitions_Error_Container\" ng-show=\"!transitionSwitcher.isLoading\">

    </div>

    <div class=\"alert alert-info\">
        ";
        // line 44
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Transitions_AvailableInOtherReports"]), "html", null, true);
        echo "
        ";
        // line 45
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Actions_PageUrls"]), "html", null, true);
        echo ", ";
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Actions_SubmenuPageTitles"]), "html", null, true);
        echo ",
        ";
        // line 46
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Actions_SubmenuPagesEntry"]), "html", null, true);
        echo ",
        ";
        // line 47
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Actions_SubmenuPagesExit"]), "html", null, true);
        echo ",
        ";
        // line 48
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_Outlinks"]), "html", null, true);
        echo ", ";
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_And"]), "html", null, true);
        echo " ";
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_Downloads"]), "html", null, true);
        echo ".
        ";
        // line 49
        echo call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Transitions_AvailableInOtherReports2", "<span class=\"icon-transition\"></span>"]);
        echo "
    </div>
</div>

";
        // line 53
        if ( !($context["isWidget"] ?? $this->getContext($context, "isWidget"))) {
            echo "</div>";
        }
    }

    public function getTemplateName()
    {
        return "@Transitions/transitions.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  131 => 53,  124 => 49,  116 => 48,  112 => 47,  108 => 46,  102 => 45,  98 => 44,  83 => 32,  69 => 21,  57 => 12,  46 => 6,  43 => 5,  39 => 4,  34 => 2,  30 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% if not isWidget %}<div piwik-content-block
                          help-text=\"{{ 'Transitions_FeatureDescription'|translate|e('html_attr') }}\"
                          help-url=\"https://matomo.org/docs/transitions/\"
                          content-title=\"{{ 'Transitions_Transitions'|translate|e('html_attr') }}\">{% endif %}

<div id=\"transitions_report\" {% if isWidget %}class=\"widgetBody\"{% endif %} ng-controller=\"TransitionSwitcherController as transitionSwitcher\">
    <div class=\"row\"> 
        <div class=\"col s12 m3\">
            <div piwik-field uicontrol=\"select\" name=\"actionType\"
                 ng-model=\"transitionSwitcher.actionType\"
                 ng-change=\"transitionSwitcher.onActionTypeChange(transitionSwitcher.actionType)\"
                 data-title=\"{{ 'Actions_ActionType'|translate|e('html_attr') }}\"
                 full-width=\"true\"
                 options='transitionSwitcher.actionTypeOptions'>
            </div>
        </div>
        <div class=\"col s12 m9\">
            <div piwik-field uicontrol=\"select\" name=\"actionName\"
                 ng-model=\"transitionSwitcher.actionName\"
                 ng-change=\"transitionSwitcher.onActionNameChange(transitionSwitcher.actionName)\"
                 data-title=\"{{ 'Transitions_TopX'|translate(100)|e('html_attr') }}\"
                 full-width=\"true\"
                 data-disabled=\"!transitionSwitcher.isEnabled\"
                 options='transitionSwitcher.actionNameOptions'>
            </div>
        </div>
    </div>

    <div piwik-activity-indicator loading=\"transitionSwitcher.isLoading\"></div>

    <div class=\"loadingPiwik\" ng-show=\"!transitionSwitcher.isLoading\" style=\"display:none\" id=\"transitions_inline_loading\">
        <img src=\"plugins/Morpheus/images/loading-blue.gif\" alt=\"\"/> <span>{{ 'General_LoadingData'|translate }}</span>
    </div>

    <div class=\"popoverContainer\" ng-show=\"!transitionSwitcher.isLoading && transitionSwitcher.isEnabled\">

    </div>

    <div id=\"Transitions_Error_Container\" ng-show=\"!transitionSwitcher.isLoading\">

    </div>

    <div class=\"alert alert-info\">
        {{ 'Transitions_AvailableInOtherReports'|translate }}
        {{ 'Actions_PageUrls'|translate }}, {{ 'Actions_SubmenuPageTitles'|translate }},
        {{ 'Actions_SubmenuPagesEntry'|translate }},
        {{ 'Actions_SubmenuPagesExit'|translate }},
        {{ 'General_Outlinks'|translate }}, {{ 'General_And'|translate }} {{ 'General_Downloads'|translate }}.
        {{ 'Transitions_AvailableInOtherReports2'|translate('<span class=\"icon-transition\"></span>')|raw }}
    </div>
</div>

{% if not isWidget %}</div>{% endif %}", "@Transitions/transitions.twig", "/home/httpd/electroluxodkuchni.pl/matomo/plugins/Transitions/templates/transitions.twig");
    }
}
