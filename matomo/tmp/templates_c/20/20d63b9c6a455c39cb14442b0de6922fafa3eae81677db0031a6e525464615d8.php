<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @Goals/manageGoals.twig */
class __TwigTemplate_f150f5390b83df2f55ebe431710087aaa910f2cc7078cff1c28da566916ef9e0 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("admin.twig", "@Goals/manageGoals.twig", 1);
        $this->blocks = [
            'topcontrols' => [$this, 'block_topcontrols'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "admin.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 3
        ob_start();
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Goals_ManageGoals"]), "html", null, true);
        $context["title"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_topcontrols($context, array $blocks = [])
    {
        // line 6
        echo "    ";
        $this->loadTemplate("@CoreHome/_siteSelectHeader.twig", "@Goals/manageGoals.twig", 6)->display($context);
    }

    // line 9
    public function block_content($context, array $blocks = [])
    {
        // line 10
        echo "
    ";
        // line 11
        $this->loadTemplate("@Goals/_addEditGoal.twig", "@Goals/manageGoals.twig", 11)->display($context);
        // line 12
        echo "
    <style type=\"text/css\">
        .entityAddContainer {
            position:relative;
        }

        .entityAddContainer > .entityCancel:first-child {
            position: absolute;
            right:0;
            bottom:100%;
        }
    </style>
";
    }

    public function getTemplateName()
    {
        return "@Goals/manageGoals.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  61 => 12,  59 => 11,  56 => 10,  53 => 9,  48 => 6,  45 => 5,  41 => 1,  37 => 3,  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'admin.twig' %}

{% set title %}{{ 'Goals_ManageGoals'|translate }}{% endset %}

{% block topcontrols %}
    {% include \"@CoreHome/_siteSelectHeader.twig\" %}
{% endblock %}

{% block content %}

    {% include \"@Goals/_addEditGoal.twig\" %}

    <style type=\"text/css\">
        .entityAddContainer {
            position:relative;
        }

        .entityAddContainer > .entityCancel:first-child {
            position: absolute;
            right:0;
            bottom:100%;
        }
    </style>
{% endblock %}
", "@Goals/manageGoals.twig", "/home/httpd/electroluxodkuchni.pl/matomo/plugins/Goals/templates/manageGoals.twig");
    }
}
