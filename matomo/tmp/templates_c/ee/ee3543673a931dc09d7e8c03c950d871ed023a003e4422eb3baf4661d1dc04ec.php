<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @Goals/addNewGoal.twig */
class __TwigTemplate_f8aa0fc0026cd5882bfd2eace0174a0b0e095b6325ee7c7dba5e95d545da0e91 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        if (($context["userCanEditGoals"] ?? $this->getContext($context, "userCanEditGoals"))) {
            // line 2
            echo "    ";
            ob_start();
            // line 3
            echo "        <p>";
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Goals_NewGoalIntro"]), "html", null, true);
            echo "</p>
        <p>";
            // line 4
            echo call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Goals_LearnMoreAboutGoalTrackingDocumentation", "<a target='_blank' rel='noreferrer noopener' href='https://matomo.org/docs/tracking-goals-web-analytics/'>", "</a>"]);
            echo "</p>
    ";
            $context["addNewGoalIntro"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
            // line 6
            echo "
    ";
            // line 7
            $this->loadTemplate("@Goals/_addEditGoal.twig", "@Goals/addNewGoal.twig", 7)->display($context);
            // line 8
            echo "
    <br/><br/>

";
        } else {
            // line 12
            echo "    <div piwik-content-block content-title=\"";
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Goals_AddNewGoal"]), "html_attr");
            echo "\">
        <p>
            ";
            // line 14
            echo call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Goals_NoGoalsNeedAccess2"]);
            echo "
        </p>
        <p>
            ";
            // line 17
            echo call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Goals_LearnMoreAboutGoalTrackingDocumentation", "<a target='_blank' rel='noreferrer noopener' href='https://matomo.org/docs/tracking-goals-web-analytics/'>", "</a>"]);
            echo "
        </p>
    </div>
    ";
            // line 20
            echo call_user_func_array($this->env->getFunction('postEvent')->getCallable(), ["Template.afterGoalCannotAddNewGoal"]);
            echo "

";
        }
    }

    public function getTemplateName()
    {
        return "@Goals/addNewGoal.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  74 => 20,  68 => 17,  62 => 14,  56 => 12,  50 => 8,  48 => 7,  45 => 6,  40 => 4,  35 => 3,  32 => 2,  30 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% if userCanEditGoals %}
    {% set addNewGoalIntro %}
        <p>{{ 'Goals_NewGoalIntro'|translate }}</p>
        <p>{{ 'Goals_LearnMoreAboutGoalTrackingDocumentation'|translate(\"<a target='_blank' rel='noreferrer noopener' href='https://matomo.org/docs/tracking-goals-web-analytics/'>\",\"</a>\")|raw }}</p>
    {% endset %}

    {% include \"@Goals/_addEditGoal.twig\" %}

    <br/><br/>

{% else %}
    <div piwik-content-block content-title=\"{{ 'Goals_AddNewGoal'|translate|e('html_attr') }}\">
        <p>
            {{ 'Goals_NoGoalsNeedAccess2'|translate|raw }}
        </p>
        <p>
            {{ 'Goals_LearnMoreAboutGoalTrackingDocumentation'|translate(\"<a target='_blank' rel='noreferrer noopener' href='https://matomo.org/docs/tracking-goals-web-analytics/'>\",\"</a>\")|raw }}
        </p>
    </div>
    {{ postEvent(\"Template.afterGoalCannotAddNewGoal\") }}

{% endif %}
", "@Goals/addNewGoal.twig", "/home/httpd/electroluxodkuchni.pl/matomo/plugins/Goals/templates/addNewGoal.twig");
    }
}
