<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @CoreAdminHome/optOut.twig */
class __TwigTemplate_e8120f6ea3612338e4d9a87cce080bd3754f08f568fa0cd0d61cc8bae9b5189f extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<!DOCTYPE html>
<html>
<head>
    <meta charset=\"utf-8\">
    ";
        // line 5
        if (($context["title"] ?? $this->getContext($context, "title"))) {
            // line 6
            echo "        <title>";
            echo \Piwik\piwik_escape_filter($this->env, ($context["title"] ?? $this->getContext($context, "title")), "html", null, true);
            echo "</title>
    ";
        }
        // line 8
        echo "    ";
        if (($context["reloadUrl"] ?? $this->getContext($context, "reloadUrl"))) {
            // line 9
            echo "        <meta http-equiv=\"refresh\" content=\"0; url=";
            echo \Piwik\piwik_escape_filter($this->env, ($context["reloadUrl"] ?? $this->getContext($context, "reloadUrl")), "html", null, true);
            echo "\" />
    ";
        }
        // line 11
        echo "
    ";
        // line 12
        if ((twig_length_filter($this->env, $this->getAttribute(($context["stylesheets"] ?? $this->getContext($context, "stylesheets")), "external", [])) > 0)) {
            // line 13
            echo "        ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["stylesheets"] ?? $this->getContext($context, "stylesheets")), "external", []));
            foreach ($context['_seq'] as $context["_key"] => $context["style"]) {
                // line 14
                echo "            <link href=\"";
                echo $context["style"];
                echo "\" rel=\"stylesheet\" type=\"text/css\">
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['style'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 16
            echo "    ";
        }
        // line 17
        echo "    ";
        if ((twig_length_filter($this->env, $this->getAttribute(($context["stylesheets"] ?? $this->getContext($context, "stylesheets")), "inline", [])) > 0)) {
            // line 18
            echo "        <style>
            ";
            // line 19
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["stylesheets"] ?? $this->getContext($context, "stylesheets")), "inline", []));
            foreach ($context['_seq'] as $context["_key"] => $context["style"]) {
                // line 20
                echo "            ";
                echo $context["style"];
                echo "
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['style'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 22
            echo "        </style>
    ";
        }
        // line 24
        echo "</head>
<body>
";
        // line 26
        if (($context["dntFound"] ?? $this->getContext($context, "dntFound"))) {
            // line 27
            echo "    ";
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CoreAdminHome_OptOutDntFound"]), "html", null, true);
            echo "
";
        } elseif (        // line 28
($context["reloadUrl"] ?? $this->getContext($context, "reloadUrl"))) {
            // line 29
            echo "    ";
        } else {
            // line 31
            echo "    ";
            // line 34
            echo "    ";
            if (($context["showConfirmOnly"] ?? $this->getContext($context, "showConfirmOnly"))) {
                // line 35
                echo "    <p>";
                echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CoreAdminHome_OptingYouOut"]), "html", null, true);
                echo "</p>
    <script>window.close();</script>
    <noscript>
    ";
            }
            // line 39
            echo "
    ";
            // line 40
            if ( !($context["trackVisits"] ?? $this->getContext($context, "trackVisits"))) {
                // line 41
                echo "        <p>
        ";
                // line 42
                echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CoreAdminHome_OptOutComplete"]), "html", null, true);
                echo "

        ";
                // line 44
                echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CoreAdminHome_OptOutCompleteBis"]), "html", null, true);
                echo "
        </p>
    ";
            } else {
                // line 47
                echo "        <p>";
                echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CoreAdminHome_YouMayOptOut2"]), "html", null, true);
                echo " ";
                echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CoreAdminHome_YouMayOptOut3"]), "html", null, true);
                echo "</p>
    ";
            }
            // line 49
            echo "
    ";
            // line 50
            if (($context["showConfirmOnly"] ?? $this->getContext($context, "showConfirmOnly"))) {
                echo "</noscript>";
            }
            // line 51
            echo "
    ";
            // line 52
            if ( !($context["showConfirmOnly"] ?? $this->getContext($context, "showConfirmOnly"))) {
                // line 53
                echo "    <form method=\"post\" action=\"?";
                echo twig_urlencode_filter(($context["queryParameters"] ?? $this->getContext($context, "queryParameters")));
                echo "\" target=\"_blank\">
        <input type=\"hidden\" name=\"nonce\" value=\"";
                // line 54
                echo \Piwik\piwik_escape_filter($this->env, ($context["nonce"] ?? $this->getContext($context, "nonce")), "html", null, true);
                echo "\" />
        <input type=\"hidden\" name=\"fuzz\" value=\"";
                // line 55
                echo \Piwik\piwik_escape_filter($this->env, twig_date_format_filter($this->env, "now"), "html", null, true);
                echo "\" />
        <input onclick=\"submitForm(event, this.form);\" type=\"checkbox\" id=\"trackVisits\" name=\"trackVisits\" ";
                // line 56
                if (($context["trackVisits"] ?? $this->getContext($context, "trackVisits"))) {
                    echo "checked=\"checked\"";
                }
                echo " />
        <label for=\"trackVisits\"><strong>
        ";
                // line 58
                if (($context["trackVisits"] ?? $this->getContext($context, "trackVisits"))) {
                    // line 59
                    echo "            ";
                    echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CoreAdminHome_YouAreNotOptedOut"]), "html", null, true);
                    echo " ";
                    echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CoreAdminHome_UncheckToOptOut"]), "html", null, true);
                    echo "
        ";
                } else {
                    // line 61
                    echo "            ";
                    echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CoreAdminHome_YouAreOptedOut"]), "html", null, true);
                    echo " ";
                    echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CoreAdminHome_CheckToOptIn"]), "html", null, true);
                    echo "
        ";
                }
                // line 63
                echo "        </strong></label>
        <noscript>
            <button type=\"submit\">";
                // line 65
                echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_Save"]), "html", null, true);
                echo "</button>
        </noscript>
    </form>
    ";
            }
        }
        // line 70
        echo "
";
        // line 71
        if ((twig_length_filter($this->env, $this->getAttribute(($context["javascripts"] ?? $this->getContext($context, "javascripts")), "external", [])) > 0)) {
            // line 72
            echo "    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["javascripts"] ?? $this->getContext($context, "javascripts")), "external", []));
            foreach ($context['_seq'] as $context["_key"] => $context["script"]) {
                // line 73
                echo "        <script type=\"text/javascript\" src=\"";
                echo $context["script"];
                echo "\"></script>
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['script'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        }
        // line 76
        if ((twig_length_filter($this->env, $this->getAttribute(($context["javascripts"] ?? $this->getContext($context, "javascripts")), "inline", [])) > 0)) {
            // line 77
            echo "    <script>
        ";
            // line 78
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["javascripts"] ?? $this->getContext($context, "javascripts")), "inline", []));
            foreach ($context['_seq'] as $context["_key"] => $context["script"]) {
                // line 79
                echo "        ";
                echo $context["script"];
                echo "
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['script'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 81
            echo "    </script>
";
        }
        // line 83
        echo "</body>
</html>
";
    }

    public function getTemplateName()
    {
        return "@CoreAdminHome/optOut.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  258 => 83,  254 => 81,  245 => 79,  241 => 78,  238 => 77,  236 => 76,  226 => 73,  221 => 72,  219 => 71,  216 => 70,  208 => 65,  204 => 63,  196 => 61,  188 => 59,  186 => 58,  179 => 56,  175 => 55,  171 => 54,  166 => 53,  164 => 52,  161 => 51,  157 => 50,  154 => 49,  146 => 47,  140 => 44,  135 => 42,  132 => 41,  130 => 40,  127 => 39,  119 => 35,  116 => 34,  114 => 31,  111 => 29,  109 => 28,  104 => 27,  102 => 26,  98 => 24,  94 => 22,  85 => 20,  81 => 19,  78 => 18,  75 => 17,  72 => 16,  63 => 14,  58 => 13,  56 => 12,  53 => 11,  47 => 9,  44 => 8,  38 => 6,  36 => 5,  30 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html>
<html>
<head>
    <meta charset=\"utf-8\">
    {% if title %}
        <title>{{ title }}</title>
    {% endif %}
    {% if reloadUrl %}
        <meta http-equiv=\"refresh\" content=\"0; url={{ reloadUrl }}\" />
    {% endif %}

    {% if stylesheets.external|length > 0 %}
        {% for style in stylesheets.external %}
            <link href=\"{{ style|raw }}\" rel=\"stylesheet\" type=\"text/css\">
        {% endfor %}
    {% endif %}
    {% if stylesheets.inline|length > 0 %}
        <style>
            {% for style in stylesheets.inline %}
            {{ style|raw }}
            {% endfor %}
        </style>
    {% endif %}
</head>
<body>
{% if dntFound %}
    {{ 'CoreAdminHome_OptOutDntFound'|translate }}
{% elseif reloadUrl %}
    {# empty #}
{% else %}
    {# if only showing confirmation (because we're in a new window), we only display the success message if JS is disabled.
     # otherwise we try to close the window immediately.
     #}
    {% if showConfirmOnly %}
    <p>{{ 'CoreAdminHome_OptingYouOut'|translate }}</p>
    <script>window.close();</script>
    <noscript>
    {% endif %}

    {% if not trackVisits %}
        <p>
        {{ 'CoreAdminHome_OptOutComplete'|translate }}

        {{ 'CoreAdminHome_OptOutCompleteBis'|translate }}
        </p>
    {% else %}
        <p>{{ 'CoreAdminHome_YouMayOptOut2'|translate }} {{ 'CoreAdminHome_YouMayOptOut3'|translate }}</p>
    {% endif %}

    {% if showConfirmOnly %}</noscript>{% endif %}

    {% if not showConfirmOnly %}
    <form method=\"post\" action=\"?{{ queryParameters|url_encode|raw }}\" target=\"_blank\">
        <input type=\"hidden\" name=\"nonce\" value=\"{{ nonce }}\" />
        <input type=\"hidden\" name=\"fuzz\" value=\"{{ \"now\"|date }}\" />
        <input onclick=\"submitForm(event, this.form);\" type=\"checkbox\" id=\"trackVisits\" name=\"trackVisits\" {% if trackVisits %}checked=\"checked\"{% endif %} />
        <label for=\"trackVisits\"><strong>
        {% if trackVisits %}
            {{ 'CoreAdminHome_YouAreNotOptedOut'|translate }} {{ 'CoreAdminHome_UncheckToOptOut'|translate }}
        {% else %}
            {{ 'CoreAdminHome_YouAreOptedOut'|translate }} {{ 'CoreAdminHome_CheckToOptIn'|translate }}
        {% endif %}
        </strong></label>
        <noscript>
            <button type=\"submit\">{{ 'General_Save'|translate }}</button>
        </noscript>
    </form>
    {% endif %}
{% endif %}

{% if javascripts.external|length > 0 %}
    {% for script in javascripts.external %}
        <script type=\"text/javascript\" src=\"{{ script|raw }}\"></script>
    {% endfor %}
{% endif %}
{% if javascripts.inline|length > 0 %}
    <script>
        {% for script in javascripts.inline %}
        {{ script|raw }}
        {% endfor %}
    </script>
{% endif %}
</body>
</html>
", "@CoreAdminHome/optOut.twig", "/home/httpd/electroluxodkuchni.pl/matomo/plugins/CoreAdminHome/templates/optOut.twig");
    }
}
