<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @ScheduledReports/reportParametersScheduledReports.twig */
class __TwigTemplate_2bafb170d53fd4b3f25182a0d7f57f6e27a0fda477ad04b67c78e031bb9950da extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<div piwik-field uicontrol=\"checkbox\"
     name=\"report_email_me\"
     introduction=\"";
        // line 3
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["ScheduledReports_SendReportTo"]), "html_attr");
        echo "\"
     ng-show=\"manageScheduledReport.report.type == 'email'\"
     ng-model=\"manageScheduledReport.report.emailMe\"
     data-title=\"";
        // line 6
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["ScheduledReports_SentToMe"]), "html_attr");
        echo " (";
        echo \Piwik\piwik_escape_filter($this->env, ($context["currentUserEmail"] ?? $this->getContext($context, "currentUserEmail")), "html_attr");
        echo ")\">
</div>

<div piwik-field uicontrol=\"textarea\" var-type=\"array\"
     ng-show=\"manageScheduledReport.report.type == 'email'\"
     ng-model=\"manageScheduledReport.report.additionalEmails\"
     data-title=\"";
        // line 12
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["ScheduledReports_AlsoSendReportToTheseEmails"]), "html_attr");
        echo "\">
</div>

<script>

    \$(function () {

        resetReportParametersFunctions ['";
        // line 19
        echo \Piwik\piwik_escape_filter($this->env, ($context["reportType"] ?? $this->getContext($context, "reportType")), "html", null, true);
        echo "'] = function (report) {
            report.displayFormat = '";
        // line 20
        echo \Piwik\piwik_escape_filter($this->env, ($context["defaultDisplayFormat"] ?? $this->getContext($context, "defaultDisplayFormat")), "html", null, true);
        echo "';
            report.emailMe = ";
        // line 21
        echo \Piwik\piwik_escape_filter($this->env, ($context["defaultEmailMe"] ?? $this->getContext($context, "defaultEmailMe")), "html", null, true);
        echo ";
            report.evolutionGraph = ";
        // line 22
        echo \Piwik\piwik_escape_filter($this->env, ($context["defaultEvolutionGraph"] ?? $this->getContext($context, "defaultEvolutionGraph")), "html", null, true);
        echo ";
            report.additionalEmails = '';
        };

        updateReportParametersFunctions['";
        // line 26
        echo \Piwik\piwik_escape_filter($this->env, ($context["reportType"] ?? $this->getContext($context, "reportType")), "html", null, true);
        echo "'] = function (report) {
            if (report == null || report.parameters == null) {
                return;
            }

            var i, field, fields = ['displayFormat', 'emailMe', 'evolutionGraph', 'additionalEmails'];
            for (i in fields) {
                field = fields[i];
                if (field in report.parameters) {
                    report[field] = report.parameters[field];
                }
            }
        };

        getReportParametersFunctions['";
        // line 40
        echo \Piwik\piwik_escape_filter($this->env, ($context["reportType"] ?? $this->getContext($context, "reportType")), "html", null, true);
        echo "'] = function (report) {

            var parameters = {};

            parameters.displayFormat = report.displayFormat;
            parameters.emailMe = report.emailMe;
            parameters.evolutionGraph = report.evolutionGraph;
            parameters.additionalEmails = report.additionalEmails ? report.additionalEmails : [];

            return parameters;
        };
    });
</script>";
    }

    public function getTemplateName()
    {
        return "@ScheduledReports/reportParametersScheduledReports.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  97 => 40,  80 => 26,  73 => 22,  69 => 21,  65 => 20,  61 => 19,  51 => 12,  40 => 6,  34 => 3,  30 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("<div piwik-field uicontrol=\"checkbox\"
     name=\"report_email_me\"
     introduction=\"{{ 'ScheduledReports_SendReportTo'|translate|e('html_attr') }}\"
     ng-show=\"manageScheduledReport.report.type == 'email'\"
     ng-model=\"manageScheduledReport.report.emailMe\"
     data-title=\"{{ 'ScheduledReports_SentToMe'|translate|e('html_attr') }} ({{ currentUserEmail|e('html_attr') }})\">
</div>

<div piwik-field uicontrol=\"textarea\" var-type=\"array\"
     ng-show=\"manageScheduledReport.report.type == 'email'\"
     ng-model=\"manageScheduledReport.report.additionalEmails\"
     data-title=\"{{ 'ScheduledReports_AlsoSendReportToTheseEmails'|translate|e('html_attr') }}\">
</div>

<script>

    \$(function () {

        resetReportParametersFunctions ['{{ reportType }}'] = function (report) {
            report.displayFormat = '{{ defaultDisplayFormat }}';
            report.emailMe = {{ defaultEmailMe }};
            report.evolutionGraph = {{ defaultEvolutionGraph }};
            report.additionalEmails = '';
        };

        updateReportParametersFunctions['{{ reportType }}'] = function (report) {
            if (report == null || report.parameters == null) {
                return;
            }

            var i, field, fields = ['displayFormat', 'emailMe', 'evolutionGraph', 'additionalEmails'];
            for (i in fields) {
                field = fields[i];
                if (field in report.parameters) {
                    report[field] = report.parameters[field];
                }
            }
        };

        getReportParametersFunctions['{{ reportType }}'] = function (report) {

            var parameters = {};

            parameters.displayFormat = report.displayFormat;
            parameters.emailMe = report.emailMe;
            parameters.evolutionGraph = report.evolutionGraph;
            parameters.additionalEmails = report.additionalEmails ? report.additionalEmails : [];

            return parameters;
        };
    });
</script>", "@ScheduledReports/reportParametersScheduledReports.twig", "/home/httpd/electroluxodkuchni.pl/matomo/plugins/ScheduledReports/templates/reportParametersScheduledReports.twig");
    }
}
