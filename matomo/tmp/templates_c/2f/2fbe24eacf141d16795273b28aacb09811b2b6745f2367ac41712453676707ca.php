<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @TagManager/tagmanager.twig */
class __TwigTemplate_06a391b1b0765cb0b8575990e31c6ea60b7b095d909b312689be8df5371faaf4 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("layout.twig", "@TagManager/tagmanager.twig", 1);
        $this->blocks = [
            'body' => [$this, 'block_body'],
            'root' => [$this, 'block_root'],
            'topcontrols' => [$this, 'block_topcontrols'],
            'notification' => [$this, 'block_notification'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "layout.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 3
        $context["categoryTitle"] = ('' === $tmp = "Tag Manager") ? '' : new Markup($tmp, $this->env->getCharset());
        // line 5
        $context["bodyClass"] = call_user_func_array($this->env->getFunction('postEvent')->getCallable(), ["Template.bodyClass", "tagmanager"]);
        // line 6
        $context["isAdminArea"] = true;
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 8
    public function block_body($context, array $blocks = [])
    {
        // line 9
        echo "    ";
        $context["topMenuModule"] = "TagManager";
        // line 10
        echo "    ";
        $context["topMenuAction"] = (((isset($context["tagAction"]) || array_key_exists("tagAction", $context))) ? (_twig_default_filter(($context["tagAction"] ?? $this->getContext($context, "tagAction")), "")) : (""));
        // line 11
        echo "    ";
        echo call_user_func_array($this->env->getFunction('postEvent')->getCallable(), ["Template.header", "tagmanager"]);
        echo "
    ";
        // line 12
        $this->displayParentBlock("body", $context, $blocks);
        echo "
    ";
        // line 13
        echo call_user_func_array($this->env->getFunction('postEvent')->getCallable(), ["Template.footer", "tagmanager"]);
        echo "
";
    }

    // line 16
    public function block_root($context, array $blocks = [])
    {
        // line 17
        echo "    ";
        $this->loadTemplate("@CoreHome/_topScreen.twig", "@TagManager/tagmanager.twig", 17)->display($context);
        // line 18
        echo "
<div class=\"top_controls\">
    <div piwik-quick-access ng-cloak class=\"piwikTopControl borderedControl\"></div>

    ";
        // line 22
        $this->displayBlock('topcontrols', $context, $blocks);
        // line 32
        echo "
    <span class=\"icon icon-arrowup\"></span>
</div>

";
        // line 36
        $context["ajax"] = $this->loadTemplate("ajaxMacros.twig", "@TagManager/tagmanager.twig", 36);
        // line 37
        echo "    ";
        echo $context["ajax"]->getrequestErrorDiv((((isset($context["emailSuperUser"]) || array_key_exists("emailSuperUser", $context))) ? (_twig_default_filter(($context["emailSuperUser"] ?? $this->getContext($context, "emailSuperUser")), "")) : ("")), ($context["areAdsForProfessionalServicesEnabled"] ?? $this->getContext($context, "areAdsForProfessionalServicesEnabled")), ($context["currentModule"] ?? $this->getContext($context, "currentModule")));
        echo "
    ";
        // line 38
        echo call_user_func_array($this->env->getFunction('postEvent')->getCallable(), ["Template.beforeContent", "admin", ($context["currentModule"] ?? $this->getContext($context, "currentModule")), ($context["currentAction"] ?? $this->getContext($context, "currentAction"))]);
        echo "

<div class=\"page\">

    ";
        // line 42
        if (( !(isset($context["showMenu"]) || array_key_exists("showMenu", $context)) || ($context["showMenu"] ?? $this->getContext($context, "showMenu")))) {
            // line 43
            echo "        ";
            $context["menu"] = $this->loadTemplate("@CoreHome/_menu.twig", "@TagManager/tagmanager.twig", 43);
            // line 44
            echo "        ";
            echo $context["menu"]->getmenu(($context["tagManagerMenu"] ?? $this->getContext($context, "tagManagerMenu")), false, "Menu--admin", ($context["currentModule"] ?? $this->getContext($context, "currentModule")), ($context["currentAction"] ?? $this->getContext($context, "currentAction")));
            echo "
    ";
        }
        // line 46
        echo "
    <div class=\"pageWrap\">
        <a name=\"main\"></a>
        ";
        // line 49
        $this->displayBlock('notification', $context, $blocks);
        // line 52
        echo "        ";
        $this->loadTemplate("@CoreHome/_warningInvalidHost.twig", "@TagManager/tagmanager.twig", 52)->display($context);
        // line 53
        echo "
        <div class=\"admin\" id=\"content\" ng-cloak>

            <div class=\"ui-confirm\" id=\"alert\">
                <h2></h2>
                <input role=\"no\" type=\"button\" value=\"";
        // line 58
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_Ok"]), "html", null, true);
        echo "\"/>
            </div>

            ";
        // line 61
        $this->displayBlock('content', $context, $blocks);
        // line 63
        echo "
        </div>
    </div>
</div>

";
    }

    // line 22
    public function block_topcontrols($context, array $blocks = [])
    {
        // line 23
        echo "
        <div class=\"top_bar_sites_selector piwikTopControl\">
            <div piwik-siteselector show-selected-site=\"true\" class=\"sites_autocomplete\"></div>
        </div>

        <div class=\"piwikTopControl\">
            <div piwik-container-selector ";
        // line 29
        if (((isset($context["container"]) || array_key_exists("container", $context)) &&  !twig_test_empty(($context["container"] ?? $this->getContext($context, "container"))))) {
            echo "container-name=\"";
            echo \Piwik\piwik_escape_filter($this->env, $this->getAttribute(($context["container"] ?? $this->getContext($context, "container")), "name", []), "html", null, true);
            echo " (";
            echo \Piwik\piwik_escape_filter($this->env, $this->getAttribute(($context["container"] ?? $this->getContext($context, "container")), "idcontainer", []), "html", null, true);
            echo ")\"";
        }
        echo "></div>
        </div>
    ";
    }

    // line 49
    public function block_notification($context, array $blocks = [])
    {
        // line 50
        echo "            ";
        $this->loadTemplate("@CoreHome/_notifications.twig", "@TagManager/tagmanager.twig", 50)->display($context);
        // line 51
        echo "        ";
    }

    // line 61
    public function block_content($context, array $blocks = [])
    {
        // line 62
        echo "            ";
    }

    public function getTemplateName()
    {
        return "@TagManager/tagmanager.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  190 => 62,  187 => 61,  183 => 51,  180 => 50,  177 => 49,  164 => 29,  156 => 23,  153 => 22,  144 => 63,  142 => 61,  136 => 58,  129 => 53,  126 => 52,  124 => 49,  119 => 46,  113 => 44,  110 => 43,  108 => 42,  101 => 38,  96 => 37,  94 => 36,  88 => 32,  86 => 22,  80 => 18,  77 => 17,  74 => 16,  68 => 13,  64 => 12,  59 => 11,  56 => 10,  53 => 9,  50 => 8,  46 => 1,  44 => 6,  42 => 5,  40 => 3,  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'layout.twig' %}

{% set categoryTitle %}Tag Manager{% endset %}

{% set bodyClass = postEvent('Template.bodyClass', 'tagmanager') %}
{% set isAdminArea = true %}

{% block body %}
    {% set topMenuModule = 'TagManager' %}
    {% set topMenuAction = tagAction|default('') %}
    {{ postEvent(\"Template.header\", \"tagmanager\") }}
    {{ parent() }}
    {{ postEvent(\"Template.footer\", \"tagmanager\") }}
{% endblock %}

{% block root %}
    {% include \"@CoreHome/_topScreen.twig\" %}

<div class=\"top_controls\">
    <div piwik-quick-access ng-cloak class=\"piwikTopControl borderedControl\"></div>

    {% block topcontrols %}

        <div class=\"top_bar_sites_selector piwikTopControl\">
            <div piwik-siteselector show-selected-site=\"true\" class=\"sites_autocomplete\"></div>
        </div>

        <div class=\"piwikTopControl\">
            <div piwik-container-selector {% if container is defined and container is not empty %}container-name=\"{{ container.name }} ({{ container.idcontainer }})\"{% endif %}></div>
        </div>
    {% endblock %}

    <span class=\"icon icon-arrowup\"></span>
</div>

{% import 'ajaxMacros.twig' as ajax %}
    {{ ajax.requestErrorDiv(emailSuperUser|default(''), areAdsForProfessionalServicesEnabled, currentModule) }}
    {{ postEvent(\"Template.beforeContent\", \"admin\", currentModule, currentAction) }}

<div class=\"page\">

    {% if showMenu is not defined or showMenu %}
        {% import '@CoreHome/_menu.twig' as menu %}
        {{ menu.menu(tagManagerMenu, false, 'Menu--admin', currentModule, currentAction) }}
    {% endif %}

    <div class=\"pageWrap\">
        <a name=\"main\"></a>
        {% block notification %}
            {% include \"@CoreHome/_notifications.twig\" %}
        {% endblock %}
        {% include \"@CoreHome/_warningInvalidHost.twig\" %}

        <div class=\"admin\" id=\"content\" ng-cloak>

            <div class=\"ui-confirm\" id=\"alert\">
                <h2></h2>
                <input role=\"no\" type=\"button\" value=\"{{ 'General_Ok'|translate }}\"/>
            </div>

            {% block content %}
            {% endblock %}

        </div>
    </div>
</div>

{% endblock %}
", "@TagManager/tagmanager.twig", "/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/templates/tagmanager.twig");
    }
}
