<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @Goals/_listGoalEdit.twig */
class __TwigTemplate_43de55f27bc38cc8565fa30d4e35341d75441739ab2bad2f54bf0f36cc944dcb extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<div id='entityEditContainer' feature=\"true\"
     ng-show=\"manageGoals.showGoalList\"
     piwik-content-block content-title=\"";
        // line 3
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Goals_ManageGoals"]), "html_attr");
        echo "\"
     class=\"managegoals\">

    <div piwik-activity-indicator loading=\"manageGoals.isLoading\"></div>

    <div class=\"contentHelp\">
        ";
        // line 9
        echo call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Goals_LearnMoreAboutGoalTrackingDocumentation", "<a target='_blank' rel='noreferrer noopener' href='https://matomo.org/docs/tracking-goals-web-analytics/'>", "</a>"]);
        echo "

        ";
        // line 11
        if ( !($context["ecommerceEnabled"] ?? $this->getContext($context, "ecommerceEnabled"))) {
            // line 12
            echo "            <br /><br/>
            ";
            // line 13
            ob_start();
            // line 14
            echo "                <a href='";
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFunction('linkTo')->getCallable(), [["module" => "SitesManager", "action" => "index"]]), "html", null, true);
            echo "'>";
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["SitesManager_WebsitesManagement"]), "html", null, true);
            echo "</a>
            ";
            $context["websiteManageText"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
            // line 16
            echo "            ";
            ob_start();
            // line 17
            echo "                <a href=\"https://matomo.org/docs/ecommerce-analytics/\" rel=\"noreferrer noopener\" target=\"_blank\">";
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Goals_EcommerceReports"]), "html", null, true);
            echo "</a>
            ";
            $context["ecommerceReportText"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
            // line 19
            echo "            ";
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Goals_Optional"]), "html", null, true);
            echo " ";
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Goals_Ecommerce"]), "html", null, true);
            echo ": ";
            echo call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Goals_YouCanEnableEcommerceReports", ($context["ecommerceReportText"] ?? $this->getContext($context, "ecommerceReportText")), ($context["websiteManageText"] ?? $this->getContext($context, "websiteManageText"))]);
            echo "
        ";
        }
        // line 21
        echo "    </div>

    <table piwik-content-table>
        <thead>
        <tr>
            <th class=\"first\">Id</th>
            <th>";
        // line 27
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Goals_GoalName"]), "html", null, true);
        echo "</th>
            <th>";
        // line 28
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_Description"]), "html", null, true);
        echo "</th>
            <th>";
        // line 29
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Goals_GoalIsTriggeredWhen"]), "html", null, true);
        echo "</th>
            <th>";
        // line 30
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_ColumnRevenue"]), "html", null, true);
        echo "</th>
            ";
        // line 31
        echo call_user_func_array($this->env->getFunction('postEvent')->getCallable(), ["Template.beforeGoalListActionsHead"]);
        echo "
            ";
        // line 32
        if (($context["userCanEditGoals"] ?? $this->getContext($context, "userCanEditGoals"))) {
            // line 33
            echo "                <th>";
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_Edit"]), "html", null, true);
            echo "</th>
                <th>";
            // line 34
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_Delete"]), "html", null, true);
            echo "</th>
            ";
        }
        // line 36
        echo "        </tr>
        </thead>
        ";
        // line 38
        if (twig_test_empty(($context["goals"] ?? $this->getContext($context, "goals")))) {
            // line 39
            echo "            <tr>
                <td colspan='8'>
                    <br/>
                    ";
            // line 42
            echo call_user_func_array($this->env->getFilter('rawSafeDecoded')->getCallable(), [call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Goals_ThereIsNoGoalToManage", ($context["siteName"] ?? $this->getContext($context, "siteName"))])]);
            echo ".
                    <br/><br/>
                </td>
            </tr>
        ";
        } else {
            // line 47
            echo "            ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["goals"] ?? $this->getContext($context, "goals")));
            foreach ($context['_seq'] as $context["_key"] => $context["goal"]) {
                // line 48
                echo "                <tr id=\"";
                echo \Piwik\piwik_escape_filter($this->env, $this->getAttribute($context["goal"], "idgoal", []), "html", null, true);
                echo "\">
                    <td class=\"first\">";
                // line 49
                echo \Piwik\piwik_escape_filter($this->env, $this->getAttribute($context["goal"], "idgoal", []), "html", null, true);
                echo "</td>
                    <td>";
                // line 50
                echo \Piwik\piwik_escape_filter($this->env, $this->getAttribute($context["goal"], "name", []), "html", null, true);
                echo "</td>
                    <td>";
                // line 51
                echo \Piwik\piwik_escape_filter($this->env, $this->getAttribute($context["goal"], "description", []), "html", null, true);
                echo "</td>
                    <td>
                        <span class='matchAttribute'>";
                // line 53
                echo \Piwik\piwik_escape_filter($this->env, $this->getAttribute($context["goal"], "match_attribute", []), "html", null, true);
                echo "</span>
                        ";
                // line 54
                if (($this->getAttribute($context["goal"], "match_attribute", []) == "visit_duration")) {
                    // line 55
                    echo "                            ";
                    echo \Piwik\piwik_escape_filter($this->env, lcfirst(call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_OperationGreaterThan"])), "html", null, true);
                    echo " ";
                    echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Intl_NMinutes", $this->getAttribute($context["goal"], "pattern", [])]), "html", null, true);
                    echo "
                        ";
                } else {
                    // line 57
                    echo "                        ";
                    if ($this->getAttribute($context["goal"], "pattern_type", [], "any", true, true)) {
                        // line 58
                        echo "                            <br/>
                            ";
                        // line 59
                        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Goals_Pattern"]), "html", null, true);
                        echo " ";
                        echo \Piwik\piwik_escape_filter($this->env, $this->getAttribute($context["goal"], "pattern_type", []), "html", null, true);
                        echo ": ";
                        echo \Piwik\piwik_escape_filter($this->env, $this->getAttribute($context["goal"], "pattern", []), "html", null, true);
                        echo "
                        ";
                    }
                    // line 61
                    echo "                        ";
                }
                // line 62
                echo "                    </td>
                    <td class=\"center\">
                        ";
                // line 64
                if (($this->getAttribute($context["goal"], "revenue", []) == 0)) {
                    echo "-";
                } else {
                    echo call_user_func_array($this->env->getFilter('money')->getCallable(), [$this->getAttribute($context["goal"], "revenue", []), ($context["idSite"] ?? $this->getContext($context, "idSite"))]);
                }
                // line 65
                echo "                    </td>
                    ";
                // line 66
                echo call_user_func_array($this->env->getFunction('postEvent')->getCallable(), ["Template.beforeGoalListActionsBody", $context["goal"]]);
                echo "
                    ";
                // line 67
                if (($context["userCanEditGoals"] ?? $this->getContext($context, "userCanEditGoals"))) {
                    // line 68
                    echo "                        <td style=\"padding-top:2px\">
                            <button ng-click=\"manageGoals.editGoal(";
                    // line 69
                    echo \Piwik\piwik_escape_filter($this->env, $this->getAttribute($context["goal"], "idgoal", []), "html", null, true);
                    echo ")\" class=\"table-action\" title=\"";
                    echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_Edit"]), "html", null, true);
                    echo "\">
                                <span class=\"icon-edit\"></span>
                            </button>
                        </td>
                        <td style=\"padding-top:2px\">
                            <button ng-click=\"manageGoals.deleteGoal(";
                    // line 74
                    echo \Piwik\piwik_escape_filter($this->env, $this->getAttribute($context["goal"], "idgoal", []), "html", null, true);
                    echo ")\" class=\"table-action\" title=\"";
                    echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_Delete"]), "html", null, true);
                    echo "\">
                                <span class=\"icon-delete\"></span>
                            </button>
                        </td>
                    ";
                }
                // line 79
                echo "                </tr>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['goal'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 81
            echo "        ";
        }
        // line 82
        echo "    </table>

    ";
        // line 84
        if ((($context["userCanEditGoals"] ?? $this->getContext($context, "userCanEditGoals")) &&  !(isset($context["onlyShowAddNewGoal"]) || array_key_exists("onlyShowAddNewGoal", $context)))) {
            // line 85
            echo "        <div class=\"tableActionBar\">
            <button id=\"add-goal\" ng-click=\"manageGoals.createGoal()\">
                <span class=\"icon-add\"></span>
                ";
            // line 88
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Goals_AddNewGoal"]), "html", null, true);
            echo "
            </button>
        </div>
    ";
        }
        // line 92
        echo "
</div>

<div class=\"ui-confirm\" id=\"confirm\">
    <h2></h2>
    <input role=\"yes\" type=\"button\" value=\"";
        // line 97
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_Yes"]), "html", null, true);
        echo "\"/>
    <input role=\"no\" type=\"button\" value=\"";
        // line 98
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_No"]), "html", null, true);
        echo "\"/>
</div>

<script type=\"text/javascript\">
    var goalTypeToTranslation = {
        \"manually\": \"";
        // line 103
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Goals_ManuallyTriggeredUsingJavascriptFunction"]), "html", null, true);
        echo "\",
        \"file\": \"";
        // line 104
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Goals_Download"]), "html", null, true);
        echo "\",
        \"url\": \"";
        // line 105
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Goals_VisitUrl"]), "html", null, true);
        echo "\",
        \"title\": \"";
        // line 106
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Goals_VisitPageTitle"]), "html", null, true);
        echo "\",
        \"external_website\": \"";
        // line 107
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Goals_ClickOutlink"]), "html", null, true);
        echo "\",
        \"event_action\": \"";
        // line 108
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Goals_SendEvent"]), "html", null, true);
        echo " (";
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Events_EventAction"]), "html", null, true);
        echo ")\",
        \"event_category\": \"";
        // line 109
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Goals_SendEvent"]), "html", null, true);
        echo " (";
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Events_EventCategory"]), "html", null, true);
        echo ")\",
        \"event_name\": \"";
        // line 110
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Goals_SendEvent"]), "html", null, true);
        echo " (";
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Events_EventName"]), "html", null, true);
        echo ")\",
        \"visit_duration\": \"";
        // line 111
        echo \Piwik\piwik_escape_filter($this->env, ucfirst(call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Goals_VisitDuration"])), "html", null, true);
        echo "\"
    };

    \$(document).ready(function () {
        // translation of the goal \"match attribute\" to human readable description
        \$('.matchAttribute').each(function () {
            var matchAttribute = \$(this).text();
            var translation = goalTypeToTranslation[matchAttribute];
            \$(this).text(translation);
        });
    });
</script>
";
    }

    public function getTemplateName()
    {
        return "@Goals/_listGoalEdit.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  323 => 111,  317 => 110,  311 => 109,  305 => 108,  301 => 107,  297 => 106,  293 => 105,  289 => 104,  285 => 103,  277 => 98,  273 => 97,  266 => 92,  259 => 88,  254 => 85,  252 => 84,  248 => 82,  245 => 81,  238 => 79,  228 => 74,  218 => 69,  215 => 68,  213 => 67,  209 => 66,  206 => 65,  200 => 64,  196 => 62,  193 => 61,  184 => 59,  181 => 58,  178 => 57,  170 => 55,  168 => 54,  164 => 53,  159 => 51,  155 => 50,  151 => 49,  146 => 48,  141 => 47,  133 => 42,  128 => 39,  126 => 38,  122 => 36,  117 => 34,  112 => 33,  110 => 32,  106 => 31,  102 => 30,  98 => 29,  94 => 28,  90 => 27,  82 => 21,  72 => 19,  66 => 17,  63 => 16,  55 => 14,  53 => 13,  50 => 12,  48 => 11,  43 => 9,  34 => 3,  30 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("<div id='entityEditContainer' feature=\"true\"
     ng-show=\"manageGoals.showGoalList\"
     piwik-content-block content-title=\"{{ 'Goals_ManageGoals'|translate|e('html_attr') }}\"
     class=\"managegoals\">

    <div piwik-activity-indicator loading=\"manageGoals.isLoading\"></div>

    <div class=\"contentHelp\">
        {{ 'Goals_LearnMoreAboutGoalTrackingDocumentation'|translate(\"<a target='_blank' rel='noreferrer noopener' href='https://matomo.org/docs/tracking-goals-web-analytics/'>\",\"</a>\")|raw }}

        {% if not ecommerceEnabled %}
            <br /><br/>
            {% set websiteManageText %}
                <a href='{{ linkTo({'module':'SitesManager','action':'index' }) }}'>{{ 'SitesManager_WebsitesManagement'|translate }}</a>
            {% endset %}
            {% set ecommerceReportText %}
                <a href=\"https://matomo.org/docs/ecommerce-analytics/\" rel=\"noreferrer noopener\" target=\"_blank\">{{ 'Goals_EcommerceReports'|translate }}</a>
            {% endset %}
            {{ 'Goals_Optional'|translate }} {{ 'Goals_Ecommerce'|translate }}: {{ 'Goals_YouCanEnableEcommerceReports'|translate(ecommerceReportText,websiteManageText)|raw }}
        {% endif %}
    </div>

    <table piwik-content-table>
        <thead>
        <tr>
            <th class=\"first\">Id</th>
            <th>{{ 'Goals_GoalName'|translate }}</th>
            <th>{{ 'General_Description'|translate }}</th>
            <th>{{ 'Goals_GoalIsTriggeredWhen'|translate }}</th>
            <th>{{ 'General_ColumnRevenue'|translate }}</th>
            {{ postEvent(\"Template.beforeGoalListActionsHead\") }}
            {% if userCanEditGoals %}
                <th>{{ 'General_Edit'|translate }}</th>
                <th>{{ 'General_Delete'|translate }}</th>
            {% endif %}
        </tr>
        </thead>
        {% if goals is empty %}
            <tr>
                <td colspan='8'>
                    <br/>
                    {{ 'Goals_ThereIsNoGoalToManage'|translate(siteName)|rawSafeDecoded }}.
                    <br/><br/>
                </td>
            </tr>
        {% else %}
            {% for goal in goals %}
                <tr id=\"{{ goal.idgoal }}\">
                    <td class=\"first\">{{ goal.idgoal }}</td>
                    <td>{{ goal.name }}</td>
                    <td>{{ goal.description }}</td>
                    <td>
                        <span class='matchAttribute'>{{ goal.match_attribute }}</span>
                        {% if goal.match_attribute == 'visit_duration' %}
                            {{ 'General_OperationGreaterThan'|translate|lcfirst }} {{ 'Intl_NMinutes'|translate(goal.pattern) }}
                        {% else %}
                        {% if goal.pattern_type is defined %}
                            <br/>
                            {{ 'Goals_Pattern'|translate }} {{ goal.pattern_type }}: {{ goal.pattern }}
                        {% endif %}
                        {% endif %}
                    </td>
                    <td class=\"center\">
                        {% if goal.revenue==0 %}-{% else %}{{ goal.revenue|money(idSite)|raw }}{% endif %}
                    </td>
                    {{ postEvent(\"Template.beforeGoalListActionsBody\", goal) }}
                    {% if userCanEditGoals %}
                        <td style=\"padding-top:2px\">
                            <button ng-click=\"manageGoals.editGoal({{ goal.idgoal }})\" class=\"table-action\" title=\"{{ 'General_Edit'|translate }}\">
                                <span class=\"icon-edit\"></span>
                            </button>
                        </td>
                        <td style=\"padding-top:2px\">
                            <button ng-click=\"manageGoals.deleteGoal({{ goal.idgoal }})\" class=\"table-action\" title=\"{{ 'General_Delete'|translate }}\">
                                <span class=\"icon-delete\"></span>
                            </button>
                        </td>
                    {% endif %}
                </tr>
            {% endfor %}
        {% endif %}
    </table>

    {% if userCanEditGoals and onlyShowAddNewGoal is not defined %}
        <div class=\"tableActionBar\">
            <button id=\"add-goal\" ng-click=\"manageGoals.createGoal()\">
                <span class=\"icon-add\"></span>
                {{ 'Goals_AddNewGoal'|translate }}
            </button>
        </div>
    {% endif %}

</div>

<div class=\"ui-confirm\" id=\"confirm\">
    <h2></h2>
    <input role=\"yes\" type=\"button\" value=\"{{ 'General_Yes'|translate }}\"/>
    <input role=\"no\" type=\"button\" value=\"{{ 'General_No'|translate }}\"/>
</div>

<script type=\"text/javascript\">
    var goalTypeToTranslation = {
        \"manually\": \"{{ 'Goals_ManuallyTriggeredUsingJavascriptFunction'|translate }}\",
        \"file\": \"{{ 'Goals_Download'|translate }}\",
        \"url\": \"{{ 'Goals_VisitUrl'|translate }}\",
        \"title\": \"{{ 'Goals_VisitPageTitle'|translate }}\",
        \"external_website\": \"{{ 'Goals_ClickOutlink'|translate }}\",
        \"event_action\": \"{{ 'Goals_SendEvent'|translate }} ({{ 'Events_EventAction'|translate }})\",
        \"event_category\": \"{{ 'Goals_SendEvent'|translate }} ({{ 'Events_EventCategory'|translate }})\",
        \"event_name\": \"{{ 'Goals_SendEvent'|translate }} ({{ 'Events_EventName'|translate }})\",
        \"visit_duration\": \"{{ 'Goals_VisitDuration'|translate|ucfirst }}\"
    };

    \$(document).ready(function () {
        // translation of the goal \"match attribute\" to human readable description
        \$('.matchAttribute').each(function () {
            var matchAttribute = \$(this).text();
            var translation = goalTypeToTranslation[matchAttribute];
            \$(this).text(translation);
        });
    });
</script>
", "@Goals/_listGoalEdit.twig", "/home/httpd/electroluxodkuchni.pl/matomo/plugins/Goals/templates/_listGoalEdit.twig");
    }
}
