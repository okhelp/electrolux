<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @ScheduledReports/_listReports.twig */
class __TwigTemplate_0901839159ee22cc2169e74a3eac2541f47b7c112827e6e93659854db5a281a9 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<div id='entityEditContainer' class=\"entityTableContainer\"
     piwik-content-block
     content-title=\"";
        // line 3
        echo \Piwik\piwik_escape_filter($this->env, ($context["title"] ?? $this->getContext($context, "title")), "html_attr");
        echo "\"
     help-url=\"https://matomo.org/docs/email-reports/\"
     feature=\"true\"
     ng-show=\"manageScheduledReport.showReportsList\">

    <table piwik-content-table>
        <thead>
        <tr>
            <th class=\"first\">";
        // line 11
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_Description"]), "html", null, true);
        echo "</th>
            <th>";
        // line 12
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["ScheduledReports_EmailSchedule"]), "html", null, true);
        echo "</th>
            <th>";
        // line 13
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["ScheduledReports_ReportFormat"]), "html", null, true);
        echo "</th>
            <th>";
        // line 14
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["ScheduledReports_SendReportTo"]), "html", null, true);
        echo "</th>
            <th>";
        // line 15
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_Download"]), "html", null, true);
        echo "</th>
            <th>";
        // line 16
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_Edit"]), "html", null, true);
        echo "</th>
            <th>";
        // line 17
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_Delete"]), "html", null, true);
        echo "</th>
        </tr>
        </thead>

    ";
        // line 21
        if ((($context["userLogin"] ?? $this->getContext($context, "userLogin")) == "anonymous")) {
            // line 22
            echo "        <tr>
            <td colspan='7'>
                <br/>
                ";
            // line 25
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["ScheduledReports_MustBeLoggedIn"]), "html", null, true);
            echo "
                <br/>&rsaquo; <a href='index.php?module=";
            // line 26
            echo \Piwik\piwik_escape_filter($this->env, ($context["loginModule"] ?? $this->getContext($context, "loginModule")), "html", null, true);
            echo "'>";
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Login_LogIn"]), "html", null, true);
            echo "</a>
                <br/><br/>
            </td>
        </tr>
    ";
        } elseif (twig_test_empty(        // line 30
($context["reports"] ?? $this->getContext($context, "reports")))) {
            // line 31
            echo "        <tr>

            <td colspan='7'>
                <br/>
                ";
            // line 35
            echo call_user_func_array($this->env->getFilter('rawSafeDecoded')->getCallable(), [call_user_func_array($this->env->getFilter('translate')->getCallable(), ["ScheduledReports_ThereIsNoReportToManage", ($context["siteName"] ?? $this->getContext($context, "siteName"))])]);
            echo ".
                <br/><br/>
            </td>
        </tr>
    ";
        } else {
            // line 40
            echo "    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["reports"] ?? $this->getContext($context, "reports")));
            foreach ($context['_seq'] as $context["_key"] => $context["report"]) {
                // line 41
                echo "        <tr>
            <td class=\"first\">
                ";
                // line 43
                echo call_user_func_array($this->env->getFilter('rawSafeDecoded')->getCallable(), [$this->getAttribute($context["report"], "description", [])]);
                echo "
                ";
                // line 44
                if ((($context["segmentEditorActivated"] ?? $this->getContext($context, "segmentEditorActivated")) && $this->getAttribute($context["report"], "idsegment", []))) {
                    // line 45
                    echo "                    <div class=\"entityInlineHelp\" style=\"font-size: 9pt;\">
                        ";
                    // line 46
                    echo \Piwik\piwik_escape_filter($this->env, $this->getAttribute(($context["savedSegmentsById"] ?? $this->getContext($context, "savedSegmentsById")), $this->getAttribute($context["report"], "idsegment", []), [], "array"), "html", null, true);
                    echo "
                    </div>
                ";
                }
                // line 49
                echo "            </td>
            <td>";
                // line 50
                echo \Piwik\piwik_escape_filter($this->env, $this->getAttribute(($context["periods"] ?? $this->getContext($context, "periods")), $this->getAttribute($context["report"], "period", []), [], "array"), "html", null, true);
                echo "
                <!-- Last sent on ";
                // line 51
                echo \Piwik\piwik_escape_filter($this->env, $this->getAttribute($context["report"], "ts_last_sent", []), "html", null, true);
                echo " -->
            </td>
            <td>
                ";
                // line 54
                if ( !twig_test_empty($this->getAttribute($context["report"], "format", []))) {
                    // line 55
                    echo "                    ";
                    echo \Piwik\piwik_escape_filter($this->env, twig_upper_filter($this->env, $this->getAttribute($context["report"], "format", [])), "html", null, true);
                    echo "
                ";
                }
                // line 57
                echo "            </td>
            <td>
                ";
                // line 60
                echo "                ";
                if ((twig_length_filter($this->env, $this->getAttribute($context["report"], "recipients", [])) == 0)) {
                    // line 61
                    echo "                    ";
                    echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["ScheduledReports_NoRecipients"]), "html", null, true);
                    echo "
                ";
                } else {
                    // line 63
                    echo "                    ";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["report"], "recipients", []));
                    foreach ($context['_seq'] as $context["_key"] => $context["recipient"]) {
                        // line 64
                        echo "                        ";
                        echo \Piwik\piwik_escape_filter($this->env, $context["recipient"], "html", null, true);
                        echo "
                        <br/>
                    ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['recipient'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 67
                    echo "                    ";
                    // line 68
                    echo "                    <a href=\"#\"
                       ng-click=\"manageScheduledReport.sendReportNow(";
                    // line 69
                    echo \Piwik\piwik_escape_filter($this->env, $this->getAttribute($context["report"], "idreport", []), "html", null, true);
                    echo ")\"
                       name=\"linkSendNow\" class=\"link_but withIcon\" style=\"margin-top:3px;\">
                        <img border=0 src='";
                    // line 71
                    echo \Piwik\piwik_escape_filter($this->env, $this->getAttribute(($context["reportTypes"] ?? $this->getContext($context, "reportTypes")), $this->getAttribute($context["report"], "type", []), [], "array"), "html", null, true);
                    echo "'/>
                        ";
                    // line 72
                    echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["ScheduledReports_SendReportNow"]), "html", null, true);
                    echo "
                    </a>
                ";
                }
                // line 75
                echo "            </td>
            <td>
                ";
                // line 78
                echo "                <form action=\"";
                echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFunction('linkTo')->getCallable(), [["module" => "API", "segment" => null, "method" => "ScheduledReports.generateReport", "idReport" => $this->getAttribute(                // line 79
$context["report"], "idreport", []), "outputType" =>                 // line 80
($context["downloadOutputType"] ?? $this->getContext($context, "downloadOutputType")), "language" => ($context["language"] ?? $this->getContext($context, "language")), "format" => ((twig_in_filter($this->getAttribute(                // line 81
$context["report"], "format", []), [0 => "html", 1 => "csv"])) ? ($this->getAttribute($context["report"], "format", [])) : (false))]]), "html", null, true);
                echo "\"
                      method=\"POST\"
                      target=\"_blank\"
                      id=\"downloadReportForm_";
                // line 84
                echo \Piwik\piwik_escape_filter($this->env, $this->getAttribute($context["report"], "idreport", []), "html_attr");
                echo "\"
                >
                    <input type=\"hidden\" name=\"token_auth\" value=\"";
                // line 86
                echo \Piwik\piwik_escape_filter($this->env, ($context["token_auth"] ?? $this->getContext($context, "token_auth")), "html_attr");
                echo "\">
                </form>
                <a href=\"javascript:void(0)\"
                   ng-click=\"manageScheduledReport.displayReport(";
                // line 89
                echo \Piwik\piwik_escape_filter($this->env, twig_jsonencode_filter($this->getAttribute($context["report"], "idreport", [])), "html", null, true);
                echo ")\"
                   rel=\"noreferrer noopener\" name=\"linkDownloadReport\" id=\"";
                // line 90
                echo \Piwik\piwik_escape_filter($this->env, $this->getAttribute($context["report"], "idreport", []), "html_attr");
                echo "\" class=\"link_but withIcon\">
                    <img src='";
                // line 91
                echo \Piwik\piwik_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["reportFormatsByReportType"] ?? $this->getContext($context, "reportFormatsByReportType")), $this->getAttribute($context["report"], "type", []), [], "array"), $this->getAttribute($context["report"], "format", []), [], "array"), "html", null, true);
                echo "' border=\"0\" width=\"16px\" height=\"16px\"/>
                    ";
                // line 92
                echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_Download"]), "html", null, true);
                echo "
                </a>
            </td>
            <td style=\"text-align: center;padding-top:2px;\">
                <button ng-click=\"manageScheduledReport.editReport(";
                // line 96
                echo \Piwik\piwik_escape_filter($this->env, twig_jsonencode_filter($this->getAttribute($context["report"], "idreport", [])), "html", null, true);
                echo ")\"
                        class=\"table-action\" title=\"";
                // line 97
                echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_Edit"]), "html_attr");
                echo "\">
                    <span class=\"icon-edit\"></span>
                </button>
            </td>
            <td style=\"text-align: center;padding-top:2px;\">
                <button ng-click=\"manageScheduledReport.deleteReport(";
                // line 102
                echo \Piwik\piwik_escape_filter($this->env, twig_jsonencode_filter($this->getAttribute($context["report"], "idreport", [])), "html", null, true);
                echo ")\"
                        class=\"table-action\" title=\"";
                // line 103
                echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_Delete"]), "html_attr");
                echo "\">
                    <span class=\"icon-delete\"></span>
                </button>
            </td>
        </tr>
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['report'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 109
            echo "    ";
        }
        // line 110
        echo "    </table>

    <div class=\"tableActionBar\">
        ";
        // line 113
        if ((($context["userLogin"] ?? $this->getContext($context, "userLogin")) != "anonymous")) {
            // line 114
            echo "            <button id=\"add-report\" ng-click=\"manageScheduledReport.createReport()\" >
                <span class=\"icon-add\"></span>
                ";
            // line 116
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["ScheduledReports_CreateAndScheduleReport"]), "html", null, true);
            echo "
            </button>
        ";
        }
        // line 119
        echo "    </div>

</div>
";
    }

    public function getTemplateName()
    {
        return "@ScheduledReports/_listReports.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  297 => 119,  291 => 116,  287 => 114,  285 => 113,  280 => 110,  277 => 109,  265 => 103,  261 => 102,  253 => 97,  249 => 96,  242 => 92,  238 => 91,  234 => 90,  230 => 89,  224 => 86,  219 => 84,  213 => 81,  212 => 80,  211 => 79,  209 => 78,  205 => 75,  199 => 72,  195 => 71,  190 => 69,  187 => 68,  185 => 67,  175 => 64,  170 => 63,  164 => 61,  161 => 60,  157 => 57,  151 => 55,  149 => 54,  143 => 51,  139 => 50,  136 => 49,  130 => 46,  127 => 45,  125 => 44,  121 => 43,  117 => 41,  112 => 40,  104 => 35,  98 => 31,  96 => 30,  87 => 26,  83 => 25,  78 => 22,  76 => 21,  69 => 17,  65 => 16,  61 => 15,  57 => 14,  53 => 13,  49 => 12,  45 => 11,  34 => 3,  30 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("<div id='entityEditContainer' class=\"entityTableContainer\"
     piwik-content-block
     content-title=\"{{ title|e('html_attr') }}\"
     help-url=\"https://matomo.org/docs/email-reports/\"
     feature=\"true\"
     ng-show=\"manageScheduledReport.showReportsList\">

    <table piwik-content-table>
        <thead>
        <tr>
            <th class=\"first\">{{ 'General_Description'|translate }}</th>
            <th>{{ 'ScheduledReports_EmailSchedule'|translate }}</th>
            <th>{{ 'ScheduledReports_ReportFormat'|translate }}</th>
            <th>{{ 'ScheduledReports_SendReportTo'|translate }}</th>
            <th>{{ 'General_Download'|translate }}</th>
            <th>{{ 'General_Edit'|translate }}</th>
            <th>{{ 'General_Delete'|translate }}</th>
        </tr>
        </thead>

    {% if userLogin == 'anonymous' %}
        <tr>
            <td colspan='7'>
                <br/>
                {{ 'ScheduledReports_MustBeLoggedIn'|translate }}
                <br/>&rsaquo; <a href='index.php?module={{ loginModule }}'>{{ 'Login_LogIn'|translate }}</a>
                <br/><br/>
            </td>
        </tr>
    {% elseif reports is empty %}
        <tr>

            <td colspan='7'>
                <br/>
                {{ 'ScheduledReports_ThereIsNoReportToManage'|translate(siteName)|rawSafeDecoded }}.
                <br/><br/>
            </td>
        </tr>
    {% else %}
    {% for report in reports %}
        <tr>
            <td class=\"first\">
                {{ report.description|rawSafeDecoded }}
                {% if segmentEditorActivated and report.idsegment %}
                    <div class=\"entityInlineHelp\" style=\"font-size: 9pt;\">
                        {{ savedSegmentsById[report.idsegment] }}
                    </div>
                {% endif %}
            </td>
            <td>{{ periods[report.period] }}
                <!-- Last sent on {{ report.ts_last_sent }} -->
            </td>
            <td>
                {% if report.format is not empty %}
                    {{ report.format|upper }}
                {% endif %}
            </td>
            <td>
                {# report recipients #}
                {% if report.recipients|length == 0 %}
                    {{ 'ScheduledReports_NoRecipients'|translate }}
                {% else %}
                    {% for recipient in report.recipients %}
                        {{ recipient }}
                        <br/>
                    {% endfor %}
                    {# send now link #}
                    <a href=\"#\"
                       ng-click=\"manageScheduledReport.sendReportNow({{ report.idreport }})\"
                       name=\"linkSendNow\" class=\"link_but withIcon\" style=\"margin-top:3px;\">
                        <img border=0 src='{{ reportTypes[report.type] }}'/>
                        {{ 'ScheduledReports_SendReportNow'|translate }}
                    </a>
                {% endif %}
            </td>
            <td>
                {# download link #}
                <form action=\"{{ linkTo({ 'module':'API', 'segment': null,
                    'method':'ScheduledReports.generateReport', 'idReport':report.idreport,
                    'outputType':downloadOutputType, 'language':language,
                    'format': (report.format in ['html', 'csv']) ? report.format : false }) }}\"
                      method=\"POST\"
                      target=\"_blank\"
                      id=\"downloadReportForm_{{ report.idreport|e('html_attr') }}\"
                >
                    <input type=\"hidden\" name=\"token_auth\" value=\"{{ token_auth|e('html_attr') }}\">
                </form>
                <a href=\"javascript:void(0)\"
                   ng-click=\"manageScheduledReport.displayReport({{ report.idreport|json_encode }})\"
                   rel=\"noreferrer noopener\" name=\"linkDownloadReport\" id=\"{{ report.idreport|e('html_attr') }}\" class=\"link_but withIcon\">
                    <img src='{{ reportFormatsByReportType[report.type][report.format] }}' border=\"0\" width=\"16px\" height=\"16px\"/>
                    {{ 'General_Download'|translate }}
                </a>
            </td>
            <td style=\"text-align: center;padding-top:2px;\">
                <button ng-click=\"manageScheduledReport.editReport({{ report.idreport|json_encode }})\"
                        class=\"table-action\" title=\"{{ 'General_Edit'|translate|e('html_attr') }}\">
                    <span class=\"icon-edit\"></span>
                </button>
            </td>
            <td style=\"text-align: center;padding-top:2px;\">
                <button ng-click=\"manageScheduledReport.deleteReport({{ report.idreport|json_encode }})\"
                        class=\"table-action\" title=\"{{ 'General_Delete'|translate|e('html_attr') }}\">
                    <span class=\"icon-delete\"></span>
                </button>
            </td>
        </tr>
    {% endfor %}
    {% endif %}
    </table>

    <div class=\"tableActionBar\">
        {% if userLogin != 'anonymous' %}
            <button id=\"add-report\" ng-click=\"manageScheduledReport.createReport()\" >
                <span class=\"icon-add\"></span>
                {{ 'ScheduledReports_CreateAndScheduleReport'|translate }}
            </button>
        {% endif %}
    </div>

</div>
", "@ScheduledReports/_listReports.twig", "/home/httpd/electroluxodkuchni.pl/matomo/plugins/ScheduledReports/templates/_listReports.twig");
    }
}
