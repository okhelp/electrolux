<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @TagManager/manageTriggers.twig */
class __TwigTemplate_8b9511fd8a1e47c58489bd1387926288e1ba095cc9dfef64b233904a75fc1ac7 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@TagManager/tagmanager.twig", "@TagManager/manageTriggers.twig", 1);
        $this->blocks = [
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "@TagManager/tagmanager.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 3
        $context["title"] = call_user_func_array($this->env->getFilter('translate')->getCallable(), ["TagManager_Triggers"]);
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_content($context, array $blocks = [])
    {
        // line 6
        echo "    <div piwik-trigger-manage id-container=\"";
        echo \Piwik\piwik_escape_filter($this->env, $this->getAttribute(($context["container"] ?? $this->getContext($context, "container")), "idcontainer", []), "html", null, true);
        echo "\" id-container-version=\"";
        echo \Piwik\piwik_escape_filter($this->env, ($context["idcontainerversion"] ?? $this->getContext($context, "idcontainerversion")), "html", null, true);
        echo "\">
    </div>
";
    }

    public function getTemplateName()
    {
        return "@TagManager/manageTriggers.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  45 => 6,  42 => 5,  38 => 1,  36 => 3,  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% extends '@TagManager/tagmanager.twig' %}

{% set title = 'TagManager_Triggers'|translate %}

{% block content %}
    <div piwik-trigger-manage id-container=\"{{ container.idcontainer }}\" id-container-version=\"{{ idcontainerversion }}\">
    </div>
{% endblock %}", "@TagManager/manageTriggers.twig", "/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/templates/manageTriggers.twig");
    }
}
