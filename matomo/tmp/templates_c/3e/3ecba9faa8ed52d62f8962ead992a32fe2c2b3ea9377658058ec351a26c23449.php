<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @Referrers/campaignBuilder.twig */
class __TwigTemplate_3b0bd05fc8e7903cb28c9571d737b074a75ff6897f17a43e28b794bacff8c930 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<div class=\"widgetBody\">
    <p>";
        // line 2
        echo call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Referrers_URLCampaignBuilderIntro", "<a href=\"https://matomo.org/docs/tracking-campaigns-url-builder/\" rel=\"noreferrer noopener\">", "</a>", "<a href=\"https://matomo.org/docs/tracking-campaigns/\" rel=\"noreferrer noopener\">", "</a>"]);
        echo "</p>
    <div matomo-campaign-builder has-extra-plugin=\"";
        // line 3
        echo \Piwik\piwik_escape_filter($this->env, ($context["hasExtraPlugin"] ?? $this->getContext($context, "hasExtraPlugin")), "html_attr");
        echo "\"></div>
</div>";
    }

    public function getTemplateName()
    {
        return "@Referrers/campaignBuilder.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  37 => 3,  33 => 2,  30 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("<div class=\"widgetBody\">
    <p>{{ 'Referrers_URLCampaignBuilderIntro'|translate('<a href=\"https://matomo.org/docs/tracking-campaigns-url-builder/\" rel=\"noreferrer noopener\">', '</a>', '<a href=\"https://matomo.org/docs/tracking-campaigns/\" rel=\"noreferrer noopener\">', '</a>')|raw }}</p>
    <div matomo-campaign-builder has-extra-plugin=\"{{ hasExtraPlugin|e('html_attr') }}\"></div>
</div>", "@Referrers/campaignBuilder.twig", "/home/httpd/electroluxodkuchni.pl/matomo/plugins/Referrers/templates/campaignBuilder.twig");
    }
}
