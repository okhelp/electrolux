<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @TagManager/debug.twig */
class __TwigTemplate_9d7979317bf9e5402984055ef4ae9eaec7ff013da53422288efb6d3dc3083941 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<!DOCTYPE html>
<html id=\"ng-app\" ng-app=\"piwikApp\">
<head>
    <title>Debug Matomo Tag Manager</title>
    <base href=\"";
        // line 5
        echo \Piwik\piwik_escape_filter($this->env, ($context["piwikUrl"] ?? $this->getContext($context, "piwikUrl")), "html_attr");
        echo "\" >
    <meta charset=\"utf-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=EDGE,chrome=1\"/>
    <meta name=\"viewport\" content=\"initial-scale=1.0\"/>
    <meta name=\"robots\" content=\"noindex,nofollow\">
    <style>
        .nav-wrapper .brand-logo {
            position: relative;
            pointer-events: none;
        }
        .home h2, .home h3 {
            margin-top: 0;
        }
    </style>
    <script>
        var piwik_translations = {};
    </script>
    <script type=\"text/javascript\">

        window.angularDigest = function () {
            if ('undefined' === typeof jQuery) {
                return;
            }
            jQuery(function () {
                var \$timeout = piwikHelper.getAngularDependency('\$timeout');
                if (\$timeout) {
                    \$timeout(function () {});
                }
            });
        };
        window.mtmEvents = {
            events: [],
            push: function (event) {
                if (event) {
                    event.index0 = this.events.length;
                    event.index = this.events.length + 1;
                }
                this.events.push(event);

                window.angularDigest();
            }
        };
        window.mtmLogs = {
            logs: [],
            push: function (log) {
                this.logs.push(log);

                window.angularDigest();
            }
        };
    </script>
</head>
<body ng-app=\"app\" >
    <div id=\"root\" ng-controller=\"MtmDebugController as mtmDebug\">
        <nav>
            <div class=\"nav-wrapper\">
                <ul>
                    <li>";
        // line 62
        $this->loadTemplate("@CoreHome/_logo.twig", "@TagManager/debug.twig", 62)->display($context);
        echo "</li>
                    <li ng-class=\"{'active': (mtmDebug.contentTab == 'tags' || !mtmDebug.contentTab)}\"><a ng-click=\"mtmDebug.contentTab = 'tags'\">Tags</a></li>
                    <li ng-class=\"{'active': (mtmDebug.contentTab == 'triggers')}\"><a ng-click=\"mtmDebug.contentTab = 'triggers'\">Triggers</a></li>
                    <li ng-class=\"{'active': (mtmDebug.contentTab == 'variables')}\"><a ng-click=\"mtmDebug.contentTab = 'variables'\">Variables</a></li>
                    <li ng-class=\"{'active': (mtmDebug.contentTab == 'dataLayer')}\"><a ng-click=\"mtmDebug.contentTab = 'dataLayer'\">Data Layer</a></li>
                    <li ng-class=\"{'active': (mtmDebug.contentTab == 'logs')}\"><a ng-click=\"mtmDebug.contentTab = 'logs'\">Logs</a></li>
                </ul>
            </div>
        </nav>

        <div class=\"page\" style=\"clear:both;\">
            <div id=\"secondNavBar\" class=\"Menu--dashboard z-depth-1\"
                 ng-show=\"mtmDebug.contentTab != 'logs'\">
                <ul class=\"navbar\" role=\"menu\" style=\"padding: 0;\">
                    <li class=\"menuTab\" role=\"menuitem\">
                        <span class=\"item\" style=\"font-weight: normal;\"> Events</span>
                    </li>
                    <li ng-show=\"(mtmDebug.mtmEvents|length) == 0\">No event executed</li>
                    <li class=\"menuTab\" role=\"menuitem\" ng-class=\"{'active': eventIndex === mtmDebug.selectedEventIndex}\"
                    ng-repeat=\"event in mtmDebug.mtmEvents | orderBy:'\$index':true\">
                        <a class=\"item ng-binding\"  ng-click=\"mtmDebug.selectEvent(event.index0)\" title=\"Time: ";
        // line 82
        echo "{{ event.time }}";
        echo ". Trigger: '";
        echo "{{ event.metTrigger.name }}";
        echo "'\">
                            ";
        // line 83
        echo "{{ event.index }}";
        echo ": ";
        echo "{{ event.name }}";
        echo "
                        </a>
                    </li>
                </ul>
            </div>
            <div class=\"pageWrap\">
                <div class=\"home\" id=\"content\" ng-cloak>
                    <h2 ng-show=\"mtmDebug.contentTab != 'logs'\">Event ";
        // line 90
        echo "{{ mtmDebug.selectedEventIndex + 1 }}";
        echo ": ";
        echo "{{ mtmDebug.selectedEvent.name }}";
        echo " (";
        echo "{{ mtmDebug.selectedEvent.container.id }} - {{ mtmDebug.selectedEvent.container.versionName || \"Draft version\" }}";
        echo ")</h2>

                    <div ng-show=\"mtmDebug.contentTab == 'tags' || !mtmDebug.contentTab\">
                        <h3>Fired Tags</h3>
                        <table class=\"entityTable\">
                            <thead>
                            <tr>
                                <th>Action</th>
                                <th>Name</th>
                                <th>Type</th>
                                <th>Fired count</th>
                            </tr>
                            </thead>
                            <tbody>
                                <tr ng-show=\"(mtmDebug.selectedEvent.tags|length) == 0\">
                                    <td colspan=\"4\">No tags</td>
                                </tr>
                                <tr ng-repeat=\"tag in mtmDebug.selectedEvent.tags\">
                                    <td>";
        // line 108
        echo "{{tag.action }}";
        echo "</td>
                                    <td>";
        // line 109
        echo "{{tag.name }}";
        echo "</td>
                                    <td>";
        // line 110
        echo "{{tag.type }}";
        echo "</td>
                                    <td>";
        // line 111
        echo "{{tag.numExecuted }}";
        echo "</td>
                                </tr>
                            </tbody>
                        </table>

                        <h3 style=\"margin-top:30px;\">Not Yet Fired Tags</h3>
                        <table class=\"entityTable\">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Type</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr ng-show=\"(mtmDebug.selectedEvent.notFiredTags|length) == 0\">
                                <td colspan=\"4\">No tags</td>
                            </tr>
                            <tr ng-repeat=\"tag in mtmDebug.selectedEvent.notFiredTags\">
                                <td>";
        // line 129
        echo "{{tag.name }}";
        echo "</td>
                                <td>";
        // line 130
        echo "{{tag.type }}";
        echo "</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div ng-show=\"mtmDebug.contentTab == 'triggers'\">
                        <h3>Triggers</h3>

                        <table class=\"entityTable\">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Type</th>
                            </tr>
                            </thead>
                            <tbody>
                                <tr ng-show=\"!mtmDebug.selectedEvent.metTrigger\">
                                    <td colspan=\"4\">No trigger</td>
                                </tr>
                                <tr ng-show=\"mtmDebug.selectedEvent.metTrigger\">
                                    <td>";
        // line 150
        echo "{{mtmDebug.selectedEvent.metTrigger.name }}";
        echo "</td>
                                    <td>";
        // line 151
        echo "{{mtmDebug.selectedEvent.metTrigger.type }}";
        echo "</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div ng-show=\"mtmDebug.contentTab == 'dataLayer'\">
                        <h3>Pushed data by this event</h3>

                        <table class=\"entityTable\">
                            <tbody>
                            <tr>
                                <td>";
        // line 162
        echo "{{ mtmDebug.selectedEvent.eventData|json }}";
        echo "</td>
                            </tr>
                            </tbody>
                        </table>

                        <h3>Content after this event</h3>

                        <table class=\"entityTable\">
                            <tbody>
                            <tr >
                                <td>";
        // line 172
        echo "{{ mtmDebug.selectedEvent.container.dataLayer|json }}";
        echo "</td>
                            </tr>
                            </tbody>
                        </table>

                    </div>
                    <div ng-show=\"mtmDebug.contentTab == 'variables'\">
                        <table class=\"entityTable\">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Type</th>
                                    <th>Value</th>
                                </tr>
                            </thead>
                            <tbody>
                            <tr ng-if=\"!mtmDebug.selectedEvent.container.variables.length\">
                                <td colspan=\"3\">No variables</td>
                            </tr>
                            <tr ng-repeat=\"variable in mtmDebug.selectedEvent.container.variables\">
                                <td>";
        // line 192
        echo "{{variable.name }}";
        echo "</td>
                                <td>";
        // line 193
        echo "{{variable.type }}";
        echo "</td>
                                <td>";
        // line 194
        echo "{{variable.value|json }}";
        echo "</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div ng-show=\"mtmDebug.contentTab == 'logs'\">
                        <table class=\"entityTable\">
                            <thead>
                            <tr>
                                <th>Time</th>
                                <th>Message</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr ng-repeat=\"log in mtmDebug.mtmLogs\">
                                <td>";
        // line 209
        echo "{{log.time }}";
        echo "</td>
                                <td><span ng-repeat=\"logMessage in log.messages\">";
        // line 210
        echo "{{ logMessage }}";
        echo "<br /></span></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    ";
        // line 219
        $this->loadTemplate("_jsCssIncludes.twig", "@TagManager/debug.twig", 219)->display($context);
        // line 220
        echo "</body>
</html>
";
    }

    public function getTemplateName()
    {
        return "@TagManager/debug.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  320 => 220,  318 => 219,  306 => 210,  302 => 209,  284 => 194,  280 => 193,  276 => 192,  253 => 172,  240 => 162,  226 => 151,  222 => 150,  199 => 130,  195 => 129,  174 => 111,  170 => 110,  166 => 109,  162 => 108,  137 => 90,  125 => 83,  119 => 82,  96 => 62,  36 => 5,  30 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html>
<html id=\"ng-app\" ng-app=\"piwikApp\">
<head>
    <title>Debug Matomo Tag Manager</title>
    <base href=\"{{ piwikUrl|e('html_attr') }}\" >
    <meta charset=\"utf-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=EDGE,chrome=1\"/>
    <meta name=\"viewport\" content=\"initial-scale=1.0\"/>
    <meta name=\"robots\" content=\"noindex,nofollow\">
    <style>
        .nav-wrapper .brand-logo {
            position: relative;
            pointer-events: none;
        }
        .home h2, .home h3 {
            margin-top: 0;
        }
    </style>
    <script>
        var piwik_translations = {};
    </script>
    <script type=\"text/javascript\">

        window.angularDigest = function () {
            if ('undefined' === typeof jQuery) {
                return;
            }
            jQuery(function () {
                var \$timeout = piwikHelper.getAngularDependency('\$timeout');
                if (\$timeout) {
                    \$timeout(function () {});
                }
            });
        };
        window.mtmEvents = {
            events: [],
            push: function (event) {
                if (event) {
                    event.index0 = this.events.length;
                    event.index = this.events.length + 1;
                }
                this.events.push(event);

                window.angularDigest();
            }
        };
        window.mtmLogs = {
            logs: [],
            push: function (log) {
                this.logs.push(log);

                window.angularDigest();
            }
        };
    </script>
</head>
<body ng-app=\"app\" >
    <div id=\"root\" ng-controller=\"MtmDebugController as mtmDebug\">
        <nav>
            <div class=\"nav-wrapper\">
                <ul>
                    <li>{% include \"@CoreHome/_logo.twig\" %}</li>
                    <li ng-class=\"{'active': (mtmDebug.contentTab == 'tags' || !mtmDebug.contentTab)}\"><a ng-click=\"mtmDebug.contentTab = 'tags'\">Tags</a></li>
                    <li ng-class=\"{'active': (mtmDebug.contentTab == 'triggers')}\"><a ng-click=\"mtmDebug.contentTab = 'triggers'\">Triggers</a></li>
                    <li ng-class=\"{'active': (mtmDebug.contentTab == 'variables')}\"><a ng-click=\"mtmDebug.contentTab = 'variables'\">Variables</a></li>
                    <li ng-class=\"{'active': (mtmDebug.contentTab == 'dataLayer')}\"><a ng-click=\"mtmDebug.contentTab = 'dataLayer'\">Data Layer</a></li>
                    <li ng-class=\"{'active': (mtmDebug.contentTab == 'logs')}\"><a ng-click=\"mtmDebug.contentTab = 'logs'\">Logs</a></li>
                </ul>
            </div>
        </nav>

        <div class=\"page\" style=\"clear:both;\">
            <div id=\"secondNavBar\" class=\"Menu--dashboard z-depth-1\"
                 ng-show=\"mtmDebug.contentTab != 'logs'\">
                <ul class=\"navbar\" role=\"menu\" style=\"padding: 0;\">
                    <li class=\"menuTab\" role=\"menuitem\">
                        <span class=\"item\" style=\"font-weight: normal;\"> Events</span>
                    </li>
                    <li ng-show=\"(mtmDebug.mtmEvents|length) == 0\">No event executed</li>
                    <li class=\"menuTab\" role=\"menuitem\" ng-class=\"{'active': eventIndex === mtmDebug.selectedEventIndex}\"
                    ng-repeat=\"event in mtmDebug.mtmEvents | orderBy:'\$index':true\">
                        <a class=\"item ng-binding\"  ng-click=\"mtmDebug.selectEvent(event.index0)\" title=\"Time: {{ '{{ event.time }}'|raw }}. Trigger: '{{ '{{ event.metTrigger.name }}'|raw }}'\">
                            {{ '{{ event.index }}'|raw }}: {{ '{{ event.name }}'|raw }}
                        </a>
                    </li>
                </ul>
            </div>
            <div class=\"pageWrap\">
                <div class=\"home\" id=\"content\" ng-cloak>
                    <h2 ng-show=\"mtmDebug.contentTab != 'logs'\">Event {{ '{{ mtmDebug.selectedEventIndex + 1 }}'|raw }}: {{ '{{ mtmDebug.selectedEvent.name }}'|raw }} ({{ '{{ mtmDebug.selectedEvent.container.id }} - {{ mtmDebug.selectedEvent.container.versionName || \"Draft version\" }}'|raw }})</h2>

                    <div ng-show=\"mtmDebug.contentTab == 'tags' || !mtmDebug.contentTab\">
                        <h3>Fired Tags</h3>
                        <table class=\"entityTable\">
                            <thead>
                            <tr>
                                <th>Action</th>
                                <th>Name</th>
                                <th>Type</th>
                                <th>Fired count</th>
                            </tr>
                            </thead>
                            <tbody>
                                <tr ng-show=\"(mtmDebug.selectedEvent.tags|length) == 0\">
                                    <td colspan=\"4\">No tags</td>
                                </tr>
                                <tr ng-repeat=\"tag in mtmDebug.selectedEvent.tags\">
                                    <td>{{ '{{tag.action }}'|raw }}</td>
                                    <td>{{ '{{tag.name }}'|raw }}</td>
                                    <td>{{ '{{tag.type }}'|raw }}</td>
                                    <td>{{ '{{tag.numExecuted }}'|raw }}</td>
                                </tr>
                            </tbody>
                        </table>

                        <h3 style=\"margin-top:30px;\">Not Yet Fired Tags</h3>
                        <table class=\"entityTable\">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Type</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr ng-show=\"(mtmDebug.selectedEvent.notFiredTags|length) == 0\">
                                <td colspan=\"4\">No tags</td>
                            </tr>
                            <tr ng-repeat=\"tag in mtmDebug.selectedEvent.notFiredTags\">
                                <td>{{ '{{tag.name }}'|raw }}</td>
                                <td>{{ '{{tag.type }}'|raw }}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div ng-show=\"mtmDebug.contentTab == 'triggers'\">
                        <h3>Triggers</h3>

                        <table class=\"entityTable\">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Type</th>
                            </tr>
                            </thead>
                            <tbody>
                                <tr ng-show=\"!mtmDebug.selectedEvent.metTrigger\">
                                    <td colspan=\"4\">No trigger</td>
                                </tr>
                                <tr ng-show=\"mtmDebug.selectedEvent.metTrigger\">
                                    <td>{{ '{{mtmDebug.selectedEvent.metTrigger.name }}'|raw }}</td>
                                    <td>{{ '{{mtmDebug.selectedEvent.metTrigger.type }}'|raw }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div ng-show=\"mtmDebug.contentTab == 'dataLayer'\">
                        <h3>Pushed data by this event</h3>

                        <table class=\"entityTable\">
                            <tbody>
                            <tr>
                                <td>{{ '{{ mtmDebug.selectedEvent.eventData|json }}'|raw }}</td>
                            </tr>
                            </tbody>
                        </table>

                        <h3>Content after this event</h3>

                        <table class=\"entityTable\">
                            <tbody>
                            <tr >
                                <td>{{ '{{ mtmDebug.selectedEvent.container.dataLayer|json }}'|raw }}</td>
                            </tr>
                            </tbody>
                        </table>

                    </div>
                    <div ng-show=\"mtmDebug.contentTab == 'variables'\">
                        <table class=\"entityTable\">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Type</th>
                                    <th>Value</th>
                                </tr>
                            </thead>
                            <tbody>
                            <tr ng-if=\"!mtmDebug.selectedEvent.container.variables.length\">
                                <td colspan=\"3\">No variables</td>
                            </tr>
                            <tr ng-repeat=\"variable in mtmDebug.selectedEvent.container.variables\">
                                <td>{{ '{{variable.name }}'|raw }}</td>
                                <td>{{ '{{variable.type }}'|raw }}</td>
                                <td>{{ '{{variable.value|json }}'|raw }}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div ng-show=\"mtmDebug.contentTab == 'logs'\">
                        <table class=\"entityTable\">
                            <thead>
                            <tr>
                                <th>Time</th>
                                <th>Message</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr ng-repeat=\"log in mtmDebug.mtmLogs\">
                                <td>{{ '{{log.time }}'|raw }}</td>
                                <td><span ng-repeat=\"logMessage in log.messages\">{{ '{{ logMessage }}'|raw }}<br /></span></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {% include \"_jsCssIncludes.twig\" %}
</body>
</html>
", "@TagManager/debug.twig", "/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/templates/debug.twig");
    }
}
