<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @Widgetize/iframe.twig */
class __TwigTemplate_76ebbd81d3cd45ee0b7b75a9924e6e673592fe0eb5587ae66759fc9a8463f1b7 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<!DOCTYPE html>
<html id=\"ng-app\" ng-app=\"piwikApp\">
    <head>
        <meta charset=\"utf-8\">
        <meta name=\"robots\" content=\"noindex,nofollow\">
        <meta name=\"google\" content=\"notranslate\">
        ";
        // line 7
        $this->loadTemplate("_jsGlobalVariables.twig", "@Widgetize/iframe.twig", 7)->display($context);
        // line 8
        echo "        <!--[if lt IE 9]>
        <script language=\"javascript\" type=\"text/javascript\" src=\"libs/jqplot/excanvas.min.js\"></script>
        <![endif]-->
        ";
        // line 11
        $this->loadTemplate("_jsCssIncludes.twig", "@Widgetize/iframe.twig", 11)->display($context);
        // line 12
        echo "    </head>
    <body ng-app=\"app\" class=\"widgetized\">
        <div piwik-popover-handler></div>
        <div class=\"widget\">
            ";
        // line 16
        echo ($context["content"] ?? $this->getContext($context, "content"));
        echo "
        </div>
        <div id=\"pageFooter\">
            ";
        // line 19
        echo call_user_func_array($this->env->getFunction('postEvent')->getCallable(), ["Template.pageFooter"]);
        echo "
        </div>
    </body>
</html>
";
    }

    public function getTemplateName()
    {
        return "@Widgetize/iframe.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  59 => 19,  53 => 16,  47 => 12,  45 => 11,  40 => 8,  38 => 7,  30 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html>
<html id=\"ng-app\" ng-app=\"piwikApp\">
    <head>
        <meta charset=\"utf-8\">
        <meta name=\"robots\" content=\"noindex,nofollow\">
        <meta name=\"google\" content=\"notranslate\">
        {% include \"_jsGlobalVariables.twig\" %}
        <!--[if lt IE 9]>
        <script language=\"javascript\" type=\"text/javascript\" src=\"libs/jqplot/excanvas.min.js\"></script>
        <![endif]-->
        {% include \"_jsCssIncludes.twig\" %}
    </head>
    <body ng-app=\"app\" class=\"widgetized\">
        <div piwik-popover-handler></div>
        <div class=\"widget\">
            {{ content|raw }}
        </div>
        <div id=\"pageFooter\">
            {{ postEvent('Template.pageFooter') }}
        </div>
    </body>
</html>
", "@Widgetize/iframe.twig", "/home/httpd/electroluxodkuchni.pl/matomo/plugins/Widgetize/templates/iframe.twig");
    }
}
