<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @UserCountry/_updaterManage.twig */
class __TwigTemplate_9a8002cc32b30ce9fdab5e23b126fdd30a4bb2251c9b0416f7990049fe8e835f extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<div ng-show=\"locationUpdater.geoipDatabaseInstalled\" id=\"geoipdb-update-info\">
    <p>
\t\t";
        // line 3
        echo call_user_func_array($this->env->getFilter('translate')->getCallable(), ["UserCountry_GeoIPUpdaterInstructions", "<a href=\"http://www.maxmind.com/en/download_files?rId=piwik\" _target=\"blank\">", "</a>", "<a href=\"http://www.maxmind.com/?rId=piwik\">", "</a>"]);
        // line 4
        echo "
        <br/><br/>
\t\t";
        // line 6
        echo call_user_func_array($this->env->getFilter('translate')->getCallable(), ["UserCountry_GeoLiteCityLink", (("<a href='" . ($context["geoLiteUrl"] ?? $this->getContext($context, "geoLiteUrl"))) . "'>"), ($context["geoLiteUrl"] ?? $this->getContext($context, "geoLiteUrl")), "</a>"]);
        echo "

\t\t<span ng-show=\"locationUpdater.geoipDatabaseInstalled\">
\t\t\t<br/><br/>";
        // line 9
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["UserCountry_GeoIPUpdaterIntro"]), "html", null, true);
        echo ":
\t\t</span>
\t</p>

\t<div piwik-field uicontrol=\"text\" name=\"geoip-location-db\"
\t\t ng-model=\"locationUpdater.locationDbUrl\"
\t\t introduction=\"";
        // line 15
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["UserCountry_LocationDatabase"]), "html_attr");
        echo "\"
\t\t data-title=\"";
        // line 16
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Actions_ColumnDownloadURL"]), "html_attr");
        echo "\"
\t\t value=\"";
        // line 17
        echo \Piwik\piwik_escape_filter($this->env, ($context["geoIPLocUrl"] ?? $this->getContext($context, "geoIPLocUrl")), "html", null, true);
        echo "\"
\t\t inline-help=\"";
        // line 18
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["UserCountry_LocationDatabaseHint"]), "html_attr");
        echo "\">
\t</div>

\t<div piwik-field uicontrol=\"text\" name=\"geoip-isp-db\"
\t\t ng-model=\"locationUpdater.ispDbUrl\"
\t\t introduction=\"";
        // line 23
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["UserCountry_ISPDatabase"]), "html_attr");
        echo "\"
\t\t data-title=\"";
        // line 24
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Actions_ColumnDownloadURL"]), "html_attr");
        echo "\"
\t\t value=\"";
        // line 25
        echo \Piwik\piwik_escape_filter($this->env, ($context["geoIPIspUrl"] ?? $this->getContext($context, "geoIPIspUrl")), "html", null, true);
        echo "\">
\t</div>

\t";
        // line 28
        if ((isset($context["geoIPOrgUrl"]) || array_key_exists("geoIPOrgUrl", $context))) {
            // line 29
            echo "\t<div piwik-field uicontrol=\"text\" name=\"geoip-org-db\"
\t\t ng-model=\"locationUpdater.orgDbUrl\"
\t\t introduction=\"";
            // line 31
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["UserCountry_OrgDatabase"]), "html_attr");
            echo "\"
\t\t data-title=\"";
            // line 32
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Actions_ColumnDownloadURL"]), "html_attr");
            echo "\"
\t\t value=\"";
            // line 33
            echo \Piwik\piwik_escape_filter($this->env, ($context["geoIPOrgUrl"] ?? $this->getContext($context, "geoIPOrgUrl")), "html", null, true);
            echo "\">
\t</div>
\t";
        }
        // line 36
        echo "
\t<div id=\"locationProviderUpdatePeriodInlineHelp\" class=\"inline-help-node\">
\t\t";
        // line 38
        if (((isset($context["lastTimeUpdaterRun"]) || array_key_exists("lastTimeUpdaterRun", $context)) &&  !twig_test_empty(($context["lastTimeUpdaterRun"] ?? $this->getContext($context, "lastTimeUpdaterRun"))))) {
            // line 39
            echo "\t\t\t";
            echo call_user_func_array($this->env->getFilter('translate')->getCallable(), ["UserCountry_UpdaterWasLastRun", ($context["lastTimeUpdaterRun"] ?? $this->getContext($context, "lastTimeUpdaterRun"))]);
            echo "
\t\t";
        } else {
            // line 41
            echo "\t\t\t";
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["UserCountry_UpdaterHasNotBeenRun"]), "html", null, true);
            echo "
\t\t";
        }
        // line 43
        echo "\t\t<br/><br/>
\t\t<div id=\"geoip-updater-next-run-time\">
\t\t\t";
        // line 45
        $this->loadTemplate("@UserCountry/_updaterNextRunTime.twig", "@UserCountry/_updaterManage.twig", 45)->display($context);
        // line 46
        echo "\t\t</div>
\t</div>

\t<div piwik-field uicontrol=\"radio\" name=\"geoip-update-period\"
\t\t ng-model=\"locationUpdater.updatePeriod\"
\t\t introduction=\"";
        // line 51
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["UserCountry_DownloadNewDatabasesEvery"]), "html_attr");
        echo "\"
\t\t value=\"";
        // line 52
        echo \Piwik\piwik_escape_filter($this->env, ($context["geoIPUpdatePeriod"] ?? $this->getContext($context, "geoIPUpdatePeriod")), "html", null, true);
        echo "\"
\t\t options=\"";
        // line 53
        echo \Piwik\piwik_escape_filter($this->env, twig_jsonencode_filter(($context["updatePeriodOptions"] ?? $this->getContext($context, "updatePeriodOptions"))), "html", null, true);
        echo "\"
\t\t inline-help=\"#locationProviderUpdatePeriodInlineHelp\">
\t</div>

\t<input type=\"button\"
\t\t   class=\"btn\"
\t\t   ng-click=\"locationUpdater.saveGeoIpLinks()\"
\t\t   ng-value=\"locationUpdater.buttonUpdateSaveText\"/>

\t<div>
\t\t<div id=\"done-updating-updater\"></div>
\t\t<div id=\"geoipdb-update-info-error\"></div>
\t\t<div piwik-progressbar
\t\t\t progress=\"locationUpdater.progressUpdateDownload\"
\t\t\t label=\"locationUpdater.progressUpdateLabel\"
\t\t\t ng-show=\"locationUpdater.isUpdatingGeoIpDatabase\"></div>
\t</div>
</div>
";
    }

    public function getTemplateName()
    {
        return "@UserCountry/_updaterManage.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  148 => 53,  144 => 52,  140 => 51,  133 => 46,  131 => 45,  127 => 43,  121 => 41,  115 => 39,  113 => 38,  109 => 36,  103 => 33,  99 => 32,  95 => 31,  91 => 29,  89 => 28,  83 => 25,  79 => 24,  75 => 23,  67 => 18,  63 => 17,  59 => 16,  55 => 15,  46 => 9,  40 => 6,  36 => 4,  34 => 3,  30 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("<div ng-show=\"locationUpdater.geoipDatabaseInstalled\" id=\"geoipdb-update-info\">
    <p>
\t\t{{ 'UserCountry_GeoIPUpdaterInstructions'|translate('<a href=\"http://www.maxmind.com/en/download_files?rId=piwik\" _target=\"blank\">','</a>',
        '<a href=\"http://www.maxmind.com/?rId=piwik\">','</a>')|raw }}
        <br/><br/>
\t\t{{ 'UserCountry_GeoLiteCityLink'|translate(\"<a href='\"~geoLiteUrl~\"'>\",geoLiteUrl,'</a>')|raw }}

\t\t<span ng-show=\"locationUpdater.geoipDatabaseInstalled\">
\t\t\t<br/><br/>{{ 'UserCountry_GeoIPUpdaterIntro'|translate }}:
\t\t</span>
\t</p>

\t<div piwik-field uicontrol=\"text\" name=\"geoip-location-db\"
\t\t ng-model=\"locationUpdater.locationDbUrl\"
\t\t introduction=\"{{ 'UserCountry_LocationDatabase'|translate|e('html_attr') }}\"
\t\t data-title=\"{{ 'Actions_ColumnDownloadURL'|translate|e('html_attr') }}\"
\t\t value=\"{{ geoIPLocUrl }}\"
\t\t inline-help=\"{{ 'UserCountry_LocationDatabaseHint'|translate|e('html_attr') }}\">
\t</div>

\t<div piwik-field uicontrol=\"text\" name=\"geoip-isp-db\"
\t\t ng-model=\"locationUpdater.ispDbUrl\"
\t\t introduction=\"{{ 'UserCountry_ISPDatabase'|translate|e('html_attr') }}\"
\t\t data-title=\"{{ 'Actions_ColumnDownloadURL'|translate|e('html_attr') }}\"
\t\t value=\"{{ geoIPIspUrl }}\">
\t</div>

\t{% if geoIPOrgUrl is defined %}
\t<div piwik-field uicontrol=\"text\" name=\"geoip-org-db\"
\t\t ng-model=\"locationUpdater.orgDbUrl\"
\t\t introduction=\"{{ 'UserCountry_OrgDatabase'|translate|e('html_attr') }}\"
\t\t data-title=\"{{ 'Actions_ColumnDownloadURL'|translate|e('html_attr') }}\"
\t\t value=\"{{ geoIPOrgUrl }}\">
\t</div>
\t{% endif %}

\t<div id=\"locationProviderUpdatePeriodInlineHelp\" class=\"inline-help-node\">
\t\t{% if lastTimeUpdaterRun is defined and lastTimeUpdaterRun is not empty %}
\t\t\t{{ 'UserCountry_UpdaterWasLastRun'|translate(lastTimeUpdaterRun)|raw }}
\t\t{% else %}
\t\t\t{{ 'UserCountry_UpdaterHasNotBeenRun'|translate }}
\t\t{% endif %}
\t\t<br/><br/>
\t\t<div id=\"geoip-updater-next-run-time\">
\t\t\t{% include \"@UserCountry/_updaterNextRunTime.twig\" %}
\t\t</div>
\t</div>

\t<div piwik-field uicontrol=\"radio\" name=\"geoip-update-period\"
\t\t ng-model=\"locationUpdater.updatePeriod\"
\t\t introduction=\"{{ 'UserCountry_DownloadNewDatabasesEvery'|translate|e('html_attr') }}\"
\t\t value=\"{{ geoIPUpdatePeriod }}\"
\t\t options=\"{{ updatePeriodOptions|json_encode }}\"
\t\t inline-help=\"#locationProviderUpdatePeriodInlineHelp\">
\t</div>

\t<input type=\"button\"
\t\t   class=\"btn\"
\t\t   ng-click=\"locationUpdater.saveGeoIpLinks()\"
\t\t   ng-value=\"locationUpdater.buttonUpdateSaveText\"/>

\t<div>
\t\t<div id=\"done-updating-updater\"></div>
\t\t<div id=\"geoipdb-update-info-error\"></div>
\t\t<div piwik-progressbar
\t\t\t progress=\"locationUpdater.progressUpdateDownload\"
\t\t\t label=\"locationUpdater.progressUpdateLabel\"
\t\t\t ng-show=\"locationUpdater.isUpdatingGeoIpDatabase\"></div>
\t</div>
</div>
", "@UserCountry/_updaterManage.twig", "/home/httpd/electroluxodkuchni.pl/matomo/plugins/UserCountry/templates/_updaterManage.twig");
    }
}
