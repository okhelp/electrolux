<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @ProfessionalServices/promoBelowEvents.twig */
class __TwigTemplate_04081a43b93f88468191a6a7aa185f6d564ea44258397143e735f1518f986c38 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<p style=\"margin-top:3em\" class=\" alert-info alert\">Did you know?
    <br/>Using Events you can measure any user interaction and gain amazing insights into your audience. <a target=\"_blank\" rel=\"noreferrer noopener\" href=\"https://matomo.org/docs/event-tracking/\">Learn more</a>.
    <br/> To measure blocks of content such as image galleries, listings or ads: use <a target=\"_blank\" rel=\"noreferrer noopener\" href=\"https://developer.matomo.org/guides/content-tracking\">Content Tracking</a> and see exactly which content is viewed and clicked.
    ";
        // line 4
        if (($context["displayMediaAnalyticsAd"] ?? $this->getContext($context, "displayMediaAnalyticsAd"))) {
            // line 5
            echo "        <br/>When you publish videos or audios, <a target=\"_blank\" rel=\"noreferrer noopener\" href=\"https://matomo.org/recommends/media-analytics-website\">Media Analytics gives deep insights into your audience</a> and how they watch your videos or listens to your music.
    ";
        }
        // line 7
        echo "</p>
";
    }

    public function getTemplateName()
    {
        return "@ProfessionalServices/promoBelowEvents.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  41 => 7,  37 => 5,  35 => 4,  30 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("<p style=\"margin-top:3em\" class=\" alert-info alert\">Did you know?
    <br/>Using Events you can measure any user interaction and gain amazing insights into your audience. <a target=\"_blank\" rel=\"noreferrer noopener\" href=\"https://matomo.org/docs/event-tracking/\">Learn more</a>.
    <br/> To measure blocks of content such as image galleries, listings or ads: use <a target=\"_blank\" rel=\"noreferrer noopener\" href=\"https://developer.matomo.org/guides/content-tracking\">Content Tracking</a> and see exactly which content is viewed and clicked.
    {% if displayMediaAnalyticsAd %}
        <br/>When you publish videos or audios, <a target=\"_blank\" rel=\"noreferrer noopener\" href=\"https://matomo.org/recommends/media-analytics-website\">Media Analytics gives deep insights into your audience</a> and how they watch your videos or listens to your music.
    {% endif %}
</p>
", "@ProfessionalServices/promoBelowEvents.twig", "/home/httpd/electroluxodkuchni.pl/matomo/plugins/ProfessionalServices/templates/promoBelowEvents.twig");
    }
}
