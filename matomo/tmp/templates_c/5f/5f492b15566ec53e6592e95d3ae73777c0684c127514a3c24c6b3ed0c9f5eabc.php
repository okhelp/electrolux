<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @TagManager/trackingCode.twig */
class __TwigTemplate_5d33d2fb10eec14330cfa6126b2be545d1f9c1753117188e587da0ce4be3944b extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<div piwik-content-block anchor=\"tagmanager\" content-title=\"";
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["TagManager_TagManager"]), "html", null, true);
        echo "\">
    <p>";
        // line 2
        echo call_user_func_array($this->env->getFilter('translate')->getCallable(), ["TagManager_TagManagerTrackingInfo", (("<a href=\"" . call_user_func_array($this->env->getFunction('linkTo')->getCallable(), [["module" => "TagManager", "action" => "gettingStarted"]])) . "\">"), "</a>", (("<a href=\"" . call_user_func_array($this->env->getFunction('linkTo')->getCallable(), [["module" => "TagManager", "action" => "manageContainers"]])) . "\">"), "</a>"]);
        echo "</p>
    <div matomo-tagmanager-tracking-code></div>
</div>";
    }

    public function getTemplateName()
    {
        return "@TagManager/trackingCode.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  35 => 2,  30 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("<div piwik-content-block anchor=\"tagmanager\" content-title=\"{{ 'TagManager_TagManager'|translate }}\">
    <p>{{ 'TagManager_TagManagerTrackingInfo'|translate('<a href=\"' ~ linkTo({module: 'TagManager', action: 'gettingStarted'})~'\">', '</a>', '<a href=\"' ~ linkTo({module: 'TagManager', action: 'manageContainers'})~'\">', '</a>')|raw }}</p>
    <div matomo-tagmanager-tracking-code></div>
</div>", "@TagManager/trackingCode.twig", "/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/templates/trackingCode.twig");
    }
}
