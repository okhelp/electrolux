<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @CoreHome/widgetContainer.twig */
class __TwigTemplate_fdda86e6b06463812c23599ba25d49410d17811d4500ed40c89864024c169540 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<div>
    <div piwik-widget
         containerid=\"";
        // line 3
        echo \Piwik\piwik_escape_filter($this->env, ($context["containerId"] ?? $this->getContext($context, "containerId")), "html_attr");
        echo "\"
         widgetized=\"";
        // line 4
        if (($context["isWidgetized"] ?? $this->getContext($context, "isWidgetized"))) {
            echo "true";
        } else {
            echo "false";
        }
        echo "\"></div>

    <script type=\"text/javascript\">
        \$(function () {

            var piwikWidget = \$('[piwik-widget][containerid=";
        // line 9
        echo \Piwik\piwik_escape_filter($this->env, \Piwik\piwik_escape_filter($this->env, ($context["containerId"] ?? $this->getContext($context, "containerId")), "js"), "html", null, true);
        echo "]');
            var isExportedAsWidget = \$('body > .widget').length;

            if (!isExportedAsWidget) {
                angular.element(document).injector().invoke(function(\$compile) {
                    var scope = angular.element(piwikWidget).scope();
                    \$compile(piwikWidget)(scope.\$new());
                });
            }

        });
    </script>
</div>";
    }

    public function getTemplateName()
    {
        return "@CoreHome/widgetContainer.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  50 => 9,  38 => 4,  34 => 3,  30 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("<div>
    <div piwik-widget
         containerid=\"{{ containerId|e('html_attr') }}\"
         widgetized=\"{% if isWidgetized %}true{% else %}false{% endif %}\"></div>

    <script type=\"text/javascript\">
        \$(function () {

            var piwikWidget = \$('[piwik-widget][containerid={{ containerId|e('js') }}]');
            var isExportedAsWidget = \$('body > .widget').length;

            if (!isExportedAsWidget) {
                angular.element(document).injector().invoke(function(\$compile) {
                    var scope = angular.element(piwikWidget).scope();
                    \$compile(piwikWidget)(scope.\$new());
                });
            }

        });
    </script>
</div>", "@CoreHome/widgetContainer.twig", "/home/httpd/electroluxodkuchni.pl/matomo/plugins/CoreHome/templates/widgetContainer.twig");
    }
}
