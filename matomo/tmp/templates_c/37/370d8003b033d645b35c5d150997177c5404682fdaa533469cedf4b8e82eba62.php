<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @TagManager/manageVersions.twig */
class __TwigTemplate_48bb1918154c66c25b0e3fca031c98b2e4d6b3567a3793ef68011bcf20872b1d extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@TagManager/tagmanager.twig", "@TagManager/manageVersions.twig", 1);
        $this->blocks = [
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "@TagManager/tagmanager.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 3
        $context["title"] = call_user_func_array($this->env->getFilter('translate')->getCallable(), ["TagManager_Versions"]);
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_content($context, array $blocks = [])
    {
        // line 6
        echo "    <div piwik-version-manage id-container=\"";
        echo \Piwik\piwik_escape_filter($this->env, $this->getAttribute(($context["container"] ?? $this->getContext($context, "container")), "idcontainer", []), "html", null, true);
        echo "\">
    </div>
";
    }

    public function getTemplateName()
    {
        return "@TagManager/manageVersions.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  45 => 6,  42 => 5,  38 => 1,  36 => 3,  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% extends '@TagManager/tagmanager.twig' %}

{% set title = 'TagManager_Versions'|translate %}

{% block content %}
    <div piwik-version-manage id-container=\"{{ container.idcontainer }}\">
    </div>
{% endblock %}", "@TagManager/manageVersions.twig", "/home/httpd/electroluxodkuchni.pl/matomo/plugins/TagManager/templates/manageVersions.twig");
    }
}
