<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @ScheduledReports/index.twig */
class __TwigTemplate_330846a69f944bae67460367bd66b26c659fdff5c3dbd00c0038812ddceb7bc3 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("admin.twig", "@ScheduledReports/index.twig", 1);
        $this->blocks = [
            'topcontrols' => [$this, 'block_topcontrols'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "admin.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 3
        ob_start();
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["ScheduledReports_PersonalEmailReports"]), "html", null, true);
        $context["title"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_topcontrols($context, array $blocks = [])
    {
        // line 6
        echo "    ";
        $this->loadTemplate("@CoreHome/_siteSelectHeader.twig", "@ScheduledReports/index.twig", 6)->display($context);
        // line 7
        echo "    ";
        $this->loadTemplate("@CoreHome/_periodSelect.twig", "@ScheduledReports/index.twig", 7)->display($context);
    }

    // line 10
    public function block_content($context, array $blocks = [])
    {
        // line 11
        echo "
<div class=\"emailReports\" piwik-manage-scheduled-report>

    <span id=\"reportSentSuccess\"></span>
    <span id=\"reportUpdatedSuccess\"></span>

    <div>
        ";
        // line 18
        $context["ajax"] = $this->loadTemplate("ajaxMacros.twig", "@ScheduledReports/index.twig", 18);
        // line 19
        echo "        ";
        echo $context["ajax"]->geterrorDiv();
        echo "
        ";
        // line 20
        echo $context["ajax"]->getloadingDiv();
        echo "
        ";
        // line 21
        $this->loadTemplate("@ScheduledReports/_listReports.twig", "@ScheduledReports/index.twig", 21)->display($context);
        // line 22
        echo "        ";
        $this->loadTemplate("@ScheduledReports/_addReport.twig", "@ScheduledReports/index.twig", 22)->display($context);
        // line 23
        echo "        <a id='bottom'></a>
    </div>
</div>

<div class=\"ui-confirm\" id=\"confirm\">
    <h2>";
        // line 28
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["ScheduledReports_AreYouSureDeleteReport"]), "html", null, true);
        echo "</h2>
    <input role=\"yes\" type=\"button\" value=\"";
        // line 29
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_Yes"]), "html", null, true);
        echo "\"/>
    <input role=\"no\" type=\"button\" value=\"";
        // line 30
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_No"]), "html", null, true);
        echo "\"/>
</div>

<script type=\"text/javascript\">
    var ReportPlugin = {};
    ReportPlugin.defaultPeriod = '";
        // line 35
        echo \Piwik\piwik_escape_filter($this->env, ($context["defaultPeriod"] ?? $this->getContext($context, "defaultPeriod")), "html", null, true);
        echo "';
    ReportPlugin.defaultHour = '";
        // line 36
        echo \Piwik\piwik_escape_filter($this->env, ($context["defaultHour"] ?? $this->getContext($context, "defaultHour")), "html", null, true);
        echo "';
    ReportPlugin.defaultReportType = '";
        // line 37
        echo \Piwik\piwik_escape_filter($this->env, ($context["defaultReportType"] ?? $this->getContext($context, "defaultReportType")), "html", null, true);
        echo "';
    ReportPlugin.defaultReportFormat = '";
        // line 38
        echo \Piwik\piwik_escape_filter($this->env, ($context["defaultReportFormat"] ?? $this->getContext($context, "defaultReportFormat")), "html", null, true);
        echo "';
    ReportPlugin.reportList = ";
        // line 39
        echo ($context["reportsJSON"] ?? $this->getContext($context, "reportsJSON"));
        echo ";
    ReportPlugin.createReportString = \"";
        // line 40
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["ScheduledReports_CreateReport"]), "html", null, true);
        echo "\";
    ReportPlugin.updateReportString = \"";
        // line 41
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["ScheduledReports_UpdateReport"]), "html", null, true);
        echo "\";
    ReportPlugin.defaultEvolutionPeriodN = ";
        // line 42
        echo twig_jsonencode_filter(($context["defaultEvolutionPeriodN"] ?? $this->getContext($context, "defaultEvolutionPeriodN")));
        echo ";
    ReportPlugin.periodTranslations = ";
        // line 43
        echo twig_jsonencode_filter(($context["periodTranslations"] ?? $this->getContext($context, "periodTranslations")));
        echo ";
</script>
<style type=\"text/css\">
    .reportCategory {
        font-weight: bold;
        margin-bottom: 5px;
    }

    .entityAddContainer {
        position:relative;
    }

    .emailReports .top_controls {
        padding-bottom: 18px;
    }

</style>
";
    }

    public function getTemplateName()
    {
        return "@ScheduledReports/index.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  139 => 43,  135 => 42,  131 => 41,  127 => 40,  123 => 39,  119 => 38,  115 => 37,  111 => 36,  107 => 35,  99 => 30,  95 => 29,  91 => 28,  84 => 23,  81 => 22,  79 => 21,  75 => 20,  70 => 19,  68 => 18,  59 => 11,  56 => 10,  51 => 7,  48 => 6,  45 => 5,  41 => 1,  37 => 3,  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'admin.twig' %}

{% set title %}{{ 'ScheduledReports_PersonalEmailReports'|translate }}{% endset %}

{% block topcontrols %}
    {% include \"@CoreHome/_siteSelectHeader.twig\" %}
    {% include \"@CoreHome/_periodSelect.twig\" %}
{% endblock %}

{% block content %}

<div class=\"emailReports\" piwik-manage-scheduled-report>

    <span id=\"reportSentSuccess\"></span>
    <span id=\"reportUpdatedSuccess\"></span>

    <div>
        {% import 'ajaxMacros.twig' as ajax %}
        {{ ajax.errorDiv() }}
        {{ ajax.loadingDiv() }}
        {% include \"@ScheduledReports/_listReports.twig\" %}
        {% include \"@ScheduledReports/_addReport.twig\" %}
        <a id='bottom'></a>
    </div>
</div>

<div class=\"ui-confirm\" id=\"confirm\">
    <h2>{{ 'ScheduledReports_AreYouSureDeleteReport'|translate }}</h2>
    <input role=\"yes\" type=\"button\" value=\"{{ 'General_Yes'|translate }}\"/>
    <input role=\"no\" type=\"button\" value=\"{{ 'General_No'|translate }}\"/>
</div>

<script type=\"text/javascript\">
    var ReportPlugin = {};
    ReportPlugin.defaultPeriod = '{{ defaultPeriod }}';
    ReportPlugin.defaultHour = '{{ defaultHour }}';
    ReportPlugin.defaultReportType = '{{ defaultReportType }}';
    ReportPlugin.defaultReportFormat = '{{ defaultReportFormat }}';
    ReportPlugin.reportList = {{ reportsJSON | raw }};
    ReportPlugin.createReportString = \"{{ 'ScheduledReports_CreateReport'|translate }}\";
    ReportPlugin.updateReportString = \"{{ 'ScheduledReports_UpdateReport'|translate }}\";
    ReportPlugin.defaultEvolutionPeriodN = {{ defaultEvolutionPeriodN|json_encode|raw }};
    ReportPlugin.periodTranslations = {{ periodTranslations|json_encode|raw }};
</script>
<style type=\"text/css\">
    .reportCategory {
        font-weight: bold;
        margin-bottom: 5px;
    }

    .entityAddContainer {
        position:relative;
    }

    .emailReports .top_controls {
        padding-bottom: 18px;
    }

</style>
{% endblock %}
", "@ScheduledReports/index.twig", "/home/httpd/electroluxodkuchni.pl/matomo/plugins/ScheduledReports/templates/index.twig");
    }
}
