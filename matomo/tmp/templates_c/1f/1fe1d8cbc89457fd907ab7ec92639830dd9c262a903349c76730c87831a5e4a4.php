<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @Events/_actionEvent.twig */
class __TwigTemplate_91825b66fc2c7deeb7f284f5cb72ad272c8b174d38752844b0aa099dac76ee7f extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<li class=\"action\" title=\"";
        echo call_user_func_array($this->env->getFunction('postEvent')->getCallable(), ["Live.renderActionTooltip", ($context["action"] ?? $this->getContext($context, "action")), ($context["visitInfo"] ?? $this->getContext($context, "visitInfo"))]);
        echo "\">
    <div>
        ";
        // line 3
        if ( !twig_test_empty((($this->getAttribute(($context["action"] ?? null), "pageTitle", [], "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["action"] ?? null), "pageTitle", []), false)) : (false)))) {
            // line 4
            echo "            <span class=\"truncated-text-line\">";
            echo call_user_func_array($this->env->getFilter('rawSafeDecoded')->getCallable(), [$this->getAttribute(($context["action"] ?? $this->getContext($context, "action")), "pageTitle", [])]);
            echo "</span>
        ";
        }
        // line 6
        echo "        <img src='plugins/Morpheus/images/event.svg' title='";
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Events_Event"]), "html", null, true);
        echo "' class=\"action-list-action-icon event\">
        <span class=\"truncated-text-line event\">";
        // line 7
        echo call_user_func_array($this->env->getFilter('rawSafeDecoded')->getCallable(), [$this->getAttribute(($context["action"] ?? $this->getContext($context, "action")), "eventCategory", [])]);
        echo "
            - ";
        // line 8
        echo call_user_func_array($this->env->getFilter('rawSafeDecoded')->getCallable(), [$this->getAttribute(($context["action"] ?? $this->getContext($context, "action")), "eventAction", [])]);
        echo " ";
        if ($this->getAttribute(($context["action"] ?? null), "eventName", [], "any", true, true)) {
            echo "- ";
            echo call_user_func_array($this->env->getFilter('rawSafeDecoded')->getCallable(), [$this->getAttribute(($context["action"] ?? $this->getContext($context, "action")), "eventName", [])]);
        }
        echo " ";
        if ($this->getAttribute(($context["action"] ?? null), "eventValue", [], "any", true, true)) {
            echo "[";
            echo \Piwik\piwik_escape_filter($this->env, $this->getAttribute(($context["action"] ?? $this->getContext($context, "action")), "eventValue", []), "html", null, true);
            echo "]";
        }
        echo "</span>
        ";
        // line 9
        if ( !twig_test_empty($this->getAttribute(($context["action"] ?? $this->getContext($context, "action")), "url", []))) {
            // line 10
            echo "            ";
            if (((($this->getAttribute(($context["previousAction"] ?? null), "url", [], "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["previousAction"] ?? null), "url", []), false)) : (false)) == $this->getAttribute(($context["action"] ?? $this->getContext($context, "action")), "url", []))) {
                // line 11
                echo "                ";
                // line 12
                echo "            ";
            } else {
                // line 13
                echo "                ";
                if ((((is_string($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 = twig_lower_filter($this->env, twig_trim_filter($this->getAttribute(($context["action"] ?? $this->getContext($context, "action")), "url", [])))) && is_string($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 = "javascript:") && ('' === $__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 || 0 === strpos($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4, $__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144))) || (is_string($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b = twig_lower_filter($this->env, twig_trim_filter($this->getAttribute(                // line 14
($context["action"] ?? $this->getContext($context, "action")), "url", [])))) && is_string($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 = "vbscript:") && ('' === $__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 || 0 === strpos($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b, $__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002)))) || (is_string($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4 = twig_lower_filter($this->env, twig_trim_filter($this->getAttribute(                // line 15
($context["action"] ?? $this->getContext($context, "action")), "url", [])))) && is_string($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666 = "data:") && ('' === $__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666 || 0 === strpos($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4, $__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666))))) {
                    // line 16
                    echo "                    ";
                    echo \Piwik\piwik_escape_filter($this->env, $this->getAttribute(($context["action"] ?? $this->getContext($context, "action")), "url", []), "html", null, true);
                    echo "
                ";
                } else {
                    // line 18
                    echo "                    <a href=\"";
                    echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('safelink')->getCallable(), [$this->getAttribute(($context["action"] ?? $this->getContext($context, "action")), "url", [])]), "html_attr");
                    echo "\" rel=\"noreferrer noopener\" target=\"_blank\" class=\"truncated-text-line\">
                        ";
                    // line 19
                    echo \Piwik\piwik_escape_filter($this->env, twig_replace_filter($this->getAttribute(($context["action"] ?? $this->getContext($context, "action")), "url", []), ["http://" => "", "https://" => ""]), "html", null, true);
                    echo "
                    </a>
                ";
                }
                // line 22
                echo "            ";
            }
            // line 23
            echo "        ";
        }
        // line 24
        echo "    </div>
</li>
";
    }

    public function getTemplateName()
    {
        return "@Events/_actionEvent.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  106 => 24,  103 => 23,  100 => 22,  94 => 19,  89 => 18,  83 => 16,  81 => 15,  80 => 14,  78 => 13,  75 => 12,  73 => 11,  70 => 10,  68 => 9,  53 => 8,  49 => 7,  44 => 6,  38 => 4,  36 => 3,  30 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("<li class=\"action\" title=\"{{ postEvent('Live.renderActionTooltip', action, visitInfo) }}\">
    <div>
        {% if action.pageTitle|default(false) is not empty %}
            <span class=\"truncated-text-line\">{{ action.pageTitle|rawSafeDecoded }}</span>
        {% endif %}
        <img src='plugins/Morpheus/images/event.svg' title='{{ 'Events_Event'|translate }}' class=\"action-list-action-icon event\">
        <span class=\"truncated-text-line event\">{{ action.eventCategory|rawSafeDecoded }}
            - {{ action.eventAction|rawSafeDecoded }} {% if action.eventName is defined %}- {{ action.eventName|rawSafeDecoded }}{% endif %} {% if action.eventValue is defined %}[{{ action.eventValue }}]{% endif %}</span>
        {% if action.url is not empty %}
            {% if previousAction.url|default(false) == action.url %}
                {# For events, do not show (url) if the Event URL is the same as the URL last displayed #}
            {% else %}
                {% if action.url|trim|lower starts with 'javascript:' or
                action.url|trim|lower starts with 'vbscript:' or
                action.url|trim|lower starts with 'data:' %}
                    {{ action.url }}
                {% else %}
                    <a href=\"{{ action.url|safelink|e('html_attr') }}\" rel=\"noreferrer noopener\" target=\"_blank\" class=\"truncated-text-line\">
                        {{ action.url|replace({'http://': '', 'https://': ''}) }}
                    </a>
                {% endif %}
            {% endif %}
        {% endif %}
    </div>
</li>
", "@Events/_actionEvent.twig", "/home/httpd/electroluxodkuchni.pl/matomo/plugins/Events/templates/_actionEvent.twig");
    }
}
