<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @CoreHome/getRowEvolutionPopover.twig */
class __TwigTemplate_001c308702189a97728b667d49a66c4815d4b98f98ee80ea47ece1a804fbb51c extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        $context["seriesColorCount"] = twig_constant("Piwik\\Plugins\\CoreVisualizations\\Visualizations\\JqplotGraph\\Evolution::SERIES_COLOR_COUNT");
        // line 2
        echo "<div class=\"rowevolution\">
    <div class=\"popover-title\">";
        // line 3
        echo ($context["popoverTitle"] ?? $this->getContext($context, "popoverTitle"));
        echo "</div>
    <div class=\"graph\">
        ";
        // line 5
        echo ($context["graph"] ?? $this->getContext($context, "graph"));
        echo "
    </div>
    <div class=\"metrics-container\">
        <h2>";
        // line 8
        echo ($context["availableMetricsText"] ?? $this->getContext($context, "availableMetricsText"));
        echo "</h2>

        <div class=\"alert alert-info\">
            ";
        // line 11
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["RowEvolution_Documentation"]), "html", null, true);
        echo "
        </div>
        <table class=\"metrics\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" data-thing=\"";
        // line 13
        echo \Piwik\piwik_escape_filter($this->env, ($context["seriesColorCount"] ?? $this->getContext($context, "seriesColorCount")), "html", null, true);
        echo "\">
            ";
        // line 14
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["metrics"] ?? $this->getContext($context, "metrics")));
        foreach ($context['_seq'] as $context["i"] => $context["metric"]) {
            // line 15
            echo "                <tr data-i=\"";
            echo \Piwik\piwik_escape_filter($this->env, $context["i"], "html", null, true);
            echo "\" ";
            if ((($this->getAttribute($context["metric"], "hide", [], "any", true, true)) ? (_twig_default_filter($this->getAttribute($context["metric"], "hide", []))) : (""))) {
                echo "style=\"display:none\"";
            }
            echo ">
                    <td class=\"sparkline\">
                        ";
            // line 17
            echo $this->getAttribute($context["metric"], "sparkline", []);
            echo "
                    </td>
                    <td class=\"text\">
                        <span class=\"evolution-graph-colors\" data-name=\"series";
            // line 20
            echo \Piwik\piwik_escape_filter($this->env, (($context["i"] % ($context["seriesColorCount"] ?? $this->getContext($context, "seriesColorCount"))) + 1), "html", null, true);
            echo "\">";
            // line 21
            echo $this->getAttribute($context["metric"], "label", []);
            // line 22
            echo "</span>
                        ";
            // line 23
            if ($this->getAttribute($context["metric"], "details", [])) {
                echo ":
                            <span class=\"details\" title=\"";
                // line 24
                echo \Piwik\piwik_escape_filter($this->env, $this->getAttribute($context["metric"], "minmax", []), "html", null, true);
                echo "\">";
                echo $this->getAttribute($context["metric"], "details", []);
                echo "</span>
                        ";
            }
            // line 26
            echo "                    </td>
                </tr>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['i'], $context['metric'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 29
        echo "        </table>
    </div>
    <div class=\"compare-container\">
        <h2>";
        // line 32
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["RowEvolution_CompareRows"]), "html", null, true);
        echo "</h2>

        <div class=\"alert alert-info\">
            ";
        // line 35
        echo call_user_func_array($this->env->getFilter('translate')->getCallable(), ["RowEvolution_CompareDocumentation"]);
        echo "
        </div>
        <a href=\"#\" class=\"rowevolution-startmulti\">» ";
        // line 37
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["RowEvolution_PickARow"]), "html", null, true);
        echo "</a>
    </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "@CoreHome/getRowEvolutionPopover.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  124 => 37,  119 => 35,  113 => 32,  108 => 29,  100 => 26,  93 => 24,  89 => 23,  86 => 22,  84 => 21,  81 => 20,  75 => 17,  65 => 15,  61 => 14,  57 => 13,  52 => 11,  46 => 8,  40 => 5,  35 => 3,  32 => 2,  30 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% set seriesColorCount = constant(\"Piwik\\\\Plugins\\\\CoreVisualizations\\\\Visualizations\\\\JqplotGraph\\\\Evolution::SERIES_COLOR_COUNT\") %}
<div class=\"rowevolution\">
    <div class=\"popover-title\">{{ popoverTitle | raw }}</div>
    <div class=\"graph\">
        {{ graph|raw }}
    </div>
    <div class=\"metrics-container\">
        <h2>{{ availableMetricsText|raw }}</h2>

        <div class=\"alert alert-info\">
            {{ 'RowEvolution_Documentation'|translate }}
        </div>
        <table class=\"metrics\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" data-thing=\"{{ seriesColorCount }}\">
            {% for i, metric in metrics %}
                <tr data-i=\"{{ i }}\" {% if metric.hide|default %}style=\"display:none\"{% endif %}>
                    <td class=\"sparkline\">
                        {{ metric.sparkline|raw }}
                    </td>
                    <td class=\"text\">
                        <span class=\"evolution-graph-colors\" data-name=\"series{{ (i % seriesColorCount) + 1 }}\">
                            {{- metric.label|raw -}}
                        </span>
                        {% if metric.details %}:
                            <span class=\"details\" title=\"{{ metric.minmax }}\">{{ metric.details|raw }}</span>
                        {% endif %}
                    </td>
                </tr>
            {% endfor %}
        </table>
    </div>
    <div class=\"compare-container\">
        <h2>{{ 'RowEvolution_CompareRows'|translate }}</h2>

        <div class=\"alert alert-info\">
            {{ 'RowEvolution_CompareDocumentation'|translate|raw }}
        </div>
        <a href=\"#\" class=\"rowevolution-startmulti\">» {{ 'RowEvolution_PickARow'|translate }}</a>
    </div>
</div>
", "@CoreHome/getRowEvolutionPopover.twig", "/home/httpd/electroluxodkuchni.pl/matomo/plugins/CoreHome/templates/getRowEvolutionPopover.twig");
    }
}
