<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @Transitions/renderPopover.twig */
class __TwigTemplate_6be421d696a3b20e06b934ec4cab91827b49251ce20479e8258538e6c569e537 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<div id=\"Transitions_Container\">
    <div id=\"Transitions_CenterBox\" class=\"Transitions_Text\">
        <h2></h2>

        <div class=\"Transitions_CenterBoxMetrics\">
            <p class=\"Transitions_Pageviews Transitions_Margin\">";
        // line 6
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), [$this->getAttribute(($context["translations"] ?? $this->getContext($context, "translations")), "pageviewsInline", [])]), "html", null, true);
        echo "</p>

            <div class=\"Transitions_IncomingTraffic\">
                <h3>";
        // line 9
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Transitions_IncomingTraffic"]), "html", null, true);
        echo "</h3>

                <p class=\"Transitions_PreviousPages\">";
        // line 11
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), [$this->getAttribute(($context["translations"] ?? $this->getContext($context, "translations")), "fromPreviousPagesInline", [])]), "html", null, true);
        echo "</p>

                <p class=\"Transitions_PreviousSiteSearches\">";
        // line 13
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), [$this->getAttribute(($context["translations"] ?? $this->getContext($context, "translations")), "fromPreviousSiteSearchesInline", [])]), "html", null, true);
        echo "</p>

                <p class=\"Transitions_SearchEngines\">";
        // line 15
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), [$this->getAttribute(($context["translations"] ?? $this->getContext($context, "translations")), "fromSearchEnginesInline", [])]), "html", null, true);
        echo "</p>

                <p class=\"Transitions_SocialNetworks\">";
        // line 17
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), [$this->getAttribute(($context["translations"] ?? $this->getContext($context, "translations")), "fromSocialNetworksInline", [])]), "html", null, true);
        echo "</p>

                <p class=\"Transitions_Websites\">";
        // line 19
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), [$this->getAttribute(($context["translations"] ?? $this->getContext($context, "translations")), "fromWebsitesInline", [])]), "html", null, true);
        echo "</p>

                <p class=\"Transitions_Campaigns\">";
        // line 21
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), [$this->getAttribute(($context["translations"] ?? $this->getContext($context, "translations")), "fromCampaignsInline", [])]), "html", null, true);
        echo "</p>

                <p class=\"Transitions_DirectEntries\">";
        // line 23
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), [$this->getAttribute(($context["translations"] ?? $this->getContext($context, "translations")), "directEntriesInline", [])]), "html", null, true);
        echo "</p>
            </div>

            <div class=\"Transitions_OutgoingTraffic\">
                <h3>";
        // line 27
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Transitions_OutgoingTraffic"]), "html", null, true);
        echo "</h3>

                <p class=\"Transitions_FollowingPages\">";
        // line 29
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), [$this->getAttribute(($context["translations"] ?? $this->getContext($context, "translations")), "toFollowingPagesInline", [])]), "html", null, true);
        echo "</p>

                <p class=\"Transitions_FollowingSiteSearches\">";
        // line 31
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), [$this->getAttribute(($context["translations"] ?? $this->getContext($context, "translations")), "toFollowingSiteSearchesInline", [])]), "html", null, true);
        echo "</p>

                <p class=\"Transitions_Downloads\">";
        // line 33
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), [$this->getAttribute(($context["translations"] ?? $this->getContext($context, "translations")), "downloadsInline", [])]), "html", null, true);
        echo "</p>

                <p class=\"Transitions_Outlinks\">";
        // line 35
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), [$this->getAttribute(($context["translations"] ?? $this->getContext($context, "translations")), "outlinksInline", [])]), "html", null, true);
        echo "</p>

                <p class=\"Transitions_Exits\">";
        // line 37
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), [$this->getAttribute(($context["translations"] ?? $this->getContext($context, "translations")), "exitsInline", [])]), "html", null, true);
        echo "</p>
            </div>
        </div>
    </div>
    <div id=\"Transitions_Loops\" class=\"Transitions_Text\">
        ";
        // line 42
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), [$this->getAttribute(($context["translations"] ?? $this->getContext($context, "translations")), "loopsInline", [])]), "html", null, true);
        echo "
    </div>
    <div id=\"Transitions_Canvas_Background_Left\" class=\"Transitions_Canvas_Container\"></div>
    <div id=\"Transitions_Canvas_Background_Right\" class=\"Transitions_Canvas_Container\"></div>
    <div id=\"Transitions_Canvas_Left\" class=\"Transitions_Canvas_Container\"></div>
    <div id=\"Transitions_Canvas_Right\" class=\"Transitions_Canvas_Container\"></div>
    <div id=\"Transitions_Canvas_Loops\" class=\"Transitions_Canvas_Container\"></div>
</div>

<script type=\"text/javascript\">
    var Piwik_Transitions_Translations = {
        ";
        // line 53
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["translations"] ?? $this->getContext($context, "translations")));
        foreach ($context['_seq'] as $context["internalKey"] => $context["translation"]) {
            // line 54
            echo "            \"";
            echo \Piwik\piwik_escape_filter($this->env, $context["internalKey"], "html", null, true);
            echo "\": \"";
            echo \Piwik\piwik_escape_filter($this->env, $context["translation"], "html", null, true);
            echo "\",
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['internalKey'], $context['translation'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 56
        echo "        \"\": \"\"
    };
</script>";
    }

    public function getTemplateName()
    {
        return "@Transitions/renderPopover.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  147 => 56,  136 => 54,  132 => 53,  118 => 42,  110 => 37,  105 => 35,  100 => 33,  95 => 31,  90 => 29,  85 => 27,  78 => 23,  73 => 21,  68 => 19,  63 => 17,  58 => 15,  53 => 13,  48 => 11,  43 => 9,  37 => 6,  30 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("<div id=\"Transitions_Container\">
    <div id=\"Transitions_CenterBox\" class=\"Transitions_Text\">
        <h2></h2>

        <div class=\"Transitions_CenterBoxMetrics\">
            <p class=\"Transitions_Pageviews Transitions_Margin\">{{ translations.pageviewsInline|translate }}</p>

            <div class=\"Transitions_IncomingTraffic\">
                <h3>{{ 'Transitions_IncomingTraffic'|translate }}</h3>

                <p class=\"Transitions_PreviousPages\">{{ translations.fromPreviousPagesInline|translate }}</p>

                <p class=\"Transitions_PreviousSiteSearches\">{{ translations.fromPreviousSiteSearchesInline|translate }}</p>

                <p class=\"Transitions_SearchEngines\">{{ translations.fromSearchEnginesInline|translate }}</p>

                <p class=\"Transitions_SocialNetworks\">{{ translations.fromSocialNetworksInline|translate }}</p>

                <p class=\"Transitions_Websites\">{{ translations.fromWebsitesInline|translate }}</p>

                <p class=\"Transitions_Campaigns\">{{ translations.fromCampaignsInline|translate }}</p>

                <p class=\"Transitions_DirectEntries\">{{ translations.directEntriesInline|translate }}</p>
            </div>

            <div class=\"Transitions_OutgoingTraffic\">
                <h3>{{ 'Transitions_OutgoingTraffic'|translate }}</h3>

                <p class=\"Transitions_FollowingPages\">{{ translations.toFollowingPagesInline|translate }}</p>

                <p class=\"Transitions_FollowingSiteSearches\">{{ translations.toFollowingSiteSearchesInline|translate }}</p>

                <p class=\"Transitions_Downloads\">{{ translations.downloadsInline|translate }}</p>

                <p class=\"Transitions_Outlinks\">{{ translations.outlinksInline|translate }}</p>

                <p class=\"Transitions_Exits\">{{ translations.exitsInline|translate }}</p>
            </div>
        </div>
    </div>
    <div id=\"Transitions_Loops\" class=\"Transitions_Text\">
        {{ translations.loopsInline|translate }}
    </div>
    <div id=\"Transitions_Canvas_Background_Left\" class=\"Transitions_Canvas_Container\"></div>
    <div id=\"Transitions_Canvas_Background_Right\" class=\"Transitions_Canvas_Container\"></div>
    <div id=\"Transitions_Canvas_Left\" class=\"Transitions_Canvas_Container\"></div>
    <div id=\"Transitions_Canvas_Right\" class=\"Transitions_Canvas_Container\"></div>
    <div id=\"Transitions_Canvas_Loops\" class=\"Transitions_Canvas_Container\"></div>
</div>

<script type=\"text/javascript\">
    var Piwik_Transitions_Translations = {
        {% for internalKey, translation in translations %}
            \"{{ internalKey }}\": \"{{ translation }}\",
        {% endfor %}
        \"\": \"\"
    };
</script>", "@Transitions/renderPopover.twig", "/home/httpd/electroluxodkuchni.pl/matomo/plugins/Transitions/templates/renderPopover.twig");
    }
}
