{if !empty($localizations)}
	<div id="map_canvas"><div id="map-open"></div></div>
	<script type="text/javascript">
		var places = [
			{foreach from=$localizations item=localization}
				[{$localization->lng}, {$localization->lat}, '{$localization->description|replace:"\n":""}', '{$localization->marker}'],
			{/foreach}
		];
	</script>
	<script src="{BASE_URL}data/js/open_maps/map.js" type="text/javascript"></script>
{/if}