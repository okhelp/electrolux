function load_map()
{
  if ($('#map-open').length > 0) {
    map = new OpenLayers.Map("map-open");
    map.addLayer(new OpenLayers.Layer.OSM());

    var lonLat = new OpenLayers.LonLat(places[0], places[1])
      .transform(
        new OpenLayers.Projection("EPSG:4326"),
        map.getProjectionObject()
      );

    var zoom = 12;
    map.setCenter(lonLat, zoom);

    var vectorLayer = new OpenLayers.Layer.Vector("Overlay");

    for (var i = 0; i < places.length; i++) {

        var lon = places[i][0];
        var lat = places[i][1];
        var description = places[i][2];
        var marker = places[i][3];

        var markerOptions = new OpenLayers.Feature.Vector(
            new OpenLayers.Geometry.Point(lon, lat).transform(
                new OpenLayers.Projection("EPSG:4326"),
                map.getProjectionObject()
            ),
            {description: description,
                longDesc: lon,
            latDesc: lat},

            {
                externalGraphic: 'data/js/open_maps/vendor/img/' + marker + '',
                graphicHeight: 35,
                graphicWidth: 30
            }
        );
        vectorLayer.addFeatures(markerOptions);
    }
        var controls = {
            selector: new OpenLayers.Control.SelectFeature(vectorLayer, { onSelect: createPopup, onUnselect: destroyPopup })
        };


        function createPopup(markerOptions) {
            markerOptions.popup = new OpenLayers.Popup("pop",
                markerOptions.geometry.getBounds().getCenterLonLat(),
                null,
                '<div class="popup_description">'+markerOptions.attributes.description+'</div>' +
                '<div class="popup_navigation"><a href="https://www.google.com/maps/dir/Current+Location/'+markerOptions.attributes.latDesc+','+markerOptions.attributes.longDesc+'">Nawiguj</a></div>',
                null,
                true,
                function() { controls['selector'].unselectAll(); }
            );
            //markerOptions.popup.closeOnMove = true;
            map.addPopup(markerOptions.popup);
        }

        function destroyPopup(feature) {
            feature.popup.destroy();
            feature.popup = null;
        }

        map.addControl(controls['selector']);
        controls['selector'].activate();


    map.addLayer(vectorLayer);

      //Add a selector control to the vectorLayer with popup functions

  }}

load_map();
