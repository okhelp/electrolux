var cookie_info_name = cookie_info_get_name();
function cookie_info_get_name()
{
	var url_cookiename = BASE_URL.split('/').join('__');
	url_cookiename = url_cookiename.split(':').join('');
	url_cookiename = url_cookiename.split('.').join('');
     	return url_cookiename;
}

function cookie_info_show()
{
	$("#cookie_info").css("display", "flex").hide().fadeIn();
}
function cookie_info_close()
{ 
	$.cookie(cookie_info_name,1);
	$("#cookie_info").fadeOut();
}

function cookie_info_is_deactive()
{
	var cookie_data = $.cookie(cookie_info_name);
	return (typeof cookie_data != 'undefined' && cookie_data == 1) ? true : false;
}

$(document).ready(function() 
{
	if(!cookie_info_is_deactive())
	{
		cookie_info_show();
	}
	$('#cookie_info a').click(function(e){
		e.preventDefault();
		cookie_info_close();
	})
        
})