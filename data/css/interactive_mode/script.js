CKEDITOR.disableAutoInline = true;

function interactive_mode_init()
{
	if (window.location.href.indexOf('interactive_mode=1') > -1)
	{
		//włączam pola
		$('[data-cms_column_name]').each(function ()
		{
			var name_ed = $(this).attr('data-cms_column_name');

			$(this).attr('id', 'ed_' + name_ed);
			$(this).attr('contenteditable', 'true');
			interactive_mode_load_ckeditor('ed_' + name_ed);
		});

		//dodaję do wszystkich odnośników geta interactive_mode = 1
		$('a').each(function ()
		{
			var a_href = $(this).attr('href');
			if (a_href != '')
			{
				set_get_params('interactive', 1, a_href);
			}
		})
		//uruchamiam belkę nawigacyjną
		$.ajax({
			type: "GET",
			url: BASE_URL + "ed/interactive_mode/uruchom.html",
			success: function (msg)
			{
				$(msg).appendTo("body");
			}
		});
	}
}

function interactive_mode_load_ckeditor(id_el)
{

	CKEDITOR.inline(id_el, {
		on: {
			blur: function (event)
			{

				// tworzę tablicę z danymi
				var dataArray = {
					"sekcja": id_el,
					"val": event.editor.getData()
				};

				$.ajax({
					type: "POST",
					url: BASE_URL + "ed/columns/ajax_zapisz.html",
					data: dataArray,
					success: function (msg)
					{
						if (msg)
						{
							alert(msg);
						}
					},
					error: function (XMLHttpRequest, textStatus, errorThrown)
					{
						alert('Wystąpił błąd poczas zapisu.');
					}
				});
			}
		}
	});

}


function set_get_params(name, value, url)
{
	if (typeof url !== "undefined")
	{
		var get_params = url;

		//sprawdzam czy są &  url (określam po jakim znaku robimy tablicę)
		if (get_params.indexOf('&') >= 0)
		{
			var url_split_char = '&';
		}
		else
		{
			var url_split_char = '?';
		}

		//tworzę tablicę z parametrami
		var get_params_arr = get_params.split(url_split_char);

		//sprawdzam czy w getach mamy name
		if (get_params.indexOf(name + '=') >= 0)
		{
			//zamieniam wartości parametru sort
			$(get_params_arr).each(function (k, v)
			{
				//szukam elementu
				if (v.indexOf(name + '=') >= 0)
				{
					get_params_arr[k] = name + '=' + value;
				}

			});

			//podmieniam wartości getów
			get_params = get_params_arr.join('&');
		}
		else //nie ma, dodaję do parametróe
		{
			if (url.indexOf('?') >= 0)
			{
				url_split_char = '&';
			}
			else
			{
				url_split_char = '?';
			}

			get_params += url_split_char + name + '=' + value;
		}

		return get_params;
	}

}

//inicjalizuję moduł
$(window).load(function ()
{
	interactive_mode_init();
})