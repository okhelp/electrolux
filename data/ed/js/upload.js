/*********************************
 * Uploader HTML5
 * author Paweł Otłowski <p.otlowski@goinweb.pl>
 ********************************/


//zmienne globlane
var id_name = "upload";

//informacja o stałym limicie plików
var static_file_limit;


/**
 * Nadaję inputom[type=file] odpowiednią klasę
 * @returns void
 */
function add_ids_name()
{
	var i = 0;
	$('input[type=file]').each(function(){
		i = i+1;
		$(this).attr('id',id_name+'_'+i)
	})
}

/**
 * Nadaję akcję przycisku "wgraj pliki/pliki
 * @returns void
 */
function add_action_btn_upload()
{
	$('.upload-form a.btn-upload').click(function(e){
		$(this).closest('div').find('input[type=file]').click();
		e.preventDefault();
	})
}

/**
 * Zbiorcza metoda to przygotowania formularza, nadanie elementom odpowiednich wartości
 * @returns void
 */
function prepare_upload_form()
{
	add_ids_name();
	add_action_btn_upload();
	//type_files = type_files.split(",");
}


/**
 * Metoda sprawdza czy plik jest zgodny z podanymi założeniami
 * @returns bool
 */
function check_file_extension(type)
{
	
	if(type_files.indexOf(type) != -1)
	{
		return true;
	}
	else
	{
		return false;
	}
}

function create_div(el_upload,file)
{
	var el = $(el_upload).find('.file_reader .row');
	el.append('<div class="col-md-2 thumbnail" data-file="'+file.name+'"></div>');
	el.find('.thumbnail:last').html('<div class="upload_file_loader text-center"><img src="'+BASE_URL+'data/img/ajax-loader-small.gif" /></div>');
	el.find('.thumbnail:last').append('<div class="desc"><small><b>Plik</b>: '+file.name+'<br /><b>Rozmiar</b>: '+parseFloat(file.size/1000000).toFixed(2)+'MB</small></div>');
	
	//dodaję możliwość usunięcia pliku, bądź jego przesunięcia jeżeli jest to multiupload
	el.find('.thumbnail:last').append('<div class="file_settings text-center"></div>')
	if(multiple)
	{
		el.find('.thumbnail:last .file_settings').append('<a href="#" class="move_upload_file" title="przesuń plik"><i class="ti-move"></i></a>');
		//inicjalizuje sortowanie
		el.sortable({
			handle: ".ti-move"
		}).disableSelection();;
	}
	el.find('.thumbnail:last .file_settings').append('<a href="#" class="remove_upload_file" title="usuń plik"><i class="ti-close"></i></a>');
	
	//dodaję akcję na usuniecie pliku
	el.find('.thumbnail:last .remove_upload_file').click(function(e){
		remove_file($(this));
		e.preventDefault();
	})
}

function remove_file(el)
{
	if(confirm('Czy na pewno chcesz usunąć ten plik?'))
	{
		var file_name = el.closest('.thumbnail').find('input[type=hidden]').val();
		var is_img = (typeof images !== "undefined" && images == 1) ? 1 : 0;
		$.ajax({
			type     : "POST",
			url      : BASE_URL+"upload/ajax_remove_file.html",
			data     : "i="+is_img+"&file_name="+file_name,
			success : function(msg) {
				el.closest('.thumbnail').fadeOut(function(){
					var el_form = el.closest('div.upload-form');
					$(this).remove();
					refresh_file_count(el_form)
				})
				
			}
		});
	}
}

function edit_file(el)
{
	var id_cimg = el.closest('.thumbnail').find('input[type=hidden]').val();
	id_cimg = parseInt(id_cimg);
	show_modal_ajax('edytuj zdjęcie', BASE_ED_URL + 'upload/edit_photo.html?id_cimg=' + id_cimg);
}

function previewImage(el_upload,file) 
{
	//dla pewności pomijam pliki, które nie są obrazkamis
    if (!file.type.match(/image.*/)) 
	{
        return;
    }

	//tworzę kontener z miniaturką, domyślnie z ajaxem
	var gallery = $(el_upload).find('.file_reader .row');
	gallery.find('.thumbnail:last').append('<div class="img"></div>')
    gallery.find('.thumbnail:last .img').html('<img class="img-responsive" />');
    var img = gallery.find('.thumbnail:last .img img');

	var reader  = new FileReader();
	reader.addEventListener("load", function () {
		//miniaturka zawsze jest dobrej orientacji
		var exif = EXIF.readFromBinaryFile(base64ToArrayBuffer(this.result));
		resetOrientation(reader.result, exif.Orientation, function(resetBase64Image) {
			img.attr('src',resetBase64Image);
		});
		
		
	}, false);

	if (file) 
	{
		reader.readAsDataURL(file);		
	}

	
}
function base64ToArrayBuffer (base64) 
{
    base64 = base64.replace(/^data\:([^\;]+)\;base64,/gmi, '');
    var binary_string =  window.atob(base64);
    var len = binary_string.length;
    var bytes = new Uint8Array( len );
    for (var i = 0; i < len; i++)        
	{
        bytes[i] = binary_string.charCodeAt(i);
    }
    return bytes.buffer;
}


function resetOrientation(srcBase64, srcOrientation, callback) 
{
	var img = new Image();	

	img.onload = function() 
	{
		var width = img.width,
				height = img.height,
			canvas = document.createElement('canvas'),
				ctx = canvas.getContext("2d");

		// pobieram wielkość obrazka canvas
		if ([5,6,7,8].indexOf(srcOrientation) > -1) 
		{
			canvas.width = height;
			canvas.height = width;
		} 
		else 
		{
			canvas.width = width;
			canvas.height = height;
		}

		//obracam obrazek
		switch (srcOrientation) 
		{
			case 2: 
				ctx.transform(-1, 0, 0, 1, width, 0); 
				break;
			case 3: 
				ctx.transform(-1, 0, 0, -1, width, height ); 
				break;
			case 4: 
				ctx.transform(1, 0, 0, -1, 0, height ); 
				break;
			case 5: 
				ctx.transform(0, 1, 1, 0, 0, 0); 
				break;
			case 6: 
				ctx.transform(0, 1, -1, 0, height , 0); 
				break;
			case 7: 
				ctx.transform(0, -1, -1, 0, height , width); 
				break;
			case 8: 
				ctx.transform(0, -1, 1, 0, 0, width); 
				break;
			default: 
				ctx.transform(1, 0, 0, 1, 0, 0);
		}

		// rysuję obrazek
		ctx.drawImage(img, 00, 0);

		// exportuję base64
		callback(canvas.toDataURL());
	};

	img.src = srcBase64;
}


/**
 * Metoda inicjalizująca upload + walidacja czy plik/pliki są zgodne z parametrami
 * @returns void
 */
function init_upload()
{
	$('.upload-form input[type=file]').on('change',function(){
		
		var el_upload = $(this).closest('div.upload-form');
		var file_count = this.files.length;
		var el_name = $(this).attr('id');
		
		//jeżeli mamy ustalony limit wgrywanych plików to sprawdzam czy nie został on przekroczony
		if(typeof file_limit !== "undefined" && file_count>file_limit)
		{
			alert('Przekroczono limit wgrywanych plików.');
			return;
		}
		
		//włączam loader i ustawiam zmienną do treści
		el_upload.find('.upload-loader').fadeIn();
		var loader_desc = el_upload.find('.upload-loader span');
		
		//przelatuję przez wszystkie wybrane pliki
		for(var i = 0; i<this.files.length; i++)
		{
	        var file =  this.files[i];
	        
			//pomijam pliki, które posiadają niedozwolone rozszerzenia
			if(check_file_extension(file.type))
			{
				
				//został przekroczony dopuszczalny rozmiar pliku, pomijam go
				if( file.size > (max_file_size* 1000) )
				{
					alert('Plik '+file.name+' został pominięty z powodu przekroczonego maksymalnego limitu rozmiaru pliku.');
					
					continue;
				}
				
				//tworzę diva z opisem wgrywanego pliku
				create_div(el_upload,file);
				
				//jeżeli mam pokazać miniaturki, robię to
				if(typeof show_thumb !== "undefined" && show_thumb == 1)
				{
					//pokazuję mniaturkę
					previewImage(el_upload, file);
					
					//zaciemniam miniaturkę
					el_upload.find('.thumbnail:last').css('opacity','0.2')
				}
				
				//no to czas na ajax wysyłający
				loader_desc.html('Proszę czekać...');
				
				//wgrywam plik na dysk xhr
				var url = BASE_URL+'upload/ajax_upload_file.html?t='+$.now();
				var xhr = new XMLHttpRequest();
				var fd = new FormData();

				//wysyłam plik xhr
				xhr.open("POST", url, false);
				xhr.onreadystatechange = function() 
				{
					//wgrałem jest ok
					if(xhr.readyState == 4)
					{
						//nazwą pliku może być id_cimg w przypadku uloadera obrazkowego
						
						var file_uploaded = xhr.responseText;
						if(multiple) //jest to multiupload
						{
							var input_name = el_name+'[]';
						}
						else //pojedyńczy wpis
						{
							var input_name = el_name;
						}
						
						//dodaję inputa z nazwą pliku
						var input_hidden = '<input type="hidden" value="'+file_uploaded+'" name="'+input_name+'" />';
						el_upload.find('.file_reader .thumbnail[data-file="'+file.name+'"]').append(input_hidden);
						
						//usuwam loader
						el_upload.find('.upload_file_loader').closest('div').remove();
						el_upload.find('.upload-loader').fadeOut();
						
						//usuwam przezroczystoc
						el_upload.find('.thumbnail').css('opacity','1');
						
						//jeżeli jest ustawiony limit to odpowiednio blokuję przycisk dodania nowych plików i zmniejszam limit o 1
						if(typeof static_file_limit == "undefined")
						{
							static_file_limit = file_limit;
						}
						
						if(typeof file_limit !== "undefined")
						{	
							refresh_file_count(el_upload);
						}
					}
				};

				//jeżeli to jest upload zdjęć to jest to ważne rozdzieleni, ponieważ je cimgujemy!!!
				if(typeof images !== "undefined" && images == 1)
				{
					fd.append("type", 'images');
				}
				else
				{
					fd.append("type", 'files');
				}

				//dodaję plik $_FILES['upload_file']
				fd.append("upload_file", file);

				//dodaję informację o maksymalnej wielkości pliku
				fd.append("max_file_size",max_file_size);

				//wysyłam POST
				xhr.send(fd);
				
			}
			else
			{
				alert('Plik '+file.name+' został pominięty z powodu niewłaściwego rozszerzenia.');
			}
	    }
		
		
	})
}

function refresh_file_count(el)
{
	var items = parseInt(el.find('.thumbnail').size());
	file_limit = parseInt(static_file_limit)-items;
}
$(document).ready(function() 
{
	//nazywam odpowienio elementy
	prepare_upload_form();
	
	//inicjowanie uploadu
	init_upload();
})


