function alert(alert_message)
{
	bootbox.alert({
		title: "IMSET CMS - komunikat",
		message: alert_message
	})
}

function init_active_menu()
{
	var current_url = window.location.href;

	//pobieram główny trzon adresu modułu
	var find_module_url_name = current_url.replace(BASE_ED_URL, '');
	find_module_url_name = find_module_url_name.split('/');
	var module_url_name = find_module_url_name[0];

	//przelatuję wszystkie główne menu w poszukiwaniu adresu
	var module_ed_url = BASE_ED_URL + module_url_name;

	$('#nav-accordion li').each(function ()
	{
		//sprawdzam czy w tym li nie występuje a, które ma w hrefie czesc frazy
		var li_a = $(this).find('a').eq(0);
		var li_a_href = li_a.attr('href').trim();
		if (li_a_href.indexOf(module_ed_url) > -1)
		{

			//pobieram pierwszy człon z urla
			var current_module = current_url.replace(BASE_ED_URL, '');
			current_module = current_module.split('/');
			current_module = current_module[0];

			//pobieram pierwszy człon z li
			var li_a_href_module = li_a_href.replace(BASE_ED_URL, '');
			li_a_href_module = li_a_href_module.split('/');
			li_a_href_module = li_a_href_module[0];

			//wersja dla submenu
			if (li_a.closest('ul.sub').size() > 0)
			{
				if (li_a_href == current_url)
				{
					li_a.closest('li').addClass('active');
					li_a.closest('ul.sub').closest('li').find('a:first').click();
					return false;
				}
				else
				{
					if (current_module === li_a_href_module)
					{
						li_a.closest('ul.sub').closest('li').find('a:first').click();
						return false;
					}
				}

			}

			//wersja bez submenu
			else
			{

				if (li_a_href === current_url || current_module === li_a_href_module)
				{
					li_a.click();
					return false;
				}

			}
		}
	})
}

$(window).load(function ()
{
	init_active_menu();
})

$(document).ready(function ()
{
	$('.show-url').click(function (e)
	{
		e.preventDefault();
		alert($(this).attr('data-url'));
	})
})

/* okienko typu confirm */
$(document).on("click", ".confirm", function (e)
{
	var link = $(this).attr("href");
	var text = $(this).attr('data-confirm-text');
	e.preventDefault();
	bootbox.confirm({
		title: 'goinweb cms - komunikat',
		message: text,
		buttons: {
			confirm: {
				label: 'Tak',
				className: 'btn-info'
			},
			cancel: {
				label: 'Nie',
				className: 'btn-danger'
			}
		},
		callback: function (result)
		{
			if (result)
			{
				document.location.href = link;
			}
		}
	});
});

function show_modal_ajax(title, ajax_url)
{
	// dodaję tytuł
	$('#modal_ajax .modal-title').text('goinweb cms - ' + title);

	//dodaję loader
	$('#modal_ajax .modal-body').html('<div class="text-center"><img src="' + BASE_URL + '/data/ed/img/ajax_loader.gif" alt="" /></div>');

	//otwieram okienko
	$('#modal_ajax').modal('show');

	//ładuję treść z linku
	$.ajax({
		type: "GET",
		url: ajax_url,
		success: function (msg)
		{
			$('#modal_ajax .modal-body div:first').slideUp();
			$('#modal_ajax .modal-body').html(msg);
		}
	});
}

function show_modal(title,content)
{
	// dodaję tytuł
	$('#modal_ajax .modal-title').text('IMSET CMS - ' + title);

	//dodaję loader
	$('#modal_ajax .modal-body').html('<div class="text-center"><img src="' + BASE_URL + '/data/ed/img/ajax_loader.gif" alt="" /></div>');

	//otwieram okienko
	$('#modal_ajax').modal('show');
	
	
	$('#modal_ajax .modal-body div:first').slideUp();
	$('#modal_ajax .modal-body').html(content);
}
function close_modal_ajax()
{
	$('#modal_ajax').modal('hide');
	$('#modal_ajax .modal-title').text('');
	$('#modal_ajax .modal-body').html('');	
}