$(document).ready(function(){
    
    /* więcej informacji */
    $('a.debug_more').click(function(e){
        
        var click_id = $(this).attr('id');
        var submenu_div = $('div.debug-submenu[data-id='+click_id+']');
        
        $('div.debug-submenu').not('[data-id='+click_id+']').fadeOut();
        submenu_div.fadeToggle();
        
        e.preventDefault();
    })
    
    //otworzenie debugera
    $('#debug_bar #debug_toggle #debug_close').show(); 
    $('#debug_bar').fadeIn();
    
    //otwarcie, zamkniecie
    
    var debug_width = $('#debug_bar').width();
    var debug_toggle_width = $('#debug_toggle').width();
    var debug_toggle_width = debug_width - debug_toggle_width - 30;
    
    $('#debug_toggle').click(function(){
        
        if($('#debug_bar').hasClass('open'))
        {
            $('#debug_bar').animate({"left":"-"+debug_toggle_width+"px"}, "fast");
            $('#debug_bar').addClass('close');
            $('#debug_bar').removeClass('open');
            $('#debug_close').fadeOut();
            $('#debug_open').fadeIn();
        }
        else
        {
            $('#debug_bar').removeClass('close');
            $('#debug_bar').animate({"left":"0px"}, "fast");
            $('#debug_bar').addClass('open'); 
            $('#debug_close').fadeIn();
            $('#debug_open').fadeOut();
        }
        
    })
	
	$('.debug_stemplate').click(function()
	{
		$(this).fadeOut(function(){
			$(this).remove();
		})
	})
})