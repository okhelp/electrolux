function cartInit() {
    $.get(BASE_URL + 'cart/get_cart_count.html', function (response) {
        if (response.length) {
            $('#cart-count').html(response).fadeIn();
        }
    });
}

cartInit();

$('.award_more__order_orderLink').on('click', function () {
    var id_prize = $(this).data('prize');
    var id_variant = $(this).data('variant');

    var data = {
        'id_prize': id_prize,
        'id_variant': id_variant,
    };

    $.ajax({
        url: BASE_URL + 'cart/add_product.html',
        type: 'POST',
        data: data,
    }).done(function () {
        $.toast({
            text: 'Dodano do koszyka',
            icon: 'success',
            position: 'top-right',
            stack: false
        })
    }).always(function () {
        cartInit();
    }).fail(function () {
        $.toast({
            text: 'Wystąpił błąd',
            icon: 'error',
            position: 'top-right',
            stack: false
        })
    })
});

/*----USUNIĘCIE PRODUKTU Z KOSZYKA-----*/
"use strict";

function deleteFromCart() {
    $('.delete_link').on('click', function () {
        var idCartElement = $(this).data('id-cart-element');

        function sendTargetVal() {
            loaderInit();
            $.ajax({
                url: BASE_URL + 'cart/remove_product.html',
                type: 'POST',
                data: {
                    'id_cart_element': idCartElement
                }
            }).done(function (response) {
                $.toast({
                    text: 'Produkt usunięty',
                    icon: 'success',
                    position: 'top-right',
                    stack: false
                });
                $('.cart-products-listing').html(response);
                cartInitFirstStep();
            }).always(function () {
                cartInit();
                loaderHide();
            }).fail(function () {
                $.toast({
                    text: 'Wystąpił błąd',
                    icon: 'error',
                    position: 'top-right',
                    stack: false
                });
            });
        }

        Swal.fire({
            text: 'Czy napewno chcesz usunąć produkt?',
            type: 'warning',
            confirmButtonText: 'TAK',
            showCancelButton: true,
            cancelButtonText: 'NIE'
        }).then(function (result) {
            if (result.value) {
                sendTargetVal();
            } else if (result.dismiss === Swal.DismissReason.cancel) {
                return false;
            }
        });
    });
}

/*--------ZMNIEJSZENIE/ZWIĘKSZENIE ILOŚCI W KOSZYKU------*/
function updateProductCount() {
    $('.input_product_quantity').on('change', function () {

        var id_cart_element = $(this).data('cart-element');
        var value = $(this).val();

        function sendTargetVal() {
            loaderInit();
            $.ajax({
                url: BASE_URL + 'cart/update_product_count.html',
                type: 'POST',
                data: {'id_cart_element': id_cart_element, 'count': value},
            }).done(function (response) {
                $.toast({
                    text: 'Zaktualizowano ilość produktu',
                    icon: 'success',
                    position: 'top-right',
                    stack: false
                });

                $('.cart-products-listing').html(response);
                cartInitFirstStep();
            }).always(function () {
                cartInit();
                loaderHide();
            }).fail(function () {
                $.toast({
                    text: 'Wystąpił błąd',
                    icon: 'error',
                    position: 'top-right',
                    stack: false
                })
            })
        }

        sendTargetVal();
    });
}

function buttonsQuantity() {
    $('.btn_change_quantity').on('click', function () {
        var inputProduct = $(this).attr('data-field');
        var typeButton = $(this).attr('data-type');
        var currentInput = $(".input_product_quantity[name='" + inputProduct + "']");
        var currentButtonMinus = $(".btn_change_quantity[data-field='" + inputProduct + "'][data-type='minus']");
        var currentVal = parseInt(currentInput.val());
        if (typeButton === 'minus') {
            if (currentVal > currentInput.attr('min')) {
                currentInput.val(currentVal - 1).change();
            }
            if (currentInput.val() === currentInput.attr('min')) {
                $(this).attr('disabled', true);
            }
        } else if (typeButton === 'plus') {
            currentInput.val(currentVal + 1).change();
            $(currentButtonMinus).attr('disabled', false);
        }
    });

    $('.btn_change_quantity[data-type="minus"]').each(function () {
        var inputProduct = $(this).attr('data-field');
        var currentInput = $(".input_product_quantity[name='" + inputProduct + "']");
        var currentVal = currentInput.val();
        if (currentVal === currentInput.attr('min')) {
            $(this).attr('disabled', true);
        }
    });
}

function cartInitFirstStep() {
    deleteFromCart();
    updateProductCount();
    buttonsQuantity();
}

cartInitFirstStep();
