/*-------------MENU-------------*/
$(document).ready(function () {
    function toggleDropdown (e) {
        const _d = $(e.target).closest('.dropdown'),
            _m = $('.dropdown-menu', _d);
        setTimeout(function(){
            const shouldOpen = e.type !== 'click' && _d.is(':hover');
            _m.toggleClass('show', shouldOpen);
            _d.toggleClass('show', shouldOpen);
            $('[data-toggle="dropdown"]', _d).attr('aria-expanded', shouldOpen);
        }, e.type === 'mouseleave' ? 0 : 0);
    }

    $('body')
        .on('mouseenter mouseleave','.megamenu-li',toggleDropdown)
        .on('click', '.dropdown-menu a', toggleDropdown);
    $('.main_page_slick').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        slide: '.main_page_slick__single',
        arrows: false,
        fade: true,
        autoplay: true,
        autoplaySpeed: 10000,
    });
    $('.prev-slide-main-more').on('click', function (e) {
        e.preventDefault();
        $('.main_page_slick').slick('slickPrev');
    });

    $('.next-slide-main-more').on('click', function (e) {
        e.preventDefault();
        $('.main_page_slick').slick('slickNext');
    });

    $(".megamenu").on("click", function (e) {
        e.stopPropagation();
    });
    $(document).ready(function () {
        var scrollTop = $(document).scrollTop();
        if (scrollTop > 0) {
            $('#navbar_top, .page').addClass('scrolled');
        }
    });
    $(document).scroll(function (e) {
        var scrollTop = $(document).scrollTop();

        if (scrollTop > 0) {
            $('#navbar_top, .page').addClass('scrolled');
        } else {
            $('#navbar_top, .page').removeClass('scrolled');
        }
    });

    function navbarPoints() {
        if ($(window).width() >= 600) {
            $(".navbar-brand").after($('.navbar_points'));
        } else {
            $(".navbar_points__mobile").append($('.navbar_points'));
        }
    }

    navbarPoints();
    $(window).resize(function () {
        navbarPoints();
    });


    $("#menu").mmenu({
        navbars: [{
            content: [
                'prev',
                'close'
            ]
        }],
        "extensions": [
            "pagedim-black",
            "fullscreen"
        ],
        wrappers: ['bootstrap4']
    }, {});

    function vertical_menu_show() {
        $('.dropdown_hide_items').toggleClass('dropdown_show_items');
    }

    $('.mm-menu .Vertical').on('click', function (e) {
        e.preventDefault();
        $(this).closest('.mm-listitem__text').find('.dropdown_hide_items').toggleClass('dropdown_show_items');
    });
});

function lazyLoaderImg() {
    $('.lazy').Lazy({
        // your configuration goes here
        scrollDirection: 'vertical',
        effect: 'fadeIn',
        attribute: "data-src",
        visibleOnly: true,
        onError: function (element) {
            console.log('error loading ' + element.data('src'));
        },
        afterLoad: function (element) {
        }
    });
}

function equalHeight(wrapper, element) {
    $(wrapper).each(function () {
        var highestBox = 0;
        $(element, this).each(function () {
            if ($(this).height() > highestBox) {
                highestBox = $(this).height();
            }
        });
        $(element, this).height(highestBox);
    });
}

function equalHeightInit() {
    $('.single_product__price_row').height('auto');
    equalHeight('.equalHeightContainer', '.single_product__price_row');
    $('.user_panel_box_associate').height('auto');
    $('.user_panel_box_address').height('auto');
    equalHeight('.user_panel_box_associate_wrap', '.user_panel_box_associate');
    equalHeight('.user_panel_box_address_wrap', '.user_panel_box_address');
}

$(window).resize(function () {
    equalHeightInit();
});

function initProductListing() {
    /*WIDOK LISTY PRODUKTÓW*/

    /*--------RÓWNA WYSOKOŚć ELEMENTÓW------------*/
    lazyLoaderImg();
    equalHeightInit();

    /*Ukrywanie filtrów produktów*/
    /*Extra info produktów na hover*/
    $('.extra_info').tooltipster({
        plugins: ['follower'],
        minWidth: 200,
        maxWidth: 350,
    });
}

initProductListing();

function lastView() {
    var lastViewUser = localStorage.getItem('view');
    if (lastViewUser === 'list') {
        listView();
    } else if (lastViewUser === 'list_no_img') {
        listNoImgView();
    }

}
function listView() {
    $('.product_listing__row').hide().addClass('product_listing--list').removeClass('product_listing--grid product_listing--noImg').fadeIn();
}

function listNoImgView() {
    $('.product_listing__row').hide().removeClass('product_listing--list product_listing--grid').addClass('product_listing--noImg').fadeIn();
}

function gridView() {
    $('.product_listing__row').hide().removeClass('product_listing--list product_listing--noImg').addClass('product_listing--grid').fadeIn();
}
$(document).ready(function () {
    $('#list').click(function (event) {
        event.preventDefault();
        listView();
        equalHeightInit();
    });
    $('#grid').click(function (event) {
        event.preventDefault();
        gridView();
        equalHeightInit();
    });
    $('#list_no_img').click(function (event) {
        event.preventDefault();
        listNoImgView();
        equalHeightInit();
    });
    $('.product_list_sort__link').click(function () {
        var viewId = $(this).attr('id');
        localStorage.setItem('view', viewId);
    });

    function showHideFilter() {
        $('.product_list_filter__title').toggleClass('product_list_filter__icon--change');
        $('.product_listing').toggleClass('product_listing--hide');
        $('.product_list_filter').toggleClass('product_list_filter--hide');
    }

    $('#filter_show').on('click', function () {
        $.when(showHideFilter()).done(function () {
            setTimeout(function () {
                equalHeightInit();
            }, 200);
        });
    });
    /*Produkt info - więcej - specyfikacja*/
    $('.product_more__tab_link').on('click', function () {
        var tabId = $(this).attr('data-tab');
        // $('.product_more__photo').addClass('product_more__photo--hide');
        $('.product_more__photo').fadeOut();
        // $('.product_more__photo img').fadeOut();
        $('html,body').animate({
                scrollTop: $('body').offset().top
            },
            'slow');
        if ($('.product_more__specification:visible').length <= 0) {

            // $('.product_more__photo').addClass('product_more__photo--hide');
            $('.product_more__photo').fadeOut(
                function () {
                    $("#" + tabId).fadeIn(function () {
                        truncateText('.product_more__specification_list');
                    });
                }
            );
            $(this).find('i').toggleClass('icon-plus icon-minus');
        } else if ($('#' + tabId).is(':visible') === true) {
            $('.product_more__specification').hide();
            // $('.product_more__photo').removeClass('product_more__photo--hide');
            $('.product_more__photo').fadeIn();
            // $('.product_more__photo img').fadeIn();
            $(this).find('i').toggleClass('icon-minus icon-plus');
            truncateText('.product_more__specification_list');
        } else {
            $('.product_more__specification').hide();
            $("#" + tabId).fadeIn().addClass('current');
            $('.product_more__tab_link i').toggleClass('icon-minus icon-plus');
            truncateText('.product_more__specification_list');
        }
    });
    function checkTabId() {
        if(window.location.hash === '#tab-1') {
            $('.product_more__tab_link[data-tab="tab-1"]').click();
        }
    }
    if ($(window).width() >= 768) {
        checkTabId();
    }
    $('.product_more__close').on('click', function () {
        $('.product_more__photo').fadeIn();
        // $('.product_more__photo img').fadeIn();
        $('.product_more__specification').hide();
        $('.product_more__tab_link i').removeClass('icon-minus').addClass('icon-plus');
    });

    function truncateText(element) {
        $(element).dotdotdot({
            height: 500,
        });
        $('.linkMore').text('Pokaż więcej');
        if ($('#product_more__specification_list1').height() > 450) {
            $('#product_more__specification_list1').closest('#tab-1').find('.linkMore').fadeIn().css("display","inline-block");
        }
        if ($('#product_more__specification_list2').height() > 450) {
            $('#product_more__specification_list2').closest('#tab-2').find('.linkMore').fadeIn().css("display","inline-block");
        }
    }


    // $('.linkMore').on('click', function () {
    //     var textTrucate_id = $(this).attr('data-target')
    //     $(this).toggleClass('show');
    //     if ($(this).hasClass('show')) {
    //         $(this).text('zmiana');
    //         $('#' + textTrucate_id).dotdotdot({
    //             height: null,
    //         });
    //     } else {
    //         $(this).text('Pokaż więcej');
    //         $('#' + textTrucate_id).dotdotdot({
    //             height: 500,
    //         });
    //     }
    // });

    $.each($('.linkMore'), function () {
        $(this).off().click(function () {
            var textTrucate_id = $(this).attr('data-target');
            var tab_id = $(this).closest('div').attr('id');
            $(this).toggleClass('show');
            if ($(this).hasClass('show')) {
                $(this).text('Pokaż mniej');
                $('#' + textTrucate_id).dotdotdot({
                    height: null,
                });
            } else {
                $(this).text('Pokaż więcej');
                $('#' + textTrucate_id).dotdotdot({
                    height: 500,
                });
                $('html,body').animate({
                        scrollTop: $("#" + tab_id).offset().top - $('#navbar_top').height()
                    },
                    'slow');
            }
        })
    });


    function DataTableInit(table) {
        $(table).DataTable({
            "scrollX": true,
            "language": {
                "processing": "Przetwarzanie...",
                "search": "Szukaj:",
                "lengthMenu": "Pokaż _MENU_ pozycji",
                "info": "Pozycje od _START_ do _END_ z _TOTAL_ łącznie",
                "infoEmpty": "Pozycji 0 z 0 dostępnych",
                "infoFiltered": "(filtrowanie spośród _MAX_ dostępnych pozycji)",
                "infoPostFix": "",
                "loadingRecords": "Wczytywanie...",
                "zeroRecords": "Nie znaleziono pasujących pozycji",
                "emptyTable": "Brak danych",
                "paginate": {
                    "first": "Pierwsza",
                    "previous": "Poprzednia",
                    "next": "Następna",
                    "last": "Ostatnia"
                },
                "aria": {
                    "sortAscending": ": aktywuj, by posortować kolumnę rosnąco",
                    "sortDescending": ": aktywuj, by posortować kolumnę malejąco"
                }
            }
        });
    }

    DataTableInit('.user_panel__table');
});

/*----czy customowy input ma value----*/
function checkInputValue() {
    $('.custom_form__input--input').each(function () {
        if ($(this).val().trim() !== "") {
            $(this).addClass('has-val');
        } else {
            $(this).removeClass('has-val');
        }
        $(this).on('blur', function () {
            if ($(this).val().trim() !== "") {
                $(this).addClass('has-val');
            } else {
                $(this).removeClass('has-val');
            }
        })
    })
}
function inputValueReset() {
    $('.custom_form__input--input').each(function () {
        $(this).removeClass('has-val');
    })
}

/*----LOADER----*/
function loaderInit() {
    $("#loader").css("display", "flex").hide().fadeIn();
}

function loaderHide() {
    $("#loader").fadeOut();
}

$(document).ready(function () {
    $('.product_more_slick__main').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        lazyLoad: 'progressive',
        slide: '.product_more_slick__main_single',
        arrows: false,
        fade: true,
        asNavFor: '.product_more_slick__thumbs'
    });
    $('.prev-slide-product-more').on('click', function (e) {
        e.preventDefault();


        $('.product_more_slick__main').slick('slickPrev');
    });

    $('.next-slide-product-more').on('click', function (e) {
        e.preventDefault();


        $('.product_more_slick__main').slick('slickNext');
    });
    $('.product_more_slick__thumbs').slick({
        slidesToShow: 1,
        infinite: true,
        slidesToScroll: 1,
        slide: '.product_more_slick__thumbs_single',
        asNavFor: '.product_more_slick__main',
        dots: false,
        arrows: false,
        centerMode: true,
        focusOnSelect: true,
        variableWidth: true,
        lazyLoad: 'progressive',
    });
    $('.product_more_slick__thumbs').slick('refresh');
    $('.product_more_slick__main').lightGallery({
        selector: '.product_more_slick__main_single',
        exThumbImage: 'data-exthumbimage',
        autoplayFirstVideo: false
    });
    if (typeof slideShowRecommended !== 'undefined') {
        var settings = {
            slidesToShow: slideShowRecommended >= 5 ? 5 : slideShowRecommended,
            slidesToScroll: 1,
            slide: '.product_listing__single_product',
            arrows: false,
            infinite: false,
            responsive: [
                {
                    breakpoint: 1800,
                    settings: {
                        slidesToShow: slideShowRecommended >= 4 ? 4 : slideShowRecommended
                    }
                },
                {
                    breakpoint: 1200,
                    settings: {
                        slidesToShow: slideShowRecommended >= 3 ? 3 : slideShowRecommended
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: slideShowRecommended >= 2 ? 2 : slideShowRecommended
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        autoplay: true,
                        autoplaySpeed: 2000,
                    }
                }
            ]
        };
    }
    $('.recommended_products_slick').slick(settings);
    $('.prev-slide-recommended').on('click', function (e) {
        e.preventDefault();
        $('.recommended_products_slick').slick('slickPrev');
    });
    $('.next-slide-recommended').on('click', function (e) {
        e.preventDefault();
        $('.recommended_products_slick').slick('slickNext');
    });
    $('.registry_sales__input').tooltip();

});
