<?php

/**
 * Sets up MinApp controller and serves files
 * 
 * DO NOT EDIT! Configure this utility via config.php and groupsConfig.php
 * 
 * @package Minify 
 */
session_start();

$app = (require __DIR__ . '/bootstrap.php');
/* @var \Minify\App $app */

//załączam plik konfiguracyjny
$core_config_file_path = dirname(dirname(dirname(__FILE__))) . '/settings/config.php';
if (is_file($core_config_file_path))
{
	//załączam plik konfiguracyjny
	require_once $core_config_file_path;
	require_once dirname(dirname(dirname(__FILE__))) . '/lib/autoload.php';

	$url_file = '/' . str_replace(array("/", 'http', 'https', ':', $_SERVER['HTTP_HOST']), '', BASE_URL);
	$template = $_SESSION['template'];

	if (!empty($_GET) && (!empty($_GET['f'] || !empty($_GET['g']))) && !empty($_SESSION['template']))
	{

		if (!empty($_GET['f']))
		{
			$f = $_GET['f'];
			$f = str_replace('/_css/', $url_file . "/templates/$template/_assets/css/", $f);
			$f = str_replace('/_fonts/', BASE_URL . "templates/$template/_assets/fonts/", $f);
			$f = str_replace('/_js/', $url_file . "/templates/$template/_assets/js/", $f);

			if (strpos($f, '?') !== FALSE)
			{
				$f_arr = explode('?', $f);
				$f = reset($f_arr);
			}

			$f_arr = explode(',', $f);

			foreach ($f_arr as $k => $v)
			{
				if (strpos($v, $url_file) !== FALSE)
				{
					
				}
				else
				{
					//jeżeli na początku mamy frazę data
					if (substr($v, 0, 6) == '/data/')
					{
						$v = $url_file . $v;
					}

					$f_arr[$k] = $v;
				}
			}


			$f = implode(',', $f_arr);

			$_GET['f'] = $f;
		}
	}

	$app->runServer();
}

