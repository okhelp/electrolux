<?php

/**
 * Klasa sesji, dodawanie, usuwanie, flashowana sesja, klasa statyczna
 * @author Paweł Otłowski
 */
class Lib__Session
{
	public static function get($attribute = NULL)
	{
		if(!empty($attribute) && isset($_SESSION[$attribute]))
		{
			return $_SESSION[$attribute];
		}
		elseif (!$attribute)
		{
			return $_SESSION;
		}
		else
		{
			return NULL;
		}
	}
	
	public static function set($attribute,$value=NULL)
	{
		if(is_array($attribute) && !$value)
		{
			foreach($attribute as $k => $v)
			{
				$_SESSION[$k] = $v;
			}
		}
		else
		{
			$_SESSION[$attribute] = $value;
		}
	}
	
	public static function remove($attr=NULL)
	{
		if(!$attr) //usuń wszystko
		{
			$_SESSION = array();
		}
		elseif($attr && is_array($attr)) //atrybut jest tablicą usuwamy wzystkie zdeklarowane klucze
		{
			foreach($attr as $k_session)
			{
				unset($_SESSION[$k_session]);
			}
		}
		else //usuwamy pojedyńczy wpis
		{
			unset($_SESSION[$attr]);
		}
	}
}