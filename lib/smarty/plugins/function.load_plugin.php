<?php

function smarty_function_load_plugin($params)
{
	$html_code = '';
	$base_url = BASE_URL . 'data/';
	$base_url .= is_ed() ? 'ed/' : '';
	$base_url_ed = $base_url . 'ed/';


	if (!empty($params['name']))
	{
		switch ($params['name'])
		{
			case 'datatable':
				$html_code = '<script type="text/javascript" src="' . $base_url . 'js/datatable/jquery.dataTables.js"></script>';
				$html_code .= '<link rel="stylesheet" type="text/css" href="' . $base_url . 'js/datatable/jquery.dataTables.css" />';
				$html_code .= '<script type="text/javascript">
					$(document).ready(function(){
					  $(\'.datatable\').dataTable({
					  "language": {
							  "url": "' . $base_url . 'js/datatable/polish.json"
							  },
					  "aaSorting": [],
					  "aoColumnDefs": [
					   {
						   "bSortable": false,
						   "aTargets": ["nosort"]
					   },

					  ]				
					  });

						  })

					  </script>';
				break;
            case 'chosen':
                $html_code = '<script src="' . BASE_URL . 'data/js/chosen/chosen.jquery.js"></script>';
                $html_code .= '<link rel="stylesheet" type="text/css" href="' . BASE_URL . 'data/js/chosen/chosen.css" />';
                $html_code .= '<script type="text/javascript">
					$(document).ready(function(){
                        $(".chosen").chosen();
                    })

					  </script>';
                break;

			case 'ckeditor':
				$html_code = '<script src="' . BASE_URL . 'data/js/ckeditor/ckeditor.js"></script>';
				break;
			case 'bootbox':
				$html_code = '<script src="' . BASE_URL . 'data/ed/js/bootbox.min.js"></script>';
				break;

			case 'theme_editor':
				$mode = !empty($params['mode']) ? $params['mode'] : 'html';
				$html_code = '<script src="' . BASE_URL . 'data/ed/js/theme_editor/ace.js"></script> ';
				$html_code .= '<script src="' . BASE_URL . 'data/ed/js/theme_editor/ext-language_tools.js"></script> ';
				$html_code .= '<script>
								// trigger extension
								ace.require("ace/ext/language_tools");
								var editor = ace.edit("editor");
								editor.session.setMode("ace/mode/'.$mode.'");
								editor.setTheme("ace/theme/tomorrow");
								editor.setAutoScrollEditorIntoView(true);
								// enable autocompletion and snippets
								editor.setOptions({
									enableBasicAutocompletion: true,
									enableSnippets: true,
									enableLiveAutocompletion: false,
									maxLines: 150
								});
							</script>';
				break;

			case 'masked_input':
				$html_code = '<script src="' . BASE_URL . 'data/js/jquery.maskedinput.js"></script>';
				break;

			case 'datepicker':
				$html_code = '<link rel="stylesheet" type="text/css" href="' . $base_url . 'js/ui/jquery-ui.css" />';
				$html_code .= '<script src="' . $base_url . 'js/ui/jquery.ui.datepicker-pl.js"></script>';
				$html_code .= '<script type="text/javascript"> $(document).ready(function()  { $(\'.datepicker\').datepicker({dateFormat: \'yy-mm-dd\'}); }) </script>';
				break;

			case 'tree':
				$html_code = '<script src="' . $base_url . 'js/tree.js"></script>';
				$html_code .= '<link rel="stylesheet" type="text/css" href="' . $base_url . 'css/tree.css" />';
				break;

			case 'colorpicker':
				$html_code = '<script src="' . $base_url . 'js/colorpicker/jquery.minicolors.min.js"></script>';
				$html_code .= '<link rel="stylesheet" type="text/css" href="' . $base_url . 'js/colorpicker/jquery.minicolors.css" />';
				break;

            case 'jstree':
                $html_code = '<script src="' . BASE_URL . 'data/ed/js/jstree/jstree.js"></script>';
                $html_code .= '<link href="' . BASE_URL .  'data/ed/js/jstree/themes/default/style.css" rel="stylesheet"/>';
                break;

			case 'interactive_mode':
				$html_code = '<style type="text/css">#debug_bar
				{
					display:none!important;
				}

				#interactive_mode
				{
					position:fixed;
					bottom: 0;
					width:100%;
					background:#313131;
				}
				
				.interactive_mode_content
				{
					padding:12px;
					height: 44px;
				}
				.interactive_mode_logo_gw
				{
					display:block;
					float:left;
				}
				.interactive_mode_logo_gw img 
				{
					height:20px;
					margin-right:12px;
					float: left;
				}

				.interactive_mode_logo_gw span
				{
					color: #fff;
					display: inline-block;
					font-size: 13px;
					position: relative;
					top: 1px;
					font-weight: bold;
					font-family: Arial,Helvetica Neue,Helvetica,sans-serif;
				}

				.interactive_mode_controls 
				{
					float:right;
				}

				#interactive_mode_close
				{
					-webkit-transition: all .2s ease-in-out;
					 -o-transition: all .2s ease-in-out;
					 -moz-transition: all .2s ease-in-out;
					 transition: all .2s ease-in-out;
					background-color: #04d18b;
					color: #313131;
					display: block;
					font-weight: bold;
					padding: 4px 12px;
				}

				#interactive_mode_close:hover
				{
					opacity:0.9;
				}

				div[data-cms_column_name]
				{
					-webkit-transition: all .2s ease-in-out;
					 -o-transition: all .2s ease-in-out;
					 -moz-transition: all .2s ease-in-out;
					 transition: all .2s ease-in-out;
				}

				div[data-cms_column_name]:hover, div[data-cms_column_name].cke_focus
				{
					-webkit-box-shadow: 10px 10px 35px -4px rgba(0,0,0,0.33);
					-moz-box-shadow: 10px 10px 35px -4px rgba(0,0,0,0.33);
					box-shadow: 10px 10px 35px -4px rgba(0,0,0,0.33);
				}</style>';
				$html_code .= '<script src="' . BASE_URL . '_js/interactive_mode/script.js"></script>';
		}
	}

	return $html_code;
}
