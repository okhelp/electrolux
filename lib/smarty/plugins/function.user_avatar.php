<?php

function smarty_function_user_avatar($params)
{
    $id_cimg = 0;
	$id_user = !empty($params['id']) ? (int)$params['id'] : 0;
	$width = !empty($params['width']) ? (int)$params['width'] : 0;
	$height = !empty($params['height']) ? (int)$params['height'] : 0;
	//pobieram id_cimg
	if(!empty($id_user))
	{
	    $pdo = new Lib__PDO;
	    $user = $pdo->query("SELECT * FROM users WHERE id=$id_user");

		if(!empty($user))
		{
			$user = reset($user);
			if(!empty($user['id_cimg']))
			{
				$id_cimg = $user['id_cimg'];
			}
		}
	}
    
	//pobieram obrazek z tego id cimga
	return App__Ed__Model__Img::get_file(array(
		'id_cimg' => $id_cimg,
		'width' => $width,
		'height' => $height
	));
}