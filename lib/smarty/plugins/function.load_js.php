<?php
function smarty_function_load_js($params)
{
	$html = NULL;
	
	if(!empty($params['name']))
	{
		$file = $params['name'];
		
		if(strpos($file,'http') !== FALSE)
		{
			$html = '<script src="'.$file.'"></script>';
		}
		else
		{
			$html = '<script src="'.BASE_URL.'_js/'.$file.'"></script>';
		}
	}
	
	return $html;
}