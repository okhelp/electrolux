<?php

function smarty_modifier_price($string)
{
    return number_format($string, 2, ',', ' ');
}
