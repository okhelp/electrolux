<?php

function smarty_modifier_to_assoc($array)
{
	$return = array();
    if(!empty($array))
	{
		foreach($array as $data)
		{
			$return[$data->id] = $data->name;
		}
	}
	return $return;
}
