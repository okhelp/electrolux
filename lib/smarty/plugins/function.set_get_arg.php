<?php
function smarty_function_set_get_arg($params)
{
	//tablica w której będą przechowywane gety
	$get = array();
	
	//pobieram stringa z getami z adresu
	$query_string = !empty($_SERVER['QUERY_STRING']) ? $_SERVER['QUERY_STRING'] : '';
	
	//tworzę tablicę z getami
	if(!empty($query_string))
	{
		foreach(explode('&', $query_string) as $query_item)
		{
			$query_item_arr = explode('=',$query_item);
			$get[$query_item_arr[0]] = $query_item_arr[1];
		}
	}
	
	//pobieram główny adres
	$main_url = BASE_URL . substr($_SERVER['PATH_INFO'], 1);
	
	//zapisuję zmianę do tablicy
	$get[$params['name']] = $params['value'];
	
	return $main_url . '?' . http_build_query($get);
}