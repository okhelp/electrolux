<?php

function smarty_modifier_friendly_date($timestamp)
{
    return friendly_date($timestamp);
}
