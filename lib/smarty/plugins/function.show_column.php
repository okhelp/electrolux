<?php
function smarty_function_show_column($params)
{
	$column_value = '';
	
	if(!empty($params) && !empty($params['name']))
	{
		$name = $params['name'];
		$column_value = App__Ed__Model__Columns__Model::show_column($name);
		if(empty($column_value))
		{
			throw new Exception("Brak zdefiniowanej kolumny: $name");
		}
	}
	else
	{
		throw new Exception("Proszę podać nazwę kolumny do wyświetlenia.");
	}
	
	return $column_value;
}
