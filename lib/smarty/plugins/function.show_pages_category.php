<?php
function smarty_function_show_pages_category($params)
{
	$html = '';
	//przypisuję parametry
    $id_category = !empty($params['id_category']) ? (int)$params['id_category'] : 0;
	$ul_class = isset($params['ul_class']) ? $params['ul_class'] : '';
	
	//jeżeli wprowadzono wartość id_category, to szukam linków
	if(!empty($id_category))
	{
		//pobieram wpisy z kategorii
		$pages_list = App__Ed__Model__Pages::find(App__Ed__Model__Pages::where("id_category = $id_category"));
		if(!empty($pages_list))
		{
			$html .= '<ul' . (!empty($ul_class) ? ' class="'.$ul_class.'"' : '') . '>';
			
			foreach($pages_list as $item)
			{
				$html .= '<li><a href="'.$item->get_url().'" title="'.$item->title.'">'.$item->title.'</a></li>';
			}
			
			$html .= "</ul>";
			
		}
		
	}
	
	return $html;
}