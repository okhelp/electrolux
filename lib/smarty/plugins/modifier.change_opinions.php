<?php

function smarty_modifier_change_opinions($opinions)
{
	if ($opinions == 1)
	{
		return 'opinia';
	}
	if ($opinions % 10 > 1 && $opinions % 10 < 5 && !( $opinions % 100 >= 10 && $opinions % 100 <= 21 ))
	{
		return 'opinie';
	}

	return 'opinii';
}
