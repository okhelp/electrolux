<?php

function smarty_function_pagination($params)
{	
	//ustawiam prefix
	if(isset($params['prefix']) && !empty($params['prefix']))
	{
		$params['prefix'] .= "_";
	}
	else
	{
		$params['prefix'] = '';
	}
	$params['prefix'] .= 'page';

	$pages_count = ($params['all_items'] / $params['per_page']);

	$pages_count = ceil($pages_count);

	//liczę ile wyszło stron
	$params['all_pages'] = round($pages_count, 0, PHP_ROUND_HALF_UP);
	
	//konwertuję numer aktualnej strony
	$params['current_page'];
	
	//ładuję widok
	$smarty = new Smarty;
	$smarty->assign('params',$params);
	return $smarty->fetch(get_app_template_path() . "default/_partials/pagination.tpl");
}