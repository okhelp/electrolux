<?php
function smarty_function_display_tree($params)
{
    $data = $params['data'];
    $template = !empty($params['template']) ? $params['template'] : '';
    $input_type = !empty($params['input_type']) ? $params['input_type'] : 'radio';
    $input_name = !empty($params['input_name']) ? $params['input_name'] : 'id_cat';
    $checked_value = isset($params['checked_value']) ? $params['checked_value'] : '';
    $type = isset($params['type']) ? $params['type'] : '';
    
    $smarty = new Smarty;
    $smarty->assign('data',$data);
    $smarty->assign('type',$type);
    $smarty->assign('input_type',$input_type);
    $smarty->assign('input_name',$input_name);
    $smarty->assign('checked_value',$checked_value);
    return $smarty->fetch($template);
}