<?php
function smarty_function_load_css($params)
{
	$html = NULL;
	
	if(!empty($params['file']))
	{
		$file = $params['file'];
		
		if(strpos($file,'http') !== FALSE)
		{
			$html = '<link href="'.$file.'" rel="stylesheet">';
		}
		else
		{
			$html = '<link href="'.BASE_URL.'_css/'.$file.'" rel="stylesheet">';
		}
	}
	
	return $html;
}