<?php

function smarty_modifier_get_age_from_date($date)
{
	$date = new DateTime($date);
	$today = new DateTime(date('Y-m-d'));
	$interval= $today->diff($date);
	return $interval->format('%Y');
}
