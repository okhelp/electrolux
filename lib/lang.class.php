<?php

class Lib__Lang
{
    /**
     * metoda zapisuje dane językowe(tłumaczeń modułu do bazy)
     * @param string $class_name
     * @param array $data
     */
    public static function save_lang_data($class_name,$id_item,$data)
    {
        //nakładam czujkę na występowanie klasy
        if(!class_exists($class_name))
        {
            trigger_error("Klasa $class_name nie istnieje. Błąd zapisu danych językowych");
        }
        
        //wykonuję operacje jeżeli mamy uzupełnione dane
        if(!empty($data))
        {
            foreach($data as $id_lang => $lang_data)
            {
                //sprawdzam czy tlumaczenie już istnieje
                $lang_item = $class_name::find(
                    $class_name::where("id_lang=$id_lang")->add("id_item=$id_item")
                );
                if(!empty($lang_item))
                {
                    $lang_item = reset($lang_item);
                }
                else
                {
                    $lang_item = new $class_name;
                }

                $lang_item->id_lang = $id_lang;
                $lang_item->id_item = $id_item;

                //odczytuje pola językowe
                foreach($lang_data as $name=>$value)
                {
                    $lang_item->{$name} = $value;
                }

                $lang_item->save();
            }
        }
    }
    
}