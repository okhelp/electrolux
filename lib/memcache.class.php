<?php

class Lib__Memcache
{   
	private static function is_enabled()
	{
		return 
			(defined('MEMCACHE_SERVER') && defined('MEMCACHE_PORT') && defined('MEMCACHE_EXPIRE')) ?
				TRUE : FALSE;
			
	}
	
    private static function connect() 
    {
		if(!self::is_enabled()) return;
		
        return array(
            'host' => MEMCACHE_SERVER,
            'port' => MEMCACHE_PORT
        );
    }
    
    /**
     * Metoda zwracająca nazwy klas, w których kluczem nie jest 
     * pełne zapytanie, ale numer id. Przydatna z uwagi na to, aby
     * ograniczyć czyszczenie cache po całej tabeli
     * @return array
     */
    public static function id_as_key()
    {
		if(!self::is_enabled()) return;
        return array(
            'Img'
        );
    }
    
    public static function get($key)
    {
		if(!self::is_enabled()) return;
        $memcache = new Memcached();
        $memcache->addServer(MEMCACHE_SERVER, MEMCACHE_PORT);
        return $memcache->get($key);//
    }
    
    public static function set($key,$value,$params=array())
    {
		if(!self::is_enabled()) return;

        $memcache = new Memcached();
        $memcache->addServer(MEMCACHE_SERVER, MEMCACHE_PORT);
        $memcache->set($key, $value, MEMCACHE_EXPIRE);
        
        $pdo = new Lib__PDO;
        $pdo->query("INSERT INTO `memcache_log` (`id`, `class_name`, `key_name`, `ts`) "
                . "VALUES (NULL, '{$params['class_name']}', '{$params['key_name']}'"
                . ", CURRENT_TIMESTAMP);");
    }
    
    public static function delete($key)
    {
		if(!self::is_enabled()) return;
        //wykluczam klucz Memcache_log%
        if(!strpos($key,"::Memcache_log") !== FALSE)
        {
            $key_type = 'ONE_ITEM';

            $memcache = new Memcached();
            $memcache->addServer(MEMCACHE_SERVER, MEMCACHE_PORT);

            //jeżeli ostatni znak do "*" to usuwam wszystkie klucze
            if(substr($key,-1) == "*")
            {
                $key_type = "ALL_ITEMS_ID";
            }
			
            switch($key_type)
            {
                case 'ONE_ITEM':
                    //szukam typu w bazie danych i usuwam
                    $pdo = new Lib__PDO;
                    $pdo->query("DELETE FROM memcache_log WHERE key_name='{$key}'");
					
                    //usuwam z memcache
                    $memcache->delete($key);
                break;

                case 'ALL_ITEMS_ID':
                    //odczytuję dane z przekazanego klucza
                    $key = str_replace(array('*','-'),'',$key);

                    //szukam powiązanych kluczy
                    $pdo = new Lib__PDO;
                    $find_keys = $pdo->query("SELECT * FROM memcache_log WHERE key_name LiKE '{$key}%'");
					
                    if(!empty($find_keys))
                    {
                        foreach($find_keys as $f_key)
                        {
                            Lib__Memcache::delete($f_key['key_name']);
                        }
                    }
                break;
            }
        }
    }
            
}