<?php

use App\ed\model\points\Get_users_points;
use App\front\models\auth\Verify_user;
use App\modules\products\front\models\Product_model;
use App\modules\products\front\objects\Products_main_menu;

/**
 * Klasa kontrolera framwrorka
 * @author Paweł Otłowski <p.otlowski@imset.it>
 */
class Lib__Base__Controller
{
	public $view;
	public $which_page;
	public $template;
	public $params;
	public $common_file = "common.tpl";
	public $session = array();
	public $breadcrumb = array();
    public $module_menu = array();
    public $page_title;
	public $metatags = array();
    protected $_id_service;
    protected $_user;
    
	public function __construct() 
	{
		$this->before();
	}
	
	/**
	 * Elementy w tej metodzie są wykonywane jako pierwsze
	 */
	public function before()
	{   
		$this->session = Lib__Session::get();
		$this->view = new Smarty;
        
        if(!empty($this->session))
        {
            $this->view->assign('session_data',$this->session);
        }
		
        
		//ładuję do widoku wążną zmienną $which_page określającą nazwę modułu
		$this->view->assign('which_page', $this->which_page);
		$this->view->assign('isContest', !empty(App__Ed__Model__Settings__Model::get('glowne')->data->is_contest));

        //ładuję id_service
        if(Lib__Session::get('id_service'))
        {
            $this->_id_service = (int)Lib__Session::get('id_service');
            $this->view->assign('service_data', App__Ed__Model__Service::find($this->_id_service));
        }

        //pobieram model użytkownika
        if(Lib__Session::get('id'))
        {
            $this->_user = App__Ed__Model__Users::find(Lib__Session::get('id'));
        }

        if(!is_ed() && $this->verify_availablity() && $this->is_not_login_page())
        {
            if(!empty($_SERVER['REQUEST_URI']))
            {
                $ref = substr($_SERVER['REQUEST_URI'],1);
                $ref = str_replace(substr(BASE_DIR,1),'',$ref);
                $ref = base64_encode($ref);
                $ref = urlencode($ref);

                redirect(BASE_URL . 'auth/login.html?ref=' . $ref);
            }
            else
            {
                redirect(BASE_URL . 'auth/login.html');
            }
        }
	}
	
	/**
	 * Elementy w tej metodzie są wykonywane na końcu
	 */
	public function after()
	{
		//czyszczenie flashsesji
		$this->remove_flash_session();
		
		//przekazywanie głównych ustawień
		$_settings = App__Ed__Model__Settings__Model::get('glowne')->data;
        $this->view->assign('_settings',$_settings);

		
		//przekazywanie ustawień social media
		$_settings_social_media = App__Ed__Model__Settings__Model::get('social_media');
		$this->view->assign('_settings_social_media',
			!empty($_settings_social_media->data) ? $_settings_social_media->data : NULL);

        //pobieram listę wszystkich języków
        $this->view->assign('languages_list',$this->get_languages_list());
		
        //dodawanie tytułu strony
		$this->view->assign('page_title',$this->page_title);
        $this->view->assign('site_title',$_settings->site_title);

        $this->view->assign('products_menu', Products_main_menu::get());

        if(!is_ed() && !empty($this->session['id']))
        {
            Lib__Session::set('user_points', App__Ed__Model__Points_log::getPoints($this->session['id']));
        }
	}
	
	public function __destruct()
	{
		$this->after();
		$this->view->assign('base_url',BASE_URL);
		
		//ładowanie okruszków do widoku
		$this->load_breadcrumb();
		
		//ładowanie metatagów
		$this->load_metatags();
		
        //ładuje nazwę wywołującego kontrolera
        if(!PRODUCTION)
        {
            if(Lib__Session::get('called_controler_name'))
            {
                Lib__Session::remove('called_controler_name');
            }
            Lib__Session::set('called_controler_name',get_called_class());
        }
        
		if(php_sapi_name() !== 'cli')
		{

		    $this->template = $this->get_template_path();

			if($this->template && $this->common_file)
			{

				$this->view->assign('template_file',$this->template);
				$view = Lib__Base__Html::prepare_output_html($this->view->fetch($this->common_file));
				echo "$view";

			}
			elseif($this->template && !$this->common_file)
			{
				$this->view->display($this->template);
			}
		}
		
	}

    private function verify_availablity()
    {
        return Verify_user::is_required_login();
    }

    private function is_not_login_page()
    {
        $uri = explode('?', $_SERVER['REQUEST_URI']);

        $uri[0] = str_replace(BASE_DIR, '/', $uri[0]);

        return $uri[0] != '/auth/login.html';
    }

    private function get_template_path()
    {
        return $this->template;
    }
	
	public function remove_flash_session()
	{
		if(isset($this->session['flash_session']))
		{
			Lib__Session::remove('flash_session');
		}
	}
	
	private function load_breadcrumb()
	{
		if(!empty($this->breadcrumb))
		{
			$this->view->assign('breadcrumb',$this->breadcrumb);
		}
	}
	
	private function load_metatags()
	{
		if(!is_ed())
		{
			$metatags = App__Ed__Model__Metatags::load($this->metatags);
			return $this->view->assign('metatags',$metatags);
		}
		
		return;
	}

    public function get_languages_list()
    {
        $return_list = array();

        $sql = "SELECT l.*, ll.name "
            . "FROM language l "
            . "INNER JOIN language_lang ll ON l.id = ll.language_id "
            . "WHERE ll.id_service = {$this->_id_service} "
            . "ORDER by ll.name ASC";

        $list = App__Ed__Model__Language::find_by_sql($sql);

        foreach($list as $item)
        {
            $return_list[$item->id] = $item;
        }

        return $return_list;
    }
}

