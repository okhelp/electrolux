<?php

class Lib__Base__Group extends Lib__Base__Args
{
	public function parse_ocr_data()
	{
		$return = NULL;
		
		//konweruje na stringa, tutaj nie uzywamy tablic
		$this->_args = trim(implode(' ',$this->_args));
		
		//jeżeli jest pusty string, bądż nie posiada separatora to robie wypad
		if(!empty($this->_args))
		{
			$return = array(
				'group' => trim($this->_args)
			);
		}
		return $return;
	}
}