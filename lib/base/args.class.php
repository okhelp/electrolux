<?php

class Lib__Base__Args
{
	protected $_args = array();
	protected $_table_name = NULL;
    
	public function __construct($attr=NULL) 
	{
        //zapobiegam przed pustymi wpisami
		if(!empty($attr))
        {
            $this->_args[] = $attr;
        }
	}
	
	public function get()
	{
		return $this;
	}
	
	public function add($attr)
	{
		$this->_args[] = $attr;
		return $this;
	}
    
    public function set_table_name($table_name)
    {
        $this->_table_name = $table_name;
    }
}