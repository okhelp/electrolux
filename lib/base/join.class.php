<?php

class Lib__Base__Join extends Lib__Base__Args
{
	public function parse_ocr_data()
	{
		$conditions_string = NULL;
		if(!empty($this->_args))
		{
			$joins_string = array("/* Lib__Base__Join */ ".implode(' ',$this->_args));
		}
		return !empty($joins_string) ? array('joins' => $joins_string) : NULL;
	}
}