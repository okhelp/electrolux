<?php

class Lib__Base__Limit extends Lib__Base__Args
{
	public function parse_ocr_data()
	{
		$return = NULL;
		
		//konweruje na stringa, tutaj nie uzywamy tablic
		$this->_args = trim(implode(' ',$this->_args));
		
		//jeżeli jest pusty string, bądż nie posiada separatora to robie wypad
		if(empty($this->_args) && strpos($this->_args,',') !== FALSE)
		{
			throw new Exception('Zapytanie limit powinno się składać z dwóch argumentów oddzielonych przecinkami, np. 0,10');
		}
		else
		{
			$limit_arr = explode(',',$this->_args);
			$return = array(
				'limit' => (int)trim($limit_arr[1]),
				'offset' => (int)trim($limit_arr[0])
			);
		}
		return $return;
	}
	
}
