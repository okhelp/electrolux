<?php

/**
 * Klasa modelu frameworka
 * @author Paweł Otłowski <p.otlowski@bluelab.pl>
 */
class Lib__Base__Model extends Lib__Base__Mysql
{
    protected $_id_service;
    static $memcache_prefix;
    
	static $where_conds = array();
	
	public function __construct ($attributes=array(), $guard_attributes=TRUE, $instantiating_via_find=FALSE, $new_record=TRUE)
	{
        $this->_id_service = !empty(Lib__Session::get('id_service')) ? Lib__Session::get('id_service') : NULL;
        
        parent::__construct ($attributes, $guard_attributes, $instantiating_via_find, $new_record);
	}
	
	
	public static function where($attr=NULL)
	{
		$where = new Lib__Base__Where($attr);       
		return $where;
	}
	
	public static function order($attr=NULL)
	{
		$order = new Lib__Base__Order($attr);
		return $order;
	}
	
	public static function limit($attr=NULL)
	{
		$limit = new Lib__Base__Limit($attr);
		return $limit;
	}
	public static function group($attr=NULL)
	{
		$where = new Lib__Base__Group($attr);
		return $where;
	}
	public static function join($attr=NULL)
	{
		$where = new Lib__Base__Join($attr);
		return $where;
	}
	
}
