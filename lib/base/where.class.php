<?php

class Lib__Base__Where extends Lib__Base__Args
{
	public function parse_ocr_data()
	{
		$conditions_string = NULL;
            
        if(!empty($this->_args))
		{
            //czyszczę puste wpisy            
            $this->_args = array_filter($this->_args);
            
			//aby mieć dokładna pewność, że jest to jeden warunek zamykam w nawias
			foreach($this->_args as $k=>$v)
            {   
				$this->_args[$k] = "($v)";
			}
            
			$conditions_string = array("/* Lib__Base__Where */ ".implode(' AND ',$this->_args));
		}
        
		return !empty($conditions_string) ? array('conditions' => $conditions_string) : NULL;
	}
    
    private function is_alias_added($value)
    {
        $value = explode('=',$value);
        $value = trim($value[0]);
        return strpos($value,".") !== FALSE;
    }
}