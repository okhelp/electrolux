<?php

class Lib__Base__Ed_Controller extends Lib__Base__Controller
{
	public $common_file = "common.tpl";
	public $module_menu = array();
    protected $_services_list;
	
    public function excluded_login_urls()
    {
        return [
            BASE_DIR . 'ed/login.html',
            BASE_DIR . 'ed/login/zmien_haslo.html',
            BASE_DIR . 'login/zmien_haslo.html',
        ];
    }
    
	public function before() 
	{
		parent::before();
		
		//ograniczenie dostępu - tylko dla admina
		if(is_ed() && (!App__Ed__Model__Users::is_logged() || !App__Ed__Model__Acl::has_access('site_managment',0)) && !in_array(preg_replace('/\?.*/', '', $_SERVER['REQUEST_URI']),$this->excluded_login_urls()))
		{
			if(strpos(uri_segment(2),'login') !== FALSE)
			{
                //nie wiedząc czemu !strpos nie dziala...
			}
			elseif(empty($_GET['ref']))
			{
                $ref = substr($_SERVER['REQUEST_URI'],1);
                $ref = str_replace(substr(BASE_DIR,1),'',$ref);
				$ref = base64_encode($ref);
				$ref = urlencode($ref);
                
                if(!empty($ref))
                {
                    $ref = "?ref=$ref";
                }
                
				$url =  "ed/login.html$ref";
				go_back('d|Dostęp zabroniony. Proszę sie zalogować.',$url);
			}
        }
        //sprawdzam czy użytkownik ma dostęp do tego serwisu
        elseif(!empty($this->_user) && !$this->_user->has_access_service())
        {
            show_403();     
        }
	}
	
	public function __construct() 
	{
		parent::__construct();
	}
	
	public function after()
	{
		parent::after();
		$this->generate_menu();
		$this->get_user_messages();
		
		//szablony sa w atalogu ed
		if(!empty($this->template))
		{
			$this->template = $this->template;
		}
        $this->view->assign('services_list', App__Ed__Model__Service::get_service());


		
	}
	
	public function generate_menu()
	{
		$this->view->assign('module_menu',App__Ed__Model__Menu::display_menu($this->module_menu));
	}

    private function get_user_messages()
    {
        $id_user = (int)Lib__Session::get('id');
        $page = !empty($_GET) && !empty($_GET['page']) ? (int)$_GET['page'] : 1;
        $per_page = 3;

        $list = new App__Ed__Model__Messages__List;
        $list->set_where("id_to = " . $id_user . " AND view_to = 0");
        $list->set_page($page);
        $list->set_per_page($per_page);
        $list->set_folder('inbox');
        $this->view->assign('messages', $list->execute());
    }
	


	
}