<?php

class Lib__Base__Html
{

	public static function prepare_output_html($html)
	{
		if (!is_ed())
		{
			//dodaję komunikat cookies
			$html = self::add_cookie_info($html);
			
			//przekładam css na góre nad tag css
			$html = self::prepare_css($html);

			//przekladam jsy na dol
			//$html = self::prepare_js($html);

			//koduję adresy e-mail
			$html = self::encode_emails($html);
			
			//dodaję kody śledzące
			$html = self::add_afiliant_code($html);

			//kompresowanie kodu html
			$html = self::minify_html($html);
		}

		return $html;
	}
	
	private static function add_cookie_info($html)
	{
		$settings = App__Ed__Model__Settings__Model::get('glowne')->data;
		
		if(isset($settings->cookie_info_status) && 
				!empty($settings->cookie_info_status) && 
				!empty($settings->cookie_info))
		{
			
			//dodaję komunikat
			$html_arr = explode("<body>",$html);
			$html = $html_arr[0]; 
			$html .= "<body>\n";
			$html .= '<div id="cookie_info">
				<div id="cookie_info_content">
					'.$settings->cookie_info.'
				</div>
				<a href="#"><i class="icon-filter_cross"></i></a>
			</div>'; 
			$html .= $html_arr[1];
			
			
			//dodaję css
			$html_arr = explode("</head>",$html);
			$html = $html_arr[0]; 
			$html .= '<link href="'.BASE_URL.'data/css/cookies_info.css" rel="stylesheet" />'."\n";
			$html .= "\n</head>" . $html_arr[1];
			
			//dodaję js
			$html_arr = explode('</body>',$html);
			$html = $html_arr[0];
			
			if(!strpos($html,'jquery.cookies.js') !== FALSE)
			{
				$html .= '<script src="'.BASE_URL.'data/js/jquery.cookies.js"></script>'."\n";
			}
			$html .= '<script src="'.BASE_URL.'data/js/cookies_info.js"></script>'."\n";
			$html .= "</body>\n";
			$html .= $html_arr[1];
		}
		
		return $html;
	}

	private static function encode_emails($html)
	{
		//koduję adresy e-mail
		preg_match_all('/[a-z\d._%+-]+@[a-z\d.-]+\.[a-z]{2,4}\b/i', $html, $found_emails);
		if (!empty($found_emails))
		{
			$found_emails = reset($found_emails);
			foreach ($found_emails as $email)
			{
				
				$html = str_replace($email, self::encode_email($email), $html);
			}
		}
		
		return $html;
	}
	
	private static function encode_email($str)
	{
		$str = mb_convert_encoding($str, 'UTF-32', 'UTF-8'); //big endian
		$split = str_split($str, 4);

		$res = "";
		foreach ($split as $c)
		{
			$cur = 0;
			for ($i = 0; $i < 4; $i++)
			{
				$cur |= ord($c[$i]) << (8 * (3 - $i));
			}
			$res .= "&#" . $cur . ";";
		}
		return $res;
	}

	private static function prepare_css($html)
	{
		/*		 * ************ <link ************* */
		preg_match_all('|\<link\ href=\"+(.*.css)+(.*\>)|m', $html, $matched);
		if (!empty($matched))
		{
			unset($matched[2]);
		}

		//tworze adres url
		foreach ($matched[1] as $k => $url)
		{
			if (strpos($url, BASE_URL) !== FALSE)
			{
				$matched[1][$k] = str_replace(BASE_URL, '/', $url);
			}
			else
			{
				unset($matched[1][$k]);
			}
		}
		
		$datetime = PRODUCTION ? strtotime(date('Y-m-d')) : time();
		
		//Jeżeli włączona jest optymalizacja kompresji css i js to podaję linki minify
		$minify_settings = App__Ed__Model__Settings__Model::get('optymalizacja');
		
		if(!empty($minify_settings) && !empty($minify_settings->data->compress_css_js))
		{
			$css_arr = $matched[1];
			
			//ograniczam w ilość minify css w linku do 5 plików
			if(count($css_arr) > 3)
			{
				
				//tworzę paczkę co 3 plików
				$css_package = array();
				
				$i = 0;
				
				foreach($css_arr as $k => $v)
				{
					$css_package[$i][] = $v;
					
					//zwiększam iterację paczki
					if(($k+1)%3 == 0)
					{
						$i++;
					}
				}
				
				//tworzę link
				$css_package_link = array();
				foreach($css_package as $package)
				{
					$css_package_link[] = '<link href="' . BASE_URL . '_combo/?f=' 
						. implode(',', $package) . '?' . $datetime. '" rel="stylesheet" />';
				}
				
				$matched[1] = implode("\n",$css_package_link);
			}
			else
			{
				$matched[1] = '<link href="' . BASE_URL . '_combo/?f=' 
						. implode(',', $matched[1]) . '?' . $datetime. '" rel="stylesheet" />';
			}
			
			
		}
		else
		{
			$matched_css = $matched[1];
			$matched[1] = NULL;
			foreach($matched_css as $css_url)
			{
				$css_url = substr($css_url,1) . '?' . $datetime;
				$matched[1] .= '<link href="' . BASE_URL . $css_url . '" rel="stylesheet" />'."\n";
			}
			
		}
		

		//rozbijam html
		$html_arr = explode('</title>', $html);
		$html_arr[0] .= "</title> \n";
		$html_arr[0] .= $matched[1]; //nasze linki jako pierwsze
		//łącze html
		$html = implode("\n", $html_arr);
		
		//przerzucam link na gore
		$html_arr[0] .= implode("\n", $matched[0]);


		//jeżeli są to nasze linki to usuwam
		foreach ($matched[0] as $html_link)
		{
			if (strpos($html_link, BASE_URL) !== FALSE)
			{
				$html = str_replace($html_link, '', $html);
			}
		}
		
		/*		 * ******* <style *********** */
		preg_match_all("/\<style(.*\<\/style\>)/is", $html, $matched);
		if (!empty($matched[0]))
		{
			$html = str_replace($matched[0], '', $html);

			$html_arr = explode('</head>', $html);
			$html_arr[0] .= "\n" . implode("\n", $matched[0]) . "\n </head>";
			$html = implode("\n", $html_arr);
		}

		return $html;
	}

	private static function prepare_js($template)
	{
		$datetime = PRODUCTION ? strtotime(date('Y-m-d')) : time();
		$js_list = array();
		$remote_js = array();

		$js_url = array();

		//szukam tagów htmla
		preg_match_all("/<script[^>]+?src[^>]*?><\/script>/i", $template, $template_html);
		
		if (!empty($template_html))
		{
			$template_html = reset($template_html);
			
			//wszystkie przed jquery przenosze na dół
			$script_extension = array();
			foreach($template_html as $k => $script)
			{
				if(strpos($script,'jquery') !== FALSE)
				{
					break;
				}
				else
				{
					$script_extension[] = $script;
					unset($template_html[$k]);
				}
			}
			
			if(!empty($script_extension))
			{
				$template_html = array_merge($template_html,$script_extension);
			}
			
			foreach ($template_html as $html)
			{
				if (strpos($html, $_SERVER['HTTP_HOST']) !== FALSE)
				{
					$html = str_replace("'", '"', $html);

					//pobieram nazwę pliku
					preg_match('~(?<=src=")[^"]+\.js~', $html, $js_preg);

					$js_file = reset($js_preg);
					$js_file = str_replace(BASE_URL, '', $js_file);

					$js_url[] = $js_file;

					//usuwam z html ten tag
					$template = str_replace($html, "", $template);
				}
				else
				{
					$remote_js[] = $html;
					$template = str_replace($html, '', $template);
				}
			}
					
			//dodaję do htmla tag z js
			$first_part = NULL;
			$template_arr = explode('</body>', $template);
			
			$minify_settings = App__Ed__Model__Settings__Model::get('optymalizacja');
		
			if(!empty($minify_settings) && !empty($minify_settings->data->compress_css_js))
			{
				//ograniczam w ilość minify css w linku do 5 plików
				if(count($js_url) > 3)
				{

					//tworzę paczkę co 5 plików
					$js_package = array();

					$i = 0;

					foreach($js_url as $k => $v)
					{
						if(strpos($v,"?") !== FALSE)
						{
							$v = explode("?",$v);
							$v = reset($v);
						}
						$js_package[$i][] = $v;

						//zwiększam iterację paczki
						if(($k+1)%3 == 0)
						{
							$i++;
						}
					}

					//tworzę link
					$js_package_link = array();
					foreach($js_package as $package)
					{
						$js_package_link[] = '<script src="' . BASE_URL . '_combo/?f=/' 
								. implode(',/', $package) . '?' . $datetime . '"></script>' . "\n";
					}

					$first_part .= implode("\n",$js_package_link);
				}
				else
				{
					$first_part .= '<script src="' . BASE_URL . '_combo/?f=/' 
							. implode(',/', $js_url) . '?' . $datetime . '"></script>' . "\n";
				}


			}
			else
			{
				foreach($js_url as $file)
				{
					$first_part .= '<script src="' . BASE_URL . $file .'"></script>' . "\n";
				}
			}
			
			if(!empty($remote_js))
			{	
				$first_part .= implode("\n",$remote_js);
			}
			
			$template = $template_arr[0] . $first_part . '</body>' .$template_arr[1];
		}

		return $template;
	}

	private static function get_before_js()
	{
		$smarty = new Smarty;
		return $smarty->fetch('_partials/footer/before_js.tpl');
	}
	
	private static function add_afiliant_code($html)
	{
		$settings =  App__Ed__Model__Settings__Model::get('kody_sledzace');
		
		if(!empty($settings))
		{
			$settings = $settings->data;
			
			//przed </head>
			if(!empty($settings->end_head))
			{
				$html_arr = explode("</head>",$html);
				$html_arr[0] .= "\n" . $settings->end_head . "\n </head>";
				$html = implode("\n",$html_arr);
			}
			
			//po <body>
			if(!empty($settings->start_body))
			{
				$html_arr = explode("<body>",$html);
				$html_arr[0] .= "\n <body> \n" . $settings->start_body ;
				$html = implode("\n",$html_arr);
			}
			
		}
		
		return $html;
	}
	
	private static function minify_html($html)
	{
		$settings = App__Ed__Model__Settings__Model::get('optymalizacja');
		if(!empty($settings))
		{
			$settings = $settings->data;
			
			if(!empty($settings) && !empty($settings->compress_html))
			{
				$search = array('/\>[^\S ]+/s','/[^\S ]+\</s','/(\s)+/s');
				$replace = array('>','<','\\1');
				if (preg_match("/\<html/i",$html) == 1 && preg_match("/\<\/html\>/i",$html) == 1) 
				{
					$html = preg_replace($search, $replace, $html);
				}
			}
			
		}
		
		return $html;
	}

}
