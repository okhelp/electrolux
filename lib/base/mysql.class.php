<?php


$database = array(
    'connection' => 'local',
    'local' => 'mysql://'.DB_USERNAME.':'.DB_PASSWORD.'@'.DB_HOST.'/'.DB_NAME.';charset=utf8',
    'server' => 'mysql://username:password@localhost/database?charset=utf8'
);

if(defined('MEMCACHE_SERVER') && defined('MEMCACHE_PORT') && defined('MEMCACHE_EXPIRE'))
{
	//$database['memcache'] = 'memcache://'.MEMCACHE_SERVER.':'.MEMCACHE_PORT;
}

ActiveRecord\Config::initialize(function($config) use ($database) {
    $config->set_model_directory(dirname(dirname(dirname(__FILE__))).'/model/');
    $config->set_connections($database);
    $config->set_default_connection($database['connection']);
	
	//tylko dla produkcji włączam cache
	if(PRODUCTION && !empty($database['memcache']))
	{
		$config->set_cache(
				$database['memcache'], array(
					'namespace' => '_' . Lib__Session::get('service_name'), 
					'expire' => ((PRODUCTION && defined('MEMCACHE_EXPIRE')) ? MEMCACHE_EXPIRE : 1)
					)
				);
	}
});

/**
 * klasa odpowiedzialna za połączenie z bazą danych, rozszerzenie phpactiverecords (ORM)
 * @author Paweł Otłowski <p.otlowski@bluelab.pl>
 */
class Lib__Base__Mysql extends ActiveRecord\Model
{
	static $table_name;
	static $inner_table_name;
	
	public function __construct ($attributes=array(), $guard_attributes=TRUE, $instantiating_via_find=FALSE, $new_record=TRUE)
	{
		parent::__construct ($attributes, $guard_attributes, $instantiating_via_find, $new_record);
	}
	
	public function debug()
	{
		echo '<pre>';
		print_r(self::connection()->last_query);
		echo '</pre>';
	}
		
}



