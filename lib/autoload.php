<?php

define('PHP_ACTIVERECORD_VERSION_ID','1.0');


/**
 * Automatyczne ładowanie plików konfiguracyjnych
 */
$Directory = new RecursiveDirectoryIterator(dirname(dirname(__FILE__))."/settings/");
$Iterator = new RecursiveIteratorIterator($Directory);
$Regex = new RegexIterator($Iterator, '/^.+\.php$/i', RecursiveRegexIterator::GET_MATCH);

foreach($Regex as $files)
{
	foreach($files as $file)
	{
		require_once $file;
	}
}

require BASE_PATH . 'vendor/autoload.php';

// --------------------------------------------------------------

/**
 * Automatyczne ładowanie klas frameworka
 */
spl_autoload_register(function($className)
{
	//ładowanie smartów - pomijam ich ładowanie
	if(strpos(strtolower($className),'smarty') !== FALSE) 
	{
		return;
	}
	
	//ładowanie pozostałych biblotek frameworka
	else
	{
		$className = strtolower($className);
		$className = str_replace('__','/',$className);
		$class = BASE_PATH."{$className}.class.php";
		
		//jezeli robumy odwołanie do modelu to sprawdzamy jeszcze w katalogu model
		if(!is_file($class))
		{
			$className = "model/".$className;
			$class_model = BASE_PATH."{$className}.class.php";
		}

        
		//ładuję klasę, która istnieje
		if(is_file($class))
		{			
			require_once ($class);
		}
		elseif (isset($class_model) && is_file($class_model))
		{
			require_once ($class_model);
		}
	}
});

// ---------------------------------------------------------------

/**
 * Automatyczne ładowanie helperów
 */
$Directory = new RecursiveDirectoryIterator(BASE_PATH."helpers/");
$Iterator = new RecursiveIteratorIterator($Directory);
$Regex = new RegexIterator($Iterator, '/^.+\.php$/i', RecursiveRegexIterator::GET_MATCH);

foreach($Regex as $files)
{
	foreach($files as $file)
	{
		require_once $file;
	}
}

// ---------------------------------------------------------------

/**
 * Ustawienia dla CLI
 */
if(php_sapi_name() === 'cli')
{
    $pdo = new Lib__PDO();

    //pobieram dane pierwszego serwisu
    $service_data = $pdo->query("SELECT * FROM service ORDER by id ASC LIMIT 1");
    if(!empty($service_data))
    {
        $service_data = reset($service_data);

        $_SERVER['REQUEST_URI'] = BASE_DIR;
        $_SERVER['HTTP_HOST'] = $service_data['domain'];

        $_SESSION = [
            'id_service' => $service_data['id'],
            'id_lang' => $service_data['id_lang'],
            'service_name' => $service_data['name'],
            'template' => $service_data['template']
        ];
    }
    else
    {
        throw new Exception('Brak zdefiniowanego serwisu w systemie.');
    }
}

/**
 * ORM
 */

if (!defined('PHP_ACTIVERECORD_AUTOLOAD_PREPEND'))
{
	define('PHP_ACTIVERECORD_AUTOLOAD_PREPEND',true);
}
require __DIR__.'/active_records/Singleton.php';
require __DIR__.'/active_records/Config.php';
require __DIR__.'/active_records/Utils.php';
require __DIR__.'/active_records/DateTimeInterface.php';
require __DIR__.'/active_records/DateTime.php';
require __DIR__.'/active_records/Model.php';
require __DIR__.'/active_records/Table.php';
require __DIR__.'/active_records/ConnectionManager.php';
require __DIR__.'/active_records/Connection.php';
require __DIR__.'/active_records/Serialization.php';
require __DIR__.'/active_records/SQLBuilder.php';
require __DIR__.'/active_records/Reflections.php';
require __DIR__.'/active_records/Inflector.php';
require __DIR__.'/active_records/CallBack.php';
require __DIR__.'/active_records/Exceptions.php';
require __DIR__.'/active_records/Cache.php';

require __DIR__.'/ssp.class.php';

// ---------------------------------------------------------------

//pobieram nazwę domeny
if (PRODUCTION) {
    $domain = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : 0;
    $domain = str_replace('www.','',$domain);
} else {
    $domain = 'dev.imset.it';
}

/**
 * Ładowanie serwisu wraz z językiem
 */
$id_service = !empty(Lib__Session::get('id_service')) ? Lib__Session::get('id_service') : 0;

if(empty($id_service))
{
    if($domain)
    {
        //pobieram dane serwisu
        $service_where = App__Ed__Model__Service::where("domain LIKE '%$domain'");
        $service_data = App__Ed__Model__Service::find($service_where);
        
        $service = reset($service_data);

        if(!empty($service))
        {	
            //ustawiam w sesji dane
            Lib__Session::set('id_service',$service->id);
            Lib__Session::set('default_lang',$service->id_lang);
            Lib__Session::set('service_name',$service->name);
            
            //ustawiam prefix memcache
            if(!defined("MEMCACHE_PREFIX") && !empty($service->memcache_prefix))
            {
                define("MEMCACHE_PREFIX",$service->memcache_prefix);
            }
        }
        else //nie znalazlem jezyka ustawiam 404
        {
            show_404();
        }
    }
}
//jeżeli nie ma w sesji języka ustawiam domyślny
if($domain && !Lib__Session::get('id_lang'))
{
	//pobieram jaki język przypisany jest do serwisu
	$connected_id_lang = App__Ed__Model__Service::find(Lib__Session::get('id_service'))->id_lang;

	//sprawdzam czy język jest w bazie
	$find_language = App__Ed__Model__Language::find($connected_id_lang);
	
	if(!empty($find_language))
	{
		App__Ed__Model__Language__Model::set_lang($connected_id_lang);
	}
	else
	{
		throw new Exception("Brak dostępnego języka o id {$this->_service->id_lang} dla "
		. $find_language->domain);
	}
}

//pobieram tłumaczenia
if(
	(Lib__Session::get('id_lang') && !Lib__Session::get('lang')) || 
	(Lib__Session::get('lang_data') != get_last_modification_dir(
		App__Ed__Model__Language__Model::get_folder_path(Lib__Session::get('lang_code'))))
)
{
	App__Ed__Model__Language__Model::actualize_session_translate(Lib__Session::get('id_lang'));
}

$service_name = Lib__Session::get('service_name');

//pobieram nazwę templatki
if(!empty(Lib__Session::get('id_service')))
{
	$service = App__Ed__Model__Service::find(Lib__Session::get('id_service'));
	if(!empty($service))
	{
		Lib__Session::set('template',$service->template);
	}
}

// sprawdzam czy wlaczony jest ssl (wykluczam cli)
if(PRODUCTION && isset($service) && !empty($service) && 
        !empty($service->ssl_enabled) && php_sapi_name() !== 'cli')
{
    $ssl_enabled = TRUE;
    
    //jeżeli w adresie nie ma https robię przekierowanie
    if (! isset($_SERVER['HTTPS']) or $_SERVER['HTTPS'] == 'off' ) 
    {
        $redirect_url = "https://" . $_SERVER['HTTP_HOST'] 
            . $_SERVER['REQUEST_URI'];
        header("Location: $redirect_url");
        exit();
    }
}
else
{
    $ssl_enabled = FALSE;
}


//ustawiam base url 
if(isset($_SERVER['HTTP_HOST'])) //poprzez cli zmienna jest niedostępna
{
    $http = $ssl_enabled ? 'https://' : 'http://';
	define('BASE_URL',$http.$_SERVER['HTTP_HOST'].BASE_DIR);
}

if(defined('BASE_URL'))
{
    define('BASE_ED_URL',BASE_URL.'ed/');
}

// ---------------------------------------------------------------

/**
 * Smarty 3
 */

if(!empty($service_name))
{
	$template_name = Lib__Session::get('template');

    //konfiguracja smarty
    if(is_ed())
    {
        $smarty_template_dir = BASE_PATH."app/ed/templates/";
        $smarty_tmp_dir = BASE_PATH."cache/ed/smarty/";
    }
    else
    {
        $smarty_template_dir = BASE_PATH."app/front/templates/$template_name/";
        $smarty_tmp_dir = BASE_PATH."cache/$template_name/smarty/";
    }



    
    if(!is_dir($smarty_template_dir))
    {
        trigger_error("Brak katalogu z template dla $template_name",E_USER_ERROR);
    }
    
    if(!is_dir($smarty_tmp_dir))
    {
        create_dirs($smarty_tmp_dir);
    }
    
    define('SMARTY_TEMPLATE_DIR',$smarty_template_dir);
    define('SMARTY_TMP_TEMPLATE_DIR',$smarty_tmp_dir);

    require_once BASE_PATH.'lib/smarty/Smarty.class.php';
    require_once BASE_PATH.'lib/smarty/Autoloader.php';
    
    //konfuguracja cache combo
    $min_cachePath = BASE_PATH."cache/$template_name/combo/";
    if(!is_dir($min_cachePath)) //jeżeli nie ma takiego katalogu to go tworzę.
    {
        create_dirs($min_cachePath);
    }
}

// ---------------------------------------------------------------
