<?php

class Lib__PDO
{
    public $pdo;
    
    public function __construct() 
    {
        //ustanawiam połącznie z bazą danych
        $this->set_connection();
    } 
    
    private function set_connection()
    {
        $this->pdo = 
		new PDO('mysql:host='.DB_HOST.';charset=utf8;dbname='.DB_NAME, DB_USERNAME, DB_PASSWORD);
		$this->pdo->setAttribute(PDO::ATTR_ERRMODE, true);
    }
    
    public function query($sql_query)
    {
        $execute_query = $this->pdo->prepare($sql_query);
		
		$execute_query->execute();
		
        
        //dla selectow pokazuje tablice
        if(strpos($sql_query,'SELECT') === 0)
        {
            $execute_query = $execute_query->fetchAll(PDO::FETCH_ASSOC);
        }
        
        return $execute_query;
    }
	
	public function insert($table_name, $data)
	{
		$values_bind_params = array();
		if(isset($data['id'])) unset($data['id']);
		foreach(array_keys($data) as $v)
		{
			$v = str_replace('`','',$v);
			$values_bind_params[] = ":" . $v;
		}

		$sql = $this->pdo->prepare("INSERT INTO $table_name (".implode(',',array_keys($data)).") "
				. "VALUES(".implode(", ", $values_bind_params).");");
		foreach ($data as $k => $v)
		{
			$k = str_replace('`','',$k);
			$sql->bindValue(":$k", $v, PDO::PARAM_STR);
		}
		
		
		$sql->execute();
	}
}