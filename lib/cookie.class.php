<?php

class Lib__cookie
{
	public static function get($k)
	{
		return !empty($_COOKIE[$k]) ? $_COOKIE[$k] : NULL;
	}
	
	public static function set($name,$value,$time=0)
	{
        //domyślne "życie" ciastka to 1 rok
        if($time == 0)
        {
            $time = 60*60*24*365;
        } 
		return setcookie($name,$value,time()+$time, "/",$_SERVER['HTTP_HOST']);
	}
    
    public static function remove($name)
    {
        $_COOKIE[$name] = '';
        setcookie($name, "", time()-3600, "/",$_SERVER['HTTP_HOST']);
    }
}