<?php

/**
 * Klasa odpowiedzialna przed zabezepieczeniem frameworka przed atakami
 */
class Lib__Security
{
	/**
     * funkcja haszujaca (md5+sol) zwracajaca skrot okreslonej dlugosci
     *
     * @param mixed $args ciag znakow lub tablica
     * @param int $length dlugosc
     * @return string
     * @author Marcin Reducha
     */
    public static function md5_part($args, $length = 32)
    {
        //zeby ktos nie odgadl parametrow, nie zmieniac!
        define('MD5_PART_SALT', 'marcin___fjdljflsf939393939393 3993320 222');
        $lenght = ($length > 32) ? 32 : $length;

        if (is_array($args))
        {
            foreach ($args as $arg)
            {
                $string .= $arg;
            }
        }
        else
        {
            $string = $args;
        }

        return substr(md5(MD5_PART_SALT . $string), 0, $length);
    }


    /**
     *  Sanityzacja podanej wartosci:
     *
     * - Strips slashes if magic quotes are enabled
     * - Normalizes all newlines to LF
     *
     * @param   mixed
     * @return  mixed  sanityzowana wartosc
     */
    public static function sanitize($data)
    {
        if (is_array($data))
        {
            foreach ($data as $key => $val)
            {
                $data[$key] = self::sanitize($val);
            }
        }
        elseif (is_string($data))
        {
            if (strpos($data, "\r") !== FALSE)
            {
                // standaryzuje nowe linie
                $data = str_replace(array("\r\n", "\r"), "\n", $data);
            }
        }

        return $data;
    }

    // sanityzacja tablic globalnych
    public static function sanitize_gpc()
    {
        $_GET = self::sanitize($_GET);
        $_POST = self::sanitize($_POST);
        $_COOKIE = self::sanitize($_COOKIE);
    }

    /**
     * Generuje pseudo-losowy ciag znakow
     *
     * @param string $type
     * @param int $length
     * @return string
     */
    public static function random_text($type = 'alnum', $length = 32)
    {
        switch ($type)
        {
            case 'alnum':
                $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                break;
            case 'alpha':
                $pool = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                break;
            case 'hexdec':
                $pool = '0123456789abcdef';
                break;
            case 'numeric':
                $pool = '0123456789';
                break;
            case 'nozero':
                $pool = '123456789';
                break;
            case 'distinct':
                $pool = '2345679ACDEFHJKLMNPRSTUVWXYZ';
                break;
            default:
                $pool = (string) $type;
                break;
        }

        $pool = str_split($pool, 1);

        $max = count($pool) - 1;

        $str = '';
        for ($i = 0; $i < $length; $i++)
        {
            $str .= $pool[mt_rand(0, $max)];
        }

        if ($type === 'alnum' AND $length > 1)
        {
            if (ctype_alpha($str))
            {
                $str[mt_rand(0, $length - 1)] = chr(mt_rand(48, 57));
            }
            elseif (ctype_digit($str))
            {
                $str[mt_rand(0, $length - 1)] = chr(mt_rand(65, 90));
            }
        }

        return $str;
    }



    /**
     *  Sprawdza czy wartość żądania nie zawiera niebezpiecznych fraz które wskazywałyby na atak
     *
     *  @param string $value
     *  @param string $type    
     *  @return boolean          
     */             
    public static function check_injection($value, $type='')
    {
        $value = rawurldecode($value);
       
        switch($type)
        {
            case 'uri':
                if (preg_match("/(^|[^a-zA-Z])union/i", $value) && preg_match("/(^|[^a-zA-Z])select/i", $value))
                {
                    throw new Exception('blind_injection: (select/union) wykryto niedozwolony ciag znakow: '.$value); 
                }
                if(strstr($value , "<script" ))
                {
                    throw new Exception('blind_injection: (script) wykryto niedozwolony ciag znakow: '.$value);
                }
            break;

            default:                                                                                             
                if (    stristr($value, '(')
                    && !stristr($value, 'utmcct') // (utmcct) - fix dla ciasteczka google
                    && (
                        ( stristr($value, 'delete') && stristr($value, 'from') )
                        ||
                        (preg_match('#select([\s\v\*]+)#', $value) && stristr($value, 'from'))
                        ||
                        stristr ($value, 'sleep(')
                    )
                ) 
                {
                    throw new Exception('blind_injection_1:  wykryto niedozwolony ciag znakow: ' . $value);
                }

                if (    stristr($value, '<')
                    && (
                        (stristr($value, 'delete') && stristr($value, 'from'))
                        || preg_match('#select([\s\v\*]+)#', $value)
                        || stristr ($value, 'sleep(')
                    )
                )
                {
                    throw new Exception('blind_injection_2: wykryto niedozwolony ciag znakow: ' . $value);
                }

                if (    stristr($value, '>')
                    && (
                        (stristr($value, 'delete') && stristr($value, 'from'))
                        || preg_match('#select([\s\v\*]+)#', $value)
                        || stristr ($value , 'sleep(')
                    )
                )
                {
                    throw new Exception('blind_injection_3: wykryto niedozwolony ciag znakow: ' . $value);
                }

                if (strpos($value, '/*')!==false)
                {
                    if (preg_match("/\/\*(.*?)\*\//m", $value))
                    {
                        setcookie("__utmz", "", time() - 3600);    // fix - przed googlowskim cookie  ????!!!!
                        throw new Exception('blind_injection_4: wykryto niedozwolony ciag znakow: ' . $value);
                    }
                }

                // warianty frazy char(124)
                if (preg_match('#char([\s\v])*\(([\s\v])*124#i', $value))
                {
                    throw new Exception('blind_injection_5: wykryto niedozwolony ciag znakow: ' . $value);
                }
            break;              
        }
        return true;               
    }
   

    /**
     *  Czyszczenie wartości z requesta - odkodowanie i usunięcie zbędnych wartości
     *      
     *  @param string $request_value
     *  @param string $request_key
     *  @return string         
     */             
    public static function sanitize_request(&$request_value, $request_key)
    {
        //formularze edycji ogłoszenia
        //jezeli pole konczy sie na _dst (disable strip tags) nie uzywamy strip_tags
        if(preg_match('/^(.)*_dst$/', $request_key ))
        {
            $request_value = rawurldecode($request_value);
        }
        else
        {
            $allowable_tags = null;
            if(defined('SECURITY_REQUEST_ALLOWED_TAGS'))
            {
                $allowable_tags = SECURITY_REQUEST_ALLOWED_TAGS;
            }
            $request_value = strip_tags(rawurldecode($request_value), $allowable_tags);
        }
    }       


    /**
     * generuje haslo bedace zlepkiem sylab, ew. mogace zawierac cyfry
     *
     * @param int $length - dlugosc hasla
     * @param bool $with_numbers - przelacznik dla stosowania cyfr
     */
    public static function generate_syllabic_password($length=10, $with_numbers=true)
    {
        // samogloski
        $vowel = str_split('aeiouy');
        // spolgloski
        $consonant = str_split('bcdfghjklmnprstwz');
        // cyfry
        $number = $with_numbers ? range(0, 9) : array('');
        // epsilon
        $epsilon = array('');

        // maszyna stanow
        $sm = array(
                0 => array('vowel' => 1, 'consonant' => 2),
                1 => array('consonant' => 2),
                2 => array('vowel' => 3),
                3 => array('number' => 4, 'consonant' => 2),
                4 => array('number' => 0, 'epsilon' => 0)
        );
        // stan poczatkowy - tak aby pierwszym znakiem byla samogl. lub spolgl.
        $curr_state = 0;

        $passwd = '';
        while (strlen($passwd) < $length)
        {
            $trans = $sm[$curr_state];
            $draw = mt_rand(0, count($trans) - 1);

            $pools = array_keys($trans);
            // zasobnik z elementami
            $pool = ${$pools[$draw]};

            // losowanie z zasobnika i dodanie do hasla
            $passwd .= $pool[mt_rand(0, count($pool) - 1)];
           
            // przeskok do nastepnego stanu
            $states = array_values($trans);
            $curr_state = $states[$draw];
        }

        return $passwd;
    }

   
    /**
     * Szyfruje słowo kluczem symetrycznym
     * @autor Daniel Kudełka
     * @param string $string
     * @return string
     */
    public static function encode_string($string)
    {
        $repeat = ceil(strlen($string)/16);
        $order = self::_encode_string_order($repeat);
        $parts = self::_encode_string_parts($repeat);

        $string_letters = str_split($string);
        $encoded_string = array();
        foreach ($order as $key=>$o)
        {
            if (isset($string_letters[$key]))
                $encoded_string[$o] = $string_letters[$key].$parts[$o];
            else
                $encoded_string[$o] = '*'.$parts[$o];
        }
        ksort($encoded_string);
        $encoded_string = implode($encoded_string);
       
        return $encoded_string;
    }
    /**
     * Odszyfrowuje słowo kluczem symetrycznym
     * @autor Daniel Kudełka
     * @param string $string
     * @return string
     */
    public static function decode_string($string)
    {
        $repeat = strlen($string);
        $order = self::_encode_string_order($repeat);
       
        $string_letters = str_split($string);

        $decoded_string = '';
        foreach ($order as $key=>$o)
        {
            $s = $string_letters[$o*3];
            if ($s!='*')
                $decoded_string .= $s;
        }
       
        return $decoded_string;
    }
    private static function _encode_string_order($repeat)
    {
        $o = array(
            0, 5, 4, 7, 9, 10, 3, 11, 2, 12, 1, 13, 6, 8, 14, 15
        );
        $shift_count = date('d'+13);
        for($i=0;$i<=$shift_count;$i++)
        {
            $shifted = array_shift($o);
            $o[] = $shifted;
        }

        $order = $o;
       
        for($i=0;$i<$repeat;$i++)
        {
            foreach ($o as $k=>$v)
                $o[$k] += 16;

            $order = array_merge($order, $o);
            $a[] = $o;
        }
       
        return $order;
    }
    private static function _encode_string_parts($repeat)
    {
        $md5 = md5(session_id());
        $p = array();
        for($i=0;$i<=15;$i++)
            $p[] = substr($md5, $i, 2);
           
        $parts_order = $p;

        for($i=0;$i<$repeat;$i++)
            $parts_order = array_merge($parts_order, $p);
       
        return $parts_order;
    }

    // sprawdza czy dane wejsciowe sa bezpieczne
    // jak nie to je zastepuje pustym stringiem
    public static function secure_http_headers($data)
    {
        foreach ($data as $key=>$value)
        {
            try
            {
                // slabsza walidacja bo tu sa znaki komentarzy /*
                if ($key == 'HTTP_ACCEPT')
                {
                    self::check_injection($value, 'uri');
                }
                elseif (strpos($key, 'HTTP_') !== false && $key!= 'HTTP_COOKIE')
                {
                    self::check_injection($value);
                }
            }
            catch (Exception $e)
            {
                //unset:
                $data[$key] = '';
            }
        }
		
        return $data;
    }
   
    public static function urlencode($data)
    {
        if (is_array($data))
        {
            foreach ($data as $key => $val)
            {
                $data[$key] = self::urlencode($val);
            }
        }
        elseif (is_string($data))
        {

            $data = rawurlencode($data);
        }

        return $data;
    }
   
    public static function htmlentities($data, $flags = NULL, $encoding = NULL, $double_encode = true)
    {
        $flags = $flags === NULL ?  ENT_COMPAT|ENT_HTML401 : $flags;
        $encoding = $encoding === NULL ?  ini_get("default_charset"): $encoding;
       
        if (is_array($data))
        {
            foreach ($data as $key => $val)
            {
                $data[$key] = self::htmlentities($val, $flags, $encoding, $double_encode);
            }
        }
        else
        {
            $data = htmlentities((string)$data, $flags, $encoding, $double_encode);
        }

        return $data;
    }
	
	public static function secure_uri()
	{
		$uri = isset($_SERVER['REQUEST_URI']) && !empty($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : '';
		
		if(!empty($uri))
		{
			self::check_injection($uri,'uri');
		}
	}
}