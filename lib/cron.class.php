<?php

class Lib__Cron
{
    
    /**
     * Ustawiam flagę na rozpoczęcie crona
     * @param string $name Nazwa crona
     * @param int $duration częstotliwość wykonywania crona [min.]
     * @param string $admin_login login odpowiedzialnego programisty
     * @author Paweł Otłowski
     */
    public static function init($name,$duration,$admin_login)
    {
        $filename = $_SERVER['SCRIPT_FILENAME'];
        
        //szukam czy taki cron już istnieje
        $find_cron = App__Ed__Model__Cron::find_by_filename($filename);
        if(!empty($find_cron))
        {
            //jeżeli zmienił się opis i czas robię aktualizacje
            if($duration != $find_cron->duration || $name != $find_cron->name || 
                    $admin_login != $find_cron->admin_login)
            {
                $find_cron->duration = $duration;
                $find_cron->name = $name;
                $find_cron->admin_login = $admin_login;
                $find_cron->save();
            }
            
            $id_cron = $find_cron->id;
        }
        else //nie ma takiego crona to dodaję go do systemu
        {
            $item = new App__Ed__Model__Cron;
            $item->name = $name;
            $item->filename = $filename;
            $item->duration = $duration;
            $item->admin_login = $admin_login;
            $item->save();
            
            $id_cron = $item->id;
        }
        
        //dodaję wpis do loga o rozpoczęciu wykonania crona
        $log = new App__Ed__Model__Cron__Log;
        $log->id_cron = $id_cron;
        $log->start = time();
        $log->status = 0;
        $log->save();
    }
    
    public static function end()
    {
        $filename = $_SERVER['SCRIPT_FILENAME'];
        $cron = App__Ed__Model__Cron::find_by_filename($filename);
        if(!empty($cron))
        {
            //pobieram ostatniego loga crona
            $cron_log = App__Ed__Model__Cron__Log::find(
                App__Ed__Model__Cron__Log::where("id_cron=".$cron->id)->add("status=0")->add("stop = ''"),
                App__Ed__Model__Cron__Log::order("id DESC")
            );
           
            if(!empty($cron_log))
            {
                $cron_log = reset($cron_log);
                $cron_log->stop = time();
                $cron_log->status = 1;
                $cron_log->save();
            }
        }
    }
}