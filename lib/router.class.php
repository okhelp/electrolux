<?php

use App\modules\products\front\models\Product_category_model;

/**
 * http://altorouter.com/
 */
class Lib__Router
{

	/**
	 * @var array Array of all routes (incl. named routes).
	 */
	protected $routes = array();

	/**
	 * @var array Array of all named routes.
	 */
	protected $namedRoutes = array();

	/**
	 * @var string Can be used to ignore leading part of the Request URL (if main file lives in subdirectory of host)
	 */
	protected $basePath = '';

	/**
	 * @var array Array of default match types (regex helpers)
	 */
	protected $matchTypes = array(
		'i' => '[0-9]++',
		'a' => '[0-9A-Za-z]++',
		'h' => '[0-9A-Fa-f]++',
		'*' => '.+?',
		'**' => '.++',
		'' => '[^/\.]++'
	);

	/**
	 * Create router in one call from config.
	 *
	 * @param array $routes
	 * @param string $basePath
	 * @param array $matchTypes
	 */
	public function __construct($routes = array(), $basePath = '', $matchTypes = array())
	{
		$this->addRoutes($routes);
		$this->setBasePath($basePath);
		$this->addMatchTypes($matchTypes);
	}

	/**
	 * Retrieves all routes.
	 * Useful if you want to process or display routes.
	 * @return array All routes.
	 */
	public function getRoutes()
	{
		return $this->routes;
	}

	/**
	 * Add multiple routes at once from array in the following format:
	 *
	 *   $routes = array(
	 *      array($method, $route, $target, $name)
	 *   );
	 *
	 * @param array $routes
	 * @return void
	 * @author Koen Punt
	 * @throws Exception
	 */
	public function addRoutes($routes)
	{
		if (!is_array($routes) && !$routes instanceof Traversable)
		{
			throw new \Exception('Routes should be an array or an instance of Traversable');
		}
		foreach ($routes as $route)
		{
			call_user_func_array(array($this, 'map'), $route);
		}
	}

	/**
	 * Set the base path.
	 * Useful if you are running your application from a subdirectory.
	 */
	public function setBasePath($basePath)
	{
		$this->basePath = $basePath;
	}

	/**
	 * Add named match types. It uses array_merge so keys can be overwritten.
	 *
	 * @param array $matchTypes The key is the name and the value is the regex.
	 */
	public function addMatchTypes($matchTypes)
	{
		$this->matchTypes = array_merge($this->matchTypes, $matchTypes);
	}

	/**
	 * Map a route to a target
	 *
	 * @param string $method One of 5 HTTP Methods, or a pipe-separated list of multiple HTTP Methods (GET|POST|PATCH|PUT|DELETE)
	 * @param string $route The route regex, custom regex must start with an @. You can use multiple pre-set regex filters, like [i:id]
	 * @param mixed $target The target where this route should point to. Can be anything.
	 * @param string $name Optional name of this route. Supply if you want to reverse route this url in your application.
	 * @throws Exception
	 */
	public function map($method, $route, $target, $name = null)
	{

		$this->routes[] = array($method, $route, $target, $name);

		if ($name)
		{
			if (isset($this->namedRoutes[$name]))
			{
				throw new \Exception("Can not redeclare route '{$name}'");
			}
			else
			{
				$this->namedRoutes[$name] = $route;
			}
		}

		return;
	}

	/**
	 * Reversed routing
	 *
	 * Generate the URL for a named route. Replace regexes with supplied parameters
	 *
	 * @param string $routeName The name of the route.
	 * @param array @params Associative array of parameters to replace placeholders with.
	 * @return string The URL of the route with named parameters in place.
	 * @throws Exception
	 */
	public function generate($routeName, array $params = array())
	{

		// Check if named route exists
		if (!isset($this->namedRoutes[$routeName]))
		{
			throw new \Exception("Route '{$routeName}' does not exist.");
		}

		// Replace named parameters
		$route = $this->namedRoutes[$routeName];

		// prepend base path to route url again
		$url = $this->basePath . $route;

		if (preg_match_all('`(/|\.|)\[([^:\]]*+)(?::([^:\]]*+))?\](\?|)`', $route, $matches, PREG_SET_ORDER))
		{

			foreach ($matches as $match)
			{
				list($block, $pre, $type, $param, $optional) = $match;

				if ($pre)
				{
					$block = substr($block, 1);
				}

				if (isset($params[$param]))
				{
					$url = str_replace($block, $params[$param], $url);
				}
				elseif ($optional)
				{
					$url = str_replace($pre . $block, '', $url);
				}
			}
		}

		return $url;
	}

	/**
	 * Match a given Request Url against stored routes
	 * @param string $requestUrl
	 * @param string $requestMethod
	 * @return array|boolean Array with route information on success, false on failure (no match).
	 */
	public function match($requestUrl = null, $requestMethod = null)
	{

		$params = array();
		$match = false;

		// set Request Url if it isn't passed as parameter
		if ($requestUrl === null)
		{
			$requestUrl = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : '/';
		}

		// strip base path from request url
		$requestUrl = substr($requestUrl, strlen($this->basePath));
		// Strip query string (?a=b) from Request Url
		if (($strpos = strpos($requestUrl, '?')) !== false)
		{
			$requestUrl = substr($requestUrl, 0, $strpos);
		}

		// set Request Method if it isn't passed as a parameter
		if ($requestMethod === null)
		{
			$requestMethod = isset($_SERVER['REQUEST_METHOD']) ? $_SERVER['REQUEST_METHOD'] : 'GET';
		}

		foreach ($this->routes as $handler)
		{
			list($methods, $route, $target, $name) = $handler;

			$method_match = (stripos($methods, $requestMethod) !== false);

			// Method did not match, continue to next route.
			if (!$method_match)
				continue;

			if ($route === '*')
			{
				// * wildcard (matches all)
				$match = true;
			}
			elseif (isset($route[0]) && $route[0] === '@')
			{
				// @ regex delimiter
				$pattern = '`' . substr($route, 1) . '`u';
				$match = preg_match($pattern, $requestUrl, $params) === 1;
			}
			elseif (($position = strpos($route, '[')) === false)
			{
				// No params in url, do string comparison
				$match = strcmp($requestUrl, $route) === 0;
			}
			else
			{
				// Compare longest non-param string with url
				if (strncmp($requestUrl, $route, $position) !== 0)
				{
					continue;
				}
				$regex = $this->compileRoute($route);
				$match = preg_match($regex, $requestUrl, $params) === 1;
			}

			if ($match)
			{

				if ($params)
				{
					foreach ($params as $key => $value)
					{
						if (is_numeric($key))
							unset($params[$key]);
					}
				}

				return array(
					'target' => $target,
					'params' => $params,
					'name' => $name
				);
			}
		}
		return false;
	}

	/**
	 * Compile the regex for a given route (EXPENSIVE)
	 */
	private function compileRoute($route)
	{
		if (preg_match_all('`(/|\.|)\[([^:\]]*+)(?::([^:\]]*+))?\](\?|)`', $route, $matches, PREG_SET_ORDER))
		{

			$matchTypes = $this->matchTypes;
			foreach ($matches as $match)
			{
				list($block, $pre, $type, $param, $optional) = $match;

				if (isset($matchTypes[$type]))
				{
					$type = $matchTypes[$type];
				}
				if ($pre === '.')
				{
					$pre = '\.';
				}

				//Older versions of PCRE require the 'P' in (?P<named>)
				$pattern = '(?:'
						. ($pre !== '' ? $pre : null)
						. '('
						. ($param !== '' ? "?P<$param>" : null)
						. $type
						. '))'
						. ($optional !== '' ? '?' : null);

				$route = str_replace($block, $pattern, $route);
			}
		}
		return "`^$route$`u";
	}

	/**
	 * generuję url /controller/ =>action_index, /controller/test.html => action_test.html
	 */
	public function controller_load()
	{
		//pobieram dane requesta
		$requestUrl = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : '/';
		$requestUrl = substr($requestUrl, strlen($this->basePath));
		$requestUrl = !$requestUrl ? '/' : $requestUrl;
		//usuwam gety
		if (strpos($requestUrl, '?') !== FALSE)
		{
			$get = explode('?', $requestUrl);
			$requestUrl = $get[0];
		}
		$requestUrl_arr = explode('/', $requestUrl);
		$requestUrl_arr = array_filter($requestUrl_arr);

        $app_prefix = "App__" . (is_ed() ? 'Ed' : 'Front') . '__';

		//jeżeli są to obrazki, css, js to pokazuję zawartość pliku
		$template_dirs = array('_images','_css','_js','_fonts');
		if(!empty($requestUrl_arr) && in_array($requestUrl_arr[0],$template_dirs))
		{
			$requestUrl_arr[0] = str_replace('_','',$requestUrl_arr[0]);
			$template_name = Lib__Session::get('template');

			$file = BASE_URL . 'app/' . (is_ed() ? 'ed' : 'front') . '/templates/' . $template_name . '/_assets/' .
				implode('/',$requestUrl_arr);


			$file_path = str_replace(BASE_URL,BASE_PATH,$file);
			$file_main_path = str_replace("templates/$template_name/_assets","data",$file_path);
			$file_path_to_data = BASE_PATH . 'data/' . implode('/',$requestUrl_arr);

			if(is_file($file_path))
			{
				$content_type = mime_content_type($file_path);

				if(strpos($file_path,'.css') !== FALSE)
				{
					$content_type = "text/css";
				}

				header("Content-type: $content_type");
			}
            elseif(is_file($file_path_to_data))
            {
                $content_type = mime_content_type($file_path_to_data);
                header("Content-type: $content_type");
                $file_path = $file_path_to_data;
            }
			//nie ma pliku w templatce, sprawdzam czy jest w głównym katalogu
			elseif(is_file($file_main_path))
			{
				$content_type = mime_content_type($file_main_path);
				header("Content-type: $content_type");
				$file_path = $file_main_path;
			}
			else //w ogóle nie ma pliku :)
			{
				show_404();
				exit;
			}

			//wyświetlam plik
			readfile($file_path);

			exit;
		}

		//akcja kiedy mamy uri
		if (!empty($requestUrl_arr))
        {
            //jeżeli należy to do podstron to ustawiam klasę podstrony
            if ($this->is_pages_controller($requestUrl_arr))
            {
                $requestUrl_arr[0] = "Pages";
            }

            //są dwa sposoby albo pobieramy ze zdefiniowanego routera albo automat
            $router_class_name = $app_prefix . "Controller__" . ucfirst($requestUrl_arr[0]) . "__Router";

            if ($this->is_product_category_controller($requestUrl_arr))
            {
                $requestUrl_arr[0] = "Products";
                $router_class_name = "App__Modules__" . ucfirst($requestUrl_arr[0]) . "__" . (is_ed() ? 'Ed' : 'Front') . "__Controller__Router";
            }

            if (class_exists($router_class_name)) //jest klasa, więc jest zdefiniowany routing
	    {
                $this->set_from_defined_router($requestUrl, $router_class_name::set($requestUrl_arr));
            }
			else
			{
				$this->set_autoload_router($requestUrl, $requestUrl_arr);
			}

			//odczytuję parametry
			$match = $this->match();

			//wydobywam nazwę klasy którą będziemy ładować
			$match_class_data = explode('#', $match['target']);
			$class_name = $match_class_data[0];
			$action_name = isset($match_class_data[1]) ? $match_class_data[1] : NULL;

			if ($class_name == 'Controller____Index')
			{
			    $class_name = $class_name = $app_prefix . "Controller__Welcome__Index";
				$controller = new $class_name;
				if (method_exists($class_name, $action_name))
				{
					$controller->$action_name();
				}
				else
				{
					show_404();
				}
			}
			elseif (class_exists($class_name) && method_exists($class_name, $action_name))
			{
				$controller = new $class_name();
				$controller->params = $match['params'];
				$controller->$action_name();
			}
			else
			{
				show_404();
			}
		}
		else
		{
		    $class_name = $app_prefix . "Controller__Welcome__Index";
			$controller = new $class_name;
			$controller->action_index();
		}
	}

	private function is_pages_controller($url_data)
	{
		//określam czy to jest szukanie po kategorii czy po bezpośrednim adresie url
		$is_category = (!strpos($url_data[0], '.html') !== FALSE);

		//ustawiam szukany string
		$end_url_data_value = end($url_data);
		$string = $is_category ? $url_data[0] : $end_url_data_value;

		//pobieram id_podstrony
		if (!$is_category)
		{
			preg_match("/p[0-9]+/m", $string,$find_id);
			$find_id = reset($find_id);
			if(!empty($find_id) && strpos($find_id,'p') !== FALSE)
			{
				$id_page = (int)str_replace('p','',$find_id);

				//sprawdzam czy podstrona istnieje, wówczas zwracam TRUE
				$page = App__Ed__Model__Pages::find(App__Ed__Model__Pages::where("id=$id_page")->add("status=1"));

				if(!empty($page))
				{
					return TRUE;
				}
				else
				{
					return FALSE;
				}

			}

		}

		//klucz cache
		$cache_key = "Pages__Category__Routing::check" . ($is_category ? "_category" : "_page");

		//pobieram dane z cache
		$cache_data = Lib__Memcache::get($cache_key);

		if (!$cache_data)
		{

			$where = App__Ed__Model__Pages__Category::where("status = 1");
			$where->add("id_parent=0");

			$data = array();
			foreach (App__Ed__Model__Pages__Category::find($where) as $categories)
			{
				$data[] = $categories->name_seo;
			}

			$data = serialize($data);

			Lib__Memcache::set($cache_key, $data, array(
				'class_name' => get_class(),
				'key_name' => $cache_key
			));
		}
		else
		{
			$data = $cache_data;
		}

		$data = unserialize_data($data);

		return in_array($string, $data);
	}

    private function is_product_category_controller($url_data)
    {
        if (!empty($url_data) && $url_data[0] == 'products' && $url_data[1] == 'category')
        {
            $url_data = array_diff($url_data, ['products', 'category']);
            $url_data = array_values($url_data);

            $is_category = !empty($url_data[0]) && !strpos($url_data[0], '.html') !== false;

            $end_url_data_value = end($url_data);
            $string = $is_category ? $url_data[0] : $end_url_data_value;

            //pobieram id_podstrony
            if (!$is_category)
            {
                preg_match("/p[0-9]+/m", $string, $find_id);
                $find_id = reset($find_id);
                if (!empty($find_id) && strpos($find_id, 'p') !== false)
                {
                    $id_page = (int)str_replace('p', '', $find_id);

                    //sprawdzam czy podstrona istnieje, wówczas zwracam TRUE
                    $page = App__Ed__Model__Pages::find(App__Ed__Model__Pages::where("id=$id_page")->add("status=1"));

                    if (!empty($page))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                }

            }

            //klucz cache
            $cache_key = "Products__Category__Routing::check_" . implode('/', $url_data);

            //pobieram dane z cache
            $cache_data = Lib__Memcache::get($cache_key);

            if (!$cache_data)
            {
                $data = [];
                foreach (Product_category_model::all() as $categories)
                {
                    $data[] = $categories->seo_name;
                }

                $data = serialize($data);

                Lib__Memcache::set(
                    $cache_key, $data, [
                    'class_name' => get_class(),
                    'key_name'   => $cache_key,
                ]);
            }
            else
            {
                $data = $cache_data;
            }

            $data = unserialize_data($data);

            return in_array($string, $data);
        }
        else
        {
            return false;
        }
    }

    private function set_ucfirst_array($array)
	{
		$return = array();
		foreach ($array as $k => $v)
		{
			$return[$k] = ucfirst($v);
		}
		return $return;
	}

	private function set_from_defined_router($request, $router_array)
	{
		foreach ($router_array as $arr)
		{
			//robię wypad jak to nie ten klucz
			if (!class_exists($arr['controller']) && !method_exists($arr['controller'], $arr['method']))
			{
				continue;
			}

			//definiuję jaką klasę i jaką metodę mam odtworzyć
			else
			{
				if(strpos($arr['uri'],'?') !== FALSE)
				{
					$uri_arr = explode('?',$arr['uri']);
					$uri = reset($uri_arr);
				}
				else
				{
					$uri = $arr['uri'];
				}

				$this->map("GET", $uri, "{$arr['controller']}#{$arr['method']}");
			}
		}
	}

	private function set_autoload_router($request_uri, $request_uri_arr)
	{
		//zamieniam żeby każðy element tablicy zaczynał się od dużej litery
		$request_uri_arr = $this->set_ucfirst_array($request_uri_arr);

		//jeżeli w ostatnim elemencie tablicy jest *.html to * oznacza action_*(), jeżeli nie to action_index()
		$last_key = count($request_uri_arr) - 1;
		$last_element = $request_uri_arr[$last_key];

		//generujemy nazwę akcji
		$action = "action";

		//przypadek kiedy mamy odwołanie do action_*()
		if (strpos($last_element, '.html') !== FALSE)
		{
			$last_element = str_replace('.html', '', $last_element);
			$action .= "_$last_element";
			unset($request_uri_arr[$last_key]);
		}
		else //przypadek action_index()
		{
			$action .= "_index";
		}

		//zamieniam nazwę metody na male litery
		$action = strtolower($action);

		//generujemy nazwę szukanej frazy
        if(is_ed() && !empty($request_uri_arr) && $request_uri_arr[0] == 'Ed')
        {
            unset($request_uri_arr[0]);
        }

        $app_prefix = "App__" . (is_ed() ? 'Ed' : 'Front') . '__';
        
		$class_name = $app_prefix . "Controller__" . implode('__', $request_uri_arr);

		//szukam po klasach głównych
        if (!class_exists($class_name))
        {
            if(!empty($request_uri_arr))
            {
                $class_name .= "__";
            }

            $class_name .= "Index";

            //szukam w modelach
            if(!class_exists($class_name))
            {
                $first_key = key($request_uri_arr);
                $module_name = $request_uri_arr[$first_key];
                unset($request_uri_arr[$first_key]);
                 
                $module_class_name =  "App__Modules__" . $module_name . "__" . (is_ed() ? 'Ed' : 'Front') . "__Controller__";
                if(!empty($request_uri_arr))
                {
                    $module_class_name .= implode('__', $request_uri_arr);
                }
                else
                {
                    $module_class_name .= "Index";
                }

                $class_name = $module_class_name;
            }

        }

		if (empty($_POST))
		{
			$this->map("GET", $request_uri, "$class_name#$action");
		}
		else
		{
			$this->map("POST", $request_uri, "$class_name#$action");
		}
	}

}
