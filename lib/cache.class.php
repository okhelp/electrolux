<?php

class Lib__Cache
{
    public $sql_query;
    public $class_name;
    public $data;
    
    public function __construct()
    {
        
    }
    
    public function set_query($query)
    {
        $this->sql_query = $query;
    }
    
    public function set_class($class_name)
    {
        $this->class_name = $class_name;
    }
    
    public function set_data($data)
    {
        $this->data = $data;
    }
    
    public function get()
    {
        return Lib__Memcache::get($this->generate_key());
    }
    
    public function add()
    {
        $key = $this->generate_key();
        Lib__Memcache::set($key,$this->data, array(
            'class_name' => $this->class_name,
            'key_name' => $key,
            'query' => $this->sql_query
        ));
    }
    
    private function generate_key()
    {
        return $this->class_name.'::'.md5($this->sql_query);
    }
    
}