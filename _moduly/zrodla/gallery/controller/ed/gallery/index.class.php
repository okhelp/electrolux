<?php

use App\modules\gallery\ed\model\gallery\Gallery_categories_model;
use App\modules\gallery\ed\model\gallery\Gallery_photos;

class App__Ed__Controller__Gallery__Index extends Lib__Base__Ed_Controller
{
	public function action_lista()
	{
		//ładuję zmienne do szablonu
		$this->view->assign('list',App__Ed__Model__Files::find("all"));
		
		//ładuję szablon
		$this->template = 'gallery/lista.tpl';
		$this->page_title = "Galerie zdjęć - lista";
        $this->breadcrumb = array(''=>'Galerie zdjęć');
	}
	
	public function action_ed()
	{
		$id_gallery = !empty($_GET) && !empty($_GET['id']) ? (int)$_GET['id'] : 0;
		$is_ed = !empty($id_gallery);
		
		//tworze obiekt galerii
		$gallery = !empty($id_gallery) ? App__Ed__Model__Files::find($id_gallery) : new App__Ed__Model__Gallery;
		
		//zapis do bazy
		if(!empty($_POST))
		{
			
			//jeżeli jest to edycja to usuwam wszystkie zdjęcia
			if(!empty($id_gallery))
			{
				$photos = Gallery_photos::find(Gallery_photos::where("id_gallery = $id_gallery"));
				if(!empty($photos))
				{
					foreach($photos as $photo)
					{
						$photo->delete();
					}
				}	
			}
			
			//zapisuję galerię
			$gallery->id_category = (int)$_POST['id_category'];
			$gallery->name = $_POST['name'];
			$gallery->status = (int)$_POST['status'];
			$gallery->save();
			
			$id_gallery = $gallery->id;
			
			
			//dodaję zdjęcia 
			if(!empty($_POST['upload_1']))
			{
				foreach($_POST['upload_1'] as $id_cimg)
				{
					$gallery_photo = new Gallery_photos();
					$gallery_photo->id_gallery = $id_gallery;
					$gallery_photo->id_cimg = $id_cimg;
					$gallery_photo->save();
				}
			}
			
			//określam komunikat powrotu
			if($is_ed)
			{
				$msg = "Galeria została zmieniona";
			}
			else
			{
				$msg = "Galeria została dodana";
			}
			
			go_back("g|$msg",BASE_ED_URL.'gallery/lista.html');
		}
		
		//pobieram listę kategorii
		$this->view->assign('categories_list', Gallery_categories_model::find('all'));
		
		//pobiram listę zdjęć
		$photos_list = !empty($id_gallery) ? $this->get_photos($id_gallery) : array();
		
		//ładuję zmienne do szablonu
		$page_title = !empty($id_gallery) ? 
			'Dodaj nową galerię' : "Edycja galerii: " . $gallery->name;
		$this->view->assign('photos_list',$photos_list);
		$this->view->assign('gallery',$gallery);
		
		//ładuję szablon
		$this->template = 'gallery/ed.tpl';
		$this->page_title = "Galerie zdjęć - lista";
        $this->breadcrumb = array(
			'gallery/lista.tpl'=>'Galerie zdjęć',
			'' => $page_title
		);
	}
	
	private function get_photos($id_gallery)
	{
		$return = array();
		$photos_list = Gallery_photos::find(Gallery_photos::where("id_gallery = $id_gallery"));
		
		if(!empty($photos_list))
		{
			$ids = array();
			foreach($photos_list as $photo)
			{
				$ids[] = $photo->id_cimg;
			}
			$return = App__Ed__Model__Img::find(App__Ed__Model__Img::where("id IN (".implode(',',$ids).")"));
		}
		
		return $return;
	}
	
	public function action_usun()
	{
		$id_gallery = !empty($_GET) && !empty($_GET['id']) ? (int)$_GET['id'] : 0;
		
		if(!empty($id_gallery))
		{
			//pobieram obiekt galerii
			$gallery = App__Ed__Model__Files::find($id_gallery);
			if(empty($gallery))
			{
				go_back("d|Błąd. Podana galeria nie została znaleziona.");
			}
			
			//pobieram listę zdjęć, które są przypisane do galerri, a następnie je usuwam
			$photos_list = Gallery_photos::find(
				Gallery_photos::where("id_gallery = $id_gallery")
			);
			
			if(!empty($photos_list))
			{
				foreach($photos_list as $photo)
				{
					$photo->delete();
				}
			}
			
			//usuwam wpis o galerii zdjeć
			$gallery->delete();
			
			go_back("g|Galeria została poprawnie usunięta.");
			
		}
		else
		{
			go_back("d|Proszę podać id galerii, którą mam usunąć");
		}
		
	}
}