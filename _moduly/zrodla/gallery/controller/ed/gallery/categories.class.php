<?php

use App\modules\gallery\ed\model\gallery\Gallery_categories_model;

class App__Ed__Controller__Gallery__Categories extends Lib__Base__Ed_Controller
{
	public function action_lista()
	{
		//pobieram listę kategorii
		$this->view->assign('list', Gallery_categories_model::find('all'));
		
		//ładuję szablon
		$this->template = 'gallery/categories/lista.tpl';
		$this->page_title = "Galerie zdjęć - kategorie - lista";
        $this->breadcrumb = array(
			''=>'Kategorie galerii zdjęć'
		);
	}
	
	public function action_ed()
	{
		$id_category = !empty($_GET['id']) && !empty($_GET['id']) ? (int)$_GET['id'] : 0;
		
		//tworzę obiekt kategorii
		$category = !empty($id_category) ? 
			Gallery_categories_model::find($id_category) : new Gallery_categories_model();
		
		//zapis do bazy
		if(!empty($_POST))
		{
			//zbiram informację na temat uzupełnionych pól
			foreach($_POST as $name => $value)
			{
				$category->{$name} = $value;
			}
			
			//jeżeli jest dodawanie, to dodaję datę dodania
			if(empty($id_category))
			{
				$category->ts = date('Y-m-d H:i:s');
			}
			
			//zapis do bazy
			if($category->save())
			{
				if(!empty($id_category)) //komunikat: edycja
				{
					$msg = lang("Kategoria została zmieniona.");
				}
				else
				{
					$msg = lang("Kategoria została pomyślnie dodana.");
				}
				
				go_back("g|" . $msg, BASE_ED_URL . 'gallery/categories/lista.html');
			}
			else
			{
				go_back("d|".lang("Wystąpił błąd podczas zapisu."));
			}
		}
		
		//przekazuję zmienne do szablonu
		$this->view->assign('category',$category);
		$this->view->assign('is_ed', !empty($id_category));
		
		//ustawiam tytuł strony
		$page_title = !empty($id_category) 
			? "Edycja kategori: " . $category->name : "Dodaj nową kategorię";
		
		//ładuję szablon
		$this->template = 'gallery/categories/ed.tpl';
		$this->page_title = $page_title;
        $this->breadcrumb = array(
			'gallery/categories/lista.html'=>'Kategorie galerii zdjęć',
			'' => $page_title
		);
	}
	
	public function action_usun()
	{
		$id_category = !empty($_GET) && !empty($_GET['id']) ? (int)$_GET['id'] : 0;
		
		if(!empty($id_category))
		{
			//sprawdzam czy przypisana jest jakaś galeria do tej kategorii 
			//(nie pozwalam wówczas usuwać)
			$find_connection = App__Ed__Model__Files::find(App__Ed__Model__Files::where("id_category = $id_category"));
			if(!empty($find_connection))
			{
				go_back("d|".lang("Wystąpił błąd podczas usuwania. "
						. "Do tej kategorii przypisana jest min. 1 galeria zdjęć."));
			}
			
			//tworzę obiekt kategorii
			$category = Gallery_categories_model::find($id_category);
			
			//jeżeli nie ma takiej kategorii w bazie - robię wypad
			if(empty($category))
			{
				go_back("d|".lang("Kategoria nie została odnaleziona. "
						. "Proszę sprawdzić poprawność podanego numeru ID."));
			}
			
			//usuwam kategorię z bazy
			if($category->delete())
			{
				go_back("g|".lang("Kategoria została poprawnie usunięta."));
			}
			else
			{
				go_back("d|".lang("Wystąpił błąd podczas usuwania kategori."));
			}
		}
		else
		{
			go_back("d|".lang("Proszę podać numer ID usuwanej kategori"));
		}
	}
}