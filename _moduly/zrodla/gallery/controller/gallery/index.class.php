<?php

use App\modules\gallery\ed\gallery\model\Gallery;
use App\modules\gallery\ed\model\gallery\Gallery_categories_model;

class Controller__Gallery__Index extends Lib__Base__Controller
{
	protected $_url;
	
	public function __construct()
	{
		parent::__construct();
		
		//ustawiam adres url
		$this->_url = $this->prepare_url();	
	}
	
	public function action_index()
	{
		$this->execute();
	}
	
	private function prepare_url()
	{
		$url = $_SERVER['PATH_INFO'];
		$url = str_replace('/gallery/','',$url);
		return $url;
	}
	
	private function execute()
	{
		//jeżeli w adresie nie ma "," to uruachamiam metoda_xxx (bez .html)
		if(!strpos($this->_url,",") !== FALSE)
		{
			$method_name = str_replace('.html','',$this->_url);
			
			if(!method_exists($this, $method_name))
			{
				show_404();
			}
			else
			{
				$this->$method_name();
			}
		}
		else
		{
			//sprawdzam czy jest to lista kategorii
			
			if($id_category = $this->is_category_list_url())
			{
				$this->category_page($id_category);
			}
			elseif($id_gallery = $this->is_galery_url())
			{
				$this->gallery_page($id_gallery);
			}
			
		}
	}
	
	private function is_category_list_url()
	{
		preg_match('/,k(-?[1-9]+\d*).html/', $this->_url,$regex);
		if(!empty($regex) && !empty($regex[0]))
		{
			return (int)$regex[1];
		}
		else
		{
			return 0;
		}
	}
	
	private function is_galery_url()
	{
		preg_match('/,g(-?[1-9]+\d*).html/', $this->_url,$regex);
		if(!empty($regex) && !empty($regex[0]))
		{
			return (int)$regex[1];
		}
		else
		{
			return 0;
		}
	}
	
	
	private function category_page($id_category)
	{
		//pobieram dane kategorii
		$category = Gallery_categories_model::find($id_category);
		if(empty($category)) //nie ma kategorii, robię wypad
		{
			show_404();
		}
		
		//sprawdzam czy adres url jest poprawny
		verify_address($category->get_url());
		
		//pobieram zdjęcia z kategorii
		$list = Gallery::get_list(array(
			'id_category' => $id_category,
			'status' => 1
		));
		
		//ładuję zmienne do szablonu
		$this->view->assign('category',$category);
		$this->view->assign('list',$list);
		
		//ładuję szablon
		$this->template = 'gallery/category_page.tpl';
        $this->page_title = $category->name . ' - ' . lang("galeria zdjęć");
		$this->breadcrumb = array(
			'gallery/index.html' => lang('Galeria zdjęć'),
			'' => $category->name
		);
	}
	
	private function gallery_page($id_gallery)
	{		
		//pobieram wzystkie dostępne galerie zdjęć
		$list = Gallery::get_list(array(
			'id' => $id_gallery,
			'status' => 1
		),1);
		
		//nie ma takiej galerii, robię wypad z 404.
		if(empty($list['list']))
		{
			show_404();
		}
		
		//tworzę zmienne z obiekatmi
		$gallery = reset($list['list']);
		verify_address($gallery->get_url());
		$category = $gallery->get_category();
		$photos = $gallery->get_photos();
		
		//ładuję zmienne 
		$this->view->assign('gallery',$gallery);
		$this->view->assign('category',$category);
		$this->view->assign('photos',$photos);
		
		//ładuję szablon
		$this->template = 'gallery/page.tpl';
        $this->page_title = $gallery->name . " - " . lang("Galeria zdjęć");
		$this->breadcrumb = array(
			'gallery/page.html' => lang('Galeria zdjęć'),
			$category->get_url() => $category->name
		);
		
	}
	
	private function index()
	{
		//określam ilość galerii na stronie
		$on_page = 12;
		
		//określam aktualną stronę
		$current_page = !empty($_GET) && !empty($_GET['page']) ? (int)$_GET['page'] : 1;
		
		//pobieram wzystkie dostępne galerie zdjęć
		$list = Gallery::get_list(array(
			'status' => 1,
			'page' => $current_page,
			'on_page' => $on_page
		),1);
		
		//ładuję zmienne do szablonu
		$this->view->assign('current_page',$current_page-1);
		$this->view->assign('on_page',$on_page);
		$this->view->assign('list',$list['list']);
		$this->view->assign('total_rows',$list['total_rows']);
		
		//ładuję szablon
		$this->template = 'gallery/index.tpl';
        $this->page_title = lang("Galeria zdjęć");
		$this->breadcrumb = array(
			'gallery/index.html' => lang('Galeria zdjęć')
		);
	}
	
	private function kategorie()
	{
		//ładuję zmienne do szablonu
		$this->view->assign('list',Gallery::get_categories());
		
		//ładuję szablon
		$this->template = 'gallery/kategorie.tpl';
        $this->page_title = lang('Lista kategorii');
		$this->breadcrumb = array(
			'gallery/index.html' => lang('Galeria zdjęć'),
			'' => lang('Kategorie')
		);
	}
	
	public function action_galeria()
	{
		
	}
}