<?php

class Controller__Gallery__Router
{
	public function set()
	{
		$router = [];

		$router[] = array(
			'uri' => str_replace(BASE_DIR,'',$_SERVER['REQUEST_URI']),
			'controller' => 'Controller__Gallery__Index',
			'method' => 'action_index'
		);
		
		return $router;
	}
}