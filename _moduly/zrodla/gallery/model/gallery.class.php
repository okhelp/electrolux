<?php

use App\modules\gallery\ed\model\gallery\Gallery_categories_model;
use App\modules\gallery\ed\model\gallery\Gallery_photos;

class Gallery extends Lib__Base__Model
{
    static $table_name = "gallery";
    static $primary_key = "id";
	
	public function get_photos()
	{
		$where = Gallery_photos::where("id_gallery = " . $this->id);
		
		return Gallery_photos::find($where);
	}
	
	public function get_category()
	{
		$category = '';
		if($this->id_category)
		{
			$category = Gallery_categories_model::find($this->id_category);
		}
		
		return $category;
	}
	
	public function get_url()
	{
		$category = $this->get_category();
		return BASE_URL . "gallery/" . url_slug($category->name) . ',' . url_slug($this->name) 
				. ',g' . $this->id . '.html';
	}
}