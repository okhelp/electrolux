<?php

use App\modules\gallery\ed\model\gallery\Gallery_photos;

class Pages__Bbcode__Gallery
{
	public function prepare($id_gallery)
	{
		//sprawdzam czy galeria jest w bazie, przy okazji pobieram obiekt galerii
		$gallery = App__Ed__Model__Files::find($id_gallery);
		if(empty($gallery))
		{
			throw new Exception("Nie znaleziono galerii o id: $id_gallery");
		}
		
		//pobieram przypisane zdjęcia
		return Gallery_photos::find(Gallery_photos::where("id_gallery = $id_gallery"));
	}
}

