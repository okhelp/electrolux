<?php

class Gallery__Categories extends Lib__Base__Model
{
    static $table_name = "gallery_categories";
    static $primary_key = "id";
	
	public function get_url()
	{
		return BASE_URL . 'gallery/' . url_slug($this->name) . ',k' . $this->id . '.html';
	}
}