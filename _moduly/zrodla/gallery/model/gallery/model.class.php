<?php

use App\modules\gallery\ed\model\gallery\Gallery_categories_model;

class Gallery__Model
{
	public function get_list($params = array(), $with_total_rows = 0)
	{
		//ustawiam limit
		$limit = NULL;
		
		//przygotowuję parametry
		$where = array();
		if(!empty($params))
		{
			if(isset($params['page']) && !empty($params['page']) 
					&& isset($params['on_page']) && !empty($params['on_page']))
			{
				$page = (int)$params['page'] - 1;
				$on_page = (int)$params['on_page'];
				unset($params['page'], $params['on_page']);
				
				$offset = $page*$on_page;
				$limit = App__Ed__Model__Files::limit("$offset,$on_page");
			}
			
			foreach($params as $param_name => $param_value)
			{
				$where[] = $param_name . " = " . "'$param_value'";
			}
		}
		
		$sql = "SELECT SQL_CALC_FOUND_ROWS g.*, gl.name FROM gallery g ";
		//dołączam odpowiednią wersję językową
		$id_service = (int)Lib__Session::get('id_service');
		$id_lang = (int)Lib__Session::get('id_lang');
		$sql .= "INNER JOIN gallery_lang gl ON gallery_id = g.id AND "
				. "gl.id_service = $id_service AND gl.id_lang = $id_lang ";
		$sql .= !empty($where) ? " WHERE " . implode(" AND ",$where) : NULL;
		$list = App__Ed__Model__Files::find_by_sql($sql);
		
		if($with_total_rows)
		{
			$total_rows = reset(App__Ed__Model__Files::find_by_sql("SELECT FOUND_ROWS() as total"))->total;
			
			return array(
				'list' => $list,
				'total_rows' => $total_rows
			);
		}
		else
		{
			return $list;
		}
		
	}
	
	public function get_categories()
	{
		return Gallery_categories_model::find(Gallery_categories_model::order("name ASC"));
	}
}