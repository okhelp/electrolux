CREATE TABLE IF NOT EXISTS `gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_category` int(11) NOT NULL,
  `status` varchar(1) NOT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `status` (`status`),
  KEY `id_category` (`id_category`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;

CREATE TABLE IF NOT EXISTS `gallery_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;

CREATE TABLE IF NOT EXISTS `gallery_categories_lang` (
  `gallery_categories_id` int(11) NOT NULL,
  `id_lang` int(11) NOT NULL,
  `id_service` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  KEY `gallery_categories_id` (`gallery_categories_id`,`id_lang`,`id_service`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `gallery_lang` (
  `gallery_id` int(11) NOT NULL,
  `id_lang` int(11) NOT NULL,
  `id_service` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  KEY `gallery_id` (`gallery_id`),
  KEY `id_service` (`id_service`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `gallery_photos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_gallery` int(11) NOT NULL,
  `id_cimg` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_gallery` (`id_gallery`,`id_cimg`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;