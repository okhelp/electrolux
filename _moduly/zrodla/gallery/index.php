<?php

class Module_Install
{
	static $module_info = array(
		'code_name' => 'gallery',
		'name' => 'Galeria zdjęć',
		'version' => '1.0',
		'author' => 'Paweł Otłowski, goinweb.pl'
	);
	
	public $copy_dirs = array('controller', 'model', 'templates');
	public $copy_root_files = array();
	
	public static function after_install($id_module)
	{
		//dodaję nowy link do menu
		$menu = new App__Ed__Model__Modules__Ed_menu::;
		$menu->name = "Galeria zdjęć";
		$menu->id_module = $id_module;
		$menu->id_parent = 0;
		$menu->url = "#";
		$menu->save();
		$id_parent = $menu->id;
		
		$menu = new App__Ed__Model__Modules__Ed_menu::;
		$menu->id_module = $id_module;
		$menu->id_parent = $id_parent;
		$menu->name = "Lista";
		$menu->url = "gallery/lista.html";
		$menu->save();
		
		$menu = new App__Ed__Model__Modules__Ed_menu::;
		$menu->id_module = $id_module;
		$menu->id_parent = $id_parent;
		$menu->name = "Kategorie";
		$menu->url = "gallery/categories/lista.html";
		$menu->save();
		
	}
}