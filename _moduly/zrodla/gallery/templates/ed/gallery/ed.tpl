<form method="POST" enctype="multipart/form-data">
	<div class="form-group">
		<label>Nazwa:</label>
		<input type="text" name="name" class="form-control required" required="required" value="{if $gallery && $gallery->name}{$gallery->name}{/if}"/>
	</div>
	
	<div class="form-group">
		<label>Kategoria</label>
		<select name="id_category" class="form-control">
			{foreach from=$categories_list item=category}
				<option value="{$category->id}"{if $gallery && $gallery->id_category == $category->id} selected="selected"{/if}>{$category->name}</option>
			{/foreach}
		</select>
	</div>
	
	<div class="form-group">
		<label>Status:</label>
		<select name="status" class="form-control">
			<option value="1"{if $gallery && $gallery->status == 1} selected{/if}>włączona</option>
			<option value="0"{if $gallery && $gallery->status == 0} selected{/if}>wyłączona</option>
		</select>
	</div>
	<div class="form-group">
		<label>Lista zdjęć:</label>
		<div class="row">
			<div class="col-md-12">
				{include file="_partials/upload/images_form.tpl" name="site_social_logo" uploaded_data=$photos_list instance_number=1 file_limit=100 show_thumb=1 multiple=1}
			</div>
		</div>
	</div>

	<button type="submit" class="btn btn-success">Zapisz</button>

</form>
			
			
				
			