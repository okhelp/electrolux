{load_plugin name="datatable"}

<a href="ed.html" class="btn btn-info">Dodaj</a>

<table class='datatable'>
	<thead>
		<tr>
			<th>#id</th>
			<th>nazwa</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		{foreach from=$list item=l}
			<tr>
				<td>{$l->id}</td>
				<td>{$l->name}</td>
				<td class="text-right">
					<a href="{BASE_ED_URL}gallery/categories/ed.html?id={$l->id}" class='btn btn-default'>edytuj</a>
					<a href="{BASE_ED_URL}gallery/categories/usun.html?id={$l->id}" class='btn btn-danger confirm' data-confirm-text="Czy na pewno chcesz usunąć tą kategorię?">usuń</a>
				</td>
			</tr>
		{/foreach}
	</tbody>
</table>
