<?php

//ładowanie wszystkich klas
require_once __DIR__ . '/../lib/autoload.php';

Lib__Cron::init('Pobieranie błędów z loga',1, 'pawelo');

$error_file = file_get_contents(PHP_ERROR_PATH);        
if($error_file)
{
    $id_service = (int) Lib__Session::get('id_service');
    
    //usuwam podwójne spacje + przedłużenie linii zamieniam na spację, aby wszystko było w jednej lini
    $error_file = str_replace(array("<-- \n", "  "),"",$error_file);
    $error_file_arr = explode(PHP_EOL,$error_file);
    $error_file_arr = array_filter($error_file_arr);
    $errors = array();
    
    foreach ($error_file_arr as $error)
    {
        if(strpos($error,'error') !== FALSE || true)
        {
            $error = str_replace('[','',$error);
            $error_arr = explode(']',$error);
            
            $last_time = strtotime($error_arr[0]);
            $description = base64_encode(trim($error_arr[1]));
            
            //sprawdzam czy już taki błąd istnieje
            $find_error = App__Ed__Model__Error::find(
                    App__Ed__Model__Error::where("description='$description'")
            );
            
            if(!empty($find_error)) //mamy już taki błąd
            {
                $find_error = reset($find_error);
                $first_time = $find_error->first_time;
                $hits = $find_error->hits;
            }
            else //nie ma 
            {
                $first_time = $last_time;
                $hits = 0;
            }
            
            $item = !empty($find_error) ? $find_error : new App__Ed__Model__Error;
            if(empty($find_error))
            {
                $item->id_user = 0;
            }
            $item->description = $description;
            $item->hits = $hits + 1;
            $item->first_time = $first_time;
            $item->last_time = $last_time;
            $item->status = 0;
            $item->save();
        }
    }

    file_put_contents(PHP_ERROR_PATH, '');
}

Lib__Cron::end();