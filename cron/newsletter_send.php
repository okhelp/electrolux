<?php

use App\ed\model\mail\Add_to_queue;
use App\ed\model\newsletter\Newsletter_model;
use App\ed\model\newsletter\Users_options;
use App\ed\model\users\Can_send_email;
use App\ed\model\users\Get_company_users_list_by_user_type;
use App\front\models\company\Get_company_services;

require_once __DIR__ . '/../lib/autoload.php';

Lib__Cron::init('Wysyłka newslettera', 1440, 'pawelo');

try
{
    $newsletters = Newsletter_model::find_by_sql(
        "
        SELECT *
        FROM newsletter
        JOIN newsletter_lang ON newsletter.id=newsletter_lang.newsletter_id
        WHERE send_date='" . (new DateTime('now'))->format('Y-m-d') . "'
            AND status=" . Newsletter_model::STATUS_ACTIVE . "
    ");

    if(!empty($newsletters))
    {
        $employes_electrolux = Get_company_users_list_by_user_type::get(Get_company_services::ID_SERVICE_ELECTROLUX, 'user');
        $admins_electrolux = Get_company_users_list_by_user_type::get(Get_company_services::ID_SERVICE_ELECTROLUX, 'admin');
        $employes_aeg = Get_company_users_list_by_user_type::get(Get_company_services::ID_SERVICE_AEG, 'user');
        $admins_aeg = Get_company_users_list_by_user_type::get(Get_company_services::ID_SERVICE_AEG, 'admin');

        foreach ($newsletters as $newsletter)
        {
            if (!empty($newsletter->{Newsletter_model::COLUMN_STATUS}))
            {
                if ($newsletter->{Newsletter_model::COLUMN_SEND_TO_ADMIN})
                {
                    $admins = $newsletter->id_service == Get_company_services::ID_SERVICE_ELECTROLUX ? $admins_electrolux : $admins_aeg;
                    foreach ($admins as $admin)
                    {
                        if (!empty($admin->email) && Can_send_email::verify((int)$admin->id, Users_options::OPTION_SEND_NEWSLETTER))
                        {
                            $smarty = new Smarty();
                            $smarty->assign('newsletter_title', $newsletter->{Newsletter_model::COLUMN_MAIL_TITLE});
                            $smarty->assign('newsletter_text', $newsletter->{Newsletter_model::COLUMN_TEXT});

                            $add_to_queue = new Add_to_queue(
                                $admin->email,
                                $newsletter->{Newsletter_model::COLUMN_MAIL_TITLE},
                                $smarty->fetch(get_app_template_path() . '_emails/newsletter.tpl')
                            );

                            $add_to_queue->add();
                        }
                    }
                }
                if ($newsletter->{Newsletter_model::COLUMN_SEND_TO_EMPLOYEE})
                {
                    $employes = $newsletter->id_service == Get_company_services::ID_SERVICE_ELECTROLUX ? $employes_electrolux : $employes_aeg;
                    foreach ($employes as $employee)
                    {
                        if (!empty($employee->email) && Can_send_email::verify((int)$employee->id, Users_options::OPTION_SEND_NEWSLETTER))
                        {
                            $smarty = new Smarty();
                            $smarty->assign('newsletter_title', $newsletter->{Newsletter_model::COLUMN_MAIL_TITLE});
                            $smarty->assign('newsletter_text', $newsletter->{Newsletter_model::COLUMN_TEXT});

                            $add_to_queue = new Add_to_queue(
                                $employee->email,
                                $newsletter->{Newsletter_model::COLUMN_MAIL_TITLE},
                                $smarty->fetch(get_app_template_path() . '_emails/newsletter.tpl')
                            );

                            $add_to_queue->add();
                        }
                    }
                }
            }
        }
    }
}
catch (Exception $exception)
{
    echo '<pre>';
    print_r($exception->getMessage());
    echo '</pre>';
}

Lib__Cron::end();