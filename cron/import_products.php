<?php

use App\modules\products\front\objects\Products_elux_import;
use App\modules\products\front\objects\Products_import;

require_once __DIR__ . '/../lib/autoload.php';
set_time_limit(0);
Lib__Cron::init('Aktualizacja bazy produktów i ich kategorii', 1440, 'pawelo');

try
{
    $products_importer = new Products_import();
    $products_importer->run();

    echo '<h3>Zaimportowano produkty</h3>';

    try
    {
        $products_importer = new Products_elux_import();
        $products_importer->run();

        echo '<h3>Zaimportowano eluxy</h3>';
    }
    catch (Exception $e)
    {
        echo '<h3>Błąd podczas importowania eluxów</h3>';
    }

    //wyczyszczenie cache
    Lib__Memcache::delete('rp*');
    $memcache_log = App__Ed__Model__Memcache_log::find('all');
    if(!empty($memcache_log))
    {
        foreach($memcache_log as $item)
        {
            Lib__Memcache::delete($item->key_name);
        }
    }

    echo '<h3>Wyczyszczono cache</h3>';

    echo '<h1>Import został wykonany</h1>';
}
catch (Exception $exception)
{
    echo '<pre>';
    print_r($exception->getMessage());
    echo '</pre>';
}

Lib__Cron::end();