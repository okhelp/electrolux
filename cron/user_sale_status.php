<?php

use ActiveRecord\ConnectionManager;
use App\ed\model\mail\Add_to_queue;
use App\ed\model\mail\Send_email_by_queue;
use App\ed\model\newsletter\Users_options;
use App\ed\model\users\Can_send_email;
use App\ed\model\users\Get_by_ids;

require_once __DIR__ . '/../lib/autoload.php';

Lib__Cron::init('Wysyłka powiadomień o statusach zamówień użytkowników', 1440, 'pawelo');

try
{
    $connection = ConnectionManager::get_connection();

    $orders_query = $connection->query(
        "
        SELECT *
        FROM sale_history
        WHERE created_at BETWEEN NOW() - INTERVAL 22 DAY AND NOW()
    ");

    $orders = [];

    while ($row = $orders_query->fetch())
    {
        $orders[$row['id_customer']][] = $row;
    }

    if (!empty($orders))
    {
        $users_ids = array_keys($orders);

        $users = Get_by_ids::get($users_ids);

        foreach ($orders as $id_user => $user_orders)
        {
            $products = [];

            foreach ($user_orders as $user_order)
            {
                $products[] = '<li>' . implode(
                        ' - ', [$user_order['created_at'], $user_order['name'], $user_order['model'], $user_order['count'] . 'szt.',
                                $user_order['price_sum'] . 'zł']) . '</li>';
            }

            if(Can_send_email::verify((int)$this->_user->id, Users_options::OPTION_SEND_ORDER_STATUSES_SUMMARY_NOTIFICATION))
            {
                $user = App__Ed__Model__Users::find($this->_user->id);

                Send_email_by_queue::send(
                    (string)$users[$id_user]->email,
                    'Podsumowanie statusów sprzedaży - Electrolux od kuchni',
                    'Podsumowanie statusów sprzedaży',
                    "<ul>" . implode(' ', $products) . "</ul>"
                );
            }
        }
    }
}
catch (Exception $exception)
{
    echo '<pre>';
    print_r($exception->getMessage());
    echo '</pre>';
}

Lib__Cron::end();