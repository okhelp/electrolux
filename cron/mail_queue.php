<?php

use App\ed\model\mail\Email_queue_model;

require_once __DIR__ . '/../lib/autoload.php';

Lib__Cron::init('Kolejka mailowa', 1, 'pawelo');

try
{
    $emails = Email_queue_model::find_by_sql(
        "
        SELECT *
        FROM email_queue
        WHERE send_status!=" . Email_queue_model::STATUS_SENT . "
            AND retry_attempts < 5
        ORDER BY priority
        LIMIT 50
    ");

    if (!empty($emails))
    {
        foreach ($emails as $email)
        {
            $send_status = App__Ed__Model__Email::send_email(
                $_SERVER['HTTP_HOST'] == 'localhost' ? $_SERVER['SERVER_ADMIN'] : $email->{Email_queue_model::COLUMN_ADDRESS},
                $email->{Email_queue_model::COLUMN_SUBJECT},
                $email->{Email_queue_model::COLUMN_CONTENT}
            );

            $queue_element = Email_queue_model::find($email->{Email_queue_model::COLUMN_ID});
            $queue_element->{Email_queue_model::COLUMN_SEND_STATUS} = $send_status ? Email_queue_model::STATUS_SENT : Email_queue_model::STATUS_FAILED;
            $queue_element->{Email_queue_model::COLUMN_RETRY_ATTEMPTS} = $send_status ? $queue_element->{Email_queue_model::COLUMN_RETRY_ATTEMPTS} :
                ($queue_element->{Email_queue_model::COLUMN_RETRY_ATTEMPTS} + 1);
            $queue_element->save();
        }
    }
}
catch (Exception $exception)
{
    echo '<pre>';
    print_r($exception->getMessage());
    echo '</pre>';
}

Lib__Cron::end();