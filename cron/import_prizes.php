<?php

use App\ed\model\log\Log;
use App\modules\prizes\ed\objects\Prizes_import;

require_once __DIR__ . '/../lib/autoload.php';

Lib__Cron::init('Kolejka mailowa', 1, 'pawelo');

try
{
    $prizes_importer = new Prizes_import();
    $prizes_importer->run();

    echo '<h1>Import wykonany pomyślnie!</h1>';
} catch (Exception $exception)
{

    $log = new Log(Prizes_import::LOG_FILE_PATH . 'log_prizes_import_' . (new DateTime('now'))->format('Y-m-d') . '.txt');
    $log->add($exception->getMessage());

    echo '<pre>';
    print_r($exception->getMessage());
    echo '</pre>';
}

Lib__Cron::end();