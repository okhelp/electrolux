<?php

use App\front\models\company\Clients_import;

require_once  __DIR__ . '/../lib/autoload.php';

Lib__Cron::init('Import nowych firm', 1440, 'pawelo');

try
{
    $products_importer = new Clients_import();
    $products_importer->run();

    echo '<h1>Import został wykonany</h1>';

} catch (Exception $exception)
{
    echo '<pre>';
    print_r($exception->getMessage());
    echo '</pre>';
}

Lib__Cron::end();