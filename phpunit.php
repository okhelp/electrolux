<?php

//ustawienie głównego kodowania
ini_set('default_charset', 'utf-8');
mb_internal_encoding('utf-8');
mb_detect_order('utf-8');

//ładowanie wszystkich klass
require_once __DIR__.'/lib/autoload.php';

//ładowanie zabezpieczeń systemu
Lib__Security::secure_uri();

$_SERVER = array();
$_COOKIE = array();
$_SESSION = array();