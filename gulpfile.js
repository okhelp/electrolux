const gulp = require('gulp');
const sass = require("gulp-sass");
const autoprefixer = require("gulp-autoprefixer");


// var autoprefixer = require('autoprefixer');
// var imageResize = require('gulp-image-resize');
//
//
// gulp.task('css', () =>
//     gulp.src('css/*.css')
//         .pipe(autoprefixer({
//             browsers: ['last 4 versions'],
//             cascade: false,
//             grid: true
//         }))
//         .pipe(gulp.dest('cssPREFIX'))
// );

gulp.task('autoprefixer', function () {
    var postcss      = require('gulp-postcss');
    var sourcemaps   = require('gulp-sourcemaps');
    var autoprefixer = require('autoprefixer');

    return gulp.src('data/front/_assets/css/*.css')
        .pipe(sourcemaps.init())
        .pipe(postcss([ autoprefixer({
            browsers: ['last 4 versions'],
            cascade: false,
            grid: "autoplace"
        }) ]))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('data/front/_assets/css/'));
});

// const image = require('gulp-image');
// gulp.task('img', function () {
//     gulp.src('./img/*')
//         .pipe(image())
//         .pipe(gulp.dest('./imgOPTIMALIZE'));
// });

var paths = {
    dest: "templates/default/_assets/",
    src: "templates/default/_assets/",
    styles: {
        src: "data/front/_assets/css/**/*.sass",
        dest: "data/front/_assets/css"
    }
};

function style() {
    return gulp
        .src(paths.styles.src)
        .pipe(sourcemaps.init())
        .pipe(sass().on("error", sass.logError))
        .pipe(
            autoprefixer({
            })
        )
        .pipe(sourcemaps.write("./"))
        .pipe(gulp.dest(paths.styles.dest))
        .pipe(browserSync.stream());
}
function watch() {
    gulp.watch(paths.styles.src, style);
}

exports.watch = watch;

gulp.task('resize', function () {
    gulp.src('./img/*')
        .pipe(imageResize({
            width : 640,
            height : 500,
            crop : true,
            upscale : false
        }))
        .pipe(gulp.dest('./imgResize'));
});


