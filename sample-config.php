<?php
 
/**
 * Plik konfiguracyny goframework
 */

//ustawiam stałe systemowe
define('CMS_VERSION','2.5.51');
define('CMS_LICENCE', 'goinweb.pl');
define('MINIFY', false);

//ustawianie rodzaju środowiska
//define('PRODUCTION', !empty($_SERVER['REMOTE_ADDR']) && (!in_array($_SERVER['REMOTE_ADDR'], ['85.222.111.54', '127.0.0.1', '::1']) ? false : false));
define('PRODUCTION',true);
if (!PRODUCTION || true)
{
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
}


//ustawiam lokalizacje
setlocale(LC_MONETARY,"pl_PL");
date_default_timezone_set('Europe/Warsaw');

//czas ładowania strony
set_time_limit(30);

//limit pamięci strony
ini_set('memory_limit','128M');

//połącznie z bazą danych
if(!defined('DB_HOST'))
{
    define('DB_HOST', 'dev.dev.it');
}
if(!defined('DB_NAME'))
{  
    define('DB_NAME', 'elektrolux');
}
if(!defined('DB_USERNAME'))
{
    define('DB_USERNAME', 'electroluxdbuser');
}
if(!defined('DB_PASSWORD'))
{
    define('DB_PASSWORD', 'devdevdev');
}


//definiujemy główne ściezki
define('BASE_PATH',dirname(dirname(__FILE__)).'/');
define('BASE_DIR','/');

//automatyczne logowanie
define('COOKIENAME_AUTOLOGIN','AL_Ess4kwo');

//memcache
if (DIRECTORY_SEPARATOR == '/')
{
    define('MEMCACHE_SERVER', '127.0.0.1');
    define('MEMCACHE_PORT', '11211');
    define('MEMCACHE_EXPIRE', 60 * 60 * 24);
}

//ilość rekursywnych odwołań
ini_set("pcre.recursion_limit",10000);

//logowanie błędów
define('PHP_ERROR_PATH',BASE_PATH."logs/error/php-error.log");
ini_set("log_errors", 1);
ini_set("error_log", PHP_ERROR_PATH);
